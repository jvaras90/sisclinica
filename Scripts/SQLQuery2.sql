Create Procedure usp_FciaCabIns
@TipoCliente char(1),
@Pac_HC varchar(20),
@ClienteNombre varchar(150),
@Total decimal(18,2)
As
Begin
	Insert Into FarmaciaCabecera
	(TipoCliente, Pac_HC, ClienteNombre, Total, Estado)
	Values
	(@TipoCliente, @Pac_HC, @ClienteNombre, @Total, 'GE')	
End
Go

Create Procedure usp_FciaCabUpd
@VentaId int,
@TipoCliente char(1),
@Pac_HC varchar(20),
@ClienteNombre varchar(150),
@Total decimal(18,2),
@Estado char(2)
As
Begin
	Update	FarmaciaCabecera
	Set		TipoCliente = @TipoCliente,
			Pac_HC = @Pac_HC,
			ClienteNombre = @ClienteNombre,
			Total = @Total,
			Estado = @Estado
	Where	VentaId = @VentaId
End
Go

Create Procedure usp_FciaDetIns
@VentaId int,
@Item int,
@Prd_Id int,
@Prd_PVP decimal(18,2),
@Prd_PVPFinal decimal(18,2),
@Cantidad int,
@TotalItem decimal(18,2)
As
Begin
	Insert Into FarmaciaDetalle
	(VentaId, Item, Prd_Id, Prd_PVP, Prd_PVPFinal, Cantidad, TotalItem)
	Values
	(@VentaId, @Item, @Prd_Id, @Prd_PVP, @Prd_PVPFinal, @Cantidad, @TotalItem)
End
Go

Create Procedure usp_FciaDetUpd
@VentaId int,
@Item int,
@Prd_Id int,
@Prd_PVP decimal(18,2),
@Prd_PVPFinal decimal(18,2),
@Cantidad int,
@TotalItem decimal(18,2)
As
Begin
	Update	FarmaciaDetalle
	Set		Prd_PVPFinal = @Prd_PVPFinal,
			Cantidad = @Cantidad,
			TotalItem = @TotalItem
	Where	VentaId = @VentaId
	And		Item = @Item
	And		Prd_Id = @Prd_Id
End
Go

Create Procedure usp_FciaDetDel
@VentaId int,
@Item int,
@Prd_Id int
As
Begin
	Delete	FarmaciaDetalle
	Where	VentaId = @VentaId
	And		Item = @Item
	And		Prd_Id = @Prd_Id
End
Go

Alter Procedure usp_FciaDetLis
As
Begin
	Select	a.Item, a.Prd_Id, 
			b.Prd_Descripcion As Producto, 
			a.Prd_PVP, 
			a.Prd_PVPFinal As Precio, 
			a.Cantidad, 
			a.TotalItem As Total
	From	FarmaciaDetalle a, Productos b
	Where	a.Prd_Id = b.Prd_Id
End
Go

Create Procedure usp_FciaDetRec
@VentaId int,
@Item int,
@Prd_Id int
As
Begin
	Select	a.Item, a.Prd_Id, b.Prd_Descripcion, a.Prd_PVP, a.Prd_PVPFinal, a.Cantidad, a.TotalItem
	From	FarmaciaDetalle a, Productos b
	Where	a.VentaId = @VentaId
	And		a.Item = @Item
	And		a.Prd_Id = @Prd_Id
	And		a.Prd_Id = b.Prd_Id
End
Go
