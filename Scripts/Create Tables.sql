USE [DermaBelle]
GO

/****** Object:  Table [dbo].[Alergias]    Script Date: 04/03/2015 14:15:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Alergias](
	[AlergiaId] [smallint] IDENTITY(1,1) NOT NULL,
	[TipoAlergiaId] [smallint] NOT NULL,
	[AlergiaNombre] [varchar](100) NOT NULL,
	[Estado] [bit] NOT NULL,
	[FecCrea] [datetime] NOT NULL,
	[UsuCrea] [smallint] NOT NULL,
	[FecModi] [datetime] NOT NULL,
	[UsuModi] [smallint] NOT NULL,
 CONSTRAINT [PK_Alergias] PRIMARY KEY CLUSTERED 
(
	[AlergiaId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[Almacenes]    Script Date: 04/03/2015 14:16:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Almacenes](
	[Alm_Id] [smallint] NOT NULL,
	[Alm_Cod] [varchar](2) NOT NULL,
	[Alm_Nombre] [varchar](50) NOT NULL,
	[Estado] [char](1) NOT NULL,
	[FecCrea] [datetime] NOT NULL,
	[UsuCrea] [smallint] NOT NULL,
	[FecMod] [datetime] NOT NULL,
	[UsuMod] [smallint] NOT NULL,
 CONSTRAINT [PK_Almacenes] PRIMARY KEY CLUSTERED 
(
	[Alm_Cod] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[Cirugias]    Script Date: 04/03/2015 14:16:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Cirugias](
	[Qui_Id] [smallint] IDENTITY(1,1) NOT NULL,
	[Qui_Nombre] [varchar](50) NOT NULL,
	[Estado] [bit] NOT NULL,
	[FecCrea] [datetime] NOT NULL,
	[UsuCrea] [smallint] NOT NULL,
	[FecModi] [datetime] NOT NULL,
	[UsuModi] [smallint] NOT NULL,
 CONSTRAINT [PK_Cirugias] PRIMARY KEY CLUSTERED 
(
	[Qui_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[Citas]    Script Date: 04/03/2015 14:16:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Citas](
	[CitaId] [smallint] IDENTITY(1,1) NOT NULL,
	[CitaFecha] [date] NOT NULL,
	[CitaHora] [time](7) NOT NULL,
	[Pac_HC] [varchar](20) NOT NULL,
	[Ser_Id] [smallint] NOT NULL,
	[Med_Id] [smallint] NOT NULL,
	[CitaEstado] [char](2) NOT NULL,
	[AntConsultaFecha] [date] NULL,
	[AntConsultaTipoAtencion] [char](1) NULL,
	[AntConsultaSer_Id] [smallint] NULL,
	[AntConsultaDiagCod] [varchar](10) NULL,
	[FichaMedicaId] [smallint] NULL,
	[FM_Estado] [char](1) NULL,
	[FecCrea] [datetime] NOT NULL,
	[UsuCrea] [smallint] NOT NULL,
	[FecModi] [datetime] NOT NULL,
	[UsuModi] [smallint] NOT NULL,
 CONSTRAINT [PK_Citas] PRIMARY KEY CLUSTERED 
(
	[CitaId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[ClasificacionTerapeutica]    Script Date: 04/03/2015 14:17:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ClasificacionTerapeutica](
	[ClasTerapId] [smallint] IDENTITY(1,1) NOT NULL,
	[ClasTerapNombre] [varchar](150) NOT NULL,
	[Estado] [bit] NOT NULL,
	[FecCrea] [datetime] NOT NULL,
	[UsuCrea] [smallint] NOT NULL,
	[FecModi] [datetime] NOT NULL,
	[UsuModi] [smallint] NOT NULL,
 CONSTRAINT [PK_ClasificacionTerapeutica] PRIMARY KEY CLUSTERED 
(
	[ClasTerapId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[Consultorios]    Script Date: 04/03/2015 14:24:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Consultorios](
	[Con_Id] [smallint] NOT NULL,
	[Con_Cod] [varchar](2) NOT NULL,
	[Con_Nombre] [varchar](50) NOT NULL,
	[Estado] [char](1) NOT NULL,
	[FecCrea] [datetime] NOT NULL,
	[UsuCrea] [smallint] NOT NULL,
	[FecMod] [datetime] NOT NULL,
	[UsuMod] [smallint] NOT NULL,
 CONSTRAINT [PK_Consultorios] PRIMARY KEY CLUSTERED 
(
	[Con_Cod] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[Diagnosticos]    Script Date: 04/03/2015 14:24:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Diagnosticos](
	[DiagCod] [varchar](10) NOT NULL,
	[DiagDescripcion] [varchar](300) NOT NULL,
	[Estado] [bit] NOT NULL,
	[FecCrea] [datetime] NOT NULL,
	[UsuCrea] [smallint] NOT NULL,
	[FecModi] [datetime] NOT NULL,
	[UsuModi] [smallint] NOT NULL,
 CONSTRAINT [PK_Diagnosticos] PRIMARY KEY CLUSTERED 
(
	[DiagCod] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[DocIdentidad]    Script Date: 04/03/2015 14:25:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DocIdentidad](
	[Did_Id] [smallint] IDENTITY(1,1) NOT NULL,
	[Did_Nombre] [varchar](50) NOT NULL,
	[Estado] [bit] NOT NULL,
	[FecCrea] [datetime] NOT NULL,
	[UsuCrea] [smallint] NOT NULL,
	[FecModi] [datetime] NOT NULL,
	[UsuModi] [smallint] NOT NULL,
 CONSTRAINT [PK_DocIdentidad] PRIMARY KEY CLUSTERED 
(
	[Did_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[Especialidades]    Script Date: 04/03/2015 14:25:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Especialidades](
	[Esp_Id] [smallint] IDENTITY(1,1) NOT NULL,
	[Esp_Nombre] [varchar](50) NOT NULL,
	[Estado] [bit] NOT NULL,
	[FecCrea] [datetime] NOT NULL,
	[UsuCrea] [smallint] NOT NULL,
	[FecModi] [datetime] NOT NULL,
	[UsuModi] [smallint] NOT NULL,
 CONSTRAINT [PK_Especialidades] PRIMARY KEY CLUSTERED 
(
	[Esp_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[ExamAyudaDiag]    Script Date: 04/03/2015 14:26:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ExamAyudaDiag](
	[EAD_Id] [smallint] NOT NULL,
	[TAD_Cod] [varchar](2) NOT NULL,
	[GAD_Cod] [varchar](2) NOT NULL,
	[EAD_Cod] [varchar](10) NOT NULL,
	[EAD_Nombre] [varchar](150) NOT NULL,
	[EAD_Observ] [varchar](500) NOT NULL,
	[Estado] [char](1) NOT NULL,
	[FecCrea] [datetime] NOT NULL,
	[UsuCrea] [smallint] NOT NULL,
	[FecMod] [datetime] NOT NULL,
	[UsuMod] [smallint] NOT NULL,
 CONSTRAINT [PK_ExamAyudaDiag] PRIMARY KEY CLUSTERED 
(
	[TAD_Cod] ASC,
	[GAD_Cod] ASC,
	[EAD_Cod] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[ExamenPreferencial]    Script Date: 04/03/2015 14:26:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ExamenPreferencial](
	[ExP_Id] [smallint] NOT NULL,
	[ExP_Cod] [varchar](2) NOT NULL,
	[ExP_Nombre] [varchar](50) NOT NULL,
	[ExP_Imagen] [varbinary](150) NULL,
	[Estado] [char](1) NOT NULL,
	[FecCrea] [datetime] NOT NULL,
	[UsuCrea] [smallint] NOT NULL,
	[FecMod] [datetime] NOT NULL,
	[UsuMod] [smallint] NOT NULL,
 CONSTRAINT [PK_ExamenPreferencial] PRIMARY KEY CLUSTERED 
(
	[ExP_Cod] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[ExamenPrefSegmentos]    Script Date: 04/03/2015 14:27:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ExamenPrefSegmentos](
	[ExP_Cod] [varchar](2) NOT NULL,
	[ExP_Segmento] [varchar](50) NOT NULL,
	[Estado] [char](1) NOT NULL,
	[FecCrea] [datetime] NOT NULL,
	[UsuCrea] [smallint] NOT NULL,
	[FecMod] [datetime] NOT NULL,
	[UsuMod] [smallint] NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[FichaMedica]    Script Date: 04/03/2015 14:27:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FichaMedica](
	[FichaMedicaId] [smallint] IDENTITY(1,1) NOT NULL,
	[Pac_TipoAtencion] [char](1) NOT NULL,
	[Pac_HC] [varchar](20) NOT NULL,
	[Pac_Edad] [smallint] NOT NULL,
	[GrI_Id] [smallint] NOT NULL,
	[ProfesionId] [smallint] NOT NULL,
	[FM_MotivoConsulta] [varchar](500) NULL,
	[FM_Alcohol] [bit] NULL,
	[FM_Tabaco] [bit] NULL,
	[FM_Drogas] [bit] NULL,
	[FM_OtrosHabNoc] [bit] NULL,
	[FM_ObservHabNoc] [varchar](250) NULL,
	[FM_FechaUltMenst] [varchar](20) NULL,
	[FM_PAMin] [smallint] NULL,
	[FM_Pulso] [smallint] NULL,
	[FM_Temp] [decimal](18, 1) NULL,
	[FM_Peso] [decimal](18, 3) NULL,
	[FM_Talla] [smallint] NULL,
	[FM_Lucido] [bit] NULL,
	[FM_OTiempo] [bit] NULL,
	[FM_OEspacio] [bit] NULL,
	[FM_OPersona] [bit] NULL,
	[FM_EstGeneral] [varchar](20) NULL,
	[FM_EstNutricion] [varchar](20) NULL,
	[FM_EstHidratacion] [varchar](20) NULL,
	[FM_PAMax] [smallint] NULL,
	[FM_ProxCita] [datetime] NULL,
	[FM_Recomend] [varchar](250) NULL,
	[FM_ObservGenerales] [varchar](250) NULL,
	[FM_Estado] [char](1) NULL,
	[FM_Fecha] [datetime] NULL,
	[FM_HoraIni] [time](7) NULL,
	[FM_HoraFin] [time](7) NULL,
	[FM_Servicio] [smallint] NULL,
 CONSTRAINT [PK_FichaMedica] PRIMARY KEY CLUSTERED 
(
	[FichaMedicaId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[FM_AntecAlergias]    Script Date: 04/03/2015 14:27:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FM_AntecAlergias](
	[FichaMedicaId] [smallint] NOT NULL,
	[Pac_HC] [varchar](20) NOT NULL,
	[AlergiaTipoId] [smallint] NOT NULL,
	[AlergiaId] [smallint] NOT NULL,
	[AlergiaObserv] [varchar](250) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[FM_AntecCirugias]    Script Date: 04/03/2015 14:27:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FM_AntecCirugias](
	[FichaMedicaId] [smallint] NOT NULL,
	[Pac_HC] [varchar](20) NOT NULL,
	[CirugiaId] [smallint] NOT NULL,
	[CirugiaObserv] [varchar](250) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[FM_AntecPatologicos]    Script Date: 04/03/2015 14:28:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FM_AntecPatologicos](
	[FichaMedicaId] [smallint] NOT NULL,
	[Pac_HC] [varchar](20) NOT NULL,
	[PatologiaId] [smallint] NOT NULL,
	[PatologiaObserv] [varchar](250) NULL,
 CONSTRAINT [PK_FM_AntecPatologicos] PRIMARY KEY CLUSTERED 
(
	[FichaMedicaId] ASC,
	[Pac_HC] ASC,
	[PatologiaId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[FM_AyudaDiagnostica]    Script Date: 04/03/2015 14:28:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FM_AyudaDiagnostica](
	[FichaMedicaId] [smallint] NOT NULL,
	[AyudaDiagTipo] [char](2) NOT NULL,
	[AyudaDiagId] [char](8) NOT NULL,
	[AyudaDiagObserv] [varchar](250) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[FM_Diagnosticos]    Script Date: 04/03/2015 14:30:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FM_Diagnosticos](
	[FichaMedicaId] [smallint] NOT NULL,
	[DiagCod] [varchar](10) NOT NULL,
	[DiagObserv] [varchar](250) NOT NULL,
	[DiagClasificacion] [char](1) NOT NULL,
	[DiagPrincipal] [bit] NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[FM_ExamPreferencial]    Script Date: 04/03/2015 14:30:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FM_ExamPreferencial](
	[FichaMedicaId] [smallint] NOT NULL,
	[ExamPrefId] [smallint] NOT NULL,
	[ExamPrefObserv] [varchar](500) NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[FM_FormulaMagistral]    Script Date: 04/03/2015 14:31:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FM_FormulaMagistral](
	[FichaMedicaId] [smallint] NOT NULL,
	[PrA_Id] [smallint] NOT NULL,
	[FormMagConcentracion] [decimal](18, 3) NOT NULL,
	[FormMagCantidad] [decimal](18, 2) NOT NULL,
	[FormMagIndicacion] [varchar](250) NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[FM_InfAyudaDiagnostica]    Script Date: 04/03/2015 14:31:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FM_InfAyudaDiagnostica](
	[FichaMedicaId] [smallint] NOT NULL,
	[InfAyudaDiagFecha] [date] NOT NULL,
	[InfAyudaDiagTipo] [char](1) NOT NULL,
	[InfAyudaDiagObserv] [varchar](500) NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[FM_Prescripcion]    Script Date: 04/03/2015 14:31:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FM_Prescripcion](
	[FichaMedicaId] [smallint] NOT NULL,
	[PrA_Id] [smallint] NOT NULL,
	[RpNomComercial] [varchar](150) NOT NULL,
	[RpIndicaciones] [varchar](250) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[FM_Procedimientos]    Script Date: 04/03/2015 14:32:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FM_Procedimientos](
	[FichaMedicaId] [smallint] NOT NULL,
	[ProcedId] [smallint] NOT NULL,
	[ProcedSesiones] [smallint] NOT NULL,
	[ProcedObserv] [varchar](250) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[FM_Sintomas]    Script Date: 04/03/2015 14:32:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FM_Sintomas](
	[FichaMedicaId] [smallint] NOT NULL,
	[SintomaId] [smallint] NOT NULL,
	[SintomaObserv] [varchar](250) NULL,
 CONSTRAINT [PK_FM_Sintomas] PRIMARY KEY CLUSTERED 
(
	[FichaMedicaId] ASC,
	[SintomaId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[GradosInstruccion]    Script Date: 04/03/2015 14:32:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[GradosInstruccion](
	[GrI_Id] [smallint] NOT NULL,
	[GrI_Nombre] [varchar](50) NOT NULL,
	[Estado] [bit] NOT NULL,
	[UsuCrea] [smallint] NOT NULL,
	[FecCrea] [datetime] NOT NULL,
	[UsuModi] [smallint] NOT NULL,
	[FecModi] [datetime] NOT NULL,
 CONSTRAINT [PK_GradosInstruccion] PRIMARY KEY CLUSTERED 
(
	[GrI_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[GruposAyudaDiag]    Script Date: 04/03/2015 14:33:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[GruposAyudaDiag](
	[GAD_Id] [smallint] NOT NULL,
	[GAD_Cod] [varchar](2) NOT NULL,
	[GAD_Nombre] [varchar](50) NOT NULL,
	[Estado] [char](1) NOT NULL,
	[FecCrea] [datetime] NOT NULL,
	[UsuCrea] [smallint] NOT NULL,
	[FecMod] [datetime] NOT NULL,
	[UsuMod] [smallint] NOT NULL,
 CONSTRAINT [PK_GruposAyudaDiag] PRIMARY KEY CLUSTERED 
(
	[GAD_Cod] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[Horarios]    Script Date: 04/03/2015 14:33:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Horarios](
	[Med_Id] [smallint] NOT NULL,
	[Con_Cod] [varchar](2) NOT NULL,
	[Hor_DiaLu] [char](1) NULL,
	[Hor_IniLu] [time](7) NULL,
	[Hor_FinLu] [time](7) NULL,
	[Hor_DiaMa] [char](1) NULL,
	[Hor_IniMa] [time](7) NULL,
	[Hor_FinMa] [time](7) NULL,
	[Hor_DiaMi] [char](1) NULL,
	[Hor_IniMi] [time](7) NULL,
	[Hor_FinMi] [time](7) NULL,
	[Hor_DiaJu] [char](1) NULL,
	[Hor_IniJu] [time](7) NULL,
	[Hor_FinJu] [time](7) NULL,
	[Hor_DiaVi] [char](1) NULL,
	[Hor_IniVi] [time](7) NULL,
	[Hor_FinVi] [time](7) NULL,
	[Hor_DiaSa] [char](1) NULL,
	[Hor_IniSa] [time](7) NULL,
	[Hor_FinSa] [time](7) NULL,
	[Hor_DiaDo] [char](1) NULL,
	[Hor_IniDo] [time](7) NULL,
	[Hor_FinDo] [time](7) NULL,
	[Estado] [char](1) NOT NULL,
	[FecCrea] [datetime] NOT NULL,
	[UsuCrea] [smallint] NOT NULL,
	[FecMod] [datetime] NOT NULL,
	[UsuMod] [smallint] NOT NULL,
 CONSTRAINT [PK_Horarios] PRIMARY KEY CLUSTERED 
(
	[Med_Id] ASC,
	[Con_Cod] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[Laboratorios]    Script Date: 04/03/2015 14:33:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Laboratorios](
	[Lab_Id] [smallint] IDENTITY(1,1) NOT NULL,
	[Lab_Nombre] [varchar](50) NOT NULL,
	[Estado] [bit] NOT NULL,
	[FecCrea] [datetime] NOT NULL,
	[UsuCrea] [smallint] NOT NULL,
	[FecModi] [datetime] NOT NULL,
	[UsuModi] [smallint] NOT NULL,
 CONSTRAINT [PK_Laboratorios] PRIMARY KEY CLUSTERED 
(
	[Lab_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[Medico_Especialidades]    Script Date: 04/03/2015 14:33:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Medico_Especialidades](
	[Med_Id] [smallint] NOT NULL,
	[Esp_Id] [smallint] NOT NULL,
	[Med_RNE] [varchar](10) NOT NULL,
	[Estado] [bit] NOT NULL,
	[FecCrea] [datetime] NOT NULL,
	[UsuCrea] [smallint] NOT NULL,
	[FecModi] [datetime] NOT NULL,
	[UsuModi] [smallint] NOT NULL,
 CONSTRAINT [PK_Medico_Especialidades] PRIMARY KEY CLUSTERED 
(
	[Med_Id] ASC,
	[Esp_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[Medico_Servicios]    Script Date: 04/03/2015 14:34:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Medico_Servicios](
	[Med_Id] [smallint] NOT NULL,
	[Ser_Id] [smallint] NOT NULL,
	[Estado] [bit] NOT NULL,
	[FecCrea] [datetime] NOT NULL,
	[UsuCrea] [smallint] NOT NULL,
	[FecModi] [datetime] NOT NULL,
	[UsuModi] [smallint] NOT NULL,
 CONSTRAINT [PK_Medico_Servicios] PRIMARY KEY CLUSTERED 
(
	[Med_Id] ASC,
	[Ser_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[Medicos]    Script Date: 04/03/2015 14:34:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Medicos](
	[Med_Id] [smallint] IDENTITY(1,1) NOT NULL,
	[Med_ApPat] [varchar](50) NOT NULL,
	[Med_ApMat] [varchar](50) NOT NULL,
	[Med_Nombre] [varchar](50) NOT NULL,
	[Med_NomComp] [varchar](150) NOT NULL,
	[Med_NumCol] [varchar](10) NOT NULL,
	[Med_Direccion] [varchar](150) NULL,
	[Med_Distrito] [varchar](10) NULL,
	[Med_Telefonos] [varchar](30) NULL,
	[Med_Email] [varchar](30) NULL,
	[Med_FIngreso] [datetime] NULL,
	[Estado] [bit] NOT NULL,
	[FecCrea] [datetime] NOT NULL,
	[UsuCrea] [smallint] NOT NULL,
	[FecModi] [datetime] NOT NULL,
	[UsuModi] [smallint] NOT NULL,
 CONSTRAINT [PK_Medicos] PRIMARY KEY CLUSTERED 
(
	[Med_NumCol] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[MovAlmCabecera]    Script Date: 04/03/2015 14:34:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[MovAlmCabecera](
	[Kdx_id] [smallint] NOT NULL,
	[Alm_Cod] [varchar](2) NOT NULL,
	[Mov_Cod] [varchar](2) NOT NULL,
	[Prv_Cod] [varchar](7) NOT NULL,
	[Mov_TDoc] [char](2) NOT NULL,
	[Mov_NDoc] [varchar](15) NOT NULL,
	[Pag_Cod] [varchar](2) NOT NULL,
	[Mov_FEmision] [datetime] NOT NULL,
	[Mov_FPago] [datetime] NOT NULL,
	[Mov_VVenta] [numeric](14, 2) NOT NULL,
	[Mov_IGV] [numeric](14, 2) NOT NULL,
	[Mov_Total] [numeric](14, 2) NOT NULL,
	[Mov_Observ] [varchar](200) NULL,
	[Mov_FRegistro] [datetime] NOT NULL,
	[Estado] [char](1) NOT NULL,
	[FecCrea] [datetime] NOT NULL,
	[UsuCrea] [smallint] NOT NULL,
	[FecMod] [datetime] NOT NULL,
	[UsuMod] [smallint] NOT NULL,
 CONSTRAINT [PK_MovAlmCabecera] PRIMARY KEY CLUSTERED 
(
	[Kdx_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[MovAlmDetalle]    Script Date: 04/03/2015 14:34:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[MovAlmDetalle](
	[Kdx_Id] [smallint] NOT NULL,
	[Alm_Cod] [varchar](2) NOT NULL,
	[Mov_Cod] [varchar](2) NOT NULL,
	[Prd_Cod] [varchar](7) NOT NULL,
	[Mov_Cant] [smallint] NOT NULL,
	[Mov_Stock] [smallint] NOT NULL,
	[Prd_VVF] [numeric](14, 2) NOT NULL,
	[Prd_PVF] [numeric](14, 2) NOT NULL,
	[Prd_PVP] [numeric](14, 2) NOT NULL,
	[Prd_CCompra] [numeric](14, 2) NOT NULL,
	[Prd_CPromedio] [numeric](14, 2) NOT NULL,
	[Prd_DctoC1] [numeric](14, 2) NOT NULL,
	[Prd_DctoC2] [numeric](14, 2) NOT NULL,
	[Prd_DctoC3] [numeric](14, 2) NOT NULL,
	[Prd_DctoC4] [numeric](14, 2) NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[Pacientes]    Script Date: 04/03/2015 14:35:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Pacientes](
	[Pac_HC] [varchar](20) NOT NULL,
	[Pac_ApPat] [varchar](50) NOT NULL,
	[Pac_ApMat] [varchar](50) NOT NULL,
	[Pac_Nombres] [varchar](50) NOT NULL,
	[Pac_NomComp] [varchar](150) NOT NULL,
	[Pac_Sexo] [char](1) NOT NULL,
	[Pac_LugNacId] [varchar](10) NULL,
	[Pac_FecNac] [datetime] NULL,
	[Pac_DocIdentId] [smallint] NOT NULL,
	[Pac_DocIdentNum] [varchar](20) NULL,
	[Pac_GpoSang] [varchar](20) NULL,
	[Pac_Direccion] [varchar](150) NULL,
	[Pac_DistritoId] [varchar](10) NULL,
	[Pac_Telefonos] [varchar](30) NULL,
	[Pac_Email] [varchar](30) NULL,
	[Pac_ProfesionId] [smallint] NULL,
	[Pac_InstruccionId] [smallint] NULL,
	[Pac_PersResp] [varchar](150) NULL,
	[Pac_DirecResp] [varchar](150) NULL,
	[Pac_DistRespId] [varchar](10) NULL,
	[Pac_TelefResp] [varchar](30) NULL,
	[Pac_EmailResp] [varchar](30) NULL,
	[Pac_NombreFact] [varchar](150) NULL,
	[Pac_DirecFact] [varchar](150) NULL,
	[Pac_DistFactId] [varchar](10) NULL,
	[Pac_RUCFact] [varchar](15) NULL,
	[Pac_EstadoHC] [char](1) NULL,
	[Pac_FecUltVisita] [datetime] NULL,
	[Pac_MotUltVisita] [char](1) NULL,
	[Estado] [bit] NOT NULL,
	[FecCrea] [datetime] NOT NULL,
	[UsuCrea] [smallint] NOT NULL,
	[FecModi] [datetime] NOT NULL,
	[UsuModi] [smallint] NOT NULL,
 CONSTRAINT [PK_Pacientes] PRIMARY KEY CLUSTERED 
(
	[Pac_HC] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[Patologias]    Script Date: 04/03/2015 14:35:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Patologias](
	[PatologiaId] [smallint] IDENTITY(1,1) NOT NULL,
	[PatologiaNombre] [varchar](100) NOT NULL,
	[Estado] [bit] NOT NULL,
	[FecCrea] [datetime] NOT NULL,
	[UsuCrea] [smallint] NOT NULL,
	[FecModi] [datetime] NOT NULL,
	[UsuModi] [smallint] NOT NULL,
 CONSTRAINT [PK_Patologias] PRIMARY KEY CLUSTERED 
(
	[PatologiaId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[Presentaciones]    Script Date: 04/03/2015 14:35:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Presentaciones](
	[Prs_Id] [smallint] IDENTITY(1,1) NOT NULL,
	[Prs_Nombre] [varchar](50) NOT NULL,
	[Estado] [bit] NOT NULL,
	[FecCrea] [datetime] NOT NULL,
	[UsuCrea] [smallint] NOT NULL,
	[FecModi] [datetime] NOT NULL,
	[UsuModi] [smallint] NOT NULL,
 CONSTRAINT [PK_Presentaciones_1] PRIMARY KEY CLUSTERED 
(
	[Prs_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[PrincipiosActivos]    Script Date: 04/03/2015 14:35:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[PrincipiosActivos](
	[PrA_Id] [smallint] IDENTITY(1,1) NOT NULL,
	[PrA_Nombre] [varchar](100) NOT NULL,
	[PrA_Tipo] [char](2) NOT NULL,
	[PrA_Presentacion] [varchar](50) NULL,
	[PrA_Dosis] [varchar](50) NULL,
	[PrA_Interacciones] [varchar](500) NULL,
	[PrA_Contraindicaciones] [varchar](500) NULL,
	[Estado] [bit] NOT NULL,
	[FecCrea] [datetime] NOT NULL,
	[UsuCrea] [smallint] NOT NULL,
	[FecModi] [datetime] NOT NULL,
	[UsuModi] [smallint] NOT NULL,
 CONSTRAINT [PK_PrincipiosActivos] PRIMARY KEY CLUSTERED 
(
	[PrA_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[Productos]    Script Date: 04/03/2015 14:36:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Productos](
	[Prd_Id] [smallint] IDENTITY(1,1) NOT NULL,
	[Prd_Descripcion] [varchar](100) NOT NULL,
	[TPr_Cod] [varchar](2) NOT NULL,
	[PrA_Cod] [smallint] NOT NULL,
	[Lab_Cod] [smallint] NOT NULL,
	[ClasTerapId] [smallint] NOT NULL,
	[Concentracion] [varchar](20) NULL,
	[Prs_Id] [smallint] NULL,
	[Prd_IGV] [char](1) NOT NULL,
	[Prd_VVF] [numeric](14, 2) NULL,
	[Prd_PVF] [numeric](14, 2) NULL,
	[Prd_PVP] [numeric](14, 2) NULL,
	[Prd_CCompra] [numeric](14, 2) NULL,
	[Prd_CPromedio] [numeric](14, 2) NULL,
	[Prd_DctoC1] [numeric](14, 2) NULL,
	[Prd_DctoC2] [numeric](14, 2) NULL,
	[Prd_DctoC3] [numeric](14, 2) NULL,
	[Prd_DEcto4] [numeric](14, 2) NULL,
	[Prd_Utilidad] [numeric](14, 2) NULL,
	[Estado] [bit] NOT NULL,
	[FecCrea] [datetime] NOT NULL,
	[UsuCrea] [smallint] NOT NULL,
	[FecModi] [datetime] NOT NULL,
	[UsuModi] [smallint] NOT NULL,
 CONSTRAINT [PK_Productos_1] PRIMARY KEY CLUSTERED 
(
	[Prd_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[Productos_Almacen]    Script Date: 04/03/2015 14:36:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Productos_Almacen](
	[Prd_Cod] [varchar](7) NOT NULL,
	[Alm_Cod] [varchar](2) NOT NULL,
	[Prd_Stock] [smallint] NOT NULL,
	[Estado] [char](1) NOT NULL,
	[FecCrea] [datetime] NOT NULL,
	[UsuCrea] [smallint] NOT NULL,
	[FecMod] [datetime] NOT NULL,
	[UsuMod] [smallint] NOT NULL,
 CONSTRAINT [PK_Productos_Almacen] PRIMARY KEY CLUSTERED 
(
	[Prd_Cod] ASC,
	[Alm_Cod] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[Profesiones]    Script Date: 04/03/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Profesiones](
	[Prf_Id] [smallint] NOT NULL,
	[Prf_Nombre] [varchar](50) NOT NULL,
	[Estado] [bit] NOT NULL,
	[FecCrea] [datetime] NULL,
	[UsuCrea] [smallint] NULL,
	[FecMod] [datetime] NULL,
	[UsuMod] [smallint] NULL,
 CONSTRAINT [PK_Profesiones] PRIMARY KEY CLUSTERED 
(
	[Prf_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[Proveedores]    Script Date: 04/03/2015 14:37:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Proveedores](
	[Prv_Id] [smallint] NOT NULL,
	[Prv_Cod] [varchar](7) NOT NULL,
	[PRv_Nombre] [varchar](50) NOT NULL,
	[Estado] [char](1) NOT NULL,
	[FecCrea] [datetime] NOT NULL,
	[UsuCrea] [smallint] NOT NULL,
	[Fec_Mod] [datetime] NOT NULL,
	[UsuMod] [smallint] NOT NULL,
 CONSTRAINT [PK_Proveedores] PRIMARY KEY CLUSTERED 
(
	[Prv_Cod] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[Secuencias]    Script Date: 04/03/2015 14:37:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Secuencias](
	[Sec_Id] [varchar](3) NOT NULL,
	[Sec_Nombre] [varchar](50) NOT NULL,
	[Sec_Ultimo] [smallint] NOT NULL,
	[Estado] [char](1) NOT NULL,
	[FecCrea] [datetime] NOT NULL,
	[UsuCrea] [smallint] NOT NULL,
	[FecMod] [datetime] NOT NULL,
	[UsuMod] [smallint] NOT NULL,
 CONSTRAINT [PK_Secuencias] PRIMARY KEY CLUSTERED 
(
	[Sec_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[Servicios]    Script Date: 04/03/2015 14:37:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Servicios](
	[Ser_Id] [smallint] IDENTITY(1,1) NOT NULL,
	[Ser_Nombre] [varchar](50) NOT NULL,
	[Estado] [bit] NOT NULL,
	[FecCrea] [datetime] NOT NULL,
	[UsuCrea] [smallint] NOT NULL,
	[FecMod] [datetime] NOT NULL,
	[UsuMod] [smallint] NOT NULL,
 CONSTRAINT [PK_Servicios] PRIMARY KEY CLUSTERED 
(
	[Ser_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[Sintomas]    Script Date: 04/03/2015 14:37:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Sintomas](
	[SintomaId] [smallint] IDENTITY(1,1) NOT NULL,
	[SintomaNombre] [varchar](50) NOT NULL,
	[Estado] [bit] NOT NULL,
	[FecCrea] [datetime] NOT NULL,
	[UsuCrea] [smallint] NOT NULL,
	[FecModi] [datetime] NOT NULL,
	[UsuModi] [smallint] NOT NULL,
 CONSTRAINT [PK_Sintomas] PRIMARY KEY CLUSTERED 
(
	[SintomaId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[Tarifario]    Script Date: 04/03/2015 14:38:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Tarifario](
	[Tar_Id] [smallint] NOT NULL,
	[TTr_Id] [smallint] NOT NULL,
	[Tar_Cod] [varchar](20) NOT NULL,
	[Tar_Nombre] [varchar](100) NOT NULL,
	[Tar_Valor] [numeric](14, 2) NOT NULL,
	[Tar_Impuesto] [char](1) NOT NULL,
	[Tar_Observ] [varchar](500) NULL,
	[Estado] [char](1) NOT NULL,
	[FecCrea] [datetime] NOT NULL,
	[UsuCrea] [smallint] NOT NULL,
	[FecMod] [datetime] NOT NULL,
	[UsuMod] [smallint] NOT NULL,
 CONSTRAINT [PK_Tarifario] PRIMARY KEY CLUSTERED 
(
	[Tar_Cod] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[TiposAlergia]    Script Date: 04/03/2015 14:38:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TiposAlergia](
	[TipoAlergiaId] [smallint] IDENTITY(1,1) NOT NULL,
	[TipoAlergiaNombre] [varchar](50) NOT NULL,
	[Estado] [bit] NOT NULL,
	[FecCrea] [datetime] NOT NULL,
	[UsuCrea] [smallint] NOT NULL,
	[FecModi] [datetime] NOT NULL,
	[UsuModi] [smallint] NOT NULL,
 CONSTRAINT [PK_TiposAlergia] PRIMARY KEY CLUSTERED 
(
	[TipoAlergiaId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[TiposAyudaDiag]    Script Date: 04/03/2015 14:39:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TiposAyudaDiag](
	[TAD_Id] [smallint] NOT NULL,
	[TAD_Cod] [varchar](2) NOT NULL,
	[TAD_Nombre] [varchar](50) NOT NULL,
	[Estado] [char](1) NOT NULL,
	[FecCrea] [datetime] NOT NULL,
	[UsuCrea] [smallint] NOT NULL,
	[FecMod] [datetime] NOT NULL,
	[UsuMod] [smallint] NOT NULL,
 CONSTRAINT [PK_TiposAyudaDiag] PRIMARY KEY CLUSTERED 
(
	[TAD_Cod] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[TipoMovAlmacen]    Script Date: 04/03/2015 14:38:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TipoMovAlmacen](
	[Mov_Id] [smallint] NOT NULL,
	[Mov_Cod] [varchar](2) NOT NULL,
	[Mov_Nombre] [varchar](50) NOT NULL,
	[Estado] [char](1) NOT NULL,
	[FecCrea] [datetime] NOT NULL,
	[UsuCrea] [smallint] NOT NULL,
	[FecMod] [datetime] NOT NULL,
	[UsuMod] [smallint] NOT NULL,
 CONSTRAINT [PK_TipoMovAlmacen] PRIMARY KEY CLUSTERED 
(
	[Mov_Cod] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[TiposProducto]    Script Date: 04/03/2015 14:39:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TiposProducto](
	[TPr_Id] [smallint] NOT NULL,
	[TPr_Cod] [varchar](2) NOT NULL,
	[TPr_Nombre] [varchar](50) NOT NULL,
	[Estado] [char](1) NOT NULL,
	[FecCrea] [datetime] NOT NULL,
	[UsuCrea] [smallint] NOT NULL,
	[FecMod] [datetime] NOT NULL,
	[UsuMod] [smallint] NOT NULL,
 CONSTRAINT [PK_TiposProducto] PRIMARY KEY CLUSTERED 
(
	[TPr_Cod] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[TiposTarifa]    Script Date: 04/03/2015 14:39:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TiposTarifa](
	[TTr_Id] [smallint] IDENTITY(1,1) NOT NULL,
	[TTr_Codigo] [char](2) NOT NULL,
	[TTr_Nombre] [varchar](50) NOT NULL,
	[Estado] [bit] NOT NULL,
	[FecCrea] [datetime] NOT NULL,
	[UsuCrea] [smallint] NOT NULL,
	[FecModi] [datetime] NOT NULL,
	[UsuModi] [smallint] NOT NULL,
 CONSTRAINT [PK_TiposTarifa] PRIMARY KEY CLUSTERED 
(
	[TTr_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[Ubigeo]    Script Date: 04/03/2015 14:40:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Ubigeo](
	[Ubi_Id] [smallint] IDENTITY(1,1) NOT NULL,
	[Ubi_Cod] [varchar](10) NOT NULL,
	[Ubi_Nombre] [varchar](50) NOT NULL,
	[Estado] [bit] NOT NULL,
	[FecCrea] [datetime] NOT NULL,
	[UsuCrea] [smallint] NOT NULL,
	[FecModi] [datetime] NOT NULL,
	[UsuModi] [smallint] NOT NULL,
 CONSTRAINT [PK_Ubigeo] PRIMARY KEY CLUSTERED 
(
	[Ubi_Cod] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[ViasAdministracion]    Script Date: 04/03/2015 14:40:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ViasAdministracion](
	[VAd_Id] [smallint] NOT NULL,
	[VAd_Cod] [varchar](2) NOT NULL,
	[VAd_Nombre] [varchar](50) NOT NULL,
	[Estado] [char](1) NOT NULL,
	[FecCrea] [datetime] NOT NULL,
	[UsuCrea] [smallint] NOT NULL,
	[FecMod] [datetime] NOT NULL,
	[UsuMod] [smallint] NOT NULL,
 CONSTRAINT [PK_ViasAdministracion] PRIMARY KEY CLUSTERED 
(
	[VAd_Cod] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

