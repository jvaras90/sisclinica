Create Procedure usp_ProductosIns
@Prd_Id int,
@Prd_Descripcion varchar(100),
@TPr_Cod varchar(2),
@PrA_Cod int,
@Lab_Cod int,
@ClasTerapId int,
@Concentracion varchar(20),
@Prs_Id int,
@Prd_IGV numeric(14,2),
@Prd_VVF numeric(14,2),
@Prd_PVF numeric(14,2),
@Prd_PVP numeric(14,2),
@Prd_CCompra numeric(14,2),
@Prd_CPromedio numeric(14,2),
@Prd_DctoC1 numeric(14,2),
@Prd_DctoC2 numeric(14,2),
@Prd_DctoC3 numeric(14,2),
@Prd_DctoC4 numeric(14,2),
@Prd_Utilidad numeric(14,2),
@Estado bit,
@UsuCrea int,
@UsuModi int
As
Begin
	Insert Into Productos
	(Prd_Descripcion, TPr_Cod, PrA_Cod, Lab_Cod, ClasTerapId, Concentracion, Prs_Id, Prd_IGV,
	 Prd_VVF, Prd_PVF, Prd_PVP, Prd_CCompra, Prd_CPromedio, Prd_DctoC1, Prd_DctoC2, Prd_DctoC3,
	 Prd_DctoC4, Prd_Utilidad, Estado, UsuCrea, FecCrea, UsuModi, FecModi)
Values
	(@Prd_Descripcion, @TPr_Cod, @PrA_Cod, @Lab_Cod, @ClasTerapId, @Concentracion, @Prs_Id, @Prd_IGV,
	 @Prd_VVF, @Prd_PVF, @Prd_PVP, @Prd_CCompra, @Prd_CPromedio, @Prd_DctoC1, @Prd_DctoC2, @Prd_DctoC3,
	 @Prd_DctoC4, @Prd_Utilidad, @Estado, @UsuCrea, GetDate(), @UsuModi, GetDate())

End
Go

Create Procedure usp_ProductosUpd
@Prd_Id int,
@Prd_Descripcion varchar(100),
@TPr_Cod varchar(2),
@PrA_Cod int,
@Lab_Cod int,
@ClasTerapId int,
@Concentracion varchar(20),
@Prs_Id int,
@Prd_IGV numeric(14,2),
@Prd_VVF numeric(14,2),
@Prd_PVF numeric(14,2),
@Prd_PVP numeric(14,2),
@Prd_CCompra numeric(14,2),
@Prd_CPromedio numeric(14,2),
@Prd_DctoC1 numeric(14,2),
@Prd_DctoC2 numeric(14,2),
@Prd_DctoC3 numeric(14,2),
@Prd_DctoC4 numeric(14,2),
@Prd_Utilidad numeric(14,2),
@Estado bit,
@UsuCrea int,
@UsuModi int
As
Begin
	Update	Productos
	Set		Prd_Descripcion = @Prd_Descripcion,
			TPr_Cod = @TPr_Cod,
			PrA_Cod = PrA_Cod,
			Lab_Cod = Lab_Cod,
			ClasTerapId = ClasTerapId,
			Concentracion = Concentracion,
			Prs_Id = Prs_Id,
			Prd_IGV = Prd_IGV,
			Prd_VVF = Prd_VVF,
			Prd_PVF = Prd_PVF,
			Prd_PVP = Prd_PVP,
			Prd_CCompra = Prd_CCompra,
			Prd_CPromedio = Prd_CPromedio,
			Prd_DctoC1 = Prd_DctoC1,
			Prd_DctoC2 = Prd_DctoC2,
			Prd_DctoC3 = Prd_DctoC3,
			Prd_DctoC4 = Prd_DctoC4,
			Prd_Utilidad = Prd_Utilidad,
			Estado = Estado,
			UsuModi = UsuModi,
			FecModi = GetDate()
	Where	Prd_Id = @Prd_Id
End
Go

Create Procedure ProuctosRec
@Prd_Id int
As
Begin
	Select * From Productos Where Prd_Id = @Prd_Id
End