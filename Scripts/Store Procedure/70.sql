USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_ProfesionesUpd]    Script Date: 04/03/2015 17:45:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Modificación
CREATE Proc [dbo].[usp_ProfesionesUpd]
@Prf_Id int, @Prf_Nombre varchar(50),
@UsuMod int, @FecMod Datetime
As
Begin
	Update Profesiones
	Set	Prf_Nombre = @Prf_Nombre,
		FecMod = @FecMod,
		Usumod = @UsuMod
	Where Prf_Id = @Prf_Id
End

GO

