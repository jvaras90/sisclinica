USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_PrincipiosActivosA_D]    Script Date: 04/03/2015 17:44:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Anulación
CREATE Proc [dbo].[usp_PrincipiosActivosA_D]
@PrA_Id int, @Estado bit, @UsuModi int
As
Begin
	Update PrincipiosActivos
	Set	Estado = @Estado,
		FecModi = GETDATE(),
		Usumodi = @UsuModi
	Where PrA_Id = @PrA_Id
End

GO

