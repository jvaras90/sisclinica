USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_PrincipiosActivosRec]    Script Date: 04/03/2015 17:45:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Recupera datos
Create Proc [dbo].[usp_PrincipiosActivosRec]
@PrA_Id int
As
Begin
	Select * From PrincipiosActivos Where PrA_Id = @PrA_Id
End

GO

