USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_InfProductos]    Script Date: 04/03/2015 17:43:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Proc [dbo].[usp_InfProductos]
@Prd_Id int
As
Begin
	Select	ct.ClasTerapNombre, pd.Prd_Descripcion, la.Lab_Nombre, pd.concentracion, pr.Prs_Nombre
	From	Productos pd, Laboratorios la, Presentaciones pr, ClasificacionTerapeutica ct
	Where	pd.Lab_Cod = la.Lab_Id
	And		pd.prs_id = pr.Prs_Id
	And		pd.ClasTerapId = ct.ClasTerapId
	And		pd.Prd_Id = @Prd_Id
End

GO

