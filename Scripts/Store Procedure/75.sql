USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_TiposTarifaRec]    Script Date: 04/03/2015 17:46:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Recupera datos
CREATE Proc [dbo].[usp_TiposTarifaRec]
@TTr_Codigo char(2)
As
Begin
	Select * From TiposTarifa Where TTr_Codigo = @TTr_Codigo
End

GO

