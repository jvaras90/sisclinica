USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_FMPrescripcionLis]    Script Date: 04/03/2015 17:42:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Proc [dbo].[usp_FMPrescripcionLis]
@FichaMedicaId int
As
Begin
--	Select	g.PrA_Nombre [Principio Activo],r.RpNomComercial [Comercial], r.RpIndicaciones [Indicaciones]
	Select	r.PrA_Id [Principio Activo],r.RpNomComercial [Comercial], r.RpIndicaciones [Indicaciones]
	From	FM_Prescripcion r, PrincipiosActivos g
	Where	r.FichaMedicaId = @FichaMedicaId
	And		r.PrA_Id = g.PrA_Id  
End

GO

