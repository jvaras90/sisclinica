USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_PacientesRec]    Script Date: 04/03/2015 17:44:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Recupera datos
CREATE Proc [dbo].[usp_PacientesRec]
@Pac_HC varchar(20)
As
Begin
	Select * From Pacientes Where Pac_HC = @Pac_HC
End

GO

