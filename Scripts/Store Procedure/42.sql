USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_GradosInstruccionRec]    Script Date: 04/03/2015 17:43:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create Proc [dbo].[usp_GradosInstruccionRec]
@GrI_Id int
As
Begin
	Select * From GradosInstruccion Where GrI_Id = @GrI_Id
End

GO

