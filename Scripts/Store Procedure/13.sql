USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_CboMedicamentos]    Script Date: 04/03/2015 17:39:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Proc [dbo].[usp_CboMedicamentos]
@PrA_Id int
As
Begin
	Select	pd.Prd_Id,(pd.Prd_Descripcion + ' ' +
			pd.concentracion + ' ' + pr.Prs_Nombre) As Prd_Descripcion
	From	Productos pd, Laboratorios la, Presentaciones pr
	Where 	pd.Estado = 1
	And		pd.Lab_Cod = la.Lab_Id
	And		pd.prs_id = pr.Prs_Id
	And		pd.PrA_Cod = @PrA_Id
End

GO

