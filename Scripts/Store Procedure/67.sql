USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_ProfesionesAnu]    Script Date: 04/03/2015 17:45:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Proc [dbo].[usp_ProfesionesAnu]
@Prf_Id int, @UsuMmod int
As
Begin
	Update Profesiones
	Set	Estado = '0',
		FecMod = GETDATE(),
		Usumod = @UsuMmod
	Where Prf_Id = @Prf_Id
End

GO

