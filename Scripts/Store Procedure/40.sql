USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_GradosInstruccionA_D]    Script Date: 04/03/2015 17:42:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Anulación
Create Proc [dbo].[usp_GradosInstruccionA_D]
@GrI_Id int, @Estado bit, @UsuModi int
As
Begin
	Update GradosInstruccion
	Set	Estado = @Estado,
		FecModi = GETDATE(),
		Usumodi = @UsuModi
	Where GrI_Id = @GrI_Id
End

GO

