USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_GradosInstruccionLis]    Script Date: 04/03/2015 17:43:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Listar
CREATE Proc [dbo].[usp_GradosInstruccionLis]
As
Begin
	Select	GrI_Id As [Id], GrI_Nombre As [Grados Instrucción],
			Case Estado When '1' Then 'Si' Else 'No' End As Activo
	From	GradosInstruccion
End

GO

