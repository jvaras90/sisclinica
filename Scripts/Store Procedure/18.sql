USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_DgvMedicamentos]    Script Date: 04/03/2015 17:39:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create Proc [dbo].[usp_DgvMedicamentos]
@PrA_Id int
As
Begin
	Select	pd.Prd_Descripcion, la.Lab_Nombre, pd.concentracion, pr.Prs_Nombre
	From	Productos pd, Laboratorios la, Presentaciones pr
	Where	pd.Lab_Cod = la.Lab_Id
	And		pd.prs_id = pr.Prs_Id
	And		pd.PrA_Cod = @PrA_Id
	And		pd.Estado = 1
End

GO

