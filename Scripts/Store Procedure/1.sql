USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_AntecPatologicosAdd]    Script Date: 04/03/2015 17:36:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create Proc [dbo].[usp_AntecPatologicosAdd]
@FichaMedicaId int,
@Pac_HC varchar(20),
@PatologiaId int,
@PatologiaObserv varchar(250)
As
Begin
	Insert FM_AntecPatologicos
	(FichaMedicaId,Pac_HC,PatologiaId,PatologiaObserv)
	Values
	(@FichaMedicaId,@Pac_HC,@PatologiaId,@PatologiaObserv)
End 

GO

