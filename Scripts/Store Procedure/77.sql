USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_UbigeoA_D]    Script Date: 04/03/2015 17:46:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Anulación
CREATE Proc [dbo].[usp_UbigeoA_D]
@Ubi_Cod varchar(10), @Estado bit, @UsuModi int
As
Begin
	Update Ubigeo
	Set	Estado = @Estado,
		FecModi = GETDATE(),
		Usumodi = @UsuModi
	Where Ubi_Cod = @Ubi_Cod
End

GO

