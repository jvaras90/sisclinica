USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_UbigeoRec]    Script Date: 04/03/2015 17:46:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Recupera datos
CREATE Proc [dbo].[usp_UbigeoRec]
@Ubi_Cod varchar(10)
As
Begin
	Select * From Ubigeo Where Ubi_Cod = @Ubi_Cod
End

GO

