USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_LaboratoriosRec]    Script Date: 04/03/2015 17:43:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Recupera datos
Create Proc [dbo].[usp_LaboratoriosRec]
@Lab_Id int
As
Begin
	Select * From Laboratorios Where Lab_Id = @Lab_Id
End

GO

