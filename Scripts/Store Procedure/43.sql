USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_GradosInstruccionUpd]    Script Date: 04/03/2015 17:43:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create Proc [dbo].[usp_GradosInstruccionUpd]
@GrI_Id int, @GrI_Nombre varchar(50),
@UsuModi int
As
Begin
	Update GradosInstruccion
	Set	GrI_Nombre = @GrI_Nombre,
		FecModi = GETDATE(),
		UsuModi = @UsuModi
	Where GrI_Id = @GrI_Id
End

GO

