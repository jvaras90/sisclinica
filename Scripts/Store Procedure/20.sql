USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_DgvSintomasList]    Script Date: 04/03/2015 17:39:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Proc [dbo].[usp_DgvSintomasList]
@FichaNum int
AS
Begin
	Select s.SintomaNombre [Síntoma],f.SintomaObserv [Observación]
	From Sintomas s Left Join FM_Sintomas f
    On  s.SintomaId = f.SintomaId And F.FichaMedicaId = @FichaNum
	Order By s.SintomaNombre
End

GO

