USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_GradosInsruccionAdd]    Script Date: 04/03/2015 17:42:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create Proc [dbo].[usp_GradosInsruccionAdd]
@GrI_Id int, @GrI_Nombre varchar(50),
@Estado bit, @UsuCrea int, @UsuModi int
As
Begin
	Insert GradosInstruccion
	(GrI_Id, GrI_Nombre,Estado,UsuCrea,FecCrea,UsuModi,FecModi)
	Values
	(@GrI_Id, @GrI_Nombre,@Estado,@UsuCrea,GETDATE(),@UsuModi,GETDATE())
End

GO

