USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_DgvSintomasLoad]    Script Date: 04/03/2015 17:40:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create Proc [dbo].[usp_DgvSintomasLoad]
AS
Begin
	Select SintomaNombre [Síntoma], '' [Observación] From Sintomas 
	Where  Estado = 1
	And    SintomaId <> 1
End

GO

