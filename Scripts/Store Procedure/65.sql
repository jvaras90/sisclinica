USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_ProfesionesA_D]    Script Date: 04/03/2015 17:45:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create Proc [dbo].[usp_ProfesionesA_D]
@Prf_Id int, @Estado bit, @UsuMmod int
As
Begin
	Update Profesiones
	Set	Estado = @Estado,
		FecMod = GETDATE(),
		Usumod = @UsuMmod
	Where Prf_Id = @Prf_Id
End

GO

