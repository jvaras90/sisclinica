USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_InfPrincActivosFA]    Script Date: 04/03/2015 17:43:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Recupera información del Principio Activo para Mostrarla
Create Proc [dbo].[usp_InfPrincActivosFA]
@PrA_Id int
As
Begin
	Select	PrA_Nombre, PrA_Presentacion,PrA_Dosis, PrA_Interacciones,
			PrA_Contraindicaciones
	From	PrincipiosActivos
	Where	PrA_Id = @PrA_Id
End
GO

