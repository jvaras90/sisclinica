USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_PacientesAdd]    Script Date: 04/03/2015 17:44:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/******************************************************
         Mantenimiento de Pacientes
*******************************************************/
--Creación
CREATE Proc [dbo].[usp_PacientesAdd]
@Pac_HC varchar(20),
@Pac_ApPat varchar(50),
@Pac_ApMat varchar(50),
@Pac_Nombres varchar(50),
@Pac_NomComp varchar(150),
@Pac_Sexo char(1),
@Pac_LugNacId varchar(10),
@Pac_FecNac datetime,
@Pac_DocIdentId int,
@Pac_DocIdentNum varchar(20),
@Pac_GpoSang varchar(20),
@Pac_Direccion varchar(150),
@Pac_DistritoId varchar(10),
@Pac_Telefonos varchar(30),
@Pac_Email varchar(30),
@Pac_ProfesionId int,
@Pac_InstruccionId int,
@Pac_PersResp varchar(150),
@Pac_DirecResp varchar(150),
@Pac_DistRespId varchar(10),
@Pac_TelefResp varchar(30),
@Pac_EmailResp varchar(30),
@Pac_NombreFact varchar(150),
@Pac_DirecFact varchar(150),
@Pac_DistFactId varchar(10),
@Pac_RUCFact varchar(15),
@Estado bit,
@UsuCrea int,
@UsuModi int
As
Begin
	Insert Pacientes
	(Pac_HC,Pac_ApPat,Pac_ApMat,Pac_Nombres,Pac_NomComp,
	Pac_Sexo,Pac_LugNacId,Pac_FecNac,Pac_GpoSang,
	Pac_DocIdentId,Pac_DocIdentNum,Pac_ProfesionId,Pac_InstruccionId,
	Pac_Direccion,Pac_DistritoId,Pac_Telefonos,Pac_Email,
	Pac_PersResp,Pac_DirecResp,Pac_DistRespId,Pac_TelefResp,Pac_EmailResp,
	Pac_NombreFact,Pac_DirecFact,Pac_DistFactId,Pac_RUCFact,
	Estado,UsuCrea,FecCrea,UsuModi,FecModi)
	Values
	(@Pac_HC,@Pac_ApPat,@Pac_ApMat,@Pac_Nombres,@Pac_NomComp,
	@Pac_Sexo,@Pac_LugNacId,@Pac_FecNac,@Pac_GpoSang,
	@Pac_DocIdentId,@Pac_DocIdentNum,@Pac_ProfesionId,@Pac_InstruccionId,
	@Pac_Direccion,@Pac_DistritoId,@Pac_Telefonos,@Pac_Email,
	@Pac_PersResp,@Pac_DirecResp,@Pac_DistRespId,@Pac_TelefResp,@Pac_EmailResp,
	@Pac_NombreFact,@Pac_DirecFact,@Pac_DistFactId,@Pac_RUCFact,
	@Estado,@UsuCrea,GETDATE(),@UsuModi,GETDATE())
End

GO

