USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_Secuencias]    Script Date: 04/03/2015 17:45:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create Proc [dbo].[usp_Secuencias]
@SecId char(3)
As
Begin
	Select Sec_Ultimo From Secuencias
	Where Sec_Id = @SecId
End

GO

