USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_CboDistrito]    Script Date: 04/03/2015 17:38:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Proc [dbo].[usp_CboDistrito]
As
Begin
	Select	Ubi_Cod,Ubi_Nombre
	From	Ubigeo
	Where	Estado = '1' And
			Left(Ubi_Cod,5) In ('15.01', '07.01') And
			Right(Ubi_Cod,2) <> '00'
	Union 

	Select	Ubi_Cod,Ubi_Nombre
	From	Ubigeo
	Where	Ubi_Cod = '00.00.00'

	Order By Ubi_Nombre
End

GO

