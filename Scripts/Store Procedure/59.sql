USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_PatologiasUpd]    Script Date: 04/03/2015 17:44:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


Create Proc [dbo].[usp_PatologiasUpd]
(@Id smallint,
 @Nom varchar(50),
 @Est char(1),
 @FecCrea datetime,
 @UsuCrea smallint,
 @FecModi datetime,
 @UsuModi smallint)
As
Begin
	Update	Patologias
	Set		PatologiaNombre = @Nom, FecModi = GETDATE(), UsuModi = @UsuModi 
	Where	PatologiaId = @Id
End

GO

