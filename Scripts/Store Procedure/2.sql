USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_AntecPatologicosDel]    Script Date: 04/03/2015 17:36:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Proc [dbo].[usp_AntecPatologicosDel]
@FichaMedicaId int,
@PatologiaId int
As
Begin
	Delete	FM_AntecPatologicos
	Where	FichaMedicaId = @FichaMedicaId
	And		PatologiaId = @PatologiaId
End 

GO

