USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_DocIdentidadUpd]    Script Date: 04/03/2015 17:41:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Modificación
Create Proc [dbo].[usp_DocIdentidadUpd]
@Did_Id int, @Did_Nombre varchar(50),
@UsuModi int
As
Begin
	Update DocIdentidad
	Set	Did_Nombre = @Did_Nombre,
		FecModi = GETDATE(),
		UsuModi = @UsuModi
	Where Did_Id = @Did_Id
End

GO

