USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_TiposTarifaLis]    Script Date: 04/03/2015 17:45:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Listar
CREATE Proc [dbo].[usp_TiposTarifaLis]
As
Begin
	Select	TTr_Codigo As [Código], TTr_Nombre As [Tipo Tarifa],
			Case Estado When '1' Then 'Si' Else 'No' End As Activo
	From	TiposTarifa
End

GO

