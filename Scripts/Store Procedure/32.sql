USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_FichaMedicaAdd]    Script Date: 04/03/2015 17:42:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/******************************************************/
/******************************************************
                 Manejo de la Ficha Medica
*******************************************************/
--Procedure para Guardar datos por primera vez
CREATE Proc [dbo].[usp_FichaMedicaAdd]
@TipoAtencion char(1),
@HC varchar(20),
@Edad int,
@InstId int,
@ProfesionId int,
@MotivoConsulta varchar(500),
@FM_FechaUltMenst varchar(20),
@Alcohol bit,
@Tabaco bit,
@Drogas bit,
@OtrosHN bit,
@ObservHN varchar(250),
@PAMaxima int,
@PAMinima int,
@Pulso int,
@Temperatura decimal(18,1),
@Peso decimal(18,3),
@Talla decimal(18,3),
@Lucido bit,
@OTiempo bit,
@OEspacio bit,
@OPersona bit,
@EstGeneral varchar(20),
@EstNutricion varchar(20),
@EstHidratacion varchar(20),
@FM_ProxCita datetime,
@FM_Recomend varchar(250),
@FM_ObservGenerales varchar(250),
@Estado char(1)
As
Begin
	Insert FichaMedica
	(Pac_TipoAtencion,Pac_HC,Pac_Edad,GrI_Id,ProfesionId,FM_MotivoConsulta,FM_FechaUltMenst,
	FM_Alcohol,FM_Tabaco,FM_Drogas,FM_OtrosHabNoc,FM_ObservHabNoc,FM_PAMax,FM_PAMin,
	FM_Pulso,FM_Temp,FM_Peso,FM_Talla,FM_Lucido,FM_OTiempo,FM_OEspacio,
	FM_OPersona,FM_EstGeneral,FM_EstNutricion,FM_EstHidratacion,FM_ProxCita,
	FM_Recomend,FM_ObservGenerales,FM_Estado)
	Values
	(@TipoAtencion,@HC,@Edad,@InstId,@ProfesionId,@MotivoConsulta,@FM_FechaUltMenst,
	@Alcohol,@Tabaco,@Drogas,@OtrosHN,@ObservHN,@PAMaxima,@PAMinima,
	@Pulso,@Temperatura,@Peso,@Talla,@Lucido,@OTiempo,@OEspacio,
	@OPersona,@EstGeneral,@EstNutricion,@EstHidratacion,@FM_ProxCita,
	@FM_Recomend,@FM_ObservGenerales,@Estado)
End

GO

