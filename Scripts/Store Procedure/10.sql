USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_CboDocIdentidad]    Script Date: 04/03/2015 17:38:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Proc [dbo].[usp_CboDocIdentidad]
As
Begin
	Select	Did_Id, Did_Nombre
	From	DocIdentidad
	Where	Estado = '1'
End

GO

