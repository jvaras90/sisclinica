USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_PacientesA_D]    Script Date: 04/03/2015 17:43:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Anulación
Create Proc [dbo].[usp_PacientesA_D]
@Pac_HC varchar(20), @Estado bit, @UsuModi int
As
Begin
	Update Pacientes
	Set	Estado = @Estado,
		FecModi = GETDATE(),
		Usumodi = @UsuModi
	Where Pac_HC = @Pac_HC
End

GO

