USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_PacientesLis]    Script Date: 04/03/2015 17:44:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Listar
Create Proc [dbo].[usp_PacientesLis]
As
Begin
	Select	p.Pac_HC As [Historia], p.Pac_NomComp As [Paciente],
			g.GrI_Nombre[Instrucción],o.Prf_Nombre [Profesión],
			Case p.Estado When '1' Then 'Si' Else 'No' End As Activo
	From	Pacientes p, GradosInstruccion g, Profesiones o
	Where	p.Pac_InstruccionId = g.GrI_Id And
			p.Pac_ProfesionId = o.Prf_Id
End

GO

