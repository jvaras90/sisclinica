USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_FMPrescripcionDel]    Script Date: 04/03/2015 17:42:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Eliminar Medicina de la tabla Ficha FM_Prescripcion
Create Proc [dbo].[usp_FMPrescripcionDel]
@FichaMedicaId int
As
Begin
	Delete	FM_Prescripcion
	Where	FichaMedicaId = @FichaMedicaId
End

GO

