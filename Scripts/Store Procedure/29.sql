USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_EspecialidadesLis]    Script Date: 04/03/2015 17:41:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Listar
Create Proc [dbo].[usp_EspecialidadesLis]
As
Begin
	Select	Esp_Id As [Id], Esp_Nombre As [Especialidad],
			Case Estado When '1' Then 'Si' Else 'No' End As Activo
	From	Especialidades
End

GO

