USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_TiposTarifaA_D]    Script Date: 04/03/2015 17:45:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Anulación
CREATE Proc [dbo].[usp_TiposTarifaA_D]
@TTr_Codigo char(2), @Estado bit, @UsuModi int
As
Begin
	Update TiposTarifa
	Set	Estado = @Estado,
		FecModi = GETDATE(),
		Usumodi = @UsuModi
	Where TTr_Codigo = @TTr_Codigo
End

GO

