USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_PacientesUpd]    Script Date: 04/03/2015 17:44:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Modificación
CREATE Proc [dbo].[usp_PacientesUpd]
@Pac_HC varchar(20),
@Pac_ApPat varchar(50),
@Pac_ApMat varchar(50),
@Pac_Nombres varchar(50),
@Pac_NomComp varchar(150),
@Pac_Sexo char(1),
@Pac_LugNacId varchar(10),
@Pac_FecNac datetime,
@Pac_DocIdentId int,
@Pac_DocIdentNum varchar(20),
@Pac_GpoSang varchar(20),
@Pac_Direccion varchar(150),
@Pac_DistritoId varchar(10),
@Pac_Telefonos varchar(30),
@Pac_Email varchar(30),
@Pac_ProfesionId int,
@Pac_InstruccionId int,
@Pac_PersResp varchar(150),
@Pac_DirecResp varchar(150),
@Pac_DistRespId varchar(10),
@Pac_TelefResp varchar(30),
@Pac_EmailResp varchar(30),
@Pac_NombreFact varchar(150),
@Pac_DirecFact varchar(150),
@Pac_DistFactId varchar(10),
@Pac_RUCFact varchar(15),
@UsuModi int
As
Begin
	Update Pacientes
	Set	Pac_ApPat = @Pac_ApPat,
		Pac_ApMat = @Pac_ApMat,
		Pac_Nombres = @Pac_Nombres,
		Pac_NomComp = @Pac_NomComp,
		Pac_Sexo = @Pac_Sexo,
		Pac_LugNacId = @Pac_LugNacId,
		Pac_FecNac = @Pac_FecNac,
		Pac_DocIdentId = @Pac_DocIdentId,
		Pac_DocIdentNum = @Pac_DocIdentNum,
		Pac_GpoSang = @Pac_GpoSang,
		Pac_Direccion = @Pac_Direccion,
		Pac_DistritoId = @Pac_DistritoId,
		Pac_Telefonos = @Pac_Telefonos,
		Pac_Email = @Pac_Email,
		Pac_ProfesionId = @Pac_ProfesionId,
		Pac_InstruccionId = @Pac_InstruccionId,
		Pac_PersResp = @Pac_PersResp,
		Pac_DirecResp = @Pac_DirecResp,
		Pac_DistRespId = @Pac_DistRespId,
		Pac_TelefResp = @Pac_TelefResp,
		Pac_EmailResp = @Pac_EmailResp,
		Pac_NombreFact = @Pac_NombreFact,
		Pac_DirecFact = @Pac_DirecFact,
		Pac_DistFactId = @Pac_DistFactId,
		Pac_RUCFact = @Pac_RUCFact,
		FecModi = GETDATE(),
		UsuModi = @UsuModi
	Where Pac_HC = @Pac_HC
End

GO

