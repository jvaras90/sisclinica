USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_CboAlergias]    Script Date: 04/03/2015 17:37:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create Proc [dbo].[usp_CboAlergias]
As
Begin
	Select	AlergiaId,AlergiaNombre
	From	Alergias
	Where	Estado = 1
	And		TipoAlergiaId = 3
	Order By AlergiaNombre
End

GO

