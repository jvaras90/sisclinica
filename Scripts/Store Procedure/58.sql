USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_PatologiasAnu]    Script Date: 04/03/2015 17:44:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


Create Proc [dbo].[usp_PatologiasAnu]
(@Id smallint)
As
Begin
	Update	Patologias
	Set		Estado = '0'
	Where	PatologiaId = @Id
End

GO

