USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_DocIdentidadAdd]    Script Date: 04/03/2015 17:41:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/******************************************************
    Mantenimiento de Tipos de Documento de Identidad
*******************************************************/
--Creación
CREATE Proc [dbo].[usp_DocIdentidadAdd]
@Did_Nombre varchar(50),
@Estado bit, @UsuCrea int, @UsuModi int
As
Begin
	Insert DocIdentidad
	(Did_Nombre,Estado,UsuCrea,FecCrea,UsuModi,FecModi)
	Values
	(@Did_Nombre,@Estado,@UsuCrea,GETDATE(),@UsuModi,GETDATE())
End

GO

