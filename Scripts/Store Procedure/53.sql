USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_PacientesCitados]    Script Date: 04/03/2015 17:44:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Proc [dbo].[usp_PacientesCitados]
As
Begin
	Select	c.CitaHora [Hora], c.Pac_HC [Historia], p.Pac_NomComp [Paciente], 
			Case c.AntConsultaTipoAtencion
				When '1' Then 'Nuevo'
				When '2' Then 'Continuador'
				When '3' Then 'Reingreso'
			End As AtendidoComo,
			c.AntConsultaFecha [Fecha], s.Ser_Nombre [Servicio], 
			m.Med_NomComp [Atendido Por],
			c.AntConsultaDiagCod [CIE], d.DiagDescripcion [Diagnóstico],
			Case c.FM_Estado
				When '1' Then 'Cerrada'
				When '2' Then 'En Proceso'
				Else 'Por Atender'
			End As EstadoFicha
	From	Citas c, Pacientes p, Servicios s, Diagnosticos d, Medicos m
	Where	Convert(char(10),c.CitaFecha,103) = Convert(char(10),Getdate(),103)
	And		c.Pac_HC = p.Pac_HC
	And		c.Ser_Id = s.Ser_Id
	And		c.AntConsultaDiagCod = d.DiagCod
End

GO

