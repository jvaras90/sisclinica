USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_UbigeoLis]    Script Date: 04/03/2015 17:46:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Listar
CREATE Proc [dbo].[usp_UbigeoLis]
As
Begin
	Select	Ubi_Cod As [Código], Ubi_Nombre As [Ubigeo],
			Case Estado When '1' Then 'Si' Else 'No' End As Activo
	From	Ubigeo
End

GO

