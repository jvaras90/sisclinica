USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_AntecPatologicosUpd]    Script Date: 04/03/2015 17:37:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


--Actualizar Antecedentes Patológicos
Create Proc [dbo].[usp_AntecPatologicosUpd]
@FichaMedicaId int,
@Pac_HC varchar(20),
@PatologiaId int,
@PatologiaObserv varchar(250)
As
Begin
	Update	FM_AntecPatologicos
	Set		PatologiaObserv = @PatologiaObserv
	Where	FichaMedicaId = @FichaMedicaId
	And		PatologiaId = @PatologiaId
End 

GO

