USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_LaboratoriosLis]    Script Date: 04/03/2015 17:43:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Listar
Create Proc [dbo].[usp_LaboratoriosLis]
As
Begin
	Select	Lab_Id As [Id], Lab_Nombre As [Laboratorio],
			Case Estado When '1' Then 'Si' Else 'No' End As Activo
	From	Laboratorios
End

GO

