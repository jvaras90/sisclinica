USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_PrincipiosActivosLis]    Script Date: 04/03/2015 17:45:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Listar
CREATE Proc [dbo].[usp_PrincipiosActivosLis]
As
Begin
	Select	PrA_Id As [Id], PrA_Nombre As [Princ_Activo],
			Pra_Presentacion [Presentación],PrA_Tipo As [Tipo],
			Case Estado When '1' Then 'Si' Else 'No' End As Activo
	From	PrincipiosActivos
End

GO

