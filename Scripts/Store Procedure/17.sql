USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_CboUbigeo]    Script Date: 04/03/2015 17:39:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Procedure para alimentar el Combo de Ubigeo
CREATE Proc [dbo].[usp_CboUbigeo]
As
Begin
	Select	Ubi_Cod,Ubi_Nombre
	From	Ubigeo
	Where	Estado = '1'
	Order By Ubi_Nombre
End

GO

