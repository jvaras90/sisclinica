USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_CboGradosInstruccion]    Script Date: 04/03/2015 17:38:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Procedure para alimentar el Combo de Grados de Instrucción
CREATE Proc [dbo].[usp_CboGradosInstruccion]
As
Begin
	Select	GrI_Id,GrI_Nombre
	From	GradosInstruccion
	Where	Estado = '1'
	Order By GrI_Nombre
End

GO

