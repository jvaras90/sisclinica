USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_DgvSintomasAdd]    Script Date: 04/03/2015 17:39:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Procedure para alimentar el grid de Síntomas
CREATE Proc [dbo].[usp_DgvSintomasAdd]
@FichaNum int
AS
Begin
	Insert Into FM_Sintomas 
	Select @FichaNum, SintomaId, Null From Sintomas Where Estado = 1
End

GO

