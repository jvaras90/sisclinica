USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_PatologiasAdd]    Script Date: 04/03/2015 17:44:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Proc [dbo].[usp_PatologiasAdd]
(@Id smallint,
 @Nom varchar(50),
 @UsuCrea smallint,
 @UsuModi smallint)
As
Begin
	Insert Patologias
	(PatologiaId,PatologiaNombre,Estado,FecCrea,UsuCrea,FecModi,UsuModi)
	Values
	(@Id,@Nom,'1',GETDATE(),@UsuCrea,GETDATE(),@UsuModi)
End

GO

