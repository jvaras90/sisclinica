USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_EspecialidadesRec]    Script Date: 04/03/2015 17:41:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Recupera datos
Create Proc [dbo].[usp_EspecialidadesRec]
@Esp_Id int
As
Begin
	Select * From Especialidades Where Esp_Id = @Esp_Id
End

GO

