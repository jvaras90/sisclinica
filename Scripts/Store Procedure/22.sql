USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_DocIdentidadA_D]    Script Date: 04/03/2015 17:40:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Anulación
Create Proc [dbo].[usp_DocIdentidadA_D]
@Did_Id int,@Estado bit, @UsuModi int
As
Begin
	Update DocIdentidad
	Set	Estado = @Estado,
		FecModi = GETDATE(),
		Usumodi = @UsuModi
	Where Did_Id = @Did_Id
End

GO

