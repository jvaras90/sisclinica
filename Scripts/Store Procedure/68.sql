USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_ProfesionesLis]    Script Date: 04/03/2015 17:45:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Listar
CREATE Proc [dbo].[usp_ProfesionesLis]
As
Begin
	Select	Prf_Id As [Id], Prf_Nombre As [Profesión],
			Case Estado When '1' Then 'Si' Else 'No' End As Activo
	From	Profesiones
End

GO

