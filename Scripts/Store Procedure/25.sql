USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_DocIdentidadRec]    Script Date: 04/03/2015 17:41:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Recupera datos
Create Proc [dbo].[usp_DocIdentidadRec]
@Did_Id int
As
Begin
	Select * From DocIdentidad Where Did_Id = @Did_Id
End

GO

