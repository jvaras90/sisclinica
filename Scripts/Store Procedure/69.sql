USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_ProfesionesRec]    Script Date: 04/03/2015 17:45:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create Proc [dbo].[usp_ProfesionesRec]
@PrfId int
As
Begin
	Select * From Profesiones Where Prf_Id = @PrfId
End

GO

