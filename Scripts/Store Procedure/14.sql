USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_CboPatologias]    Script Date: 04/03/2015 17:39:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create Proc [dbo].[usp_CboPatologias]
As
Begin
	Select	PatologiaId,PatologiaNombre
	From	Patologias
	Where	Estado = '1' 
	Order By PatologiaNombre
End

GO

