USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_PrincipiosActivosUpd]    Script Date: 04/03/2015 17:45:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Modificación
CREATE Proc [dbo].[usp_PrincipiosActivosUpd]
@PrA_Id int, @PrA_Nombre varchar(100),@PrA_Tipo char(2),
@PrA_Presentacion varchar(50),@PrA_Dosis varchar(50),
@PrA_Interacciones varchar(500),@PrA_Contraindicaciones varchar(500),
@UsuModi int
As
Begin
	Update PrincipiosActivos
	Set	PrA_Nombre = @PrA_Nombre,
		PrA_Tipo = @PrA_Tipo,
		PrA_Presentacion = @PrA_Presentacion,
		PrA_Dosis = @Pra_Dosis,
		PrA_Interacciones = @PrA_Interacciones,
		PrA_Contraindicaciones = @PrA_Contraindicaciones,
		FecModi = GETDATE(),
		UsuModi = @UsuModi
	Where PrA_Id = @PrA_Id
End

GO

