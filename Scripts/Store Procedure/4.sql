USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_AntecPatologicosRec]    Script Date: 04/03/2015 17:37:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Proc [dbo].[usp_AntecPatologicosRec]
@FichaMedicaId int,
@PatologiaId int
As
Begin
	Select	b.PatologiaId, b.PatologiaNombre As Patología, a.PatologiaObserv As Observaciones
	From	FM_AntecPatologicos a, Patologias b
	Where	a.FichaMedicaId = @FichaMedicaId
	And		a.PatologiaId = @PatologiaId
	And		a.PatologiaId = b.PatologiaId
End 

GO

