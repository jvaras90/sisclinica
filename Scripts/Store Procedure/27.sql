USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_EspecialidadesA_D]    Script Date: 04/03/2015 17:41:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Anulación
Create Proc [dbo].[usp_EspecialidadesA_D]
@Esp_Id int,@Estado bit, @UsuModi int
As
Begin
	Update Especialidades
	Set	Estado = @Estado,
		FecModi = GETDATE(),
		Usumodi = @UsuModi
	Where Esp_Id = @Esp_Id
End

GO

