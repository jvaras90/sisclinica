USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_LaboratoriosUpd]    Script Date: 04/03/2015 17:43:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Modificación
Create Proc [dbo].[usp_LaboratoriosUpd]
@Lab_Id int, @Lab_Nombre varchar(50),
@UsuModi int
As
Begin
	Update Laboratorios
	Set	Lab_Nombre = @Lab_Nombre,
		FecModi = GETDATE(),
		UsuModi = @UsuModi
	Where Lab_Id = @Lab_Id
End

GO

