USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_EspecialidadesAdd]    Script Date: 04/03/2015 17:41:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create Proc [dbo].[usp_EspecialidadesAdd]
@Esp_Nombre varchar(50),
@Estado bit, @UsuCrea int, @UsuModi int
As
Begin
	Insert Especialidades
	(Esp_Nombre,Estado,UsuCrea,FecCrea,UsuModi,FecModi)
	Values
	(@Esp_Nombre,@Estado,@UsuCrea,GETDATE(),@UsuModi,GETDATE())
End

GO

