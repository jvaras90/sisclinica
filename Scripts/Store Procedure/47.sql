USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_LaboratoriosAdd]    Script Date: 04/03/2015 17:43:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create Proc [dbo].[usp_LaboratoriosAdd]
@Lab_Nombre varchar(50),
@Estado bit, @UsuCrea int, @UsuModi int
As
Begin
	Insert Laboratorios
	(Lab_Nombre,Estado,UsuCrea,FecCrea,UsuModi,FecModi)
	Values
	(@Lab_Nombre,@Estado,@UsuCrea,GETDATE(),@UsuModi,GETDATE())
End

GO

