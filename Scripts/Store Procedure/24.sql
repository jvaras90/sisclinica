USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_DocIdentidadLis]    Script Date: 04/03/2015 17:41:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Listar
Create Proc [dbo].[usp_DocIdentidadLis]
As
Begin
	Select	Did_Id As [Código], Did_Nombre As [Documento Identidad],
			Case Estado When '1' Then 'Si' Else 'No' End As Activo
	From	DocIdentidad
End

GO

