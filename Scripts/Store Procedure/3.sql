USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_AntecPatologicosLis]    Script Date: 04/03/2015 17:37:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Proc [dbo].[usp_AntecPatologicosLis]
@Pac_HC varchar(20)
As
Begin
	Select	b.PatologiaId, b.PatologiaNombre As Patología, a.PatologiaObserv As Observaciones
	From	FM_AntecPatologicos a, Patologias b
	Where	a.Pac_HC = @Pac_HC
	And		a.PatologiaId = b.PatologiaId
End 

GO

