USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_CboLugNacimiento]    Script Date: 04/03/2015 17:39:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Procedure para alimentar el Combo de Lugar de Nacimiento
CREATE Proc [dbo].[usp_CboLugNacimiento]
As
Begin
	Select	Ubi_Cod,Ubi_Nombre
	From	Ubigeo
	Where	Estado = '1' And
			RIGHT(Ubi_Cod,5) = '00.00' Or
			Ubi_Cod = '99.99.99'
	Order By Ubi_Nombre
End

GO

