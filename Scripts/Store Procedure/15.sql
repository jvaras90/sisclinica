USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_CboPrincActivosFA]    Script Date: 04/03/2015 17:39:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create Proc [dbo].[usp_CboPrincActivosFA]
As
Begin
	Select	PrA_Id, PrA_Nombre
	From	PrincipiosActivos
	Where	PrA_Tipo = 'FA'
	And		Estado = 1
End

GO

