USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_FichaMedicaUpd]    Script Date: 04/03/2015 17:42:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


--Procedure para Guardar datos cuando esta en "Proceso" y al dar click
--en el botón Cerrar Ficha
CREATE Proc [dbo].[usp_FichaMedicaUpd]
@FichaMedicaId int,
@TipoAtencion char(1),
@HC varchar(20),
@Edad int,
@InstId int,
@ProfesionId int,
@MotivoConsulta varchar(500),
@FM_FechaUltMenst varchar(20),
@Alcohol bit,
@Tabaco bit,
@Drogas bit,
@OtrosHN bit,
@ObservHN varchar(250),
@PAMaxima int,
@PAMinima int,
@Pulso int,
@Temperatura decimal(18,1),
@Peso decimal(18,3),
@Talla decimal(18,3),
@Lucido bit,
@OTiempo bit,
@OEspacio bit,
@OPersona bit,
@EstGeneral varchar(20),
@EstNutricion varchar(20),
@EstHidratacion varchar(20),
@FM_ProxCita datetime,
@FM_Recomend varchar(250),
@FM_ObservGenerales varchar(250),
@Estado char(1)
As
Begin
	Update FichaMedica
	Set	Pac_TipoAtencion = @TipoAtencion,
		Pac_HC = @HC,
		Pac_Edad = @Edad,
		GrI_Id = @InstId,
		ProfesionId = @ProfesionId,
		FM_MotivoConsulta = @MotivoConsulta,
		FM_FechaUltMenst = @FM_FechaUltMenst,
		FM_Alcohol = @Alcohol,
		FM_Tabaco = @Tabaco,
		FM_Drogas = @Drogas,
		FM_OtrosHabNoc = @OtrosHN,
		FM_ObservHabNoc = @ObservHN,
		FM_PAMax = @PAMaxima,
		FM_PAMin = @PAMinima,
		FM_Pulso = @Pulso,
		FM_Temp = @Temperatura,
		FM_Peso = @Peso,
		FM_Talla = @Talla,
		FM_Lucido = @Lucido,
		FM_OTiempo = @OTiempo,
		FM_OEspacio = @OEspacio,
		FM_OPersona = @OPersona,
		FM_EstGeneral = @EstGeneral,
		FM_EstNutricion = @EstNutricion,
		FM_EstHidratacion = @EstHidratacion,
		FM_ProxCita = @FM_ProxCita,
		FM_Recomend = @FM_Recomend,
		FM_ObservGenerales = @FM_ObservGenerales,
		FM_Estado = @Estado
	Where FichaMedicaId = @FichaMedicaId
End

GO

