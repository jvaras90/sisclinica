USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_CboProfesiones]    Script Date: 04/03/2015 17:39:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--Procedure para alimentar el Combo de Profesiones
CREATE Proc [dbo].[usp_CboProfesiones]
As
Begin
	Select	Prf_Id, Prf_Nombre
	From	Profesiones
	Where	Estado = '1'
	Order By Prf_Nombre
End

GO

