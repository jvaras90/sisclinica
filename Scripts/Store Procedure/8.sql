USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_CboDiagnosticos]    Script Date: 04/03/2015 17:37:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create Proc [dbo].[usp_CboDiagnosticos]
As
Begin
	Select	DiagCod,DiagDescripcion
	From	Diagnosticos
	Where	Estado = 1
	Order By DiagDescripcion
End

GO

