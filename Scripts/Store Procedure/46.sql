USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_LaboratoriosA_D]    Script Date: 04/03/2015 17:43:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Anulación
Create Proc [dbo].[usp_LaboratoriosA_D]
@Lab_Id int, @Estado bit, @UsuModi int
As
Begin
	Update Laboratorios
	Set	Estado = @Estado,
		FecModi = GETDATE(),
		Usumodi = @UsuModi
	Where Lab_Id = @Lab_Id
End

GO

