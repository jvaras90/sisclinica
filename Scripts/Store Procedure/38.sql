USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_FMPrescripcionUpd]    Script Date: 04/03/2015 17:42:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Proc [dbo].[usp_FMPrescripcionUpd]
@FichaMedicaId int,
@RpIndicaciones varchar(250)
As
Begin
	Update	FM_Prescripcion
	Set		RpIndicaciones = @RpIndicaciones
	Where	FichaMedicaId = @FichaMedicaId
End

GO

