USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_TiposTarifaAdd]    Script Date: 04/03/2015 17:45:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create Proc [dbo].[usp_TiposTarifaAdd]
@TTr_Codigo char(2), @TTr_Nombre varchar(50),
@Estado bit, @UsuCrea int, @UsuModi int
As
Begin
	Insert TiposTarifa
	(TTr_Codigo, Ttr_Nombre,Estado,UsuCrea,FecCrea,UsuModi,FecModi)
	Values
	(@TTr_Codigo, @TTr_Nombre,@Estado,@UsuCrea,GETDATE(),@UsuModi,GETDATE())
End

GO

