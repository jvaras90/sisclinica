USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_FMedicaCabecera]    Script Date: 04/03/2015 17:42:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Proc [dbo].[usp_FMedicaCabecera]
@Pac_HC varchar(10)
As
Begin
	Select	p.Pac_HC, p.Pac_NomComp, d.Did_Nombre + ' ' + p.Pac_DocIdentNum As DocIdent,
			i.GrI_Nombre, o.Prf_Nombre,p.Pac_GpoSang,
			Case p.Pac_Sexo
				When 'F' Then 'Femenino'
				Else 'Masculino'
			End As Pac_Sexo,
			YEAR(getdate()) - YEAR(p.Pac_FecNac) As Pac_Edad,
			(Select Ubi_Nombre From Ubigeo 
			 Where Ubi_Cod = p.Pac_LugNacId) As Pac_LugNacim,
			(Select Ubi_Nombre From Ubigeo 
			 Where Ubi_Cod = p.Pac_DistritoId) As Pac_Proced
	From	Pacientes p, DocIdentidad d, GradosInstruccion i, Profesiones o
	Where	p.Pac_HC = @Pac_HC
	And		p.Pac_DocIdentId = d.Did_Id
	And		p.Pac_InstruccionId = i.GrI_Id
	And		p.Pac_ProfesionId = o.Prf_Id
End

GO

