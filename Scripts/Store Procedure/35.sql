USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_FMPrescripcionAdd]    Script Date: 04/03/2015 17:42:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create Proc [dbo].[usp_FMPrescripcionAdd]
@FichaMedicaId int,
@PrA_Id int,
@RpNomComercial varchar(150),
@RpIndicaciones varchar(250)
As
Begin
	Insert FM_Prescripcion
	(FichaMedicaId,PrA_Id,RpNomComercial,RpIndicaciones)
	Values
	(@FichaMedicaId,@PrA_Id,@RpNomComercial,@RpIndicaciones)
End

GO

