USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_UbigeoAdd]    Script Date: 04/03/2015 17:46:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/******************************************************
                Mantenimiento de Ubigeos
*******************************************************/
--Creación
CREATE Proc [dbo].[usp_UbigeoAdd]
@Ubi_Cod varchar(10), @Ubi_Nombre varchar(50),
@Estado bit, @UsuCrea int, @UsuModi int
As
Begin
	Insert Ubigeo
	(Ubi_Cod, Ubi_Nombre,Estado,UsuCrea,FecCrea,UsuModi,FecModi)
	Values
	(@Ubi_Cod, @Ubi_Nombre,@Estado,@UsuCrea,GETDATE(),@UsuModi,GETDATE())
End

GO

