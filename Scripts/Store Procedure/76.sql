USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_TiposTarifaUpd]    Script Date: 04/03/2015 17:46:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Modificación
CREATE Proc [dbo].[usp_TiposTarifaUpd]
@TTr_Codigo char(2), @TTr_Nombre varchar(50),
@UsuModi int
As
Begin
	Update TiposTarifa
	Set	@TTr_Nombre = @TTr_Nombre,
		FecModi = GETDATE(),
		UsuModi = @UsuModi
	Where TTr_Codigo = @TTr_Codigo
End

GO

