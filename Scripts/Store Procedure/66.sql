USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_ProfesionesAdd]    Script Date: 04/03/2015 17:45:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/*************************************************************************/

/******************************************************
               Mantenimiento de Profesiones
*******************************************************/
--Creación
CREATE Proc [dbo].[usp_ProfesionesAdd]
@Prf_Id int, @Prf_Nombre varchar(50),
@Estado bit, @UsuCrea int, @UsuMod int
As
Begin
	Insert Profesiones
	(Prf_Id, Prf_Nombre,Estado,UsuCrea,FecCrea,UsuMod,FecMod)
	Values
	(@Prf_Id, @Prf_Nombre,@Estado,@UsuCrea,GETDATE(),@UsuMod,GETDATE())
End

GO

