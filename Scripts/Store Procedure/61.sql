USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_PrincipiosActivosAdd]    Script Date: 04/03/2015 17:44:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/******************************************************
         Mantenimiento de Principios Activos
*******************************************************/
--Creación
CREATE Proc [dbo].[usp_PrincipiosActivosAdd]
@PrA_Nombre varchar(100), @PrA_Tipo char(2),
@PrA_Presentacion varchar(50),@PrA_Dosis varchar(50),
@PrA_Interacciones varchar(500),@PrA_Contraindicaciones varchar(500),
@Estado bit, @UsuCrea int, @UsuModi int
As
Begin
	Insert PrincipiosActivos
	(PrA_Nombre,PrA_Tipo,PrA_Presentacion,PrA_Dosis,
	 PrA_Interacciones, PrA_Contraindicaciones,
	 Estado,UsuCrea,FecCrea,UsuModi,FecModi)
	Values
	(@PrA_Nombre,@PrA_Tipo,@PrA_Presentacion,@PrA_Dosis,
	 @PrA_Interacciones, @PrA_Contraindicaciones,
	 @Estado,@UsuCrea,GETDATE(),@UsuModi,GETDATE())
End

GO

