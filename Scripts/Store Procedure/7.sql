USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_CboCirugias]    Script Date: 04/03/2015 17:37:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create Proc [dbo].[usp_CboCirugias]
As
Begin
	Select	Qui_Id,Qui_Nombre
	From	Cirugias
	Where	Estado = 1
	Order By Qui_Nombre
End
GO

