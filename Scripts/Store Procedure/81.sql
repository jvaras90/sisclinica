USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_UbigeoUpd]    Script Date: 04/03/2015 17:46:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Modificación
CREATE Proc [dbo].[usp_UbigeoUpd]
@Ubi_Cod varchar(10), @Ubi_Nombre varchar(50),
@UsuModi int
As
Begin
	Update Ubigeo
	Set	Ubi_Nombre = @Ubi_Nombre,
		FecModi = GETDATE(),
		UsuModi = @UsuModi
	Where Ubi_Cod = @Ubi_Cod
End

GO

