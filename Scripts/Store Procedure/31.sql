USE [DermaBelle]
GO

/****** Object:  StoredProcedure [dbo].[usp_EspecialidadesUpd]    Script Date: 04/03/2015 17:42:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Modificación
Create Proc [dbo].[usp_EspecialidadesUpd]
@Esp_Id int, @Esp_Nombre varchar(50),
@UsuModi int
As
Begin
	Update Especialidades
	Set	Esp_Nombre = @Esp_Nombre,
		FecModi = GETDATE(),
		UsuModi = @UsuModi
	Where Esp_Id = @Esp_Id
End

GO

