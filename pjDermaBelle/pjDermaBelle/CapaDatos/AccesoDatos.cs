﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace pjDermaBelle.CapaDatos
{
    public class AccesoDatos
    {
        public int InsertaProfesiones(string Prf_Nombre, string Estado, int UsuCrea, int UsuModi)
        {
            SqlCommand _comando = MetodosDatos.CrearComandoProc();
            _comando.Parameters.AddWithValue("@prf_nombre", Prf_Nombre);
            _comando.Parameters.AddWithValue("@estado", Estado);
            _comando.Parameters.AddWithValue("@usucrea", UsuCrea);
            _comando.Parameters.AddWithValue("@usumodi", UsuModi);
            return MetodosDatos.EjecutarComandoInsert(_comando);
        }

        public static DataTable ListarProfesiones()
        {
            SqlCommand _comando = MetodosDatos.CrearComando();
            _comando.CommandText = "SELECT * FROM Profesiones";
            return MetodosDatos.EjecutarComandoSelect(_comando);
        }
    }
}
