﻿namespace pjDermaBelle
{
    partial class frmMenuPrincipal
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.administrativoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.maestrosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.profesionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gradosDeInstrucciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ubigeoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.documentosDeIdentidadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pacientesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.filiaciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.controlToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.profesionalesDeLaSaludToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.especialidadesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tarifarioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.serviciosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tipoDeTarifaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.productosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tablasMaestrasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.principiosActivosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.laboratoriosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.citasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.definicionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultoriosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.serviciosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.horariiosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.citasToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.reservaDeCitasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cajaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.operacionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.arqueoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.definicionesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.documentosDePagoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tarjetasCréditoDébitoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.reportesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pacientesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.citadosVsAtendidosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pacientesAtendidosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.citadosVsAtendidosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.citasReservadasVsConfirmadasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cajaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ingresosPorDíaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ingresosPorServicioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ingresosPorTipoDePagoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.arqueosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asistencialToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tomaDeSignosVitalesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tomaDeSignosVitalesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.actoMédicoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.actoMédicoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.historialMédicoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.reportesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.síntomasMásComunesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.diagnósticosMásComunesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.medicamentosMásPrescritosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tratamientosMásEmpleadosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.utilitariosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inventarioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.comprasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.movimientosDeAlmacénToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.kardexToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inventarioToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.formulariosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.actualizaciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.inventarioValorizadoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.utilitariosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.mantenimientoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.secuenciasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usuariosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.perfilesDeUsuarioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usuariosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ayudaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ayudaEnLíneaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.acercaDeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.administrativoToolStripMenuItem,
            this.citasToolStripMenuItem,
            this.asistencialToolStripMenuItem,
            this.utilitariosToolStripMenuItem,
            this.utilitariosToolStripMenuItem1,
            this.salirToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(477, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // administrativoToolStripMenuItem
            // 
            this.administrativoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.maestrosToolStripMenuItem,
            this.pacientesToolStripMenuItem,
            this.profesionalesDeLaSaludToolStripMenuItem,
            this.toolStripSeparator1,
            this.tarifarioToolStripMenuItem});
            this.administrativoToolStripMenuItem.Name = "administrativoToolStripMenuItem";
            this.administrativoToolStripMenuItem.Size = new System.Drawing.Size(97, 20);
            this.administrativoToolStripMenuItem.Text = "Administrativo";
            // 
            // maestrosToolStripMenuItem
            // 
            this.maestrosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.profesionesToolStripMenuItem,
            this.gradosDeInstrucciónToolStripMenuItem,
            this.ubigeoToolStripMenuItem,
            this.documentosDeIdentidadToolStripMenuItem});
            this.maestrosToolStripMenuItem.Name = "maestrosToolStripMenuItem";
            this.maestrosToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.maestrosToolStripMenuItem.Text = "Maestros";
            // 
            // profesionesToolStripMenuItem
            // 
            this.profesionesToolStripMenuItem.Name = "profesionesToolStripMenuItem";
            this.profesionesToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.profesionesToolStripMenuItem.Text = "Profesiones";
            this.profesionesToolStripMenuItem.Click += new System.EventHandler(this.profesionesToolStripMenuItem_Click);
            // 
            // gradosDeInstrucciónToolStripMenuItem
            // 
            this.gradosDeInstrucciónToolStripMenuItem.Name = "gradosDeInstrucciónToolStripMenuItem";
            this.gradosDeInstrucciónToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.gradosDeInstrucciónToolStripMenuItem.Text = "Grados de Instrucción";
            this.gradosDeInstrucciónToolStripMenuItem.Click += new System.EventHandler(this.gradosDeInstrucciónToolStripMenuItem_Click);
            // 
            // ubigeoToolStripMenuItem
            // 
            this.ubigeoToolStripMenuItem.Name = "ubigeoToolStripMenuItem";
            this.ubigeoToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.ubigeoToolStripMenuItem.Text = "Ubigeo";
            this.ubigeoToolStripMenuItem.Click += new System.EventHandler(this.ubigeoToolStripMenuItem_Click);
            // 
            // documentosDeIdentidadToolStripMenuItem
            // 
            this.documentosDeIdentidadToolStripMenuItem.Name = "documentosDeIdentidadToolStripMenuItem";
            this.documentosDeIdentidadToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.documentosDeIdentidadToolStripMenuItem.Text = "Documentos de Identidad";
            this.documentosDeIdentidadToolStripMenuItem.Click += new System.EventHandler(this.documentosDeIdentidadToolStripMenuItem_Click);
            // 
            // pacientesToolStripMenuItem
            // 
            this.pacientesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.filiaciónToolStripMenuItem,
            this.controlToolStripMenuItem});
            this.pacientesToolStripMenuItem.Name = "pacientesToolStripMenuItem";
            this.pacientesToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.pacientesToolStripMenuItem.Text = "Pacientes";
            // 
            // filiaciónToolStripMenuItem
            // 
            this.filiaciónToolStripMenuItem.Name = "filiaciónToolStripMenuItem";
            this.filiaciónToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.filiaciónToolStripMenuItem.Text = "Filiación";
            this.filiaciónToolStripMenuItem.Click += new System.EventHandler(this.filiaciónToolStripMenuItem_Click);
            // 
            // controlToolStripMenuItem
            // 
            this.controlToolStripMenuItem.Name = "controlToolStripMenuItem";
            this.controlToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.controlToolStripMenuItem.Text = "Control de Asistencia";
            this.controlToolStripMenuItem.Click += new System.EventHandler(this.controlToolStripMenuItem_Click);
            // 
            // profesionalesDeLaSaludToolStripMenuItem
            // 
            this.profesionalesDeLaSaludToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registroToolStripMenuItem,
            this.especialidadesToolStripMenuItem});
            this.profesionalesDeLaSaludToolStripMenuItem.Name = "profesionalesDeLaSaludToolStripMenuItem";
            this.profesionalesDeLaSaludToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.profesionalesDeLaSaludToolStripMenuItem.Text = "Profesionales de la Salud";
            // 
            // registroToolStripMenuItem
            // 
            this.registroToolStripMenuItem.Name = "registroToolStripMenuItem";
            this.registroToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.registroToolStripMenuItem.Text = "Registro";
            this.registroToolStripMenuItem.Click += new System.EventHandler(this.registroToolStripMenuItem_Click);
            // 
            // especialidadesToolStripMenuItem
            // 
            this.especialidadesToolStripMenuItem.Name = "especialidadesToolStripMenuItem";
            this.especialidadesToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.especialidadesToolStripMenuItem.Text = "Especialidades";
            this.especialidadesToolStripMenuItem.Click += new System.EventHandler(this.especialidadesToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(201, 6);
            // 
            // tarifarioToolStripMenuItem
            // 
            this.tarifarioToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.serviciosToolStripMenuItem,
            this.tipoDeTarifaToolStripMenuItem,
            this.toolStripSeparator8,
            this.productosToolStripMenuItem,
            this.tablasMaestrasToolStripMenuItem});
            this.tarifarioToolStripMenuItem.Name = "tarifarioToolStripMenuItem";
            this.tarifarioToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.tarifarioToolStripMenuItem.Text = "Tarifario";
            // 
            // serviciosToolStripMenuItem
            // 
            this.serviciosToolStripMenuItem.Name = "serviciosToolStripMenuItem";
            this.serviciosToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.serviciosToolStripMenuItem.Text = "Tarifario";
            this.serviciosToolStripMenuItem.Click += new System.EventHandler(this.serviciosToolStripMenuItem_Click);
            // 
            // tipoDeTarifaToolStripMenuItem
            // 
            this.tipoDeTarifaToolStripMenuItem.Name = "tipoDeTarifaToolStripMenuItem";
            this.tipoDeTarifaToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.tipoDeTarifaToolStripMenuItem.Text = "Tipos de Tarifa";
            this.tipoDeTarifaToolStripMenuItem.Click += new System.EventHandler(this.tipoDeTarifaToolStripMenuItem_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(149, 6);
            // 
            // productosToolStripMenuItem
            // 
            this.productosToolStripMenuItem.Name = "productosToolStripMenuItem";
            this.productosToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.productosToolStripMenuItem.Text = "Productos";
            // 
            // tablasMaestrasToolStripMenuItem
            // 
            this.tablasMaestrasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.principiosActivosToolStripMenuItem,
            this.laboratoriosToolStripMenuItem});
            this.tablasMaestrasToolStripMenuItem.Name = "tablasMaestrasToolStripMenuItem";
            this.tablasMaestrasToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.tablasMaestrasToolStripMenuItem.Text = "Definiciones";
            // 
            // principiosActivosToolStripMenuItem
            // 
            this.principiosActivosToolStripMenuItem.Name = "principiosActivosToolStripMenuItem";
            this.principiosActivosToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.principiosActivosToolStripMenuItem.Text = "Principios Activos";
            this.principiosActivosToolStripMenuItem.Click += new System.EventHandler(this.principiosActivosToolStripMenuItem_Click);
            // 
            // laboratoriosToolStripMenuItem
            // 
            this.laboratoriosToolStripMenuItem.Name = "laboratoriosToolStripMenuItem";
            this.laboratoriosToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.laboratoriosToolStripMenuItem.Text = "Laboratorios";
            this.laboratoriosToolStripMenuItem.Click += new System.EventHandler(this.laboratoriosToolStripMenuItem_Click);
            // 
            // citasToolStripMenuItem
            // 
            this.citasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.definicionesToolStripMenuItem,
            this.citasToolStripMenuItem1,
            this.cajaToolStripMenuItem,
            this.toolStripSeparator2,
            this.reportesToolStripMenuItem});
            this.citasToolStripMenuItem.Name = "citasToolStripMenuItem";
            this.citasToolStripMenuItem.Size = new System.Drawing.Size(45, 20);
            this.citasToolStripMenuItem.Text = "Citas";
            // 
            // definicionesToolStripMenuItem
            // 
            this.definicionesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.consultoriosToolStripMenuItem,
            this.serviciosToolStripMenuItem1,
            this.horariiosToolStripMenuItem});
            this.definicionesToolStripMenuItem.Name = "definicionesToolStripMenuItem";
            this.definicionesToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.definicionesToolStripMenuItem.Text = "Definiciones";
            // 
            // consultoriosToolStripMenuItem
            // 
            this.consultoriosToolStripMenuItem.Name = "consultoriosToolStripMenuItem";
            this.consultoriosToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.consultoriosToolStripMenuItem.Text = "Consultorios";
            // 
            // serviciosToolStripMenuItem1
            // 
            this.serviciosToolStripMenuItem1.Name = "serviciosToolStripMenuItem1";
            this.serviciosToolStripMenuItem1.Size = new System.Drawing.Size(141, 22);
            this.serviciosToolStripMenuItem1.Text = "Servicios";
            // 
            // horariiosToolStripMenuItem
            // 
            this.horariiosToolStripMenuItem.Name = "horariiosToolStripMenuItem";
            this.horariiosToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.horariiosToolStripMenuItem.Text = "Horarios";
            // 
            // citasToolStripMenuItem1
            // 
            this.citasToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.reservaDeCitasToolStripMenuItem});
            this.citasToolStripMenuItem1.Name = "citasToolStripMenuItem1";
            this.citasToolStripMenuItem1.Size = new System.Drawing.Size(139, 22);
            this.citasToolStripMenuItem1.Text = "Citas";
            // 
            // reservaDeCitasToolStripMenuItem
            // 
            this.reservaDeCitasToolStripMenuItem.Name = "reservaDeCitasToolStripMenuItem";
            this.reservaDeCitasToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.reservaDeCitasToolStripMenuItem.Text = "Reserva de Citas";
            // 
            // cajaToolStripMenuItem
            // 
            this.cajaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.operacionesToolStripMenuItem,
            this.arqueoToolStripMenuItem,
            this.toolStripSeparator3,
            this.definicionesToolStripMenuItem1});
            this.cajaToolStripMenuItem.Name = "cajaToolStripMenuItem";
            this.cajaToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.cajaToolStripMenuItem.Text = "Caja";
            // 
            // operacionesToolStripMenuItem
            // 
            this.operacionesToolStripMenuItem.Name = "operacionesToolStripMenuItem";
            this.operacionesToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.operacionesToolStripMenuItem.Text = "Operaciones";
            // 
            // arqueoToolStripMenuItem
            // 
            this.arqueoToolStripMenuItem.Name = "arqueoToolStripMenuItem";
            this.arqueoToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.arqueoToolStripMenuItem.Text = "Arqueo";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(137, 6);
            // 
            // definicionesToolStripMenuItem1
            // 
            this.definicionesToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.documentosDePagoToolStripMenuItem,
            this.tarjetasCréditoDébitoToolStripMenuItem});
            this.definicionesToolStripMenuItem1.Name = "definicionesToolStripMenuItem1";
            this.definicionesToolStripMenuItem1.Size = new System.Drawing.Size(140, 22);
            this.definicionesToolStripMenuItem1.Text = "Definiciones";
            // 
            // documentosDePagoToolStripMenuItem
            // 
            this.documentosDePagoToolStripMenuItem.Name = "documentosDePagoToolStripMenuItem";
            this.documentosDePagoToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.documentosDePagoToolStripMenuItem.Text = "Documentos de Pago";
            // 
            // tarjetasCréditoDébitoToolStripMenuItem
            // 
            this.tarjetasCréditoDébitoToolStripMenuItem.Name = "tarjetasCréditoDébitoToolStripMenuItem";
            this.tarjetasCréditoDébitoToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.tarjetasCréditoDébitoToolStripMenuItem.Text = "Tarjetas Crédito / Débito";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(136, 6);
            // 
            // reportesToolStripMenuItem
            // 
            this.reportesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pacientesToolStripMenuItem1,
            this.cajaToolStripMenuItem1});
            this.reportesToolStripMenuItem.Name = "reportesToolStripMenuItem";
            this.reportesToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.reportesToolStripMenuItem.Text = "Reportes";
            // 
            // pacientesToolStripMenuItem1
            // 
            this.pacientesToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.citadosVsAtendidosToolStripMenuItem,
            this.pacientesAtendidosToolStripMenuItem,
            this.citadosVsAtendidosToolStripMenuItem1,
            this.citasReservadasVsConfirmadasToolStripMenuItem});
            this.pacientesToolStripMenuItem1.Name = "pacientesToolStripMenuItem1";
            this.pacientesToolStripMenuItem1.Size = new System.Drawing.Size(124, 22);
            this.pacientesToolStripMenuItem1.Text = "Pacientes";
            // 
            // citadosVsAtendidosToolStripMenuItem
            // 
            this.citadosVsAtendidosToolStripMenuItem.Name = "citadosVsAtendidosToolStripMenuItem";
            this.citadosVsAtendidosToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.citadosVsAtendidosToolStripMenuItem.Text = "Pacientes Citados";
            // 
            // pacientesAtendidosToolStripMenuItem
            // 
            this.pacientesAtendidosToolStripMenuItem.Name = "pacientesAtendidosToolStripMenuItem";
            this.pacientesAtendidosToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.pacientesAtendidosToolStripMenuItem.Text = "Pacientes Atendidos";
            // 
            // citadosVsAtendidosToolStripMenuItem1
            // 
            this.citadosVsAtendidosToolStripMenuItem1.Name = "citadosVsAtendidosToolStripMenuItem1";
            this.citadosVsAtendidosToolStripMenuItem1.Size = new System.Drawing.Size(250, 22);
            this.citadosVsAtendidosToolStripMenuItem1.Text = "Citados Vs. Atendidos";
            // 
            // citasReservadasVsConfirmadasToolStripMenuItem
            // 
            this.citasReservadasVsConfirmadasToolStripMenuItem.Name = "citasReservadasVsConfirmadasToolStripMenuItem";
            this.citasReservadasVsConfirmadasToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.citasReservadasVsConfirmadasToolStripMenuItem.Text = "Citas Reservadas Vs. Confirmadas";
            // 
            // cajaToolStripMenuItem1
            // 
            this.cajaToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ingresosPorDíaToolStripMenuItem,
            this.ingresosPorServicioToolStripMenuItem,
            this.ingresosPorTipoDePagoToolStripMenuItem,
            this.toolStripSeparator4,
            this.arqueosToolStripMenuItem});
            this.cajaToolStripMenuItem1.Name = "cajaToolStripMenuItem1";
            this.cajaToolStripMenuItem1.Size = new System.Drawing.Size(124, 22);
            this.cajaToolStripMenuItem1.Text = "Caja";
            // 
            // ingresosPorDíaToolStripMenuItem
            // 
            this.ingresosPorDíaToolStripMenuItem.Name = "ingresosPorDíaToolStripMenuItem";
            this.ingresosPorDíaToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.ingresosPorDíaToolStripMenuItem.Text = "Ingresos por Día";
            // 
            // ingresosPorServicioToolStripMenuItem
            // 
            this.ingresosPorServicioToolStripMenuItem.Name = "ingresosPorServicioToolStripMenuItem";
            this.ingresosPorServicioToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.ingresosPorServicioToolStripMenuItem.Text = "Ingresos por Servicio";
            // 
            // ingresosPorTipoDePagoToolStripMenuItem
            // 
            this.ingresosPorTipoDePagoToolStripMenuItem.Name = "ingresosPorTipoDePagoToolStripMenuItem";
            this.ingresosPorTipoDePagoToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.ingresosPorTipoDePagoToolStripMenuItem.Text = "Ingresos por Tipo de Pago";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(209, 6);
            // 
            // arqueosToolStripMenuItem
            // 
            this.arqueosToolStripMenuItem.Name = "arqueosToolStripMenuItem";
            this.arqueosToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.arqueosToolStripMenuItem.Text = "Arqueos";
            // 
            // asistencialToolStripMenuItem
            // 
            this.asistencialToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tomaDeSignosVitalesToolStripMenuItem,
            this.actoMédicoToolStripMenuItem,
            this.toolStripSeparator5,
            this.reportesToolStripMenuItem1});
            this.asistencialToolStripMenuItem.Name = "asistencialToolStripMenuItem";
            this.asistencialToolStripMenuItem.Size = new System.Drawing.Size(75, 20);
            this.asistencialToolStripMenuItem.Text = "Asistencial";
            // 
            // tomaDeSignosVitalesToolStripMenuItem
            // 
            this.tomaDeSignosVitalesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tomaDeSignosVitalesToolStripMenuItem1,
            this.actoMédicoToolStripMenuItem1});
            this.tomaDeSignosVitalesToolStripMenuItem.Name = "tomaDeSignosVitalesToolStripMenuItem";
            this.tomaDeSignosVitalesToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.tomaDeSignosVitalesToolStripMenuItem.Text = "Asistencial";
            // 
            // tomaDeSignosVitalesToolStripMenuItem1
            // 
            this.tomaDeSignosVitalesToolStripMenuItem1.Name = "tomaDeSignosVitalesToolStripMenuItem1";
            this.tomaDeSignosVitalesToolStripMenuItem1.Size = new System.Drawing.Size(196, 22);
            this.tomaDeSignosVitalesToolStripMenuItem1.Text = "Toma de Signos Vitales";
            // 
            // actoMédicoToolStripMenuItem1
            // 
            this.actoMédicoToolStripMenuItem1.Name = "actoMédicoToolStripMenuItem1";
            this.actoMédicoToolStripMenuItem1.Size = new System.Drawing.Size(196, 22);
            this.actoMédicoToolStripMenuItem1.Text = "Acto Médico";
            this.actoMédicoToolStripMenuItem1.Click += new System.EventHandler(this.actoMédicoToolStripMenuItem1_Click);
            // 
            // actoMédicoToolStripMenuItem
            // 
            this.actoMédicoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.historialMédicoToolStripMenuItem});
            this.actoMédicoToolStripMenuItem.Name = "actoMédicoToolStripMenuItem";
            this.actoMédicoToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.actoMédicoToolStripMenuItem.Text = "Consulta";
            // 
            // historialMédicoToolStripMenuItem
            // 
            this.historialMédicoToolStripMenuItem.Name = "historialMédicoToolStripMenuItem";
            this.historialMédicoToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.historialMédicoToolStripMenuItem.Text = "Historial Médico";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(149, 6);
            // 
            // reportesToolStripMenuItem1
            // 
            this.reportesToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.síntomasMásComunesToolStripMenuItem,
            this.diagnósticosMásComunesToolStripMenuItem,
            this.medicamentosMásPrescritosToolStripMenuItem,
            this.tratamientosMásEmpleadosToolStripMenuItem});
            this.reportesToolStripMenuItem1.Name = "reportesToolStripMenuItem1";
            this.reportesToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.reportesToolStripMenuItem1.Text = "Reportes";
            // 
            // síntomasMásComunesToolStripMenuItem
            // 
            this.síntomasMásComunesToolStripMenuItem.Name = "síntomasMásComunesToolStripMenuItem";
            this.síntomasMásComunesToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.síntomasMásComunesToolStripMenuItem.Text = "Síntomas más comunes";
            // 
            // diagnósticosMásComunesToolStripMenuItem
            // 
            this.diagnósticosMásComunesToolStripMenuItem.Name = "diagnósticosMásComunesToolStripMenuItem";
            this.diagnósticosMásComunesToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.diagnósticosMásComunesToolStripMenuItem.Text = "Diagnósticos más comunes";
            // 
            // medicamentosMásPrescritosToolStripMenuItem
            // 
            this.medicamentosMásPrescritosToolStripMenuItem.Name = "medicamentosMásPrescritosToolStripMenuItem";
            this.medicamentosMásPrescritosToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.medicamentosMásPrescritosToolStripMenuItem.Text = "Medicamentos más prescritos";
            // 
            // tratamientosMásEmpleadosToolStripMenuItem
            // 
            this.tratamientosMásEmpleadosToolStripMenuItem.Name = "tratamientosMásEmpleadosToolStripMenuItem";
            this.tratamientosMásEmpleadosToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.tratamientosMásEmpleadosToolStripMenuItem.Text = "Tratamientos más empleados";
            // 
            // utilitariosToolStripMenuItem
            // 
            this.utilitariosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.inventarioToolStripMenuItem,
            this.inventarioToolStripMenuItem1});
            this.utilitariosToolStripMenuItem.Name = "utilitariosToolStripMenuItem";
            this.utilitariosToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.utilitariosToolStripMenuItem.Text = "Farmacia";
            // 
            // inventarioToolStripMenuItem
            // 
            this.inventarioToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.comprasToolStripMenuItem,
            this.movimientosDeAlmacénToolStripMenuItem,
            this.toolStripSeparator6,
            this.kardexToolStripMenuItem});
            this.inventarioToolStripMenuItem.Name = "inventarioToolStripMenuItem";
            this.inventarioToolStripMenuItem.Size = new System.Drawing.Size(127, 22);
            this.inventarioToolStripMenuItem.Text = "Almacén";
            // 
            // comprasToolStripMenuItem
            // 
            this.comprasToolStripMenuItem.Name = "comprasToolStripMenuItem";
            this.comprasToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.comprasToolStripMenuItem.Text = "Compras";
            // 
            // movimientosDeAlmacénToolStripMenuItem
            // 
            this.movimientosDeAlmacénToolStripMenuItem.Name = "movimientosDeAlmacénToolStripMenuItem";
            this.movimientosDeAlmacénToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.movimientosDeAlmacénToolStripMenuItem.Text = "Movimientos de Almacén";
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(207, 6);
            // 
            // kardexToolStripMenuItem
            // 
            this.kardexToolStripMenuItem.Name = "kardexToolStripMenuItem";
            this.kardexToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.kardexToolStripMenuItem.Text = "Kardex";
            // 
            // inventarioToolStripMenuItem1
            // 
            this.inventarioToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.formulariosToolStripMenuItem,
            this.actualizaciónToolStripMenuItem,
            this.toolStripSeparator7,
            this.inventarioValorizadoToolStripMenuItem});
            this.inventarioToolStripMenuItem1.Name = "inventarioToolStripMenuItem1";
            this.inventarioToolStripMenuItem1.Size = new System.Drawing.Size(127, 22);
            this.inventarioToolStripMenuItem1.Text = "Inventario";
            // 
            // formulariosToolStripMenuItem
            // 
            this.formulariosToolStripMenuItem.Name = "formulariosToolStripMenuItem";
            this.formulariosToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.formulariosToolStripMenuItem.Text = "Formularios";
            // 
            // actualizaciónToolStripMenuItem
            // 
            this.actualizaciónToolStripMenuItem.Name = "actualizaciónToolStripMenuItem";
            this.actualizaciónToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.actualizaciónToolStripMenuItem.Text = "Actualización";
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(182, 6);
            // 
            // inventarioValorizadoToolStripMenuItem
            // 
            this.inventarioValorizadoToolStripMenuItem.Name = "inventarioValorizadoToolStripMenuItem";
            this.inventarioValorizadoToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.inventarioValorizadoToolStripMenuItem.Text = "Inventario Valorizado";
            // 
            // utilitariosToolStripMenuItem1
            // 
            this.utilitariosToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mantenimientoToolStripMenuItem,
            this.usuariosToolStripMenuItem,
            this.ayudaToolStripMenuItem});
            this.utilitariosToolStripMenuItem1.Name = "utilitariosToolStripMenuItem1";
            this.utilitariosToolStripMenuItem1.Size = new System.Drawing.Size(69, 20);
            this.utilitariosToolStripMenuItem1.Text = "Utilitarios";
            // 
            // mantenimientoToolStripMenuItem
            // 
            this.mantenimientoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.secuenciasToolStripMenuItem});
            this.mantenimientoToolStripMenuItem.Name = "mantenimientoToolStripMenuItem";
            this.mantenimientoToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.mantenimientoToolStripMenuItem.Text = "Mantenimiento";
            // 
            // secuenciasToolStripMenuItem
            // 
            this.secuenciasToolStripMenuItem.Name = "secuenciasToolStripMenuItem";
            this.secuenciasToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.secuenciasToolStripMenuItem.Text = "Secuencias";
            // 
            // usuariosToolStripMenuItem
            // 
            this.usuariosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.perfilesDeUsuarioToolStripMenuItem,
            this.usuariosToolStripMenuItem1});
            this.usuariosToolStripMenuItem.Name = "usuariosToolStripMenuItem";
            this.usuariosToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.usuariosToolStripMenuItem.Text = "Usuarios";
            // 
            // perfilesDeUsuarioToolStripMenuItem
            // 
            this.perfilesDeUsuarioToolStripMenuItem.Name = "perfilesDeUsuarioToolStripMenuItem";
            this.perfilesDeUsuarioToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.perfilesDeUsuarioToolStripMenuItem.Text = "Perfiles de Usuario";
            // 
            // usuariosToolStripMenuItem1
            // 
            this.usuariosToolStripMenuItem1.Name = "usuariosToolStripMenuItem1";
            this.usuariosToolStripMenuItem1.Size = new System.Drawing.Size(171, 22);
            this.usuariosToolStripMenuItem1.Text = "Usuarios";
            // 
            // ayudaToolStripMenuItem
            // 
            this.ayudaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ayudaEnLíneaToolStripMenuItem,
            this.acercaDeToolStripMenuItem});
            this.ayudaToolStripMenuItem.Name = "ayudaToolStripMenuItem";
            this.ayudaToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.ayudaToolStripMenuItem.Text = "Ayuda";
            // 
            // ayudaEnLíneaToolStripMenuItem
            // 
            this.ayudaEnLíneaToolStripMenuItem.Name = "ayudaEnLíneaToolStripMenuItem";
            this.ayudaEnLíneaToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.ayudaEnLíneaToolStripMenuItem.Text = "Ayuda en línea";
            // 
            // acercaDeToolStripMenuItem
            // 
            this.acercaDeToolStripMenuItem.Name = "acercaDeToolStripMenuItem";
            this.acercaDeToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.acercaDeToolStripMenuItem.Text = "Acerca de...";
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(41, 20);
            this.salirToolStripMenuItem.Text = "Salir";
            this.salirToolStripMenuItem.Click += new System.EventHandler(this.salirToolStripMenuItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 240);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.ManagerRenderMode;
            this.statusStrip1.Size = new System.Drawing.Size(477, 22);
            this.statusStrip1.TabIndex = 4;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(47, 17);
            this.toolStripStatusLabel1.Text = "Usuario";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = global::pjDermaBelle.Properties.Resources.Logo;
            this.pictureBox1.Location = new System.Drawing.Point(0, 24);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(477, 238);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // frmMenuPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(477, 262);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmMenuPrincipal";
            this.Text = "Sistema de Gestión de Historias Clínicas y Administrativo";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem administrativoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem maestrosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem profesionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gradosDeInstrucciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ubigeoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem documentosDeIdentidadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem citasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asistencialToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem utilitariosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pacientesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem filiaciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem controlToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem profesionalesDeLaSaludToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem especialidadesToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem tarifarioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem serviciosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem productosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tablasMaestrasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem definicionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultoriosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem serviciosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem horariiosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem citasToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem reservaDeCitasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cajaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem operacionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem arqueoToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem definicionesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem documentosDePagoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tarjetasCréditoDébitoToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem reportesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pacientesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem citadosVsAtendidosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pacientesAtendidosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem citadosVsAtendidosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem citasReservadasVsConfirmadasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cajaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem ingresosPorDíaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ingresosPorServicioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ingresosPorTipoDePagoToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem arqueosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tomaDeSignosVitalesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tomaDeSignosVitalesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem actoMédicoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem actoMédicoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem historialMédicoToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem reportesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem síntomasMásComunesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem diagnósticosMásComunesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem medicamentosMásPrescritosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tratamientosMásEmpleadosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inventarioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem comprasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem movimientosDeAlmacénToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem kardexToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inventarioToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem formulariosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem actualizaciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem inventarioValorizadoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem utilitariosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem mantenimientoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem secuenciasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem usuariosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem perfilesDeUsuarioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem usuariosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem ayudaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ayudaEnLíneaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem acercaDeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripMenuItem tipoDeTarifaToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem principiosActivosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem laboratoriosToolStripMenuItem;
    }
}

