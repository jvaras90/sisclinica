﻿namespace pjDermaBelle.Formularios.Farmacia
{
    partial class frmVentas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtProducto = new System.Windows.Forms.TextBox();
            this.lstConsulta = new System.Windows.Forms.ListBox();
            this.txtCantidad = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.chkPrecio = new System.Windows.Forms.CheckBox();
            this.txtPUnit = new System.Windows.Forms.TextBox();
            this.btnGrabar = new System.Windows.Forms.Button();
            this.txtPedidoId = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPrd_Id = new System.Windows.Forms.TextBox();
            this.btnCaja = new System.Windows.Forms.Button();
            this.btnSalir = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPedidoTotal = new System.Windows.Forms.Label();
            this.txtPedidoCliente = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtPacHC = new System.Windows.Forms.TextBox();
            this.rbtPaciente = new System.Windows.Forms.RadioButton();
            this.rbtParticular = new System.Windows.Forms.RadioButton();
            this.txtprd_Stock = new System.Windows.Forms.TextBox();
            this.txtPrd_PVP = new System.Windows.Forms.TextBox();
            this.dgvItems = new System.Windows.Forms.DataGridView();
            this.btnDel = new System.Windows.Forms.DataGridViewButtonColumn();
            this.btnUpd = new System.Windows.Forms.DataGridViewButtonColumn();
            this.ItemFcia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Codigo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Producto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Precio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvItems)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 93);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Producto";
            // 
            // txtProducto
            // 
            this.txtProducto.Location = new System.Drawing.Point(83, 89);
            this.txtProducto.Name = "txtProducto";
            this.txtProducto.Size = new System.Drawing.Size(555, 20);
            this.txtProducto.TabIndex = 4;
            this.txtProducto.TextChanged += new System.EventHandler(this.txtProducto_TextChanged);
            // 
            // lstConsulta
            // 
            this.lstConsulta.FormattingEnabled = true;
            this.lstConsulta.Location = new System.Drawing.Point(186, 8);
            this.lstConsulta.Name = "lstConsulta";
            this.lstConsulta.Size = new System.Drawing.Size(285, 17);
            this.lstConsulta.TabIndex = 2;
            this.lstConsulta.Visible = false;
            this.lstConsulta.SelectedIndexChanged += new System.EventHandler(this.lstConsulta_SelectedIndexChanged);
            // 
            // txtCantidad
            // 
            this.txtCantidad.Location = new System.Drawing.Point(83, 118);
            this.txtCantidad.Name = "txtCantidad";
            this.txtCantidad.Size = new System.Drawing.Size(56, 20);
            this.txtCantidad.TabIndex = 5;
            this.txtCantidad.Leave += new System.EventHandler(this.txtCantidad_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 123);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Cantidad";
            // 
            // chkPrecio
            // 
            this.chkPrecio.AutoSize = true;
            this.chkPrecio.Location = new System.Drawing.Point(16, 151);
            this.chkPrecio.Name = "chkPrecio";
            this.chkPrecio.Size = new System.Drawing.Size(56, 17);
            this.chkPrecio.TabIndex = 5;
            this.chkPrecio.Text = "Precio";
            this.chkPrecio.UseVisualStyleBackColor = true;
            this.chkPrecio.CheckedChanged += new System.EventHandler(this.chkPrecio_CheckedChanged);
            // 
            // txtPUnit
            // 
            this.txtPUnit.Location = new System.Drawing.Point(83, 147);
            this.txtPUnit.Name = "txtPUnit";
            this.txtPUnit.Size = new System.Drawing.Size(92, 20);
            this.txtPUnit.TabIndex = 6;
            // 
            // btnGrabar
            // 
            this.btnGrabar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGrabar.Location = new System.Drawing.Point(584, 132);
            this.btnGrabar.Name = "btnGrabar";
            this.btnGrabar.Size = new System.Drawing.Size(205, 35);
            this.btnGrabar.TabIndex = 6;
            this.btnGrabar.Text = "Agregar Item";
            this.btnGrabar.UseVisualStyleBackColor = true;
            this.btnGrabar.Click += new System.EventHandler(this.btnGrabar_Click);
            // 
            // txtPedidoId
            // 
            this.txtPedidoId.Location = new System.Drawing.Point(82, 5);
            this.txtPedidoId.Name = "txtPedidoId";
            this.txtPedidoId.Size = new System.Drawing.Size(56, 20);
            this.txtPedidoId.TabIndex = 10;
            this.txtPedidoId.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Pedido No.";
            this.label3.Visible = false;
            // 
            // txtPrd_Id
            // 
            this.txtPrd_Id.Location = new System.Drawing.Point(645, 89);
            this.txtPrd_Id.Name = "txtPrd_Id";
            this.txtPrd_Id.Size = new System.Drawing.Size(56, 20);
            this.txtPrd_Id.TabIndex = 12;
            this.txtPrd_Id.Visible = false;
            // 
            // btnCaja
            // 
            this.btnCaja.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCaja.Location = new System.Drawing.Point(12, 418);
            this.btnCaja.Name = "btnCaja";
            this.btnCaja.Size = new System.Drawing.Size(126, 42);
            this.btnCaja.TabIndex = 7;
            this.btnCaja.Text = "Caja";
            this.btnCaja.UseVisualStyleBackColor = true;
            this.btnCaja.Click += new System.EventHandler(this.btnCaja_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalir.Location = new System.Drawing.Point(663, 418);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(126, 42);
            this.btnSalir.TabIndex = 8;
            this.btnSalir.Text = "Salir";
            this.btnSalir.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(580, 373);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 24);
            this.label4.TabIndex = 16;
            this.label4.Text = "Total S/.";
            // 
            // txtPedidoTotal
            // 
            this.txtPedidoTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPedidoTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPedidoTotal.ForeColor = System.Drawing.Color.Blue;
            this.txtPedidoTotal.Location = new System.Drawing.Point(663, 373);
            this.txtPedidoTotal.Name = "txtPedidoTotal";
            this.txtPedidoTotal.Size = new System.Drawing.Size(126, 24);
            this.txtPedidoTotal.TabIndex = 17;
            this.txtPedidoTotal.Text = "Total";
            this.txtPedidoTotal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtPedidoCliente
            // 
            this.txtPedidoCliente.Location = new System.Drawing.Point(171, 60);
            this.txtPedidoCliente.Name = "txtPedidoCliente";
            this.txtPedidoCliente.Size = new System.Drawing.Size(467, 20);
            this.txtPedidoCliente.TabIndex = 3;
            this.txtPedidoCliente.TextChanged += new System.EventHandler(this.txtPedidoCliente_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(22, 64);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Cliente";
            // 
            // txtPacHC
            // 
            this.txtPacHC.Location = new System.Drawing.Point(83, 60);
            this.txtPacHC.Name = "txtPacHC";
            this.txtPacHC.Size = new System.Drawing.Size(82, 20);
            this.txtPacHC.TabIndex = 20;
            // 
            // rbtPaciente
            // 
            this.rbtPaciente.AutoSize = true;
            this.rbtPaciente.Location = new System.Drawing.Point(26, 34);
            this.rbtPaciente.Name = "rbtPaciente";
            this.rbtPaciente.Size = new System.Drawing.Size(67, 17);
            this.rbtPaciente.TabIndex = 1;
            this.rbtPaciente.TabStop = true;
            this.rbtPaciente.Text = "Paciente";
            this.rbtPaciente.UseVisualStyleBackColor = true;
            this.rbtPaciente.CheckedChanged += new System.EventHandler(this.rbtPaciente_CheckedChanged);
            // 
            // rbtParticular
            // 
            this.rbtParticular.AutoSize = true;
            this.rbtParticular.Location = new System.Drawing.Point(156, 34);
            this.rbtParticular.Name = "rbtParticular";
            this.rbtParticular.Size = new System.Drawing.Size(69, 17);
            this.rbtParticular.TabIndex = 2;
            this.rbtParticular.TabStop = true;
            this.rbtParticular.Text = "Particular";
            this.rbtParticular.UseVisualStyleBackColor = true;
            this.rbtParticular.CheckedChanged += new System.EventHandler(this.rbtParticular_CheckedChanged);
            // 
            // txtprd_Stock
            // 
            this.txtprd_Stock.Location = new System.Drawing.Point(171, 118);
            this.txtprd_Stock.Name = "txtprd_Stock";
            this.txtprd_Stock.Size = new System.Drawing.Size(56, 20);
            this.txtprd_Stock.TabIndex = 23;
            this.txtprd_Stock.Visible = false;
            // 
            // txtPrd_PVP
            // 
            this.txtPrd_PVP.Location = new System.Drawing.Point(181, 147);
            this.txtPrd_PVP.Name = "txtPrd_PVP";
            this.txtPrd_PVP.Size = new System.Drawing.Size(56, 20);
            this.txtPrd_PVP.TabIndex = 24;
            this.txtPrd_PVP.Visible = false;
            // 
            // dgvItems
            // 
            this.dgvItems.AllowUserToAddRows = false;
            this.dgvItems.AllowUserToDeleteRows = false;
            this.dgvItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvItems.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.btnDel,
            this.btnUpd,
            this.ItemFcia,
            this.Codigo,
            this.Producto,
            this.Precio,
            this.Cantidad,
            this.Total});
            this.dgvItems.Location = new System.Drawing.Point(12, 176);
            this.dgvItems.Name = "dgvItems";
            this.dgvItems.ReadOnly = true;
            this.dgvItems.Size = new System.Drawing.Size(777, 188);
            this.dgvItems.TabIndex = 8;
            this.dgvItems.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvItems_CellClick);
            this.dgvItems.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgvItems_CellPainting);
            // 
            // btnDel
            // 
            this.btnDel.HeaderText = "";
            this.btnDel.Name = "btnDel";
            this.btnDel.ReadOnly = true;
            this.btnDel.Width = 20;
            // 
            // btnUpd
            // 
            this.btnUpd.HeaderText = "";
            this.btnUpd.Name = "btnUpd";
            this.btnUpd.ReadOnly = true;
            this.btnUpd.Width = 20;
            // 
            // ItemFcia
            // 
            this.ItemFcia.HeaderText = "Item";
            this.ItemFcia.Name = "ItemFcia";
            this.ItemFcia.ReadOnly = true;
            this.ItemFcia.Visible = false;
            // 
            // Codigo
            // 
            this.Codigo.HeaderText = "Código";
            this.Codigo.Name = "Codigo";
            this.Codigo.ReadOnly = true;
            this.Codigo.Width = 50;
            // 
            // Producto
            // 
            this.Producto.HeaderText = "Producto";
            this.Producto.Name = "Producto";
            this.Producto.ReadOnly = true;
            this.Producto.Width = 480;
            // 
            // Precio
            // 
            this.Precio.HeaderText = "Precio";
            this.Precio.Name = "Precio";
            this.Precio.ReadOnly = true;
            this.Precio.Width = 50;
            // 
            // Cantidad
            // 
            this.Cantidad.HeaderText = "Cantidad";
            this.Cantidad.Name = "Cantidad";
            this.Cantidad.ReadOnly = true;
            this.Cantidad.Width = 50;
            // 
            // Total
            // 
            this.Total.HeaderText = "Total";
            this.Total.Name = "Total";
            this.Total.ReadOnly = true;
            this.Total.Width = 50;
            // 
            // frmVentas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(801, 466);
            this.Controls.Add(this.txtPrd_PVP);
            this.Controls.Add(this.txtprd_Stock);
            this.Controls.Add(this.lstConsulta);
            this.Controls.Add(this.rbtParticular);
            this.Controls.Add(this.rbtPaciente);
            this.Controls.Add(this.txtPacHC);
            this.Controls.Add(this.txtPedidoCliente);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtPedidoTotal);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.btnCaja);
            this.Controls.Add(this.txtPrd_Id);
            this.Controls.Add(this.txtPedidoId);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dgvItems);
            this.Controls.Add(this.btnGrabar);
            this.Controls.Add(this.txtPUnit);
            this.Controls.Add(this.chkPrecio);
            this.Controls.Add(this.txtCantidad);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtProducto);
            this.Controls.Add(this.label1);
            this.Name = "frmVentas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ventas Farmacia";
            this.Load += new System.EventHandler(this.frmVentas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvItems)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtProducto;
        private System.Windows.Forms.ListBox lstConsulta;
        private System.Windows.Forms.TextBox txtCantidad;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox chkPrecio;
        private System.Windows.Forms.TextBox txtPUnit;
        private System.Windows.Forms.Button btnGrabar;
        private System.Windows.Forms.TextBox txtPedidoId;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtPrd_Id;
        private System.Windows.Forms.Button btnCaja;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label txtPedidoTotal;
        private System.Windows.Forms.TextBox txtPedidoCliente;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtPacHC;
        private System.Windows.Forms.RadioButton rbtPaciente;
        private System.Windows.Forms.RadioButton rbtParticular;
        private System.Windows.Forms.TextBox txtprd_Stock;
        private System.Windows.Forms.TextBox txtPrd_PVP;
        private System.Windows.Forms.DataGridView dgvItems;
        private System.Windows.Forms.DataGridViewButtonColumn btnDel;
        private System.Windows.Forms.DataGridViewButtonColumn btnUpd;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemFcia;
        private System.Windows.Forms.DataGridViewTextBoxColumn Codigo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Producto;
        private System.Windows.Forms.DataGridViewTextBoxColumn Precio;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cantidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total;
    }
}