﻿using pjDermaBelle.Formularios.Citas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pjDermaBelle.Formularios.Farmacia
{
    public partial class frmVentas : Form
    {
        ClaseDatos obj = new ClaseDatos();
        DermaBelleDataDataContext linq = new DermaBelleDataDataContext();
        FarmaciaCabecera oFmciaCabecera = new FarmaciaCabecera();
        FarmaciaDetalle oFmciaDetalle = new FarmaciaDetalle();
        frmCajaOperaciones caja = new frmCajaOperaciones();

        public string[,] arrayFmciaDetalle = new string[1, 6];
        public string[,] arrayCajaDetalle = new string[1, 11];
        decimal TotalPedido = 0;

        public int opc;
        int item = 0;
        bool editar = false;
        
        void Mostrar()
        {
            switch (opc)
            {
                case 1:
                    if (!string.IsNullOrEmpty(txtPedidoCliente.Text))
                    {
                        if (!rbtParticular.Checked)
                        {
                            lstConsulta.Visible = true;

                            lstConsulta.Location = new Point(171, 86);
                            lstConsulta.Size = new Size(467, 160);
                            List<usp_PacientesBusResult> listaPacientes = new List<usp_PacientesBusResult>();
                            listaPacientes = obj.BuscarPaciente(txtPedidoCliente.Text);
                            lstConsulta.Items.Clear();
                            if (listaPacientes.Count() > 0)
                            {
                                foreach (usp_PacientesBusResult reg in listaPacientes)
                                {
                                    lstConsulta.Items.Add(reg.Pac_NomComp);
                                }
                            }
                        }
                    }
                    break;
                case 2:
                    if (!string.IsNullOrEmpty(txtProducto.Text) && !editar)
                    {
                        lstConsulta.Visible = true;

                        lstConsulta.Location = new Point(83, 115);
                        lstConsulta.Size = new Size(555, 160);
                        lstConsulta.Items.Add(txtProducto.Text);
                        List<usp_ProductosVenResult> listaProductos = new List<usp_ProductosVenResult>();
                        listaProductos = obj.ProductosVen(txtProducto.Text);
                        lstConsulta.Items.Clear();
                        if (listaProductos.Count() > 0)
                        {
                            foreach (usp_ProductosVenResult reg in listaProductos)
                            {
                                lstConsulta.Items.Add(reg.Prd_Descripcion + "   [Stock: " + reg.Prd_Stock + "]");
                            }
                        }
                    }
                    break;
            }
        }

        public frmVentas()
        {
            InitializeComponent();
        }

        private void frmVentas_Load(object sender, EventArgs e)
        {
            txtPedidoId.Text = "0";
            txtPUnit.Enabled = false;
            rbtPaciente.Checked = true;
            btnCaja.Enabled = false;
        }

        private void rbtPaciente_CheckedChanged(object sender, EventArgs e)
        {
            txtPacHC.Text = "";
            txtPacHC.Enabled = rbtPaciente.Checked;
        }

        private void rbtParticular_CheckedChanged(object sender, EventArgs e)
        {
            txtPacHC.Text = "999999999";
            txtPacHC.Enabled = rbtPaciente.Checked;
        }


        void GrabarItem()
        {
            if(string.IsNullOrEmpty(txtPedidoId.Text))
            {
                if (rbtPaciente.Checked)
                { oFmciaCabecera.TipoCliente = 'P';}
                if (rbtParticular.Checked)
                { oFmciaCabecera.TipoCliente ='C';}
                oFmciaCabecera.Pac_HC = txtPacHC.Text;
                oFmciaCabecera.ClienteNombre = txtPedidoCliente.Text;
                oFmciaCabecera.Total = Convert.ToDecimal(txtPedidoTotal.Text);
                obj.FmciaCabeceraAdd(oFmciaCabecera);

                List<usp_RecupPedIdResult> lista = new List<usp_RecupPedIdResult>();
                lista = obj.RecupPedId();

                if (lista.Count() > 0)
                {
                    foreach (usp_RecupPedIdResult reg in lista)
                    {
                        txtPedidoId.Text = reg.VentaId.ToString();
                    }
                }

            }

            //Detalle
           // oFmciaDetalle.Prd_Id = txtPedidoId.Text;

            //        HabilitarReg(false);
            //        break;
            //}
        }

        private void btnGrabar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtProducto.Text) ||
                string.IsNullOrEmpty(txtCantidad.Text) ||
                string.IsNullOrEmpty(txtPUnit.Text))
            {
                MessageBox.Show("Datos incompletos. Por favor revise");
                return;
            }

            item = item + 1;
            arrayFmciaDetalle[0, 0] = item.ToString();  //Item
            arrayFmciaDetalle[0, 1] = txtPrd_Id.Text;   //Código Producto
            arrayFmciaDetalle[0, 2] = txtProducto.Text; //Producto
            arrayFmciaDetalle[0, 3] = txtPUnit.Text;    //Precio Unitario
            arrayFmciaDetalle[0, 4] = txtCantidad.Text; //Cantidad
            arrayFmciaDetalle[0, 5] = (Convert.ToDecimal(txtPUnit.Text) * int.Parse(txtCantidad.Text)).ToString(); //Total

            TotalPedido = TotalPedido + (Convert.ToDecimal(txtPUnit.Text) * int.Parse(txtCantidad.Text));
            dgvItems.Rows.Add("", "", arrayFmciaDetalle[0, 0],    //Item
                                      arrayFmciaDetalle[0, 1],    //Código Producto
                                      arrayFmciaDetalle[0, 2],    //Producto
                                      arrayFmciaDetalle[0, 3],    //Precio Unitario
                                      arrayFmciaDetalle[0, 4],    //Cantidad
                                      arrayFmciaDetalle[0, 5]);   //Total

            arrayCajaDetalle[0, 0] = item.ToString(); //CajaItem
            arrayCajaDetalle[0, 1] = "SE"; //CajaTipoItem
            arrayCajaDetalle[0, 2] = txtPrd_Id.Text.ToString(); //CajaCodigo
            arrayCajaDetalle[0, 3] = "FA"; //CajaTipoTarifa
            arrayCajaDetalle[0, 4] = txtProducto.Text; //Descripcion
            arrayCajaDetalle[0, 5] = txtPUnit.Text; //CajaItemPrecioSol
            arrayCajaDetalle[0, 6] = "0.00"; //CajaItemPrecioDol
            arrayCajaDetalle[0, 7] = "18.00"; //CajaItemIgv
            arrayCajaDetalle[0, 8] = txtCantidad.Text; //CajaCantidad
            arrayCajaDetalle[0, 9] = txtPUnit.Text; //CajaItemPrecioFinal
            arrayCajaDetalle[0, 10] = (Convert.ToDecimal(txtPUnit.Text) * int.Parse(txtCantidad.Text)).ToString(); //CajaItemTotal

            caja.dgvDetalleCaja.Rows.Add("", "", arrayCajaDetalle[0, 0],    //CajaItem
                                                 arrayCajaDetalle[0, 1],    //CajaTipoItem
                                                 arrayCajaDetalle[0, 2],    //Codigo
                                                 arrayCajaDetalle[0, 3],    //TipoTarifa
                                                 arrayCajaDetalle[0, 4],    //Descripcion
                                                 arrayCajaDetalle[0, 5],    //PrecioSol
                                                 arrayCajaDetalle[0, 6],    //PrecioDol
                                                 arrayCajaDetalle[0, 7],    //Igv
                                                 arrayCajaDetalle[0, 8],    //Cantidad
                                                 arrayCajaDetalle[0, 9],    //PrecioFinal
                                                 arrayCajaDetalle[0, 10]);  //Total

            btnCaja.Enabled = true;
            txtProducto.Text = "";
            txtCantidad.Text = "";
            txtPUnit.Text = "";
            lstConsulta.Visible = false;
            txtProducto.Enabled = true;
            txtPedidoTotal.Text = TotalPedido.ToString();
            editar = false;
        }

        private void btnCaja_Click(object sender, EventArgs e)
        {
            caja.esreserva = false;
            caja.txtCitaId.Text = txtPedidoId.Text;
            caja.pPac_HC = txtPacHC.Text;
            caja.txtCajaClienteNombre.Text = txtPedidoCliente.Text;
            caja.txtTCambio.Text = "0.00";
            caja.userId = 1; // userId;
            caja.ShowDialog();
            this.Close();
        }

        private void txtPedidoCliente_TextChanged(object sender, EventArgs e)
        {
            opc = 1;
            Mostrar();
        }

        private void txtProducto_TextChanged(object sender, EventArgs e)
        {
            opc = 2;
            Mostrar();
        }

        private void lstConsulta_SelectedIndexChanged(object sender, EventArgs e)
        {
           switch (opc)
           {
               case 1:
                   txtPedidoCliente.Text = lstConsulta.SelectedItem.ToString();
                   List<usp_PacientesBusResult> listaPacientes = new List<usp_PacientesBusResult>();
                   listaPacientes = obj.BuscarPaciente(txtPedidoCliente.Text);
                   if (listaPacientes.Count() > 0)
                   {
                       foreach (usp_PacientesBusResult reg in listaPacientes)
                       {
                           txtPacHC.Text = reg.Pac_Hc;
                       }
                   }                       
                   break;
               case 2:
                   string cadena = lstConsulta.SelectedItem.ToString();
                   int pos = cadena.IndexOf("[");
                   txtProducto.Text = lstConsulta.SelectedItem.ToString().Substring(0,pos);
                   List<usp_ProductosVenResult> listaProductos = new List<usp_ProductosVenResult>();
                   listaProductos = obj.ProductosVen(txtProducto.Text.Trim());
                   if (listaProductos.Count() > 0)
                   {
                      foreach (usp_ProductosVenResult reg in listaProductos)
                      {
                          if (reg.Prd_Stock == 0)
                          { 
                              MessageBox.Show("Producto sin Stock, por favor verifique");
                              txtProducto.Text = "";
                          }
                          else
                          { 
                              txtPrd_Id.Text = reg.Prd_Id.ToString();
                              txtprd_Stock.Text = reg.Prd_Stock.ToString();
                              txtPrd_PVP.Text = reg.Prd_PVP.ToString();
                              txtPUnit.Text = reg.Prd_PVP.ToString();
                          }
                      }
                   }
                   break;
           }
           lstConsulta.Visible = false;
        }

        private void txtCantidad_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtCantidad.Text) &&
                int.Parse(txtCantidad.Text) > int.Parse(txtprd_Stock.Text))
            { MessageBox.Show("Cantidad Solicitada es mayor al Stock Disponible. Por favor revise"); }
        }

        private void chkPrecio_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPrecio.Checked)
            {
                frmClavePrecios clave = new frmClavePrecios();
                clave.ShowDialog();
                if (clave.validado)
                {
                    txtPUnit.Enabled = true;
                    txtPUnit.Focus();
                }
                else
                {
                    MessageBox.Show("Usuario no autorizado para modificar precios");
                }
            }
            else
                txtPUnit.Enabled = false;

        }

        private void dgvItems_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.ColumnIndex >= 0 && this.dgvItems.Columns[e.ColumnIndex].Name == "btnDel" && e.RowIndex >= 0)
            {
                e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                DataGridViewButtonCell celBoton = this.dgvItems.Rows[e.RowIndex].Cells["btnDel"] as DataGridViewButtonCell;
                Icon icoEliminar = new Icon(@"C:\Windows\sisclinica\Ico\Delete.ico");
                e.Graphics.DrawIcon(icoEliminar, e.CellBounds.Left + 3, e.CellBounds.Top + 3);

                this.dgvItems.Rows[e.RowIndex].Height = icoEliminar.Height + 10;
                this.dgvItems.Columns[e.ColumnIndex].Width = icoEliminar.Width + 10;

                e.Handled = true;
            }

            if (e.ColumnIndex >= 0 && this.dgvItems.Columns[e.ColumnIndex].Name == "btnUpd" && e.RowIndex >= 0)
            {
                e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                DataGridViewButtonCell celBoton = this.dgvItems.Rows[e.RowIndex].Cells["btnUpd"] as DataGridViewButtonCell;
                Icon icoModificar = new Icon(@"C:\Windows\sisclinica\Ico\Style_file.ico");
                e.Graphics.DrawIcon(icoModificar, e.CellBounds.Left + 3, e.CellBounds.Top + 3);

                this.dgvItems.Rows[e.RowIndex].Height = icoModificar.Height + 10;
                this.dgvItems.Columns[e.ColumnIndex].Width = icoModificar.Width + 10;

                e.Handled = true;
            }
        }

        private void dgvItems_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (this.dgvItems.Columns[e.ColumnIndex].Name == "btnDel")
            {
                int fil = dgvItems.CurrentRow.Index;
                dgvItems.Rows.RemoveAt(fil);
            }

            if (this.dgvItems.Columns[e.ColumnIndex].Name == "btnUpd")
            {
                DataGridViewRow row = dgvItems.CurrentRow;
                if (row != null)
                {
                    editar = true;
                    txtProducto.Text = row.Cells["Producto"].Value.ToString();
                    txtCantidad.Text = row.Cells["Cantidad"].Value.ToString();
                    txtPUnit.Text = row.Cells["Precio"].Value.ToString();
                    txtProducto.Enabled = false;
                    int fil = dgvItems.CurrentRow.Index;
                    dgvItems.Rows.RemoveAt(fil);
                }
            }
        }
    }
}
