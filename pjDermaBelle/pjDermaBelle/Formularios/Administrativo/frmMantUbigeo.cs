﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pjDermaBelle
{
    public partial class frmMantUbigeo : Form
    {
        public frmMantUbigeo()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        ClaseDatos obj = new ClaseDatos();
        DermaBelleDataDataContext linq = new DermaBelleDataDataContext();
        Ubigeo oUbigeo = new Ubigeo();

        bool nuevo = true;

        private void Limpiar()
        {
            txtUbiCod.Clear();
            txtNombre.Clear();
            chkEstado.Checked = false;
            txtUbiCod.Enabled = false;
            txtNombre.Enabled = false;
            btnNuevo.Enabled = true;
            btnGrabar.Enabled = false;
            btnActualizar.Enabled = false;
            btnAnular.Enabled = false;
            btnCerrar.Enabled = true;

            lblCreado.Visible = false;
            lblFecCrea.Visible = false;
            lblModificado.Visible = false;
            lblFecMod.Visible = false;

            btnNuevo.Focus();
        }

        void CargarLista()
        {
            dgvLista.DataSource = obj.UbigeoListar();

            for (int indice = 0; indice < dgvLista.Rows.Count; indice++)
            {
                dgvLista.Columns[0].Width = 60;     //Código
                dgvLista.Columns[1].Width = 320;    //Ubigeo
                dgvLista.Columns[2].Width = 50;     //Estado

                dgvLista.Rows[indice].ReadOnly = true;
            }
        }

        private void frmMantUbigeo_Load(object sender, EventArgs e)
        {            
            Limpiar();
            CargarLista();
        }

        private void txtNombre_TextChanged(object sender, EventArgs e)
        {
            btnNuevo.Enabled = false;
            if (nuevo)
            {
                btnGrabar.Enabled = txtUbiCod.Text != string.Empty &&
                     txtNombre.Text != string.Empty;
                btnAnular.Enabled = false;
            }
            else
            {
                btnGrabar.Enabled = false;
                btnActualizar.Enabled = true;
                btnAnular.Enabled = true;
            }
            btnCerrar.Enabled = true;
        }

        private void txtNombre_Leave(object sender, EventArgs e)
        {
            if (nuevo)
            {
                btnGrabar.Focus();
            }
            else
            { btnActualizar.Focus(); }
        }

        private void dgvLista_SelectionChanged(object sender, EventArgs e)
        {
            int indice = dgvLista.CurrentCell.RowIndex;
            string Ubi_Cod = dgvLista.Rows[indice].Cells[0].Value.ToString();

            List<usp_UbigeoRecResult> lista = new List<usp_UbigeoRecResult>();
            lista = obj.UbigeoDatos(Ubi_Cod);                
            if (lista.Count() > 0)
            {
                foreach (usp_UbigeoRecResult reg in lista)
                {
                    txtUbiCod.Text = reg.Ubi_Cod;
                    txtNombre.Text = reg.Ubi_Nombre;
                    lblFecCrea.Text = reg.FecCrea.ToString();
                    lblFecMod.Text = reg.FecModi.ToString();
                    chkEstado.Checked = reg.Estado;

                    if (chkEstado.Checked)
                    { btnAnular.Text = "A&nular"; }
                    else
                    { btnAnular.Text = "A&ctivar"; }

                    txtNombre.Enabled = true;
                    btnNuevo.Enabled = true;
                    btnGrabar.Enabled = false;
                    btnActualizar.Enabled = true;
                    btnAnular.Enabled = true;
                    btnCerrar.Enabled = true;

                    lblCreado.Visible = true;
                    lblFecCrea.Visible = true;
                    lblModificado.Visible = true;
                    lblFecMod.Visible = true;

                    nuevo = false;
                    txtNombre.Focus();
                }
            }

        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Limpiar();
            //lblFecCrea.Text = lblFecha.Text;

            txtUbiCod.Enabled = true;
            txtNombre.Enabled = true;
            chkEstado.Checked = true;
            btnNuevo.Enabled = false;
            btnGrabar.Enabled = false;
            btnActualizar.Enabled = false;
            btnAnular.Enabled = false;
            btnCerrar.Enabled = true;

            txtUbiCod.Focus();
            nuevo = true;
        }

        private void btnGrabar_Click(object sender, EventArgs e)
        {
            oUbigeo.Ubi_Cod = txtUbiCod.Text;
            oUbigeo.Ubi_Nombre = txtNombre.Text;
            oUbigeo.Estado = chkEstado.Checked;
            oUbigeo.UsuCrea = 1;
            oUbigeo.UsuModi = 1;
            obj.UbigeoAdicionar(oUbigeo);
            Limpiar();
            MessageBox.Show("Registro GRABADO satisfactoriamente", "AVISO");
            CargarLista();
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            oUbigeo.Ubi_Cod = txtUbiCod.Text;
            oUbigeo.Ubi_Nombre = txtNombre.Text;
            oUbigeo.UsuModi = 1;
            obj.UbigeoActualizar(oUbigeo);
            Limpiar();
            MessageBox.Show("Registro ACTUALIZADO satisfactoriamente", "AVISO");
            CargarLista();
        }

        private void btnAnular_Click(object sender, EventArgs e)
        {
            oUbigeo.Ubi_Cod = txtUbiCod.Text;
            oUbigeo.Ubi_Nombre = txtNombre.Text;
            chkEstado.Checked = !chkEstado.Checked;
            oUbigeo.Estado = chkEstado.Checked;
            oUbigeo.UsuModi = 1;
            obj.UbigeoAnular(oUbigeo);
            if (chkEstado.Checked)
            { MessageBox.Show("Registro ACTIVADO satisfactoriamente", "AVISO"); }
            else
            { MessageBox.Show("Registro ANULADO satisfactoriamente", "AVISO"); }
            Limpiar();
            CargarLista();
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            if ((MessageBox.Show("¿Seguro que desea salir ?", "Aviso",
                     MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                     MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes))
            {
                this.Close();
            }
        }
    }
}
