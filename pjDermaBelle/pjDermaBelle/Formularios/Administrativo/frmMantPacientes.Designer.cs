﻿namespace pjDermaBelle
{
    partial class frmMantPacientes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tbcListado = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.txtPaciente = new System.Windows.Forms.TextBox();
            this.dgvPacientes = new System.Windows.Forms.DataGridView();
            this.btnMostrar = new System.Windows.Forms.DataGridViewButtonColumn();
            this.label26 = new System.Windows.Forms.Label();
            this.tbcRegistro = new System.Windows.Forms.TabPage();
            this.lblPac_HC = new System.Windows.Forms.Label();
            this.gpbBotones = new System.Windows.Forms.GroupBox();
            this.btnNuevo = new System.Windows.Forms.Button();
            this.btnGrabar = new System.Windows.Forms.Button();
            this.btnActualizar = new System.Windows.Forms.Button();
            this.btnAnular = new System.Windows.Forms.Button();
            this.btnCerrar = new System.Windows.Forms.Button();
            this.lblFecMod = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.lblModificado = new System.Windows.Forms.Label();
            this.lblCreado = new System.Windows.Forms.Label();
            this.lblFecCrea = new System.Windows.Forms.Label();
            this.tbcDetalle = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label36 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.cboDistPac = new System.Windows.Forms.ComboBox();
            this.txtPac_Direccion = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtPac_Telefonos = new System.Windows.Forms.TextBox();
            this.txtPac_Email = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label33 = new System.Windows.Forms.Label();
            this.txtPac_PersResp = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.cboDistResp = new System.Windows.Forms.ComboBox();
            this.txtPac_DirecResp = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtPac_TelefResp = new System.Windows.Forms.TextBox();
            this.txtPac_EmailResp = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label34 = new System.Windows.Forms.Label();
            this.txtPac_NombreFact = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.cboDistFacturacion = new System.Windows.Forms.ComboBox();
            this.txtPac_DirecFact = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtPac_RUCFact = new System.Windows.Forms.TextBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.txtPac_InfAdicional = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkEstado = new System.Windows.Forms.CheckBox();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.cboGpoSang = new System.Windows.Forms.ComboBox();
            this.label31 = new System.Windows.Forms.Label();
            this.rbtFemenino = new System.Windows.Forms.RadioButton();
            this.rbtMasculino = new System.Windows.Forms.RadioButton();
            this.txtPac_DocIdentNum = new System.Windows.Forms.TextBox();
            this.cboProfesion = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cboInstruccion = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cboDocIdentidad = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpPac_FecNac = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cboLugNacimiento = new System.Windows.Forms.ComboBox();
            this.txtPac_ApPat = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtPac_ApMat = new System.Windows.Forms.TextBox();
            this.txtPac_Nombres = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblPac_NomComp = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lstPacientes = new System.Windows.Forms.ListBox();
            this.txtPac_HC = new System.Windows.Forms.TextBox();
            this.tabControl1.SuspendLayout();
            this.tbcListado.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPacientes)).BeginInit();
            this.tbcRegistro.SuspendLayout();
            this.gpbBotones.SuspendLayout();
            this.tbcDetalle.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tbcListado);
            this.tabControl1.Controls.Add(this.tbcRegistro);
            this.tabControl1.Location = new System.Drawing.Point(3, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1021, 563);
            this.tabControl1.TabIndex = 0;
            // 
            // tbcListado
            // 
            this.tbcListado.Controls.Add(this.txtPac_HC);
            this.tbcListado.Controls.Add(this.lstPacientes);
            this.tbcListado.Controls.Add(this.button1);
            this.tbcListado.Controls.Add(this.txtPaciente);
            this.tbcListado.Controls.Add(this.dgvPacientes);
            this.tbcListado.Controls.Add(this.label26);
            this.tbcListado.Location = new System.Drawing.Point(4, 22);
            this.tbcListado.Name = "tbcListado";
            this.tbcListado.Padding = new System.Windows.Forms.Padding(3);
            this.tbcListado.Size = new System.Drawing.Size(1013, 537);
            this.tbcListado.TabIndex = 1;
            this.tbcListado.Text = "Listado";
            this.tbcListado.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(864, 14);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(143, 23);
            this.button1.TabIndex = 30;
            this.button1.Text = "&Nuevo";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtPaciente
            // 
            this.txtPaciente.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPaciente.Location = new System.Drawing.Point(61, 16);
            this.txtPaciente.Name = "txtPaciente";
            this.txtPaciente.Size = new System.Drawing.Size(601, 20);
            this.txtPaciente.TabIndex = 29;
            this.txtPaciente.TextChanged += new System.EventHandler(this.txtPaciente_TextChanged);
            // 
            // dgvPacientes
            // 
            this.dgvPacientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPacientes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.btnMostrar});
            this.dgvPacientes.Location = new System.Drawing.Point(7, 43);
            this.dgvPacientes.Name = "dgvPacientes";
            this.dgvPacientes.Size = new System.Drawing.Size(1000, 488);
            this.dgvPacientes.TabIndex = 28;
            this.dgvPacientes.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPacientes_CellClick);
            this.dgvPacientes.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgvPacientes_CellPainting);
            this.dgvPacientes.SelectionChanged += new System.EventHandler(this.dgvPacientes_SelectionChanged);
            // 
            // btnMostrar
            // 
            this.btnMostrar.HeaderText = "";
            this.btnMostrar.Name = "btnMostrar";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(6, 19);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(49, 13);
            this.label26.TabIndex = 25;
            this.label26.Text = "Paciente";
            // 
            // tbcRegistro
            // 
            this.tbcRegistro.Controls.Add(this.lblPac_HC);
            this.tbcRegistro.Controls.Add(this.gpbBotones);
            this.tbcRegistro.Controls.Add(this.lblFecMod);
            this.tbcRegistro.Controls.Add(this.label28);
            this.tbcRegistro.Controls.Add(this.lblModificado);
            this.tbcRegistro.Controls.Add(this.lblCreado);
            this.tbcRegistro.Controls.Add(this.lblFecCrea);
            this.tbcRegistro.Controls.Add(this.tbcDetalle);
            this.tbcRegistro.Controls.Add(this.label27);
            this.tbcRegistro.Controls.Add(this.groupBox1);
            this.tbcRegistro.Controls.Add(this.label1);
            this.tbcRegistro.Controls.Add(this.pictureBox1);
            this.tbcRegistro.Controls.Add(this.lblPac_NomComp);
            this.tbcRegistro.Controls.Add(this.label5);
            this.tbcRegistro.Controls.Add(this.shapeContainer1);
            this.tbcRegistro.Location = new System.Drawing.Point(4, 22);
            this.tbcRegistro.Name = "tbcRegistro";
            this.tbcRegistro.Padding = new System.Windows.Forms.Padding(3);
            this.tbcRegistro.Size = new System.Drawing.Size(1013, 537);
            this.tbcRegistro.TabIndex = 0;
            this.tbcRegistro.Text = "Registro";
            this.tbcRegistro.UseVisualStyleBackColor = true;
            // 
            // lblPac_HC
            // 
            this.lblPac_HC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPac_HC.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPac_HC.ForeColor = System.Drawing.Color.Blue;
            this.lblPac_HC.Location = new System.Drawing.Point(160, 7);
            this.lblPac_HC.Name = "lblPac_HC";
            this.lblPac_HC.Size = new System.Drawing.Size(151, 24);
            this.lblPac_HC.TabIndex = 81;
            this.lblPac_HC.Text = "0000000000";
            this.lblPac_HC.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gpbBotones
            // 
            this.gpbBotones.Controls.Add(this.btnNuevo);
            this.gpbBotones.Controls.Add(this.btnGrabar);
            this.gpbBotones.Controls.Add(this.btnActualizar);
            this.gpbBotones.Controls.Add(this.btnAnular);
            this.gpbBotones.Controls.Add(this.btnCerrar);
            this.gpbBotones.Location = new System.Drawing.Point(854, 234);
            this.gpbBotones.Name = "gpbBotones";
            this.gpbBotones.Size = new System.Drawing.Size(153, 254);
            this.gpbBotones.TabIndex = 80;
            this.gpbBotones.TabStop = false;
            // 
            // btnNuevo
            // 
            this.btnNuevo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNuevo.Location = new System.Drawing.Point(6, 10);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(140, 31);
            this.btnNuevo.TabIndex = 24;
            this.btnNuevo.Text = "&Nuevo";
            this.btnNuevo.UseVisualStyleBackColor = true;
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // btnGrabar
            // 
            this.btnGrabar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGrabar.Location = new System.Drawing.Point(6, 41);
            this.btnGrabar.Name = "btnGrabar";
            this.btnGrabar.Size = new System.Drawing.Size(140, 31);
            this.btnGrabar.TabIndex = 25;
            this.btnGrabar.Text = "&Grabar";
            this.btnGrabar.UseVisualStyleBackColor = true;
            this.btnGrabar.Click += new System.EventHandler(this.btnGrabar_Click);
            // 
            // btnActualizar
            // 
            this.btnActualizar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnActualizar.Location = new System.Drawing.Point(6, 72);
            this.btnActualizar.Name = "btnActualizar";
            this.btnActualizar.Size = new System.Drawing.Size(140, 31);
            this.btnActualizar.TabIndex = 26;
            this.btnActualizar.Text = "&Actualizar";
            this.btnActualizar.UseVisualStyleBackColor = true;
            this.btnActualizar.Click += new System.EventHandler(this.btnActualizar_Click);
            // 
            // btnAnular
            // 
            this.btnAnular.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAnular.Location = new System.Drawing.Point(6, 103);
            this.btnAnular.Name = "btnAnular";
            this.btnAnular.Size = new System.Drawing.Size(140, 31);
            this.btnAnular.TabIndex = 27;
            this.btnAnular.Text = "A&nular";
            this.btnAnular.UseVisualStyleBackColor = true;
            this.btnAnular.Click += new System.EventHandler(this.btnAnular_Click);
            // 
            // btnCerrar
            // 
            this.btnCerrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCerrar.Location = new System.Drawing.Point(6, 214);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(140, 31);
            this.btnCerrar.TabIndex = 28;
            this.btnCerrar.Text = "&Cerrar";
            this.btnCerrar.UseVisualStyleBackColor = true;
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // lblFecMod
            // 
            this.lblFecMod.AutoSize = true;
            this.lblFecMod.ForeColor = System.Drawing.Color.Crimson;
            this.lblFecMod.Location = new System.Drawing.Point(272, 507);
            this.lblFecMod.Name = "lblFecMod";
            this.lblFecMod.Size = new System.Drawing.Size(133, 13);
            this.lblFecMod.TabIndex = 77;
            this.lblFecMod.Text = "dd/mm/aaaa hh:mm:ss ?m";
            this.lblFecMod.Visible = false;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Red;
            this.label28.Location = new System.Drawing.Point(864, 507);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(140, 13);
            this.label28.TabIndex = 74;
            this.label28.Text = "( * )  Datos Obligatorios";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblModificado
            // 
            this.lblModificado.AutoSize = true;
            this.lblModificado.ForeColor = System.Drawing.Color.Crimson;
            this.lblModificado.Location = new System.Drawing.Point(188, 507);
            this.lblModificado.Name = "lblModificado";
            this.lblModificado.Size = new System.Drawing.Size(84, 13);
            this.lblModificado.TabIndex = 78;
            this.lblModificado.Text = "/  Modificado el:";
            this.lblModificado.Visible = false;
            // 
            // lblCreado
            // 
            this.lblCreado.AutoSize = true;
            this.lblCreado.ForeColor = System.Drawing.Color.Crimson;
            this.lblCreado.Location = new System.Drawing.Point(5, 507);
            this.lblCreado.Name = "lblCreado";
            this.lblCreado.Size = new System.Drawing.Size(55, 13);
            this.lblCreado.TabIndex = 75;
            this.lblCreado.Text = "Creado el:";
            this.lblCreado.Visible = false;
            // 
            // lblFecCrea
            // 
            this.lblFecCrea.AutoSize = true;
            this.lblFecCrea.ForeColor = System.Drawing.Color.Crimson;
            this.lblFecCrea.Location = new System.Drawing.Point(58, 507);
            this.lblFecCrea.Name = "lblFecCrea";
            this.lblFecCrea.Size = new System.Drawing.Size(133, 13);
            this.lblFecCrea.TabIndex = 76;
            this.lblFecCrea.Text = "dd/mm/aaaa hh:mm:ss ?m";
            this.lblFecCrea.Visible = false;
            // 
            // tbcDetalle
            // 
            this.tbcDetalle.Controls.Add(this.tabPage1);
            this.tbcDetalle.Controls.Add(this.tabPage2);
            this.tbcDetalle.Controls.Add(this.tabPage3);
            this.tbcDetalle.Controls.Add(this.tabPage4);
            this.tbcDetalle.Location = new System.Drawing.Point(6, 220);
            this.tbcDetalle.Name = "tbcDetalle";
            this.tbcDetalle.SelectedIndex = 0;
            this.tbcDetalle.Size = new System.Drawing.Size(842, 268);
            this.tbcDetalle.TabIndex = 33;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label36);
            this.tabPage1.Controls.Add(this.label29);
            this.tabPage1.Controls.Add(this.label32);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.cboDistPac);
            this.tabPage1.Controls.Add(this.txtPac_Direccion);
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.Controls.Add(this.txtPac_Telefonos);
            this.tabPage1.Controls.Add(this.txtPac_Email);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(834, 242);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Datos Personales (*)";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Red;
            this.label36.Location = new System.Drawing.Point(6, 135);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(14, 16);
            this.label36.TabIndex = 47;
            this.label36.Text = "*";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Red;
            this.label29.Location = new System.Drawing.Point(6, 100);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(14, 16);
            this.label29.TabIndex = 46;
            this.label29.Text = "*";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(29, 23);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(135, 20);
            this.label32.TabIndex = 45;
            this.label32.Text = "Datos Personales";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(28, 64);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(52, 13);
            this.label12.TabIndex = 35;
            this.label12.Text = "Dirección";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Location = new System.Drawing.Point(26, 135);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(54, 13);
            this.label13.TabIndex = 37;
            this.label13.Text = "Teléfonos";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(46, 170);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(34, 13);
            this.label14.TabIndex = 39;
            this.label14.Text = "e-mail";
            // 
            // cboDistPac
            // 
            this.cboDistPac.FormattingEnabled = true;
            this.cboDistPac.Location = new System.Drawing.Point(86, 95);
            this.cboDistPac.Name = "cboDistPac";
            this.cboDistPac.Size = new System.Drawing.Size(363, 21);
            this.cboDistPac.TabIndex = 38;
            // 
            // txtPac_Direccion
            // 
            this.txtPac_Direccion.Location = new System.Drawing.Point(86, 60);
            this.txtPac_Direccion.Name = "txtPac_Direccion";
            this.txtPac_Direccion.Size = new System.Drawing.Size(710, 20);
            this.txtPac_Direccion.TabIndex = 36;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(41, 99);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(39, 13);
            this.label15.TabIndex = 42;
            this.label15.Text = "Distrito";
            // 
            // txtPac_Telefonos
            // 
            this.txtPac_Telefonos.Location = new System.Drawing.Point(86, 131);
            this.txtPac_Telefonos.Name = "txtPac_Telefonos";
            this.txtPac_Telefonos.Size = new System.Drawing.Size(293, 20);
            this.txtPac_Telefonos.TabIndex = 40;
            this.txtPac_Telefonos.TextChanged += new System.EventHandler(this.txtPac_Telefonos_TextChanged_1);
            // 
            // txtPac_Email
            // 
            this.txtPac_Email.Location = new System.Drawing.Point(86, 166);
            this.txtPac_Email.Name = "txtPac_Email";
            this.txtPac_Email.Size = new System.Drawing.Size(445, 20);
            this.txtPac_Email.TabIndex = 41;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.MistyRose;
            this.tabPage2.Controls.Add(this.label33);
            this.tabPage2.Controls.Add(this.txtPac_PersResp);
            this.tabPage2.Controls.Add(this.label20);
            this.tabPage2.Controls.Add(this.label16);
            this.tabPage2.Controls.Add(this.label17);
            this.tabPage2.Controls.Add(this.label18);
            this.tabPage2.Controls.Add(this.cboDistResp);
            this.tabPage2.Controls.Add(this.txtPac_DirecResp);
            this.tabPage2.Controls.Add(this.label19);
            this.tabPage2.Controls.Add(this.txtPac_TelefResp);
            this.tabPage2.Controls.Add(this.txtPac_EmailResp);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(834, 242);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Datos de Emergencia";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(8, 19);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(238, 20);
            this.label33.TabIndex = 42;
            this.label33.Text = "En caso de Emergencia lamar a:";
            // 
            // txtPac_PersResp
            // 
            this.txtPac_PersResp.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtPac_PersResp.Location = new System.Drawing.Point(106, 62);
            this.txtPac_PersResp.Name = "txtPac_PersResp";
            this.txtPac_PersResp.Size = new System.Drawing.Size(646, 20);
            this.txtPac_PersResp.TabIndex = 35;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(9, 66);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(91, 13);
            this.label20.TabIndex = 34;
            this.label20.Text = "Nombre Completo";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(48, 94);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(52, 13);
            this.label16.TabIndex = 26;
            this.label16.Text = "Dirección";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(46, 151);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(54, 13);
            this.label17.TabIndex = 27;
            this.label17.Text = "Teléfonos";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(65, 179);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(34, 13);
            this.label18.TabIndex = 28;
            this.label18.Text = "e-mail";
            // 
            // cboDistResp
            // 
            this.cboDistResp.FormattingEnabled = true;
            this.cboDistResp.Location = new System.Drawing.Point(106, 118);
            this.cboDistResp.Name = "cboDistResp";
            this.cboDistResp.Size = new System.Drawing.Size(292, 21);
            this.cboDistResp.TabIndex = 33;
            // 
            // txtPac_DirecResp
            // 
            this.txtPac_DirecResp.Location = new System.Drawing.Point(106, 90);
            this.txtPac_DirecResp.Name = "txtPac_DirecResp";
            this.txtPac_DirecResp.Size = new System.Drawing.Size(646, 20);
            this.txtPac_DirecResp.TabIndex = 29;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(61, 122);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(39, 13);
            this.label19.TabIndex = 32;
            this.label19.Text = "Distrito";
            // 
            // txtPac_TelefResp
            // 
            this.txtPac_TelefResp.Location = new System.Drawing.Point(106, 147);
            this.txtPac_TelefResp.Name = "txtPac_TelefResp";
            this.txtPac_TelefResp.Size = new System.Drawing.Size(293, 20);
            this.txtPac_TelefResp.TabIndex = 31;
            // 
            // txtPac_EmailResp
            // 
            this.txtPac_EmailResp.Location = new System.Drawing.Point(106, 175);
            this.txtPac_EmailResp.Name = "txtPac_EmailResp";
            this.txtPac_EmailResp.Size = new System.Drawing.Size(445, 20);
            this.txtPac_EmailResp.TabIndex = 30;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.label34);
            this.tabPage3.Controls.Add(this.txtPac_NombreFact);
            this.tabPage3.Controls.Add(this.label21);
            this.tabPage3.Controls.Add(this.label22);
            this.tabPage3.Controls.Add(this.label23);
            this.tabPage3.Controls.Add(this.cboDistFacturacion);
            this.tabPage3.Controls.Add(this.txtPac_DirecFact);
            this.tabPage3.Controls.Add(this.label24);
            this.tabPage3.Controls.Add(this.txtPac_RUCFact);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(834, 242);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Datos de Facturación";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(6, 19);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(162, 20);
            this.label34.TabIndex = 41;
            this.label34.Text = "Datos de Facturación";
            // 
            // txtPac_NombreFact
            // 
            this.txtPac_NombreFact.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtPac_NombreFact.Location = new System.Drawing.Point(140, 62);
            this.txtPac_NombreFact.Name = "txtPac_NombreFact";
            this.txtPac_NombreFact.Size = new System.Drawing.Size(584, 20);
            this.txtPac_NombreFact.TabIndex = 33;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(9, 66);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(119, 13);
            this.label21.TabIndex = 40;
            this.label21.Text = "Nombre o Razón Social";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(76, 108);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(52, 13);
            this.label22.TabIndex = 37;
            this.label22.Text = "Dirección";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(89, 193);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(39, 13);
            this.label23.TabIndex = 38;
            this.label23.Text = "R.U.C.";
            // 
            // cboDistFacturacion
            // 
            this.cboDistFacturacion.FormattingEnabled = true;
            this.cboDistFacturacion.Location = new System.Drawing.Point(139, 146);
            this.cboDistFacturacion.Name = "cboDistFacturacion";
            this.cboDistFacturacion.Size = new System.Drawing.Size(365, 21);
            this.cboDistFacturacion.TabIndex = 35;
            // 
            // txtPac_DirecFact
            // 
            this.txtPac_DirecFact.Location = new System.Drawing.Point(139, 104);
            this.txtPac_DirecFact.Name = "txtPac_DirecFact";
            this.txtPac_DirecFact.Size = new System.Drawing.Size(584, 20);
            this.txtPac_DirecFact.TabIndex = 34;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(89, 150);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(39, 13);
            this.label24.TabIndex = 39;
            this.label24.Text = "Distrito";
            // 
            // txtPac_RUCFact
            // 
            this.txtPac_RUCFact.Location = new System.Drawing.Point(139, 189);
            this.txtPac_RUCFact.Name = "txtPac_RUCFact";
            this.txtPac_RUCFact.Size = new System.Drawing.Size(158, 20);
            this.txtPac_RUCFact.TabIndex = 36;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.txtPac_InfAdicional);
            this.tabPage4.Controls.Add(this.label35);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(834, 242);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Información Adicional";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // txtPac_InfAdicional
            // 
            this.txtPac_InfAdicional.Location = new System.Drawing.Point(12, 40);
            this.txtPac_InfAdicional.Multiline = true;
            this.txtPac_InfAdicional.Name = "txtPac_InfAdicional";
            this.txtPac_InfAdicional.Size = new System.Drawing.Size(816, 196);
            this.txtPac_InfAdicional.TabIndex = 43;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(8, 16);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(161, 20);
            this.label35.TabIndex = 42;
            this.label35.Text = "Información Adicional";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Red;
            this.label27.Location = new System.Drawing.Point(8, 16);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(14, 16);
            this.label27.TabIndex = 32;
            this.label27.Text = "*";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkEstado);
            this.groupBox1.Controls.Add(this.label39);
            this.groupBox1.Controls.Add(this.label38);
            this.groupBox1.Controls.Add(this.label30);
            this.groupBox1.Controls.Add(this.label37);
            this.groupBox1.Controls.Add(this.cboGpoSang);
            this.groupBox1.Controls.Add(this.label31);
            this.groupBox1.Controls.Add(this.rbtFemenino);
            this.groupBox1.Controls.Add(this.rbtMasculino);
            this.groupBox1.Controls.Add(this.txtPac_DocIdentNum);
            this.groupBox1.Controls.Add(this.cboProfesion);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.cboInstruccion);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.cboDocIdentidad);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.dtpPac_FecNac);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cboLugNacimiento);
            this.groupBox1.Controls.Add(this.txtPac_ApPat);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtPac_ApMat);
            this.groupBox1.Controls.Add(this.txtPac_Nombres);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Location = new System.Drawing.Point(150, 48);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(857, 166);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "    Datos de Filiación";
            // 
            // chkEstado
            // 
            this.chkEstado.AutoSize = true;
            this.chkEstado.Enabled = false;
            this.chkEstado.Location = new System.Drawing.Point(747, 128);
            this.chkEstado.Name = "chkEstado";
            this.chkEstado.Size = new System.Drawing.Size(56, 17);
            this.chkEstado.TabIndex = 39;
            this.chkEstado.Text = "Activo";
            this.chkEstado.UseVisualStyleBackColor = true;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Red;
            this.label39.Location = new System.Drawing.Point(466, 26);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(14, 16);
            this.label39.TabIndex = 38;
            this.label39.Text = "*";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Red;
            this.label38.Location = new System.Drawing.Point(7, 76);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(14, 16);
            this.label38.TabIndex = 37;
            this.label38.Text = "*";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Red;
            this.label30.Location = new System.Drawing.Point(6, 54);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(14, 16);
            this.label30.TabIndex = 36;
            this.label30.Text = "*";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Red;
            this.label37.Location = new System.Drawing.Point(6, 28);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(14, 16);
            this.label37.TabIndex = 35;
            this.label37.Text = "*";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cboGpoSang
            // 
            this.cboGpoSang.FormattingEnabled = true;
            this.cboGpoSang.Items.AddRange(new object[] {
            "-- No Indica --",
            "Grupo \"AB\" POSITIVO",
            "Grupo \"AB\" NEGATIVO",
            "Grupo \"A\" POSITIVO",
            "Grupo \"A\"\tNEGATIVO",
            "Grupo \"B\" POSITIVO",
            "Grupo \"B\" NEGATIVO",
            "Grupo \"O\" POSITIVO",
            "Grupo \"O\" NEGATIVO"});
            this.cboGpoSang.Location = new System.Drawing.Point(524, 126);
            this.cboGpoSang.Name = "cboGpoSang";
            this.cboGpoSang.Size = new System.Drawing.Size(178, 21);
            this.cboGpoSang.TabIndex = 33;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.ForeColor = System.Drawing.Color.Red;
            this.label31.Location = new System.Drawing.Point(427, 130);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(90, 13);
            this.label31.TabIndex = 34;
            this.label31.Text = "Grupo Sanguineo";
            // 
            // rbtFemenino
            // 
            this.rbtFemenino.AutoSize = true;
            this.rbtFemenino.Location = new System.Drawing.Point(631, 26);
            this.rbtFemenino.Name = "rbtFemenino";
            this.rbtFemenino.Size = new System.Drawing.Size(71, 17);
            this.rbtFemenino.TabIndex = 9;
            this.rbtFemenino.TabStop = true;
            this.rbtFemenino.Text = "Femenino";
            this.rbtFemenino.UseVisualStyleBackColor = true;
            // 
            // rbtMasculino
            // 
            this.rbtMasculino.AutoSize = true;
            this.rbtMasculino.Location = new System.Drawing.Point(524, 26);
            this.rbtMasculino.Name = "rbtMasculino";
            this.rbtMasculino.Size = new System.Drawing.Size(73, 17);
            this.rbtMasculino.TabIndex = 8;
            this.rbtMasculino.TabStop = true;
            this.rbtMasculino.Text = "Masculino";
            this.rbtMasculino.UseVisualStyleBackColor = true;
            // 
            // txtPac_DocIdentNum
            // 
            this.txtPac_DocIdentNum.Location = new System.Drawing.Point(707, 50);
            this.txtPac_DocIdentNum.Name = "txtPac_DocIdentNum";
            this.txtPac_DocIdentNum.Size = new System.Drawing.Size(144, 20);
            this.txtPac_DocIdentNum.TabIndex = 11;
            // 
            // cboProfesion
            // 
            this.cboProfesion.FormattingEnabled = true;
            this.cboProfesion.Location = new System.Drawing.Point(524, 100);
            this.cboProfesion.Name = "cboProfesion";
            this.cboProfesion.Size = new System.Drawing.Size(326, 21);
            this.cboProfesion.TabIndex = 13;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(402, 104);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(115, 13);
            this.label11.TabIndex = 21;
            this.label11.Text = "Profesión u Ocupación";
            // 
            // cboInstruccion
            // 
            this.cboInstruccion.FormattingEnabled = true;
            this.cboInstruccion.Location = new System.Drawing.Point(524, 74);
            this.cboInstruccion.Name = "cboInstruccion";
            this.cboInstruccion.Size = new System.Drawing.Size(178, 21);
            this.cboInstruccion.TabIndex = 12;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(411, 78);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(106, 13);
            this.label10.TabIndex = 19;
            this.label10.Text = "Grado de Instrucción";
            // 
            // cboDocIdentidad
            // 
            this.cboDocIdentidad.FormattingEnabled = true;
            this.cboDocIdentidad.Location = new System.Drawing.Point(524, 49);
            this.cboDocIdentidad.Name = "cboDocIdentidad";
            this.cboDocIdentidad.Size = new System.Drawing.Size(178, 21);
            this.cboDocIdentidad.TabIndex = 10;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(393, 53);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(124, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "Documento de Identidad";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(27, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Apellido Paterno";
            // 
            // dtpPac_FecNac
            // 
            this.dtpPac_FecNac.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpPac_FecNac.Location = new System.Drawing.Point(117, 126);
            this.dtpPac_FecNac.Name = "dtpPac_FecNac";
            this.dtpPac_FecNac.Size = new System.Drawing.Size(79, 20);
            this.dtpPac_FecNac.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(25, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Apellido Materno";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 130);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(108, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Fecha de Nacimiento";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(62, 78);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Nombres";
            // 
            // cboLugNacimiento
            // 
            this.cboLugNacimiento.FormattingEnabled = true;
            this.cboLugNacimiento.Location = new System.Drawing.Point(117, 100);
            this.cboLugNacimiento.Name = "cboLugNacimiento";
            this.cboLugNacimiento.Size = new System.Drawing.Size(237, 21);
            this.cboLugNacimiento.TabIndex = 6;
            // 
            // txtPac_ApPat
            // 
            this.txtPac_ApPat.Location = new System.Drawing.Point(117, 24);
            this.txtPac_ApPat.Name = "txtPac_ApPat";
            this.txtPac_ApPat.Size = new System.Drawing.Size(237, 20);
            this.txtPac_ApPat.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 104);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(105, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Lugar de Nacimiento";
            // 
            // txtPac_ApMat
            // 
            this.txtPac_ApMat.Location = new System.Drawing.Point(117, 49);
            this.txtPac_ApMat.Name = "txtPac_ApMat";
            this.txtPac_ApMat.Size = new System.Drawing.Size(237, 20);
            this.txtPac_ApMat.TabIndex = 4;
            // 
            // txtPac_Nombres
            // 
            this.txtPac_Nombres.Location = new System.Drawing.Point(117, 74);
            this.txtPac_Nombres.Name = "txtPac_Nombres";
            this.txtPac_Nombres.Size = new System.Drawing.Size(237, 20);
            this.txtPac_Nombres.TabIndex = 5;
            this.txtPac_Nombres.Leave += new System.EventHandler(this.txtPac_Nombres_Leave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(486, 28);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Sexo";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(19, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 16);
            this.label1.TabIndex = 19;
            this.label1.Text = "Historia Clínica Nro.";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(9, 48);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(135, 166);
            this.pictureBox1.TabIndex = 18;
            this.pictureBox1.TabStop = false;
            // 
            // lblPac_NomComp
            // 
            this.lblPac_NomComp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPac_NomComp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPac_NomComp.ForeColor = System.Drawing.Color.Blue;
            this.lblPac_NomComp.Location = new System.Drawing.Point(550, 7);
            this.lblPac_NomComp.Name = "lblPac_NomComp";
            this.lblPac_NomComp.Size = new System.Drawing.Size(457, 24);
            this.lblPac_NomComp.TabIndex = 21;
            this.lblPac_NomComp.Text = "Nombre Completo";
            this.lblPac_NomComp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(423, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(118, 16);
            this.label5.TabIndex = 21;
            this.label5.Text = "Nombre Completo";
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(3, 3);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(1007, 531);
            this.shapeContainer1.TabIndex = 79;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape1
            // 
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 2;
            this.lineShape1.X2 = 1006;
            this.lineShape1.Y1 = 495;
            this.lineShape1.Y2 = 495;
            // 
            // lstPacientes
            // 
            this.lstPacientes.FormattingEnabled = true;
            this.lstPacientes.Location = new System.Drawing.Point(61, 43);
            this.lstPacientes.Name = "lstPacientes";
            this.lstPacientes.Size = new System.Drawing.Size(601, 277);
            this.lstPacientes.TabIndex = 31;
            this.lstPacientes.SelectedIndexChanged += new System.EventHandler(this.lstPacientes_SelectedIndexChanged);
            // 
            // txtPac_HC
            // 
            this.txtPac_HC.Location = new System.Drawing.Point(668, 16);
            this.txtPac_HC.Name = "txtPac_HC";
            this.txtPac_HC.Size = new System.Drawing.Size(100, 20);
            this.txtPac_HC.TabIndex = 32;
            this.txtPac_HC.Visible = false;
            // 
            // frmMantPacientes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1026, 573);
            this.Controls.Add(this.tabControl1);
            this.Name = "frmMantPacientes";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mantenimiento de Pacientes";
            this.Load += new System.EventHandler(this.frmMantPacientes_Load);
            this.tabControl1.ResumeLayout(false);
            this.tbcListado.ResumeLayout(false);
            this.tbcListado.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPacientes)).EndInit();
            this.tbcRegistro.ResumeLayout(false);
            this.tbcRegistro.PerformLayout();
            this.gpbBotones.ResumeLayout(false);
            this.tbcDetalle.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tbcRegistro;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtPac_DocIdentNum;
        private System.Windows.Forms.ComboBox cboProfesion;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cboInstruccion;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cboDocIdentidad;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpPac_FecNac;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cboLugNacimiento;
        private System.Windows.Forms.TextBox txtPac_ApPat;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtPac_ApMat;
        private System.Windows.Forms.TextBox txtPac_Nombres;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TabPage tbcListado;
        private System.Windows.Forms.Button btnCerrar;
        private System.Windows.Forms.Button btnAnular;
        private System.Windows.Forms.Button btnActualizar;
        private System.Windows.Forms.Button btnGrabar;
        private System.Windows.Forms.Button btnNuevo;
        private System.Windows.Forms.DataGridView dgvPacientes;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtPaciente;
        private System.Windows.Forms.RadioButton rbtFemenino;
        private System.Windows.Forms.RadioButton rbtMasculino;
        private System.Windows.Forms.Label lblPac_NomComp;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.ComboBox cboGpoSang;
        private System.Windows.Forms.Label label31;
        public System.Windows.Forms.TabControl tabControl1;
        public System.Windows.Forms.Button button1;
        internal System.Windows.Forms.Label lblFecMod;
        private System.Windows.Forms.Label label28;
        internal System.Windows.Forms.Label lblModificado;
        internal System.Windows.Forms.Label lblCreado;
        internal System.Windows.Forms.Label lblFecCrea;
        private System.Windows.Forms.TabControl tbcDetalle;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cboDistPac;
        private System.Windows.Forms.TextBox txtPac_Direccion;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtPac_Telefonos;
        private System.Windows.Forms.TextBox txtPac_Email;
        private System.Windows.Forms.TextBox txtPac_PersResp;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox cboDistResp;
        private System.Windows.Forms.TextBox txtPac_DirecResp;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtPac_TelefResp;
        private System.Windows.Forms.TextBox txtPac_EmailResp;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox txtPac_NombreFact;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox cboDistFacturacion;
        private System.Windows.Forms.TextBox txtPac_DirecFact;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtPac_RUCFact;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private System.Windows.Forms.GroupBox gpbBotones;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label lblPac_HC;
        private System.Windows.Forms.DataGridViewButtonColumn btnMostrar;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox txtPac_InfAdicional;
        private System.Windows.Forms.CheckBox chkEstado;
        private System.Windows.Forms.ListBox lstPacientes;
        private System.Windows.Forms.TextBox txtPac_HC;

    }
}