﻿namespace pjDermaBelle.Formularios.Administrativo
{
    partial class frmProductos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbpProductos = new System.Windows.Forms.TabControl();
            this.tbpRegistro = new System.Windows.Forms.TabPage();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnEditar = new System.Windows.Forms.Button();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.lstPActivos = new System.Windows.Forms.ListBox();
            this.btnAnular = new System.Windows.Forms.Button();
            this.btnGrabar = new System.Windows.Forms.Button();
            this.btnNuevo = new System.Windows.Forms.Button();
            this.cmbFrmFarmac = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cmbClasifTerap = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbLaboratorios = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtPrA_Cod = new System.Windows.Forms.TextBox();
            this.txtPActivo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.rbtFMagistral = new System.Windows.Forms.RadioButton();
            this.rbtPTerminado = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDescripcion = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblPrd_Id = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.chkVenta = new System.Windows.Forms.CheckBox();
            this.chkEstado = new System.Windows.Forms.CheckBox();
            this.chkIgv = new System.Windows.Forms.CheckBox();
            this.txtIgv = new System.Windows.Forms.TextBox();
            this.tbpFormula = new System.Windows.Forms.TabPage();
            this.gpbFormula = new System.Windows.Forms.GroupBox();
            this.lblUnidad = new System.Windows.Forms.Label();
            this.txtCantidad = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnGrabaComp = new System.Windows.Forms.Button();
            this.dgvFormula = new System.Windows.Forms.DataGridView();
            this.btnDelComponente = new System.Windows.Forms.DataGridViewButtonColumn();
            this.btnUpdComponente = new System.Windows.Forms.DataGridViewButtonColumn();
            this.cmbElemento = new System.Windows.Forms.ComboBox();
            this.lblElemento = new System.Windows.Forms.Label();
            this.rbtElemBase = new System.Windows.Forms.RadioButton();
            this.rbtComponente = new System.Windows.Forms.RadioButton();
            this.tbpAlmacenes = new System.Windows.Forms.TabPage();
            this.gpbAlmacenes = new System.Windows.Forms.GroupBox();
            this.dgvProdAlmacen = new System.Windows.Forms.DataGridView();
            this.btnAlmacenDel = new System.Windows.Forms.DataGridViewButtonColumn();
            this.btnAgregar = new System.Windows.Forms.Button();
            this.cmbAlmacenes = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbpProductos.SuspendLayout();
            this.tbpRegistro.SuspendLayout();
            this.tbpFormula.SuspendLayout();
            this.gpbFormula.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFormula)).BeginInit();
            this.tbpAlmacenes.SuspendLayout();
            this.gpbAlmacenes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProdAlmacen)).BeginInit();
            this.SuspendLayout();
            // 
            // tbpProductos
            // 
            this.tbpProductos.Controls.Add(this.tbpRegistro);
            this.tbpProductos.Controls.Add(this.tbpFormula);
            this.tbpProductos.Controls.Add(this.tbpAlmacenes);
            this.tbpProductos.Location = new System.Drawing.Point(2, 3);
            this.tbpProductos.Name = "tbpProductos";
            this.tbpProductos.SelectedIndex = 0;
            this.tbpProductos.Size = new System.Drawing.Size(773, 366);
            this.tbpProductos.TabIndex = 0;
            // 
            // tbpRegistro
            // 
            this.tbpRegistro.Controls.Add(this.btnCancelar);
            this.tbpRegistro.Controls.Add(this.btnEditar);
            this.tbpRegistro.Controls.Add(this.btnBuscar);
            this.tbpRegistro.Controls.Add(this.lstPActivos);
            this.tbpRegistro.Controls.Add(this.btnAnular);
            this.tbpRegistro.Controls.Add(this.btnGrabar);
            this.tbpRegistro.Controls.Add(this.btnNuevo);
            this.tbpRegistro.Controls.Add(this.cmbFrmFarmac);
            this.tbpRegistro.Controls.Add(this.label8);
            this.tbpRegistro.Controls.Add(this.cmbClasifTerap);
            this.tbpRegistro.Controls.Add(this.label6);
            this.tbpRegistro.Controls.Add(this.cmbLaboratorios);
            this.tbpRegistro.Controls.Add(this.label5);
            this.tbpRegistro.Controls.Add(this.txtPrA_Cod);
            this.tbpRegistro.Controls.Add(this.txtPActivo);
            this.tbpRegistro.Controls.Add(this.label2);
            this.tbpRegistro.Controls.Add(this.rbtFMagistral);
            this.tbpRegistro.Controls.Add(this.rbtPTerminado);
            this.tbpRegistro.Controls.Add(this.label4);
            this.tbpRegistro.Controls.Add(this.txtDescripcion);
            this.tbpRegistro.Controls.Add(this.label3);
            this.tbpRegistro.Controls.Add(this.lblPrd_Id);
            this.tbpRegistro.Controls.Add(this.label1);
            this.tbpRegistro.Controls.Add(this.chkVenta);
            this.tbpRegistro.Controls.Add(this.chkEstado);
            this.tbpRegistro.Controls.Add(this.chkIgv);
            this.tbpRegistro.Controls.Add(this.txtIgv);
            this.tbpRegistro.Location = new System.Drawing.Point(4, 22);
            this.tbpRegistro.Name = "tbpRegistro";
            this.tbpRegistro.Padding = new System.Windows.Forms.Padding(3);
            this.tbpRegistro.Size = new System.Drawing.Size(765, 340);
            this.tbpRegistro.TabIndex = 0;
            this.tbpRegistro.Text = "Registro";
            this.tbpRegistro.UseVisualStyleBackColor = true;
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(654, 263);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(99, 36);
            this.btnCancelar.TabIndex = 31;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnEditar
            // 
            this.btnEditar.Location = new System.Drawing.Point(654, 177);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(99, 36);
            this.btnEditar.TabIndex = 30;
            this.btnEditar.Text = "Editar";
            this.btnEditar.UseVisualStyleBackColor = true;
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(654, 48);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(99, 36);
            this.btnBuscar.TabIndex = 26;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // lstPActivos
            // 
            this.lstPActivos.FormattingEnabled = true;
            this.lstPActivos.Location = new System.Drawing.Point(551, 95);
            this.lstPActivos.Name = "lstPActivos";
            this.lstPActivos.Size = new System.Drawing.Size(82, 17);
            this.lstPActivos.TabIndex = 25;
            this.lstPActivos.Visible = false;
            this.lstPActivos.SelectedIndexChanged += new System.EventHandler(this.lstPActivos_SelectedIndexChanged);
            // 
            // btnAnular
            // 
            this.btnAnular.Location = new System.Drawing.Point(654, 220);
            this.btnAnular.Name = "btnAnular";
            this.btnAnular.Size = new System.Drawing.Size(99, 36);
            this.btnAnular.TabIndex = 23;
            this.btnAnular.Text = "Anular";
            this.btnAnular.UseVisualStyleBackColor = true;
            this.btnAnular.Click += new System.EventHandler(this.btnAnular_Click);
            // 
            // btnGrabar
            // 
            this.btnGrabar.Location = new System.Drawing.Point(654, 134);
            this.btnGrabar.Name = "btnGrabar";
            this.btnGrabar.Size = new System.Drawing.Size(99, 36);
            this.btnGrabar.TabIndex = 22;
            this.btnGrabar.Text = "Grabar";
            this.btnGrabar.UseVisualStyleBackColor = true;
            this.btnGrabar.Click += new System.EventHandler(this.btnGrabar_Click);
            // 
            // btnNuevo
            // 
            this.btnNuevo.Location = new System.Drawing.Point(654, 91);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(99, 36);
            this.btnNuevo.TabIndex = 21;
            this.btnNuevo.Text = "Nuevo";
            this.btnNuevo.UseVisualStyleBackColor = true;
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // cmbFrmFarmac
            // 
            this.cmbFrmFarmac.FormattingEnabled = true;
            this.cmbFrmFarmac.Location = new System.Drawing.Point(148, 213);
            this.cmbFrmFarmac.Name = "cmbFrmFarmac";
            this.cmbFrmFarmac.Size = new System.Drawing.Size(179, 21);
            this.cmbFrmFarmac.TabIndex = 18;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(31, 217);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(103, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Forma Farmaceútica";
            // 
            // cmbClasifTerap
            // 
            this.cmbClasifTerap.FormattingEnabled = true;
            this.cmbClasifTerap.Location = new System.Drawing.Point(148, 183);
            this.cmbClasifTerap.Name = "cmbClasifTerap";
            this.cmbClasifTerap.Size = new System.Drawing.Size(488, 21);
            this.cmbClasifTerap.TabIndex = 14;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 187);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(126, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Clasificación Terapeútica";
            // 
            // cmbLaboratorios
            // 
            this.cmbLaboratorios.FormattingEnabled = true;
            this.cmbLaboratorios.Location = new System.Drawing.Point(148, 153);
            this.cmbLaboratorios.Name = "cmbLaboratorios";
            this.cmbLaboratorios.Size = new System.Drawing.Size(488, 21);
            this.cmbLaboratorios.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(74, 157);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Laboratorio";
            // 
            // txtPrA_Cod
            // 
            this.txtPrA_Cod.Location = new System.Drawing.Point(459, 94);
            this.txtPrA_Cod.Name = "txtPrA_Cod";
            this.txtPrA_Cod.Size = new System.Drawing.Size(82, 20);
            this.txtPrA_Cod.TabIndex = 10;
            this.txtPrA_Cod.Visible = false;
            // 
            // txtPActivo
            // 
            this.txtPActivo.Location = new System.Drawing.Point(148, 123);
            this.txtPActivo.Name = "txtPActivo";
            this.txtPActivo.Size = new System.Drawing.Size(488, 20);
            this.txtPActivo.TabIndex = 8;
            this.txtPActivo.TextChanged += new System.EventHandler(this.txtPActivo_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(54, 127);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Principio Activo";
            // 
            // rbtFMagistral
            // 
            this.rbtFMagistral.AutoSize = true;
            this.rbtFMagistral.Location = new System.Drawing.Point(314, 95);
            this.rbtFMagistral.Name = "rbtFMagistral";
            this.rbtFMagistral.Size = new System.Drawing.Size(107, 17);
            this.rbtFMagistral.TabIndex = 6;
            this.rbtFMagistral.TabStop = true;
            this.rbtFMagistral.Text = "Fórmula Magistral";
            this.rbtFMagistral.UseVisualStyleBackColor = true;
            this.rbtFMagistral.CheckedChanged += new System.EventHandler(this.rbtFMagistral_CheckedChanged);
            // 
            // rbtPTerminado
            // 
            this.rbtPTerminado.AutoSize = true;
            this.rbtPTerminado.Location = new System.Drawing.Point(148, 95);
            this.rbtPTerminado.Name = "rbtPTerminado";
            this.rbtPTerminado.Size = new System.Drawing.Size(121, 17);
            this.rbtPTerminado.TabIndex = 5;
            this.rbtPTerminado.TabStop = true;
            this.rbtPTerminado.Text = "Producto Terminado";
            this.rbtPTerminado.UseVisualStyleBackColor = true;
            this.rbtPTerminado.CheckedChanged += new System.EventHandler(this.rbtPTerminado_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(45, 97);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Tipo de Producto";
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.Location = new System.Drawing.Point(148, 63);
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.Size = new System.Drawing.Size(488, 20);
            this.txtDescripcion.TabIndex = 3;
            this.txtDescripcion.TextChanged += new System.EventHandler(this.txtDescripcion_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(71, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Descripción";
            // 
            // lblPrd_Id
            // 
            this.lblPrd_Id.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrd_Id.ForeColor = System.Drawing.Color.Blue;
            this.lblPrd_Id.Location = new System.Drawing.Point(148, 37);
            this.lblPrd_Id.Name = "lblPrd_Id";
            this.lblPrd_Id.Size = new System.Drawing.Size(64, 13);
            this.lblPrd_Id.TabIndex = 1;
            this.lblPrd_Id.Text = "Código";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(94, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Código";
            // 
            // chkVenta
            // 
            this.chkVenta.AutoSize = true;
            this.chkVenta.Enabled = false;
            this.chkVenta.Location = new System.Drawing.Point(148, 246);
            this.chkVenta.Name = "chkVenta";
            this.chkVenta.Size = new System.Drawing.Size(141, 17);
            this.chkVenta.TabIndex = 32;
            this.chkVenta.Text = "Disponible para la Venta";
            this.chkVenta.UseVisualStyleBackColor = true;
            // 
            // chkEstado
            // 
            this.chkEstado.AutoSize = true;
            this.chkEstado.Enabled = false;
            this.chkEstado.Location = new System.Drawing.Point(580, 246);
            this.chkEstado.Name = "chkEstado";
            this.chkEstado.Size = new System.Drawing.Size(56, 17);
            this.chkEstado.TabIndex = 24;
            this.chkEstado.Text = "Activo";
            this.chkEstado.UseVisualStyleBackColor = true;
            // 
            // chkIgv
            // 
            this.chkIgv.AutoSize = true;
            this.chkIgv.Location = new System.Drawing.Point(348, 246);
            this.chkIgv.Name = "chkIgv";
            this.chkIgv.Size = new System.Drawing.Size(106, 17);
            this.chkIgv.TabIndex = 28;
            this.chkIgv.Text = "Afecto al IGV (%)";
            this.chkIgv.UseVisualStyleBackColor = true;
            // 
            // txtIgv
            // 
            this.txtIgv.Location = new System.Drawing.Point(468, 244);
            this.txtIgv.Name = "txtIgv";
            this.txtIgv.ReadOnly = true;
            this.txtIgv.Size = new System.Drawing.Size(61, 20);
            this.txtIgv.TabIndex = 27;
            this.txtIgv.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtIgv.Visible = false;
            // 
            // tbpFormula
            // 
            this.tbpFormula.Controls.Add(this.gpbFormula);
            this.tbpFormula.Location = new System.Drawing.Point(4, 22);
            this.tbpFormula.Name = "tbpFormula";
            this.tbpFormula.Padding = new System.Windows.Forms.Padding(3);
            this.tbpFormula.Size = new System.Drawing.Size(765, 340);
            this.tbpFormula.TabIndex = 2;
            this.tbpFormula.Text = "Fórmula Magistral";
            this.tbpFormula.UseVisualStyleBackColor = true;
            this.tbpFormula.Enter += new System.EventHandler(this.tbpFormula_Enter);
            // 
            // gpbFormula
            // 
            this.gpbFormula.Controls.Add(this.lblUnidad);
            this.gpbFormula.Controls.Add(this.txtCantidad);
            this.gpbFormula.Controls.Add(this.label7);
            this.gpbFormula.Controls.Add(this.btnGrabaComp);
            this.gpbFormula.Controls.Add(this.dgvFormula);
            this.gpbFormula.Controls.Add(this.cmbElemento);
            this.gpbFormula.Controls.Add(this.lblElemento);
            this.gpbFormula.Controls.Add(this.rbtElemBase);
            this.gpbFormula.Controls.Add(this.rbtComponente);
            this.gpbFormula.Location = new System.Drawing.Point(6, 6);
            this.gpbFormula.Name = "gpbFormula";
            this.gpbFormula.Size = new System.Drawing.Size(753, 328);
            this.gpbFormula.TabIndex = 0;
            this.gpbFormula.TabStop = false;
            // 
            // lblUnidad
            // 
            this.lblUnidad.AutoSize = true;
            this.lblUnidad.Location = new System.Drawing.Point(152, 85);
            this.lblUnidad.Name = "lblUnidad";
            this.lblUnidad.Size = new System.Drawing.Size(27, 13);
            this.lblUnidad.TabIndex = 49;
            this.lblUnidad.Text = "Und";
            // 
            // txtCantidad
            // 
            this.txtCantidad.Location = new System.Drawing.Point(72, 81);
            this.txtCantidad.Name = "txtCantidad";
            this.txtCantidad.Size = new System.Drawing.Size(71, 20);
            this.txtCantidad.TabIndex = 48;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 85);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 13);
            this.label7.TabIndex = 47;
            this.label7.Text = "Cantidad";
            // 
            // btnGrabaComp
            // 
            this.btnGrabaComp.Location = new System.Drawing.Point(654, 80);
            this.btnGrabaComp.Name = "btnGrabaComp";
            this.btnGrabaComp.Size = new System.Drawing.Size(93, 23);
            this.btnGrabaComp.TabIndex = 46;
            this.btnGrabaComp.Text = "Agregar";
            this.btnGrabaComp.UseVisualStyleBackColor = true;
            this.btnGrabaComp.Click += new System.EventHandler(this.btnGrabaComp_Click_1);
            // 
            // dgvFormula
            // 
            this.dgvFormula.AllowUserToAddRows = false;
            this.dgvFormula.AllowUserToDeleteRows = false;
            this.dgvFormula.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFormula.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.btnDelComponente,
            this.btnUpdComponente});
            this.dgvFormula.Location = new System.Drawing.Point(6, 119);
            this.dgvFormula.Name = "dgvFormula";
            this.dgvFormula.ReadOnly = true;
            this.dgvFormula.Size = new System.Drawing.Size(741, 203);
            this.dgvFormula.TabIndex = 45;
            this.dgvFormula.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvFormula_CellClick);
            this.dgvFormula.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgvFormula_CellPainting);
            // 
            // btnDelComponente
            // 
            this.btnDelComponente.HeaderText = "";
            this.btnDelComponente.Name = "btnDelComponente";
            this.btnDelComponente.ReadOnly = true;
            // 
            // btnUpdComponente
            // 
            this.btnUpdComponente.HeaderText = "";
            this.btnUpdComponente.Name = "btnUpdComponente";
            this.btnUpdComponente.ReadOnly = true;
            // 
            // cmbElemento
            // 
            this.cmbElemento.FormattingEnabled = true;
            this.cmbElemento.Location = new System.Drawing.Point(72, 47);
            this.cmbElemento.Name = "cmbElemento";
            this.cmbElemento.Size = new System.Drawing.Size(553, 21);
            this.cmbElemento.TabIndex = 44;
            this.cmbElemento.SelectedIndexChanged += new System.EventHandler(this.cmbElemento_SelectedIndexChanged);
            // 
            // lblElemento
            // 
            this.lblElemento.AutoSize = true;
            this.lblElemento.Location = new System.Drawing.Point(6, 51);
            this.lblElemento.Name = "lblElemento";
            this.lblElemento.Size = new System.Drawing.Size(51, 13);
            this.lblElemento.TabIndex = 43;
            this.lblElemento.Text = "Elemento";
            // 
            // rbtElemBase
            // 
            this.rbtElemBase.AutoSize = true;
            this.rbtElemBase.Location = new System.Drawing.Point(182, 19);
            this.rbtElemBase.Name = "rbtElemBase";
            this.rbtElemBase.Size = new System.Drawing.Size(96, 17);
            this.rbtElemBase.TabIndex = 42;
            this.rbtElemBase.TabStop = true;
            this.rbtElemBase.Text = "Elemento Base";
            this.rbtElemBase.UseVisualStyleBackColor = true;
            this.rbtElemBase.CheckedChanged += new System.EventHandler(this.rbtElemBase_CheckedChanged);
            // 
            // rbtComponente
            // 
            this.rbtComponente.AutoSize = true;
            this.rbtComponente.Location = new System.Drawing.Point(16, 19);
            this.rbtComponente.Name = "rbtComponente";
            this.rbtComponente.Size = new System.Drawing.Size(85, 17);
            this.rbtComponente.TabIndex = 41;
            this.rbtComponente.TabStop = true;
            this.rbtComponente.Text = "Componente";
            this.rbtComponente.UseVisualStyleBackColor = true;
            this.rbtComponente.CheckedChanged += new System.EventHandler(this.rbtComponente_CheckedChanged_1);
            // 
            // tbpAlmacenes
            // 
            this.tbpAlmacenes.Controls.Add(this.gpbAlmacenes);
            this.tbpAlmacenes.Location = new System.Drawing.Point(4, 22);
            this.tbpAlmacenes.Name = "tbpAlmacenes";
            this.tbpAlmacenes.Padding = new System.Windows.Forms.Padding(3);
            this.tbpAlmacenes.Size = new System.Drawing.Size(765, 340);
            this.tbpAlmacenes.TabIndex = 3;
            this.tbpAlmacenes.Text = "Almacenes";
            this.tbpAlmacenes.UseVisualStyleBackColor = true;
            this.tbpAlmacenes.Enter += new System.EventHandler(this.tbpAlmacenes_Enter);
            // 
            // gpbAlmacenes
            // 
            this.gpbAlmacenes.Controls.Add(this.dgvProdAlmacen);
            this.gpbAlmacenes.Controls.Add(this.btnAgregar);
            this.gpbAlmacenes.Controls.Add(this.cmbAlmacenes);
            this.gpbAlmacenes.Controls.Add(this.label9);
            this.gpbAlmacenes.Location = new System.Drawing.Point(6, 6);
            this.gpbAlmacenes.Name = "gpbAlmacenes";
            this.gpbAlmacenes.Size = new System.Drawing.Size(753, 328);
            this.gpbAlmacenes.TabIndex = 0;
            this.gpbAlmacenes.TabStop = false;
            // 
            // dgvProdAlmacen
            // 
            this.dgvProdAlmacen.AllowUserToAddRows = false;
            this.dgvProdAlmacen.AllowUserToDeleteRows = false;
            this.dgvProdAlmacen.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProdAlmacen.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.btnAlmacenDel});
            this.dgvProdAlmacen.Location = new System.Drawing.Point(10, 55);
            this.dgvProdAlmacen.Name = "dgvProdAlmacen";
            this.dgvProdAlmacen.ReadOnly = true;
            this.dgvProdAlmacen.Size = new System.Drawing.Size(362, 267);
            this.dgvProdAlmacen.TabIndex = 3;
            this.dgvProdAlmacen.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvProdAlmacen_CellClick);
            this.dgvProdAlmacen.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgvProdAlmacen_CellPainting);
            // 
            // btnAlmacenDel
            // 
            this.btnAlmacenDel.HeaderText = "";
            this.btnAlmacenDel.Name = "btnAlmacenDel";
            this.btnAlmacenDel.ReadOnly = true;
            // 
            // btnAgregar
            // 
            this.btnAgregar.Location = new System.Drawing.Point(297, 20);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(75, 23);
            this.btnAgregar.TabIndex = 2;
            this.btnAgregar.Text = "Agregar";
            this.btnAgregar.UseVisualStyleBackColor = true;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // cmbAlmacenes
            // 
            this.cmbAlmacenes.FormattingEnabled = true;
            this.cmbAlmacenes.Location = new System.Drawing.Point(77, 20);
            this.cmbAlmacenes.Name = "cmbAlmacenes";
            this.cmbAlmacenes.Size = new System.Drawing.Size(189, 21);
            this.cmbAlmacenes.TabIndex = 1;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 24);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(48, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Almacén";
            // 
            // frmProductos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(777, 372);
            this.Controls.Add(this.tbpProductos);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmProductos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Registro de Productos";
            this.Load += new System.EventHandler(this.frmProductos_Load);
            this.tbpProductos.ResumeLayout(false);
            this.tbpRegistro.ResumeLayout(false);
            this.tbpRegistro.PerformLayout();
            this.tbpFormula.ResumeLayout(false);
            this.gpbFormula.ResumeLayout(false);
            this.gpbFormula.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFormula)).EndInit();
            this.tbpAlmacenes.ResumeLayout(false);
            this.gpbAlmacenes.ResumeLayout(false);
            this.gpbAlmacenes.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProdAlmacen)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tbpProductos;
        private System.Windows.Forms.TabPage tbpRegistro;
        private System.Windows.Forms.Button btnAnular;
        private System.Windows.Forms.Button btnGrabar;
        private System.Windows.Forms.Button btnNuevo;
        private System.Windows.Forms.ComboBox cmbFrmFarmac;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cmbClasifTerap;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbLaboratorios;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtPrA_Cod;
        private System.Windows.Forms.TextBox txtPActivo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rbtFMagistral;
        private System.Windows.Forms.RadioButton rbtPTerminado;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtDescripcion;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblPrd_Id;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chkEstado;
        private System.Windows.Forms.ListBox lstPActivos;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.CheckBox chkIgv;
        private System.Windows.Forms.TextBox txtIgv;
        private System.Windows.Forms.Button btnEditar;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.TabPage tbpFormula;
        private System.Windows.Forms.TabPage tbpAlmacenes;
        private System.Windows.Forms.GroupBox gpbFormula;
        private System.Windows.Forms.Label lblUnidad;
        private System.Windows.Forms.TextBox txtCantidad;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnGrabaComp;
        private System.Windows.Forms.DataGridView dgvFormula;
        private System.Windows.Forms.DataGridViewButtonColumn btnDelComponente;
        private System.Windows.Forms.DataGridViewButtonColumn btnUpdComponente;
        private System.Windows.Forms.ComboBox cmbElemento;
        private System.Windows.Forms.Label lblElemento;
        private System.Windows.Forms.RadioButton rbtElemBase;
        private System.Windows.Forms.RadioButton rbtComponente;
        private System.Windows.Forms.CheckBox chkVenta;
        private System.Windows.Forms.GroupBox gpbAlmacenes;
        private System.Windows.Forms.DataGridView dgvProdAlmacen;
        private System.Windows.Forms.DataGridViewButtonColumn btnAlmacenDel;
        private System.Windows.Forms.Button btnAgregar;
        private System.Windows.Forms.ComboBox cmbAlmacenes;
        private System.Windows.Forms.Label label9;
    }
}