﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pjDermaBelle
{
    public partial class frmMantPrincipiosActivos : Form
    {
        public frmMantPrincipiosActivos()
        {
            InitializeComponent();
        }

        ClaseDatos obj = new ClaseDatos();
        DermaBelleDataDataContext linq = new DermaBelleDataDataContext();
        PrincipiosActivos oPrincipiosActivos = new PrincipiosActivos();

        bool nuevo = true;

        private void Limpiar()
        {
            txtId.Clear();
            txtNombre.Clear();

            rbFarmacia.Checked = false;
            rbFormMagistral.Checked = false;
            rbElemBase.Checked = false;
            chkEstado.Checked = false;
            txtId.Enabled = false;
            txtNombre.Enabled = false;
            
            btnNuevo.Enabled = true;
            btnGrabar.Enabled = false;
            btnActualizar.Enabled = false;
            btnAnular.Enabled = false;
            btnCerrar.Enabled = true;

            lblCreado.Visible = false;
            lblFecCrea.Visible = false;
            lblModificado.Visible = false;
            lblFecMod.Visible = false;

            btnNuevo.Focus();
        }

        void CargarLista()
        {
            dgvLista.DataSource = obj.PrincipiosActivosListar();

            for (int indice = 0; indice < dgvLista.Rows.Count; indice++)
            {
                dgvLista.Columns[0].Width = 50;         //Código
                dgvLista.Columns[1].Width = 480;        //Principio Activo
                dgvLista.Columns[2].Visible = false;    //Presentación
                dgvLista.Columns[3].Width = 50;         //Tipo
                dgvLista.Columns[4].Width = 50;         //Estado

                dgvLista.Rows[indice].ReadOnly = true;
            }
        }


        private void frmMantPrincipiosActivos_Load(object sender, EventArgs e)
        {
            Limpiar();
            CargarLista();
        }

        private void txtNombre_Leave(object sender, EventArgs e)
        {
            if (nuevo)
            {
                btnGrabar.Focus();
            }
            else
            { btnActualizar.Focus(); }
        }

        private void txtNombre_TextChanged(object sender, EventArgs e)
        {
            btnNuevo.Enabled = false;
            if (nuevo)
            {
                btnGrabar.Enabled = txtNombre.Text != string.Empty;
                btnAnular.Enabled = false;
            }
            else
            {
                btnGrabar.Enabled = false;
                btnActualizar.Enabled = true;
                btnAnular.Enabled = true;
            }
            btnCerrar.Enabled = true;
        }

        private void dgvLista_SelectionChanged(object sender, EventArgs e)
        {
            int indice = dgvLista.CurrentCell.RowIndex;
            int PrA_Id = int.Parse(dgvLista.Rows[indice].Cells[0].Value.ToString());

            List<usp_PrincipiosActivosRecResult> lista = new List<usp_PrincipiosActivosRecResult>();
            lista = obj.PrincipiosActivosDatos(PrA_Id);
            if (lista.Count() > 0)
            {
                foreach (usp_PrincipiosActivosRecResult reg in lista)
                {
                    txtId.Text = reg.PrA_Id.ToString();
                    txtNombre.Text = reg.PrA_Nombre;
                    if (reg.PrA_Tipo == "FA")
                    { rbFarmacia.Checked = true; }

                    if (reg.PrA_Tipo == "FM")
                    { rbFormMagistral.Checked = true; }

                    if (reg.PrA_Tipo == "EB")
                    { rbElemBase.Checked = true; }

                    lblFecCrea.Text = reg.FecCrea.ToString();
                    lblFecMod.Text = reg.FecModi.ToString();
                    chkEstado.Checked = reg.Estado;

                    if (chkEstado.Checked)
                    { btnAnular.Text = "De&sactivar"; }
                    else
                    { btnAnular.Text = "A&ctivar"; }

                    txtNombre.Enabled = true;
                    rbFarmacia.Enabled = true;
                    rbFormMagistral.Enabled = true;
                    rbElemBase.Enabled = true;

                    btnNuevo.Enabled = true;
                    btnGrabar.Enabled = false;
                    btnActualizar.Enabled = true;
                    btnAnular.Enabled = true;
                    btnCerrar.Enabled = true;

                    lblCreado.Visible = true;
                    lblFecCrea.Visible = true;
                    lblModificado.Visible = true;
                    lblFecMod.Visible = true;

                    nuevo = false;
                    txtNombre.Focus();
                }
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Limpiar();

            txtId.Enabled = false;
            txtNombre.Enabled = true;
            chkEstado.Checked = true;

            btnNuevo.Enabled = false;
            btnGrabar.Enabled = false;
            btnActualizar.Enabled = false;
            btnAnular.Enabled = false;
            btnCerrar.Enabled = true;

            txtNombre.Focus();
            nuevo = true;
        }

        private void btnGrabar_Click(object sender, EventArgs e)
        {
            oPrincipiosActivos.PrA_Nombre = txtNombre.Text.ToUpper();
            if (rbFarmacia.Checked)
            { oPrincipiosActivos.PrA_Tipo = "FA"; }
            if (rbFormMagistral.Checked)
            { oPrincipiosActivos.PrA_Tipo = "FM"; }
            if (rbElemBase.Checked)
            { oPrincipiosActivos.PrA_Tipo = "EB"; }
            oPrincipiosActivos.Estado = chkEstado.Checked;
            oPrincipiosActivos.UsuCrea = 1;
            oPrincipiosActivos.UsuModi = 1;
            obj.PrincipiosActivosAdicionar(oPrincipiosActivos);
            MessageBox.Show("Registro GRABADO satisfactoriamente", "AVISO");
            Limpiar();
            CargarLista();
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            oPrincipiosActivos.PrA_Id = Convert.ToInt16(txtId.Text);
            oPrincipiosActivos.PrA_Nombre = txtNombre.Text.ToUpper();
            if (rbFarmacia.Checked)
            { oPrincipiosActivos.PrA_Tipo = "FA"; }
            if (rbFormMagistral.Checked)
            { oPrincipiosActivos.PrA_Tipo = "FM"; }
            if (rbElemBase.Checked)
            { oPrincipiosActivos.PrA_Tipo = "EB"; }
            oPrincipiosActivos.UsuModi = 1;
            obj.PrincipiosActivosActualizar(oPrincipiosActivos);
            Limpiar();
            MessageBox.Show("Registro ACTUALIZADO satisfactoriamente", "AVISO");
            CargarLista();
        }

        private void btnAnular_Click(object sender, EventArgs e)
        {
            oPrincipiosActivos.PrA_Id = Convert.ToInt16(txtId.Text);
            oPrincipiosActivos.Estado = !chkEstado.Checked;
            oPrincipiosActivos.UsuModi = 1;
            obj.PrincipiosActivosAnular(oPrincipiosActivos);
            if (oPrincipiosActivos.Estado)
            { MessageBox.Show("Registro ACTIVADO satisfactoriamente", "AVISO"); }
            else
            { MessageBox.Show("Registro DESACTIVADO satisfactoriamente", "AVISO"); }
            Limpiar();
            CargarLista();
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            if ((MessageBox.Show("¿Seguro que desea salir ?", "Aviso",
                     MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                     MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes))
            {
                this.Close();
            }
        }
    }
}
