﻿namespace pjDermaBelle
{
    partial class frmMantProfSalud
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tbpRegistro = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pbxMedFirma = new System.Windows.Forms.PictureBox();
            this.btnCerrar = new System.Windows.Forms.Button();
            this.btnAD = new System.Windows.Forms.Button();
            this.btnGrabar = new System.Windows.Forms.Button();
            this.btnNuevo = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.cmbMedDistrito = new System.Windows.Forms.ComboBox();
            this.txtMedDireccion = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtMedTelefonos = new System.Windows.Forms.TextBox();
            this.txtMedEmail = new System.Windows.Forms.TextBox();
            this.dtpMedFIngreso = new System.Windows.Forms.DateTimePicker();
            this.txtMedRNE = new System.Windows.Forms.TextBox();
            this.cmbMedEspecialidad = new System.Windows.Forms.ComboBox();
            this.txtMedNumCol = new System.Windows.Forms.TextBox();
            this.txtMedNomComp = new System.Windows.Forms.TextBox();
            this.txtMedNombres = new System.Windows.Forms.TextBox();
            this.txtMedApMat = new System.Windows.Forms.TextBox();
            this.txtMedApPat = new System.Windows.Forms.TextBox();
            this.txtMedId = new System.Windows.Forms.TextBox();
            this.chkEstado = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbpListado = new System.Windows.Forms.TabPage();
            this.lsbMedicos = new System.Windows.Forms.ListBox();
            this.dgvMedicos = new System.Windows.Forms.DataGridView();
            this.txtBuscarMedico = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.btnNuevoReg = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tbpRegistro.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxMedFirma)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tbpListado.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMedicos)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(173, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(272, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Registro de Profesionales de la Salud";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tbpRegistro);
            this.tabControl1.Controls.Add(this.tbpListado);
            this.tabControl1.Location = new System.Drawing.Point(5, 40);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(670, 586);
            this.tabControl1.TabIndex = 1;
            // 
            // tbpRegistro
            // 
            this.tbpRegistro.Controls.Add(this.label19);
            this.tbpRegistro.Controls.Add(this.label18);
            this.tbpRegistro.Controls.Add(this.label17);
            this.tbpRegistro.Controls.Add(this.label16);
            this.tbpRegistro.Controls.Add(this.label10);
            this.tbpRegistro.Controls.Add(this.groupBox2);
            this.tbpRegistro.Controls.Add(this.btnCerrar);
            this.tbpRegistro.Controls.Add(this.btnAD);
            this.tbpRegistro.Controls.Add(this.btnGrabar);
            this.tbpRegistro.Controls.Add(this.btnNuevo);
            this.tbpRegistro.Controls.Add(this.groupBox1);
            this.tbpRegistro.Controls.Add(this.dtpMedFIngreso);
            this.tbpRegistro.Controls.Add(this.txtMedRNE);
            this.tbpRegistro.Controls.Add(this.cmbMedEspecialidad);
            this.tbpRegistro.Controls.Add(this.txtMedNumCol);
            this.tbpRegistro.Controls.Add(this.txtMedNomComp);
            this.tbpRegistro.Controls.Add(this.txtMedNombres);
            this.tbpRegistro.Controls.Add(this.txtMedApMat);
            this.tbpRegistro.Controls.Add(this.txtMedApPat);
            this.tbpRegistro.Controls.Add(this.txtMedId);
            this.tbpRegistro.Controls.Add(this.chkEstado);
            this.tbpRegistro.Controls.Add(this.label9);
            this.tbpRegistro.Controls.Add(this.label8);
            this.tbpRegistro.Controls.Add(this.label7);
            this.tbpRegistro.Controls.Add(this.label6);
            this.tbpRegistro.Controls.Add(this.label5);
            this.tbpRegistro.Controls.Add(this.label4);
            this.tbpRegistro.Controls.Add(this.label3);
            this.tbpRegistro.Controls.Add(this.label2);
            this.tbpRegistro.Location = new System.Drawing.Point(4, 22);
            this.tbpRegistro.Name = "tbpRegistro";
            this.tbpRegistro.Padding = new System.Windows.Forms.Padding(3);
            this.tbpRegistro.Size = new System.Drawing.Size(662, 560);
            this.tbpRegistro.TabIndex = 0;
            this.tbpRegistro.Text = "Registro";
            this.tbpRegistro.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnBuscar);
            this.groupBox2.Controls.Add(this.pbxMedFirma);
            this.groupBox2.Location = new System.Drawing.Point(18, 393);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(619, 125);
            this.groupBox2.TabIndex = 31;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Sello y Firma del Médico";
            // 
            // pbxMedFirma
            // 
            this.pbxMedFirma.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbxMedFirma.Location = new System.Drawing.Point(6, 19);
            this.pbxMedFirma.Name = "pbxMedFirma";
            this.pbxMedFirma.Size = new System.Drawing.Size(524, 100);
            this.pbxMedFirma.TabIndex = 0;
            this.pbxMedFirma.TabStop = false;
            // 
            // btnCerrar
            // 
            this.btnCerrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCerrar.Location = new System.Drawing.Point(540, 527);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(75, 23);
            this.btnCerrar.TabIndex = 35;
            this.btnCerrar.Text = "&Cerrar";
            this.btnCerrar.UseVisualStyleBackColor = true;
            // 
            // btnAD
            // 
            this.btnAD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAD.Location = new System.Drawing.Point(367, 527);
            this.btnAD.Name = "btnAD";
            this.btnAD.Size = new System.Drawing.Size(75, 23);
            this.btnAD.TabIndex = 34;
            this.btnAD.Text = "De&sactivar";
            this.btnAD.UseVisualStyleBackColor = true;
            this.btnAD.Click += new System.EventHandler(this.btnAD_Click);
            // 
            // btnGrabar
            // 
            this.btnGrabar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGrabar.Location = new System.Drawing.Point(194, 527);
            this.btnGrabar.Name = "btnGrabar";
            this.btnGrabar.Size = new System.Drawing.Size(75, 23);
            this.btnGrabar.TabIndex = 32;
            this.btnGrabar.Text = "&Grabar";
            this.btnGrabar.UseVisualStyleBackColor = true;
            this.btnGrabar.Click += new System.EventHandler(this.btnGrabar_Click);
            // 
            // btnNuevo
            // 
            this.btnNuevo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNuevo.Location = new System.Drawing.Point(21, 527);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(75, 23);
            this.btnNuevo.TabIndex = 31;
            this.btnNuevo.Text = "&Nuevo";
            this.btnNuevo.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.cmbMedDistrito);
            this.groupBox1.Controls.Add(this.txtMedDireccion);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.txtMedTelefonos);
            this.groupBox1.Controls.Add(this.txtMedEmail);
            this.groupBox1.Location = new System.Drawing.Point(18, 255);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(619, 132);
            this.groupBox1.TabIndex = 24;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos Generales";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(11, 22);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(52, 13);
            this.label12.TabIndex = 23;
            this.label12.Text = "Dirección";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(9, 76);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(54, 13);
            this.label13.TabIndex = 24;
            this.label13.Text = "Teléfonos";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(29, 105);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(34, 13);
            this.label14.TabIndex = 25;
            this.label14.Text = "e-mail";
            // 
            // cmbMedDistrito
            // 
            this.cmbMedDistrito.FormattingEnabled = true;
            this.cmbMedDistrito.Items.AddRange(new object[] {
            "Masculino",
            "Femenino"});
            this.cmbMedDistrito.Location = new System.Drawing.Point(69, 45);
            this.cmbMedDistrito.Name = "cmbMedDistrito";
            this.cmbMedDistrito.Size = new System.Drawing.Size(300, 21);
            this.cmbMedDistrito.TabIndex = 27;
            // 
            // txtMedDireccion
            // 
            this.txtMedDireccion.Location = new System.Drawing.Point(69, 19);
            this.txtMedDireccion.Name = "txtMedDireccion";
            this.txtMedDireccion.Size = new System.Drawing.Size(542, 20);
            this.txtMedDireccion.TabIndex = 26;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(24, 49);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(39, 13);
            this.label15.TabIndex = 29;
            this.label15.Text = "Distrito";
            // 
            // txtMedTelefonos
            // 
            this.txtMedTelefonos.Location = new System.Drawing.Point(69, 72);
            this.txtMedTelefonos.Name = "txtMedTelefonos";
            this.txtMedTelefonos.Size = new System.Drawing.Size(200, 20);
            this.txtMedTelefonos.TabIndex = 28;
            // 
            // txtMedEmail
            // 
            this.txtMedEmail.Location = new System.Drawing.Point(69, 100);
            this.txtMedEmail.Name = "txtMedEmail";
            this.txtMedEmail.Size = new System.Drawing.Size(542, 20);
            this.txtMedEmail.TabIndex = 29;
            // 
            // dtpMedFIngreso
            // 
            this.dtpMedFIngreso.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpMedFIngreso.Location = new System.Drawing.Point(119, 220);
            this.dtpMedFIngreso.Name = "dtpMedFIngreso";
            this.dtpMedFIngreso.Size = new System.Drawing.Size(99, 20);
            this.dtpMedFIngreso.TabIndex = 23;
            // 
            // txtMedRNE
            // 
            this.txtMedRNE.Location = new System.Drawing.Point(545, 184);
            this.txtMedRNE.Name = "txtMedRNE";
            this.txtMedRNE.Size = new System.Drawing.Size(84, 20);
            this.txtMedRNE.TabIndex = 22;
            // 
            // cmbMedEspecialidad
            // 
            this.cmbMedEspecialidad.FormattingEnabled = true;
            this.cmbMedEspecialidad.Location = new System.Drawing.Point(119, 184);
            this.cmbMedEspecialidad.Name = "cmbMedEspecialidad";
            this.cmbMedEspecialidad.Size = new System.Drawing.Size(268, 21);
            this.cmbMedEspecialidad.TabIndex = 21;
            // 
            // txtMedNumCol
            // 
            this.txtMedNumCol.Location = new System.Drawing.Point(119, 149);
            this.txtMedNumCol.Name = "txtMedNumCol";
            this.txtMedNumCol.Size = new System.Drawing.Size(84, 20);
            this.txtMedNumCol.TabIndex = 20;
            // 
            // txtMedNomComp
            // 
            this.txtMedNomComp.Enabled = false;
            this.txtMedNomComp.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtMedNomComp.Location = new System.Drawing.Point(182, 9);
            this.txtMedNomComp.Name = "txtMedNomComp";
            this.txtMedNomComp.Size = new System.Drawing.Size(447, 20);
            this.txtMedNomComp.TabIndex = 19;
            // 
            // txtMedNombres
            // 
            this.txtMedNombres.Location = new System.Drawing.Point(119, 114);
            this.txtMedNombres.Name = "txtMedNombres";
            this.txtMedNombres.Size = new System.Drawing.Size(323, 20);
            this.txtMedNombres.TabIndex = 18;
            this.txtMedNombres.TextChanged += new System.EventHandler(this.txtMedNombres_TextChanged);
            // 
            // txtMedApMat
            // 
            this.txtMedApMat.Location = new System.Drawing.Point(119, 79);
            this.txtMedApMat.Name = "txtMedApMat";
            this.txtMedApMat.Size = new System.Drawing.Size(323, 20);
            this.txtMedApMat.TabIndex = 17;
            this.txtMedApMat.TextChanged += new System.EventHandler(this.txtMedApMat_TextChanged);
            // 
            // txtMedApPat
            // 
            this.txtMedApPat.Location = new System.Drawing.Point(119, 44);
            this.txtMedApPat.Name = "txtMedApPat";
            this.txtMedApPat.Size = new System.Drawing.Size(323, 20);
            this.txtMedApPat.TabIndex = 16;
            this.txtMedApPat.TextChanged += new System.EventHandler(this.txtMedApPat_TextChanged);
            // 
            // txtMedId
            // 
            this.txtMedId.Enabled = false;
            this.txtMedId.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtMedId.Location = new System.Drawing.Point(119, 9);
            this.txtMedId.Name = "txtMedId";
            this.txtMedId.Size = new System.Drawing.Size(52, 20);
            this.txtMedId.TabIndex = 15;
            // 
            // chkEstado
            // 
            this.chkEstado.AutoSize = true;
            this.chkEstado.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkEstado.Location = new System.Drawing.Point(500, 222);
            this.chkEstado.Name = "chkEstado";
            this.chkEstado.Size = new System.Drawing.Size(56, 17);
            this.chkEstado.TabIndex = 14;
            this.chkEstado.Text = "Activo";
            this.chkEstado.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(26, 224);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(90, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "Fecha de Ingreso";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(497, 188);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(39, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "R.N.E.";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(49, 188);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Especialidad";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(21, 153);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "No. de Colegiatura";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(67, 118);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Nombres";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(30, 83);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Apellido Materno";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(32, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Apellido Paterno";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Código Identificador";
            // 
            // tbpListado
            // 
            this.tbpListado.Controls.Add(this.btnNuevoReg);
            this.tbpListado.Controls.Add(this.lsbMedicos);
            this.tbpListado.Controls.Add(this.dgvMedicos);
            this.tbpListado.Controls.Add(this.txtBuscarMedico);
            this.tbpListado.Controls.Add(this.label11);
            this.tbpListado.Location = new System.Drawing.Point(4, 22);
            this.tbpListado.Name = "tbpListado";
            this.tbpListado.Padding = new System.Windows.Forms.Padding(3);
            this.tbpListado.Size = new System.Drawing.Size(662, 560);
            this.tbpListado.TabIndex = 1;
            this.tbpListado.Text = "Listado";
            this.tbpListado.UseVisualStyleBackColor = true;
            // 
            // lsbMedicos
            // 
            this.lsbMedicos.FormattingEnabled = true;
            this.lsbMedicos.Location = new System.Drawing.Point(109, 38);
            this.lsbMedicos.Name = "lsbMedicos";
            this.lsbMedicos.Size = new System.Drawing.Size(407, 199);
            this.lsbMedicos.TabIndex = 25;
            // 
            // dgvMedicos
            // 
            this.dgvMedicos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMedicos.Location = new System.Drawing.Point(9, 51);
            this.dgvMedicos.Name = "dgvMedicos";
            this.dgvMedicos.Size = new System.Drawing.Size(644, 503);
            this.dgvMedicos.TabIndex = 24;
            this.dgvMedicos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMedicos_CellClick);
            // 
            // txtBuscarMedico
            // 
            this.txtBuscarMedico.Location = new System.Drawing.Point(109, 17);
            this.txtBuscarMedico.Name = "txtBuscarMedico";
            this.txtBuscarMedico.Size = new System.Drawing.Size(407, 20);
            this.txtBuscarMedico.TabIndex = 23;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(15, 21);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(91, 13);
            this.label11.TabIndex = 21;
            this.label11.Text = "Nombre Completo";
            // 
            // btnNuevoReg
            // 
            this.btnNuevoReg.Location = new System.Drawing.Point(578, 17);
            this.btnNuevoReg.Name = "btnNuevoReg";
            this.btnNuevoReg.Size = new System.Drawing.Size(75, 23);
            this.btnNuevoReg.TabIndex = 26;
            this.btnNuevoReg.Text = "Nuevo";
            this.btnNuevoReg.UseVisualStyleBackColor = true;
            this.btnNuevoReg.Click += new System.EventHandler(this.btnNuevoReg_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(8, 48);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(13, 17);
            this.label10.TabIndex = 40;
            this.label10.Text = "*";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(8, 83);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(13, 17);
            this.label16.TabIndex = 41;
            this.label16.Text = "*";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Location = new System.Drawing.Point(8, 118);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(13, 17);
            this.label17.TabIndex = 42;
            this.label17.Text = "*";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Red;
            this.label18.Location = new System.Drawing.Point(8, 153);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(13, 17);
            this.label18.TabIndex = 43;
            this.label18.Text = "*";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Red;
            this.label19.Location = new System.Drawing.Point(8, 184);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(13, 17);
            this.label19.TabIndex = 44;
            this.label19.Text = "*";
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(536, 19);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(75, 100);
            this.btnBuscar.TabIndex = 30;
            this.btnBuscar.Text = "&Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            // 
            // frmMantProfSalud
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(681, 632);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.label1);
            this.Name = "frmMantProfSalud";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mantenimiento de Profesionales de la Salud";
            this.Load += new System.EventHandler(this.frmMantProfSalud_Load);
            this.tabControl1.ResumeLayout(false);
            this.tbpRegistro.ResumeLayout(false);
            this.tbpRegistro.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbxMedFirma)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tbpListado.ResumeLayout(false);
            this.tbpListado.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMedicos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tbpRegistro;
        private System.Windows.Forms.TextBox txtMedApPat;
        private System.Windows.Forms.TextBox txtMedId;
        private System.Windows.Forms.CheckBox chkEstado;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage tbpListado;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DateTimePicker dtpMedFIngreso;
        private System.Windows.Forms.TextBox txtMedRNE;
        private System.Windows.Forms.ComboBox cmbMedEspecialidad;
        private System.Windows.Forms.TextBox txtMedNumCol;
        private System.Windows.Forms.TextBox txtMedNomComp;
        private System.Windows.Forms.TextBox txtMedNombres;
        private System.Windows.Forms.TextBox txtMedApMat;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cmbMedDistrito;
        private System.Windows.Forms.TextBox txtMedDireccion;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtMedTelefonos;
        private System.Windows.Forms.TextBox txtMedEmail;
        private System.Windows.Forms.Button btnCerrar;
        private System.Windows.Forms.Button btnAD;
        private System.Windows.Forms.Button btnGrabar;
        private System.Windows.Forms.Button btnNuevo;
        private System.Windows.Forms.DataGridView dgvMedicos;
        private System.Windows.Forms.TextBox txtBuscarMedico;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ListBox lsbMedicos;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox pbxMedFirma;
        private System.Windows.Forms.Button btnNuevoReg;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnBuscar;
    }
}