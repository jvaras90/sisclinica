﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pjDermaBelle
{
    public partial class frmMantTiposTarifa : Form
    {
        public frmMantTiposTarifa()
        {
            InitializeComponent();
        }

        ClaseDatos obj = new ClaseDatos();
        DermaBelleDataDataContext linq = new DermaBelleDataDataContext();
        TiposTarifa oTiposTarifa = new TiposTarifa();

        bool nuevo = true;

        private void Limpiar()
        {
            txtCodigo.Clear();
            txtNombre.Clear();
            chkEstado.Checked = false;
            txtCodigo.Enabled = false;
            txtNombre.Enabled = false;
            btnNuevo.Enabled = true;
            btnGrabar.Enabled = false;
            btnActualizar.Enabled = false;
            btnAnular.Enabled = false;
            btnCerrar.Enabled = true;

            lblCreado.Visible = false;
            lblFecCrea.Visible = false;
            lblModificado.Visible = false;
            lblFecMod.Visible = false;

            btnNuevo.Focus();
        }

        void CargarLista()
        {
            dgvLista.DataSource = obj.TiposTarifaListar();

            for (int indice = 0; indice < dgvLista.Rows.Count; indice++)
            {
                dgvLista.Columns[0].Width = 50;     //Código
                dgvLista.Columns[1].Width = 350;    //Documento de Identidad
                dgvLista.Columns[2].Width = 50;     //Estado

                dgvLista.Rows[indice].ReadOnly = true;
            }
        }
        

        private void frmMantTiposTarifa_Load(object sender, EventArgs e)
        {
            Limpiar();
            CargarLista();
        }

        private void txtNombre_Leave(object sender, EventArgs e)
        {
            if (nuevo)
            {
                btnGrabar.Focus();
            }
            else
            { btnActualizar.Focus(); }
        }

        private void txtNombre_TextChanged(object sender, EventArgs e)
        {
            btnNuevo.Enabled = false;
            if (nuevo)
            {
                btnGrabar.Enabled = txtCodigo.Text != string.Empty &&
                     txtNombre.Text != string.Empty;
                btnAnular.Enabled = false;
            }
            else
            {
                btnGrabar.Enabled = false;
                btnActualizar.Enabled = true;
                btnAnular.Enabled = true;
            }
            btnCerrar.Enabled = true;
        }

        private void dgvLista_SelectionChanged(object sender, EventArgs e)
        {
            int indice = dgvLista.CurrentCell.RowIndex;
            string TTr_Codigo = dgvLista.Rows[indice].Cells[0].Value.ToString();

            List<usp_TiposTarifaRecResult> lista = new List<usp_TiposTarifaRecResult>();
            lista = obj.TiposTarifaDatos(TTr_Codigo);
            if (lista.Count() > 0)
            {
                foreach (usp_TiposTarifaRecResult reg in lista)
                {
                    txtCodigo.Text = reg.TTr_Codigo;
                    txtNombre.Text = reg.TTr_Nombre;
                    lblFecCrea.Text = reg.FecCrea.ToString();
                    lblFecMod.Text = reg.FecModi.ToString();
                    chkEstado.Checked = reg.Estado;

                    if (chkEstado.Checked)
                    { btnAnular.Text = "De&sactivar"; }
                    else
                    { btnAnular.Text = "A&ctivar"; }

                    txtNombre.Enabled = true;
                    btnNuevo.Enabled = true;
                    btnGrabar.Enabled = false;
                    btnActualizar.Enabled = true;
                    btnAnular.Enabled = true;
                    btnCerrar.Enabled = true;

                    lblCreado.Visible = true;
                    lblFecCrea.Visible = true;
                    lblModificado.Visible = true;
                    lblFecMod.Visible = true;

                    nuevo = false;
                    txtNombre.Focus();
                }
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Limpiar();

            txtCodigo.Enabled = true;
            txtNombre.Enabled = true;
            chkEstado.Checked = true;
            btnNuevo.Enabled = false;
            btnGrabar.Enabled = false;
            btnActualizar.Enabled = false;
            btnAnular.Enabled = false;
            btnCerrar.Enabled = true;

            txtCodigo.Focus();
            nuevo = true;
        }

        private void btnGrabar_Click(object sender, EventArgs e)
        {
            oTiposTarifa.TTr_Codigo = txtCodigo.Text.ToUpper();
            oTiposTarifa.TTr_Nombre = txtNombre.Text.ToUpper();
            oTiposTarifa.Estado = chkEstado.Checked;
            oTiposTarifa.UsuCrea = 1;
            oTiposTarifa.UsuModi = 1;
            obj.TiposTarifaAdicionar(oTiposTarifa);
            Limpiar();
            MessageBox.Show("Registro GRABADO satisfactoriamente", "AVISO");
            CargarLista();
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            oTiposTarifa.TTr_Codigo = txtCodigo.Text.ToUpper();
            oTiposTarifa.TTr_Nombre = txtNombre.Text.ToUpper();
            oTiposTarifa.UsuModi = 1;
            obj.TiposTarifaActualizar(oTiposTarifa);
            Limpiar();
            MessageBox.Show("Registro ACTUALIZADO satisfactoriamente", "AVISO");
            CargarLista();
        }

        private void btnAnular_Click(object sender, EventArgs e)
        {
            oTiposTarifa.TTr_Codigo = txtCodigo.Text;
            oTiposTarifa.TTr_Nombre = txtNombre.Text;
            chkEstado.Checked = !chkEstado.Checked;
            oTiposTarifa.Estado = chkEstado.Checked;
            oTiposTarifa.UsuModi = 1;
            obj.TiposTarifaAnular(oTiposTarifa);
            if (chkEstado.Checked)
            { MessageBox.Show("Registro ACTIVADO satisfactoriamente", "AVISO"); }
            else
            { MessageBox.Show("Registro DESACTIVADO satisfactoriamente", "AVISO"); }
            Limpiar();
            CargarLista();
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            if ((MessageBox.Show("¿Seguro que desea salir ?", "Aviso",
                     MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                     MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes))
            {
                this.Close();
            }
        }
    }
}
