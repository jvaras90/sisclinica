﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pjDermaBelle
{
    public partial class frmMantGradosInstruccion : Form
    {
        public frmMantGradosInstruccion()
        {
            InitializeComponent();
        }

        ClaseDatos obj = new ClaseDatos();
        DermaBelleDataDataContext linq = new DermaBelleDataDataContext();
        GradosInstruccion oGrados = new GradosInstruccion();

        bool nuevo = true;

        private void Limpiar()
        {
            txtId.Clear();
            txtNombre.Clear();
            chkEstado.Checked = false;
            txtId.Enabled = false;
            txtNombre.Enabled = false;
            btnNuevo.Enabled = true;
            btnGrabar.Enabled = false;
            btnActualizar.Enabled = false;
            btnAnular.Enabled = false;
            btnCerrar.Enabled = true;

            lblCreado.Visible = false;
            lblFecCrea.Visible = false;
            lblModificado.Visible = false;
            lblFecMod.Visible = false;

            btnNuevo.Focus();
        }

        private void frmMantGradosInstruccion_Load(object sender, EventArgs e)
        {
            lblFecha.Text = obj.Fecha;
            //lblHora.Text = obj.Hora; 
            Limpiar();
            CargarLista();
        }

        void CargarLista()
        {
            dgvLista.DataSource = obj.GradosListar();

            for (int indice = 0; indice < dgvLista.Rows.Count; indice++)
            {
                dgvLista.Columns[0].Width = 50;     //Id
                dgvLista.Columns[1].Width = 300;    //Grado de Instrucción
                dgvLista.Columns[2].Width = 50;    //Estado

                dgvLista.Rows[indice].ReadOnly = true;
            }
        }

        private void dgvLista_SelectionChanged(object sender, EventArgs e)
        {
            int indice = dgvLista.CurrentCell.RowIndex;
            int GrI_Id = int.Parse(dgvLista.Rows[indice].Cells[0].Value.ToString());

            List<usp_GradosInstruccionRecResult> lista = new List<usp_GradosInstruccionRecResult>();
            lista = obj.GradosDatos(GrI_Id);
            if (lista.Count() > 0)
            {
                foreach (usp_GradosInstruccionRecResult reg in lista)
                {
                    txtId.Text = reg.GrI_Id.ToString();
                    txtNombre.Text = reg.GrI_Nombre;
                    lblFecCrea.Text = reg.FecCrea.ToString();
                    lblFecMod.Text = reg.FecModi.ToString();
                    chkEstado.Checked = reg.Estado;

                    if (chkEstado.Checked)
                    { btnAnular.Text = "A&nular"; }
                    else
                    { btnAnular.Text = "A&ctivar"; }

                    txtNombre.Enabled = true;
                    btnNuevo.Enabled = true;
                    btnGrabar.Enabled = false;
                    btnActualizar.Enabled = true;
                    btnAnular.Enabled = true;
                    btnCerrar.Enabled = true;

                    lblCreado.Visible = true;
                    lblFecCrea.Visible = true;
                    lblModificado.Visible = true;
                    lblFecMod.Visible = true;

                    nuevo = false;
                    txtNombre.Focus();
                }
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Limpiar();
            lblFecCrea.Text = lblFecha.Text;

            txtId.Enabled = false;
            txtNombre.Enabled = true;
            chkEstado.Checked = true;
            btnNuevo.Enabled = false;
            btnGrabar.Enabled = false;
            btnActualizar.Enabled = false;
            btnAnular.Enabled = false;
            btnCerrar.Enabled = true;

            txtNombre.Focus();
            nuevo = true;
        }

        private void btnGrabar_Click(object sender, EventArgs e)
        {
            oGrados.GrI_Nombre = txtNombre.Text.ToUpper();
            oGrados.Estado = chkEstado.Checked;
            oGrados.UsuCrea = 1;
            oGrados.UsuModi = 1;
            obj.GradosAdicionar(oGrados);
            Limpiar();
            MessageBox.Show("Registro GRABADO satisfactoriamente", "AVISO");
            CargarLista();
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            oGrados.GrI_Id = Convert.ToInt16(txtId.Text);
            oGrados.GrI_Nombre = txtNombre.Text.ToUpper();
            oGrados.UsuModi = 1;
            obj.GradosActualizar(oGrados);
            Limpiar();
            MessageBox.Show("Registro ACTUALIZADO satisfactoriamente", "AVISO");
            CargarLista();
        }

        private void btnAnular_Click(object sender, EventArgs e)
        {
            oGrados.GrI_Id = Convert.ToInt16(txtId.Text);
            oGrados.GrI_Nombre = txtNombre.Text;
            oGrados.Estado = !chkEstado.Checked;
            oGrados.UsuModi = 1;
            obj.GradosAnular(oGrados);
            Limpiar();
            if (oGrados.Estado)
            { MessageBox.Show("Registro ACTIVADO satisfactoriamente", "AVISO"); }
            else
            { MessageBox.Show("Registro ANULADO satisfactoriamente", "AVISO"); }
            CargarLista();
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            if ((MessageBox.Show("¿Seguro que desea salir ?", "Aviso",
                     MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                     MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes))
            {
                this.Close();
            }
        }

        private void txtNombre_TextChanged(object sender, EventArgs e)
        {
            btnNuevo.Enabled = false;
            if (nuevo)
            {
                btnGrabar.Enabled = txtNombre.Text != string.Empty;
                btnAnular.Enabled = false;
            }
            else
            {
                btnGrabar.Enabled = false;
                btnActualizar.Enabled = true;
                btnAnular.Enabled = true;
            }
            btnCerrar.Enabled = true;
        }

        private void txtNombre_Leave(object sender, EventArgs e)
        {
            if (nuevo)
            {
                btnGrabar.Focus();
            }
            else
            { btnActualizar.Focus(); }
        }
    }
}
