﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pjDermaBelle
{
    public partial class frmMantDocIdentidad : Form
    {
        public frmMantDocIdentidad()
        {
            InitializeComponent();
        }

        ClaseDatos obj = new ClaseDatos();
        DermaBelleDataDataContext linq = new DermaBelleDataDataContext();
        DocIdentidad oDocumentos = new DocIdentidad();

        bool nuevo = true;

        private void Limpiar()
        {
            txtId.Clear();
            txtNombre.Clear();
            chkEstado.Checked = false;
            txtId.Enabled = false;
            txtNombre.Enabled = false;
            btnNuevo.Enabled = true;
            btnGrabar.Enabled = false;
            btnActualizar.Enabled = false;
            btnAnular.Enabled = false;
            btnCerrar.Enabled = true;

            lblCreado.Visible = false;
            lblFecCrea.Visible = false;
            lblModificado.Visible = false;
            lblFecMod.Visible = false;

            btnNuevo.Focus();
        }

        void CargarLista()
        {
            dgvLista.DataSource = obj.DocIdentidadListar();

            for (int indice = 0; indice < dgvLista.Rows.Count; indice++)
            {
                dgvLista.Columns[0].Width = 50;     //Código
                dgvLista.Columns[1].Width = 327;    //Documento de Identidad
                dgvLista.Columns[2].Width = 50;     //Estado

                dgvLista.Rows[indice].ReadOnly = true;
            }
        }

        private void frmMantDocIdentidad_Load(object sender, EventArgs e)
        {
            Limpiar();
            CargarLista();
        }

        private void txtNombre_Leave(object sender, EventArgs e)
        {
            if (nuevo)
            {
                btnGrabar.Focus();
            }
            else
            { btnActualizar.Focus(); }
        }

        private void txtNombre_TextChanged(object sender, EventArgs e)
        {
            btnNuevo.Enabled = false;
            if (nuevo)
            {
                btnGrabar.Enabled = txtNombre.Text != string.Empty;
                btnAnular.Enabled = false;
            }
            else
            {
                btnGrabar.Enabled = false;
                btnActualizar.Enabled = true;
                btnAnular.Enabled = true;
            }
            btnCerrar.Enabled = true;
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Limpiar();
            
            txtId.Enabled = false;
            txtNombre.Enabled = true;
            chkEstado.Checked = true;
            btnNuevo.Enabled = false;
            btnGrabar.Enabled = false;
            btnActualizar.Enabled = false;
            btnAnular.Enabled = false;
            btnCerrar.Enabled = true;

            txtNombre.Focus();
            nuevo = true;
        }

        private void btnGrabar_Click(object sender, EventArgs e)
        {
            oDocumentos.Did_Nombre = txtNombre.Text.ToUpper();
            oDocumentos.Estado = chkEstado.Checked;
            oDocumentos.UsuCrea = 1;
            oDocumentos.UsuModi = 1;
            obj.DocIdentidadAdicionar(oDocumentos);
            MessageBox.Show("Registro GRABADO satisfactoriamente", "AVISO");
            Limpiar();
            CargarLista();
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            oDocumentos.Did_Id = Convert.ToInt16(txtId.Text);
            oDocumentos.Did_Nombre = txtNombre.Text.ToUpper();
            oDocumentos.UsuModi = 1;
            obj.DocIdentidadActualizar(oDocumentos);
            MessageBox.Show("Registro ACTUALIZADO satisfactoriamente", "AVISO");
            Limpiar();
            CargarLista();
        }

        private void btnAnular_Click(object sender, EventArgs e)
        {
            oDocumentos.Did_Id = Convert.ToInt16(txtId.Text);
            oDocumentos.Did_Nombre = txtNombre.Text;
            oDocumentos.Estado = !chkEstado.Checked;
            oDocumentos.UsuModi = 1;
            obj.DocIdentidadAnular(oDocumentos);
            if (oDocumentos.Estado)
            { MessageBox.Show("Registro ACTIVADO satisfactoriamente", "AVISO"); }
            else
            { MessageBox.Show("Registro ANULADO satisfactoriamente", "AVISO"); }
            Limpiar();
            CargarLista();
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            if ((MessageBox.Show("¿Seguro que desea salir ?", "Aviso",
                     MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                     MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes))
            {
                this.Close();
            }
        }

        private void dgvLista_SelectionChanged(object sender, EventArgs e)
        {
            int indice = dgvLista.CurrentCell.RowIndex;
            int Did_Id = int.Parse(dgvLista.Rows[indice].Cells[0].Value.ToString());

            List<usp_DocIdentidadRecResult> lista = new List<usp_DocIdentidadRecResult>();
            lista = obj.DocIdentidadDatos(Did_Id);
            if (lista.Count() > 0)
            {
                foreach (usp_DocIdentidadRecResult reg in lista)
                {
                    txtId.Text = reg.Did_Id.ToString();
                    txtNombre.Text = reg.Did_Nombre;
                    lblFecCrea.Text = reg.FecCrea.ToString();
                    lblFecMod.Text = reg.FecModi.ToString();
                    chkEstado.Checked = reg.Estado;

                    if (chkEstado.Checked)
                    { btnAnular.Text = "A&nular"; }
                    else
                    { btnAnular.Text = "A&ctivar"; }

                    txtNombre.Enabled = true;
                    btnNuevo.Enabled = true;
                    btnGrabar.Enabled = false;
                    btnActualizar.Enabled = true;
                    btnAnular.Enabled = true;
                    btnCerrar.Enabled = true;

                    lblCreado.Visible = true;
                    lblFecCrea.Visible = true;
                    lblModificado.Visible = true;
                    lblFecMod.Visible = true;

                    nuevo = false;
                    txtNombre.Focus();
                }
            }
        }    
    }
}
