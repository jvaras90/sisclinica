﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pjDermaBelle
{
    public partial class frmMantProfSalud : Form
    {
        public frmMantProfSalud()
        {
            InitializeComponent();
        }

        ClaseDatos obj = new ClaseDatos();
        DermaBelleDataDataContext linq = new DermaBelleDataDataContext();
        Medicos oMedicos = new Medicos();

        public bool grabar;

        private void frmMantProfSalud_Load(object sender, EventArgs e)
        {
            Especialidad();
            Distritos();
            Limpiar();
            CargarLista();
            tabControl1.SelectTab("tbpListado");
            lsbMedicos.Visible = false;
        }

        void Limpiar()
        {
            txtMedId.Text = "";
            txtMedApPat.Text = "";
            txtMedApMat.Text = "";
            txtMedNombres.Text = "";
            txtMedNomComp.Text = "";
            txtMedNumCol.Text = "";
            cmbMedEspecialidad.Text = "";
            txtMedRNE.Text = "";
            chkEstado.Checked = false;
            txtMedDireccion.Text = "";
            cmbMedDistrito.Text = "";
            txtMedTelefonos.Text = "";
            txtMedEmail.Text = "";
            //pbxMedFirma.Image
        }

        void Especialidad()
        {
            cmbMedEspecialidad.DataSource = obj.CboEspecialidades();
            cmbMedEspecialidad.ValueMember = "Esp_Id";
            cmbMedEspecialidad.DisplayMember = "Esp_Nombre";
        }

        void Distritos()
        {
            cmbMedDistrito.DataSource = obj.CboDistritos();
            cmbMedDistrito.ValueMember = "Ubi_Cod";
            cmbMedDistrito.DisplayMember = "Ubi_Nombre";
        }

        void CargarLista()
        {
            dgvMedicos.DataSource = obj.MedicosListar();

            for (int indice = 0; indice < dgvMedicos.Rows.Count; indice++)
            {
                dgvMedicos.Columns[0].Visible = false;    //Identificador del Médico
                dgvMedicos.Columns[1].Width = 300;        //Nombre del Médico
                dgvMedicos.Columns[2].Width = 50;         //CMP
                dgvMedicos.Columns[3].Width = 200;        //Especialidad
                dgvMedicos.Columns[4].Visible = false;    //RNE
                dgvMedicos.Columns[5].Visible = false;    //Dirección
                dgvMedicos.Columns[6].Visible = false;    //Telefonos
                dgvMedicos.Columns[7].Width = 50;         //Estado

                dgvMedicos.Rows[indice].ReadOnly = true;
            }
        }

        void Nuevo()
        {
            Limpiar();
            chkEstado.Checked = true;
            chkEstado.Enabled = false;
            btnBuscar.Enabled = true;
            btnNuevo.Enabled = false;
            btnGrabar.Enabled = true;
            btnAD.Enabled = false;
            btnCerrar.Enabled = true;
        }


        void Validar()
        {
            grabar = true;

            if (string.IsNullOrEmpty(txtMedApPat.Text))
                grabar = false;
            if (string.IsNullOrEmpty(txtMedApMat.Text))
                grabar = false;
            if (string.IsNullOrEmpty(txtMedNombres.Text))
                grabar = false;
            if (string.IsNullOrEmpty(txtMedNumCol.Text))
                grabar = false;
            if (string.IsNullOrEmpty(cmbMedEspecialidad.Text))
                grabar = false;
        }

        void GrabarRegistro()
        {
            Validar();
          
            if (!grabar)
            {
                MessageBox.Show("Datos incompletos, por favor revise", "AVISO");
                return;
            }

            //MemoryStream memoriaimagen = new MemoryStream();
            //pbxParteCuerpo.Image.Save(memoriaimagen, System.Drawing.Imaging.ImageFormat.Png);
            //Byte[] bytefoto = new byte[memoriaimagen.Length];
            //memoriaimagen.Position = 0;
            //memoriaimagen.Read(bytefoto, 0, Convert.ToInt32(memoriaimagen.Length));

            oMedicos.Med_ApPat = txtMedApPat.Text.ToUpper();
            oMedicos.Med_ApMat = txtMedApMat.Text.ToUpper();
            oMedicos.Med_Nombre = txtMedNombres.Text.ToUpper();
            oMedicos.Med_NomComp = txtMedNomComp.Text;
            oMedicos.Med_NumCol = txtMedNumCol.Text;
            oMedicos.Med_EspecId = int.Parse(cmbMedEspecialidad.SelectedValue.ToString());
            oMedicos.Med_RNE = txtMedRNE.Text;
            oMedicos.Med_FIngreso = dtpMedFIngreso.Value;
            oMedicos.Estado = chkEstado.Checked;
            oMedicos.Med_Direccion = txtMedDireccion.Text.ToUpper();
            oMedicos.Med_Distrito = cmbMedDistrito.SelectedValue.ToString();
            oMedicos.Med_Telefonos = txtMedTelefonos.Text;
            oMedicos.Med_Email = txtMedEmail.Text;
            //oMedicos.PartesCuerpoImagen = bytefoto;
            oMedicos.UsuCrea = 1; //Reemplazar por la variable Id Usuario
            oMedicos.UsuModi = 1; //Reemplazar por la variable Id Usuario
            if (btnGrabar.Text == "Grabar")
            {
                obj.MedicosAdicionar(oMedicos);
                MessageBox.Show("Registro GRABADO satisfactoriamente", "AVISO");
            }
            if(btnGrabar.Text == "Actualizar")
            {
                oMedicos.Med_Id = Convert.ToInt16(txtMedId.Text);
                obj.MedicosActualizar(oMedicos);
                MessageBox.Show("Registro ACTUALIZADO satisfactoriamente", "AVISO");
            }
            Limpiar();
            CargarLista();
        }

        private void btnNuevoReg_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab("tbpRegistro");
            Nuevo();
        }

        private void btnGrabar_Click(object sender, EventArgs e)
        {
            GrabarRegistro();
        }

        private void txtMedApPat_TextChanged(object sender, EventArgs e)
        {
            txtMedNomComp.Text = (txtMedApPat.Text + " " + txtMedApMat.Text + ", " + txtMedNombres.Text).ToUpper();
        }

        private void txtMedApMat_TextChanged(object sender, EventArgs e)
        {
            txtMedNomComp.Text = (txtMedApPat.Text + " " + txtMedApMat.Text + ", " + txtMedNombres.Text).ToUpper();
        }

        private void txtMedNombres_TextChanged(object sender, EventArgs e)
        {
            txtMedNomComp.Text = (txtMedApPat.Text + " " + txtMedApMat.Text + ", " + txtMedNombres.Text).ToUpper();
        }

        private void dgvMedicos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int ind = dgvMedicos.CurrentCell.RowIndex;
            int MedId = int.Parse(dgvMedicos.Rows[ind].Cells[0].Value.ToString());

            List<usp_MedicosRecResult> lista = new List<usp_MedicosRecResult>();
            lista = obj.MedicosDatos(MedId);
            if (lista.Count() > 0)
            {
                foreach (usp_MedicosRecResult reg in lista)
                {
                    tabControl1.SelectTab("tbpRegistro");
                    txtMedId.Text = reg.Med_Id.ToString();
                    txtMedNomComp.Text = reg.Med_NomComp;
                    txtMedApPat.Text = reg.Med_ApPat;
                    txtMedApMat.Text = reg.Med_ApMat;
                    txtMedNombres.Text = reg.Med_Nombre;
                    txtMedNumCol.Text = reg.Med_NumCol;
                    //cmbMedEspecialidad.Text = reg.
                    txtMedRNE.Text = reg.Med_RNE;
                    dtpMedFIngreso.Value = Convert.ToDateTime(reg.Med_FIngreso);
                    chkEstado.Checked = reg.Estado;
                    txtMedDireccion.Text = reg.Med_Direccion;
                    //cmbMedDistrito.Text = ;
                    txtMedTelefonos.Text = reg.Med_Telefonos;
                    txtMedEmail.Text = reg.Med_Email;

                    //CargarImagen();
                   
                    btnGrabar.Text = "Actualizar";
                    btnGrabar.Enabled = true;
                    if (chkEstado.Checked)
                        btnAD.Text = "Desactivar";
                    else
                        btnAD.Text = "Activar";
                    btnAD.Enabled = true;
                }
            }
        }

        private void btnAD_Click(object sender, EventArgs e)
        {
            oMedicos.Med_Id = Convert.ToInt16(txtMedId.Text);
            oMedicos.Estado = !chkEstado.Checked;
            oMedicos.UsuModi = 1;
            obj.MedicosEstado(oMedicos);
            chkEstado.Checked = oMedicos.Estado;
            if (chkEstado.Checked)
                MessageBox.Show("Registro ACTIVADO satisfactoriamente");
            else
                MessageBox.Show("Registro DESACTIVADO satisfactoriamente");
            Limpiar();
            CargarLista();
        }
    }
}
