﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pjDermaBelle.Formularios.Administrativo
{
    public partial class frmProductos : Form
    {
        public frmProductos()
        {
            InitializeComponent();
        }

        ClaseDatos obj = new ClaseDatos();
        DermaBelleDataDataContext linq = new DermaBelleDataDataContext();
        Productos oProductos = new Productos();
        Preparados oPreparados = new Preparados();
        Productos_Almacen oProdAlmacen = new Productos_Almacen();

        public string buscar;
        public bool mostrar;
        public bool grabar;

        void CargarProductos()
        {
            lstPActivos.Items.Clear();
            lblPrd_Id.Text = "";
            if (string.IsNullOrEmpty(txtDescripcion.Text))
            {
                lstPActivos.Visible = false;
                return;
            }
            List<usp_ProductosLisResult> lista = new List<usp_ProductosLisResult>();
            lista = obj.ProductosLis(txtDescripcion.Text);

            if (lista.Count() > 0)
            {
                foreach (usp_ProductosLisResult reg in lista)
                {
                    lstPActivos.Items.Add(reg.Prd_Descripcion);
                    lblPrd_Id.Text = reg.Prd_Id.ToString();
                }
            }
        }

        void Validar()
        {
            grabar = true;
            if (string.IsNullOrEmpty(txtDescripcion.Text))
                grabar = false;
            if (!rbtPTerminado.Checked && !rbtFMagistral.Checked)
                grabar = false;
            if (string.IsNullOrEmpty(txtPActivo.Text))
                grabar = false;
            if (string.IsNullOrEmpty(cmbLaboratorios.Text))
                grabar = false;
            if (string.IsNullOrEmpty(cmbClasifTerap.Text))
                grabar = false;
            if (string.IsNullOrEmpty(cmbFrmFarmac.Text))
                grabar = false;
        }

        void GrabarProducto()
        {
            Validar();
            if (!grabar)
            {
                MessageBox.Show("Datos incompletos. Por favor revise");
                return;
            }
            oProductos.Prd_Descripcion = txtDescripcion.Text;
            if (rbtPTerminado.Checked)
            { oProductos.TPr_Cod = "FA"; }
            if (rbtFMagistral.Checked)
            { oProductos.TPr_Cod = "FM"; }
            oProductos.PrA_Cod = short.Parse(txtPrA_Cod.Text);
            oProductos.Lab_Cod = short.Parse(cmbLaboratorios.SelectedValue.ToString());
            oProductos.ClasTerapId = short.Parse(cmbClasifTerap.SelectedValue.ToString());
            oProductos.Prs_Id = short.Parse(cmbFrmFarmac.SelectedValue.ToString());
            oProductos.prd_Venta = chkVenta.Checked;
            oProductos.Prd_IGV = chkIgv.Checked;
            oProductos.Estado = chkEstado.Checked;
            switch (btnGrabar.Text)
            {
                case "Agregar":
                    obj.ProductosAdd(oProductos);
                    MessageBox.Show("Registro AGREGADO satisfactoriamente");
                    HabilitarReg(false);

                    if (oProductos.TPr_Cod == "FM")
                    {
                        rbtComponente.Checked = true;
                        gpbFormula.Enabled = true;
                        tbpProductos.SelectedTab = tbpFormula; 
                    }
                    else
                    {
                        gpbAlmacenes.Enabled = true;
                        tbpProductos.SelectedTab = tbpAlmacenes; 
                    }

                    List<usp_ProductosIdResult> lista = new List<usp_ProductosIdResult>();
                    lista = obj.ProductosId();

                    if (lista.Count() > 0)
                    {
                        foreach (usp_ProductosIdResult reg in lista)
                        {
                            lblPrd_Id.Text = reg.Prd_Id.ToString();
                        }
                    }
                   break;
                case "Grabar":
                    oProductos.Prd_Id = short.Parse(lblPrd_Id.Text);
                    obj.ProductosUpd(oProductos);
                    MessageBox.Show("Registro MODIFICADO satisfactoriamente");
                    Limpiar();
                    HabilitarReg(false);
                    break;
            }
        }
 
        void CargarPActivos()
        {
            lstPActivos.Items.Clear();
            txtPrA_Cod.Text = "";
            if (string.IsNullOrWhiteSpace(txtPActivo.Text))
            {
                lstPActivos.Visible = false;
                return; 
            }
            List<usp_CboPrincActivosResult> lista = new List<usp_CboPrincActivosResult>();
            lista = obj.cmbPrincipios("FA", txtPActivo.Text);

            if (lista.Count() > 0)
            {
                foreach (usp_CboPrincActivosResult reg in lista)
                {
                    lstPActivos.Items.Add(reg.PrA_Nombre);
                    txtPrA_Cod.Text = reg.PrA_Id.ToString();
                }
            }
        }

        void CargarComponentes(string valor)
        {
            cmbElemento.DataSource = null;

            cmbElemento.DataSource = obj.cmbPrincipios(valor, "%");
            cmbElemento.ValueMember = "PrA_Id";
            cmbElemento.DisplayMember = "PrA_Nombre";
            cmbElemento.Text = "";
        }
 
        void Limpiar()
        {
            lstPActivos.Visible = false;
            rbtPTerminado.Checked = false;
            lblPrd_Id.Text = "Código";
            txtDescripcion.Text="";
            rbtPTerminado.Checked = true;
            rbtFMagistral.Checked = false;
            txtPrA_Cod.Text = "";
            txtPActivo.Text = "";
            cmbLaboratorios.Text = "";
            cmbClasifTerap.Text = "";
            cmbFrmFarmac.Text = "";
            chkVenta.Checked = false;
            chkIgv.Checked = false;
            txtIgv.Text = "18.00"; // Cambiar posteriormente por una variable 
            chkEstado.Checked = true;
            rbtComponente.Checked = false;
            rbtElemBase.Checked = false;
            cmbElemento.Text = "";
            dgvFormula.DataSource = null;

            txtDescripcion.Enabled = true;
            rbtPTerminado.Enabled = true;
            rbtFMagistral.Enabled = true;
            txtPActivo.Enabled = true;
            cmbLaboratorios.Enabled = true;
            cmbClasifTerap.Enabled = true;
            cmbFrmFarmac.Enabled = true;

            btnGrabar.Enabled = false;
            btnAnular.Enabled = false;

            txtDescripcion.Focus();
        }

        void RecuperaReg()
        {
            List<usp_ProductosRecResult> lista = new List<usp_ProductosRecResult>();
            lista = obj.ProductosRec(int.Parse(lblPrd_Id.Text));
            if (lista.Count() > 0)
            {
                foreach (usp_ProductosRecResult reg in lista)
                {
                    switch (reg.TPr_Cod)
                    {
                        case "FA":
                            rbtPTerminado.Checked = true;
                            break;
                        case "FM":
                            rbtFMagistral.Checked = true;
                            break;
                    }
                    txtPrA_Cod.Text = reg.PrA_Cod.ToString();
                    txtPActivo.Text = reg.PrA_Nombre;
                    cmbLaboratorios.Text = reg.Lab_Nombre;
                    cmbClasifTerap.Text = reg.ClasTerapNombre;
                    cmbFrmFarmac.Text = reg.Prs_Nombre;
                    chkVenta.Checked = Convert.ToBoolean(reg.Prd_Venta);
                    chkIgv.Checked = Convert.ToBoolean(reg.Prd_IGV);
                    txtIgv.Text = reg.Prd_IGV.ToString();
                    chkEstado.Checked = Convert.ToBoolean(reg.Estado);
                }
                lstPActivos.Visible = false;
                txtPActivo.Enabled = false;
                if (rbtFMagistral.Checked)
                {CargarFormula();}
                CargaProdAlmacen();
                btnBuscar.Enabled = false;
                btnNuevo.Enabled = false;
                btnGrabar.Enabled = false;
                btnEditar.Enabled = true;
                btnAnular.Enabled = true;
            }
        }
        
        private void frmProductos_Load(object sender, EventArgs e)
        {
            cmbLaboratorios.DataSource = null;
            cmbLaboratorios.DataSource = obj.LaboratoriosCmb();
            cmbLaboratorios.ValueMember = "Lab_Id";
            cmbLaboratorios.DisplayMember = "Lab_Nombre";
            cmbLaboratorios.Text = "";

            cmbClasifTerap.DataSource = null;
            cmbClasifTerap.DataSource = obj.ClasifTerapCmb();
            cmbClasifTerap.ValueMember = "ClasTerapID";
            cmbClasifTerap.DisplayMember = "ClasTerapNombre";
            cmbClasifTerap.Text = "";

            cmbFrmFarmac.DataSource = null;
            cmbFrmFarmac.DataSource = obj.PresentacionCmb();
            cmbFrmFarmac.ValueMember = "Prs_Id";
            cmbFrmFarmac.DisplayMember = "Prs_Nombre";
            cmbFrmFarmac.Text = "";

            cmbAlmacenes.DataSource = null;
            cmbAlmacenes.DataSource = obj.AlmacenesCmb();
            cmbAlmacenes.ValueMember = "Alm_Id";
            cmbAlmacenes.DisplayMember = "Alm_Nombre";
            cmbAlmacenes.Text = "";

            HabilitarReg(false);
        }

        private void rbtPTerminado_CheckedChanged(object sender, EventArgs e)
        {
            txtPActivo.Enabled = rbtPTerminado.Checked;
            txtPActivo.Text = "";
            txtPrA_Cod.Text = "";
            txtDescripcion.Focus();
        }

        private void rbtFMagistral_CheckedChanged(object sender, EventArgs e)
        {
            txtPActivo.Enabled = !rbtFMagistral.Checked;
            txtPActivo.Text = "FORMULA MAGISTRAL";
            txtPrA_Cod.Text = "1546";
            cmbLaboratorios.Text = "-- NO ESPECIFICA --";
            cmbClasifTerap.Text = "-- NO ESPECIFICA--";
            cmbFrmFarmac.Text = "-- NO ESPECIFICA --";
            cmbLaboratorios.Enabled = false;
            cmbClasifTerap.Enabled = false;
            cmbFrmFarmac.Enabled = false;
            lstPActivos.Visible = false;
        }

        private void txtPActivo_TextChanged(object sender, EventArgs e)
        {
            lstPActivos.Visible = true;
            lstPActivos.Size = new Size(488, 161);
            lstPActivos.Location = new Point(148, 149);
            CargarPActivos();
            buscar = "PA";
        }

        private void lstPActivos_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                switch (buscar)
                {
                    case "PR":
                        txtDescripcion.Text = lstPActivos.SelectedItem.ToString();
                        lstPActivos.Visible = false;
                        RecuperaReg();
                        break;
                    case "PA":
                        txtPActivo.Text = lstPActivos.SelectedItem.ToString();
                        lstPActivos.Visible = false;
                        List<usp_RecupPra_IdResult> lista = new List<usp_RecupPra_IdResult>();
                        lista = obj.RecupPra_Id(txtPActivo.Text);
                        if (lista.Count() > 0)
                        {
                            foreach (usp_RecupPra_IdResult reg in lista)
                            {
                                txtPrA_Cod.Text = reg.Pra_Id.ToString();
                            }
                        }
                        break;
                }
            }
            catch { }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Limpiar();
            cmbClasifTerap.Text = "-- NO ESPECIFICA --";
            cmbFrmFarmac.Text = "-- NO ESPECIFICA --";
            HabilitarReg(true);
            chkEstado.Checked = true;
            btnGrabar.Text = "Agregar";
            btnGrabar.Enabled = true;
            buscar = "PA";
            mostrar = false;
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            HabilitarReg(false);
            txtDescripcion.Enabled = true;
            txtDescripcion.Focus();
            txtPActivo.Enabled = false;
            buscar = "PR";
            mostrar = true;
        }

        private void btnGrabaComp_Click(object sender, EventArgs e)
        {
            btnGrabar.Enabled = true;
        }

        private void chkIgv_CheckedChanged(object sender, EventArgs e)
        {
            if (chkIgv.Checked)
            { txtIgv.Text = "18.00"; }    // Cambiar luego por una variable
            else
            { txtIgv.Text = "0.00"; }
        }

        private void txtDescripcion_TextChanged(object sender, EventArgs e)
        {
            if (mostrar)
            {
                lstPActivos.Visible = true;
                lstPActivos.Size = new Size(488, 161);
                lstPActivos.Location = new Point(148, 89);
                CargarProductos();
            }
        }

        private void btnAnular_Click(object sender, EventArgs e)
        {
            btnGrabar.Text = "Grabar";
            chkEstado.Checked = !chkEstado.Checked;
            if (chkEstado.Checked)
            { btnAnular.Text = "Desactivar"; }
            else
            { btnAnular.Text = "Activar"; }
            GrabarProducto();
        }

        void HabilitarReg(bool valor)
        {
            rbtPTerminado.Enabled = valor;
            rbtFMagistral.Enabled = valor;
            txtDescripcion.Enabled = valor;
            txtPActivo.Enabled = valor;
            cmbLaboratorios.Enabled = valor;
            cmbClasifTerap.Enabled = valor;
            cmbFrmFarmac.Enabled = valor;
            chkVenta.Enabled = valor;
            chkIgv.Enabled = valor;
            gpbFormula.Enabled = false;
            gpbAlmacenes.Enabled = false;

            btnBuscar.Enabled = true;
            btnNuevo.Enabled = true;
            btnGrabar.Enabled = false;
            btnEditar.Enabled = false;
            btnAnular.Enabled = false;
            btnCancelar.Enabled = true;
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            mostrar = false;
            HabilitarReg(true);
            gpbFormula.Enabled = rbtFMagistral.Checked;
            gpbAlmacenes.Enabled = true;

            btnGrabar.Text = "Grabar";
            btnGrabar.Enabled = true;
            btnEditar.Enabled = false;
            btnAnular.Enabled = false;
        }

        private void btnGrabar_Click(object sender, EventArgs e)
        {
            GrabarProducto();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Limpiar();
            HabilitarReg(false);
        }

        private void rbtComponente_CheckedChanged_1(object sender, EventArgs e)
        {
            if (rbtComponente.Checked)
            { CargarComponentes("FM"); }
        }

        private void rbtElemBase_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtElemBase.Checked)
            { CargarComponentes("EB"); }
        }

        private void cmbElemento_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                List<usp_RecuperaUndPAResult> lista = new List<usp_RecuperaUndPAResult>();
                lista = obj.RecuperaUndPA(int.Parse(cmbElemento.SelectedValue.ToString()));
                if (lista.Count() > 0)
                {
                    foreach (usp_RecuperaUndPAResult reg in lista)
                    {
                        lblUnidad.Text = reg.PrA_Presentacion;
                    }
                }
            }
            catch { }
        }

        void CargarFormula()
        {
            if (lblPrd_Id.Text != "Código")
            {
                dgvFormula.DataSource = obj.FormulaLis(short.Parse(lblPrd_Id.Text));
                for (int indice = 0; indice < dgvFormula.Rows.Count; indice++)
                {
                    dgvFormula.Columns[2].Visible = false;   //Prd_Id
                    dgvFormula.Columns[4].Visible = false;   //PrA_Id
                    dgvFormula.Columns[3].Width = 50;        //PrA_Tipo
                    dgvFormula.Columns[5].Width = 480;       //PrA_Nombre
                    dgvFormula.Columns[6].Width = 60;        //Cantidad
                    dgvFormula.Columns[7].Width = 60;        //Unidad
                }
            }
        }

        private void btnGrabaComp_Click_1(object sender, EventArgs e)
        {
            oPreparados.Prd_Id = short.Parse(lblPrd_Id.Text);
            if (rbtComponente.Checked)
            { oPreparados.PrA_Tipo = "FM"; }
            if (rbtElemBase.Checked)
            { oPreparados.PrA_Tipo = "EB"; }
            oPreparados.PrA_Id = short.Parse(cmbElemento.SelectedValue.ToString());
            oPreparados.Cantidad = txtCantidad.Text;
            oPreparados.Unidad = lblUnidad.Text;
            switch (btnGrabaComp.Text)
            {
                case "Agregar":
                    obj.FormulaAdd(oPreparados);
                    break;
                case "Grabar":
                    obj.FormulaUpd(oPreparados);
                    break;
            }
            rbtComponente.Checked = false;
            rbtElemBase.Checked = false;
            cmbElemento.Text = "";
            txtCantidad.Text = "";
            lblUnidad.Text = "";
            CargarFormula();
        }

        private void dgvFormula_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.ColumnIndex >= 0 && this.dgvFormula.Columns[e.ColumnIndex].Name == "btnDelComponente" && e.RowIndex >= 0)
            {
                e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                DataGridViewButtonCell celBoton = this.dgvFormula.Rows[e.RowIndex].Cells["btnDelComponente"] as DataGridViewButtonCell;
                Icon icoEliminar = new Icon(@"C:\Windows\Dermabelle\Ico\Delete.ico");
                e.Graphics.DrawIcon(icoEliminar, e.CellBounds.Left + 3, e.CellBounds.Top + 3);

                this.dgvFormula.Rows[e.RowIndex].Height = icoEliminar.Height + 10;
                this.dgvFormula.Columns[e.ColumnIndex].Width = icoEliminar.Width + 10;

                e.Handled = true;
            }

            if (e.ColumnIndex >= 0 && this.dgvFormula.Columns[e.ColumnIndex].Name == "btnUpdComponente" && e.RowIndex >= 0)
            {
                e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                DataGridViewButtonCell celBoton = this.dgvFormula.Rows[e.RowIndex].Cells["btnUpdComponente"] as DataGridViewButtonCell;
                Icon icoModificar = new Icon(@"C:\Windows\Dermabelle\Ico\Style_file.ico");
                e.Graphics.DrawIcon(icoModificar, e.CellBounds.Left + 3, e.CellBounds.Top + 3);

                this.dgvFormula.Rows[e.RowIndex].Height = icoModificar.Height + 10;
                this.dgvFormula.Columns[e.ColumnIndex].Width = icoModificar.Width + 10;

                e.Handled = true;
            }
        }



        private void dgvProdAlmacen_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.ColumnIndex >= 0 && this.dgvProdAlmacen.Columns[e.ColumnIndex].Name == "btnAlmacenDel" && e.RowIndex >= 0)
            {
                e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                DataGridViewButtonCell celBoton = this.dgvProdAlmacen.Rows[e.RowIndex].Cells["btnAlmacenDel"] as DataGridViewButtonCell;
                Icon icoEliminar = new Icon(@"C:\Windows\Dermabelle\Ico\Delete.ico");
                e.Graphics.DrawIcon(icoEliminar, e.CellBounds.Left + 3, e.CellBounds.Top + 3);

                this.dgvProdAlmacen.Rows[e.RowIndex].Height = icoEliminar.Height + 10;
                this.dgvProdAlmacen.Columns[e.ColumnIndex].Width = icoEliminar.Width + 10;

                e.Handled = true;
            }
        }

        void CargaProdAlmacen()
        {
            if (lblPrd_Id.Text != "Código")
            {
                dgvProdAlmacen.DataSource = null;
                dgvProdAlmacen.DataSource = obj.ProdAlmacenLis(short.Parse(lblPrd_Id.Text));
                for (int indice = 0; indice < dgvProdAlmacen.Rows.Count; indice++)
                {
                    dgvProdAlmacen.Columns[1].Visible = false;   //Alm_Id
                    dgvProdAlmacen.Columns[2].Width = 250;       //Almacen
                    dgvProdAlmacen.Columns[3].Width = 50;        //Stock
                }
            }
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            oProdAlmacen.Prd_Id = short.Parse(lblPrd_Id.Text);
            oProdAlmacen.Alm_Id = short.Parse(cmbAlmacenes.SelectedValue.ToString());
            obj.ProdAlmacenIns(oProdAlmacen);
            CargaProdAlmacen();
        }

        private void tbpAlmacenes_Enter(object sender, EventArgs e)
        {
            CargaProdAlmacen();
        }

        private void tbpFormula_Enter(object sender, EventArgs e)
        {
            CargarFormula();
        }

        private void dgvProdAlmacen_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (this.dgvProdAlmacen.Columns[e.ColumnIndex].Name == "btnAlmacenDel")
            {
                int indice = dgvProdAlmacen.CurrentCell.RowIndex;
                oProdAlmacen.Prd_Id = short.Parse(lblPrd_Id.Text);
                oProdAlmacen.Alm_Id = short.Parse(dgvProdAlmacen.Rows[indice].Cells[1].Value.ToString());

                obj.ProdAlmacenDel(oProdAlmacen);
                CargaProdAlmacen();
            }
        }

        private void dgvFormula_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int indice = dgvFormula.CurrentCell.RowIndex;

            if (dgvFormula.Rows[indice].Cells[3].Value.ToString() == "FM")
                rbtComponente.Checked = true;
            if (dgvFormula.Rows[indice].Cells[3].Value.ToString() == "EB")
                rbtElemBase.Checked = true;

            cmbElemento.Text = dgvFormula.Rows[indice].Cells[5].Value.ToString();
            txtCantidad.Text = dgvFormula.Rows[indice].Cells[6].Value.ToString();
            
            if (this.dgvFormula.Columns[e.ColumnIndex].Name == "btnDelComponente")
            {
                rbtComponente.Enabled = false;
                rbtElemBase.Enabled = false;
                cmbElemento.Enabled = false;
                txtCantidad.Enabled = false;

                if ((MessageBox.Show("¿Seguro que desea Eliminar este registro?", "Aviso",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes))
                {
                    oPreparados.Prd_Id = short.Parse(lblPrd_Id.Text);
                    oPreparados.PrA_Id = short.Parse(cmbElemento.SelectedValue.ToString());
                    obj.FormulaDel(oPreparados);
                }
            }
            else
            {
                if (this.dgvFormula.Columns[e.ColumnIndex].Name == "btnUpdComponente")
                {
                    rbtComponente.Enabled = false;
                    rbtElemBase.Enabled = false;
                    cmbElemento.Enabled = false;
                    btnGrabaComp.Text = "Grabar";
                }
                else
                { return; }
            }
            rbtComponente.Checked = false;
            rbtElemBase.Checked = false;
            cmbElemento.Text = "";
            txtCantidad.Text = "";
            lblUnidad.Text = "";
            CargarFormula();
        }
    }
}
