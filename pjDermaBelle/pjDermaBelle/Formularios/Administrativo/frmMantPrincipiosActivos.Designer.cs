﻿namespace pjDermaBelle
{
    partial class frmMantPrincipiosActivos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblModificado = new System.Windows.Forms.Label();
            this.lblFecMod = new System.Windows.Forms.Label();
            this.lblCreado = new System.Windows.Forms.Label();
            this.txtId = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.chkEstado = new System.Windows.Forms.CheckBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblFecCrea = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rbElemBase = new System.Windows.Forms.RadioButton();
            this.rbFormMagistral = new System.Windows.Forms.RadioButton();
            this.rbFarmacia = new System.Windows.Forms.RadioButton();
            this.dgvLista = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.btnAnular = new System.Windows.Forms.Button();
            this.btnActualizar = new System.Windows.Forms.Button();
            this.btnGrabar = new System.Windows.Forms.Button();
            this.btnNuevo = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnCerrar = new System.Windows.Forms.Button();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLista)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblModificado
            // 
            this.lblModificado.AutoSize = true;
            this.lblModificado.ForeColor = System.Drawing.Color.Crimson;
            this.lblModificado.Location = new System.Drawing.Point(187, 403);
            this.lblModificado.Name = "lblModificado";
            this.lblModificado.Size = new System.Drawing.Size(84, 13);
            this.lblModificado.TabIndex = 65;
            this.lblModificado.Text = "/  Modificado el:";
            this.lblModificado.Visible = false;
            // 
            // lblFecMod
            // 
            this.lblFecMod.AutoSize = true;
            this.lblFecMod.ForeColor = System.Drawing.Color.Crimson;
            this.lblFecMod.Location = new System.Drawing.Point(271, 403);
            this.lblFecMod.Name = "lblFecMod";
            this.lblFecMod.Size = new System.Drawing.Size(133, 13);
            this.lblFecMod.TabIndex = 64;
            this.lblFecMod.Text = "dd/mm/aaaa hh:mm:ss ?m";
            this.lblFecMod.Visible = false;
            // 
            // lblCreado
            // 
            this.lblCreado.AutoSize = true;
            this.lblCreado.ForeColor = System.Drawing.Color.Crimson;
            this.lblCreado.Location = new System.Drawing.Point(4, 403);
            this.lblCreado.Name = "lblCreado";
            this.lblCreado.Size = new System.Drawing.Size(55, 13);
            this.lblCreado.TabIndex = 62;
            this.lblCreado.Text = "Creado el:";
            this.lblCreado.Visible = false;
            // 
            // txtId
            // 
            this.txtId.Location = new System.Drawing.Point(108, 22);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(35, 20);
            this.txtId.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(89, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(16, 13);
            this.label4.TabIndex = 32;
            this.label4.Text = "Id";
            // 
            // chkEstado
            // 
            this.chkEstado.AutoSize = true;
            this.chkEstado.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkEstado.Enabled = false;
            this.chkEstado.Location = new System.Drawing.Point(63, 108);
            this.chkEstado.Name = "chkEstado";
            this.chkEstado.Size = new System.Drawing.Size(62, 17);
            this.chkEstado.TabIndex = 31;
            this.chkEstado.Text = "Activo  ";
            this.chkEstado.UseVisualStyleBackColor = true;
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(108, 49);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(660, 20);
            this.txtNombre.TabIndex = 2;
            this.txtNombre.TextChanged += new System.EventHandler(this.txtNombre_TextChanged);
            this.txtNombre.Leave += new System.EventHandler(this.txtNombre_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 13);
            this.label3.TabIndex = 29;
            this.label3.Text = "Principio Activo";
            // 
            // lblFecCrea
            // 
            this.lblFecCrea.AutoSize = true;
            this.lblFecCrea.ForeColor = System.Drawing.Color.Crimson;
            this.lblFecCrea.Location = new System.Drawing.Point(57, 403);
            this.lblFecCrea.Name = "lblFecCrea";
            this.lblFecCrea.Size = new System.Drawing.Size(133, 13);
            this.lblFecCrea.TabIndex = 63;
            this.lblFecCrea.Text = "dd/mm/aaaa hh:mm:ss ?m";
            this.lblFecCrea.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rbElemBase);
            this.groupBox2.Controls.Add(this.rbFormMagistral);
            this.groupBox2.Controls.Add(this.rbFarmacia);
            this.groupBox2.Controls.Add(this.txtId);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.chkEstado);
            this.groupBox2.Controls.Add(this.txtNombre);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(8, 32);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(774, 146);
            this.groupBox2.TabIndex = 61;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Registro";
            // 
            // rbElemBase
            // 
            this.rbElemBase.AutoSize = true;
            this.rbElemBase.Location = new System.Drawing.Point(320, 76);
            this.rbElemBase.Name = "rbElemBase";
            this.rbElemBase.Size = new System.Drawing.Size(96, 17);
            this.rbElemBase.TabIndex = 5;
            this.rbElemBase.TabStop = true;
            this.rbElemBase.Text = "Elemento Base";
            this.rbElemBase.UseVisualStyleBackColor = true;
            // 
            // rbFormMagistral
            // 
            this.rbFormMagistral.AutoSize = true;
            this.rbFormMagistral.Location = new System.Drawing.Point(196, 76);
            this.rbFormMagistral.Name = "rbFormMagistral";
            this.rbFormMagistral.Size = new System.Drawing.Size(107, 17);
            this.rbFormMagistral.TabIndex = 4;
            this.rbFormMagistral.TabStop = true;
            this.rbFormMagistral.Text = "Fórmula Magistral";
            this.rbFormMagistral.UseVisualStyleBackColor = true;
            // 
            // rbFarmacia
            // 
            this.rbFarmacia.AutoSize = true;
            this.rbFarmacia.Location = new System.Drawing.Point(111, 76);
            this.rbFarmacia.Name = "rbFarmacia";
            this.rbFarmacia.Size = new System.Drawing.Size(71, 17);
            this.rbFarmacia.TabIndex = 3;
            this.rbFarmacia.TabStop = true;
            this.rbFarmacia.Text = "Farmacos";
            this.rbFarmacia.UseVisualStyleBackColor = true;
            // 
            // dgvLista
            // 
            this.dgvLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLista.Location = new System.Drawing.Point(8, 184);
            this.dgvLista.Name = "dgvLista";
            this.dgvLista.Size = new System.Drawing.Size(681, 203);
            this.dgvLista.TabIndex = 59;
            this.dgvLista.SelectionChanged += new System.EventHandler(this.dgvLista_SelectionChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(289, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(217, 16);
            this.label1.TabIndex = 58;
            this.label1.Text = "Registro de Principios Activos";
            // 
            // btnAnular
            // 
            this.btnAnular.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAnular.Location = new System.Drawing.Point(6, 122);
            this.btnAnular.Name = "btnAnular";
            this.btnAnular.Size = new System.Drawing.Size(75, 30);
            this.btnAnular.TabIndex = 21;
            this.btnAnular.Text = "A&nular";
            this.btnAnular.UseVisualStyleBackColor = true;
            this.btnAnular.Click += new System.EventHandler(this.btnAnular_Click);
            // 
            // btnActualizar
            // 
            this.btnActualizar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnActualizar.Location = new System.Drawing.Point(6, 86);
            this.btnActualizar.Name = "btnActualizar";
            this.btnActualizar.Size = new System.Drawing.Size(75, 30);
            this.btnActualizar.TabIndex = 20;
            this.btnActualizar.Text = "&Actualizar";
            this.btnActualizar.UseVisualStyleBackColor = true;
            this.btnActualizar.Click += new System.EventHandler(this.btnActualizar_Click);
            // 
            // btnGrabar
            // 
            this.btnGrabar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGrabar.Location = new System.Drawing.Point(6, 50);
            this.btnGrabar.Name = "btnGrabar";
            this.btnGrabar.Size = new System.Drawing.Size(75, 30);
            this.btnGrabar.TabIndex = 19;
            this.btnGrabar.Text = "&Grabar";
            this.btnGrabar.UseVisualStyleBackColor = true;
            this.btnGrabar.Click += new System.EventHandler(this.btnGrabar_Click);
            // 
            // btnNuevo
            // 
            this.btnNuevo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNuevo.Location = new System.Drawing.Point(6, 14);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(75, 30);
            this.btnNuevo.TabIndex = 18;
            this.btnNuevo.Text = "&Nuevo";
            this.btnNuevo.UseVisualStyleBackColor = true;
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnCerrar);
            this.groupBox1.Controls.Add(this.btnAnular);
            this.groupBox1.Controls.Add(this.btnActualizar);
            this.groupBox1.Controls.Add(this.btnGrabar);
            this.groupBox1.Controls.Add(this.btnNuevo);
            this.groupBox1.Location = new System.Drawing.Point(695, 184);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(87, 203);
            this.groupBox1.TabIndex = 60;
            this.groupBox1.TabStop = false;
            // 
            // btnCerrar
            // 
            this.btnCerrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCerrar.Location = new System.Drawing.Point(6, 158);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(75, 30);
            this.btnCerrar.TabIndex = 22;
            this.btnCerrar.Text = "&Salir";
            this.btnCerrar.UseVisualStyleBackColor = true;
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // frmMantPrincipiosActivos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(794, 429);
            this.Controls.Add(this.lblModificado);
            this.Controls.Add(this.lblFecMod);
            this.Controls.Add(this.lblCreado);
            this.Controls.Add(this.lblFecCrea);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.dgvLista);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Name = "frmMantPrincipiosActivos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mantenimiento de Principios Activos";
            this.Load += new System.EventHandler(this.frmMantPrincipiosActivos_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLista)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Label lblModificado;
        internal System.Windows.Forms.Label lblFecMod;
        internal System.Windows.Forms.Label lblCreado;
        private System.Windows.Forms.TextBox txtId;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chkEstado;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label label3;
        internal System.Windows.Forms.Label lblFecCrea;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dgvLista;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnAnular;
        private System.Windows.Forms.Button btnActualizar;
        private System.Windows.Forms.Button btnGrabar;
        private System.Windows.Forms.Button btnNuevo;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnCerrar;
        private System.Windows.Forms.RadioButton rbElemBase;
        private System.Windows.Forms.RadioButton rbFormMagistral;
        private System.Windows.Forms.RadioButton rbFarmacia;
    }
}