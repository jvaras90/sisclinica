﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace pjDermaBelle.Formularios.Utilitarios
{
    public partial class frmPartesCuerpo : Form
    {
        public frmPartesCuerpo()
        {
            InitializeComponent();
        }

        ClaseDatos obj = new ClaseDatos();
        DermaBelleDataDataContext linq = new DermaBelleDataDataContext();
        PartesCuerpo oPartesCuerpo = new PartesCuerpo();
        PartesCuerpoSegmentos oSegmentos = new PartesCuerpoSegmentos();

        private string ruta = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Remove(0 ,6) + "/";

        void Limpiar()
        {
            lblPartesCuerpoId.Text = "Id Automático";
            txtPartesCuerpoDescripcion.Text = "";
            cmbServicioId.Text = "";
            txtPartesCuerpoOrden.Text = "";
            pbxParteCuerpo.Image = null;
            dgvSegmentos.DataSource = null;

            //btnBuscar.Enabled = false;
            btnNuevo.Enabled = true;
            btnGrabar.Enabled = false;
            btnModificar.Enabled = false;
            btnActEstado.Enabled = false;
            btnSalir.Enabled = true;
        }

        void CargarServicios()
        {
            cmbServicioId.DataSource = obj.CboServicios();
            cmbServicioId.ValueMember = "Ser_id";
            cmbServicioId.DisplayMember = "Ser_Nombre";
 
            cmbServicio.DataSource = obj.CboServicios();
            cmbServicio.ValueMember = "Ser_id";
            cmbServicio.DisplayMember = "Ser_Nombre";
        }

        void CargardgvSegmentos()
        {
            dgvSegmentos.DataSource = obj.PartesCuerpoDetalleLis(int.Parse(lblPartesCuerpoId.Text));

            for (int indice = 0; indice < dgvSegmentos.Rows.Count; indice++)
            {
                dgvSegmentos.Columns[1].Visible = false;   //SegmentoId
                dgvSegmentos.Columns[2].Width = 340;       //SegmentoDescripcion
                dgvSegmentos.Columns[3].Width = 50;        //Estado

                dgvSegmentos.Rows[indice].ReadOnly = true;
            }
        }

        void Actualizar()
        {
            MemoryStream memoriaimagen = new MemoryStream();
            pbxParteCuerpo.Image.Save(memoriaimagen, System.Drawing.Imaging.ImageFormat.Png);
            Byte[] bytefoto = new byte[memoriaimagen.Length];
            memoriaimagen.Position = 0;
            memoriaimagen.Read(bytefoto, 0, Convert.ToInt32(memoriaimagen.Length));

            oPartesCuerpo.PartesCuerpoId = int.Parse(lblPartesCuerpoId.Text);
            oPartesCuerpo.PartesCuerpoDescripcion = txtPartesCuerpoDescripcion.Text;
            oPartesCuerpo.PartesCuerpoSerId = int.Parse(cmbServicioId.SelectedValue.ToString());
            oPartesCuerpo.PartesCuerpoOrden = int.Parse(txtPartesCuerpoOrden.Text);
            oPartesCuerpo.Estado = ckbEstado.Checked;
            oPartesCuerpo.PartesCuerpoImagen = bytefoto;
            oPartesCuerpo.UsuCrea = 1; //Reemplazar por la variable Id Usuario
            oPartesCuerpo.UsuModi = 1; //Reemplazar por la variable Id Usuario
            obj.PartesCuerpoUpd(oPartesCuerpo);
            MessageBox.Show("Registro MODIFICADO satisfactoriamente", "AVISO");
            Limpiar();
            CargardgvPartesCuerpo();
            tabControl1.SelectedTab = tabPage2;
        }

        private byte[] byimagen;
        void CargarImagen()
        {
            SqlConnection con1 = new SqlConnection();
            //con1.ConnectionString = "Data Source=.;Initial Catalog=Dermabelle;uid=DBUser;pwd=dbuser;Integrated Security=False";
            con1.ConnectionString = "Data Source=.;Initial Catalog=Dermabelle;Integrated Security=true;";

            SqlCommand cmd = new SqlCommand("Select PartesCuerpoImagen From PartesCuerpo Where PartesCuerpoId = @PartesCuerpoId", con1);
            cmd.Parameters.AddWithValue("@PartesCuerpoId",lblPartesCuerpoId.Text);
            con1.Open();

            SqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                byimagen = (byte[])dr.GetValue(0);
            }

            MemoryStream ms = new MemoryStream(byimagen);
            pbxParteCuerpo.Image = Image.FromStream(ms);
            con1.Close();
        }

        void CargardgvPartesCuerpo()
        {
            int SerId = int.Parse(cmbServicio.SelectedValue.ToString());
            dgvPartesCuerpo.DataSource = obj.PartesCuerpoList(SerId);

            for (int indice = 0; indice < dgvPartesCuerpo.Rows.Count; indice++)
            {
                dgvPartesCuerpo.Columns[0].Visible = false;   //PartesCuerpoId
                dgvPartesCuerpo.Columns[1].Width = 50;        //PartesCuerpoOrden
                dgvPartesCuerpo.Columns[2].Width = 590;       //PartesCuerpoDescripcion
                dgvPartesCuerpo.Columns[3].Width = 50;        //Estado

                dgvPartesCuerpo.Rows[indice].ReadOnly = true;
            }
        }

        private void frmPartesCuerpo_Load(object sender, EventArgs e)
        {
            Limpiar();
            CargarServicios();
            tabControl1.SelectedTab = tabPage2;
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            // Se crea el OpenFileDialog
            OpenFileDialog dialog = new OpenFileDialog();
            // Se muestra al usuario esperando una acción
            DialogResult result = dialog.ShowDialog();

            // Si seleccionó un archivo (asumiendo que es una imagen lo que seleccionó)
            // la mostramos en el PictureBox de la inferfaz
            if (result == DialogResult.OK)
            {
                //pbxParteCuerpo.Image = Image.FromFile(dialog.FileName);
                pbxParteCuerpo.ImageLocation = dialog.FileName;
            }
        }

        private void tabPage2_Enter(object sender, EventArgs e)
        {
            CargardgvPartesCuerpo();
        }

        private void dgvPartesCuerpo_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int ind = dgvPartesCuerpo.CurrentCell.RowIndex;
                int PartesCuerpoId = int.Parse(dgvPartesCuerpo.Rows[ind].Cells[0].Value.ToString());

                List<usp_PartesCuerpoRecResult> lista = new List<usp_PartesCuerpoRecResult>();
                lista = obj.PartesCuerpoRec(PartesCuerpoId);
                if (lista.Count() > 0)
                {
                    foreach (usp_PartesCuerpoRecResult reg in lista)
                    {
                        lblPartesCuerpoId.Text = reg.PartesCuerpoId.ToString();
                        txtPartesCuerpoDescripcion.Text = reg.PartesCuerpoDescripcion;
                        cmbServicioId.Text = reg.Ser_Nombre;
                        txtPartesCuerpoOrden.Text = reg.PartesCuerpoOrden.ToString();
                        ckbEstado.Checked = reg.Estado;
                        CargarImagen();
                        CargardgvSegmentos();
                        btnGrabar.Enabled = false;
                        btnModificar.Enabled = true;
                        btnActEstado.Enabled = true;
                        tabControl1.SelectedTab = tabPage1;
                    }
                }
            }
            catch {
                MessageBox.Show("No hay registros, Registre uno nuevo");
                btnNuevo_Click_1(sender,e);
            }

        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
        }

        private void btnGrabar_Click(object sender, EventArgs e)
        {
            MemoryStream memoriaimagen = new MemoryStream();
            pbxParteCuerpo.Image.Save(memoriaimagen, System.Drawing.Imaging.ImageFormat.Png);
            Byte[] bytefoto = new byte[memoriaimagen.Length];
            memoriaimagen.Position = 0;
            memoriaimagen.Read(bytefoto, 0, Convert.ToInt32(memoriaimagen.Length));

            oPartesCuerpo.PartesCuerpoDescripcion = txtPartesCuerpoDescripcion.Text;
            oPartesCuerpo.PartesCuerpoSerId = int.Parse(cmbServicioId.SelectedValue.ToString());
            oPartesCuerpo.PartesCuerpoOrden = int.Parse(txtPartesCuerpoOrden.Text);
            oPartesCuerpo.Estado = ckbEstado.Checked;
            oPartesCuerpo.PartesCuerpoImagen = bytefoto;
            oPartesCuerpo.UsuCrea = 1; //Reemplazar por la variable Id Usuario
            oPartesCuerpo.UsuModi = 1; //Reemplazar por la variable Id Usuario
            obj.PartesCuerpoAdd(oPartesCuerpo);
            MessageBox.Show("Registro GRABADO satisfactoriamente", "AVISO");
            Limpiar();
            CargardgvPartesCuerpo();
            tabControl1.SelectedTab = tabPage2;
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            Actualizar();
        }

        private void btnNuevo_Click_1(object sender, EventArgs e)
        {
            Limpiar();
            ckbEstado.Checked = true;
            btnGrabar.Enabled = true;
            tabControl1.SelectedTab = tabPage1;
        }

        private void dgvSegmentos_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.ColumnIndex >= 0 && this.dgvSegmentos.Columns[e.ColumnIndex].Name == "btnSegmentosUpd" && e.RowIndex >= 0)
            {
                e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                DataGridViewButtonCell celBoton = this.dgvSegmentos.Rows[e.RowIndex].Cells["btnSegmentosUpd"] as DataGridViewButtonCell;
                Icon icoModificar = new Icon(@"C:\Windows\sisclinica\Ico\Style_file.ico");
                e.Graphics.DrawIcon(icoModificar, e.CellBounds.Left + 3, e.CellBounds.Top + 3);

                this.dgvSegmentos.Rows[e.RowIndex].Height = icoModificar.Height + 10;
                this.dgvSegmentos.Columns[e.ColumnIndex].Width = icoModificar.Width + 10;

                e.Handled = true;
            }
        }

        private void btnNuevoSegmento_Click(object sender, EventArgs e)
        {
            txtSegmento.Text = "";
            txtSegmento.Enabled = true;
            ckbSegmentoActivo.Checked = true;
            ckbSegmentoActivo.Enabled = false;
            btnGrabaSegmento.Enabled = true;
            btnGrabaSegmento.Text = "Agregar";
            txtSegmento.Focus();
        }

        private void btnGrabaSegmento_Click(object sender, EventArgs e)
        {
            oSegmentos.PartesCuerpoId = int.Parse(lblPartesCuerpoId.Text);
            oSegmentos.SegmentoDescripcion = txtSegmento.Text;
            oSegmentos.Estado = ckbSegmentoActivo.Checked;
            oSegmentos.UsuCrea = 1;
            oSegmentos.UsuModi = 1;
            txtSegmento.Text = "";
            if (string.Equals(btnGrabaSegmento.Text, "Agregar"))
            { obj.SegmentosAdd(oSegmentos); }
            else
            { obj.SegmentosUpd(oSegmentos); }
            btnGrabaSegmento.Text = "Agregar";
            CargardgvSegmentos();
            ckbSegmentoActivo.Checked = false;
            txtSegmento.Enabled = false;
            ckbSegmentoActivo.Enabled = false;
            btnGrabaSegmento.Enabled = false;
        }

        private void dgvSegmentos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (this.dgvSegmentos.Columns[e.ColumnIndex].Name == "btnSegmentosUpd")
            {
                int indice = dgvSegmentos.CurrentCell.RowIndex;
                oSegmentos.SegmentoId = int.Parse(dgvSegmentos.Rows[indice].Cells[1].Value.ToString());
                txtSegmento.Text = dgvSegmentos.Rows[indice].Cells[2].Value.ToString();
                ckbSegmentoActivo.Checked = bool.Parse(dgvSegmentos.Rows[indice].Cells[3].Value.ToString());
                txtSegmento.Enabled = true;
                ckbSegmentoActivo.Enabled = true;
                btnGrabaSegmento.Text = "Actualizar";
                btnGrabaSegmento.Enabled = true;
            }
            else
            {
                txtSegmento.Text = "";
                ckbSegmentoActivo.Checked = false;
                ckbSegmentoActivo.Enabled = false;
                btnGrabaSegmento.Text = "Agregar";
                btnNuevoSegmento.Enabled = true;
                btnGrabaSegmento.Enabled = false;
                return;
            }
        }

        private void btnActEstado_Click(object sender, EventArgs e)
        {
            ckbEstado.Checked = !ckbEstado.Checked;
            Actualizar();
        }

        private void cmbServicio_SelectedIndexChanged(object sender, EventArgs e)
        {
            try { CargardgvPartesCuerpo(); }
            catch { }
        }
    }
}
