﻿namespace pjDermaBelle.Formularios.Utilitarios
{
    partial class frmPartesCuerpo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.txtPartesCuerpoOrden = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ckbEstado = new System.Windows.Forms.CheckBox();
            this.btnSalir = new System.Windows.Forms.Button();
            this.btnActEstado = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ckbSegmentoActivo = new System.Windows.Forms.CheckBox();
            this.btnNuevoSegmento = new System.Windows.Forms.Button();
            this.btnGrabaSegmento = new System.Windows.Forms.Button();
            this.txtSegmento = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.dgvSegmentos = new System.Windows.Forms.DataGridView();
            this.btnSegmentosUpd = new System.Windows.Forms.DataGridViewButtonColumn();
            this.btnModificar = new System.Windows.Forms.Button();
            this.btnGrabar = new System.Windows.Forms.Button();
            this.cmbServicioId = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPartesCuerpoDescripcion = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblPartesCuerpoId = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnNuevo = new System.Windows.Forms.Button();
            this.cmbServicio = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dgvPartesCuerpo = new System.Windows.Forms.DataGridView();
            this.pbxParteCuerpo = new System.Windows.Forms.PictureBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSegmentos)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPartesCuerpo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxParteCuerpo)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(4, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(752, 427);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.txtPartesCuerpoOrden);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.ckbEstado);
            this.tabPage1.Controls.Add(this.btnSalir);
            this.tabPage1.Controls.Add(this.btnActEstado);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.btnModificar);
            this.tabPage1.Controls.Add(this.btnGrabar);
            this.tabPage1.Controls.Add(this.cmbServicioId);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.txtPartesCuerpoDescripcion);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.lblPartesCuerpoId);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(744, 401);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Registro";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // txtPartesCuerpoOrden
            // 
            this.txtPartesCuerpoOrden.Location = new System.Drawing.Point(176, 84);
            this.txtPartesCuerpoOrden.Name = "txtPartesCuerpoOrden";
            this.txtPartesCuerpoOrden.Size = new System.Drawing.Size(42, 20);
            this.txtPartesCuerpoOrden.TabIndex = 30;
            this.txtPartesCuerpoOrden.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(99, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 29;
            this.label2.Text = "Nro. de Orden";
            // 
            // ckbEstado
            // 
            this.ckbEstado.AutoSize = true;
            this.ckbEstado.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ckbEstado.Enabled = false;
            this.ckbEstado.Location = new System.Drawing.Point(136, 109);
            this.ckbEstado.Name = "ckbEstado";
            this.ckbEstado.Size = new System.Drawing.Size(56, 17);
            this.ckbEstado.TabIndex = 28;
            this.ckbEstado.Text = "Activo";
            this.ckbEstado.UseVisualStyleBackColor = true;
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(584, 103);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(156, 23);
            this.btnSalir.TabIndex = 27;
            this.btnSalir.Text = "Salir";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // btnActEstado
            // 
            this.btnActEstado.Location = new System.Drawing.Point(584, 72);
            this.btnActEstado.Name = "btnActEstado";
            this.btnActEstado.Size = new System.Drawing.Size(156, 23);
            this.btnActEstado.TabIndex = 26;
            this.btnActEstado.Text = "Activar / Desactivar";
            this.btnActEstado.UseVisualStyleBackColor = true;
            this.btnActEstado.Click += new System.EventHandler(this.btnActEstado_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ckbSegmentoActivo);
            this.groupBox1.Controls.Add(this.btnNuevoSegmento);
            this.groupBox1.Controls.Add(this.btnGrabaSegmento);
            this.groupBox1.Controls.Add(this.txtSegmento);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.btnBuscar);
            this.groupBox1.Controls.Add(this.dgvSegmentos);
            this.groupBox1.Controls.Add(this.pbxParteCuerpo);
            this.groupBox1.Location = new System.Drawing.Point(6, 132);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(734, 264);
            this.groupBox1.TabIndex = 25;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Detalle de Segmentos";
            // 
            // ckbSegmentoActivo
            // 
            this.ckbSegmentoActivo.AutoSize = true;
            this.ckbSegmentoActivo.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ckbSegmentoActivo.Enabled = false;
            this.ckbSegmentoActivo.Location = new System.Drawing.Point(235, 55);
            this.ckbSegmentoActivo.Name = "ckbSegmentoActivo";
            this.ckbSegmentoActivo.Size = new System.Drawing.Size(56, 17);
            this.ckbSegmentoActivo.TabIndex = 29;
            this.ckbSegmentoActivo.Text = "Activo";
            this.ckbSegmentoActivo.UseVisualStyleBackColor = true;
            // 
            // btnNuevoSegmento
            // 
            this.btnNuevoSegmento.Location = new System.Drawing.Point(536, 51);
            this.btnNuevoSegmento.Name = "btnNuevoSegmento";
            this.btnNuevoSegmento.Size = new System.Drawing.Size(93, 23);
            this.btnNuevoSegmento.TabIndex = 27;
            this.btnNuevoSegmento.Text = "Nuevo";
            this.btnNuevoSegmento.UseVisualStyleBackColor = true;
            this.btnNuevoSegmento.Click += new System.EventHandler(this.btnNuevoSegmento_Click);
            // 
            // btnGrabaSegmento
            // 
            this.btnGrabaSegmento.Location = new System.Drawing.Point(635, 51);
            this.btnGrabaSegmento.Name = "btnGrabaSegmento";
            this.btnGrabaSegmento.Size = new System.Drawing.Size(93, 23);
            this.btnGrabaSegmento.TabIndex = 26;
            this.btnGrabaSegmento.Text = "Grabar";
            this.btnGrabaSegmento.UseVisualStyleBackColor = true;
            this.btnGrabaSegmento.Click += new System.EventHandler(this.btnGrabaSegmento_Click);
            // 
            // txtSegmento
            // 
            this.txtSegmento.Location = new System.Drawing.Point(277, 25);
            this.txtSegmento.Name = "txtSegmento";
            this.txtSegmento.Size = new System.Drawing.Size(451, 20);
            this.txtSegmento.TabIndex = 25;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(218, 29);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 13);
            this.label6.TabIndex = 24;
            this.label6.Text = "Segmento";
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(6, 232);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(206, 23);
            this.btnBuscar.TabIndex = 11;
            this.btnBuscar.Text = "Buscar Imagen";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // dgvSegmentos
            // 
            this.dgvSegmentos.AllowUserToAddRows = false;
            this.dgvSegmentos.AllowUserToDeleteRows = false;
            this.dgvSegmentos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSegmentos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.btnSegmentosUpd});
            this.dgvSegmentos.Location = new System.Drawing.Point(218, 76);
            this.dgvSegmentos.Name = "dgvSegmentos";
            this.dgvSegmentos.ReadOnly = true;
            this.dgvSegmentos.Size = new System.Drawing.Size(510, 149);
            this.dgvSegmentos.TabIndex = 10;
            this.dgvSegmentos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSegmentos_CellClick);
            this.dgvSegmentos.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgvSegmentos_CellPainting);
            // 
            // btnSegmentosUpd
            // 
            this.btnSegmentosUpd.HeaderText = "";
            this.btnSegmentosUpd.Name = "btnSegmentosUpd";
            this.btnSegmentosUpd.ReadOnly = true;
            // 
            // btnModificar
            // 
            this.btnModificar.Location = new System.Drawing.Point(584, 41);
            this.btnModificar.Name = "btnModificar";
            this.btnModificar.Size = new System.Drawing.Size(156, 23);
            this.btnModificar.TabIndex = 24;
            this.btnModificar.Text = "Modificar";
            this.btnModificar.UseVisualStyleBackColor = true;
            this.btnModificar.Click += new System.EventHandler(this.btnModificar_Click);
            // 
            // btnGrabar
            // 
            this.btnGrabar.Location = new System.Drawing.Point(584, 10);
            this.btnGrabar.Name = "btnGrabar";
            this.btnGrabar.Size = new System.Drawing.Size(156, 23);
            this.btnGrabar.TabIndex = 23;
            this.btnGrabar.Text = "Grabar";
            this.btnGrabar.UseVisualStyleBackColor = true;
            this.btnGrabar.Click += new System.EventHandler(this.btnGrabar_Click);
            // 
            // cmbServicioId
            // 
            this.cmbServicioId.FormattingEnabled = true;
            this.cmbServicioId.Location = new System.Drawing.Point(176, 56);
            this.cmbServicioId.Name = "cmbServicioId";
            this.cmbServicioId.Size = new System.Drawing.Size(271, 21);
            this.cmbServicioId.TabIndex = 21;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(128, 60);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "Servicio";
            // 
            // txtPartesCuerpoDescripcion
            // 
            this.txtPartesCuerpoDescripcion.Location = new System.Drawing.Point(176, 29);
            this.txtPartesCuerpoDescripcion.Name = "txtPartesCuerpoDescripcion";
            this.txtPartesCuerpoDescripcion.Size = new System.Drawing.Size(360, 20);
            this.txtPartesCuerpoDescripcion.TabIndex = 19;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 33);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(149, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "Parte del Examen Preferencial";
            // 
            // lblPartesCuerpoId
            // 
            this.lblPartesCuerpoId.AutoSize = true;
            this.lblPartesCuerpoId.ForeColor = System.Drawing.Color.Blue;
            this.lblPartesCuerpoId.Location = new System.Drawing.Point(176, 9);
            this.lblPartesCuerpoId.Name = "lblPartesCuerpoId";
            this.lblPartesCuerpoId.Size = new System.Drawing.Size(72, 13);
            this.lblPartesCuerpoId.TabIndex = 17;
            this.lblPartesCuerpoId.Text = "Id Automático";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(108, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Identificador";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnNuevo);
            this.tabPage2.Controls.Add(this.cmbServicio);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.dgvPartesCuerpo);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(744, 401);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Listado";
            this.tabPage2.UseVisualStyleBackColor = true;
            this.tabPage2.Enter += new System.EventHandler(this.tabPage2_Enter);
            // 
            // btnNuevo
            // 
            this.btnNuevo.Location = new System.Drawing.Point(582, 8);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(156, 23);
            this.btnNuevo.TabIndex = 24;
            this.btnNuevo.Text = "Nuevo";
            this.btnNuevo.UseVisualStyleBackColor = true;
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click_1);
            // 
            // cmbServicio
            // 
            this.cmbServicio.FormattingEnabled = true;
            this.cmbServicio.Location = new System.Drawing.Point(52, 9);
            this.cmbServicio.Name = "cmbServicio";
            this.cmbServicio.Size = new System.Drawing.Size(271, 21);
            this.cmbServicio.TabIndex = 23;
            this.cmbServicio.SelectedIndexChanged += new System.EventHandler(this.cmbServicio_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 13);
            this.label5.TabIndex = 22;
            this.label5.Text = "Servicio";
            // 
            // dgvPartesCuerpo
            // 
            this.dgvPartesCuerpo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPartesCuerpo.Location = new System.Drawing.Point(6, 39);
            this.dgvPartesCuerpo.Name = "dgvPartesCuerpo";
            this.dgvPartesCuerpo.Size = new System.Drawing.Size(732, 356);
            this.dgvPartesCuerpo.TabIndex = 0;
            this.dgvPartesCuerpo.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPartesCuerpo_CellClick);
            // 
            // pbxParteCuerpo
            // 
            this.pbxParteCuerpo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbxParteCuerpo.Location = new System.Drawing.Point(6, 19);
            this.pbxParteCuerpo.Name = "pbxParteCuerpo";
            this.pbxParteCuerpo.Size = new System.Drawing.Size(206, 206);
            this.pbxParteCuerpo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxParteCuerpo.TabIndex = 9;
            this.pbxParteCuerpo.TabStop = false;
            // 
            // frmPartesCuerpo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(758, 433);
            this.Controls.Add(this.tabControl1);
            this.Name = "frmPartesCuerpo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mantenimiento Partes del Examen Preferencial";
            this.Load += new System.EventHandler(this.frmPartesCuerpo_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSegmentos)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPartesCuerpo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxParteCuerpo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.CheckBox ckbEstado;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.Button btnActEstado;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.DataGridView dgvSegmentos;
        private System.Windows.Forms.PictureBox pbxParteCuerpo;
        private System.Windows.Forms.Button btnModificar;
        private System.Windows.Forms.Button btnGrabar;
        private System.Windows.Forms.ComboBox cmbServicioId;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtPartesCuerpoDescripcion;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblPartesCuerpoId;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ComboBox cmbServicio;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView dgvPartesCuerpo;
        private System.Windows.Forms.TextBox txtPartesCuerpoOrden;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnNuevo;
        private System.Windows.Forms.Button btnNuevoSegmento;
        private System.Windows.Forms.Button btnGrabaSegmento;
        private System.Windows.Forms.TextBox txtSegmento;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridViewButtonColumn btnSegmentosUpd;
        private System.Windows.Forms.CheckBox ckbSegmentoActivo;

    }
}