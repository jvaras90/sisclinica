﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pjDermaBelle.Formularios.Citas
{
    public partial class frmSesiones : Form
    {
        public frmSesiones()
        {
            InitializeComponent();
        }

        public SqlConnection cn = new SqlConnection("data source = PC; integrated security = true; initial catalog = DermaBelle; user id = DBUser; password = dbuser");

        public string campos;
        public string tablas;
        public string valores;
        public string condicion;

        public string consulta;
        public string PacHc;
        public string TarCod;

        public short SerId;
        public short ConId;
        public short MedId;

        public string PacNombreComp;
        public string PacTelefonos;
        public decimal TipoCambio;

        void CargarLista()
        {
            campos = "NumSesion As Sesión, Convert(char(10),CitaFecha,103) As Fecha, Left(CitaHora,5) As Hora, Observacion As Observaciones";
            tablas = "Citas";
            condicion = "Pac_HC = '" + PacHc + "' And Tar_Cod = '" + TarCod + "'";

            consulta = "Select " + campos + " From " + tablas + " Where " + condicion;
            cn.Open();
            SqlDataAdapter daCitas = new SqlDataAdapter(consulta, cn);
            DataSet dsCitas = new DataSet();
            daCitas.Fill(dsCitas, "Citas");

            dgvSesiones.DataSource = dsCitas;
            dgvSesiones.DataMember = "Citas";

            for (int indice = 0; indice < dgvSesiones.Rows.Count; indice++)
            {
                dgvSesiones.Columns[0].Width = 50;        //Sesion
                dgvSesiones.Columns[1].Width = 100;       //Fecha
                dgvSesiones.Columns[2].Width = 50;        //Hora
                dgvSesiones.Columns[3].Width = 540;       //Observacion
            }

            cn.Close(); 
        }

        void Limpiar()
        {
            txtNumSesiones.Text = "";
            txtObservaciones.Text = "";
        }

        private void frmSesiones_Load(object sender, EventArgs e)
        {
            CargarLista();
        }

        private void dtpInicio_ValueChanged(object sender, EventArgs e)
        {
            dtpFinal.Value = dtpInicio.Value.AddMinutes(int.Parse(txtDuracion.Text));
        }

        private void txtDuracion_TextChanged(object sender, EventArgs e)
        {
            dtpFinal.Value = dtpInicio.Value.AddMinutes(int.Parse(txtDuracion.Text));
        }

        private void btnGrabar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtNumSesiones.Text) ||
                string.IsNullOrEmpty(txtDuracion.Text))
            {
                MessageBox.Show("El Numero de Sesión o la Duración estan vacios. Por favor revise");
                return;
            }

            try
            {
                SqlCommand comando = new SqlCommand();
                comando.Connection = cn;

                tablas = "Citas";
                campos = "(CitaFecha, CitaHora, CitaDuracion, Pac_HC, Ser_Id, Con_Id, Med_Id, CitaEstado, Observacion, FecCrea, UsuCrea, FecModi, UsuModi, Tar_Cod, Pac_NomComp, Pac_Telefonos, TipoCambio, NumSesion, Tar_Valor)";
                valores = "(Cast(Left('" + dtpFecha.Value + "',20) As Date), Cast(Left('" + dtpInicio.Value + "',20) As Time), " + int.Parse(txtDuracion.Text) + ", '" + PacHc + "', " + SerId + ", " + ConId + ", " + MedId + ", 'R', '" + txtObservaciones.Text + "', GetDate(), 1, GetDate(), 1, '" + TarCod + "', '" + PacNombreComp + "', '" + PacTelefonos + "', " + TipoCambio + ", " + int.Parse(txtNumSesiones.Text) + ", 0)";

                consulta = "Insert Into " + tablas + " " + campos + " Values " + valores;

                txtObservaciones.Text = consulta;

                comando.CommandText = consulta;
                cn.Open();
                int nFilas = comando.ExecuteNonQuery();
                if (nFilas > 0)
                {
                    //MessageBox.Show("Registro AGREGADO correctamente");
                    cn.Close();
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show("ERROR: " + ex.Message);
            }
            Limpiar();
            CargarLista();
        }
    }
}
