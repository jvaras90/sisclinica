﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pjDermaBelle.Formularios.Citas
{
    public partial class frmCajaOperaciones : Form
    {
        public frmCajaOperaciones()
        {
            InitializeComponent();
        }

        ClaseDatos obj = new ClaseDatos();
        DermaBelleDataDataContext linq = new DermaBelleDataDataContext();
        Caja_Cabecera oCabecera = new Caja_Cabecera();
        Caja_Detalle oDetalle = new Caja_Detalle();
        frm_Reserva reserva = new frm_Reserva();

        public bool esreserva;
        public int userId;
        public string pPac_HC;

        void Documentos()
        {
            cboTDocFact.DataSource = obj.CboDocFact();
            cboTDocFact.ValueMember = "TipoDocumentoId";
            cboTDocFact.DisplayMember = "TipoDocumentoNombre";
        }

        void CargarCliente()
        {
            if(!string.Equals(pPac_HC,"9999999999"))
            {
                List<usp_PacientesRecResult> lista = new List<usp_PacientesRecResult>();
                lista = obj.PacientesDatos(pPac_HC);
                foreach (usp_PacientesRecResult reg in lista)
                {
                    txtCajaClienteNombre.Text = reg.Pac_NombreFact;
                    txtCajaClienteRUC.Text = reg.Pac_RUCFact;
                    txtCajaClienteDireccion.Text = reg.Pac_DirecFact;
                }
            }
        }

        void CalcularTotales()
        {
            decimal subtotal = 0;
            decimal total = 0;
            decimal igv = 0;

            foreach (DataGridViewRow row in dgvDetalleCaja.Rows)
            {
                total += Decimal.Parse(row.Cells[12].Value.ToString());
            }

            subtotal = Math.Round((Decimal.Divide(total, decimal.Parse("1.18"))),2);
            igv = total - subtotal;
            txtSubTotal.Text = string.Format("{0:N2}", subtotal);
            txtIGV.Text = string.Format("{0:N2}", Math.Round(igv, 2));
            txtTotal.Text = total.ToString();
        }

        private void frmCajaOperaciones_Load(object sender, EventArgs e)
        {
            Documentos();
            foreach (DataGridViewRow row in dgvDetalleCaja.Rows)
            {
                row.Cells[0] = new DataGridViewButtonCell();
                ((DataGridViewButtonCell)row.Cells[0]).ToolTipText = "Eliminar Item";

                row.Cells[1] = new DataGridViewButtonCell();
                ((DataGridViewButtonCell)row.Cells[1]).ToolTipText = "Modificar Item";
            }

            CargarCliente();
            CalcularTotales();
        }

        private void dgvDetalleCaja_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (this.dgvDetalleCaja.Columns[e.ColumnIndex].Name == "btnDel")
            {
                MessageBox.Show("Eliminar");
            }

            if (this.dgvDetalleCaja.Columns[e.ColumnIndex].Name == "btnUpd")
            {
                gpbItemSelec.Enabled = true;

                DataGridViewRow row = dgvDetalleCaja.CurrentRow;
                if (row != null)
                {
                    txtDescripcion.Text = row.Cells["Descripcion"].Value.ToString();
                    txtCantidad.Text = row.Cells["CajaItemCantidad"].Value.ToString();
                    txtPrecioFinalSol.Text = row.Cells["CajaItemPrecioFinal"].Value.ToString();
                    txtPrecioFinalDol.Text = string.Format("{0:N2}",Math.Round(decimal.Divide(decimal.Parse(txtPrecioFinalSol.Text), decimal.Parse(txtTCambio.Text)),2));
                }                      
            }
        }

        private void dgvDetalleCaja_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.ColumnIndex >= 0 && this.dgvDetalleCaja.Columns[e.ColumnIndex].Name == "btnDel" && e.RowIndex >= 0)
            {
                e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                DataGridViewButtonCell celBoton = this.dgvDetalleCaja.Rows[e.RowIndex].Cells["btnDel"] as DataGridViewButtonCell;
                Icon icoEliminar = new Icon(@"C:\Windows\sisclinica\Ico\Delete.ico");
                e.Graphics.DrawIcon(icoEliminar, e.CellBounds.Left + 3, e.CellBounds.Top + 3);

                this.dgvDetalleCaja.Rows[e.RowIndex].Height = icoEliminar.Height + 10;
                this.dgvDetalleCaja.Columns[e.ColumnIndex].Width = icoEliminar.Width + 10;

                e.Handled = true;
            }

            if (e.ColumnIndex >= 0 && this.dgvDetalleCaja.Columns[e.ColumnIndex].Name == "btnUpd" && e.RowIndex >= 0)
            {
                e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                DataGridViewButtonCell celBoton = this.dgvDetalleCaja.Rows[e.RowIndex].Cells["btnUpd"] as DataGridViewButtonCell;
                Icon icoModificar = new Icon(@"C:\Windows\sisclinica\Ico\Style_file.ico");
                e.Graphics.DrawIcon(icoModificar, e.CellBounds.Left + 3, e.CellBounds.Top + 3);

                this.dgvDetalleCaja.Rows[e.RowIndex].Height = icoModificar.Height + 10;
                this.dgvDetalleCaja.Columns[e.ColumnIndex].Width = icoModificar.Width + 10;

                e.Handled = true;
            }
        }

        private void rbtSoles_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtSoles.Checked)
            {
                rbtDolares.Checked = false;
                frmClavePrecios clave = new frmClavePrecios();
                clave.ShowDialog();
                if (clave.validado)
                {
                    txtPrecioFinalSol.Enabled = rbtSoles.Checked;
                    txtPrecioFinalDol.Enabled = rbtDolares.Checked;
                    txtPrecioFinalSol.Focus();
                }
                else
                {
                    MessageBox.Show("Usuario no autorizado para modificar precios");
                }
            }
        }

        private void rbtDolares_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtDolares.Checked)
            {
                rbtSoles.Checked = false;
                frmClavePrecios clave = new frmClavePrecios();
                clave.ShowDialog();
                if (clave.validado)
                {
                    txtPrecioFinalSol.Enabled = rbtSoles.Checked;
                    txtPrecioFinalDol.Enabled = rbtDolares.Checked;
                    txtPrecioFinalDol.Focus();
                }
                else
                {
                    MessageBox.Show("Usuario no autorizado para modificar precios");
                }
            }
        }

        int ObtenerSec()
        {
            int sec = 0;
            List<usp_CajaSecuenciaResult> lista = new List<usp_CajaSecuenciaResult>();
            lista = obj.CajaSecuencia();

            if (lista.Count() > 0)
            {
                foreach (usp_CajaSecuenciaResult reg in lista)
                {
                    sec =  reg.CajaId;
                }
            }

            return sec;
        }

        private void btnCobrar_Click(object sender, EventArgs e)
        {
            oCabecera.CajaOrigen = "CE";
            oCabecera.CajaNumReferencia = int.Parse(txtCitaId.Text);
            oCabecera.TDocFact = cboTDocFact.SelectedValue.ToString();
            oCabecera.CajaClienteDireccion = txtCajaClienteDireccion.Text;
            oCabecera.CajaSerie = txtCajaSerie.Text;
            oCabecera.CajaCorrelativo = txtCajaCorrelativo.Text;
            oCabecera.CajaFecha = dtpCajaFecha.Value;
            oCabecera.Pac_HC = pPac_HC;
            oCabecera.CajaClienteNombre = txtCajaClienteNombre.Text;
            oCabecera.CajaClienteRUC = txtCajaClienteRUC.Text;
            oCabecera.CajaClienteDireccion = txtCajaClienteDireccion.Text;
            oCabecera.CajaClienteDNI = txtDNI.Text;
            oCabecera.CajaTotal = decimal.Parse(txtTotal.Text);
            oCabecera.CajaIGV = decimal.Parse(txtIGV.Text);
            oCabecera.CajaEstado = "PA";
            oCabecera.UsuCrea = userId;
            oCabecera.UsuModi = userId;

            obj.CajaCabeceraAdd(oCabecera);

            int secCaja = ObtenerSec();
            // Grabar detalle
            for (int indice = 0; indice < dgvDetalleCaja.Rows.Count; indice++)
            {
                oDetalle.CajaId = secCaja;
                oDetalle.CajaItem = indice + 1;
                oDetalle.CajaTipoItem = dgvDetalleCaja.Rows[indice].Cells[3].Value.ToString();
                oDetalle.CajaCodigo = dgvDetalleCaja.Rows[indice].Cells[4].Value.ToString();
                oDetalle.CajaTipoTarifa = dgvDetalleCaja.Rows[indice].Cells[5].Value.ToString();
                oDetalle.CajaItemPrecioSol = decimal.Parse(dgvDetalleCaja.Rows[indice].Cells[7].Value.ToString());
                oDetalle.CajaItemPrecioDol = decimal.Parse(dgvDetalleCaja.Rows[indice].Cells[8].Value.ToString());
                oDetalle.CajaItemIgv = decimal.Parse(dgvDetalleCaja.Rows[indice].Cells[9].Value.ToString());
                oDetalle.CajaItemCantidad = int.Parse(dgvDetalleCaja.Rows[indice].Cells[10].Value.ToString());
                oDetalle.CajaItemPrecioFinal = decimal.Parse(dgvDetalleCaja.Rows[indice].Cells[11].Value.ToString());
                oDetalle.CajaItemTotal = decimal.Parse(dgvDetalleCaja.Rows[indice].Cells[12].Value.ToString());
                oDetalle.UsuCrea = userId;
                oDetalle.UsuModi = userId;

                obj.CajaDetalleAdd(oDetalle);
            }

            if(esreserva)
            {
                reserva.txtCitaId.Text = txtCitaId.Text;
                reserva.Confirmar();
            }
            this.Close();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            reserva.grabar = false;
            this.Close();
        }

        private void cboTDocFact_SelectedIndexChanged(object sender, EventArgs e)
        {

            List<usp_TDocumentoRecResult> lista = new List<usp_TDocumentoRecResult>();
            lista = obj.TipoDocumento(cboTDocFact.SelectedValue.ToString());

            if (lista.Count() > 0)
            {
                foreach (usp_TDocumentoRecResult reg in lista)
                {
                    txtCajaSerie.Text = reg.TipoDocumentoSerie;
                    txtCajaCorrelativo.Text = reg.TipoDocumentoCorrelativo;
                }
            }
        }

        private void ckbCambiar_CheckedChanged(object sender, EventArgs e)
        {
            if (ckbCambiar.Checked)
            {
                txtCajaClienteNombre.Text = "";
                txtCajaClienteRUC.Text = "";
                txtCajaClienteDireccion.Text = "";

                txtCajaClienteNombre.Focus();
            }
            else
            {
                CargarCliente();
            }

            txtCajaClienteNombre.Enabled = ckbCambiar.Checked;
            txtCajaClienteRUC.Enabled = ckbCambiar.Checked;
            txtCajaClienteDireccion.Enabled = ckbCambiar.Checked;
        }

        private void btnGrabar_Click(object sender, EventArgs e)
        {
            DataGridViewRow row = dgvDetalleCaja.CurrentRow;
            if (row != null)
            {
                row.Cells["CajaItemCantidad"].Value = txtCantidad.Text;
                row.Cells["CajaItemPrecioFinal"].Value = Math.Round(decimal.Parse(txtPrecioFinalSol.Text),2);
                row.Cells["CajaItemTotal"].Value = Math.Round(decimal.Parse(txtPrecioFinalSol.Text) * int.Parse(txtCantidad.Text),2);
            }
            CalcularTotales();
            rbtSoles.Checked = false;
            rbtDolares.Checked = false;
            txtDescripcion.Text = "";
            txtCantidad.Text = "";
            txtPrecioFinalSol.Text = "";
            txtPrecioFinalDol.Text = "";
            gpbItemSelec.Enabled = false;
        }

        private void txtPrecioFinalSol_TextChanged(object sender, EventArgs e)
        {
        }

        private void txtPrecioFinalDol_TextChanged(object sender, EventArgs e)
        {
        }

        private void txtPrecioFinalSol_Leave(object sender, EventArgs e)
        {
            if (decimal.Parse(txtPrecioFinalSol.Text) > 0)
            {
                txtPrecioFinalDol.Text = string.Format("{0:N2}", Math.Round(decimal.Divide(decimal.Parse(txtPrecioFinalSol.Text), decimal.Parse(txtTCambio.Text)), 2));
            }
            else
            { txtPrecioFinalDol.Text = "0.00"; }
        }

        private void txtPrecioFinalDol_Leave(object sender, EventArgs e)
        {
            if (decimal.Parse(txtPrecioFinalDol.Text) > 0)
            {
                txtPrecioFinalSol.Text = string.Format("{0:N2}", Math.Round(decimal.Parse(txtPrecioFinalDol.Text) * decimal.Parse(txtTCambio.Text), 2)); 
            }
            else
            { txtPrecioFinalSol.Text = "0.00"; }
        }
    }
}
