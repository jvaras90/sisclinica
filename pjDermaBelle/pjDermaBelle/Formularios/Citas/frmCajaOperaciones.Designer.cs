﻿namespace pjDermaBelle.Formularios.Citas
{
    partial class frmCajaOperaciones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cboTDocFact = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtCajaCorrelativo = new System.Windows.Forms.TextBox();
            this.txtCajaSerie = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCajaId = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpCajaFecha = new System.Windows.Forms.DateTimePicker();
            this.txtCajaClienteNombre = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtCajaClienteRUC = new System.Windows.Forms.TextBox();
            this.lblDocIdent = new System.Windows.Forms.Label();
            this.txtCajaClienteDireccion = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.dgvDetalleCaja = new System.Windows.Forms.DataGridView();
            this.txtTotal = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtIGV = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtSubTotal = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btnCobrar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.gpbItemSelec = new System.Windows.Forms.GroupBox();
            this.btnGrabar = new System.Windows.Forms.Button();
            this.rbtDolares = new System.Windows.Forms.RadioButton();
            this.rbtSoles = new System.Windows.Forms.RadioButton();
            this.txtDescripcion = new System.Windows.Forms.TextBox();
            this.txtPrecioFinalDol = new System.Windows.Forms.TextBox();
            this.txtPrecioFinalSol = new System.Windows.Forms.TextBox();
            this.txtCantidad = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.rbtProductos = new System.Windows.Forms.RadioButton();
            this.rbtServicios = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtTCambio = new System.Windows.Forms.TextBox();
            this.ckbCambiar = new System.Windows.Forms.CheckBox();
            this.txtCitaId = new System.Windows.Forms.TextBox();
            this.txtDNI = new System.Windows.Forms.TextBox();
            this.btnDel = new System.Windows.Forms.DataGridViewButtonColumn();
            this.btnUpd = new System.Windows.Forms.DataGridViewButtonColumn();
            this.CajaItem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CajaTipoItem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CajaCodigo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CajaTipoTarifa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CajaItemPrecioSol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CajaItemPrecioDol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CajaItemIgv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CajaItemCantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CajaItemPrecioFinal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CajaItemTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetalleCaja)).BeginInit();
            this.gpbItemSelec.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // cboTDocFact
            // 
            this.cboTDocFact.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboTDocFact.FormattingEnabled = true;
            this.cboTDocFact.Location = new System.Drawing.Point(794, 12);
            this.cboTDocFact.Name = "cboTDocFact";
            this.cboTDocFact.Size = new System.Drawing.Size(243, 28);
            this.cboTDocFact.TabIndex = 0;
            this.cboTDocFact.SelectedIndexChanged += new System.EventHandler(this.cboTDocFact_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.txtCajaCorrelativo);
            this.groupBox1.Controls.Add(this.txtCajaSerie);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(794, 49);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(242, 123);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(69, 61);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(19, 26);
            this.label16.TabIndex = 3;
            this.label16.Text = "-";
            // 
            // txtCajaCorrelativo
            // 
            this.txtCajaCorrelativo.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCajaCorrelativo.Location = new System.Drawing.Point(89, 60);
            this.txtCajaCorrelativo.Name = "txtCajaCorrelativo";
            this.txtCajaCorrelativo.Size = new System.Drawing.Size(146, 29);
            this.txtCajaCorrelativo.TabIndex = 2;
            this.txtCajaCorrelativo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtCajaSerie
            // 
            this.txtCajaSerie.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCajaSerie.Location = new System.Drawing.Point(8, 60);
            this.txtCajaSerie.Name = "txtCajaSerie";
            this.txtCajaSerie.Size = new System.Drawing.Size(60, 29);
            this.txtCajaSerie.TabIndex = 1;
            this.txtCajaSerie.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(48, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(147, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "RUC 12345678901";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "N° de Operación";
            // 
            // txtCajaId
            // 
            this.txtCajaId.Enabled = false;
            this.txtCajaId.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCajaId.Location = new System.Drawing.Point(129, 25);
            this.txtCajaId.Name = "txtCajaId";
            this.txtCajaId.Size = new System.Drawing.Size(100, 23);
            this.txtCajaId.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(75, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Fecha";
            // 
            // dtpCajaFecha
            // 
            this.dtpCajaFecha.Enabled = false;
            this.dtpCajaFecha.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpCajaFecha.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpCajaFecha.Location = new System.Drawing.Point(129, 56);
            this.dtpCajaFecha.Name = "dtpCajaFecha";
            this.dtpCajaFecha.Size = new System.Drawing.Size(109, 23);
            this.dtpCajaFecha.TabIndex = 5;
            // 
            // txtCajaClienteNombre
            // 
            this.txtCajaClienteNombre.Enabled = false;
            this.txtCajaClienteNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCajaClienteNombre.Location = new System.Drawing.Point(129, 87);
            this.txtCajaClienteNombre.Name = "txtCajaClienteNombre";
            this.txtCajaClienteNombre.Size = new System.Drawing.Size(552, 23);
            this.txtCajaClienteNombre.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(71, 90);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "Cliente";
            // 
            // txtCajaClienteRUC
            // 
            this.txtCajaClienteRUC.Enabled = false;
            this.txtCajaClienteRUC.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCajaClienteRUC.Location = new System.Drawing.Point(129, 118);
            this.txtCajaClienteRUC.Name = "txtCajaClienteRUC";
            this.txtCajaClienteRUC.Size = new System.Drawing.Size(142, 23);
            this.txtCajaClienteRUC.TabIndex = 9;
            // 
            // lblDocIdent
            // 
            this.lblDocIdent.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDocIdent.Location = new System.Drawing.Point(10, 121);
            this.lblDocIdent.Name = "lblDocIdent";
            this.lblDocIdent.Size = new System.Drawing.Size(112, 17);
            this.lblDocIdent.TabIndex = 8;
            this.lblDocIdent.Text = "RUC";
            this.lblDocIdent.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtCajaClienteDireccion
            // 
            this.txtCajaClienteDireccion.Enabled = false;
            this.txtCajaClienteDireccion.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCajaClienteDireccion.Location = new System.Drawing.Point(129, 149);
            this.txtCajaClienteDireccion.Name = "txtCajaClienteDireccion";
            this.txtCajaClienteDireccion.Size = new System.Drawing.Size(552, 23);
            this.txtCajaClienteDireccion.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(55, 152);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 17);
            this.label6.TabIndex = 10;
            this.label6.Text = "Dirección";
            // 
            // dgvDetalleCaja
            // 
            this.dgvDetalleCaja.AllowUserToAddRows = false;
            this.dgvDetalleCaja.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDetalleCaja.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.btnDel,
            this.btnUpd,
            this.CajaItem,
            this.CajaTipoItem,
            this.CajaCodigo,
            this.CajaTipoTarifa,
            this.Descripcion,
            this.CajaItemPrecioSol,
            this.CajaItemPrecioDol,
            this.CajaItemIgv,
            this.CajaItemCantidad,
            this.CajaItemPrecioFinal,
            this.CajaItemTotal});
            this.dgvDetalleCaja.Location = new System.Drawing.Point(10, 354);
            this.dgvDetalleCaja.Name = "dgvDetalleCaja";
            this.dgvDetalleCaja.Size = new System.Drawing.Size(1025, 228);
            this.dgvDetalleCaja.TabIndex = 12;
            this.dgvDetalleCaja.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDetalleCaja_CellClick);
            this.dgvDetalleCaja.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgvDetalleCaja_CellPainting);
            // 
            // txtTotal
            // 
            this.txtTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotal.ForeColor = System.Drawing.Color.Blue;
            this.txtTotal.Location = new System.Drawing.Point(937, 591);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.ReadOnly = true;
            this.txtTotal.Size = new System.Drawing.Size(98, 26);
            this.txtTotal.TabIndex = 18;
            this.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(867, 594);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 20);
            this.label7.TabIndex = 17;
            this.label7.Text = "Total S/.";
            // 
            // txtIGV
            // 
            this.txtIGV.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIGV.ForeColor = System.Drawing.Color.Blue;
            this.txtIGV.Location = new System.Drawing.Point(490, 591);
            this.txtIGV.Name = "txtIGV";
            this.txtIGV.ReadOnly = true;
            this.txtIGV.Size = new System.Drawing.Size(98, 26);
            this.txtIGV.TabIndex = 16;
            this.txtIGV.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(426, 594);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(61, 20);
            this.label8.TabIndex = 15;
            this.label8.Text = "IGV S/.";
            // 
            // txtSubTotal
            // 
            this.txtSubTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSubTotal.ForeColor = System.Drawing.Color.Blue;
            this.txtSubTotal.Location = new System.Drawing.Point(110, 591);
            this.txtSubTotal.Name = "txtSubTotal";
            this.txtSubTotal.ReadOnly = true;
            this.txtSubTotal.Size = new System.Drawing.Size(98, 26);
            this.txtSubTotal.TabIndex = 14;
            this.txtSubTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(7, 594);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 20);
            this.label9.TabIndex = 13;
            this.label9.Text = "Sub Total S/.";
            // 
            // btnCobrar
            // 
            this.btnCobrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCobrar.Location = new System.Drawing.Point(10, 637);
            this.btnCobrar.Name = "btnCobrar";
            this.btnCobrar.Size = new System.Drawing.Size(198, 32);
            this.btnCobrar.TabIndex = 20;
            this.btnCobrar.Text = "Cobrar";
            this.btnCobrar.UseVisualStyleBackColor = true;
            this.btnCobrar.Click += new System.EventHandler(this.btnCobrar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.Location = new System.Drawing.Point(837, 637);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(198, 32);
            this.btnCancelar.TabIndex = 22;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // gpbItemSelec
            // 
            this.gpbItemSelec.Controls.Add(this.btnGrabar);
            this.gpbItemSelec.Controls.Add(this.rbtDolares);
            this.gpbItemSelec.Controls.Add(this.rbtSoles);
            this.gpbItemSelec.Controls.Add(this.txtDescripcion);
            this.gpbItemSelec.Controls.Add(this.txtPrecioFinalDol);
            this.gpbItemSelec.Controls.Add(this.txtPrecioFinalSol);
            this.gpbItemSelec.Controls.Add(this.txtCantidad);
            this.gpbItemSelec.Controls.Add(this.label11);
            this.gpbItemSelec.Controls.Add(this.label10);
            this.gpbItemSelec.Controls.Add(this.rbtProductos);
            this.gpbItemSelec.Controls.Add(this.rbtServicios);
            this.gpbItemSelec.Enabled = false;
            this.gpbItemSelec.Location = new System.Drawing.Point(10, 178);
            this.gpbItemSelec.Name = "gpbItemSelec";
            this.gpbItemSelec.Size = new System.Drawing.Size(730, 170);
            this.gpbItemSelec.TabIndex = 26;
            this.gpbItemSelec.TabStop = false;
            this.gpbItemSelec.Text = "Detalle del Item Seleccionado";
            // 
            // btnGrabar
            // 
            this.btnGrabar.Location = new System.Drawing.Point(502, 141);
            this.btnGrabar.Name = "btnGrabar";
            this.btnGrabar.Size = new System.Drawing.Size(196, 23);
            this.btnGrabar.TabIndex = 37;
            this.btnGrabar.Text = "Grabar cambios";
            this.btnGrabar.UseVisualStyleBackColor = true;
            this.btnGrabar.Click += new System.EventHandler(this.btnGrabar_Click);
            // 
            // rbtDolares
            // 
            this.rbtDolares.AutoSize = true;
            this.rbtDolares.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtDolares.Location = new System.Drawing.Point(490, 98);
            this.rbtDolares.Name = "rbtDolares";
            this.rbtDolares.Size = new System.Drawing.Size(87, 21);
            this.rbtDolares.TabIndex = 36;
            this.rbtDolares.TabStop = true;
            this.rbtDolares.Text = "Dólares $";
            this.rbtDolares.UseVisualStyleBackColor = true;
            this.rbtDolares.CheckedChanged += new System.EventHandler(this.rbtDolares_CheckedChanged);
            // 
            // rbtSoles
            // 
            this.rbtSoles.AutoSize = true;
            this.rbtSoles.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtSoles.Location = new System.Drawing.Point(256, 98);
            this.rbtSoles.Name = "rbtSoles";
            this.rbtSoles.Size = new System.Drawing.Size(82, 21);
            this.rbtSoles.TabIndex = 35;
            this.rbtSoles.TabStop = true;
            this.rbtSoles.Text = "Soles S/.";
            this.rbtSoles.UseVisualStyleBackColor = true;
            this.rbtSoles.CheckedChanged += new System.EventHandler(this.rbtSoles_CheckedChanged);
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.Enabled = false;
            this.txtDescripcion.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescripcion.Location = new System.Drawing.Point(118, 55);
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.ReadOnly = true;
            this.txtDescripcion.Size = new System.Drawing.Size(579, 23);
            this.txtDescripcion.TabIndex = 28;
            // 
            // txtPrecioFinalDol
            // 
            this.txtPrecioFinalDol.Enabled = false;
            this.txtPrecioFinalDol.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrecioFinalDol.Location = new System.Drawing.Point(583, 97);
            this.txtPrecioFinalDol.Name = "txtPrecioFinalDol";
            this.txtPrecioFinalDol.Size = new System.Drawing.Size(115, 23);
            this.txtPrecioFinalDol.TabIndex = 32;
            this.txtPrecioFinalDol.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPrecioFinalDol.TextChanged += new System.EventHandler(this.txtPrecioFinalDol_TextChanged);
            this.txtPrecioFinalDol.Leave += new System.EventHandler(this.txtPrecioFinalDol_Leave);
            // 
            // txtPrecioFinalSol
            // 
            this.txtPrecioFinalSol.Enabled = false;
            this.txtPrecioFinalSol.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrecioFinalSol.Location = new System.Drawing.Point(344, 97);
            this.txtPrecioFinalSol.Name = "txtPrecioFinalSol";
            this.txtPrecioFinalSol.Size = new System.Drawing.Size(115, 23);
            this.txtPrecioFinalSol.TabIndex = 30;
            this.txtPrecioFinalSol.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPrecioFinalSol.TextChanged += new System.EventHandler(this.txtPrecioFinalSol_TextChanged);
            this.txtPrecioFinalSol.Leave += new System.EventHandler(this.txtPrecioFinalSol_Leave);
            // 
            // txtCantidad
            // 
            this.txtCantidad.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCantidad.Location = new System.Drawing.Point(120, 97);
            this.txtCantidad.Name = "txtCantidad";
            this.txtCantidad.Size = new System.Drawing.Size(70, 23);
            this.txtCantidad.TabIndex = 28;
            this.txtCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(49, 100);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(64, 17);
            this.label11.TabIndex = 27;
            this.label11.Text = "Cantidad";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(30, 58);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(82, 17);
            this.label10.TabIndex = 7;
            this.label10.Text = "Descripción";
            // 
            // rbtProductos
            // 
            this.rbtProductos.AutoSize = true;
            this.rbtProductos.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtProductos.Location = new System.Drawing.Point(136, 20);
            this.rbtProductos.Name = "rbtProductos";
            this.rbtProductos.Size = new System.Drawing.Size(90, 21);
            this.rbtProductos.TabIndex = 1;
            this.rbtProductos.Text = "Productos";
            this.rbtProductos.UseVisualStyleBackColor = true;
            // 
            // rbtServicios
            // 
            this.rbtServicios.AutoSize = true;
            this.rbtServicios.Checked = true;
            this.rbtServicios.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtServicios.Location = new System.Drawing.Point(7, 20);
            this.rbtServicios.Name = "rbtServicios";
            this.rbtServicios.Size = new System.Drawing.Size(83, 21);
            this.rbtServicios.TabIndex = 0;
            this.rbtServicios.TabStop = true;
            this.rbtServicios.Text = "Servicios";
            this.rbtServicios.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.txtTCambio);
            this.groupBox3.Location = new System.Drawing.Point(762, 178);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(273, 170);
            this.groupBox3.TabIndex = 27;
            this.groupBox3.TabStop = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(77, 42);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(119, 20);
            this.label12.TabIndex = 1;
            this.label12.Text = "Tipo de Cambio";
            // 
            // txtTCambio
            // 
            this.txtTCambio.Enabled = false;
            this.txtTCambio.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTCambio.Location = new System.Drawing.Point(21, 89);
            this.txtTCambio.Name = "txtTCambio";
            this.txtTCambio.Size = new System.Drawing.Size(231, 44);
            this.txtTCambio.TabIndex = 0;
            this.txtTCambio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ckbCambiar
            // 
            this.ckbCambiar.AutoSize = true;
            this.ckbCambiar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckbCambiar.Location = new System.Drawing.Point(695, 88);
            this.ckbCambiar.Name = "ckbCambiar";
            this.ckbCambiar.Size = new System.Drawing.Size(79, 21);
            this.ckbCambiar.TabIndex = 28;
            this.ckbCambiar.Text = "Cambiar";
            this.ckbCambiar.UseVisualStyleBackColor = true;
            this.ckbCambiar.CheckedChanged += new System.EventHandler(this.ckbCambiar_CheckedChanged);
            // 
            // txtCitaId
            // 
            this.txtCitaId.Location = new System.Drawing.Point(466, 17);
            this.txtCitaId.Name = "txtCitaId";
            this.txtCitaId.Size = new System.Drawing.Size(100, 20);
            this.txtCitaId.TabIndex = 29;
            this.txtCitaId.Visible = false;
            // 
            // txtDNI
            // 
            this.txtDNI.Enabled = false;
            this.txtDNI.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDNI.Location = new System.Drawing.Point(345, 118);
            this.txtDNI.Name = "txtDNI";
            this.txtDNI.Size = new System.Drawing.Size(142, 23);
            this.txtDNI.TabIndex = 31;
            // 
            // btnDel
            // 
            this.btnDel.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.btnDel.HeaderText = "";
            this.btnDel.MinimumWidth = 20;
            this.btnDel.Name = "btnDel";
            this.btnDel.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.btnDel.Text = "X";
            this.btnDel.ToolTipText = "Eliminar Item";
            this.btnDel.Width = 20;
            // 
            // btnUpd
            // 
            this.btnUpd.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.btnUpd.HeaderText = "";
            this.btnUpd.MinimumWidth = 20;
            this.btnUpd.Name = "btnUpd";
            this.btnUpd.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.btnUpd.Text = "Ç";
            this.btnUpd.ToolTipText = "Modiificar Item";
            this.btnUpd.Width = 20;
            // 
            // CajaItem
            // 
            this.CajaItem.HeaderText = "Item";
            this.CajaItem.MinimumWidth = 35;
            this.CajaItem.Name = "CajaItem";
            this.CajaItem.ReadOnly = true;
            this.CajaItem.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.CajaItem.Width = 35;
            // 
            // CajaTipoItem
            // 
            this.CajaTipoItem.HeaderText = "TipoItem";
            this.CajaTipoItem.Name = "CajaTipoItem";
            this.CajaTipoItem.Visible = false;
            // 
            // CajaCodigo
            // 
            this.CajaCodigo.HeaderText = "Código";
            this.CajaCodigo.Name = "CajaCodigo";
            // 
            // CajaTipoTarifa
            // 
            this.CajaTipoTarifa.HeaderText = "TipoTarifa";
            this.CajaTipoTarifa.Name = "CajaTipoTarifa";
            this.CajaTipoTarifa.Visible = false;
            // 
            // Descripcion
            // 
            this.Descripcion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Descripcion.HeaderText = "Descripción";
            this.Descripcion.Name = "Descripcion";
            this.Descripcion.ReadOnly = true;
            this.Descripcion.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Descripcion.Width = 500;
            // 
            // CajaItemPrecioSol
            // 
            this.CajaItemPrecioSol.HeaderText = "CajaItemPrecioSol";
            this.CajaItemPrecioSol.Name = "CajaItemPrecioSol";
            this.CajaItemPrecioSol.Visible = false;
            // 
            // CajaItemPrecioDol
            // 
            this.CajaItemPrecioDol.HeaderText = "CajaItemPrecioDol";
            this.CajaItemPrecioDol.Name = "CajaItemPrecioDol";
            this.CajaItemPrecioDol.Visible = false;
            // 
            // CajaItemIgv
            // 
            this.CajaItemIgv.HeaderText = "CajaItemIgv";
            this.CajaItemIgv.Name = "CajaItemIgv";
            this.CajaItemIgv.Visible = false;
            // 
            // CajaItemCantidad
            // 
            this.CajaItemCantidad.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.CajaItemCantidad.HeaderText = "Cantidad";
            this.CajaItemCantidad.Name = "CajaItemCantidad";
            this.CajaItemCantidad.ReadOnly = true;
            this.CajaItemCantidad.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // CajaItemPrecioFinal
            // 
            this.CajaItemPrecioFinal.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.CajaItemPrecioFinal.HeaderText = "Precio";
            this.CajaItemPrecioFinal.Name = "CajaItemPrecioFinal";
            this.CajaItemPrecioFinal.ReadOnly = true;
            this.CajaItemPrecioFinal.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // CajaItemTotal
            // 
            this.CajaItemTotal.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.CajaItemTotal.HeaderText = "Total";
            this.CajaItemTotal.Name = "CajaItemTotal";
            this.CajaItemTotal.ReadOnly = true;
            this.CajaItemTotal.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // frmCajaOperaciones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1049, 681);
            this.Controls.Add(this.txtDNI);
            this.Controls.Add(this.txtCitaId);
            this.Controls.Add(this.ckbCambiar);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.gpbItemSelec);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnCobrar);
            this.Controls.Add(this.txtTotal);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtIGV);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtSubTotal);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.dgvDetalleCaja);
            this.Controls.Add(this.txtCajaClienteDireccion);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtCajaClienteRUC);
            this.Controls.Add(this.lblDocIdent);
            this.Controls.Add(this.txtCajaClienteNombre);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.dtpCajaFecha);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtCajaId);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cboTDocFact);
            this.Name = "frmCajaOperaciones";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Operaciones de Caja";
            this.Load += new System.EventHandler(this.frmCajaOperaciones_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetalleCaja)).EndInit();
            this.gpbItemSelec.ResumeLayout(false);
            this.gpbItemSelec.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cboTDocFact;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtCajaCorrelativo;
        private System.Windows.Forms.TextBox txtCajaSerie;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCajaId;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dtpCajaFecha;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblDocIdent;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtTotal;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtIGV;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtSubTotal;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnCobrar;
        private System.Windows.Forms.Button btnCancelar;
        public System.Windows.Forms.TextBox txtCajaClienteNombre;
        public System.Windows.Forms.DataGridView dgvDetalleCaja;
        private System.Windows.Forms.GroupBox gpbItemSelec;
        private System.Windows.Forms.TextBox txtPrecioFinalDol;
        private System.Windows.Forms.TextBox txtPrecioFinalSol;
        private System.Windows.Forms.TextBox txtCantidad;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.RadioButton rbtProductos;
        private System.Windows.Forms.RadioButton rbtServicios;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label16;
        public System.Windows.Forms.TextBox txtCajaClienteRUC;
        public System.Windows.Forms.TextBox txtTCambio;
        public System.Windows.Forms.TextBox txtDescripcion;
        private System.Windows.Forms.RadioButton rbtDolares;
        private System.Windows.Forms.RadioButton rbtSoles;
        private System.Windows.Forms.TextBox txtCajaClienteDireccion;
        private System.Windows.Forms.CheckBox ckbCambiar;
        private System.Windows.Forms.Button btnGrabar;
        public System.Windows.Forms.TextBox txtCitaId;
        public System.Windows.Forms.TextBox txtDNI;
        private System.Windows.Forms.DataGridViewButtonColumn btnDel;
        private System.Windows.Forms.DataGridViewButtonColumn btnUpd;
        private System.Windows.Forms.DataGridViewTextBoxColumn CajaItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn CajaTipoItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn CajaCodigo;
        private System.Windows.Forms.DataGridViewTextBoxColumn CajaTipoTarifa;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descripcion;
        private System.Windows.Forms.DataGridViewTextBoxColumn CajaItemPrecioSol;
        private System.Windows.Forms.DataGridViewTextBoxColumn CajaItemPrecioDol;
        private System.Windows.Forms.DataGridViewTextBoxColumn CajaItemIgv;
        private System.Windows.Forms.DataGridViewTextBoxColumn CajaItemCantidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn CajaItemPrecioFinal;
        private System.Windows.Forms.DataGridViewTextBoxColumn CajaItemTotal;
    }
}