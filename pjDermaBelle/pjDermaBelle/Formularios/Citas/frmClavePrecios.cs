﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pjDermaBelle.Formularios.Citas
{
    public partial class frmClavePrecios : Form
    {
        public frmClavePrecios()
        {
            InitializeComponent();
        }

        ClaseDatos obj = new ClaseDatos();

        frmCajaOperaciones caja = new frmCajaOperaciones();
        public bool validado;
        
        private void btnAceptar_Click(object sender, EventArgs e)
        {
            List<usp_UsuarioDatResult> lista = new List<usp_UsuarioDatResult>();
            lista = obj.BuscaUsuario(txtUsuario.Text, txtPassword.Text);
            if (lista.Count() > 0)
            {
                foreach (usp_UsuarioDatResult reg in lista)
                {
                    validado = Convert.ToBoolean(reg.UsuCambiaPrecio);
                    this.Close();
                }
            }
            else
            { MessageBox.Show("El Nombre de Usuario o la Contraseña ingresados no son válidos. Por favor revise", "Aviso"); }
        }

        private void frmClavePrecios_Load(object sender, EventArgs e)
        {
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            validado = false;
            this.Close();
        }
    }
}
