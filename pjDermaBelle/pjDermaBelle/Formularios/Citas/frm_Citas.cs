﻿using pjDermaBelle.Formularios.Citas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pjDermaBelle
{
    public partial class frm_Citas : Form
    {
        public frm_Citas()
        {
            InitializeComponent();
        }

        ClaseDatos obj = new ClaseDatos();
        DermaBelleDataDataContext linq = new DermaBelleDataDataContext();

        frm_Reserva reserva = new frm_Reserva();

        public SqlConnection cn = new SqlConnection("data source = PC; integrated security = true; initial catalog = DermaBelle; user id = DBUser; password = dbuser");
        public int pSerId;
        public int pConId;
        public DateTime pFecha;
        public int userId;

        public bool agregar = true;

        void CargarDisponibilidad()
        {
            //try
            //{
            //    string campos = "h.HoraIni, h.HoraFin, (Select t.TTr_Nombre From	Citas c, TiposTarifa t Where CONVERT(char(10), c.CitaFecha, 103) = CONVERT(char(10), GETDATE(), 103) And	Ser_Id = 1 And Med_Id = 1 And Left(CAST(c.CitaHora As CHAR(10)),5) Between h.HoraIni And h.HoraFin And t.TTr_Codigo = LEFT(c.Tar_Cod,2)) As Motivo, (Select Case CitaEstado When 'R' Then 'RESERVADA' When 'C' Then 'CONFIRMADA' End From Citas c, TiposTarifa t Where CONVERT(char(10), c.CitaFecha, 103) = CONVERT(char(10), GETDATE(), 103) And Ser_Id = " + pSerId + " And Med_Id = " + int.Parse(cboMedicos.SelectedValue) + " And Left(CAST(c.CitaHora As CHAR(10)),5) Between h.HoraIni And h.HoraFin And t.TTr_Codigo = LEFT(c.Tar_Cod,2)) As Estado";
            //    string tablas = "HorasDia h";

            //    string consulta = "Select " + campos + " From " + tablas;
            //    cn.Open();
            //    SqlDataAdapter daCitas = new SqlDataAdapter(consulta, cn);
            //    DataSet dsCitas = new DataSet();
            //    daCitas.Fill(dsCitas, "HorasDia");

            //    dgvDisponibilidad.DataSource = dsCitas;
            //    dgvDisponibilidad.DataMember = "Citas";

            //    for (int indice = 0; indice < dgvDisponibilidad.Rows.Count; indice++)
            //    {
            //        dgvDisponibilidad.Columns[0].Width = 50;        //HoraIni
            //        dgvDisponibilidad.Columns[1].Width = 50;        //HoraFin
            //        dgvDisponibilidad.Columns[2].Width = 200;       //Motivo
            //        dgvDisponibilidad.Columns[3].Width = 200;       //Estado
            //    }

            //    cn.Close();
            //}
            //catch 
            //{ }
        }

        public void CargarCitas()
        {
            try
            {
                pSerId = int.Parse(cboServicios.SelectedValue.ToString());
                pConId = int.Parse(cboConsultorios.SelectedValue.ToString());
                pFecha = dtpCitaFecha.Value;

                dgvPacCitados.DataSource = obj.GridCitas(pSerId, pConId, pFecha);
                for (int indice = 0; indice < dgvPacCitados.Rows.Count; indice++)
                {
                    //dgvPacCitados.Columns[1].DefaultCellStyle.Format = "d";
                    dgvPacCitados.Columns[0].Width = 50;      //CitasId
                    dgvPacCitados.Columns[1].Width = 50;      //Hora
                    dgvPacCitados.Columns[2].Width = 100;      //HIstoria
                    dgvPacCitados.Columns[3].Width = 200;     //Paciente
                    dgvPacCitados.Columns[4].Width = 200;     //Direccion
                    dgvPacCitados.Columns[5].Width = 100;     //Telefonos
                    dgvPacCitados.Columns[6].Width = 100;      //Estado
                    dgvPacCitados.Columns[7].Width = 300;     //Motivo
                    dgvPacCitados.Columns[8].Width = 300;     //Observaciones

                    dgvPacCitados.Rows[indice].ReadOnly = true;
                    dgvPacCitados.Columns[0].Visible = false;
                    //dgvPacCitados.Rows[indice].Cells[0]. = true;
                    //dgvPacCitados.Rows[indice].Cells[2].ReadOnly = true;

                    DateTime hoy = new DateTime();
                    hoy = DateTime.Today;
                    if (dtpCitaFecha.Value < hoy)
                    {
                        btnAgregar.Enabled = false;
                    }
                    else
                    {
                        btnAgregar.Enabled = true;
                    }
                }
                CargarDisponibilidad();
            }
            catch { }
        }

        public void Servicios()
        {
            cboServicios.DataSource = obj.CboServicios();
            cboServicios.ValueMember = "Ser_Id";
            cboServicios.DisplayMember = "Ser_Nombre";
        }

        public void Consultorios()
        {
            cboConsultorios.DataSource = obj.CboConsultorios();
            cboConsultorios.ValueMember = "Con_Id";
            cboConsultorios.DisplayMember = "Con_Nombre";
        }

        public void Medicos()
        {
            try
            {
                cboMedicos.DataSource = obj.CboMedicos(int.Parse(cboServicios.SelectedValue.ToString()));
                cboMedicos.ValueMember = "Med_Id";
                cboMedicos.DisplayMember = "Med_NomComp";
            }
            catch
            { }
        }


        private void frm_Citas_Load(object sender, EventArgs e)
        {
            Servicios();
            //Consultorios();
            //Medicos();
            pSerId = int.Parse(cboServicios.SelectedValue.ToString());
            pConId = int.Parse(cboConsultorios.SelectedValue.ToString());
            pFecha = dtpCitaFecha.Value;
            CargarCitas();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            //pSerId = int.Parse(cboServicios.SelectedValue.ToString());
            //pConId = int.Parse(cboConsultorios.SelectedValue.ToString());
            //pFecha = dtpCitaFecha.Value;
            CargarCitas();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            agregar = true;
            reserva.FormClosed += new FormClosedEventHandler(reserva_FormClosed);

            reserva.dtpCitaFecha.Value = dtpCitaFecha.Value;

            reserva.txtCitaId.Text = "";
            reserva.txtPac_HC.Text = "";
            reserva.txtPac_NomComp.Text = "";
            reserva.txtPac_Direccion.Text = "";
            reserva.txtPac_Telefonos.Text = "";
            reserva.txtDuracion.Text = "0";
            reserva.dtpInicio.Value = DateTime.Now.AddDays(1);
            reserva.pSerId = short.Parse(cboServicios.SelectedValue.ToString());
            reserva.txtpSerNombre.Text = cboServicios.Text;
            reserva.pConId = short.Parse(cboConsultorios.SelectedValue.ToString());
            reserva.txtpConNombre.Text = cboConsultorios.Text;
            reserva.pMedId = short.Parse(cboMedicos.SelectedValue.ToString());
            reserva.txtpMedNomComp.Text = cboMedicos.Text;
            reserva.pEstado = " ";
            reserva.userId = userId;
            reserva.ShowDialog ();
        }

        private void dgvPacCitados_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            agregar = false;
            frm_Reserva reserva = new frm_Reserva();
            int indice = dgvPacCitados.CurrentCell.RowIndex;
            short CitaId = short.Parse(dgvPacCitados.Rows[indice].Cells[0].Value.ToString());

            List<usp_CitasRecuperaResult> lista = new List<usp_CitasRecuperaResult>();
            lista = obj.CitaDatos(CitaId);
            if (lista.Count() > 0)
            {
                foreach (usp_CitasRecuperaResult reg in lista)
                {

                    reserva.FormClosed += new FormClosedEventHandler(reserva_FormClosed);
                    
                    reserva.txtCitaId.Text = reg.CitaId.ToString();
                    reserva.txtPac_HC.Text = reg.Pac_HC;
                    reserva.txtPac_NomComp.Text = dgvPacCitados.Rows[indice].Cells[3].Value.ToString();
                    reserva.txtPac_Direccion.Text = dgvPacCitados.Rows[indice].Cells[4].Value.ToString();
                    reserva.txtPac_Telefonos.Text = dgvPacCitados.Rows[indice].Cells[5].Value.ToString();
                    reserva.txtTCambio.Text = reg.TipoCambio.ToString();
                    reserva.dtpCitaFecha.Value = reg.CitaFecha;
                    reserva.agregar = false;
                    reserva.pSerId = reg.Ser_Id;
                    reserva.txtpSerNombre.Text = cboServicios.Text;
                    reserva.pConId = short.Parse(reg.Con_Id.ToString());
                    reserva.txtpConNombre.Text = cboConsultorios.Text;
                    reserva.pMedId = reg.Med_Id;
                    reserva.txtpMedNomComp.Text = cboMedicos.Text;
                    reserva.pTarCod = reg.Tar_Cod;
                    reserva.pEstado = reg.CitaEstado;
                    reserva.txtDuracion.Text = reg.CitaDuracion.ToString();
                    reserva.dtpInicio.Value = DateTime.Parse(reg.CitaHora.ToString());
                    reserva.dtpFinal.Value = reserva.dtpInicio.Value.AddMinutes(int.Parse(reg.CitaDuracion.ToString()));
                    reserva.txtObservaciones.Text = reg.Observacion;
                    reserva.pTar_Moneda = reg.Tar_Moneda;
                    reserva.pTar_Valor = decimal.Parse(reg.Tar_Valor.ToString());
                    reserva.pTar_Tiempo = int.Parse(reg.CitaDuracion.ToString());
                    reserva.userId = userId;
                }
            }

            reserva.ShowDialog();
        }

        private void reserva_FormClosed(object sender, FormClosedEventArgs e)
        {
            pSerId = int.Parse(cboServicios.SelectedValue.ToString());
            pConId = int.Parse(cboConsultorios.SelectedValue.ToString());
            pFecha = dtpCitaFecha.Value;
            CargarCitas();
        }

        private void cboServicios_SelectedIndexChanged(object sender, EventArgs e)
        {
            cboMedicos.Text = "";
            Consultorios();
            Medicos();
            CargarCitas();
        }

        private void dtpCitaFecha_ValueChanged(object sender, EventArgs e)
        {
            CargarCitas();
        }

    }
}
