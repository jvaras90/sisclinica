﻿namespace pjDermaBelle
{
    partial class frm_Reserva
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtPac_HC = new System.Windows.Forms.TextBox();
            this.txtPac_NomComp = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPac_Direccion = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPac_Telefonos = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.ckbHistoria = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lstPacientes = new System.Windows.Forms.ListBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cmbTarifario = new System.Windows.Forms.ComboBox();
            this.cmbTipoTarifa = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtpMedNomComp = new System.Windows.Forms.TextBox();
            this.txtpConNombre = new System.Windows.Forms.TextBox();
            this.txtpSerNombre = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.dtpCitaFecha = new System.Windows.Forms.DateTimePicker();
            this.label14 = new System.Windows.Forms.Label();
            this.dtpFinal = new System.Windows.Forms.DateTimePicker();
            this.dtpInicio = new System.Windows.Forms.DateTimePicker();
            this.txtObservaciones = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtDuracion = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtTCambio = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtDolares = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtSoles = new System.Windows.Forms.TextBox();
            this.cboTarifa = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.chkPresencial = new System.Windows.Forms.CheckBox();
            this.btnRegistrar = new System.Windows.Forms.Button();
            this.lblCitaId = new System.Windows.Forms.Label();
            this.txtCitaId = new System.Windows.Forms.TextBox();
            this.lblAsteriscoPac = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.lblEstado = new System.Windows.Forms.Label();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.btnReservar = new System.Windows.Forms.Button();
            this.btnConfirmar = new System.Windows.Forms.Button();
            this.btnModificar = new System.Windows.Forms.Button();
            this.btnAnular = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnSesiones = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(15, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "H.Clínica";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtPac_HC
            // 
            this.txtPac_HC.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPac_HC.Location = new System.Drawing.Point(106, 46);
            this.txtPac_HC.Name = "txtPac_HC";
            this.txtPac_HC.Size = new System.Drawing.Size(106, 23);
            this.txtPac_HC.TabIndex = 1;
            // 
            // txtPac_NomComp
            // 
            this.txtPac_NomComp.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPac_NomComp.Location = new System.Drawing.Point(106, 74);
            this.txtPac_NomComp.Name = "txtPac_NomComp";
            this.txtPac_NomComp.Size = new System.Drawing.Size(356, 23);
            this.txtPac_NomComp.TabIndex = 3;
            this.txtPac_NomComp.TextChanged += new System.EventHandler(this.txtPac_NomComp_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(17, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Paciente";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtPac_Direccion
            // 
            this.txtPac_Direccion.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPac_Direccion.Location = new System.Drawing.Point(106, 103);
            this.txtPac_Direccion.Name = "txtPac_Direccion";
            this.txtPac_Direccion.ReadOnly = true;
            this.txtPac_Direccion.Size = new System.Drawing.Size(457, 23);
            this.txtPac_Direccion.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(14, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Dirección";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtPac_Telefonos
            // 
            this.txtPac_Telefonos.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPac_Telefonos.Location = new System.Drawing.Point(106, 131);
            this.txtPac_Telefonos.Name = "txtPac_Telefonos";
            this.txtPac_Telefonos.Size = new System.Drawing.Size(194, 23);
            this.txtPac_Telefonos.TabIndex = 7;
            this.txtPac_Telefonos.Leave += new System.EventHandler(this.txtPac_Telefonos_Leave);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 134);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "Teléfono(s)";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // ckbHistoria
            // 
            this.ckbHistoria.AutoSize = true;
            this.ckbHistoria.Checked = true;
            this.ckbHistoria.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckbHistoria.Location = new System.Drawing.Point(218, 50);
            this.ckbHistoria.Name = "ckbHistoria";
            this.ckbHistoria.Size = new System.Drawing.Size(127, 17);
            this.ckbHistoria.TabIndex = 8;
            this.ckbHistoria.Text = "Tiene Historia Clínica";
            this.ckbHistoria.UseVisualStyleBackColor = true;
            this.ckbHistoria.CheckedChanged += new System.EventHandler(this.ckbHistoria_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lstPacientes);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.chkPresencial);
            this.groupBox1.Controls.Add(this.btnRegistrar);
            this.groupBox1.Controls.Add(this.lblCitaId);
            this.groupBox1.Controls.Add(this.txtCitaId);
            this.groupBox1.Controls.Add(this.lblAsteriscoPac);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.lblEstado);
            this.groupBox1.Controls.Add(this.btnBuscar);
            this.groupBox1.Controls.Add(this.ckbHistoria);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtPac_HC);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtPac_Telefonos);
            this.groupBox1.Controls.Add(this.txtPac_NomComp);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtPac_Direccion);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(571, 557);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos del Paciente";
            // 
            // lstPacientes
            // 
            this.lstPacientes.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstPacientes.FormattingEnabled = true;
            this.lstPacientes.ItemHeight = 16;
            this.lstPacientes.Location = new System.Drawing.Point(376, 131);
            this.lstPacientes.Name = "lstPacientes";
            this.lstPacientes.Size = new System.Drawing.Size(187, 20);
            this.lstPacientes.TabIndex = 16;
            this.lstPacientes.Visible = false;
            this.lstPacientes.SelectedIndexChanged += new System.EventHandler(this.lstPacientes_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnSesiones);
            this.groupBox2.Controls.Add(this.cmbTarifario);
            this.groupBox2.Controls.Add(this.cmbTipoTarifa);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.txtpMedNomComp);
            this.groupBox2.Controls.Add(this.txtpConNombre);
            this.groupBox2.Controls.Add(this.txtpSerNombre);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.dtpCitaFecha);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.dtpFinal);
            this.groupBox2.Controls.Add(this.dtpInicio);
            this.groupBox2.Controls.Add(this.txtObservaciones);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.txtDuracion);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.txtTCambio);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.txtDolares);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.txtSoles);
            this.groupBox2.Controls.Add(this.cboTarifa);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.shapeContainer1);
            this.groupBox2.Location = new System.Drawing.Point(0, 170);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(571, 383);
            this.groupBox2.TabIndex = 18;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Detalle del Servicio";
            // 
            // cmbTarifario
            // 
            this.cmbTarifario.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbTarifario.FormattingEnabled = true;
            this.cmbTarifario.Location = new System.Drawing.Point(254, 81);
            this.cmbTarifario.Name = "cmbTarifario";
            this.cmbTarifario.Size = new System.Drawing.Size(309, 24);
            this.cmbTarifario.TabIndex = 45;
            this.cmbTarifario.SelectedIndexChanged += new System.EventHandler(this.cmbTarifario_SelectedIndexChanged);
            // 
            // cmbTipoTarifa
            // 
            this.cmbTipoTarifa.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbTipoTarifa.FormattingEnabled = true;
            this.cmbTipoTarifa.Location = new System.Drawing.Point(92, 81);
            this.cmbTipoTarifa.Name = "cmbTipoTarifa";
            this.cmbTipoTarifa.Size = new System.Drawing.Size(156, 24);
            this.cmbTipoTarifa.TabIndex = 43;
            this.cmbTipoTarifa.SelectedIndexChanged += new System.EventHandler(this.cmbTipoTarifa_SelectedIndexChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(23, 85);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(61, 17);
            this.label19.TabIndex = 44;
            this.label19.Text = "Tarifario";
            // 
            // txtpMedNomComp
            // 
            this.txtpMedNomComp.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpMedNomComp.ForeColor = System.Drawing.Color.Blue;
            this.txtpMedNomComp.Location = new System.Drawing.Point(92, 48);
            this.txtpMedNomComp.Name = "txtpMedNomComp";
            this.txtpMedNomComp.ReadOnly = true;
            this.txtpMedNomComp.Size = new System.Drawing.Size(309, 23);
            this.txtpMedNomComp.TabIndex = 42;
            // 
            // txtpConNombre
            // 
            this.txtpConNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpConNombre.ForeColor = System.Drawing.Color.Blue;
            this.txtpConNombre.Location = new System.Drawing.Point(391, 20);
            this.txtpConNombre.Name = "txtpConNombre";
            this.txtpConNombre.ReadOnly = true;
            this.txtpConNombre.Size = new System.Drawing.Size(172, 23);
            this.txtpConNombre.TabIndex = 40;
            // 
            // txtpSerNombre
            // 
            this.txtpSerNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpSerNombre.ForeColor = System.Drawing.Color.Blue;
            this.txtpSerNombre.Location = new System.Drawing.Point(92, 20);
            this.txtpSerNombre.Name = "txtpSerNombre";
            this.txtpSerNombre.ReadOnly = true;
            this.txtpSerNombre.Size = new System.Drawing.Size(208, 23);
            this.txtpSerNombre.TabIndex = 38;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(33, 51);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(53, 17);
            this.label15.TabIndex = 30;
            this.label15.Text = "Médico";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(28, 23);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(58, 17);
            this.label16.TabIndex = 31;
            this.label16.Text = "Servicio";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(306, 23);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(79, 17);
            this.label17.TabIndex = 32;
            this.label17.Text = "Consultorio";
            // 
            // dtpCitaFecha
            // 
            this.dtpCitaFecha.Enabled = false;
            this.dtpCitaFecha.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpCitaFecha.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpCitaFecha.Location = new System.Drawing.Point(463, 48);
            this.dtpCitaFecha.Name = "dtpCitaFecha";
            this.dtpCitaFecha.Size = new System.Drawing.Size(100, 23);
            this.dtpCitaFecha.TabIndex = 29;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(413, 51);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(47, 17);
            this.label14.TabIndex = 28;
            this.label14.Text = "Fecha";
            this.label14.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // dtpFinal
            // 
            this.dtpFinal.Enabled = false;
            this.dtpFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFinal.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpFinal.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.dtpFinal.Location = new System.Drawing.Point(158, 287);
            this.dtpFinal.Name = "dtpFinal";
            this.dtpFinal.ShowUpDown = true;
            this.dtpFinal.Size = new System.Drawing.Size(100, 23);
            this.dtpFinal.TabIndex = 27;
            // 
            // dtpInicio
            // 
            this.dtpInicio.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpInicio.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpInicio.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.dtpInicio.Location = new System.Drawing.Point(158, 256);
            this.dtpInicio.Name = "dtpInicio";
            this.dtpInicio.ShowUpDown = true;
            this.dtpInicio.Size = new System.Drawing.Size(100, 23);
            this.dtpInicio.TabIndex = 26;
            // 
            // txtObservaciones
            // 
            this.txtObservaciones.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtObservaciones.Location = new System.Drawing.Point(108, 322);
            this.txtObservaciones.Multiline = true;
            this.txtObservaciones.Name = "txtObservaciones";
            this.txtObservaciones.Size = new System.Drawing.Size(456, 52);
            this.txtObservaciones.TabIndex = 25;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(0, 322);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(103, 17);
            this.label13.TabIndex = 24;
            this.label13.Text = "Observaciones";
            this.label13.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(530, 261);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(34, 17);
            this.label12.TabIndex = 23;
            this.label12.Text = "min.";
            this.label12.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(97, 290);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(58, 17);
            this.label11.TabIndex = 21;
            this.label11.Text = "Hora fin";
            this.label11.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtDuracion
            // 
            this.txtDuracion.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDuracion.Location = new System.Drawing.Point(454, 258);
            this.txtDuracion.Name = "txtDuracion";
            this.txtDuracion.Size = new System.Drawing.Size(70, 23);
            this.txtDuracion.TabIndex = 20;
            this.txtDuracion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(383, 261);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 17);
            this.label10.TabIndex = 19;
            this.label10.Text = "Duración";
            this.label10.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(80, 259);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(75, 17);
            this.label8.TabIndex = 17;
            this.label8.Text = "Hora inicio";
            this.label8.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtTCambio
            // 
            this.txtTCambio.Enabled = false;
            this.txtTCambio.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTCambio.Location = new System.Drawing.Point(194, 173);
            this.txtTCambio.Name = "txtTCambio";
            this.txtTCambio.Size = new System.Drawing.Size(79, 23);
            this.txtTCambio.TabIndex = 16;
            this.txtTCambio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(97, 176);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(93, 17);
            this.label9.TabIndex = 15;
            this.label9.Text = "T. Cambio S/.";
            this.label9.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtDolares
            // 
            this.txtDolares.Enabled = false;
            this.txtDolares.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDolares.ForeColor = System.Drawing.Color.Green;
            this.txtDolares.Location = new System.Drawing.Point(454, 202);
            this.txtDolares.Name = "txtDolares";
            this.txtDolares.Size = new System.Drawing.Size(111, 23);
            this.txtDolares.TabIndex = 13;
            this.txtDolares.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(379, 205);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 17);
            this.label7.TabIndex = 12;
            this.label7.Text = "Dólares $";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtSoles
            // 
            this.txtSoles.Enabled = false;
            this.txtSoles.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoles.ForeColor = System.Drawing.Color.Blue;
            this.txtSoles.Location = new System.Drawing.Point(454, 173);
            this.txtSoles.Name = "txtSoles";
            this.txtSoles.Size = new System.Drawing.Size(111, 23);
            this.txtSoles.TabIndex = 11;
            this.txtSoles.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // cboTarifa
            // 
            this.cboTarifa.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboTarifa.FormattingEnabled = true;
            this.cboTarifa.Location = new System.Drawing.Point(92, 115);
            this.cboTarifa.Name = "cboTarifa";
            this.cboTarifa.Size = new System.Drawing.Size(368, 24);
            this.cboTarifa.TabIndex = 46;
            this.cboTarifa.SelectedIndexChanged += new System.EventHandler(this.cboTarifa_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(384, 176);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 17);
            this.label6.TabIndex = 10;
            this.label6.Text = "Soles S/.";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(23, 119);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 17);
            this.label5.TabIndex = 10;
            this.label5.Text = "Atención";
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(3, 16);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape2,
            this.lineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(565, 364);
            this.shapeContainer1.TabIndex = 36;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape2
            // 
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.X1 = 11;
            this.lineShape2.X2 = 561;
            this.lineShape2.Y1 = 220;
            this.lineShape2.Y2 = 220;
            // 
            // lineShape1
            // 
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 9;
            this.lineShape1.X2 = 559;
            this.lineShape1.Y1 = 143;
            this.lineShape1.Y2 = 143;
            // 
            // chkPresencial
            // 
            this.chkPresencial.AutoSize = true;
            this.chkPresencial.Checked = true;
            this.chkPresencial.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkPresencial.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPresencial.Location = new System.Drawing.Point(254, 14);
            this.chkPresencial.Name = "chkPresencial";
            this.chkPresencial.Size = new System.Drawing.Size(101, 24);
            this.chkPresencial.TabIndex = 17;
            this.chkPresencial.Text = "Presencial";
            this.chkPresencial.UseVisualStyleBackColor = true;
            this.chkPresencial.Visible = false;
            // 
            // btnRegistrar
            // 
            this.btnRegistrar.Enabled = false;
            this.btnRegistrar.Location = new System.Drawing.Point(387, 46);
            this.btnRegistrar.Name = "btnRegistrar";
            this.btnRegistrar.Size = new System.Drawing.Size(75, 23);
            this.btnRegistrar.TabIndex = 15;
            this.btnRegistrar.Text = "Registrar";
            this.btnRegistrar.UseVisualStyleBackColor = true;
            this.btnRegistrar.Click += new System.EventHandler(this.btnRegistrar_Click);
            // 
            // lblCitaId
            // 
            this.lblCitaId.AutoSize = true;
            this.lblCitaId.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCitaId.Location = new System.Drawing.Point(27, 19);
            this.lblCitaId.Name = "lblCitaId";
            this.lblCitaId.Size = new System.Drawing.Size(51, 17);
            this.lblCitaId.TabIndex = 14;
            this.lblCitaId.Text = "Cita Nº";
            this.lblCitaId.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtCitaId
            // 
            this.txtCitaId.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCitaId.Location = new System.Drawing.Point(106, 16);
            this.txtCitaId.Name = "txtCitaId";
            this.txtCitaId.ReadOnly = true;
            this.txtCitaId.Size = new System.Drawing.Size(100, 23);
            this.txtCitaId.TabIndex = 13;
            this.txtCitaId.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblAsteriscoPac
            // 
            this.lblAsteriscoPac.AutoSize = true;
            this.lblAsteriscoPac.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAsteriscoPac.ForeColor = System.Drawing.Color.Red;
            this.lblAsteriscoPac.Location = new System.Drawing.Point(84, 77);
            this.lblAsteriscoPac.Name = "lblAsteriscoPac";
            this.lblAsteriscoPac.Size = new System.Drawing.Size(13, 17);
            this.lblAsteriscoPac.TabIndex = 12;
            this.lblAsteriscoPac.Text = "*";
            this.lblAsteriscoPac.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Red;
            this.label18.Location = new System.Drawing.Point(84, 134);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(13, 17);
            this.label18.TabIndex = 11;
            this.label18.Text = "*";
            this.label18.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblEstado
            // 
            this.lblEstado.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstado.Location = new System.Drawing.Point(380, 15);
            this.lblEstado.Name = "lblEstado";
            this.lblEstado.Size = new System.Drawing.Size(183, 20);
            this.lblEstado.TabIndex = 10;
            this.lblEstado.Text = "Procesando...";
            this.lblEstado.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // btnBuscar
            // 
            this.btnBuscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscar.Location = new System.Drawing.Point(477, 38);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(87, 59);
            this.btnBuscar.TabIndex = 9;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // btnReservar
            // 
            this.btnReservar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReservar.Location = new System.Drawing.Point(12, 575);
            this.btnReservar.Name = "btnReservar";
            this.btnReservar.Size = new System.Drawing.Size(87, 30);
            this.btnReservar.TabIndex = 13;
            this.btnReservar.Text = "Reservar";
            this.btnReservar.UseVisualStyleBackColor = true;
            this.btnReservar.Click += new System.EventHandler(this.btnReservar_Click);
            // 
            // btnConfirmar
            // 
            this.btnConfirmar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfirmar.Location = new System.Drawing.Point(115, 575);
            this.btnConfirmar.Name = "btnConfirmar";
            this.btnConfirmar.Size = new System.Drawing.Size(87, 30);
            this.btnConfirmar.TabIndex = 14;
            this.btnConfirmar.Text = "Confirmar";
            this.btnConfirmar.UseVisualStyleBackColor = true;
            this.btnConfirmar.Click += new System.EventHandler(this.btnConfirmar_Click);
            // 
            // btnModificar
            // 
            this.btnModificar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModificar.Location = new System.Drawing.Point(218, 575);
            this.btnModificar.Name = "btnModificar";
            this.btnModificar.Size = new System.Drawing.Size(87, 30);
            this.btnModificar.TabIndex = 15;
            this.btnModificar.Text = "Modificar";
            this.btnModificar.UseVisualStyleBackColor = true;
            this.btnModificar.Click += new System.EventHandler(this.btnModificar_Click);
            // 
            // btnAnular
            // 
            this.btnAnular.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAnular.Location = new System.Drawing.Point(321, 575);
            this.btnAnular.Name = "btnAnular";
            this.btnAnular.Size = new System.Drawing.Size(87, 30);
            this.btnAnular.TabIndex = 16;
            this.btnAnular.Text = "Anular";
            this.btnAnular.UseVisualStyleBackColor = true;
            this.btnAnular.Click += new System.EventHandler(this.btnAnular_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.Location = new System.Drawing.Point(496, 575);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(87, 30);
            this.btnCancelar.TabIndex = 17;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnSesiones
            // 
            this.btnSesiones.Location = new System.Drawing.Point(467, 115);
            this.btnSesiones.Name = "btnSesiones";
            this.btnSesiones.Size = new System.Drawing.Size(96, 23);
            this.btnSesiones.TabIndex = 47;
            this.btnSesiones.Text = "Sesiones";
            this.btnSesiones.UseVisualStyleBackColor = true;
            this.btnSesiones.Click += new System.EventHandler(this.btnSesiones_Click);
            // 
            // frm_Reserva
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(595, 617);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnAnular);
            this.Controls.Add(this.btnModificar);
            this.Controls.Add(this.btnConfirmar);
            this.Controls.Add(this.btnReservar);
            this.Controls.Add(this.groupBox1);
            this.Name = "frm_Reserva";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Reserva de Citas";
            this.Load += new System.EventHandler(this.frm_Reserva_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.Button btnCancelar;
        public System.Windows.Forms.TextBox txtPac_HC;
        public System.Windows.Forms.TextBox txtPac_NomComp;
        public System.Windows.Forms.TextBox txtPac_Direccion;
        public System.Windows.Forms.TextBox txtPac_Telefonos;
        public System.Windows.Forms.Button btnReservar;
        public System.Windows.Forms.Button btnConfirmar;
        public System.Windows.Forms.Button btnModificar;
        public System.Windows.Forms.Button btnAnular;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label lblAsteriscoPac;
        public System.Windows.Forms.TextBox txtCitaId;
        private System.Windows.Forms.Label lblCitaId;
        private System.Windows.Forms.Button btnRegistrar;
        public System.Windows.Forms.CheckBox ckbHistoria;
        public System.Windows.Forms.Label lblEstado;
        private System.Windows.Forms.ListBox lstPacientes;
        private System.Windows.Forms.CheckBox chkPresencial;
        private System.Windows.Forms.GroupBox groupBox2;
        public System.Windows.Forms.TextBox txtpMedNomComp;
        public System.Windows.Forms.TextBox txtpConNombre;
        public System.Windows.Forms.TextBox txtpSerNombre;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        public System.Windows.Forms.DateTimePicker dtpCitaFecha;
        private System.Windows.Forms.Label label14;
        public System.Windows.Forms.DateTimePicker dtpFinal;
        public System.Windows.Forms.DateTimePicker dtpInicio;
        public System.Windows.Forms.TextBox txtObservaciones;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        public System.Windows.Forms.TextBox txtDuracion;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox txtTCambio;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.TextBox txtDolares;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.TextBox txtSoles;
        public System.Windows.Forms.ComboBox cboTarifa;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        public System.Windows.Forms.ComboBox cmbTarifario;
        public System.Windows.Forms.ComboBox cmbTipoTarifa;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button btnSesiones;
    }
}