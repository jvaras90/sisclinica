﻿using pjDermaBelle.Formularios.Citas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pjDermaBelle 
{
    public partial class frm_Reserva : Form
    {
        public frm_Reserva()
        {
            InitializeComponent();
        }

        ClaseDatos obj = new ClaseDatos();
        DermaBelleDataDataContext linq = new DermaBelleDataDataContext();
        Citas oCitas = new Citas();

        public bool grabar;
        public bool agregar = true;
        public string pTarCod;
        public short pSerId;
        public short pConId;
        public short pMedId;
        public string pEstado;
        public string pTar_Moneda;
        public decimal pTar_Valor;
        public int pTar_Tiempo;
        public string[,] arrayCajaDetalle =  new string[1,11];
        public string HCPac;
        public bool mostrar;
        public int userId;

        void TipoTarifa()
        {
            cmbTipoTarifa.DataSource = obj.CboTipoTarifa();
            cmbTipoTarifa.ValueMember = "TTr_Codigo";
            cmbTipoTarifa.DisplayMember = "TTr_Nombre";
        }

        void Tarifario()
        {
            try 
            {
                string TtrCod = cmbTipoTarifa.SelectedValue.ToString();
                cmbTarifario.DataSource = obj.CboTarifario(TtrCod);
                cmbTarifario.ValueMember = "Tar_Cod";
                cmbTarifario.DisplayMember = "Tar_Nombre";
            }
            catch { }
        }

        void Tarifas()
        {
          try
            {
                string filtro = cmbTarifario.SelectedValue.ToString().Substring(0, 5);
                cboTarifa.DataSource = obj.CboTarifas(filtro);
                cboTarifa.ValueMember = "Tar_Cod";
                cboTarifa.DisplayMember = "Tar_Nombre";
            }
            catch { }
        }

        public void BuscarHC()
        {
            if (!string.IsNullOrWhiteSpace(txtPac_HC.Text))
            {
                int desde = txtPac_HC.Text.Trim().Length;
                string texto = ("0000000000"+txtPac_HC.Text.Trim()).Substring(desde,10);
                List<usp_BuscarHClinicaResult> lista = new List<usp_BuscarHClinicaResult>();
                lista = obj.BuscarHClinica(texto);
                if (lista.Count() > 0)
                {
                    foreach (usp_BuscarHClinicaResult reg in lista)
                    {
                        txtPac_HC.Text = texto;
                        txtPac_NomComp.Text = reg.Pac_Nomcomp;
                        txtPac_Direccion.Text = reg.Pac_Direccion;
                        txtPac_Telefonos.Text = reg.Pac_Telefonos;
                    }
                }
            }
        }
        
        public void Confirmar()
        {
            oCitas.CitaId = short.Parse(txtCitaId.Text);
            oCitas.CitaEstado = Convert.ToChar("C");
            oCitas.UsuModi = 1; // Reemplazar luego por el usuario logeado
            obj.CitasEstado(oCitas);
            MessageBox.Show("Cita CONFIRMADA satisfactoriamente", "AVISO");
            lblEstado.Text = "CONFIRMADA";
            lblEstado.ForeColor = Color.Blue;
            btnConfirmar.Enabled = false;
        }

        void CargarValores()
        {
            try
            {
                List<usp_BuscarTarifaResult> lista = new List<usp_BuscarTarifaResult>();
                lista = obj.BuscarTarifaResult(cboTarifa.SelectedValue.ToString());
                DateTime hoy = new DateTime();
                hoy = DateTime.Today;

                if (dtpCitaFecha.Value < hoy)
                {
                    if (pTar_Moneda == "SO")
                    {
                        txtSoles.Text = string.Format("{0:N2}", pTar_Valor);
                        //txtDolares.Text = (double.Parse(reg.Tar_Valor.ToString()) / double.Parse(txtTCambio.Text)).ToString();
                        txtDolares.Text = string.Format("{0:N2}", Math.Round((double.Parse(pTar_Valor.ToString()) / 3.50), 2));
                    }
                    else
                    {
                        txtDolares.Text = pTar_Valor.ToString();
                        //txtSoles.Text = (double.Parse(reg.Tar_Valor.ToString()) * double.Parse(txtTCambio.Text)).ToString();
                        txtSoles.Text = Math.Round((double.Parse(pTar_Valor.ToString()) * 3.50), 2).ToString();
                    }

                    txtDuracion.Text = pTar_Tiempo.ToString();
                }
                else
                {
                    if (lista.Count() > 0)
                    {
                        decimal tcambio = decimal.Parse(txtTCambio.Text);
                        foreach (usp_BuscarTarifaResult reg in lista)
                        {
                            if (reg.Tar_Mon == "SO")
                            {
                                txtSoles.Text = string.Format("{0:N2}", reg.Tar_Valor);
                                txtDolares.Text = string.Format("{0:N2}", Math.Round(decimal.Divide(reg.Tar_Valor, tcambio), 2).ToString());
                            }
                            else
                            {
                                txtDolares.Text = string.Format("{0:N2}", reg.Tar_Valor);
                                txtSoles.Text = string.Format("{0:N2}", Math.Round(reg.Tar_Valor * tcambio), 2).ToString();
                            }


                            //MessageBox.Show(pTar_Tiempo.ToString());

                            if (pTar_Tiempo == 0)
                            { txtDuracion.Text = reg.Tar_Tiempo.ToString(); }
                            else
                            {
                                if (pTar_Tiempo.ToString() != reg.Tar_Tiempo.ToString())
                                { txtDuracion.Text = pTar_Tiempo.ToString(); }
                            }
                        }
                    }
                }
            }
            catch { }
        }

        void ValidarCampos()
        {
            grabar = true;
            if (!ckbHistoria.Checked)
            {
                if (string.IsNullOrWhiteSpace(txtPac_NomComp.Text))
                {
                    MessageBox.Show("El campo Paciente no puede quedar vacío");
                    grabar = false;
                    txtPac_NomComp.Focus();
                }
            }
            else
            {
                if (string.IsNullOrWhiteSpace(txtPac_HC.Text))
                {
                    MessageBox.Show("El campo Historia Clínica no puede quedar vacío");
                    grabar = false;
                    txtPac_HC.Focus();
                }
            }

            if (string.IsNullOrWhiteSpace(txtPac_Telefonos.Text))
            {
                MessageBox.Show("El campo Teléfono(s) no puede quedar vacío");
                grabar = false;
                txtPac_Telefonos.Focus();
            }
        }

        private void frm_Reserva_Load(object sender, EventArgs e)
        {
            //txtPac_NomComp.Text = "";
            lstPacientes.Visible = false;
            DateTime hoy = new DateTime();
            hoy = DateTime.Today;
            mostrar = true;
            List<usp_TCambioResult> cambio = new List<usp_TCambioResult>();
            cambio = obj.TipoCambio();
            if (cambio.Count() > 0)
            {
                foreach (usp_TCambioResult reg in cambio)
                {
                    txtTCambio.Text = reg.TCambioCompra.ToString();
                }
            }

            TipoTarifa();
            cmbTipoTarifa.Text = "CONSULTAS";
            Tarifario();
            cmbTarifario.Text = "CONSULTA MEDICA";
            Tarifas();
            cboTarifa.Text = "Consulta por Médico Especialista";
            //txtDuracion.Text = "0";
            //dtpInicio.Value = DateTime.Now.AddDays(1);

            if (dtpCitaFecha.Value < hoy)
            {
                btnReservar.Enabled = false;
                btnConfirmar.Enabled = false;
                btnModificar.Enabled = false;
                btnAnular.Enabled = false;
                ckbHistoria.Enabled = false;
                txtPac_HC.Enabled = false;
                txtPac_NomComp.Enabled = false;
                txtPac_Telefonos.Enabled = false;
                cboTarifa.Enabled = false;
                dtpInicio.Enabled = false;
                txtDuracion.Enabled = false;
                txtObservaciones.Enabled = false;
            }
            else
            {
                btnBuscar.Enabled = agregar;
                btnReservar.Enabled = agregar;
                btnConfirmar.Enabled = !agregar;
                btnModificar.Enabled = !agregar;
                btnAnular.Enabled = !agregar;
            
                txtPac_HC.ReadOnly = false;
                txtPac_NomComp.ReadOnly = false;
                if (!agregar)
                {
                    txtPac_HC.ReadOnly = true;
                    txtPac_NomComp.ReadOnly = true;
                    cmbTipoTarifa.SelectedValue = pTarCod.Substring(0, 2);
                    cmbTarifario.SelectedValue = pTarCod.Substring(0, 6)+"00";
                    cboTarifa.SelectedValue = pTarCod;
                }
            }
            if (pEstado.Trim() == "R")
            {
                lblEstado.Text = "RESERVADA";
                lblEstado.ForeColor = Color.Green;
            }

            if (pEstado.Trim() == "C")
            {
                btnReservar.Enabled = false;
                btnConfirmar.Enabled = false;
                btnAnular.Enabled = false;
                lblEstado.Text = "CONFIRMADA";
                lblEstado.ForeColor = Color.Blue;
                ckbHistoria.Enabled = false;
                txtPac_Telefonos.Enabled = false;
                dtpCitaFecha.Enabled = false;
                cmbTipoTarifa.Enabled = false;
                cmbTarifario.Enabled = false;
                cboTarifa.Enabled = false;
            }

            if (pEstado.Trim() == "A")
            {
                btnConfirmar.Enabled = false;
                btnModificar.Enabled = false;
                btnAnular.Enabled = false;
                ckbHistoria.Enabled = false;
                txtPac_HC.Enabled = false;
                txtPac_NomComp.Enabled = false;
                txtPac_Telefonos.Enabled = false;
                cboTarifa.Enabled = false;
                dtpInicio.Enabled = false;
                txtDuracion.Enabled = false;
                txtObservaciones.Enabled = false;
                lblEstado.Text = "ANULADA";
                lblEstado.ForeColor = Color.Red;
            }
            
            CargarValores();
            if (string.Equals(txtPac_HC.Text, "0000000000"))
            { 
                btnRegistrar.Enabled = true;
                ckbHistoria.Checked = false;
                btnConfirmar.Enabled = false;
            }
            else
            { 
                btnRegistrar.Enabled = false;
                ckbHistoria.Checked = true;
            }
        }

        private void ckbHistoria_CheckedChanged(object sender, EventArgs e)
        {
            txtPac_HC.ReadOnly = !ckbHistoria.Checked;
            btnRegistrar.Enabled = !ckbHistoria.Checked;
            btnBuscar.Enabled = ckbHistoria.Checked;
            if (ckbHistoria.Checked)
            { txtPac_HC.Text = ""; }
            else
            { txtPac_HC.Text = "0000000000"; }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            BuscarHC();
        }

        private void btnReservar_Click(object sender, EventArgs e)
        {
            ValidarCampos();
            if (grabar)
            {
                oCitas.CitaFecha = dtpCitaFecha.Value;
                oCitas.CitaHora = dtpInicio.Value.TimeOfDay;
                oCitas.CitaDuracion = int.Parse(txtDuracion.Text);
                oCitas.Pac_HC = txtPac_HC.Text;
                oCitas.Pac_NomComp = txtPac_NomComp.Text.ToUpper();
                oCitas.Pac_Telefonos = txtPac_Telefonos.Text;
                oCitas.Ser_Id = pSerId;
                oCitas.Con_Id = pConId;
                oCitas.Med_Id = pMedId;
                oCitas.Tar_Cod = cboTarifa.SelectedValue.ToString();
                oCitas.Tar_Moneda = pTar_Moneda;
                oCitas.Tar_Valor = pTar_Valor;
                oCitas.TipoCambio = decimal.Parse(txtTCambio.Text);
                oCitas.CitaEstado = Convert.ToChar("R");
                oCitas.Observacion = txtObservaciones.Text;

                oCitas.UsuCrea = Convert.ToSByte(userId);
                oCitas.UsuModi = Convert.ToSByte(userId);
                obj.CitasReservar(oCitas);

                MessageBox.Show("Cita RESERVADA satisfactoriamente", "AVISO");
                this.Close();
                
                //if (chkPresencial.Checked)
                //    ConfirmarCita();
            }
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            ConfirmarCita();
        }

        void ConfirmarCita()
        {
            if (string.Equals(txtPac_HC.Text, "0000000000"))
            {
                MessageBox.Show("Debe registrar al paciente para poder CONFIRMAR la cita", "AVISO");
                return;
            }

            arrayCajaDetalle[0, 0] = "1"; //CajaItem
            arrayCajaDetalle[0, 1] = "SE"; //CajaTipoItem
            arrayCajaDetalle[0, 2] = cboTarifa.SelectedValue.ToString(); //CajaCodigo
            arrayCajaDetalle[0, 3] = "CO"; //CajaTipoTarifa
            arrayCajaDetalle[0, 4] = cboTarifa.Text.ToString(); //Descripcion
            arrayCajaDetalle[0, 5] = txtSoles.Text; //CajaItemPrecioSol
            arrayCajaDetalle[0, 6] = txtDolares.Text; //CajaItemPrecioDol
            arrayCajaDetalle[0, 7] = "18.00"; //CajaItemIgv
            arrayCajaDetalle[0, 8] = "1"; //CajaCantidad
            arrayCajaDetalle[0, 9] = txtSoles.Text; //CajaItemPrecioFinal
            arrayCajaDetalle[0, 10] = txtSoles.Text; //CajaItemTotal

            frmCajaOperaciones caja = new frmCajaOperaciones();
            caja.esreserva = true;
            caja.txtCitaId.Text = txtCitaId.Text;
            caja.pPac_HC = txtPac_HC.Text;
            caja.txtTCambio.Text = txtTCambio.Text;
            caja.userId = userId;
            caja.dgvDetalleCaja.Rows.Add("", "", arrayCajaDetalle[0, 0],    //CajaItem
                                                 arrayCajaDetalle[0, 1],    //CajaTipoItem
                                                 arrayCajaDetalle[0, 2],    //Codigo
                                                 arrayCajaDetalle[0, 3],    //TipoTarifa
                                                 arrayCajaDetalle[0, 4],    //Descripcion
                                                 arrayCajaDetalle[0, 5],    //PrecioSol
                                                 arrayCajaDetalle[0, 6],    //PrecioDol
                                                 arrayCajaDetalle[0, 7],    //Igv
                                                 arrayCajaDetalle[0, 8],    //Cantidad
                                                 arrayCajaDetalle[0, 9],    //PrecioFinal
                                                 arrayCajaDetalle[0, 10]);  //Total
            this.Close();
            caja.ShowDialog();
        }

        private void btnAnular_Click(object sender, EventArgs e)
        {
            if ((MessageBox.Show("¿Seguro que desea ANULAR ?", "Aviso",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                        MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes))
            {
                oCitas.CitaId = Convert.ToInt16(txtCitaId.Text);
                oCitas.CitaEstado = Convert.ToChar("A");
                oCitas.UsuModi = 1;
                obj.CitasEstado(oCitas);
                { MessageBox.Show("Cita ANULADA satisfactoriamente", "AVISO"); }
                lblEstado.Text = "ANULADA";
                lblEstado.ForeColor = Color.Red;
                btnConfirmar.Enabled = false;
                btnModificar.Enabled = false;
                btnAnular.Enabled = false;
                ckbHistoria.Enabled = false;
                txtPac_HC.Enabled = false;
                txtPac_NomComp.Enabled = false;
                txtPac_Telefonos.Enabled = false;
                cboTarifa.Enabled = false;
                dtpInicio.Enabled = false;
                txtDuracion.Enabled = false;
                txtObservaciones.Enabled = false;
            }

        }

        private void cboTarifas_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargarValores();
        }

        private void dtpInicio_ValueChanged(object sender, EventArgs e)
        {
           dtpFinal.Value = dtpInicio.Value.AddMinutes(int.Parse(txtDuracion.Text));
        }

        private void txtDuracion_TextChanged(object sender, EventArgs e)
        {
            dtpFinal.Value = dtpInicio.Value.AddMinutes(int.Parse(txtDuracion.Text));
        }

        private void txtPac_Telefonos_Leave(object sender, EventArgs e)
        {
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            frmMantPacientes pacientes = new frmMantPacientes();
            pacientes.retornaHC = true;
            pacientes.Show();
            if(!string.IsNullOrEmpty(HCPac))
            { MessageBox.Show("Se generó la Historia Clínica Nº: " + HCPac); }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            ValidarCampos();
            if (grabar)
            {
                oCitas.CitaId = short.Parse(txtCitaId.Text);
                oCitas.CitaFecha = dtpCitaFecha.Value;
                oCitas.CitaHora = dtpInicio.Value.TimeOfDay;
                oCitas.CitaDuracion = int.Parse(txtDuracion.Text);
                oCitas.Pac_HC = txtPac_HC.Text;
                oCitas.Pac_NomComp = txtPac_NomComp.Text.ToUpper();
                oCitas.Pac_Telefonos = txtPac_Telefonos.Text;
                oCitas.Ser_Id = pSerId;
                oCitas.Con_Id = pConId;
                oCitas.Med_Id = pMedId;
                oCitas.Tar_Cod = cboTarifa.SelectedValue.ToString();
                oCitas.Tar_Moneda = pTar_Moneda;
                oCitas.Tar_Valor = pTar_Valor;
                oCitas.TipoCambio = decimal.Parse(txtTCambio.Text);
                oCitas.CitaEstado = Convert.ToChar("R");
                oCitas.Observacion = txtObservaciones.Text;

                oCitas.UsuCrea = Convert.ToSByte(userId);
                oCitas.UsuModi = Convert.ToSByte(userId);
                obj.CitasModificar(oCitas);

                MessageBox.Show("Cita MODIFICADA satisfactoriamente", "AVISO");
                if (!string.Equals(txtPac_HC.Text, "0000000000"))
                { btnConfirmar.Enabled = true; }
            }
        }

        private void txtPac_NomComp_TextChanged(object sender, EventArgs e)
        {
            if (!ckbHistoria.Checked)
                return;
            if (string.IsNullOrEmpty(txtPac_NomComp.Text))
            {mostrar = true;}
            lstPacientes.Items.Clear();
            lstPacientes.Visible = mostrar;
            lstPacientes.Location = new Point(106, 97);
            lstPacientes.Size = new Size(457, 324);

            List<usp_BuscarPacienteResult> listaPac = new List<usp_BuscarPacienteResult>();
            listaPac = obj.BuscarPacienteResult(txtPac_NomComp.Text);
            if (listaPac.Count() > 0)
            {
                foreach (usp_BuscarPacienteResult reg in listaPac)
                {
                    lstPacientes.Items.Add(reg.Paciente);
                    //txtPac_HC.Text = reg.Historia;
                }
            }
        }

        private void lstPacientes_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtPac_HC.Text = lstPacientes.SelectedItem.ToString().Substring(1, 10);
            txtPac_NomComp.Text = lstPacientes.SelectedItem.ToString();
            lstPacientes.Visible = false;
            mostrar = false;
            BuscarHC();
        }

        private void cmbTipoTarifa_SelectedIndexChanged(object sender, EventArgs e)
        {
            Tarifario();
            Tarifas();
        }

        private void cmbTarifario_SelectedIndexChanged(object sender, EventArgs e)
        {
            Tarifas();
        }

        private void cboTarifa_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargarValores();
            if (!string.Equals(cmbTipoTarifa.Text, "CONSULTAS"))
            {
                btnSesiones.Enabled = true;
                btnReservar.Enabled = false;
            }
            else
            {
                btnSesiones.Enabled = false;
                btnReservar.Enabled = true;
            }
        }

        private void btnSesiones_Click(object sender, EventArgs e)
        {
            frmSesiones sesiones = new frmSesiones();
            sesiones.lblTitulo.Text = cmbTarifario.Text + ": " + cboTarifa.Text;
            sesiones.PacHc = txtPac_HC.Text;
            sesiones.TarCod = cboTarifa.SelectedValue.ToString();
            sesiones.txtDuracion.Text = txtDuracion.Text;
            sesiones.SerId = pSerId;
            sesiones.ConId = pConId;
            sesiones.MedId = pMedId;
            sesiones.PacNombreComp = txtPac_NomComp.Text;
            sesiones.PacTelefonos = txtPac_Telefonos.Text;
            sesiones.ShowDialog();
        }
    }
}
