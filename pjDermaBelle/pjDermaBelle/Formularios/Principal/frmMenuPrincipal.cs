﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pjDermaBelle
{
    public partial class frmMenuPrincipal : Form
    {
        public frmMenuPrincipal()
        {
            InitializeComponent();
        }

        ClaseDatos obj = new ClaseDatos();
        public int medicoId;
        public int userId;

        private void Cerrar()
        {
            if ((MessageBox.Show("¿Seguro que desea salir ?", "Aviso",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                    MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes))
            {
                this.Close();
            }
        }

        public bool VerModulo(int PerfilId, string ModuloIdent)
        {
            bool valor = false;
            List<usp_HabilitarModuloResult> lista = new List<usp_HabilitarModuloResult>();
            lista = obj.Habilitar(PerfilId, ModuloIdent);
            if (lista.Count() > 0)
            {
                foreach (usp_HabilitarModuloResult reg in lista)
                {
                    valor =  reg.Habilitar;
                }
            }
            return valor;
        }

        private void HabilitarMenu(sbyte perfil)
        {
            menuPrincipal.Enabled = true;
            int Perfil = int.Parse(toolStripStatuslblPerfilId.Text);
            //Opciones del MENU ADMINISTRACION
            administrativo.Visible = VerModulo(Perfil, "administrativo");
            maestros.Visible = VerModulo(Perfil,  "maestros");
            profesionesToolStripMenuItem.Visible = VerModulo(Perfil,  "profesionesToolStripMenuItem");
            gradosDeInstrucciónToolStripMenuItem.Visible = VerModulo(Perfil,  "gradosDeInstrucciónToolStripMenuItem");
            ubigeoToolStripMenuItem.Visible = VerModulo(Perfil,  "ubigeoToolStripMenuItem");
            documentosDeIdentidadToolStripMenuItem.Visible = VerModulo(Perfil,  "documentosDeIdentidadToolStripMenuItem");
            pacientes.Visible = VerModulo(Perfil, "pacientes");
            filiaciónToolStripMenuItem.Visible = VerModulo(Perfil,  "filiaciónToolStripMenuItem");
            controlToolStripMenuItem.Visible = VerModulo(Perfil,  "controlToolStripMenuItem");
            tarifario.Visible= VerModulo(Perfil,"tarifario");
            serviciosToolStripMenuItem.Visible = VerModulo(Perfil,"serviciosToolStripMenuItem");
            tipoDeTarifaToolStripMenuItem.Visible = VerModulo(Perfil,"tipoDeTarifaToolStripMenuItem");
            productosToolStripMenuItem.Visible = VerModulo(Perfil, "productosToolStripMenuItem");
            definiciones.Visible = VerModulo(Perfil, "definiciones");
            principiosActivosToolStripMenuItem.Visible = VerModulo(Perfil, "principiosActivosToolStripMenuItem");
            laboratoriosToolStripMenuItem.Visible = VerModulo(Perfil, "laboratoriosToolStripMenuItem");

            citas.Visible = VerModulo(Perfil, "citas");
            definicionesToolStripMenuItem.Visible = VerModulo(Perfil, "definicionesToolStripMenuItem");
            consultoriosToolStripMenuItem.Visible = VerModulo(Perfil, "consultoriosToolStripMenuItem");
            serviciosToolStripMenuItem1.Visible = VerModulo(Perfil, "serviciosToolStripMenuItem1");
            horariiosToolStripMenuItem.Visible = VerModulo(Perfil, "horariiosToolStripMenuItem");
            citasToolStripMenuItem1.Visible = VerModulo(Perfil, "citasToolStripMenuItem1");
            reservaDeCitasToolStripMenuItem.Visible = VerModulo(Perfil, "reservaDeCitasToolStripMenuItem");
            cajaToolStripMenuItem.Visible = VerModulo(Perfil, "cajaToolStripMenuItem");
            operacionesToolStripMenuItem.Visible = VerModulo(Perfil, "operacionesToolStripMenuItem");
            arqueoToolStripMenuItem.Visible = VerModulo(Perfil, "arqueoToolStripMenuItem");
            definicionesToolStripMenuItem1.Visible = VerModulo(Perfil, "definicionesToolStripMenuItem1");
            documentosDePagoToolStripMenuItem.Visible = VerModulo(Perfil, "documentosDePagoToolStripMenuItem");
            tarjetasCréditoDébitoToolStripMenuItem.Visible = VerModulo(Perfil, "tarjetasCréditoDébitoToolStripMenuItem");
            reportesToolStripMenuItem.Visible = VerModulo(Perfil, "reportesToolStripMenuItem");
            pacientesToolStripMenuItem1.Visible = VerModulo(Perfil, "pacientesToolStripMenuItem1");
            citadosVsAtendidosToolStripMenuItem.Visible = VerModulo(Perfil, "citadosVsAtendidosToolStripMenuItem");
            pacientesAtendidosToolStripMenuItem.Visible = VerModulo(Perfil, "pacientesAtendidosToolStripMenuItem");
            citadosVsAtendidosToolStripMenuItem1.Visible = VerModulo(Perfil, "citadosVsAtendidosToolStripMenuItem1");
            citasReservadasVsConfirmadasToolStripMenuItem.Visible = VerModulo(Perfil, "citasReservadasVsConfirmadasToolStripMenuItem");
            cajaToolStripMenuItem1.Visible = VerModulo(Perfil, "cajaToolStripMenuItem1");
            ingresosPorDíaToolStripMenuItem.Visible = VerModulo(Perfil, "ingresosPorDíaToolStripMenuItem");
            ingresosPorServicioToolStripMenuItem.Visible = VerModulo(Perfil, "ingresosPorServicioToolStripMenuItem");
            ingresosPorTipoDePagoToolStripMenuItem.Visible = VerModulo(Perfil, "ingresosPorTipoDePagoToolStripMenuItem");
            arqueosToolStripMenuItem.Visible = VerModulo(Perfil, "arqueosToolStripMenuItem");

            asistencial.Visible = VerModulo(Perfil, "asistencial");
            asistencialToolStripMenuItem.Visible = VerModulo(Perfil, "asistencialToolStripMenuItem");
            tomaDeSignosVitalesToolStripMenuItem1.Visible = VerModulo(Perfil, "tomaDeSignosVitalesToolStripMenuItem1");
            actoMédicoToolStripMenuItem1.Visible = VerModulo(Perfil, "actoMédicoToolStripMenuItem1");
            consultaToolStripMenuItem.Visible = VerModulo(Perfil, "consultaToolStripMenuItem");
            historialMédicoToolStripMenuItem.Visible = VerModulo(Perfil, "historialMédicoToolStripMenuItem");
            reportesToolStripMenuItem1.Visible = VerModulo(Perfil, "reportesToolStripMenuItem1");
            síntomasMásComunesToolStripMenuItem.Visible = VerModulo(Perfil, "síntomasMásComunesToolStripMenuItem");
            diagnósticosMásComunesToolStripMenuItem.Visible = VerModulo(Perfil, "diagnósticosMásComunesToolStripMenuItem");
            medicamentosMásPrescritosToolStripMenuItem.Visible = VerModulo(Perfil, "medicamentosMásPrescritosToolStripMenuItem");
            tratamientosMásEmpleadosToolStripMenuItem.Visible = VerModulo(Perfil, "tratamientosMásEmpleadosToolStripMenuItem");

            farmacia.Visible = VerModulo(Perfil, "farmacia");
            ventasToolStripMenuItem.Visible = VerModulo(Perfil, "ventasToolStripMenuItem");
            almacenToolStripMenuItem.Visible = VerModulo(Perfil, "almacenToolStripMenuItem");
            comprasToolStripMenuItem.Visible = VerModulo(Perfil, "comprasToolStripMenuItem");
            movimientosDeAlmacénToolStripMenuItem.Visible = VerModulo(Perfil, "movimientosDeAlmacénToolStripMenuItem");
            kardexToolStripMenuItem.Visible = VerModulo(Perfil, "kardexToolStripMenuItem");
            inventarioToolStripMenuItem1.Visible = VerModulo(Perfil, "inventarioToolStripMenuItem1");
            formulariosToolStripMenuItem.Visible = VerModulo(Perfil, "formulariosToolStripMenuItem");
            actualizaciónToolStripMenuItem.Visible = VerModulo(Perfil, "actualizaciónToolStripMenuItem");
            inventarioValorizadoToolStripMenuItem.Visible = VerModulo(Perfil, "inventarioValorizadoToolStripMenuItem");

            utilitarios.Visible = VerModulo(Perfil, "utilitarios");
            usuariosToolStripMenuItem.Visible = VerModulo(Perfil, "usuariosToolStripMenuItem");
            perfilesDeUsuarioToolStripMenuItem.Visible = VerModulo(Perfil, "perfilesDeUsuarioToolStripMenuItem");
            registroUsuariosToolStripMenuItem1.Visible = VerModulo(Perfil, "registroUsuariosToolStripMenuItem1");
            definicionesToolStripMenuItem2.Visible = VerModulo(Perfil, "definicionesToolStripMenuItem2");
            partesDelCuerpoToolStripMenuItem.Visible = VerModulo(Perfil, "partesDelCuerpoToolStripMenuItem");
            ayudaToolStripMenuItem.Visible = VerModulo(Perfil, "ayudaToolStripMenuItem");
            ayudaEnLíneaToolStripMenuItem.Visible = VerModulo(Perfil, "ayudaEnLíneaToolStripMenuItem");
            acercaDeToolStripMenuItem.Visible = VerModulo(Perfil, "acercaDeToolStripMenuItem");
        }

        private void profesionesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMantProfesiones frm = new frmMantProfesiones();
            frm.ShowDialog();
        }

        private void gradosDeInstrucciónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMantGradosInstruccion frm = new frmMantGradosInstruccion();
            frm.ShowDialog();
        }

        private void ubigeoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMantUbigeo frm = new frmMantUbigeo();
            frm.ShowDialog();
        }

        private void documentosDeIdentidadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMantDocIdentidad frm = new frmMantDocIdentidad();
            frm.ShowDialog();
        }

        private void filiaciónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMantPacientes frm = new frmMantPacientes();
            frm.ShowDialog();
        }

        private void controlToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAdmCtrlAsistPacientes frm = new frmAdmCtrlAsistPacientes();
            frm.ShowDialog();
        }

        private void especialidadesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMantEspecialidades frm = new frmMantEspecialidades();
            frm.ShowDialog();
        }

        private void registroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMantProfSalud frm = new frmMantProfSalud();
            frm.ShowDialog();
        }

        private void serviciosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMantTarifario frm = new frmMantTarifario();
            frm.ShowDialog();
        }

        private void tipoDeTarifaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMantTiposTarifa frm = new frmMantTiposTarifa();
            frm.ShowDialog();
        }

        private void principiosActivosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMantPrincipiosActivos frm = new frmMantPrincipiosActivos();
            frm.ShowDialog();
        }

        private void laboratoriosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMantLaboratorios frm = new frmMantLaboratorios();
            frm.ShowDialog();
        }

        private void actoMédicoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmPacCitados frm = new frmPacCitados();            
            List<usp_IdentificaMedicoResult> lista = new List<usp_IdentificaMedicoResult>();
            lista = obj.IdentificaMedico(medicoId);
            if (lista.Count() > 0)
            {
                foreach (usp_IdentificaMedicoResult reg in lista)
                {
                    frm._SerId = reg.Ser_Id;
                    frm._MedCod = medicoId;
                    frm.lblServicio.Text = reg.Ser_Nombre;
                    frm.lblMedico.Text = reg.Med_NomComp;
                    frm.lblConsultorio.Text = reg.Con_Nombre;
                }
            }
                        
            frm.ShowDialog();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cerrar();
        }

        private void reservaDeCitasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frm_Citas frm = new frm_Citas();
            frm.userId = userId;
            frm.ShowDialog();
        }

        private void frmMenuPrincipal_Load(object sender, EventArgs e)
        {
            menuPrincipal.Enabled = false;

        }

        private void btnIngresar_Click(object sender, EventArgs e)
        {
            List<usp_UsuarioDatResult> lista = new List<usp_UsuarioDatResult>();
            lista = obj.BuscaUsuario(txtUsuario.Text,txtPassword.Text);
            if (lista.Count() > 0)
            {
                foreach (usp_UsuarioDatResult reg in lista)
                {
                    frmPacCitados paccitados = new frmPacCitados();
                    userId = reg.UsuId;
                    toolStripStatuslblUsuario.Text = reg.UsuNComp;
                    toolStripStatuslblPerfilNombre.Text = reg.PerfilNombre;
                    toolStripStatuslblPerfilId.Text = reg.UsuPerfil.ToString();
                    medicoId = int.Parse(reg.Med_Id.ToString());
                    gpbLogin.Visible = false;
                    HabilitarMenu(1);
                }
            }
            else
            {MessageBox.Show("El Nombre de Usuario o la Contraseña ingresados no son válidos. Por favor revise","Aviso");}
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Cerrar();
        }
       
        private void txtPassword_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            { btnIngresar_Click(null, null); }
        }

        private void cambiarDeUsuarioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            txtUsuario.Text = "";
            txtPassword.Text = "";
            gpbLogin.Visible = true;
        }

        private void partesDelCuerpoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pjDermaBelle.Formularios.Utilitarios.frmPartesCuerpo frm = new Formularios.Utilitarios.frmPartesCuerpo();
            frm.ShowDialog();
        }

        private void productosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pjDermaBelle.Formularios.Administrativo.frmProductos frm = new Formularios.Administrativo.frmProductos();
            frm.ShowDialog();
        }

        private void ventasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pjDermaBelle.Formularios.Farmacia.frmVentas frm = new Formularios.Farmacia.frmVentas();
            frm.ShowDialog();
        }

        private void perfilesDeUsuarioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pjDermaBelle.Formularios.Utilitarios.frmUsuarios frm = new Formularios.Utilitarios.frmUsuarios();
            frm.ShowDialog();
        }
    }
}
