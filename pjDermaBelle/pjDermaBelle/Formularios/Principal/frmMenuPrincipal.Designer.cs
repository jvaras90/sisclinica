﻿namespace pjDermaBelle
{
    partial class frmMenuPrincipal
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuPrincipal = new System.Windows.Forms.MenuStrip();
            this.administrativo = new System.Windows.Forms.ToolStripMenuItem();
            this.maestros = new System.Windows.Forms.ToolStripMenuItem();
            this.profesionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gradosDeInstrucciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ubigeoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.documentosDeIdentidadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pacientes = new System.Windows.Forms.ToolStripMenuItem();
            this.filiaciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.controlToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.profesionalesDeLaSaludToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.especialidadesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tarifario = new System.Windows.Forms.ToolStripMenuItem();
            this.serviciosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tipoDeTarifaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.productosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.definiciones = new System.Windows.Forms.ToolStripMenuItem();
            this.principiosActivosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.laboratoriosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.citas = new System.Windows.Forms.ToolStripMenuItem();
            this.definicionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultoriosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.serviciosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.horariiosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.citasToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.reservaDeCitasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cajaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.operacionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.arqueoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.definicionesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.documentosDePagoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tarjetasCréditoDébitoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.reportesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pacientesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.citadosVsAtendidosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pacientesAtendidosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.citadosVsAtendidosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.citasReservadasVsConfirmadasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cajaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ingresosPorDíaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ingresosPorServicioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ingresosPorTipoDePagoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.arqueosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asistencial = new System.Windows.Forms.ToolStripMenuItem();
            this.asistencialToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tomaDeSignosVitalesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.actoMédicoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.historialMédicoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.reportesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.síntomasMásComunesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.diagnósticosMásComunesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.medicamentosMásPrescritosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tratamientosMásEmpleadosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.farmacia = new System.Windows.Forms.ToolStripMenuItem();
            this.ventasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.almacenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.comprasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.movimientosDeAlmacénToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.kardexToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inventarioToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.formulariosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.actualizaciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.inventarioValorizadoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.utilitarios = new System.Windows.Forms.ToolStripMenuItem();
            this.usuariosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.perfilesDeUsuarioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registroUsuariosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.cambiarClaveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ayudaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ayudaEnLíneaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.acercaDeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.cambiarDeUsuarioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.definicionesToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.partesDelCuerpoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salir = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatuslblUsuario = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatuslblPerfilNombre = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatuslblPerfilId = new System.Windows.Forms.ToolStripStatusLabel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.gpbLogin = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.btnSalir = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnIngresar = new System.Windows.Forms.Button();
            this.txtUsuario = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.shapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.menuPrincipal.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.gpbLogin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // menuPrincipal
            // 
            this.menuPrincipal.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.administrativo,
            this.citas,
            this.asistencial,
            this.farmacia,
            this.utilitarios,
            this.salir});
            this.menuPrincipal.Location = new System.Drawing.Point(0, 0);
            this.menuPrincipal.Name = "menuPrincipal";
            this.menuPrincipal.Size = new System.Drawing.Size(913, 24);
            this.menuPrincipal.TabIndex = 1;
            this.menuPrincipal.Text = "menuStrip1";
            // 
            // administrativo
            // 
            this.administrativo.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.maestros,
            this.pacientes,
            this.profesionalesDeLaSaludToolStripMenuItem,
            this.toolStripSeparator1,
            this.tarifario});
            this.administrativo.Name = "administrativo";
            this.administrativo.Size = new System.Drawing.Size(97, 20);
            this.administrativo.Text = "Administrativo";
            // 
            // maestros
            // 
            this.maestros.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.profesionesToolStripMenuItem,
            this.gradosDeInstrucciónToolStripMenuItem,
            this.ubigeoToolStripMenuItem,
            this.documentosDeIdentidadToolStripMenuItem});
            this.maestros.Name = "maestros";
            this.maestros.Size = new System.Drawing.Size(204, 22);
            this.maestros.Text = "Maestros";
            // 
            // profesionesToolStripMenuItem
            // 
            this.profesionesToolStripMenuItem.Name = "profesionesToolStripMenuItem";
            this.profesionesToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.profesionesToolStripMenuItem.Text = "Profesiones";
            this.profesionesToolStripMenuItem.Click += new System.EventHandler(this.profesionesToolStripMenuItem_Click);
            // 
            // gradosDeInstrucciónToolStripMenuItem
            // 
            this.gradosDeInstrucciónToolStripMenuItem.Name = "gradosDeInstrucciónToolStripMenuItem";
            this.gradosDeInstrucciónToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.gradosDeInstrucciónToolStripMenuItem.Text = "Grados de Instrucción";
            this.gradosDeInstrucciónToolStripMenuItem.Click += new System.EventHandler(this.gradosDeInstrucciónToolStripMenuItem_Click);
            // 
            // ubigeoToolStripMenuItem
            // 
            this.ubigeoToolStripMenuItem.Name = "ubigeoToolStripMenuItem";
            this.ubigeoToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.ubigeoToolStripMenuItem.Text = "Ubigeo";
            this.ubigeoToolStripMenuItem.Click += new System.EventHandler(this.ubigeoToolStripMenuItem_Click);
            // 
            // documentosDeIdentidadToolStripMenuItem
            // 
            this.documentosDeIdentidadToolStripMenuItem.Name = "documentosDeIdentidadToolStripMenuItem";
            this.documentosDeIdentidadToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.documentosDeIdentidadToolStripMenuItem.Text = "Documentos de Identidad";
            this.documentosDeIdentidadToolStripMenuItem.Click += new System.EventHandler(this.documentosDeIdentidadToolStripMenuItem_Click);
            // 
            // pacientes
            // 
            this.pacientes.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.filiaciónToolStripMenuItem,
            this.controlToolStripMenuItem});
            this.pacientes.Name = "pacientes";
            this.pacientes.Size = new System.Drawing.Size(204, 22);
            this.pacientes.Text = "Pacientes";
            // 
            // filiaciónToolStripMenuItem
            // 
            this.filiaciónToolStripMenuItem.Name = "filiaciónToolStripMenuItem";
            this.filiaciónToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.filiaciónToolStripMenuItem.Text = "Filiación";
            this.filiaciónToolStripMenuItem.Click += new System.EventHandler(this.filiaciónToolStripMenuItem_Click);
            // 
            // controlToolStripMenuItem
            // 
            this.controlToolStripMenuItem.Name = "controlToolStripMenuItem";
            this.controlToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.controlToolStripMenuItem.Text = "Control de Asistencia";
            this.controlToolStripMenuItem.Click += new System.EventHandler(this.controlToolStripMenuItem_Click);
            // 
            // profesionalesDeLaSaludToolStripMenuItem
            // 
            this.profesionalesDeLaSaludToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registroToolStripMenuItem,
            this.especialidadesToolStripMenuItem});
            this.profesionalesDeLaSaludToolStripMenuItem.Name = "profesionalesDeLaSaludToolStripMenuItem";
            this.profesionalesDeLaSaludToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.profesionalesDeLaSaludToolStripMenuItem.Text = "Profesionales de la Salud";
            // 
            // registroToolStripMenuItem
            // 
            this.registroToolStripMenuItem.Name = "registroToolStripMenuItem";
            this.registroToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.registroToolStripMenuItem.Text = "Registro";
            this.registroToolStripMenuItem.Click += new System.EventHandler(this.registroToolStripMenuItem_Click);
            // 
            // especialidadesToolStripMenuItem
            // 
            this.especialidadesToolStripMenuItem.Name = "especialidadesToolStripMenuItem";
            this.especialidadesToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.especialidadesToolStripMenuItem.Text = "Especialidades";
            this.especialidadesToolStripMenuItem.Click += new System.EventHandler(this.especialidadesToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(201, 6);
            // 
            // tarifario
            // 
            this.tarifario.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.serviciosToolStripMenuItem,
            this.tipoDeTarifaToolStripMenuItem,
            this.toolStripSeparator8,
            this.productosToolStripMenuItem,
            this.definiciones});
            this.tarifario.Name = "tarifario";
            this.tarifario.Size = new System.Drawing.Size(204, 22);
            this.tarifario.Text = "Tarifario";
            // 
            // serviciosToolStripMenuItem
            // 
            this.serviciosToolStripMenuItem.Name = "serviciosToolStripMenuItem";
            this.serviciosToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.serviciosToolStripMenuItem.Text = "Tarifas";
            this.serviciosToolStripMenuItem.Click += new System.EventHandler(this.serviciosToolStripMenuItem_Click);
            // 
            // tipoDeTarifaToolStripMenuItem
            // 
            this.tipoDeTarifaToolStripMenuItem.Name = "tipoDeTarifaToolStripMenuItem";
            this.tipoDeTarifaToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.tipoDeTarifaToolStripMenuItem.Text = "Tipos de Tarifa";
            this.tipoDeTarifaToolStripMenuItem.Click += new System.EventHandler(this.tipoDeTarifaToolStripMenuItem_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(149, 6);
            // 
            // productosToolStripMenuItem
            // 
            this.productosToolStripMenuItem.Name = "productosToolStripMenuItem";
            this.productosToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.productosToolStripMenuItem.Text = "Productos";
            this.productosToolStripMenuItem.Click += new System.EventHandler(this.productosToolStripMenuItem_Click);
            // 
            // definiciones
            // 
            this.definiciones.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.principiosActivosToolStripMenuItem,
            this.laboratoriosToolStripMenuItem});
            this.definiciones.Name = "definiciones";
            this.definiciones.Size = new System.Drawing.Size(152, 22);
            this.definiciones.Text = "Definiciones";
            // 
            // principiosActivosToolStripMenuItem
            // 
            this.principiosActivosToolStripMenuItem.Name = "principiosActivosToolStripMenuItem";
            this.principiosActivosToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.principiosActivosToolStripMenuItem.Text = "Principios Activos";
            this.principiosActivosToolStripMenuItem.Click += new System.EventHandler(this.principiosActivosToolStripMenuItem_Click);
            // 
            // laboratoriosToolStripMenuItem
            // 
            this.laboratoriosToolStripMenuItem.Name = "laboratoriosToolStripMenuItem";
            this.laboratoriosToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.laboratoriosToolStripMenuItem.Text = "Laboratorios";
            this.laboratoriosToolStripMenuItem.Click += new System.EventHandler(this.laboratoriosToolStripMenuItem_Click);
            // 
            // citas
            // 
            this.citas.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.definicionesToolStripMenuItem,
            this.citasToolStripMenuItem1,
            this.cajaToolStripMenuItem,
            this.toolStripSeparator2,
            this.reportesToolStripMenuItem});
            this.citas.Name = "citas";
            this.citas.Size = new System.Drawing.Size(45, 20);
            this.citas.Text = "Citas";
            // 
            // definicionesToolStripMenuItem
            // 
            this.definicionesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.consultoriosToolStripMenuItem,
            this.serviciosToolStripMenuItem1,
            this.horariiosToolStripMenuItem});
            this.definicionesToolStripMenuItem.Name = "definicionesToolStripMenuItem";
            this.definicionesToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.definicionesToolStripMenuItem.Text = "Definiciones";
            // 
            // consultoriosToolStripMenuItem
            // 
            this.consultoriosToolStripMenuItem.Name = "consultoriosToolStripMenuItem";
            this.consultoriosToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.consultoriosToolStripMenuItem.Text = "Consultorios";
            // 
            // serviciosToolStripMenuItem1
            // 
            this.serviciosToolStripMenuItem1.Name = "serviciosToolStripMenuItem1";
            this.serviciosToolStripMenuItem1.Size = new System.Drawing.Size(141, 22);
            this.serviciosToolStripMenuItem1.Text = "Servicios";
            // 
            // horariiosToolStripMenuItem
            // 
            this.horariiosToolStripMenuItem.Name = "horariiosToolStripMenuItem";
            this.horariiosToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.horariiosToolStripMenuItem.Text = "Horarios";
            // 
            // citasToolStripMenuItem1
            // 
            this.citasToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.reservaDeCitasToolStripMenuItem});
            this.citasToolStripMenuItem1.Name = "citasToolStripMenuItem1";
            this.citasToolStripMenuItem1.Size = new System.Drawing.Size(139, 22);
            this.citasToolStripMenuItem1.Text = "Citas";
            // 
            // reservaDeCitasToolStripMenuItem
            // 
            this.reservaDeCitasToolStripMenuItem.Name = "reservaDeCitasToolStripMenuItem";
            this.reservaDeCitasToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.reservaDeCitasToolStripMenuItem.Text = "Reserva de Citas";
            this.reservaDeCitasToolStripMenuItem.Click += new System.EventHandler(this.reservaDeCitasToolStripMenuItem_Click);
            // 
            // cajaToolStripMenuItem
            // 
            this.cajaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.operacionesToolStripMenuItem,
            this.arqueoToolStripMenuItem,
            this.toolStripSeparator3,
            this.definicionesToolStripMenuItem1});
            this.cajaToolStripMenuItem.Name = "cajaToolStripMenuItem";
            this.cajaToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.cajaToolStripMenuItem.Text = "Caja";
            // 
            // operacionesToolStripMenuItem
            // 
            this.operacionesToolStripMenuItem.Name = "operacionesToolStripMenuItem";
            this.operacionesToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.operacionesToolStripMenuItem.Text = "Operaciones";
            // 
            // arqueoToolStripMenuItem
            // 
            this.arqueoToolStripMenuItem.Name = "arqueoToolStripMenuItem";
            this.arqueoToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.arqueoToolStripMenuItem.Text = "Arqueo";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(137, 6);
            // 
            // definicionesToolStripMenuItem1
            // 
            this.definicionesToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.documentosDePagoToolStripMenuItem,
            this.tarjetasCréditoDébitoToolStripMenuItem});
            this.definicionesToolStripMenuItem1.Name = "definicionesToolStripMenuItem1";
            this.definicionesToolStripMenuItem1.Size = new System.Drawing.Size(140, 22);
            this.definicionesToolStripMenuItem1.Text = "Definiciones";
            // 
            // documentosDePagoToolStripMenuItem
            // 
            this.documentosDePagoToolStripMenuItem.Name = "documentosDePagoToolStripMenuItem";
            this.documentosDePagoToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.documentosDePagoToolStripMenuItem.Text = "Tipo de Documentos";
            // 
            // tarjetasCréditoDébitoToolStripMenuItem
            // 
            this.tarjetasCréditoDébitoToolStripMenuItem.Name = "tarjetasCréditoDébitoToolStripMenuItem";
            this.tarjetasCréditoDébitoToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.tarjetasCréditoDébitoToolStripMenuItem.Text = "Tarjetas Crédito / Débito";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(136, 6);
            // 
            // reportesToolStripMenuItem
            // 
            this.reportesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pacientesToolStripMenuItem1,
            this.cajaToolStripMenuItem1});
            this.reportesToolStripMenuItem.Name = "reportesToolStripMenuItem";
            this.reportesToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.reportesToolStripMenuItem.Text = "Reportes";
            // 
            // pacientesToolStripMenuItem1
            // 
            this.pacientesToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.citadosVsAtendidosToolStripMenuItem,
            this.pacientesAtendidosToolStripMenuItem,
            this.citadosVsAtendidosToolStripMenuItem1,
            this.citasReservadasVsConfirmadasToolStripMenuItem});
            this.pacientesToolStripMenuItem1.Name = "pacientesToolStripMenuItem1";
            this.pacientesToolStripMenuItem1.Size = new System.Drawing.Size(124, 22);
            this.pacientesToolStripMenuItem1.Text = "Pacientes";
            // 
            // citadosVsAtendidosToolStripMenuItem
            // 
            this.citadosVsAtendidosToolStripMenuItem.Name = "citadosVsAtendidosToolStripMenuItem";
            this.citadosVsAtendidosToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.citadosVsAtendidosToolStripMenuItem.Text = "Pacientes Citados";
            // 
            // pacientesAtendidosToolStripMenuItem
            // 
            this.pacientesAtendidosToolStripMenuItem.Name = "pacientesAtendidosToolStripMenuItem";
            this.pacientesAtendidosToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.pacientesAtendidosToolStripMenuItem.Text = "Pacientes Atendidos";
            // 
            // citadosVsAtendidosToolStripMenuItem1
            // 
            this.citadosVsAtendidosToolStripMenuItem1.Name = "citadosVsAtendidosToolStripMenuItem1";
            this.citadosVsAtendidosToolStripMenuItem1.Size = new System.Drawing.Size(250, 22);
            this.citadosVsAtendidosToolStripMenuItem1.Text = "Citados Vs. Atendidos";
            // 
            // citasReservadasVsConfirmadasToolStripMenuItem
            // 
            this.citasReservadasVsConfirmadasToolStripMenuItem.Name = "citasReservadasVsConfirmadasToolStripMenuItem";
            this.citasReservadasVsConfirmadasToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.citasReservadasVsConfirmadasToolStripMenuItem.Text = "Citas Reservadas Vs. Confirmadas";
            // 
            // cajaToolStripMenuItem1
            // 
            this.cajaToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ingresosPorDíaToolStripMenuItem,
            this.ingresosPorServicioToolStripMenuItem,
            this.ingresosPorTipoDePagoToolStripMenuItem,
            this.toolStripSeparator4,
            this.arqueosToolStripMenuItem});
            this.cajaToolStripMenuItem1.Name = "cajaToolStripMenuItem1";
            this.cajaToolStripMenuItem1.Size = new System.Drawing.Size(124, 22);
            this.cajaToolStripMenuItem1.Text = "Caja";
            // 
            // ingresosPorDíaToolStripMenuItem
            // 
            this.ingresosPorDíaToolStripMenuItem.Name = "ingresosPorDíaToolStripMenuItem";
            this.ingresosPorDíaToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.ingresosPorDíaToolStripMenuItem.Text = "Ingresos por Día";
            // 
            // ingresosPorServicioToolStripMenuItem
            // 
            this.ingresosPorServicioToolStripMenuItem.Name = "ingresosPorServicioToolStripMenuItem";
            this.ingresosPorServicioToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.ingresosPorServicioToolStripMenuItem.Text = "Ingresos por Servicio";
            // 
            // ingresosPorTipoDePagoToolStripMenuItem
            // 
            this.ingresosPorTipoDePagoToolStripMenuItem.Name = "ingresosPorTipoDePagoToolStripMenuItem";
            this.ingresosPorTipoDePagoToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.ingresosPorTipoDePagoToolStripMenuItem.Text = "Ingresos por Tipo de Pago";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(209, 6);
            // 
            // arqueosToolStripMenuItem
            // 
            this.arqueosToolStripMenuItem.Name = "arqueosToolStripMenuItem";
            this.arqueosToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.arqueosToolStripMenuItem.Text = "Arqueos";
            // 
            // asistencial
            // 
            this.asistencial.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.asistencialToolStripMenuItem,
            this.consultaToolStripMenuItem,
            this.toolStripSeparator5,
            this.reportesToolStripMenuItem1});
            this.asistencial.Name = "asistencial";
            this.asistencial.Size = new System.Drawing.Size(75, 20);
            this.asistencial.Text = "Asistencial";
            // 
            // asistencialToolStripMenuItem
            // 
            this.asistencialToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tomaDeSignosVitalesToolStripMenuItem1,
            this.actoMédicoToolStripMenuItem1});
            this.asistencialToolStripMenuItem.Name = "asistencialToolStripMenuItem";
            this.asistencialToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.asistencialToolStripMenuItem.Text = "Asistencial";
            // 
            // tomaDeSignosVitalesToolStripMenuItem1
            // 
            this.tomaDeSignosVitalesToolStripMenuItem1.Name = "tomaDeSignosVitalesToolStripMenuItem1";
            this.tomaDeSignosVitalesToolStripMenuItem1.Size = new System.Drawing.Size(196, 22);
            this.tomaDeSignosVitalesToolStripMenuItem1.Text = "Toma de Signos Vitales";
            // 
            // actoMédicoToolStripMenuItem1
            // 
            this.actoMédicoToolStripMenuItem1.Name = "actoMédicoToolStripMenuItem1";
            this.actoMédicoToolStripMenuItem1.Size = new System.Drawing.Size(196, 22);
            this.actoMédicoToolStripMenuItem1.Text = "Acto Médico";
            this.actoMédicoToolStripMenuItem1.Click += new System.EventHandler(this.actoMédicoToolStripMenuItem1_Click);
            // 
            // consultaToolStripMenuItem
            // 
            this.consultaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.historialMédicoToolStripMenuItem});
            this.consultaToolStripMenuItem.Name = "consultaToolStripMenuItem";
            this.consultaToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.consultaToolStripMenuItem.Text = "Consulta";
            // 
            // historialMédicoToolStripMenuItem
            // 
            this.historialMédicoToolStripMenuItem.Name = "historialMédicoToolStripMenuItem";
            this.historialMédicoToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.historialMédicoToolStripMenuItem.Text = "Historial Médico";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(127, 6);
            // 
            // reportesToolStripMenuItem1
            // 
            this.reportesToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.síntomasMásComunesToolStripMenuItem,
            this.diagnósticosMásComunesToolStripMenuItem,
            this.medicamentosMásPrescritosToolStripMenuItem,
            this.tratamientosMásEmpleadosToolStripMenuItem});
            this.reportesToolStripMenuItem1.Name = "reportesToolStripMenuItem1";
            this.reportesToolStripMenuItem1.Size = new System.Drawing.Size(130, 22);
            this.reportesToolStripMenuItem1.Text = "Reportes";
            // 
            // síntomasMásComunesToolStripMenuItem
            // 
            this.síntomasMásComunesToolStripMenuItem.Name = "síntomasMásComunesToolStripMenuItem";
            this.síntomasMásComunesToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.síntomasMásComunesToolStripMenuItem.Text = "Síntomas más comunes";
            // 
            // diagnósticosMásComunesToolStripMenuItem
            // 
            this.diagnósticosMásComunesToolStripMenuItem.Name = "diagnósticosMásComunesToolStripMenuItem";
            this.diagnósticosMásComunesToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.diagnósticosMásComunesToolStripMenuItem.Text = "Diagnósticos más comunes";
            // 
            // medicamentosMásPrescritosToolStripMenuItem
            // 
            this.medicamentosMásPrescritosToolStripMenuItem.Name = "medicamentosMásPrescritosToolStripMenuItem";
            this.medicamentosMásPrescritosToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.medicamentosMásPrescritosToolStripMenuItem.Text = "Medicamentos más prescritos";
            // 
            // tratamientosMásEmpleadosToolStripMenuItem
            // 
            this.tratamientosMásEmpleadosToolStripMenuItem.Name = "tratamientosMásEmpleadosToolStripMenuItem";
            this.tratamientosMásEmpleadosToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.tratamientosMásEmpleadosToolStripMenuItem.Text = "Tratamientos más empleados";
            // 
            // farmacia
            // 
            this.farmacia.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ventasToolStripMenuItem,
            this.toolStripSeparator9,
            this.almacenToolStripMenuItem,
            this.inventarioToolStripMenuItem1});
            this.farmacia.Name = "farmacia";
            this.farmacia.Size = new System.Drawing.Size(67, 20);
            this.farmacia.Text = "Farmacia";
            // 
            // ventasToolStripMenuItem
            // 
            this.ventasToolStripMenuItem.Name = "ventasToolStripMenuItem";
            this.ventasToolStripMenuItem.Size = new System.Drawing.Size(127, 22);
            this.ventasToolStripMenuItem.Text = "Ventas";
            this.ventasToolStripMenuItem.Click += new System.EventHandler(this.ventasToolStripMenuItem_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(124, 6);
            // 
            // almacenToolStripMenuItem
            // 
            this.almacenToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.comprasToolStripMenuItem,
            this.movimientosDeAlmacénToolStripMenuItem,
            this.toolStripSeparator6,
            this.kardexToolStripMenuItem});
            this.almacenToolStripMenuItem.Name = "almacenToolStripMenuItem";
            this.almacenToolStripMenuItem.Size = new System.Drawing.Size(127, 22);
            this.almacenToolStripMenuItem.Text = "Almacén";
            // 
            // comprasToolStripMenuItem
            // 
            this.comprasToolStripMenuItem.Name = "comprasToolStripMenuItem";
            this.comprasToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.comprasToolStripMenuItem.Text = "Compras";
            // 
            // movimientosDeAlmacénToolStripMenuItem
            // 
            this.movimientosDeAlmacénToolStripMenuItem.Name = "movimientosDeAlmacénToolStripMenuItem";
            this.movimientosDeAlmacénToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.movimientosDeAlmacénToolStripMenuItem.Text = "Movimientos de Almacén";
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(207, 6);
            // 
            // kardexToolStripMenuItem
            // 
            this.kardexToolStripMenuItem.Name = "kardexToolStripMenuItem";
            this.kardexToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.kardexToolStripMenuItem.Text = "Kardex";
            // 
            // inventarioToolStripMenuItem1
            // 
            this.inventarioToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.formulariosToolStripMenuItem,
            this.actualizaciónToolStripMenuItem,
            this.toolStripSeparator7,
            this.inventarioValorizadoToolStripMenuItem});
            this.inventarioToolStripMenuItem1.Name = "inventarioToolStripMenuItem1";
            this.inventarioToolStripMenuItem1.Size = new System.Drawing.Size(127, 22);
            this.inventarioToolStripMenuItem1.Text = "Inventario";
            // 
            // formulariosToolStripMenuItem
            // 
            this.formulariosToolStripMenuItem.Name = "formulariosToolStripMenuItem";
            this.formulariosToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.formulariosToolStripMenuItem.Text = "Formularios";
            // 
            // actualizaciónToolStripMenuItem
            // 
            this.actualizaciónToolStripMenuItem.Name = "actualizaciónToolStripMenuItem";
            this.actualizaciónToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.actualizaciónToolStripMenuItem.Text = "Actualización";
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(182, 6);
            // 
            // inventarioValorizadoToolStripMenuItem
            // 
            this.inventarioValorizadoToolStripMenuItem.Name = "inventarioValorizadoToolStripMenuItem";
            this.inventarioValorizadoToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.inventarioValorizadoToolStripMenuItem.Text = "Inventario Valorizado";
            // 
            // utilitarios
            // 
            this.utilitarios.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.usuariosToolStripMenuItem,
            this.ayudaToolStripMenuItem,
            this.toolStripSeparator10,
            this.cambiarDeUsuarioToolStripMenuItem,
            this.definicionesToolStripMenuItem2});
            this.utilitarios.Name = "utilitarios";
            this.utilitarios.Size = new System.Drawing.Size(69, 20);
            this.utilitarios.Text = "Utilitarios";
            // 
            // usuariosToolStripMenuItem
            // 
            this.usuariosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.perfilesDeUsuarioToolStripMenuItem,
            this.registroUsuariosToolStripMenuItem1,
            this.toolStripSeparator11,
            this.cambiarClaveToolStripMenuItem});
            this.usuariosToolStripMenuItem.Name = "usuariosToolStripMenuItem";
            this.usuariosToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.usuariosToolStripMenuItem.Text = "Usuarios";
            // 
            // perfilesDeUsuarioToolStripMenuItem
            // 
            this.perfilesDeUsuarioToolStripMenuItem.Name = "perfilesDeUsuarioToolStripMenuItem";
            this.perfilesDeUsuarioToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.perfilesDeUsuarioToolStripMenuItem.Text = "Perfiles de Usuario";
            this.perfilesDeUsuarioToolStripMenuItem.Click += new System.EventHandler(this.perfilesDeUsuarioToolStripMenuItem_Click);
            // 
            // registroUsuariosToolStripMenuItem1
            // 
            this.registroUsuariosToolStripMenuItem1.Name = "registroUsuariosToolStripMenuItem1";
            this.registroUsuariosToolStripMenuItem1.Size = new System.Drawing.Size(181, 22);
            this.registroUsuariosToolStripMenuItem1.Text = "Registro de Usuarios";
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(178, 6);
            // 
            // cambiarClaveToolStripMenuItem
            // 
            this.cambiarClaveToolStripMenuItem.Name = "cambiarClaveToolStripMenuItem";
            this.cambiarClaveToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.cambiarClaveToolStripMenuItem.Text = "Cambiar Clave";
            // 
            // ayudaToolStripMenuItem
            // 
            this.ayudaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ayudaEnLíneaToolStripMenuItem,
            this.acercaDeToolStripMenuItem});
            this.ayudaToolStripMenuItem.Name = "ayudaToolStripMenuItem";
            this.ayudaToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.ayudaToolStripMenuItem.Text = "Ayuda";
            // 
            // ayudaEnLíneaToolStripMenuItem
            // 
            this.ayudaEnLíneaToolStripMenuItem.Name = "ayudaEnLíneaToolStripMenuItem";
            this.ayudaEnLíneaToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.ayudaEnLíneaToolStripMenuItem.Text = "Ayuda en línea";
            // 
            // acercaDeToolStripMenuItem
            // 
            this.acercaDeToolStripMenuItem.Name = "acercaDeToolStripMenuItem";
            this.acercaDeToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.acercaDeToolStripMenuItem.Text = "Acerca de...";
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(175, 6);
            // 
            // cambiarDeUsuarioToolStripMenuItem
            // 
            this.cambiarDeUsuarioToolStripMenuItem.Name = "cambiarDeUsuarioToolStripMenuItem";
            this.cambiarDeUsuarioToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.cambiarDeUsuarioToolStripMenuItem.Text = "Cambiar de Usuario";
            this.cambiarDeUsuarioToolStripMenuItem.Click += new System.EventHandler(this.cambiarDeUsuarioToolStripMenuItem_Click);
            // 
            // definicionesToolStripMenuItem2
            // 
            this.definicionesToolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.partesDelCuerpoToolStripMenuItem});
            this.definicionesToolStripMenuItem2.Name = "definicionesToolStripMenuItem2";
            this.definicionesToolStripMenuItem2.Size = new System.Drawing.Size(178, 22);
            this.definicionesToolStripMenuItem2.Text = "Definiciones";
            // 
            // partesDelCuerpoToolStripMenuItem
            // 
            this.partesDelCuerpoToolStripMenuItem.Name = "partesDelCuerpoToolStripMenuItem";
            this.partesDelCuerpoToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.partesDelCuerpoToolStripMenuItem.Text = "Partes del Cuerpo";
            this.partesDelCuerpoToolStripMenuItem.Click += new System.EventHandler(this.partesDelCuerpoToolStripMenuItem_Click);
            // 
            // salir
            // 
            this.salir.Name = "salir";
            this.salir.Size = new System.Drawing.Size(41, 20);
            this.salir.Text = "Salir";
            this.salir.Click += new System.EventHandler(this.salirToolStripMenuItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatuslblUsuario,
            this.toolStripStatusLabel3,
            this.toolStripStatusLabel2,
            this.toolStripStatuslblPerfilNombre,
            this.toolStripStatuslblPerfilId});
            this.statusStrip1.Location = new System.Drawing.Point(0, 424);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.ManagerRenderMode;
            this.statusStrip1.Size = new System.Drawing.Size(913, 22);
            this.statusStrip1.TabIndex = 4;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(53, 17);
            this.toolStripStatusLabel1.Text = "Usuario: ";
            // 
            // toolStripStatuslblUsuario
            // 
            this.toolStripStatuslblUsuario.ForeColor = System.Drawing.Color.Blue;
            this.toolStripStatuslblUsuario.Name = "toolStripStatuslblUsuario";
            this.toolStripStatuslblUsuario.Size = new System.Drawing.Size(47, 17);
            this.toolStripStatuslblUsuario.Text = "Usuario";
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(64, 17);
            this.toolStripStatusLabel3.Text = "                   ";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(37, 17);
            this.toolStripStatusLabel2.Text = "Perfil:";
            // 
            // toolStripStatuslblPerfilNombre
            // 
            this.toolStripStatuslblPerfilNombre.ForeColor = System.Drawing.Color.Blue;
            this.toolStripStatuslblPerfilNombre.Name = "toolStripStatuslblPerfilNombre";
            this.toolStripStatuslblPerfilNombre.Size = new System.Drawing.Size(34, 17);
            this.toolStripStatuslblPerfilNombre.Text = "Perfil";
            // 
            // toolStripStatuslblPerfilId
            // 
            this.toolStripStatuslblPerfilId.Name = "toolStripStatuslblPerfilId";
            this.toolStripStatuslblPerfilId.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatuslblPerfilId.Text = "toolStripStatusLabel3";
            this.toolStripStatuslblPerfilId.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = global::pjDermaBelle.Properties.Resources.Logo1;
            this.pictureBox1.Location = new System.Drawing.Point(0, 24);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(913, 422);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // gpbLogin
            // 
            this.gpbLogin.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.gpbLogin.Controls.Add(this.label4);
            this.gpbLogin.Controls.Add(this.label3);
            this.gpbLogin.Controls.Add(this.pictureBox2);
            this.gpbLogin.Controls.Add(this.btnSalir);
            this.gpbLogin.Controls.Add(this.label1);
            this.gpbLogin.Controls.Add(this.btnIngresar);
            this.gpbLogin.Controls.Add(this.txtUsuario);
            this.gpbLogin.Controls.Add(this.txtPassword);
            this.gpbLogin.Controls.Add(this.label2);
            this.gpbLogin.Controls.Add(this.shapeContainer2);
            this.gpbLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbLogin.Location = new System.Drawing.Point(402, 163);
            this.gpbLogin.Name = "gpbLogin";
            this.gpbLogin.Size = new System.Drawing.Size(499, 237);
            this.gpbLogin.TabIndex = 12;
            this.gpbLogin.TabStop = false;
            this.gpbLogin.Text = "Inicio de Sesión";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(244, 153);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(240, 21);
            this.label4.TabIndex = 10;
            this.label4.Text = "(NOTA: No distingue mayúsculas de minúsculas)";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(158, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(325, 56);
            this.label3.TabIndex = 9;
            this.label3.Text = "Por favor ingrese un Nombre de Usuario y Contraseña válidos para iniciar sesión e" +
    "n el sistema";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::pjDermaBelle.Properties.Resources.register;
            this.pictureBox2.Location = new System.Drawing.Point(6, 49);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(147, 138);
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // btnSalir
            // 
            this.btnSalir.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSalir.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalir.Location = new System.Drawing.Point(369, 196);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(109, 32);
            this.btnSalir.TabIndex = 7;
            this.btnSalir.Text = "Salir";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(197, 92);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Usuario";
            // 
            // btnIngresar
            // 
            this.btnIngresar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnIngresar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIngresar.Location = new System.Drawing.Point(176, 196);
            this.btnIngresar.Name = "btnIngresar";
            this.btnIngresar.Size = new System.Drawing.Size(109, 32);
            this.btnIngresar.TabIndex = 6;
            this.btnIngresar.Text = "Ingresar";
            this.btnIngresar.UseVisualStyleBackColor = true;
            this.btnIngresar.Click += new System.EventHandler(this.btnIngresar_Click);
            // 
            // txtUsuario
            // 
            this.txtUsuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUsuario.Location = new System.Drawing.Point(260, 89);
            this.txtUsuario.MaxLength = 20;
            this.txtUsuario.Name = "txtUsuario";
            this.txtUsuario.Size = new System.Drawing.Size(218, 23);
            this.txtUsuario.TabIndex = 3;
            // 
            // txtPassword
            // 
            this.txtPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPassword.Location = new System.Drawing.Point(260, 117);
            this.txtPassword.MaxLength = 20;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(218, 23);
            this.txtPassword.TabIndex = 5;
            this.txtPassword.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPassword_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(173, 120);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Contraseña";
            // 
            // shapeContainer2
            // 
            this.shapeContainer2.Location = new System.Drawing.Point(3, 19);
            this.shapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer2.Name = "shapeContainer2";
            this.shapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape1});
            this.shapeContainer2.Size = new System.Drawing.Size(493, 215);
            this.shapeContainer2.TabIndex = 8;
            this.shapeContainer2.TabStop = false;
            // 
            // lineShape1
            // 
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 153;
            this.lineShape1.X2 = 153;
            this.lineShape1.Y1 = -7;
            this.lineShape1.Y2 = 212;
            // 
            // frmMenuPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(913, 446);
            this.Controls.Add(this.gpbLogin);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuPrincipal);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuPrincipal;
            this.Name = "frmMenuPrincipal";
            this.Text = "Sistema de Gestión de Historias Clínicas y Administrativo";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmMenuPrincipal_Load);
            this.menuPrincipal.ResumeLayout(false);
            this.menuPrincipal.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.gpbLogin.ResumeLayout(false);
            this.gpbLogin.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuPrincipal;
        private System.Windows.Forms.ToolStripMenuItem administrativo;
        private System.Windows.Forms.ToolStripMenuItem maestros;
        private System.Windows.Forms.ToolStripMenuItem profesionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gradosDeInstrucciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ubigeoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem documentosDeIdentidadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem citas;
        private System.Windows.Forms.ToolStripMenuItem asistencial;
        private System.Windows.Forms.ToolStripMenuItem farmacia;
        private System.Windows.Forms.ToolStripMenuItem pacientes;
        private System.Windows.Forms.ToolStripMenuItem filiaciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem controlToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem profesionalesDeLaSaludToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem especialidadesToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem tarifario;
        private System.Windows.Forms.ToolStripMenuItem serviciosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem productosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem definiciones;
        private System.Windows.Forms.ToolStripMenuItem definicionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultoriosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem serviciosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem horariiosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem citasToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem reservaDeCitasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cajaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem operacionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem arqueoToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem definicionesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem documentosDePagoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tarjetasCréditoDébitoToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem reportesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pacientesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem citadosVsAtendidosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pacientesAtendidosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem citadosVsAtendidosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem citasReservadasVsConfirmadasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cajaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem ingresosPorDíaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ingresosPorServicioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ingresosPorTipoDePagoToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem arqueosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asistencialToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tomaDeSignosVitalesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem actoMédicoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem consultaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem historialMédicoToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem reportesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem síntomasMásComunesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem diagnósticosMásComunesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem medicamentosMásPrescritosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tratamientosMásEmpleadosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem almacenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem comprasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem movimientosDeAlmacénToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem kardexToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inventarioToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem formulariosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem actualizaciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem inventarioValorizadoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem utilitarios;
        private System.Windows.Forms.ToolStripMenuItem usuariosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem perfilesDeUsuarioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registroUsuariosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem ayudaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ayudaEnLíneaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem acercaDeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salir;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatuslblUsuario;
        private System.Windows.Forms.ToolStripMenuItem tipoDeTarifaToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem principiosActivosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem laboratoriosToolStripMenuItem;
        private System.Windows.Forms.GroupBox gpbLogin;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnIngresar;
        private System.Windows.Forms.TextBox txtUsuario;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label label2;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer2;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripMenuItem ventasToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.StatusStrip statusStrip1;
        public System.Windows.Forms.ToolStripStatusLabel toolStripStatuslblPerfilNombre;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatuslblPerfilId;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripMenuItem cambiarClaveToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripMenuItem cambiarDeUsuarioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem definicionesToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem partesDelCuerpoToolStripMenuItem;
    }
}

