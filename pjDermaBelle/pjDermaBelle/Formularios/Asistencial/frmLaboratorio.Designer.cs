﻿namespace pjDermaBelle.Formularios.Asistencial
{
    partial class frmLaboratorio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbcLaboratorio = new System.Windows.Forms.TabControl();
            this.tbpAnverso = new System.Windows.Forms.TabPage();
            this.label5 = new System.Windows.Forms.Label();
            this.chklDrogas = new System.Windows.Forms.CheckedListBox();
            this.label7 = new System.Windows.Forms.Label();
            this.chklOrina = new System.Windows.Forms.CheckedListBox();
            this.label6 = new System.Windows.Forms.Label();
            this.chklBioquimica = new System.Windows.Forms.CheckedListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.chklHemostasis = new System.Windows.Forms.CheckedListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.chklHematologia = new System.Windows.Forms.CheckedListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.chklPerfiles = new System.Windows.Forms.CheckedListBox();
            this.tbpReverso = new System.Windows.Forms.TabPage();
            this.label14 = new System.Windows.Forms.Label();
            this.chklInfecciosas = new System.Windows.Forms.CheckedListBox();
            this.txtOtros = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.chklLiquidos = new System.Windows.Forms.CheckedListBox();
            this.label11 = new System.Windows.Forms.Label();
            this.chklMicrobiologia = new System.Windows.Forms.CheckedListBox();
            this.label8 = new System.Windows.Forms.Label();
            this.chklVirales = new System.Windows.Forms.CheckedListBox();
            this.label9 = new System.Windows.Forms.Label();
            this.chklAutoinmunidad = new System.Windows.Forms.CheckedListBox();
            this.label10 = new System.Windows.Forms.Label();
            this.chklHormonas = new System.Windows.Forms.CheckedListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.chklOncologicos = new System.Windows.Forms.CheckedListBox();
            this.tbcLaboratorio.SuspendLayout();
            this.tbpAnverso.SuspendLayout();
            this.tbpReverso.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbcLaboratorio
            // 
            this.tbcLaboratorio.Controls.Add(this.tbpAnverso);
            this.tbcLaboratorio.Controls.Add(this.tbpReverso);
            this.tbcLaboratorio.Location = new System.Drawing.Point(2, 2);
            this.tbcLaboratorio.Name = "tbcLaboratorio";
            this.tbcLaboratorio.SelectedIndex = 0;
            this.tbcLaboratorio.Size = new System.Drawing.Size(1346, 679);
            this.tbcLaboratorio.TabIndex = 0;
            // 
            // tbpAnverso
            // 
            this.tbpAnverso.Controls.Add(this.label5);
            this.tbpAnverso.Controls.Add(this.chklDrogas);
            this.tbpAnverso.Controls.Add(this.label7);
            this.tbpAnverso.Controls.Add(this.chklOrina);
            this.tbpAnverso.Controls.Add(this.label6);
            this.tbpAnverso.Controls.Add(this.chklBioquimica);
            this.tbpAnverso.Controls.Add(this.label3);
            this.tbpAnverso.Controls.Add(this.chklHemostasis);
            this.tbpAnverso.Controls.Add(this.label2);
            this.tbpAnverso.Controls.Add(this.chklHematologia);
            this.tbpAnverso.Controls.Add(this.label1);
            this.tbpAnverso.Controls.Add(this.chklPerfiles);
            this.tbpAnverso.Location = new System.Drawing.Point(4, 22);
            this.tbpAnverso.Name = "tbpAnverso";
            this.tbpAnverso.Padding = new System.Windows.Forms.Padding(3);
            this.tbpAnverso.Size = new System.Drawing.Size(1338, 653);
            this.tbpAnverso.TabIndex = 0;
            this.tbpAnverso.Text = "Anverso";
            this.tbpAnverso.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Blue;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(872, 392);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(463, 20);
            this.label5.TabIndex = 21;
            this.label5.Text = "DROGAS";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chklDrogas
            // 
            this.chklDrogas.FormattingEnabled = true;
            this.chklDrogas.Location = new System.Drawing.Point(872, 418);
            this.chklDrogas.Name = "chklDrogas";
            this.chklDrogas.Size = new System.Drawing.Size(463, 229);
            this.chklDrogas.TabIndex = 20;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Blue;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(872, 3);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(463, 20);
            this.label7.TabIndex = 19;
            this.label7.Text = "ORINA";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chklOrina
            // 
            this.chklOrina.FormattingEnabled = true;
            this.chklOrina.Location = new System.Drawing.Point(872, 26);
            this.chklOrina.Name = "chklOrina";
            this.chklOrina.Size = new System.Drawing.Size(463, 364);
            this.chklOrina.TabIndex = 18;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Blue;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(471, 3);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(396, 20);
            this.label6.TabIndex = 17;
            this.label6.Text = "BIOQUIMICA";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chklBioquimica
            // 
            this.chklBioquimica.FormattingEnabled = true;
            this.chklBioquimica.Location = new System.Drawing.Point(471, 28);
            this.chklBioquimica.Name = "chklBioquimica";
            this.chklBioquimica.Size = new System.Drawing.Size(396, 619);
            this.chklBioquimica.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Blue;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(3, 423);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(463, 20);
            this.label3.TabIndex = 15;
            this.label3.Text = "HEMOSTASIS Y TROMBOSIS";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chklHemostasis
            // 
            this.chklHemostasis.FormattingEnabled = true;
            this.chklHemostasis.Location = new System.Drawing.Point(3, 448);
            this.chklHemostasis.Name = "chklHemostasis";
            this.chklHemostasis.Size = new System.Drawing.Size(463, 199);
            this.chklHemostasis.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Blue;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(3, 168);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(463, 20);
            this.label2.TabIndex = 13;
            this.label2.Text = "HEMATOLOGÍA";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chklHematologia
            // 
            this.chklHematologia.FormattingEnabled = true;
            this.chklHematologia.Location = new System.Drawing.Point(3, 191);
            this.chklHematologia.Name = "chklHematologia";
            this.chklHematologia.Size = new System.Drawing.Size(463, 229);
            this.chklHematologia.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Blue;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(3, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(463, 20);
            this.label1.TabIndex = 11;
            this.label1.Text = "PERFILES";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chklPerfiles
            // 
            this.chklPerfiles.FormattingEnabled = true;
            this.chklPerfiles.Location = new System.Drawing.Point(3, 26);
            this.chklPerfiles.Name = "chklPerfiles";
            this.chklPerfiles.Size = new System.Drawing.Size(463, 139);
            this.chklPerfiles.TabIndex = 10;
            // 
            // tbpReverso
            // 
            this.tbpReverso.Controls.Add(this.label14);
            this.tbpReverso.Controls.Add(this.chklInfecciosas);
            this.tbpReverso.Controls.Add(this.txtOtros);
            this.tbpReverso.Controls.Add(this.label13);
            this.tbpReverso.Controls.Add(this.label12);
            this.tbpReverso.Controls.Add(this.chklLiquidos);
            this.tbpReverso.Controls.Add(this.label11);
            this.tbpReverso.Controls.Add(this.chklMicrobiologia);
            this.tbpReverso.Controls.Add(this.label8);
            this.tbpReverso.Controls.Add(this.chklVirales);
            this.tbpReverso.Controls.Add(this.label9);
            this.tbpReverso.Controls.Add(this.chklAutoinmunidad);
            this.tbpReverso.Controls.Add(this.label10);
            this.tbpReverso.Controls.Add(this.chklHormonas);
            this.tbpReverso.Controls.Add(this.label4);
            this.tbpReverso.Controls.Add(this.chklOncologicos);
            this.tbpReverso.Location = new System.Drawing.Point(4, 22);
            this.tbpReverso.Name = "tbpReverso";
            this.tbpReverso.Padding = new System.Windows.Forms.Padding(3);
            this.tbpReverso.Size = new System.Drawing.Size(1338, 653);
            this.tbpReverso.TabIndex = 1;
            this.tbpReverso.Text = "Reverso";
            this.tbpReverso.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.BackColor = System.Drawing.Color.Blue;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(403, 293);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(463, 20);
            this.label14.TabIndex = 37;
            this.label14.Text = "INFECCIOSAS";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chklInfecciosas
            // 
            this.chklInfecciosas.FormattingEnabled = true;
            this.chklInfecciosas.Location = new System.Drawing.Point(403, 317);
            this.chklInfecciosas.Name = "chklInfecciosas";
            this.chklInfecciosas.Size = new System.Drawing.Size(463, 334);
            this.chklInfecciosas.TabIndex = 36;
            // 
            // txtOtros
            // 
            this.txtOtros.Location = new System.Drawing.Point(871, 521);
            this.txtOtros.Multiline = true;
            this.txtOtros.Name = "txtOtros";
            this.txtOtros.Size = new System.Drawing.Size(463, 130);
            this.txtOtros.TabIndex = 35;
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.Color.Blue;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(871, 497);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(463, 20);
            this.label13.TabIndex = 34;
            this.label13.Text = "OTROS";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.Color.Blue;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(871, 333);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(463, 20);
            this.label12.TabIndex = 33;
            this.label12.Text = "LIQUIDOS CORPORALES";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chklLiquidos
            // 
            this.chklLiquidos.FormattingEnabled = true;
            this.chklLiquidos.Location = new System.Drawing.Point(871, 356);
            this.chklLiquidos.Name = "chklLiquidos";
            this.chklLiquidos.Size = new System.Drawing.Size(463, 139);
            this.chklLiquidos.TabIndex = 32;
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.Blue;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(871, 168);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(463, 20);
            this.label11.TabIndex = 31;
            this.label11.Text = "MICROBIOLOGIA";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chklMicrobiologia
            // 
            this.chklMicrobiologia.FormattingEnabled = true;
            this.chklMicrobiologia.Location = new System.Drawing.Point(871, 191);
            this.chklMicrobiologia.Name = "chklMicrobiologia";
            this.chklMicrobiologia.Size = new System.Drawing.Size(463, 139);
            this.chklMicrobiologia.TabIndex = 30;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.Blue;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(871, 3);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(463, 20);
            this.label8.TabIndex = 29;
            this.label8.Text = "VIRALES";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chklVirales
            // 
            this.chklVirales.FormattingEnabled = true;
            this.chklVirales.Location = new System.Drawing.Point(871, 26);
            this.chklVirales.Name = "chklVirales";
            this.chklVirales.Size = new System.Drawing.Size(463, 139);
            this.chklVirales.TabIndex = 28;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.Blue;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(403, 3);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(463, 20);
            this.label9.TabIndex = 27;
            this.label9.Text = "AUTOINMUNIDAD";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chklAutoinmunidad
            // 
            this.chklAutoinmunidad.FormattingEnabled = true;
            this.chklAutoinmunidad.Location = new System.Drawing.Point(403, 27);
            this.chklAutoinmunidad.Name = "chklAutoinmunidad";
            this.chklAutoinmunidad.Size = new System.Drawing.Size(463, 259);
            this.chklAutoinmunidad.TabIndex = 26;
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.Blue;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(-64, 293);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(463, 20);
            this.label10.TabIndex = 25;
            this.label10.Text = "HORMONAS";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chklHormonas
            // 
            this.chklHormonas.FormattingEnabled = true;
            this.chklHormonas.Location = new System.Drawing.Point(3, 317);
            this.chklHormonas.Name = "chklHormonas";
            this.chklHormonas.Size = new System.Drawing.Size(396, 334);
            this.chklHormonas.TabIndex = 24;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Blue;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(3, 3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(396, 20);
            this.label4.TabIndex = 23;
            this.label4.Text = "MARCADORES ONCOLOGICOS";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chklOncologicos
            // 
            this.chklOncologicos.FormattingEnabled = true;
            this.chklOncologicos.Location = new System.Drawing.Point(3, 26);
            this.chklOncologicos.Name = "chklOncologicos";
            this.chklOncologicos.Size = new System.Drawing.Size(396, 259);
            this.chklOncologicos.TabIndex = 22;
            // 
            // frmLaboratorio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1348, 684);
            this.Controls.Add(this.tbcLaboratorio);
            this.Name = "frmLaboratorio";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Solicitud de Examenes Auxilires - Laboratorio";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmLaboratorio_FormClosed);
            this.Load += new System.EventHandler(this.frmLaboratorio_Load);
            this.tbcLaboratorio.ResumeLayout(false);
            this.tbpAnverso.ResumeLayout(false);
            this.tbpReverso.ResumeLayout(false);
            this.tbpReverso.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tbcLaboratorio;
        private System.Windows.Forms.TabPage tbpAnverso;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckedListBox chklHemostasis;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckedListBox chklHematologia;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckedListBox chklPerfiles;
        private System.Windows.Forms.TabPage tbpReverso;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckedListBox chklBioquimica;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckedListBox chklDrogas;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckedListBox chklOrina;
        private System.Windows.Forms.TextBox txtOtros;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckedListBox chklLiquidos;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckedListBox chklMicrobiologia;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckedListBox chklVirales;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckedListBox chklAutoinmunidad;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckedListBox chklHormonas;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckedListBox chklOncologicos;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.CheckedListBox chklInfecciosas;


    }
}