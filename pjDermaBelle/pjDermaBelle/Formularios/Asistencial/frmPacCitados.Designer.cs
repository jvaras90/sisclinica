﻿namespace pjDermaBelle
{
    partial class frmPacCitados
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dgvPacCitados = new System.Windows.Forms.DataGridView();
            this.btnAbrirFicha = new System.Windows.Forms.DataGridViewButtonColumn();
            this.label6 = new System.Windows.Forms.Label();
            this.lblCitados = new System.Windows.Forms.Label();
            this.lblAtendidos = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.dgvPacFichas = new System.Windows.Forms.DataGridView();
            this.dtpCitaFecha = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblServicio = new System.Windows.Forms.Label();
            this.lblMedico = new System.Windows.Forms.Label();
            this.lblConsultorio = new System.Windows.Forms.Label();
            this.citasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPacCitados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPacFichas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.citasBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvPacCitados
            // 
            this.dgvPacCitados.AllowUserToAddRows = false;
            this.dgvPacCitados.AllowUserToDeleteRows = false;
            this.dgvPacCitados.AllowUserToOrderColumns = true;
            this.dgvPacCitados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPacCitados.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.btnAbrirFicha});
            this.dgvPacCitados.Location = new System.Drawing.Point(13, 67);
            this.dgvPacCitados.Name = "dgvPacCitados";
            this.dgvPacCitados.ReadOnly = true;
            this.dgvPacCitados.Size = new System.Drawing.Size(1318, 500);
            this.dgvPacCitados.TabIndex = 3;
            this.dgvPacCitados.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPacCitados_CellClick);
            this.dgvPacCitados.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgvPacCitados_CellPainting);
            this.dgvPacCitados.SelectionChanged += new System.EventHandler(this.dgvPacCitados_SelectionChanged);
            // 
            // btnAbrirFicha
            // 
            this.btnAbrirFicha.HeaderText = "";
            this.btnAbrirFicha.Name = "btnAbrirFicha";
            this.btnAbrirFicha.ReadOnly = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(12, 575);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(198, 20);
            this.label6.TabIndex = 4;
            this.label6.Text = "Total de Pacientes Citados";
            // 
            // lblCitados
            // 
            this.lblCitados.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCitados.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCitados.ForeColor = System.Drawing.Color.Blue;
            this.lblCitados.Location = new System.Drawing.Point(216, 574);
            this.lblCitados.Name = "lblCitados";
            this.lblCitados.Size = new System.Drawing.Size(52, 23);
            this.lblCitados.TabIndex = 5;
            this.lblCitados.Text = "label7";
            this.lblCitados.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblAtendidos
            // 
            this.lblAtendidos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAtendidos.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAtendidos.ForeColor = System.Drawing.Color.Blue;
            this.lblAtendidos.Location = new System.Drawing.Point(637, 574);
            this.lblAtendidos.Name = "lblAtendidos";
            this.lblAtendidos.Size = new System.Drawing.Size(52, 23);
            this.lblAtendidos.TabIndex = 7;
            this.lblAtendidos.Text = "label8";
            this.lblAtendidos.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(415, 575);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(216, 20);
            this.label9.TabIndex = 6;
            this.label9.Text = "Total de Pacientes Atendidos";
            // 
            // dgvPacFichas
            // 
            this.dgvPacFichas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPacFichas.Location = new System.Drawing.Point(1017, 8);
            this.dgvPacFichas.Name = "dgvPacFichas";
            this.dgvPacFichas.ReadOnly = true;
            this.dgvPacFichas.Size = new System.Drawing.Size(314, 46);
            this.dgvPacFichas.TabIndex = 15;
            this.dgvPacFichas.Visible = false;
            // 
            // dtpCitaFecha
            // 
            this.dtpCitaFecha.Enabled = false;
            this.dtpCitaFecha.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpCitaFecha.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpCitaFecha.Location = new System.Drawing.Point(783, 29);
            this.dtpCitaFecha.Name = "dtpCitaFecha";
            this.dtpCitaFecha.Size = new System.Drawing.Size(103, 23);
            this.dtpCitaFecha.TabIndex = 25;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(724, 32);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 17);
            this.label4.TabIndex = 18;
            this.label4.Text = "Fecha";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(16, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 17);
            this.label3.TabIndex = 19;
            this.label3.Text = "Médico";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(11, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 17);
            this.label5.TabIndex = 20;
            this.label5.Text = "Servicio";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(692, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 17);
            this.label2.TabIndex = 21;
            this.label2.Text = "Consultorio";
            // 
            // lblServicio
            // 
            this.lblServicio.AutoSize = true;
            this.lblServicio.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblServicio.ForeColor = System.Drawing.Color.Blue;
            this.lblServicio.Location = new System.Drawing.Point(77, 8);
            this.lblServicio.Name = "lblServicio";
            this.lblServicio.Size = new System.Drawing.Size(58, 17);
            this.lblServicio.TabIndex = 26;
            this.lblServicio.Text = "Servicio";
            // 
            // lblMedico
            // 
            this.lblMedico.AutoSize = true;
            this.lblMedico.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMedico.ForeColor = System.Drawing.Color.Blue;
            this.lblMedico.Location = new System.Drawing.Point(77, 32);
            this.lblMedico.Name = "lblMedico";
            this.lblMedico.Size = new System.Drawing.Size(53, 17);
            this.lblMedico.TabIndex = 27;
            this.lblMedico.Text = "Médico";
            // 
            // lblConsultorio
            // 
            this.lblConsultorio.AutoSize = true;
            this.lblConsultorio.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConsultorio.ForeColor = System.Drawing.Color.Blue;
            this.lblConsultorio.Location = new System.Drawing.Point(780, 8);
            this.lblConsultorio.Name = "lblConsultorio";
            this.lblConsultorio.Size = new System.Drawing.Size(79, 17);
            this.lblConsultorio.TabIndex = 28;
            this.lblConsultorio.Text = "Consultorio";
            // 
            // citasBindingSource
            // 
            this.citasBindingSource.DataMember = "Citas";
            // 
            // frmPacCitados
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1343, 608);
            this.Controls.Add(this.lblConsultorio);
            this.Controls.Add(this.lblMedico);
            this.Controls.Add(this.lblServicio);
            this.Controls.Add(this.dtpCitaFecha);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dgvPacFichas);
            this.Controls.Add(this.lblAtendidos);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.lblCitados);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.dgvPacCitados);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Name = "frmPacCitados";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pacientes Citados";
            this.Load += new System.EventHandler(this.frmPacCitados_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPacCitados)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPacFichas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.citasBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource citasBindingSource;
        private System.Windows.Forms.DataGridView dgvPacCitados;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblCitados;
        private System.Windows.Forms.Label lblAtendidos;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridView dgvPacFichas;
        public System.Windows.Forms.DateTimePicker dtpCitaFecha;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridViewButtonColumn btnAbrirFicha;
        public System.Windows.Forms.Label lblMedico;
        public System.Windows.Forms.Label lblServicio;
        public System.Windows.Forms.Label lblConsultorio;
    }
}