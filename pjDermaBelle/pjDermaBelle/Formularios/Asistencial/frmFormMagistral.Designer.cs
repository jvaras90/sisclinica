﻿namespace pjDermaBelle.Formularios.Asistencial
{
    partial class frmFormMagistral
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cmbCompuestos = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCantComp = new System.Windows.Forms.TextBox();
            this.cmbElementoBase = new System.Windows.Forms.ComboBox();
            this.txtPrd_Descripcion = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lblUnidad = new System.Windows.Forms.Label();
            this.btnAgregar = new System.Windows.Forms.Button();
            this.btnGrabar = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCantElemBase = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.txtPrd_Id = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dgvFormula = new System.Windows.Forms.DataGridView();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.DelCompuesto = new System.Windows.Forms.DataGridViewButtonColumn();
            this.UpdCompuesto = new System.Windows.Forms.DataGridViewButtonColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFormula)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(53, 84);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Compuesto";
            // 
            // cmbCompuestos
            // 
            this.cmbCompuestos.FormattingEnabled = true;
            this.cmbCompuestos.Location = new System.Drawing.Point(116, 80);
            this.cmbCompuestos.Name = "cmbCompuestos";
            this.cmbCompuestos.Size = new System.Drawing.Size(299, 21);
            this.cmbCompuestos.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(424, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Cantidad";
            // 
            // txtCantComp
            // 
            this.txtCantComp.Location = new System.Drawing.Point(479, 80);
            this.txtCantComp.Name = "txtCantComp";
            this.txtCantComp.Size = new System.Drawing.Size(47, 20);
            this.txtCantComp.TabIndex = 3;
            // 
            // cmbElementoBase
            // 
            this.cmbElementoBase.FormattingEnabled = true;
            this.cmbElementoBase.Location = new System.Drawing.Point(116, 109);
            this.cmbElementoBase.Name = "cmbElementoBase";
            this.cmbElementoBase.Size = new System.Drawing.Size(299, 21);
            this.cmbElementoBase.TabIndex = 5;
            // 
            // txtPrd_Descripcion
            // 
            this.txtPrd_Descripcion.Location = new System.Drawing.Point(116, 39);
            this.txtPrd_Descripcion.Name = "txtPrd_Descripcion";
            this.txtPrd_Descripcion.Size = new System.Drawing.Size(441, 20);
            this.txtPrd_Descripcion.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(69, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Nombre";
            // 
            // lblUnidad
            // 
            this.lblUnidad.AutoSize = true;
            this.lblUnidad.Location = new System.Drawing.Point(530, 84);
            this.lblUnidad.Name = "lblUnidad";
            this.lblUnidad.Size = new System.Drawing.Size(27, 13);
            this.lblUnidad.TabIndex = 11;
            this.lblUnidad.Text = "Und";
            this.lblUnidad.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnAgregar
            // 
            this.btnAgregar.Location = new System.Drawing.Point(572, 80);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(69, 52);
            this.btnAgregar.TabIndex = 12;
            this.btnAgregar.Text = "Agregar a la Fórmula";
            this.btnAgregar.UseVisualStyleBackColor = true;
            // 
            // btnGrabar
            // 
            this.btnGrabar.Location = new System.Drawing.Point(501, 340);
            this.btnGrabar.Name = "btnGrabar";
            this.btnGrabar.Size = new System.Drawing.Size(140, 42);
            this.btnGrabar.TabIndex = 13;
            this.btnGrabar.Text = "Grabar Fórmula";
            this.btnGrabar.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(530, 113);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(27, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Und";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtCantElemBase
            // 
            this.txtCantElemBase.Location = new System.Drawing.Point(479, 109);
            this.txtCantElemBase.Name = "txtCantElemBase";
            this.txtCantElemBase.Size = new System.Drawing.Size(47, 20);
            this.txtCantElemBase.TabIndex = 15;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(424, 113);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Cantidad";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(16, 111);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(97, 17);
            this.checkBox1.TabIndex = 17;
            this.checkBox1.Text = "Elemento Base";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // txtPrd_Id
            // 
            this.txtPrd_Id.Location = new System.Drawing.Point(116, 11);
            this.txtPrd_Id.Name = "txtPrd_Id";
            this.txtPrd_Id.Size = new System.Drawing.Size(96, 20);
            this.txtPrd_Id.TabIndex = 19;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(73, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "Código";
            // 
            // dgvFormula
            // 
            this.dgvFormula.AllowUserToAddRows = false;
            this.dgvFormula.AllowUserToDeleteRows = false;
            this.dgvFormula.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFormula.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DelCompuesto,
            this.UpdCompuesto});
            this.dgvFormula.Location = new System.Drawing.Point(16, 144);
            this.dgvFormula.Name = "dgvFormula";
            this.dgvFormula.ReadOnly = true;
            this.dgvFormula.Size = new System.Drawing.Size(479, 238);
            this.dgvFormula.TabIndex = 20;
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(650, 397);
            this.shapeContainer1.TabIndex = 21;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape1
            // 
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 2;
            this.lineShape1.X2 = 645;
            this.lineShape1.Y1 = 69;
            this.lineShape1.Y2 = 69;
            // 
            // DelCompuesto
            // 
            this.DelCompuesto.HeaderText = "";
            this.DelCompuesto.Name = "DelCompuesto";
            this.DelCompuesto.ReadOnly = true;
            // 
            // UpdCompuesto
            // 
            this.UpdCompuesto.HeaderText = "";
            this.UpdCompuesto.Name = "UpdCompuesto";
            this.UpdCompuesto.ReadOnly = true;
            // 
            // frmFormMagistral
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(650, 397);
            this.Controls.Add(this.dgvFormula);
            this.Controls.Add(this.txtPrd_Id);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtCantElemBase);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnGrabar);
            this.Controls.Add(this.btnAgregar);
            this.Controls.Add(this.lblUnidad);
            this.Controls.Add(this.txtPrd_Descripcion);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cmbElementoBase);
            this.Controls.Add(this.txtCantComp);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmbCompuestos);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.shapeContainer1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmFormMagistral";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Composición de Fórmulas Magistrales";
            this.Load += new System.EventHandler(this.frmFormMagistral_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFormula)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbCompuestos;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCantComp;
        private System.Windows.Forms.ComboBox cmbElementoBase;
        private System.Windows.Forms.TextBox txtPrd_Descripcion;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblUnidad;
        private System.Windows.Forms.Button btnAgregar;
        private System.Windows.Forms.Button btnGrabar;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtCantElemBase;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.TextBox txtPrd_Id;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dgvFormula;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private System.Windows.Forms.DataGridViewButtonColumn DelCompuesto;
        private System.Windows.Forms.DataGridViewButtonColumn UpdCompuesto;
    }
}