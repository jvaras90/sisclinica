﻿namespace pjDermaBelle.Formularios.Asistencial
{
    partial class frmProcedimientos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblServicio = new System.Windows.Forms.Label();
            this.lblProfesion = new System.Windows.Forms.Label();
            this.lblProcedencia = new System.Windows.Forms.Label();
            this.lblGradoInstruccion = new System.Windows.Forms.Label();
            this.lblLugarNacimiento = new System.Windows.Forms.Label();
            this.lblDocIdentidad = new System.Windows.Forms.Label();
            this.lblEdad = new System.Windows.Forms.Label();
            this.lblSexo = new System.Windows.Forms.Label();
            this.lblGrupoSanguineo = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lblHora = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblFecha = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblPaciente = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblNumHistoria = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.gpbInforme = new System.Windows.Forms.GroupBox();
            this.txtNumSesion = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtHoraAtencion = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.dtpFechaAtencion = new System.Windows.Forms.DateTimePicker();
            this.label17 = new System.Windows.Forms.Label();
            this.btnGrabar = new System.Windows.Forms.Button();
            this.btnDescartar = new System.Windows.Forms.Button();
            this.txtInfSesion = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtHoraCita = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpFechaCita = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.dgvProcedimientos = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.gpbInforme.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProcedimientos)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblServicio);
            this.groupBox1.Controls.Add(this.lblProfesion);
            this.groupBox1.Controls.Add(this.lblProcedencia);
            this.groupBox1.Controls.Add(this.lblGradoInstruccion);
            this.groupBox1.Controls.Add(this.lblLugarNacimiento);
            this.groupBox1.Controls.Add(this.lblDocIdentidad);
            this.groupBox1.Controls.Add(this.lblEdad);
            this.groupBox1.Controls.Add(this.lblSexo);
            this.groupBox1.Controls.Add(this.lblGrupoSanguineo);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.lblHora);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.lblFecha);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.lblPaciente);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.lblNumHistoria);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.shapeContainer1);
            this.groupBox1.Location = new System.Drawing.Point(3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1356, 169);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos del Paciente";
            // 
            // lblServicio
            // 
            this.lblServicio.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblServicio.ForeColor = System.Drawing.Color.Crimson;
            this.lblServicio.Location = new System.Drawing.Point(232, 11);
            this.lblServicio.Name = "lblServicio";
            this.lblServicio.Size = new System.Drawing.Size(892, 23);
            this.lblServicio.TabIndex = 43;
            this.lblServicio.Text = "label6";
            this.lblServicio.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblProfesion
            // 
            this.lblProfesion.AutoSize = true;
            this.lblProfesion.ForeColor = System.Drawing.Color.Blue;
            this.lblProfesion.Location = new System.Drawing.Point(238, 145);
            this.lblProfesion.Name = "lblProfesion";
            this.lblProfesion.Size = new System.Drawing.Size(115, 13);
            this.lblProfesion.TabIndex = 40;
            this.lblProfesion.Text = "Profesión u Ocupación";
            // 
            // lblProcedencia
            // 
            this.lblProcedencia.AutoSize = true;
            this.lblProcedencia.ForeColor = System.Drawing.Color.Blue;
            this.lblProcedencia.Location = new System.Drawing.Point(1100, 145);
            this.lblProcedencia.Name = "lblProcedencia";
            this.lblProcedencia.Size = new System.Drawing.Size(67, 13);
            this.lblProcedencia.TabIndex = 40;
            this.lblProcedencia.Text = "Procedencia";
            // 
            // lblGradoInstruccion
            // 
            this.lblGradoInstruccion.AutoSize = true;
            this.lblGradoInstruccion.ForeColor = System.Drawing.Color.Blue;
            this.lblGradoInstruccion.Location = new System.Drawing.Point(238, 127);
            this.lblGradoInstruccion.Name = "lblGradoInstruccion";
            this.lblGradoInstruccion.Size = new System.Drawing.Size(106, 13);
            this.lblGradoInstruccion.TabIndex = 40;
            this.lblGradoInstruccion.Text = "Grado de Instrucción";
            // 
            // lblLugarNacimiento
            // 
            this.lblLugarNacimiento.AutoSize = true;
            this.lblLugarNacimiento.ForeColor = System.Drawing.Color.Blue;
            this.lblLugarNacimiento.Location = new System.Drawing.Point(1100, 127);
            this.lblLugarNacimiento.Name = "lblLugarNacimiento";
            this.lblLugarNacimiento.Size = new System.Drawing.Size(105, 13);
            this.lblLugarNacimiento.TabIndex = 40;
            this.lblLugarNacimiento.Text = "Lugar de Nacimiento";
            // 
            // lblDocIdentidad
            // 
            this.lblDocIdentidad.AutoSize = true;
            this.lblDocIdentidad.ForeColor = System.Drawing.Color.Blue;
            this.lblDocIdentidad.Location = new System.Drawing.Point(238, 108);
            this.lblDocIdentidad.Name = "lblDocIdentidad";
            this.lblDocIdentidad.Size = new System.Drawing.Size(124, 13);
            this.lblDocIdentidad.TabIndex = 40;
            this.lblDocIdentidad.Text = "Documento de Identidad";
            // 
            // lblEdad
            // 
            this.lblEdad.AutoSize = true;
            this.lblEdad.ForeColor = System.Drawing.Color.Blue;
            this.lblEdad.Location = new System.Drawing.Point(1100, 108);
            this.lblEdad.Name = "lblEdad";
            this.lblEdad.Size = new System.Drawing.Size(32, 13);
            this.lblEdad.TabIndex = 40;
            this.lblEdad.Text = "Edad";
            // 
            // lblSexo
            // 
            this.lblSexo.AutoSize = true;
            this.lblSexo.ForeColor = System.Drawing.Color.Blue;
            this.lblSexo.Location = new System.Drawing.Point(1100, 90);
            this.lblSexo.Name = "lblSexo";
            this.lblSexo.Size = new System.Drawing.Size(31, 13);
            this.lblSexo.TabIndex = 40;
            this.lblSexo.Text = "Sexo";
            // 
            // lblGrupoSanguineo
            // 
            this.lblGrupoSanguineo.AutoSize = true;
            this.lblGrupoSanguineo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGrupoSanguineo.ForeColor = System.Drawing.Color.Red;
            this.lblGrupoSanguineo.Location = new System.Drawing.Point(238, 90);
            this.lblGrupoSanguineo.Name = "lblGrupoSanguineo";
            this.lblGrupoSanguineo.Size = new System.Drawing.Size(107, 13);
            this.lblGrupoSanguineo.TabIndex = 40;
            this.lblGrupoSanguineo.Text = "Grupo Sanguíneo";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(1062, 109);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(32, 13);
            this.label9.TabIndex = 33;
            this.label9.Text = "Edad";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(108, 109);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(124, 13);
            this.label12.TabIndex = 26;
            this.label12.Text = "Documento de Identidad";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(140, 91);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(92, 13);
            this.label16.TabIndex = 39;
            this.label16.Text = "Grupo Sanguíneo";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(1027, 145);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(67, 13);
            this.label15.TabIndex = 37;
            this.label15.Text = "Procedencia";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(126, 127);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(106, 13);
            this.label10.TabIndex = 28;
            this.label10.Text = "Grado de Instrucción";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(117, 145);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(115, 13);
            this.label11.TabIndex = 30;
            this.label11.Text = "Profesión u Ocupación";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(989, 127);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(105, 13);
            this.label14.TabIndex = 33;
            this.label14.Text = "Lugar de Nacimiento";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(1063, 91);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(31, 13);
            this.label13.TabIndex = 24;
            this.label13.Text = "Sexo";
            // 
            // lblHora
            // 
            this.lblHora.AutoSize = true;
            this.lblHora.Location = new System.Drawing.Point(1272, 32);
            this.lblHora.Name = "lblHora";
            this.lblHora.Size = new System.Drawing.Size(51, 13);
            this.lblHora.TabIndex = 8;
            this.lblHora.Text = "hh:mm:ss";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(1238, 32);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(30, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Hora";
            // 
            // lblFecha
            // 
            this.lblFecha.AutoSize = true;
            this.lblFecha.Location = new System.Drawing.Point(1272, 16);
            this.lblFecha.Name = "lblFecha";
            this.lblFecha.Size = new System.Drawing.Size(69, 13);
            this.lblFecha.TabIndex = 6;
            this.lblFecha.Text = "dd/mm/aaaa";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(1231, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Fecha";
            // 
            // lblPaciente
            // 
            this.lblPaciente.AutoSize = true;
            this.lblPaciente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPaciente.ForeColor = System.Drawing.Color.Blue;
            this.lblPaciente.Location = new System.Drawing.Point(239, 61);
            this.lblPaciente.Name = "lblPaciente";
            this.lblPaciente.Size = new System.Drawing.Size(448, 16);
            this.lblPaciente.TabIndex = 4;
            this.lblPaciente.Text = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(171, 61);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 16);
            this.label4.TabIndex = 3;
            this.label4.Text = "Paciente";
            // 
            // lblNumHistoria
            // 
            this.lblNumHistoria.AutoSize = true;
            this.lblNumHistoria.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumHistoria.ForeColor = System.Drawing.Color.Blue;
            this.lblNumHistoria.Location = new System.Drawing.Point(239, 45);
            this.lblNumHistoria.Name = "lblNumHistoria";
            this.lblNumHistoria.Size = new System.Drawing.Size(78, 16);
            this.lblNumHistoria.TabIndex = 2;
            this.lblNumHistoria.Text = "9999999999";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(151, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "H. Clinica N°";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(10, 39);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(89, 122);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(3, 16);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(1350, 150);
            this.shapeContainer1.TabIndex = 42;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape1
            // 
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 106;
            this.lineShape1.X2 = 1340;
            this.lineShape1.Y1 = 68;
            this.lineShape1.Y2 = 68;
            // 
            // gpbInforme
            // 
            this.gpbInforme.Controls.Add(this.txtNumSesion);
            this.gpbInforme.Controls.Add(this.label18);
            this.gpbInforme.Controls.Add(this.txtHoraAtencion);
            this.gpbInforme.Controls.Add(this.label7);
            this.gpbInforme.Controls.Add(this.dtpFechaAtencion);
            this.gpbInforme.Controls.Add(this.label17);
            this.gpbInforme.Controls.Add(this.btnGrabar);
            this.gpbInforme.Controls.Add(this.btnDescartar);
            this.gpbInforme.Controls.Add(this.txtInfSesion);
            this.gpbInforme.Controls.Add(this.label6);
            this.gpbInforme.Controls.Add(this.txtHoraCita);
            this.gpbInforme.Controls.Add(this.label3);
            this.gpbInforme.Controls.Add(this.dtpFechaCita);
            this.gpbInforme.Controls.Add(this.label2);
            this.gpbInforme.Location = new System.Drawing.Point(6, 190);
            this.gpbInforme.Name = "gpbInforme";
            this.gpbInforme.Size = new System.Drawing.Size(882, 153);
            this.gpbInforme.TabIndex = 2;
            this.gpbInforme.TabStop = false;
            this.gpbInforme.Text = "Informe de la Atención";
            // 
            // txtNumSesion
            // 
            this.txtNumSesion.Enabled = false;
            this.txtNumSesion.Location = new System.Drawing.Point(330, 22);
            this.txtNumSesion.Name = "txtNumSesion";
            this.txtNumSesion.Size = new System.Drawing.Size(48, 20);
            this.txtNumSesion.TabIndex = 13;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(230, 26);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(94, 13);
            this.label18.TabIndex = 12;
            this.label18.Text = "Número de Sesión";
            // 
            // txtHoraAtencion
            // 
            this.txtHoraAtencion.Enabled = false;
            this.txtHoraAtencion.Location = new System.Drawing.Point(114, 130);
            this.txtHoraAtencion.Name = "txtHoraAtencion";
            this.txtHoraAtencion.Size = new System.Drawing.Size(87, 20);
            this.txtHoraAtencion.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(17, 134);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(90, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Hora de Atención";
            // 
            // dtpFechaAtencion
            // 
            this.dtpFechaAtencion.Enabled = false;
            this.dtpFechaAtencion.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFechaAtencion.Location = new System.Drawing.Point(114, 104);
            this.dtpFechaAtencion.Name = "dtpFechaAtencion";
            this.dtpFechaAtencion.Size = new System.Drawing.Size(87, 20);
            this.dtpFechaAtencion.TabIndex = 9;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(10, 108);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(97, 13);
            this.label17.TabIndex = 8;
            this.label17.Text = "Fecha de Atención";
            // 
            // btnGrabar
            // 
            this.btnGrabar.Location = new System.Drawing.Point(689, 105);
            this.btnGrabar.Name = "btnGrabar";
            this.btnGrabar.Size = new System.Drawing.Size(186, 42);
            this.btnGrabar.TabIndex = 7;
            this.btnGrabar.Text = "Grabar Informe";
            this.btnGrabar.UseVisualStyleBackColor = true;
            this.btnGrabar.Click += new System.EventHandler(this.btnGrabar_Click);
            // 
            // btnDescartar
            // 
            this.btnDescartar.Location = new System.Drawing.Point(689, 48);
            this.btnDescartar.Name = "btnDescartar";
            this.btnDescartar.Size = new System.Drawing.Size(186, 42);
            this.btnDescartar.TabIndex = 6;
            this.btnDescartar.Text = "Descartar Informe";
            this.btnDescartar.UseVisualStyleBackColor = true;
            this.btnDescartar.Click += new System.EventHandler(this.btnDescartar_Click);
            // 
            // txtInfSesion
            // 
            this.txtInfSesion.Location = new System.Drawing.Point(330, 48);
            this.txtInfSesion.Multiline = true;
            this.txtInfSesion.Name = "txtInfSesion";
            this.txtInfSesion.Size = new System.Drawing.Size(354, 99);
            this.txtInfSesion.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(282, 52);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Informe";
            // 
            // txtHoraCita
            // 
            this.txtHoraCita.Enabled = false;
            this.txtHoraCita.Location = new System.Drawing.Point(114, 48);
            this.txtHoraCita.Name = "txtHoraCita";
            this.txtHoraCita.Size = new System.Drawing.Size(87, 20);
            this.txtHoraCita.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(30, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Hora de la Cita";
            // 
            // dtpFechaCita
            // 
            this.dtpFechaCita.Enabled = false;
            this.dtpFechaCita.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFechaCita.Location = new System.Drawing.Point(114, 22);
            this.dtpFechaCita.Name = "dtpFechaCita";
            this.dtpFechaCita.Size = new System.Drawing.Size(87, 20);
            this.dtpFechaCita.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Fecha de la Cita";
            // 
            // dgvProcedimientos
            // 
            this.dgvProcedimientos.AllowUserToAddRows = false;
            this.dgvProcedimientos.AllowUserToDeleteRows = false;
            this.dgvProcedimientos.AllowUserToOrderColumns = true;
            this.dgvProcedimientos.AllowUserToResizeColumns = false;
            this.dgvProcedimientos.AllowUserToResizeRows = false;
            this.dgvProcedimientos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProcedimientos.Location = new System.Drawing.Point(6, 349);
            this.dgvProcedimientos.Name = "dgvProcedimientos";
            this.dgvProcedimientos.Size = new System.Drawing.Size(1353, 329);
            this.dgvProcedimientos.TabIndex = 4;
            this.dgvProcedimientos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvProcedimientos_CellClick);
            // 
            // frmProcedimientos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1362, 690);
            this.Controls.Add(this.dgvProcedimientos);
            this.Controls.Add(this.gpbInforme);
            this.Controls.Add(this.groupBox1);
            this.Name = "frmProcedimientos";
            this.Text = "Procedimientos y/o Tratamientos";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmProcedimientos_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.gpbInforme.ResumeLayout(false);
            this.gpbInforme.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProcedimientos)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.Label lblServicio;
        private System.Windows.Forms.Label lblProfesion;
        private System.Windows.Forms.Label lblProcedencia;
        private System.Windows.Forms.Label lblGradoInstruccion;
        private System.Windows.Forms.Label lblLugarNacimiento;
        private System.Windows.Forms.Label lblDocIdentidad;
        private System.Windows.Forms.Label lblEdad;
        private System.Windows.Forms.Label lblSexo;
        private System.Windows.Forms.Label lblGrupoSanguineo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lblHora;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblFecha;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblPaciente;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblNumHistoria;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private System.Windows.Forms.GroupBox gpbInforme;
        private System.Windows.Forms.TextBox txtHoraCita;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dtpFechaCita;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnDescartar;
        private System.Windows.Forms.TextBox txtInfSesion;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView dgvProcedimientos;
        private System.Windows.Forms.Button btnGrabar;
        private System.Windows.Forms.TextBox txtNumSesion;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtHoraAtencion;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker dtpFechaAtencion;
        private System.Windows.Forms.Label label17;


    }
}