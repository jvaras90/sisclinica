﻿namespace pjDermaBelle.Formularios.Asistencial
{
    partial class frmHistorial
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvHistorial = new System.Windows.Forms.DataGridView();
            this.btnVerFicha = new System.Windows.Forms.DataGridViewButtonColumn();
            this.lstFichaMedica = new System.Windows.Forms.ListBox();
            this.lblPaciente = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblNumHistoria = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblDocIdentidad = new System.Windows.Forms.Label();
            this.lblGrupoSanguineo = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lblProcedencia = new System.Windows.Forms.Label();
            this.lblSexo = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHistorial)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvHistorial
            // 
            this.dgvHistorial.AllowUserToAddRows = false;
            this.dgvHistorial.AllowUserToDeleteRows = false;
            this.dgvHistorial.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvHistorial.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.btnVerFicha});
            this.dgvHistorial.Location = new System.Drawing.Point(3, 145);
            this.dgvHistorial.Name = "dgvHistorial";
            this.dgvHistorial.ReadOnly = true;
            this.dgvHistorial.Size = new System.Drawing.Size(754, 547);
            this.dgvHistorial.TabIndex = 0;
            this.dgvHistorial.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvHistorial_CellClick);
            this.dgvHistorial.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgvHistorial_CellPainting);
            // 
            // btnVerFicha
            // 
            this.btnVerFicha.HeaderText = "";
            this.btnVerFicha.Name = "btnVerFicha";
            this.btnVerFicha.ReadOnly = true;
            // 
            // lstFichaMedica
            // 
            this.lstFichaMedica.BackColor = System.Drawing.Color.LemonChiffon;
            this.lstFichaMedica.FormattingEnabled = true;
            this.lstFichaMedica.Location = new System.Drawing.Point(763, 12);
            this.lstFichaMedica.Name = "lstFichaMedica";
            this.lstFichaMedica.Size = new System.Drawing.Size(588, 680);
            this.lstFichaMedica.TabIndex = 1;
            // 
            // lblPaciente
            // 
            this.lblPaciente.AutoSize = true;
            this.lblPaciente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPaciente.ForeColor = System.Drawing.Color.Blue;
            this.lblPaciente.Location = new System.Drawing.Point(100, 28);
            this.lblPaciente.Name = "lblPaciente";
            this.lblPaciente.Size = new System.Drawing.Size(448, 16);
            this.lblPaciente.TabIndex = 8;
            this.lblPaciente.Text = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(32, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 16);
            this.label4.TabIndex = 7;
            this.label4.Text = "Paciente";
            // 
            // lblNumHistoria
            // 
            this.lblNumHistoria.AutoSize = true;
            this.lblNumHistoria.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumHistoria.ForeColor = System.Drawing.Color.Blue;
            this.lblNumHistoria.Location = new System.Drawing.Point(100, 12);
            this.lblNumHistoria.Name = "lblNumHistoria";
            this.lblNumHistoria.Size = new System.Drawing.Size(78, 16);
            this.lblNumHistoria.TabIndex = 6;
            this.lblNumHistoria.Text = "9999999999";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 16);
            this.label1.TabIndex = 5;
            this.label1.Text = "H. Clinica N°";
            // 
            // lblDocIdentidad
            // 
            this.lblDocIdentidad.AutoSize = true;
            this.lblDocIdentidad.ForeColor = System.Drawing.Color.Blue;
            this.lblDocIdentidad.Location = new System.Drawing.Point(142, 85);
            this.lblDocIdentidad.Name = "lblDocIdentidad";
            this.lblDocIdentidad.Size = new System.Drawing.Size(124, 13);
            this.lblDocIdentidad.TabIndex = 47;
            this.lblDocIdentidad.Text = "Documento de Identidad";
            // 
            // lblGrupoSanguineo
            // 
            this.lblGrupoSanguineo.AutoSize = true;
            this.lblGrupoSanguineo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGrupoSanguineo.ForeColor = System.Drawing.Color.Red;
            this.lblGrupoSanguineo.Location = new System.Drawing.Point(142, 67);
            this.lblGrupoSanguineo.Name = "lblGrupoSanguineo";
            this.lblGrupoSanguineo.Size = new System.Drawing.Size(107, 13);
            this.lblGrupoSanguineo.TabIndex = 48;
            this.lblGrupoSanguineo.Text = "Grupo Sanguíneo";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(12, 86);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(124, 13);
            this.label12.TabIndex = 41;
            this.label12.Text = "Documento de Identidad";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(44, 68);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(92, 13);
            this.label16.TabIndex = 44;
            this.label16.Text = "Grupo Sanguíneo";
            // 
            // lblProcedencia
            // 
            this.lblProcedencia.AutoSize = true;
            this.lblProcedencia.ForeColor = System.Drawing.Color.Blue;
            this.lblProcedencia.Location = new System.Drawing.Point(142, 122);
            this.lblProcedencia.Name = "lblProcedencia";
            this.lblProcedencia.Size = new System.Drawing.Size(67, 13);
            this.lblProcedencia.TabIndex = 53;
            this.lblProcedencia.Text = "Procedencia";
            // 
            // lblSexo
            // 
            this.lblSexo.AutoSize = true;
            this.lblSexo.ForeColor = System.Drawing.Color.Blue;
            this.lblSexo.Location = new System.Drawing.Point(142, 104);
            this.lblSexo.Name = "lblSexo";
            this.lblSexo.Size = new System.Drawing.Size(31, 13);
            this.lblSexo.TabIndex = 56;
            this.lblSexo.Text = "Sexo";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(69, 122);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(67, 13);
            this.label15.TabIndex = 52;
            this.label15.Text = "Procedencia";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(105, 104);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(31, 13);
            this.label13.TabIndex = 49;
            this.label13.Text = "Sexo";
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(1354, 695);
            this.shapeContainer1.TabIndex = 57;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape1
            // 
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 5;
            this.lineShape1.X2 = 757;
            this.lineShape1.Y1 = 54;
            this.lineShape1.Y2 = 54;
            // 
            // frmHistorial
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1354, 695);
            this.Controls.Add(this.lblProcedencia);
            this.Controls.Add(this.lblSexo);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.lblDocIdentidad);
            this.Controls.Add(this.lblGrupoSanguineo);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.lblPaciente);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblNumHistoria);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lstFichaMedica);
            this.Controls.Add(this.dgvHistorial);
            this.Controls.Add(this.shapeContainer1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmHistorial";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Historial de Atenciones";
            this.Load += new System.EventHandler(this.frmHistorial_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvHistorial)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvHistorial;
        private System.Windows.Forms.ListBox lstFichaMedica;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label13;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        public System.Windows.Forms.Label lblPaciente;
        public System.Windows.Forms.Label lblNumHistoria;
        public System.Windows.Forms.Label lblDocIdentidad;
        public System.Windows.Forms.Label lblGrupoSanguineo;
        public System.Windows.Forms.Label lblProcedencia;
        public System.Windows.Forms.Label lblSexo;
        private System.Windows.Forms.DataGridViewButtonColumn btnVerFicha;
    }
}