﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pjDermaBelle.Formularios.Asistencial
{
    public partial class frmRayosX : Form
    {
        public frmRayosX()
        {
            InitializeComponent();
        }

        ClaseDatos obj = new ClaseDatos();
        DermaBelleDataDataContext linq = new DermaBelleDataDataContext();
        FM_ExamenesAuxiliares oFM_ExamenesAuxiliares = new FM_ExamenesAuxiliares();

        public int FichaNum;

        //**********************Area de Carga De los CheckListBox***************************************************

        void CargarTorax()
        {
            List<usp_dgvExamenesLisResult> Torax = new List<usp_dgvExamenesLisResult>();
            Torax = obj.dgvExamenesLis(FichaNum, 2, 15);
            if (Torax.Count() > 0)
            {
                foreach (usp_dgvExamenesLisResult reg in Torax)
                {
                    chklTorax.Items.Add(reg.Examen, reg.Seleccionado);
                }
            }
        }

        void CargarAbdomen()
        {
            List<usp_dgvExamenesLisResult> Abdomen = new List<usp_dgvExamenesLisResult>();
            Abdomen = obj.dgvExamenesLis(FichaNum, 2, 16);
            if (Abdomen.Count() > 0)
            {
                foreach (usp_dgvExamenesLisResult reg in Abdomen)
                {
                    chklAbdomen.Items.Add(reg.Examen, reg.Seleccionado);
                }
            }
        }

        void CargarGastro()
        {
            List<usp_dgvExamenesLisResult> Gastro = new List<usp_dgvExamenesLisResult>();
            Gastro = obj.dgvExamenesLis(FichaNum, 2, 17);
            if (Gastro.Count() > 0)
            {
                foreach (usp_dgvExamenesLisResult reg in Gastro)
                {
                    chklGastro.Items.Add(reg.Examen, reg.Seleccionado);
                }
            }
        }

        void CargarApUrinario()
        {
            List<usp_dgvExamenesLisResult> ApUrinario = new List<usp_dgvExamenesLisResult>();
            ApUrinario = obj.dgvExamenesLis(FichaNum, 2, 18);
            if (ApUrinario.Count() > 0)
            {
                foreach (usp_dgvExamenesLisResult reg in ApUrinario)
                {
                    chklApUrinario.Items.Add(reg.Examen, reg.Seleccionado);
                }
            }
        }

        void CargarOtros1()
        {
            List<usp_dgvExamenesLisResult> Otros1 = new List<usp_dgvExamenesLisResult>();
            Otros1 = obj.dgvExamenesLis(FichaNum, 2, 31);
            if (Otros1.Count() > 0)
            {
                foreach (usp_dgvExamenesLisResult reg in Otros1)
                {
                    txtIndicaciones1.Text = reg.Observacion;
                }
            }
        }

        void CargarCabeza()
        {
            List<usp_dgvExamenesLisResult> Cabeza = new List<usp_dgvExamenesLisResult>();
            Cabeza = obj.dgvExamenesLis(FichaNum, 2, 19);
            if (Cabeza.Count() > 0)
            {
                foreach (usp_dgvExamenesLisResult reg in Cabeza)
                {
                    chklCabeza.Items.Add(reg.Examen, reg.Seleccionado);
                }
            }
        }

        void CargarColumna()
        {
            List<usp_dgvExamenesLisResult> Columna = new List<usp_dgvExamenesLisResult>();
            Columna = obj.dgvExamenesLis(FichaNum, 2, 20);
            if (Columna.Count() > 0)
            {
                foreach (usp_dgvExamenesLisResult reg in Columna)
                {
                    chklColumna.Items.Add(reg.Examen, reg.Seleccionado);
                }
            }
        }

        void CargarSuperiores()
        {
            List<usp_dgvExamenesLisResult> Superiores = new List<usp_dgvExamenesLisResult>();
            Superiores = obj.dgvExamenesLis(FichaNum, 2, 21);
            if (Superiores.Count() > 0)
            {
                foreach (usp_dgvExamenesLisResult reg in Superiores)
                {
                    chklSuperiores.Items.Add(reg.Examen, reg.Seleccionado);
                }
            }
        }

        void CargarInferiores()
        {
            List<usp_dgvExamenesLisResult> Inferiores = new List<usp_dgvExamenesLisResult>();
            Inferiores = obj.dgvExamenesLis(FichaNum, 2, 22);
            if (Inferiores.Count() > 0)
            {
                foreach (usp_dgvExamenesLisResult reg in Inferiores)
                {
                    chklInferiores.Items.Add(reg.Examen, reg.Seleccionado);
                }
            }
        }

        void CargarOtros2()
        {
            List<usp_dgvExamenesLisResult> Otros2 = new List<usp_dgvExamenesLisResult>();
            Otros2 = obj.dgvExamenesLis(FichaNum, 2, 34);
            if (Otros2.Count() > 0)
            {
                foreach (usp_dgvExamenesLisResult reg in Otros2)
                {
                    txtIndicaciones2.Text = reg.Observacion;
                }
            }
        }
        
        //**********************Area de Almacenamiento en Tablas***************************************************
        void GuardarTorax()
        {
            oFM_ExamenesAuxiliares.FichaMedicaId = Convert.ToSByte(FichaNum);
            oFM_ExamenesAuxiliares.ExamenTipo = 2;
            oFM_ExamenesAuxiliares.ExamenGrupo = 15;
            obj.FM_ExamAuxDel(oFM_ExamenesAuxiliares);
            foreach (object ind in chklTorax.CheckedItems)
            {
                string titulo = ind.ToString();
                List<usp_ExamenIdRecResult> listado = new List<usp_ExamenIdRecResult>();
                listado = obj.usp_ExamenIdRec(titulo);
                if (listado.Count() > 0)
                {
                    foreach (usp_ExamenIdRecResult reg in listado)
                    {
                        oFM_ExamenesAuxiliares.ExamenId = reg.ExamenId;
                        oFM_ExamenesAuxiliares.ExamenSelec = true;
                        obj.FM_ExamAuxAdd(oFM_ExamenesAuxiliares);
                    }
                }
            }
        }

        void GuardarAbdomen()
        {
            oFM_ExamenesAuxiliares.FichaMedicaId = Convert.ToSByte(FichaNum);
            oFM_ExamenesAuxiliares.ExamenTipo = 2;
            oFM_ExamenesAuxiliares.ExamenGrupo = 16;
            obj.FM_ExamAuxDel(oFM_ExamenesAuxiliares);
            foreach (object ind in chklAbdomen.CheckedItems)
            {
                string titulo = ind.ToString();
                List<usp_ExamenIdRecResult> listado = new List<usp_ExamenIdRecResult>();
                listado = obj.usp_ExamenIdRec(titulo);
                if (listado.Count() > 0)
                {
                    foreach (usp_ExamenIdRecResult reg in listado)
                    {
                        oFM_ExamenesAuxiliares.ExamenId = reg.ExamenId;
                        oFM_ExamenesAuxiliares.ExamenSelec = true;
                        obj.FM_ExamAuxAdd(oFM_ExamenesAuxiliares);
                    }
                }
            }
        }

        void GuardarGastro()
        {
            oFM_ExamenesAuxiliares.FichaMedicaId = Convert.ToSByte(FichaNum);
            oFM_ExamenesAuxiliares.ExamenTipo = 2;
            oFM_ExamenesAuxiliares.ExamenGrupo = 17;
            obj.FM_ExamAuxDel(oFM_ExamenesAuxiliares);
            foreach (object ind in chklGastro.CheckedItems)
            {
                string titulo = ind.ToString();
                List<usp_ExamenIdRecResult> listado = new List<usp_ExamenIdRecResult>();
                listado = obj.usp_ExamenIdRec(titulo);
                if (listado.Count() > 0)
                {
                    foreach (usp_ExamenIdRecResult reg in listado)
                    {
                        oFM_ExamenesAuxiliares.ExamenId = reg.ExamenId;
                        oFM_ExamenesAuxiliares.ExamenSelec = true;
                        obj.FM_ExamAuxAdd(oFM_ExamenesAuxiliares);
                    }
                }
            }
        }

        void GuardarApUrinario()
        {
            oFM_ExamenesAuxiliares.FichaMedicaId = Convert.ToSByte(FichaNum);
            oFM_ExamenesAuxiliares.ExamenTipo = 2;
            oFM_ExamenesAuxiliares.ExamenGrupo = 18;
            obj.FM_ExamAuxDel(oFM_ExamenesAuxiliares);
            foreach (object ind in chklApUrinario.CheckedItems)
            {
                string titulo = ind.ToString();
                List<usp_ExamenIdRecResult> listado = new List<usp_ExamenIdRecResult>();
                listado = obj.usp_ExamenIdRec(titulo);
                if (listado.Count() > 0)
                {
                    foreach (usp_ExamenIdRecResult reg in listado)
                    {
                        oFM_ExamenesAuxiliares.ExamenId = reg.ExamenId;
                        oFM_ExamenesAuxiliares.ExamenSelec = true;
                        obj.FM_ExamAuxAdd(oFM_ExamenesAuxiliares);
                    }
                }
            }
        }

        void GuardarOtros1()
        {   
            oFM_ExamenesAuxiliares.FichaMedicaId = Convert.ToSByte(FichaNum);
            oFM_ExamenesAuxiliares.ExamenTipo = 1;
            oFM_ExamenesAuxiliares.ExamenGrupo = 31;
            obj.FM_ExamAuxDel(oFM_ExamenesAuxiliares);
            string titulo = "OTRAS IMAGENES 1";
            List<usp_ExamenIdRecResult> listado = new List<usp_ExamenIdRecResult>();
            listado = obj.usp_ExamenIdRec(titulo);
            if (listado.Count() > 0)
            {
                foreach (usp_ExamenIdRecResult reg in listado)
                {
                    oFM_ExamenesAuxiliares.ExamenId = reg.ExamenId;
                    oFM_ExamenesAuxiliares.ExamenObserv = txtIndicaciones1.Text.ToUpper();
                    oFM_ExamenesAuxiliares.ExamenSelec = true;
                    obj.FM_ExamAuxAdd(oFM_ExamenesAuxiliares);
                }
            }
        }

        void GuardarCabeza()
        {
            oFM_ExamenesAuxiliares.FichaMedicaId = Convert.ToSByte(FichaNum);
            oFM_ExamenesAuxiliares.ExamenTipo = 2;
            oFM_ExamenesAuxiliares.ExamenGrupo = 19;
            obj.FM_ExamAuxDel(oFM_ExamenesAuxiliares);
            foreach (object ind in chklCabeza.CheckedItems)
            {
                string titulo = ind.ToString();
                List<usp_ExamenIdRecResult> listado = new List<usp_ExamenIdRecResult>();
                listado = obj.usp_ExamenIdRec(titulo);
                if (listado.Count() > 0)
                {
                    foreach (usp_ExamenIdRecResult reg in listado)
                    {
                        oFM_ExamenesAuxiliares.ExamenId = reg.ExamenId;
                        oFM_ExamenesAuxiliares.ExamenSelec = true;
                        obj.FM_ExamAuxAdd(oFM_ExamenesAuxiliares);
                    }
                }
            }
        }

        void GuardarColumna()
        {
            oFM_ExamenesAuxiliares.FichaMedicaId = Convert.ToSByte(FichaNum);
            oFM_ExamenesAuxiliares.ExamenTipo = 2;
            oFM_ExamenesAuxiliares.ExamenGrupo = 20;
            obj.FM_ExamAuxDel(oFM_ExamenesAuxiliares);
            foreach (object ind in chklColumna.CheckedItems)
            {
                string titulo = ind.ToString();
                List<usp_ExamenIdRecResult> listado = new List<usp_ExamenIdRecResult>();
                listado = obj.usp_ExamenIdRec(titulo);
                if (listado.Count() > 0)
                {
                    foreach (usp_ExamenIdRecResult reg in listado)
                    {
                        oFM_ExamenesAuxiliares.ExamenId = reg.ExamenId;
                        oFM_ExamenesAuxiliares.ExamenSelec = true;
                        obj.FM_ExamAuxAdd(oFM_ExamenesAuxiliares);
                    }
                }
            }
        }

        void GuardarSuperiores()
        {
            oFM_ExamenesAuxiliares.FichaMedicaId = Convert.ToSByte(FichaNum);
            oFM_ExamenesAuxiliares.ExamenTipo = 2;
            oFM_ExamenesAuxiliares.ExamenGrupo = 21;
            obj.FM_ExamAuxDel(oFM_ExamenesAuxiliares);
            foreach (object ind in chklSuperiores.CheckedItems)
            {
                string titulo = ind.ToString();
                List<usp_ExamenIdRecResult> listado = new List<usp_ExamenIdRecResult>();
                listado = obj.usp_ExamenIdRec(titulo);
                if (listado.Count() > 0)
                {
                    foreach (usp_ExamenIdRecResult reg in listado)
                    {
                        oFM_ExamenesAuxiliares.ExamenId = reg.ExamenId;
                        oFM_ExamenesAuxiliares.ExamenSelec = true;
                        obj.FM_ExamAuxAdd(oFM_ExamenesAuxiliares);
                    }
                }
            }
        }

        void GuardarInferiores()
        {
            oFM_ExamenesAuxiliares.FichaMedicaId = Convert.ToSByte(FichaNum);
            oFM_ExamenesAuxiliares.ExamenTipo = 2;
            oFM_ExamenesAuxiliares.ExamenGrupo = 22;
            obj.FM_ExamAuxDel(oFM_ExamenesAuxiliares);
            foreach (object ind in chklInferiores.CheckedItems)
            {
                string titulo = ind.ToString();
                List<usp_ExamenIdRecResult> listado = new List<usp_ExamenIdRecResult>();
                listado = obj.usp_ExamenIdRec(titulo);
                if (listado.Count() > 0)
                {
                    foreach (usp_ExamenIdRecResult reg in listado)
                    {
                        oFM_ExamenesAuxiliares.ExamenId = reg.ExamenId;
                        oFM_ExamenesAuxiliares.ExamenSelec = true;
                        obj.FM_ExamAuxAdd(oFM_ExamenesAuxiliares);
                    }
                }
            }
        }

        void GuardarOtros2()
        {
            oFM_ExamenesAuxiliares.FichaMedicaId = Convert.ToSByte(FichaNum);
            oFM_ExamenesAuxiliares.ExamenTipo = 1;
            oFM_ExamenesAuxiliares.ExamenGrupo = 34;
            obj.FM_ExamAuxDel(oFM_ExamenesAuxiliares);
            string titulo = "OTRAS IMAGENES 2";
            List<usp_ExamenIdRecResult> listado = new List<usp_ExamenIdRecResult>();
            listado = obj.usp_ExamenIdRec(titulo);
            if (listado.Count() > 0)
            {
                foreach (usp_ExamenIdRecResult reg in listado)
                {
                    oFM_ExamenesAuxiliares.ExamenId = reg.ExamenId;
                    oFM_ExamenesAuxiliares.ExamenObserv = txtIndicaciones2.Text.ToUpper();
                    oFM_ExamenesAuxiliares.ExamenSelec = true;
                    obj.FM_ExamAuxAdd(oFM_ExamenesAuxiliares);
                }
            }
        }

        private void frmRayosX_Load(object sender, EventArgs e)
        {
            CargarTorax();
            CargarAbdomen();
            CargarGastro();
            CargarApUrinario();
            CargarOtros1();
            CargarCabeza();
            CargarColumna();
            CargarSuperiores();
            CargarInferiores();
            CargarOtros2();
        }

        private void frmRayosX_FormClosed(object sender, FormClosedEventArgs e)
        {
            GuardarTorax();
            GuardarAbdomen();
            GuardarGastro();
            GuardarApUrinario();
            GuardarOtros1();
            GuardarCabeza();
            GuardarColumna();
            GuardarSuperiores();
            GuardarInferiores();
            GuardarOtros2();
        }
    }
}
