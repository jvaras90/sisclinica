﻿namespace pjDermaBelle.Formularios.Asistencial
{
    partial class frmRayosX
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.txtIndicaciones1 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.chklApUrinario = new System.Windows.Forms.CheckedListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.chklAbdomen = new System.Windows.Forms.CheckedListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.chklGastro = new System.Windows.Forms.CheckedListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.chklTorax = new System.Windows.Forms.CheckedListBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.txtIndicaciones2 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.chklInferiores = new System.Windows.Forms.CheckedListBox();
            this.label8 = new System.Windows.Forms.Label();
            this.chklColumna = new System.Windows.Forms.CheckedListBox();
            this.label11 = new System.Windows.Forms.Label();
            this.chklSuperiores = new System.Windows.Forms.CheckedListBox();
            this.label12 = new System.Windows.Forms.Label();
            this.chklCabeza = new System.Windows.Forms.CheckedListBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(3, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1345, 680);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.txtIndicaciones1);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.chklApUrinario);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.chklAbdomen);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.chklGastro);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.chklTorax);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1337, 654);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Partes Blandas";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // txtIndicaciones1
            // 
            this.txtIndicaciones1.Location = new System.Drawing.Point(893, 26);
            this.txtIndicaciones1.Multiline = true;
            this.txtIndicaciones1.Name = "txtIndicaciones1";
            this.txtIndicaciones1.Size = new System.Drawing.Size(441, 620);
            this.txtIndicaciones1.TabIndex = 36;
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.Blue;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(893, 3);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(441, 20);
            this.label10.TabIndex = 33;
            this.label10.Text = "INDICACIONES Y/U OTROS";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Blue;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(448, 334);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(441, 20);
            this.label2.TabIndex = 31;
            this.label2.Text = "APARATO URINARIO";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chklApUrinario
            // 
            this.chklApUrinario.FormattingEnabled = true;
            this.chklApUrinario.Location = new System.Drawing.Point(448, 357);
            this.chklApUrinario.Name = "chklApUrinario";
            this.chklApUrinario.Size = new System.Drawing.Size(441, 289);
            this.chklApUrinario.TabIndex = 30;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Blue;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(3, 333);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(441, 20);
            this.label3.TabIndex = 29;
            this.label3.Text = "ABDOMEN";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chklAbdomen
            // 
            this.chklAbdomen.FormattingEnabled = true;
            this.chklAbdomen.Location = new System.Drawing.Point(3, 357);
            this.chklAbdomen.Name = "chklAbdomen";
            this.chklAbdomen.Size = new System.Drawing.Size(441, 289);
            this.chklAbdomen.TabIndex = 28;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Blue;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(448, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(441, 20);
            this.label1.TabIndex = 27;
            this.label1.Text = "TRACTO GASTRO INTESTINAL";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chklGastro
            // 
            this.chklGastro.FormattingEnabled = true;
            this.chklGastro.Location = new System.Drawing.Point(448, 26);
            this.chklGastro.Name = "chklGastro";
            this.chklGastro.Size = new System.Drawing.Size(441, 304);
            this.chklGastro.TabIndex = 26;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Blue;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(3, 3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(441, 20);
            this.label4.TabIndex = 25;
            this.label4.Text = "TORAX";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chklTorax
            // 
            this.chklTorax.FormattingEnabled = true;
            this.chklTorax.Location = new System.Drawing.Point(3, 26);
            this.chklTorax.Name = "chklTorax";
            this.chklTorax.Size = new System.Drawing.Size(441, 304);
            this.chklTorax.TabIndex = 24;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.txtIndicaciones2);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.chklInferiores);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.chklColumna);
            this.tabPage2.Controls.Add(this.label11);
            this.tabPage2.Controls.Add(this.chklSuperiores);
            this.tabPage2.Controls.Add(this.label12);
            this.tabPage2.Controls.Add(this.chklCabeza);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1337, 654);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Estructura Ósea";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // txtIndicaciones2
            // 
            this.txtIndicaciones2.Location = new System.Drawing.Point(893, 29);
            this.txtIndicaciones2.Multiline = true;
            this.txtIndicaciones2.Name = "txtIndicaciones2";
            this.txtIndicaciones2.Size = new System.Drawing.Size(441, 620);
            this.txtIndicaciones2.TabIndex = 48;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Blue;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(893, 6);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(441, 20);
            this.label6.TabIndex = 46;
            this.label6.Text = "INDICACIONES Y/U OTROS";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Blue;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(448, 337);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(441, 20);
            this.label7.TabIndex = 44;
            this.label7.Text = "MIEMBROS INFERIORES";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chklInferiores
            // 
            this.chklInferiores.FormattingEnabled = true;
            this.chklInferiores.Location = new System.Drawing.Point(448, 360);
            this.chklInferiores.Name = "chklInferiores";
            this.chklInferiores.Size = new System.Drawing.Size(441, 289);
            this.chklInferiores.TabIndex = 43;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.Blue;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(3, 336);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(441, 20);
            this.label8.TabIndex = 42;
            this.label8.Text = "COLUMNA Y PÉLVIS ÓSEA";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chklColumna
            // 
            this.chklColumna.FormattingEnabled = true;
            this.chklColumna.Location = new System.Drawing.Point(3, 360);
            this.chklColumna.Name = "chklColumna";
            this.chklColumna.Size = new System.Drawing.Size(441, 289);
            this.chklColumna.TabIndex = 41;
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.Blue;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(448, 6);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(441, 20);
            this.label11.TabIndex = 40;
            this.label11.Text = "MIEMBROS SUPERIORES";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chklSuperiores
            // 
            this.chklSuperiores.FormattingEnabled = true;
            this.chklSuperiores.Location = new System.Drawing.Point(448, 29);
            this.chklSuperiores.Name = "chklSuperiores";
            this.chklSuperiores.Size = new System.Drawing.Size(441, 304);
            this.chklSuperiores.TabIndex = 39;
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.Color.Blue;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(3, 6);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(441, 20);
            this.label12.TabIndex = 38;
            this.label12.Text = "CABEZA, CUELLO Y TORAX";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chklCabeza
            // 
            this.chklCabeza.FormattingEnabled = true;
            this.chklCabeza.Location = new System.Drawing.Point(3, 29);
            this.chklCabeza.Name = "chklCabeza";
            this.chklCabeza.Size = new System.Drawing.Size(441, 304);
            this.chklCabeza.TabIndex = 37;
            // 
            // frmRayosX
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1348, 684);
            this.Controls.Add(this.tabControl1);
            this.Name = "frmRayosX";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Solicitud de Examenes - Rayos X";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmRayosX_FormClosed);
            this.Load += new System.EventHandler(this.frmRayosX_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckedListBox chklGastro;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckedListBox chklTorax;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckedListBox chklApUrinario;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckedListBox chklAbdomen;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtIndicaciones1;
        private System.Windows.Forms.TextBox txtIndicaciones2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckedListBox chklInferiores;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckedListBox chklColumna;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckedListBox chklSuperiores;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckedListBox chklCabeza;
    }
}