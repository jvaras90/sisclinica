﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pjDermaBelle.Formularios.Asistencial
{
    public partial class frmHistorial : Form
    {
        public frmHistorial()
        {
            InitializeComponent();
        }

        ClaseDatos obj = new ClaseDatos();
        DermaBelleDataDataContext linq = new DermaBelleDataDataContext();

        public int FichaMedicaId;

        private void frmHistorial_Load(object sender, EventArgs e)
        {
            dgvHistorial.DataSource = obj.Hist_Listado(lblNumHistoria.Text);
            for (int indice = 0; indice < dgvHistorial.Rows.Count; indice++)
            {
                dgvHistorial.Columns[1].Visible = false;      //NumFicha
                dgvHistorial.Columns[2].Visible = false;       //Ocultar
                dgvHistorial.Columns[3].Visible = false;      //Ocultar
                dgvHistorial.Columns[4].Width = 80;        //Fecha
                dgvHistorial.Columns[5].Width = 80;       //Tipo de Atención
                dgvHistorial.Columns[6].Width = 150;      //Servicio
                dgvHistorial.Columns[7].Width = 200;      //Médico
                dgvHistorial.Columns[8].Width = 60;       //CIE10
                dgvHistorial.Columns[9].Width = 300;      //Diagnostico
            }

        }

        private void dgvHistorial_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.ColumnIndex >= 0 && this.dgvHistorial.Columns[e.ColumnIndex].Name == "btnVerFicha" && e.RowIndex >= 0)
            {
                e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                DataGridViewButtonCell celBoton = this.dgvHistorial.Rows[e.RowIndex].Cells["btnVerFicha"] as DataGridViewButtonCell;
                Icon icoEliminar = new Icon(@"C:\Windows\Dermabelle\Ico\Open_Folder.ico");
                e.Graphics.DrawIcon(icoEliminar, e.CellBounds.Left + 3, e.CellBounds.Top + 3);

                this.dgvHistorial.Rows[e.RowIndex].Height = icoEliminar.Height + 10;
                this.dgvHistorial.Columns[e.ColumnIndex].Width = icoEliminar.Width + 10;

                e.Handled = true;
            }
        }

        private void dgvHistorial_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == -1)
            { return; }

            if (this.dgvHistorial.Columns[e.ColumnIndex].Name == "btnVerFicha")
            {
                int ind = dgvHistorial.CurrentCell.RowIndex;
                FichaMedicaId = int.Parse(dgvHistorial.Rows[ind].Cells[1].Value.ToString());
                MotivoConsulta();
                ExamenFisico();
                AyudaDiagnostica();
                PlanTrabajo();
                //Diagnosticos();
                //Prescripción();
            }   
        }

        void MotivoConsulta()
        {
            lstFichaMedica.Items.Clear();
            // Leer tabla y Mostrar datos
            List<Hist_FM_MotivConsultaResult> lista = new List<Hist_FM_MotivConsultaResult>();
            lista = obj.Hist_FMMotivoConsulta(FichaMedicaId);

            if (lista.Count() > 0)
            {
                foreach (Hist_FM_MotivConsultaResult reg in lista)
                {
                    lstFichaMedica.Font = new Font(lstFichaMedica.Font.Name, 10.0F, lstFichaMedica.Font.Style, lstFichaMedica.Font.Unit);
                    //lstFichaMedica.Items.Add("..................1..................2..................3..................4..................5..................6..................7..................8..................9..................0");
                    //                                 1         2         3         4         5         6         7         8         9         0         1         1         2         3         4         5         6         7         8
                    //lstFichaMedica.Items.Add("1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890");
                    lstFichaMedica.Items.Add("                                            FICHA MEDICA                                            ");
                    lstFichaMedica.Items.Add(reg.Ser_Nombre + " - " + reg.Pac_TipoAtencion);
                    //lstFichaMedica.Font = new Font(lstFichaMedica.Font.Name, 10.0F, lstFichaMedica.Font.Style, lstFichaMedica.Font.Unit);
                    lstFichaMedica.Items.Add("");
                    lstFichaMedica.Items.Add("");
                    lstFichaMedica.Items.Add("Ficha Medica       :    " + reg.FichaMedicaId);
                    lstFichaMedica.Items.Add("Historia Clínica    :    " + reg.Pac_HC);
                    lstFichaMedica.Items.Add("Paciente                :    " + reg.Pac_NomComp);
                    lstFichaMedica.Items.Add("Edad                       :    " + reg.Pac_Edad + " años");
                    lstFichaMedica.Items.Add("Fecha                     :    " + reg.FM_Fecha);
                    lstFichaMedica.Items.Add("Hora                        :    " + reg.FM_HoraFin);
                    lstFichaMedica.Items.Add("____________________________________________________________________________________________________");
                    lstFichaMedica.Items.Add("");
                    lstFichaMedica.Items.Add("I. MOTIVO DE LA CONSULTA");
                    lstFichaMedica.Items.Add("1.1. Anamnesis");
                    lstFichaMedica.Items.Add("        Motivo de la Consulta o Evolución del Paciente");
                    lstFichaMedica.Items.Add("          " + reg.FM_MotivoConsulta);
                    lstFichaMedica.Items.Add("");
                    lstFichaMedica.Items.Add("        Síntomas y Signos Referidos");
                    lstFichaMedica.Items.Add("          Síntoma                                      Observación");
                    lstFichaMedica.Items.Add("          --------------------------------------------------------------------");
                    List<Hist_FMSintomasResult> sintomas = new List<Hist_FMSintomasResult>();
                    sintomas = obj.Hist_FMSintomas(FichaMedicaId);
                    if (sintomas.Count() > 0)
                    {
                        foreach (Hist_FMSintomasResult regsintomas in sintomas)
                        {
                            lstFichaMedica.Items.Add("          " + regsintomas.SintomaNombre +
                                                     "          " + regsintomas.SintomaObserv);
                        }
                    }

                    lstFichaMedica.Items.Add("");
                    lstFichaMedica.Items.Add("        Hábitos Nocivos");
                    lstFichaMedica.Items.Add("        [ " + reg.FM_Alcohol + " ] Alcohol          " +
                                             "[ " + reg.FM_Tabaco + " ] Tabaco          " +
                                             "[ " + reg.FM_Drogas + " ] Drogas          " +
                                             "[ " + reg.FM_OtrosHabNoc + " ] Otros");
                    lstFichaMedica.Items.Add("        Especificar: " + reg.FM_ObservHabNoc);
                    lstFichaMedica.Items.Add("");
                    lstFichaMedica.Items.Add("        Otros Datos de Interés");
                    lstFichaMedica.Items.Add("         Fecha Ultima Menstruación: " + reg.FM_FechaUltMenst);
                    lstFichaMedica.Items.Add("");
                    lstFichaMedica.Items.Add("1.2. Antecedentes");
                    lstFichaMedica.Items.Add("        Antecedentes Patológicos");
                    lstFichaMedica.Items.Add("          Patología                                                                    Observación");
                    lstFichaMedica.Items.Add("          -----------------------------------------------------------------------------------------------------------------------------");
                    List<Hist_FMAntecPatResult> patologias = new List<Hist_FMAntecPatResult>();
                    patologias = obj.Hist_FMAntecPat(FichaMedicaId);
                    if (patologias.Count() > 0)
                    {
                        foreach (Hist_FMAntecPatResult regpatologias in patologias)
                        {
                            lstFichaMedica.Items.Add("          " + regpatologias.PatologiaNombre +
                                                     "          " + regpatologias.PatologiaObserv);
                        }
                    }
                    lstFichaMedica.Items.Add("");
                    lstFichaMedica.Items.Add("        Antecedentes Quirúrgicos");
                    lstFichaMedica.Items.Add("          Cirugía                                                                         Observación");
                    lstFichaMedica.Items.Add("          -----------------------------------------------------------------------------------------------------------------------------");
                    List<Hist_FMAntecQuiResult> cirugias = new List<Hist_FMAntecQuiResult>();
                    cirugias = obj.Hist_FMAntecQui(FichaMedicaId);
                    if (cirugias.Count() > 0)
                    {
                        foreach (Hist_FMAntecQuiResult regcirugias in cirugias)
                        {
                            lstFichaMedica.Items.Add("          " + regcirugias.Qui_Nombre +
                                                     "          " + regcirugias.CirugiaObserv);
                        }
                    }
                    lstFichaMedica.Items.Add("");
                    lstFichaMedica.Items.Add("        Antecedentes Alérgicos");
                    lstFichaMedica.Items.Add("          Alergia                                                                         Observación");
                    lstFichaMedica.Items.Add("          -----------------------------------------------------------------------------------------------------------------------------");
                    List<Hist_FMAntecAleResult> alergias = new List<Hist_FMAntecAleResult>();
                    alergias = obj.Hist_FMAntecAle(FichaMedicaId);
                    if (alergias.Count() > 0)
                    {
                        foreach (Hist_FMAntecAleResult regalergias in alergias)
                        {
                            lstFichaMedica.Items.Add("          " + regalergias.AlergiaNombre +
                                                     "          " + regalergias.AlergiaObserv);
                        }
                    }
                    lstFichaMedica.Items.Add("");
                    lstFichaMedica.Items.Add("        Antecedentes Familiares");
                    lstFichaMedica.Items.Add("          Familiar            Patología                                               Observación");
                    lstFichaMedica.Items.Add("          -----------------------------------------------------------------------------------------------------------------------------");
                    List<Hist_FMAntecFamResult> familiares = new List<Hist_FMAntecFamResult>();
                    familiares = obj.Hist_FMAntecFam(FichaMedicaId);
                    if (familiares.Count() > 0)
                    {
                        foreach (Hist_FMAntecFamResult regfamiliares in familiares)
                        {
                            lstFichaMedica.Items.Add("          " + regfamiliares.FamiliarNombre +
                                                     "          " + regfamiliares.PatologiaNombre+
                                                     "          " + regfamiliares.AntecFamObserv);
                        }
                    }
                    lstFichaMedica.Items.Add("");
                }
            }    
        }

        void ExamenFisico()
        {
            // Leer tabla y Mostrar datos
            List<Hist_FuncVitalesResult> lista = new List<Hist_FuncVitalesResult>();
            lista = obj.Hist_FuncVitales(FichaMedicaId);

            if (lista.Count() > 0)
            {
                foreach (Hist_FuncVitalesResult reg in lista)
                {
                    lstFichaMedica.Items.Add("II. EXAMEN FISICO");
                    lstFichaMedica.Items.Add("2.1. Funciones Vitales");
                    lstFichaMedica.Items.Add("        Funciones Vitales");
                    lstFichaMedica.Items.Add("         P.A.: " + reg.FM_Presion + "  mmHg" + "                           " +
                                             "Pulso: " + reg.FM_Pulso + " x'                      " +
                                             "T°: " + reg.FM_Temp + "C°");
                    lstFichaMedica.Items.Add("         Peso: " + reg.FM_Peso + " kg                   " +
                                              "Talla: " + reg.FM_Talla + " mts");
                    lstFichaMedica.Items.Add("");
                    lstFichaMedica.Items.Add("        LOTEP");
                    lstFichaMedica.Items.Add("          [ " + reg.FM_Lucido + " ] Lúcido   " +
                                             "[ " + reg.FM_OTiempo + " ] Orientado Tiempo  " +
                                             "[ " + reg.FM_OEspacio + " ] Orientado Espacio    " +
                                             "[ " + reg.FM_OPersona + " ] Orientado Persona");
                    lstFichaMedica.Items.Add("");
                    lstFichaMedica.Items.Add("        Estados del Paciente");
                    lstFichaMedica.Items.Add("          Estado General: " + reg.FM_EstGeneral +
                                             "          Estado Nutricional: " + reg.FM_EstNutricion +
                                             "          Estado Hidratación: " + reg.FM_EstHidratacion);
                    lstFichaMedica.Items.Add("");
                }
            }
            lstFichaMedica.Items.Add("2.2 Examen Preferencial");
            List<Hist_PartesCuerpoResult> partes = new List<Hist_PartesCuerpoResult>();
            partes = obj.Hist_PartesCuerpo(FichaMedicaId);

            if (partes.Count() > 0)
            {
                foreach (Hist_PartesCuerpoResult reg in partes)
                {
                    lstFichaMedica.Items.Add("         " + reg.PartesCuerpoDescripcion +
                                             "  [" + reg.ParteCuerpoObserv + "]");
                    List<Hist_SegmentosResult> segmentos = new List<Hist_SegmentosResult>();
                    segmentos = obj.Hist_Segmentos(FichaMedicaId, reg.ParteCuerpoId);
                    if (segmentos.Count() > 0)
                    {
                        foreach (Hist_SegmentosResult reg1 in segmentos)
                        {
                            lstFichaMedica.Items.Add("            " + reg1.SegmentoDescripcion +
                                                     "    [" + reg1.SegmentoObserv + "]");
                        }
                    }
                    lstFichaMedica.Items.Add("         _________________________________________________");
                }
            }
            lstFichaMedica.Items.Add("");
        }

        void AyudaDiagnostica()
        {
            lstFichaMedica.Items.Add("III. INFORMES DE AYUDA DIAGNÓSTCA");
            List<Hist_InfAydDiagnostResult> ayudadx = new List<Hist_InfAydDiagnostResult>();
            ayudadx = obj.Hist_InfAydDiagnost(FichaMedicaId);

            if (ayudadx.Count() > 0)
            {
                foreach (Hist_InfAydDiagnostResult reg in ayudadx)
                {
                    lstFichaMedica.Items.Add("        " + reg.AyudaDiagFecha + "    " + reg.TAyudaDiagNombre);
                    lstFichaMedica.Items.Add("         [" + reg.AyudaDiagInforme + "]");
                }
            }
            lstFichaMedica.Items.Add("");
        }

        void PlanTrabajo()
        {
            lstFichaMedica.Items.Add("IV. PLAN DETRABAJO");
            lstFichaMedica.Items.Add("4.1. Ayuda Diagnóstica");

            lstFichaMedica.Items.Add("   4.1.1. Laboratorio");
            List<Hist_ExamAuxResult> laboratorio = new List<Hist_ExamAuxResult>();
            laboratorio = obj.Hist_ExamAux(FichaMedicaId, 1);
            if (laboratorio.Count() > 0)
            {
                foreach (Hist_ExamAuxResult reg in laboratorio)
                {
                    lstFichaMedica.Items.Add("      " + reg.ExamenNombre);
                }
                lstFichaMedica.Items.Add("      " + "-----------------------------------------------------------------------");
            }
            lstFichaMedica.Items.Add("");

            lstFichaMedica.Items.Add("   4.1.2. Imágenes");
            List<Hist_ExamAuxResult> imagenes = new List<Hist_ExamAuxResult>();
            imagenes = obj.Hist_ExamAux(FichaMedicaId, 2);
            if (imagenes.Count() > 0)
            {
                foreach (Hist_ExamAuxResult reg in imagenes)
                {
                    lstFichaMedica.Items.Add("      " + reg.ExamenNombre + "      OBSERVACIONES: " + reg.ExamenObserv);
                }
                lstFichaMedica.Items.Add("      " + "-----------------------------------------------------------------------");
            }
            lstFichaMedica.Items.Add("");

            lstFichaMedica.Items.Add("   4.1.3. Patología");
            List<Hist_ExamAuxResult> patologia = new List<Hist_ExamAuxResult>();
            patologia = obj.Hist_ExamAux(FichaMedicaId, 3);
            if (patologia.Count() > 0)
            {
                foreach (Hist_ExamAuxResult reg in patologia)
                {
                    lstFichaMedica.Items.Add("      " + reg.ExamenNombre + "      OBSERVACIONES: " + reg.ExamenObserv);
                }
                lstFichaMedica.Items.Add("      " + "-----------------------------------------------------------------------");
            }
            lstFichaMedica.Items.Add("");

            lstFichaMedica.Items.Add("   4.1.4. Ecografías");
            List<Hist_ExamAuxResult> ecografia = new List<Hist_ExamAuxResult>();
            ecografia = obj.Hist_ExamAux(FichaMedicaId, 4);
            if (ecografia.Count() > 0)
            {
                foreach (Hist_ExamAuxResult reg in ecografia)
                {
                    lstFichaMedica.Items.Add("      " + reg.ExamenNombre + "      OBSERVACIONES: " + reg.ExamenObserv);
                }
                lstFichaMedica.Items.Add("      " + "-----------------------------------------------------------------------");
            }
            lstFichaMedica.Items.Add("");

        }
    }
}
