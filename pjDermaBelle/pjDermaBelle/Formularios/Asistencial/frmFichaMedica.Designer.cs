﻿namespace pjDermaBelle
{
    partial class frmFichaMedica
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cboTipoAtencion = new System.Windows.Forms.ComboBox();
            this.lblServicio = new System.Windows.Forms.Label();
            this.lblProfesion = new System.Windows.Forms.Label();
            this.lblProcedencia = new System.Windows.Forms.Label();
            this.lblGradoInstruccion = new System.Windows.Forms.Label();
            this.lblLugarNacimiento = new System.Windows.Forms.Label();
            this.lblDocIdentidad = new System.Windows.Forms.Label();
            this.lblEdad = new System.Windows.Forms.Label();
            this.lblSexo = new System.Windows.Forms.Label();
            this.lblGrupoSanguineo = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lblHora = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblFecha = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblPaciente = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblNumHistoria = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtAlergias = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tbpMotivoConsulta = new System.Windows.Forms.TabPage();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tbpAnamnesis = new System.Windows.Forms.TabPage();
            this.groupBox40 = new System.Windows.Forms.GroupBox();
            this.txtFUltMenstruacion = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this.chkOtros = new System.Windows.Forms.CheckBox();
            this.chkDrogas = new System.Windows.Forms.CheckBox();
            this.chkTabaco = new System.Windows.Forms.CheckBox();
            this.chkAlcohol = new System.Windows.Forms.CheckBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtObservHabNoc = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.dgvSintomas = new System.Windows.Forms.DataGridView();
            this.Seleccion = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtMotivo = new System.Windows.Forms.TextBox();
            this.btnAntecPatologicos = new System.Windows.Forms.TabPage();
            this.btnAntecFamiliares = new System.Windows.Forms.Button();
            this.btnAntecAlergias = new System.Windows.Forms.Button();
            this.btnAntecQuirurg = new System.Windows.Forms.Button();
            this.btnAntecPatol = new System.Windows.Forms.Button();
            this.gpbAntecedentes = new System.Windows.Forms.GroupBox();
            this.cmbFamiliar = new System.Windows.Forms.ComboBox();
            this.lblFamiliar = new System.Windows.Forms.Label();
            this.dgvAntecedentes = new System.Windows.Forms.DataGridView();
            this.btnDelAntecedentes = new System.Windows.Forms.DataGridViewButtonColumn();
            this.btnUpdAntecedentes = new System.Windows.Forms.DataGridViewButtonColumn();
            this.btnAddAntecedentes = new System.Windows.Forms.Button();
            this.txtAntecedenteObserv = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.cboAntecedente = new System.Windows.Forms.ComboBox();
            this.lblAntecedente = new System.Windows.Forms.Label();
            this.tbpDiagnosticosAnteriores = new System.Windows.Forms.TabPage();
            this.dgvAntecDiagnosticos = new System.Windows.Forms.DataGridView();
            this.tpgExamFisico = new System.Windows.Forms.TabPage();
            this.tabControl4 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.cboEstHidratacion = new System.Windows.Forms.ComboBox();
            this.cboEstNutricional = new System.Windows.Forms.ComboBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.cboEstGeneral = new System.Windows.Forms.ComboBox();
            this.label36 = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.chkOPersona = new System.Windows.Forms.CheckBox();
            this.chkOEspacio = new System.Windows.Forms.CheckBox();
            this.chkOTiempo = new System.Windows.Forms.CheckBox();
            this.chkLucido = new System.Windows.Forms.CheckBox();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.txtTalla = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.txtPeso = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.txtTemperatura = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.txtPulso = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.txtPAMinima = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txtPAMaxima = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.pbxParteCuerpo = new System.Windows.Forms.PictureBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.dgvPartesCuerpo = new System.Windows.Forms.DataGridView();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.btnGuardaSegmentos = new System.Windows.Forms.Button();
            this.dgvSegmentos = new System.Windows.Forms.DataGridView();
            this.tbpAyudaDx = new System.Windows.Forms.TabPage();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.dgvAyudaDiagnostica = new System.Windows.Forms.DataGridView();
            this.btnDelAyudaDx = new System.Windows.Forms.DataGridViewButtonColumn();
            this.btnUpdAyudaDx = new System.Windows.Forms.DataGridViewButtonColumn();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.btnAyudaDiagNuevo = new System.Windows.Forms.Button();
            this.txtAyudaDiagId = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnGuardarAyudaDx = new System.Windows.Forms.Button();
            this.txtInfExamen = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.cmbTipoExamen = new System.Windows.Forms.ComboBox();
            this.label44 = new System.Windows.Forms.Label();
            this.dtpFechaExamen = new System.Windows.Forms.DateTimePicker();
            this.label43 = new System.Windows.Forms.Label();
            this.tbpPlanTrabajo = new System.Windows.Forms.TabPage();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.dgvAsistencia = new System.Windows.Forms.DataGridView();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.txtTratamObserv = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.dgvTratamientos = new System.Windows.Forms.DataGridView();
            this.btnTratamientosDel = new System.Windows.Forms.DataGridViewButtonColumn();
            this.btnTratamientosUpd = new System.Windows.Forms.DataGridViewButtonColumn();
            this.btnGrabarTx = new System.Windows.Forms.Button();
            this.txtTratamNumSesiones = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.cmbTratamientos = new System.Windows.Forms.ComboBox();
            this.label46 = new System.Windows.Forms.Label();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.btnEco = new System.Windows.Forms.Button();
            this.btnPat = new System.Windows.Forms.Button();
            this.btnImg = new System.Windows.Forms.Button();
            this.btnLab = new System.Windows.Forms.Button();
            this.tbpDiagnostico = new System.Windows.Forms.TabPage();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this.txtDxNombre = new System.Windows.Forms.TextBox();
            this.lstDiagnosticos = new System.Windows.Forms.ListBox();
            this.chkDxPrincipal = new System.Windows.Forms.CheckBox();
            this.groupBox27 = new System.Windows.Forms.GroupBox();
            this.rbtPresuntivo = new System.Windows.Forms.RadioButton();
            this.rbtDefinitivo = new System.Windows.Forms.RadioButton();
            this.rbtCronico = new System.Windows.Forms.RadioButton();
            this.dgvDiagnosticos = new System.Windows.Forms.DataGridView();
            this.btnDelDiagnostico = new System.Windows.Forms.DataGridViewButtonColumn();
            this.btnUpdDiagnostico = new System.Windows.Forms.DataGridViewButtonColumn();
            this.btnGrabar = new System.Windows.Forms.Button();
            this.txtDiagObserv = new System.Windows.Forms.TextBox();
            this.txtCIECod = new System.Windows.Forms.TextBox();
            this.label56 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.tbpPrescripcion = new System.Windows.Forms.TabPage();
            this.groupBox24 = new System.Windows.Forms.GroupBox();
            this.btnAceptar = new System.Windows.Forms.Button();
            this.gpbDatos = new System.Windows.Forms.GroupBox();
            this.btnAgregarComponente = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.rbtElementoBase = new System.Windows.Forms.RadioButton();
            this.rbtPrincActivo = new System.Windows.Forms.RadioButton();
            this.lblUndElemBase = new System.Windows.Forms.Label();
            this.txtCantElemBase = new System.Windows.Forms.TextBox();
            this.lblCantidad = new System.Windows.Forms.Label();
            this.cmbComponente = new System.Windows.Forms.ComboBox();
            this.btnAddCompuesto = new System.Windows.Forms.Button();
            this.rbtPreparados = new System.Windows.Forms.RadioButton();
            this.dgvPreparados = new System.Windows.Forms.DataGridView();
            this.btnDelCompuesto = new System.Windows.Forms.DataGridViewButtonColumn();
            this.btnUpdCompuesto = new System.Windows.Forms.DataGridViewButtonColumn();
            this.rbtLaboratorio = new System.Windows.Forms.RadioButton();
            this.rbtGenerico = new System.Windows.Forms.RadioButton();
            this.lstMedicinasFiltro = new System.Windows.Forms.ListBox();
            this.cmbCriterio = new System.Windows.Forms.ComboBox();
            this.lblCriterio = new System.Windows.Forms.Label();
            this.groupBox23 = new System.Windows.Forms.GroupBox();
            this.label22 = new System.Windows.Forms.Label();
            this.lblRpCaduca = new System.Windows.Forms.Label();
            this.cmbVigencia = new System.Windows.Forms.ComboBox();
            this.label24 = new System.Windows.Forms.Label();
            this.lstMedicinas = new System.Windows.Forms.ListBox();
            this.txtPrdId = new System.Windows.Forms.TextBox();
            this.txtPrdDescripcion = new System.Windows.Forms.TextBox();
            this.btnPresc = new System.Windows.Forms.Button();
            this.btnRepiteRx = new System.Windows.Forms.Button();
            this.dgvPrescripcion = new System.Windows.Forms.DataGridView();
            this.btnDelMedicinas = new System.Windows.Forms.DataGridViewButtonColumn();
            this.btnUpdMedicinas = new System.Windows.Forms.DataGridViewButtonColumn();
            this.btnAddPresc = new System.Windows.Forms.Button();
            this.txtIndicaciones = new System.Windows.Forms.TextBox();
            this.label61 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.tbpComplementarios = new System.Windows.Forms.TabPage();
            this.groupBox43 = new System.Windows.Forms.GroupBox();
            this.txtObservaciones = new System.Windows.Forms.TextBox();
            this.groupBox42 = new System.Windows.Forms.GroupBox();
            this.txtRecomendaciones = new System.Windows.Forms.TextBox();
            this.groupBox41 = new System.Windows.Forms.GroupBox();
            this.label17 = new System.Windows.Forms.Label();
            this.dtpProximaCita = new System.Windows.Forms.DateTimePicker();
            this.tbpConsultaAnterior = new System.Windows.Forms.TabPage();
            this.groupBox31 = new System.Windows.Forms.GroupBox();
            this.tabControl3 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.dgvPrescCA = new System.Windows.Forms.DataGridView();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.dgvDiagCA = new System.Windows.Forms.DataGridView();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.btnEcoCA = new System.Windows.Forms.Button();
            this.btnProcCA = new System.Windows.Forms.Button();
            this.dgvExamCA = new System.Windows.Forms.DataGridView();
            this.btnPatCA = new System.Windows.Forms.Button();
            this.btnImgCA = new System.Windows.Forms.Button();
            this.btnLabCA = new System.Windows.Forms.Button();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.groupBox38 = new System.Windows.Forms.GroupBox();
            this.dgvFamCA = new System.Windows.Forms.DataGridView();
            this.groupBox37 = new System.Windows.Forms.GroupBox();
            this.dgvQuirCA = new System.Windows.Forms.DataGridView();
            this.groupBox36 = new System.Windows.Forms.GroupBox();
            this.dgvPatCA = new System.Windows.Forms.DataGridView();
            this.groupBox30 = new System.Windows.Forms.GroupBox();
            this.groupBox35 = new System.Windows.Forms.GroupBox();
            this.groupBox39 = new System.Windows.Forms.GroupBox();
            this.chkOtrosHabCA = new System.Windows.Forms.CheckBox();
            this.chkDrogasCA = new System.Windows.Forms.CheckBox();
            this.chkTabacoCA = new System.Windows.Forms.CheckBox();
            this.chkAlcoholCA = new System.Windows.Forms.CheckBox();
            this.label66 = new System.Windows.Forms.Label();
            this.txtEspecHabCA = new System.Windows.Forms.TextBox();
            this.txtMotivoEvolCA = new System.Windows.Forms.TextBox();
            this.groupBox29 = new System.Windows.Forms.GroupBox();
            this.txtTipoAtencionCA = new System.Windows.Forms.TextBox();
            this.txtHoraCA = new System.Windows.Forms.TextBox();
            this.txtFechaCA = new System.Windows.Forms.TextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.btnCerrar = new System.Windows.Forms.Button();
            this.pbGrabar = new System.Windows.Forms.ProgressBar();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lblGuardando = new System.Windows.Forms.Label();
            this.btnHistorial = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tbpMotivoConsulta.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tbpAnamnesis.SuspendLayout();
            this.groupBox40.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox21.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSintomas)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.btnAntecPatologicos.SuspendLayout();
            this.gpbAntecedentes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAntecedentes)).BeginInit();
            this.tbpDiagnosticosAnteriores.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAntecDiagnosticos)).BeginInit();
            this.tpgExamFisico.SuspendLayout();
            this.tabControl4.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxParteCuerpo)).BeginInit();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPartesCuerpo)).BeginInit();
            this.groupBox13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSegmentos)).BeginInit();
            this.tbpAyudaDx.SuspendLayout();
            this.groupBox16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAyudaDiagnostica)).BeginInit();
            this.groupBox15.SuspendLayout();
            this.tbpPlanTrabajo.SuspendLayout();
            this.groupBox19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAsistencia)).BeginInit();
            this.groupBox18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTratamientos)).BeginInit();
            this.groupBox17.SuspendLayout();
            this.tbpDiagnostico.SuspendLayout();
            this.groupBox20.SuspendLayout();
            this.groupBox27.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDiagnosticos)).BeginInit();
            this.tbpPrescripcion.SuspendLayout();
            this.groupBox24.SuspendLayout();
            this.gpbDatos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPreparados)).BeginInit();
            this.groupBox23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPrescripcion)).BeginInit();
            this.tbpComplementarios.SuspendLayout();
            this.groupBox43.SuspendLayout();
            this.groupBox42.SuspendLayout();
            this.groupBox41.SuspendLayout();
            this.tbpConsultaAnterior.SuspendLayout();
            this.groupBox31.SuspendLayout();
            this.tabControl3.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPrescCA)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDiagCA)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvExamCA)).BeginInit();
            this.tabPage5.SuspendLayout();
            this.groupBox38.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFamCA)).BeginInit();
            this.groupBox37.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvQuirCA)).BeginInit();
            this.groupBox36.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPatCA)).BeginInit();
            this.groupBox30.SuspendLayout();
            this.groupBox35.SuspendLayout();
            this.groupBox39.SuspendLayout();
            this.groupBox29.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cboTipoAtencion);
            this.groupBox1.Controls.Add(this.lblServicio);
            this.groupBox1.Controls.Add(this.lblProfesion);
            this.groupBox1.Controls.Add(this.lblProcedencia);
            this.groupBox1.Controls.Add(this.lblGradoInstruccion);
            this.groupBox1.Controls.Add(this.lblLugarNacimiento);
            this.groupBox1.Controls.Add(this.lblDocIdentidad);
            this.groupBox1.Controls.Add(this.lblEdad);
            this.groupBox1.Controls.Add(this.lblSexo);
            this.groupBox1.Controls.Add(this.lblGrupoSanguineo);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.lblHora);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.lblFecha);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.lblPaciente);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.lblNumHistoria);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.shapeContainer1);
            this.groupBox1.Location = new System.Drawing.Point(8, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1356, 152);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos del Paciente";
            // 
            // cboTipoAtencion
            // 
            this.cboTipoAtencion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboTipoAtencion.ForeColor = System.Drawing.Color.MediumSeaGreen;
            this.cboTipoAtencion.FormattingEnabled = true;
            this.cboTipoAtencion.Items.AddRange(new object[] {
            "CONTINUADOR",
            "REINGRESO"});
            this.cboTipoAtencion.Location = new System.Drawing.Point(1020, 13);
            this.cboTipoAtencion.Name = "cboTipoAtencion";
            this.cboTipoAtencion.Size = new System.Drawing.Size(192, 28);
            this.cboTipoAtencion.TabIndex = 44;
            // 
            // lblServicio
            // 
            this.lblServicio.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblServicio.ForeColor = System.Drawing.Color.Crimson;
            this.lblServicio.Location = new System.Drawing.Point(476, 11);
            this.lblServicio.Name = "lblServicio";
            this.lblServicio.Size = new System.Drawing.Size(464, 23);
            this.lblServicio.TabIndex = 43;
            this.lblServicio.Text = "label6";
            this.lblServicio.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblProfesion
            // 
            this.lblProfesion.AutoSize = true;
            this.lblProfesion.ForeColor = System.Drawing.Color.Blue;
            this.lblProfesion.Location = new System.Drawing.Point(238, 129);
            this.lblProfesion.Name = "lblProfesion";
            this.lblProfesion.Size = new System.Drawing.Size(115, 13);
            this.lblProfesion.TabIndex = 40;
            this.lblProfesion.Text = "Profesión u Ocupación";
            // 
            // lblProcedencia
            // 
            this.lblProcedencia.AutoSize = true;
            this.lblProcedencia.ForeColor = System.Drawing.Color.Blue;
            this.lblProcedencia.Location = new System.Drawing.Point(1100, 129);
            this.lblProcedencia.Name = "lblProcedencia";
            this.lblProcedencia.Size = new System.Drawing.Size(67, 13);
            this.lblProcedencia.TabIndex = 40;
            this.lblProcedencia.Text = "Procedencia";
            // 
            // lblGradoInstruccion
            // 
            this.lblGradoInstruccion.AutoSize = true;
            this.lblGradoInstruccion.ForeColor = System.Drawing.Color.Blue;
            this.lblGradoInstruccion.Location = new System.Drawing.Point(238, 111);
            this.lblGradoInstruccion.Name = "lblGradoInstruccion";
            this.lblGradoInstruccion.Size = new System.Drawing.Size(106, 13);
            this.lblGradoInstruccion.TabIndex = 40;
            this.lblGradoInstruccion.Text = "Grado de Instrucción";
            // 
            // lblLugarNacimiento
            // 
            this.lblLugarNacimiento.AutoSize = true;
            this.lblLugarNacimiento.ForeColor = System.Drawing.Color.Blue;
            this.lblLugarNacimiento.Location = new System.Drawing.Point(1100, 111);
            this.lblLugarNacimiento.Name = "lblLugarNacimiento";
            this.lblLugarNacimiento.Size = new System.Drawing.Size(105, 13);
            this.lblLugarNacimiento.TabIndex = 40;
            this.lblLugarNacimiento.Text = "Lugar de Nacimiento";
            // 
            // lblDocIdentidad
            // 
            this.lblDocIdentidad.AutoSize = true;
            this.lblDocIdentidad.ForeColor = System.Drawing.Color.Blue;
            this.lblDocIdentidad.Location = new System.Drawing.Point(238, 92);
            this.lblDocIdentidad.Name = "lblDocIdentidad";
            this.lblDocIdentidad.Size = new System.Drawing.Size(124, 13);
            this.lblDocIdentidad.TabIndex = 40;
            this.lblDocIdentidad.Text = "Documento de Identidad";
            // 
            // lblEdad
            // 
            this.lblEdad.AutoSize = true;
            this.lblEdad.ForeColor = System.Drawing.Color.Blue;
            this.lblEdad.Location = new System.Drawing.Point(1100, 92);
            this.lblEdad.Name = "lblEdad";
            this.lblEdad.Size = new System.Drawing.Size(32, 13);
            this.lblEdad.TabIndex = 40;
            this.lblEdad.Text = "Edad";
            // 
            // lblSexo
            // 
            this.lblSexo.AutoSize = true;
            this.lblSexo.ForeColor = System.Drawing.Color.Blue;
            this.lblSexo.Location = new System.Drawing.Point(1100, 74);
            this.lblSexo.Name = "lblSexo";
            this.lblSexo.Size = new System.Drawing.Size(31, 13);
            this.lblSexo.TabIndex = 40;
            this.lblSexo.Text = "Sexo";
            // 
            // lblGrupoSanguineo
            // 
            this.lblGrupoSanguineo.AutoSize = true;
            this.lblGrupoSanguineo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGrupoSanguineo.ForeColor = System.Drawing.Color.Red;
            this.lblGrupoSanguineo.Location = new System.Drawing.Point(238, 74);
            this.lblGrupoSanguineo.Name = "lblGrupoSanguineo";
            this.lblGrupoSanguineo.Size = new System.Drawing.Size(107, 13);
            this.lblGrupoSanguineo.TabIndex = 40;
            this.lblGrupoSanguineo.Text = "Grupo Sanguíneo";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(1062, 93);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(32, 13);
            this.label9.TabIndex = 33;
            this.label9.Text = "Edad";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(108, 93);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(124, 13);
            this.label12.TabIndex = 26;
            this.label12.Text = "Documento de Identidad";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(140, 75);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(92, 13);
            this.label16.TabIndex = 39;
            this.label16.Text = "Grupo Sanguíneo";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(1027, 129);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(67, 13);
            this.label15.TabIndex = 37;
            this.label15.Text = "Procedencia";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(126, 111);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(106, 13);
            this.label10.TabIndex = 28;
            this.label10.Text = "Grado de Instrucción";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(117, 129);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(115, 13);
            this.label11.TabIndex = 30;
            this.label11.Text = "Profesión u Ocupación";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(989, 111);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(105, 13);
            this.label14.TabIndex = 33;
            this.label14.Text = "Lugar de Nacimiento";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(1063, 75);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(31, 13);
            this.label13.TabIndex = 24;
            this.label13.Text = "Sexo";
            // 
            // lblHora
            // 
            this.lblHora.AutoSize = true;
            this.lblHora.Location = new System.Drawing.Point(1272, 32);
            this.lblHora.Name = "lblHora";
            this.lblHora.Size = new System.Drawing.Size(51, 13);
            this.lblHora.TabIndex = 8;
            this.lblHora.Text = "hh:mm:ss";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(1238, 32);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(30, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Hora";
            // 
            // lblFecha
            // 
            this.lblFecha.AutoSize = true;
            this.lblFecha.Location = new System.Drawing.Point(1272, 16);
            this.lblFecha.Name = "lblFecha";
            this.lblFecha.Size = new System.Drawing.Size(69, 13);
            this.lblFecha.TabIndex = 6;
            this.lblFecha.Text = "dd/mm/aaaa";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(1231, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Fecha";
            // 
            // lblPaciente
            // 
            this.lblPaciente.AutoSize = true;
            this.lblPaciente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPaciente.ForeColor = System.Drawing.Color.Blue;
            this.lblPaciente.Location = new System.Drawing.Point(239, 45);
            this.lblPaciente.Name = "lblPaciente";
            this.lblPaciente.Size = new System.Drawing.Size(448, 16);
            this.lblPaciente.TabIndex = 4;
            this.lblPaciente.Text = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(171, 45);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 16);
            this.label4.TabIndex = 3;
            this.label4.Text = "Paciente";
            // 
            // lblNumHistoria
            // 
            this.lblNumHistoria.AutoSize = true;
            this.lblNumHistoria.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumHistoria.ForeColor = System.Drawing.Color.Blue;
            this.lblNumHistoria.Location = new System.Drawing.Point(239, 29);
            this.lblNumHistoria.Name = "lblNumHistoria";
            this.lblNumHistoria.Size = new System.Drawing.Size(78, 16);
            this.lblNumHistoria.TabIndex = 2;
            this.lblNumHistoria.Text = "9999999999";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(151, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "H. Clinica N°";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(10, 20);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(89, 122);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(3, 16);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(1350, 133);
            this.shapeContainer1.TabIndex = 42;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape1
            // 
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 106;
            this.lineShape1.X2 = 1340;
            this.lineShape1.Y1 = 50;
            this.lineShape1.Y2 = 50;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox3.Controls.Add(this.txtAlergias);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(8, 156);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1356, 51);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = " Alergias";
            // 
            // txtAlergias
            // 
            this.txtAlergias.BackColor = System.Drawing.Color.DarkRed;
            this.txtAlergias.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAlergias.ForeColor = System.Drawing.Color.White;
            this.txtAlergias.Location = new System.Drawing.Point(7, 19);
            this.txtAlergias.Name = "txtAlergias";
            this.txtAlergias.ReadOnly = true;
            this.txtAlergias.Size = new System.Drawing.Size(1343, 26);
            this.txtAlergias.TabIndex = 0;
            this.txtAlergias.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tabControl1
            // 
            this.tabControl1.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.tabControl1.Controls.Add(this.tbpMotivoConsulta);
            this.tabControl1.Controls.Add(this.tpgExamFisico);
            this.tabControl1.Controls.Add(this.tbpAyudaDx);
            this.tabControl1.Controls.Add(this.tbpPlanTrabajo);
            this.tabControl1.Controls.Add(this.tbpDiagnostico);
            this.tabControl1.Controls.Add(this.tbpPrescripcion);
            this.tabControl1.Controls.Add(this.tbpComplementarios);
            this.tabControl1.Controls.Add(this.tbpConsultaAnterior);
            this.tabControl1.HotTrack = true;
            this.tabControl1.Location = new System.Drawing.Point(9, 213);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1349, 439);
            this.tabControl1.TabIndex = 3;
            // 
            // tbpMotivoConsulta
            // 
            this.tbpMotivoConsulta.Controls.Add(this.tabControl2);
            this.tbpMotivoConsulta.Location = new System.Drawing.Point(61, 4);
            this.tbpMotivoConsulta.Name = "tbpMotivoConsulta";
            this.tbpMotivoConsulta.Padding = new System.Windows.Forms.Padding(3);
            this.tbpMotivoConsulta.Size = new System.Drawing.Size(1284, 431);
            this.tbpMotivoConsulta.TabIndex = 0;
            this.tbpMotivoConsulta.Text = "Motivo de la Consulta";
            this.tbpMotivoConsulta.UseVisualStyleBackColor = true;
            this.tbpMotivoConsulta.Enter += new System.EventHandler(this.tbpMotivoConsulta_Enter);
            this.tbpMotivoConsulta.Leave += new System.EventHandler(this.tabPage1_Leave);
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tbpAnamnesis);
            this.tabControl2.Controls.Add(this.btnAntecPatologicos);
            this.tabControl2.Controls.Add(this.tbpDiagnosticosAnteriores);
            this.tabControl2.Location = new System.Drawing.Point(7, 7);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(1273, 418);
            this.tabControl2.TabIndex = 0;
            // 
            // tbpAnamnesis
            // 
            this.tbpAnamnesis.Controls.Add(this.groupBox40);
            this.tbpAnamnesis.Controls.Add(this.groupBox6);
            this.tbpAnamnesis.Controls.Add(this.groupBox4);
            this.tbpAnamnesis.Controls.Add(this.groupBox2);
            this.tbpAnamnesis.Location = new System.Drawing.Point(4, 22);
            this.tbpAnamnesis.Name = "tbpAnamnesis";
            this.tbpAnamnesis.Padding = new System.Windows.Forms.Padding(3);
            this.tbpAnamnesis.Size = new System.Drawing.Size(1265, 392);
            this.tbpAnamnesis.TabIndex = 0;
            this.tbpAnamnesis.Text = "Anamnesis";
            this.tbpAnamnesis.UseVisualStyleBackColor = true;
            this.tbpAnamnesis.Leave += new System.EventHandler(this.tabPage8_Leave);
            // 
            // groupBox40
            // 
            this.groupBox40.Controls.Add(this.txtFUltMenstruacion);
            this.groupBox40.Controls.Add(this.label2);
            this.groupBox40.Location = new System.Drawing.Point(7, 291);
            this.groupBox40.Name = "groupBox40";
            this.groupBox40.Size = new System.Drawing.Size(433, 95);
            this.groupBox40.TabIndex = 5;
            this.groupBox40.TabStop = false;
            this.groupBox40.Text = "Otros datos de Interés";
            // 
            // txtFUltMenstruacion
            // 
            this.txtFUltMenstruacion.Location = new System.Drawing.Point(163, 21);
            this.txtFUltMenstruacion.Name = "txtFUltMenstruacion";
            this.txtFUltMenstruacion.Size = new System.Drawing.Size(100, 20);
            this.txtFUltMenstruacion.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(151, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Fecha de Última Menstruación";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.groupBox21);
            this.groupBox6.Controls.Add(this.label18);
            this.groupBox6.Controls.Add(this.txtObservHabNoc);
            this.groupBox6.Location = new System.Drawing.Point(7, 153);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(433, 132);
            this.groupBox6.TabIndex = 3;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Hábitos Nocivos";
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this.chkOtros);
            this.groupBox21.Controls.Add(this.chkDrogas);
            this.groupBox21.Controls.Add(this.chkTabaco);
            this.groupBox21.Controls.Add(this.chkAlcohol);
            this.groupBox21.Location = new System.Drawing.Point(6, 13);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(421, 27);
            this.groupBox21.TabIndex = 10;
            this.groupBox21.TabStop = false;
            // 
            // chkOtros
            // 
            this.chkOtros.AutoSize = true;
            this.chkOtros.Location = new System.Drawing.Point(360, 8);
            this.chkOtros.Name = "chkOtros";
            this.chkOtros.Size = new System.Drawing.Size(51, 17);
            this.chkOtros.TabIndex = 0;
            this.chkOtros.Text = "Otros";
            this.chkOtros.UseVisualStyleBackColor = true;
            // 
            // chkDrogas
            // 
            this.chkDrogas.AutoSize = true;
            this.chkDrogas.Location = new System.Drawing.Point(243, 8);
            this.chkDrogas.Name = "chkDrogas";
            this.chkDrogas.Size = new System.Drawing.Size(60, 17);
            this.chkDrogas.TabIndex = 0;
            this.chkDrogas.Text = "Drogas";
            this.chkDrogas.UseVisualStyleBackColor = true;
            // 
            // chkTabaco
            // 
            this.chkTabaco.AutoSize = true;
            this.chkTabaco.Location = new System.Drawing.Point(123, 8);
            this.chkTabaco.Name = "chkTabaco";
            this.chkTabaco.Size = new System.Drawing.Size(63, 17);
            this.chkTabaco.TabIndex = 0;
            this.chkTabaco.Text = "Tabaco";
            this.chkTabaco.UseVisualStyleBackColor = true;
            // 
            // chkAlcohol
            // 
            this.chkAlcohol.AutoSize = true;
            this.chkAlcohol.Location = new System.Drawing.Point(5, 8);
            this.chkAlcohol.Name = "chkAlcohol";
            this.chkAlcohol.Size = new System.Drawing.Size(61, 17);
            this.chkAlcohol.TabIndex = 0;
            this.chkAlcohol.Text = "Alcohol";
            this.chkAlcohol.UseVisualStyleBackColor = true;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 42);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(59, 13);
            this.label18.TabIndex = 9;
            this.label18.Text = "Especificar";
            // 
            // txtObservHabNoc
            // 
            this.txtObservHabNoc.Location = new System.Drawing.Point(5, 58);
            this.txtObservHabNoc.Multiline = true;
            this.txtObservHabNoc.Name = "txtObservHabNoc";
            this.txtObservHabNoc.Size = new System.Drawing.Size(421, 66);
            this.txtObservHabNoc.TabIndex = 8;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.dgvSintomas);
            this.groupBox4.Location = new System.Drawing.Point(448, 7);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(811, 379);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Síntomas y Signos Referidos";
            // 
            // dgvSintomas
            // 
            this.dgvSintomas.AllowUserToAddRows = false;
            this.dgvSintomas.AllowUserToDeleteRows = false;
            this.dgvSintomas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSintomas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Seleccion});
            this.dgvSintomas.Location = new System.Drawing.Point(6, 20);
            this.dgvSintomas.Name = "dgvSintomas";
            this.dgvSintomas.Size = new System.Drawing.Size(799, 353);
            this.dgvSintomas.TabIndex = 0;
            this.dgvSintomas.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSintomas_CellContentClick);
            this.dgvSintomas.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvSintomas_CurrentCellDirtyStateChanged);
            // 
            // Seleccion
            // 
            this.Seleccion.HeaderText = "";
            this.Seleccion.Name = "Seleccion";
            this.Seleccion.Width = 30;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtMotivo);
            this.groupBox2.Location = new System.Drawing.Point(7, 7);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(433, 140);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Motivo de la Consulta o Evolución del Paciente";
            // 
            // txtMotivo
            // 
            this.txtMotivo.Location = new System.Drawing.Point(6, 19);
            this.txtMotivo.Multiline = true;
            this.txtMotivo.Name = "txtMotivo";
            this.txtMotivo.Size = new System.Drawing.Size(421, 111);
            this.txtMotivo.TabIndex = 5;
            // 
            // btnAntecPatologicos
            // 
            this.btnAntecPatologicos.Controls.Add(this.btnAntecFamiliares);
            this.btnAntecPatologicos.Controls.Add(this.btnAntecAlergias);
            this.btnAntecPatologicos.Controls.Add(this.btnAntecQuirurg);
            this.btnAntecPatologicos.Controls.Add(this.btnAntecPatol);
            this.btnAntecPatologicos.Controls.Add(this.gpbAntecedentes);
            this.btnAntecPatologicos.Location = new System.Drawing.Point(4, 22);
            this.btnAntecPatologicos.Name = "btnAntecPatologicos";
            this.btnAntecPatologicos.Padding = new System.Windows.Forms.Padding(3);
            this.btnAntecPatologicos.Size = new System.Drawing.Size(1265, 392);
            this.btnAntecPatologicos.TabIndex = 1;
            this.btnAntecPatologicos.Text = "Antecedentes";
            this.btnAntecPatologicos.UseVisualStyleBackColor = true;
            this.btnAntecPatologicos.Enter += new System.EventHandler(this.tbpAntecedentes_Enter);
            this.btnAntecPatologicos.Leave += new System.EventHandler(this.tabPage9_Leave);
            // 
            // btnAntecFamiliares
            // 
            this.btnAntecFamiliares.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAntecFamiliares.Location = new System.Drawing.Point(56, 252);
            this.btnAntecFamiliares.Name = "btnAntecFamiliares";
            this.btnAntecFamiliares.Size = new System.Drawing.Size(315, 42);
            this.btnAntecFamiliares.TabIndex = 4;
            this.btnAntecFamiliares.Text = "Antecedentes Familiares";
            this.btnAntecFamiliares.UseVisualStyleBackColor = true;
            this.btnAntecFamiliares.Click += new System.EventHandler(this.btnAntecFamiliares_Click);
            // 
            // btnAntecAlergias
            // 
            this.btnAntecAlergias.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAntecAlergias.Location = new System.Drawing.Point(56, 201);
            this.btnAntecAlergias.Name = "btnAntecAlergias";
            this.btnAntecAlergias.Size = new System.Drawing.Size(315, 42);
            this.btnAntecAlergias.TabIndex = 3;
            this.btnAntecAlergias.Text = "Antecedentes Alérgicos";
            this.btnAntecAlergias.UseVisualStyleBackColor = true;
            this.btnAntecAlergias.Click += new System.EventHandler(this.btnAntecAlergias_Click);
            // 
            // btnAntecQuirurg
            // 
            this.btnAntecQuirurg.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAntecQuirurg.Location = new System.Drawing.Point(56, 150);
            this.btnAntecQuirurg.Name = "btnAntecQuirurg";
            this.btnAntecQuirurg.Size = new System.Drawing.Size(315, 42);
            this.btnAntecQuirurg.TabIndex = 2;
            this.btnAntecQuirurg.Text = "Antecedentes Quirúrgicos";
            this.btnAntecQuirurg.UseVisualStyleBackColor = true;
            this.btnAntecQuirurg.Click += new System.EventHandler(this.btnAntecQuirurg_Click);
            // 
            // btnAntecPatol
            // 
            this.btnAntecPatol.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAntecPatol.Location = new System.Drawing.Point(56, 99);
            this.btnAntecPatol.Name = "btnAntecPatol";
            this.btnAntecPatol.Size = new System.Drawing.Size(315, 42);
            this.btnAntecPatol.TabIndex = 1;
            this.btnAntecPatol.Text = "Antecedentes Patológicos";
            this.btnAntecPatol.UseVisualStyleBackColor = true;
            this.btnAntecPatol.Click += new System.EventHandler(this.btnAntecPatol_Click);
            // 
            // gpbAntecedentes
            // 
            this.gpbAntecedentes.Controls.Add(this.cmbFamiliar);
            this.gpbAntecedentes.Controls.Add(this.lblFamiliar);
            this.gpbAntecedentes.Controls.Add(this.dgvAntecedentes);
            this.gpbAntecedentes.Controls.Add(this.btnAddAntecedentes);
            this.gpbAntecedentes.Controls.Add(this.txtAntecedenteObserv);
            this.gpbAntecedentes.Controls.Add(this.label20);
            this.gpbAntecedentes.Controls.Add(this.cboAntecedente);
            this.gpbAntecedentes.Controls.Add(this.lblAntecedente);
            this.gpbAntecedentes.Location = new System.Drawing.Point(451, 8);
            this.gpbAntecedentes.Name = "gpbAntecedentes";
            this.gpbAntecedentes.Size = new System.Drawing.Size(815, 378);
            this.gpbAntecedentes.TabIndex = 0;
            this.gpbAntecedentes.TabStop = false;
            this.gpbAntecedentes.Text = "Patológicos";
            // 
            // cmbFamiliar
            // 
            this.cmbFamiliar.FormattingEnabled = true;
            this.cmbFamiliar.Location = new System.Drawing.Point(535, 17);
            this.cmbFamiliar.Name = "cmbFamiliar";
            this.cmbFamiliar.Size = new System.Drawing.Size(171, 21);
            this.cmbFamiliar.TabIndex = 7;
            // 
            // lblFamiliar
            // 
            this.lblFamiliar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblFamiliar.Location = new System.Drawing.Point(452, 21);
            this.lblFamiliar.Name = "lblFamiliar";
            this.lblFamiliar.Size = new System.Drawing.Size(77, 13);
            this.lblFamiliar.TabIndex = 6;
            this.lblFamiliar.Text = "Familiar";
            this.lblFamiliar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dgvAntecedentes
            // 
            this.dgvAntecedentes.AllowUserToAddRows = false;
            this.dgvAntecedentes.AllowUserToDeleteRows = false;
            this.dgvAntecedentes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAntecedentes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.btnDelAntecedentes,
            this.btnUpdAntecedentes});
            this.dgvAntecedentes.Location = new System.Drawing.Point(6, 83);
            this.dgvAntecedentes.Name = "dgvAntecedentes";
            this.dgvAntecedentes.ReadOnly = true;
            this.dgvAntecedentes.Size = new System.Drawing.Size(803, 288);
            this.dgvAntecedentes.TabIndex = 5;
            this.dgvAntecedentes.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAntecedentes_CellClick);
            this.dgvAntecedentes.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgvAntecedentes_CellPainting);
            // 
            // btnDelAntecedentes
            // 
            this.btnDelAntecedentes.HeaderText = "";
            this.btnDelAntecedentes.Name = "btnDelAntecedentes";
            this.btnDelAntecedentes.ReadOnly = true;
            this.btnDelAntecedentes.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.btnDelAntecedentes.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // btnUpdAntecedentes
            // 
            this.btnUpdAntecedentes.HeaderText = "";
            this.btnUpdAntecedentes.Name = "btnUpdAntecedentes";
            this.btnUpdAntecedentes.ReadOnly = true;
            this.btnUpdAntecedentes.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.btnUpdAntecedentes.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // btnAddAntecedentes
            // 
            this.btnAddAntecedentes.Location = new System.Drawing.Point(734, 43);
            this.btnAddAntecedentes.Name = "btnAddAntecedentes";
            this.btnAddAntecedentes.Size = new System.Drawing.Size(75, 23);
            this.btnAddAntecedentes.TabIndex = 4;
            this.btnAddAntecedentes.Text = "Agregar";
            this.btnAddAntecedentes.UseVisualStyleBackColor = true;
            this.btnAddAntecedentes.Click += new System.EventHandler(this.btnAddAntecedentes_Click);
            // 
            // txtAntecedenteObserv
            // 
            this.txtAntecedenteObserv.Location = new System.Drawing.Point(90, 45);
            this.txtAntecedenteObserv.Name = "txtAntecedenteObserv";
            this.txtAntecedenteObserv.Size = new System.Drawing.Size(617, 20);
            this.txtAntecedenteObserv.TabIndex = 3;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(10, 49);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(78, 13);
            this.label20.TabIndex = 2;
            this.label20.Text = "Observaciones";
            // 
            // cboAntecedente
            // 
            this.cboAntecedente.FormattingEnabled = true;
            this.cboAntecedente.Location = new System.Drawing.Point(90, 17);
            this.cboAntecedente.Name = "cboAntecedente";
            this.cboAntecedente.Size = new System.Drawing.Size(356, 21);
            this.cboAntecedente.TabIndex = 1;
            // 
            // lblAntecedente
            // 
            this.lblAntecedente.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblAntecedente.Location = new System.Drawing.Point(7, 21);
            this.lblAntecedente.Name = "lblAntecedente";
            this.lblAntecedente.Size = new System.Drawing.Size(77, 13);
            this.lblAntecedente.TabIndex = 0;
            this.lblAntecedente.Text = "Antecedente";
            this.lblAntecedente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbpDiagnosticosAnteriores
            // 
            this.tbpDiagnosticosAnteriores.Controls.Add(this.dgvAntecDiagnosticos);
            this.tbpDiagnosticosAnteriores.Location = new System.Drawing.Point(4, 22);
            this.tbpDiagnosticosAnteriores.Name = "tbpDiagnosticosAnteriores";
            this.tbpDiagnosticosAnteriores.Padding = new System.Windows.Forms.Padding(3);
            this.tbpDiagnosticosAnteriores.Size = new System.Drawing.Size(1265, 392);
            this.tbpDiagnosticosAnteriores.TabIndex = 2;
            this.tbpDiagnosticosAnteriores.Text = "Diagnósticos Anteriores";
            this.tbpDiagnosticosAnteriores.UseVisualStyleBackColor = true;
            this.tbpDiagnosticosAnteriores.Leave += new System.EventHandler(this.tabPage10_Leave);
            // 
            // dgvAntecDiagnosticos
            // 
            this.dgvAntecDiagnosticos.AllowUserToAddRows = false;
            this.dgvAntecDiagnosticos.AllowUserToDeleteRows = false;
            this.dgvAntecDiagnosticos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAntecDiagnosticos.Enabled = false;
            this.dgvAntecDiagnosticos.Location = new System.Drawing.Point(7, 7);
            this.dgvAntecDiagnosticos.Name = "dgvAntecDiagnosticos";
            this.dgvAntecDiagnosticos.ReadOnly = true;
            this.dgvAntecDiagnosticos.Size = new System.Drawing.Size(1259, 379);
            this.dgvAntecDiagnosticos.TabIndex = 0;
            // 
            // tpgExamFisico
            // 
            this.tpgExamFisico.Controls.Add(this.tabControl4);
            this.tpgExamFisico.Location = new System.Drawing.Point(61, 4);
            this.tpgExamFisico.Name = "tpgExamFisico";
            this.tpgExamFisico.Padding = new System.Windows.Forms.Padding(3);
            this.tpgExamFisico.Size = new System.Drawing.Size(1284, 431);
            this.tpgExamFisico.TabIndex = 1;
            this.tpgExamFisico.Text = "Examen Físico";
            this.tpgExamFisico.UseVisualStyleBackColor = true;
            this.tpgExamFisico.Enter += new System.EventHandler(this.tpgExamFisico_Enter);
            this.tpgExamFisico.Leave += new System.EventHandler(this.tabPage2_Leave);
            // 
            // tabControl4
            // 
            this.tabControl4.Controls.Add(this.tabPage1);
            this.tabControl4.Controls.Add(this.tabPage2);
            this.tabControl4.Location = new System.Drawing.Point(6, 7);
            this.tabControl4.Name = "tabControl4";
            this.tabControl4.SelectedIndex = 0;
            this.tabControl4.Size = new System.Drawing.Size(1274, 418);
            this.tabControl4.TabIndex = 4;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox14);
            this.tabPage1.Controls.Add(this.groupBox12);
            this.tabPage1.Controls.Add(this.groupBox11);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1266, 392);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Funciones Vitales";
            this.tabPage1.UseVisualStyleBackColor = true;
            this.tabPage1.Leave += new System.EventHandler(this.tabPage1_Leave_1);
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.cboEstHidratacion);
            this.groupBox14.Controls.Add(this.cboEstNutricional);
            this.groupBox14.Controls.Add(this.label40);
            this.groupBox14.Controls.Add(this.label37);
            this.groupBox14.Controls.Add(this.cboEstGeneral);
            this.groupBox14.Controls.Add(this.label36);
            this.groupBox14.Location = new System.Drawing.Point(0, 279);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(293, 106);
            this.groupBox14.TabIndex = 5;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Estado del Paciente";
            // 
            // cboEstHidratacion
            // 
            this.cboEstHidratacion.FormattingEnabled = true;
            this.cboEstHidratacion.Items.AddRange(new object[] {
            "Bueno",
            "Malo",
            "Regular"});
            this.cboEstHidratacion.Location = new System.Drawing.Point(127, 82);
            this.cboEstHidratacion.Name = "cboEstHidratacion";
            this.cboEstHidratacion.Size = new System.Drawing.Size(157, 21);
            this.cboEstHidratacion.TabIndex = 1;
            // 
            // cboEstNutricional
            // 
            this.cboEstNutricional.FormattingEnabled = true;
            this.cboEstNutricional.Items.AddRange(new object[] {
            "Bueno",
            "Malo",
            "Regular"});
            this.cboEstNutricional.Location = new System.Drawing.Point(127, 51);
            this.cboEstNutricional.Name = "cboEstNutricional";
            this.cboEstNutricional.Size = new System.Drawing.Size(157, 21);
            this.cboEstNutricional.TabIndex = 1;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(5, 86);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(112, 13);
            this.label40.TabIndex = 0;
            this.label40.Text = "Estado de Hidratación";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(24, 55);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(93, 13);
            this.label37.TabIndex = 0;
            this.label37.Text = "Estado Nutricional";
            // 
            // cboEstGeneral
            // 
            this.cboEstGeneral.FormattingEnabled = true;
            this.cboEstGeneral.Items.AddRange(new object[] {
            "Bueno",
            "Malo",
            "Regular"});
            this.cboEstGeneral.Location = new System.Drawing.Point(127, 22);
            this.cboEstGeneral.Name = "cboEstGeneral";
            this.cboEstGeneral.Size = new System.Drawing.Size(157, 21);
            this.cboEstGeneral.TabIndex = 1;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(37, 26);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(80, 13);
            this.label36.TabIndex = 0;
            this.label36.Text = "Estado General";
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.chkOPersona);
            this.groupBox12.Controls.Add(this.chkOEspacio);
            this.groupBox12.Controls.Add(this.chkOTiempo);
            this.groupBox12.Controls.Add(this.chkLucido);
            this.groupBox12.Location = new System.Drawing.Point(0, 188);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(293, 87);
            this.groupBox12.TabIndex = 4;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "LOTEP";
            // 
            // chkOPersona
            // 
            this.chkOPersona.AutoSize = true;
            this.chkOPersona.Location = new System.Drawing.Point(170, 56);
            this.chkOPersona.Name = "chkOPersona";
            this.chkOPersona.Size = new System.Drawing.Size(114, 17);
            this.chkOPersona.TabIndex = 1;
            this.chkOPersona.Text = "Orientado Persona";
            this.chkOPersona.UseVisualStyleBackColor = true;
            // 
            // chkOEspacio
            // 
            this.chkOEspacio.AutoSize = true;
            this.chkOEspacio.Location = new System.Drawing.Point(11, 56);
            this.chkOEspacio.Name = "chkOEspacio";
            this.chkOEspacio.Size = new System.Drawing.Size(113, 17);
            this.chkOEspacio.TabIndex = 1;
            this.chkOEspacio.Text = "Orientado Espacio";
            this.chkOEspacio.UseVisualStyleBackColor = true;
            // 
            // chkOTiempo
            // 
            this.chkOTiempo.AutoSize = true;
            this.chkOTiempo.Location = new System.Drawing.Point(170, 23);
            this.chkOTiempo.Name = "chkOTiempo";
            this.chkOTiempo.Size = new System.Drawing.Size(110, 17);
            this.chkOTiempo.TabIndex = 1;
            this.chkOTiempo.Text = "Orientado Tiempo";
            this.chkOTiempo.UseVisualStyleBackColor = true;
            // 
            // chkLucido
            // 
            this.chkLucido.AutoSize = true;
            this.chkLucido.Location = new System.Drawing.Point(11, 23);
            this.chkLucido.Name = "chkLucido";
            this.chkLucido.Size = new System.Drawing.Size(58, 17);
            this.chkLucido.TabIndex = 1;
            this.chkLucido.Text = "Lucido";
            this.chkLucido.UseVisualStyleBackColor = true;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.label6);
            this.groupBox11.Controls.Add(this.label38);
            this.groupBox11.Controls.Add(this.txtTalla);
            this.groupBox11.Controls.Add(this.label39);
            this.groupBox11.Controls.Add(this.label41);
            this.groupBox11.Controls.Add(this.txtPeso);
            this.groupBox11.Controls.Add(this.label42);
            this.groupBox11.Controls.Add(this.label33);
            this.groupBox11.Controls.Add(this.txtTemperatura);
            this.groupBox11.Controls.Add(this.label32);
            this.groupBox11.Controls.Add(this.txtPulso);
            this.groupBox11.Controls.Add(this.label34);
            this.groupBox11.Controls.Add(this.label31);
            this.groupBox11.Controls.Add(this.txtPAMinima);
            this.groupBox11.Controls.Add(this.label30);
            this.groupBox11.Controls.Add(this.txtPAMaxima);
            this.groupBox11.Controls.Add(this.label29);
            this.groupBox11.Location = new System.Drawing.Point(0, 6);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(293, 178);
            this.groupBox11.TabIndex = 3;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Funciones Vitales";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(4, 87);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(78, 13);
            this.label6.TabIndex = 21;
            this.label6.Text = "Temperatura";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(158, 147);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(29, 13);
            this.label38.TabIndex = 20;
            this.label38.Text = "cmts";
            // 
            // txtTalla
            // 
            this.txtTalla.Location = new System.Drawing.Point(113, 143);
            this.txtTalla.Name = "txtTalla";
            this.txtTalla.Size = new System.Drawing.Size(39, 20);
            this.txtTalla.TabIndex = 19;
            this.txtTalla.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(4, 146);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(35, 13);
            this.label39.TabIndex = 18;
            this.label39.Text = "Talla";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(158, 118);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(20, 13);
            this.label41.TabIndex = 15;
            this.label41.Text = "Kg";
            // 
            // txtPeso
            // 
            this.txtPeso.Location = new System.Drawing.Point(113, 115);
            this.txtPeso.Name = "txtPeso";
            this.txtPeso.Size = new System.Drawing.Size(39, 20);
            this.txtPeso.TabIndex = 14;
            this.txtPeso.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(5, 118);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(35, 13);
            this.label42.TabIndex = 13;
            this.label42.Text = "Peso";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(158, 88);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(18, 13);
            this.label33.TabIndex = 12;
            this.label33.Text = "C°";
            // 
            // txtTemperatura
            // 
            this.txtTemperatura.Location = new System.Drawing.Point(113, 84);
            this.txtTemperatura.Name = "txtTemperatura";
            this.txtTemperatura.Size = new System.Drawing.Size(39, 20);
            this.txtTemperatura.TabIndex = 11;
            this.txtTemperatura.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(158, 58);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(14, 13);
            this.label32.TabIndex = 9;
            this.label32.Text = "x\'";
            // 
            // txtPulso
            // 
            this.txtPulso.Location = new System.Drawing.Point(113, 54);
            this.txtPulso.Name = "txtPulso";
            this.txtPulso.Size = new System.Drawing.Size(39, 20);
            this.txtPulso.TabIndex = 6;
            this.txtPulso.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(5, 57);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(38, 13);
            this.label34.TabIndex = 5;
            this.label34.Text = "Pulso";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(210, 27);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(37, 13);
            this.label31.TabIndex = 4;
            this.label31.Text = "mmHg";
            // 
            // txtPAMinima
            // 
            this.txtPAMinima.Location = new System.Drawing.Point(165, 23);
            this.txtPAMinima.Name = "txtPAMinima";
            this.txtPAMinima.Size = new System.Drawing.Size(39, 20);
            this.txtPAMinima.TabIndex = 3;
            this.txtPAMinima.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(153, 27);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(12, 13);
            this.label30.TabIndex = 2;
            this.label30.Text = "/";
            // 
            // txtPAMaxima
            // 
            this.txtPAMaxima.Location = new System.Drawing.Point(113, 23);
            this.txtPAMaxima.Name = "txtPAMaxima";
            this.txtPAMaxima.Size = new System.Drawing.Size(39, 20);
            this.txtPAMaxima.TabIndex = 1;
            this.txtPAMaxima.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(5, 26);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(93, 13);
            this.label29.TabIndex = 0;
            this.label29.Text = "Presión Arterial";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.pbxParteCuerpo);
            this.tabPage2.Controls.Add(this.groupBox8);
            this.tabPage2.Controls.Add(this.groupBox13);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1266, 392);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Examen Dirigido";
            this.tabPage2.UseVisualStyleBackColor = true;
            this.tabPage2.Leave += new System.EventHandler(this.tabPage2_Leave_1);
            // 
            // pbxParteCuerpo
            // 
            this.pbxParteCuerpo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbxParteCuerpo.Location = new System.Drawing.Point(6, 6);
            this.pbxParteCuerpo.Name = "pbxParteCuerpo";
            this.pbxParteCuerpo.Size = new System.Drawing.Size(284, 380);
            this.pbxParteCuerpo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxParteCuerpo.TabIndex = 7;
            this.pbxParteCuerpo.TabStop = false;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.dgvPartesCuerpo);
            this.groupBox8.Location = new System.Drawing.Point(296, 6);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(964, 159);
            this.groupBox8.TabIndex = 5;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Partes del Examen Preferencial";
            // 
            // dgvPartesCuerpo
            // 
            this.dgvPartesCuerpo.AllowUserToAddRows = false;
            this.dgvPartesCuerpo.AllowUserToDeleteRows = false;
            this.dgvPartesCuerpo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPartesCuerpo.Location = new System.Drawing.Point(6, 19);
            this.dgvPartesCuerpo.Name = "dgvPartesCuerpo";
            this.dgvPartesCuerpo.Size = new System.Drawing.Size(949, 134);
            this.dgvPartesCuerpo.TabIndex = 9;
            this.dgvPartesCuerpo.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPartesCuerpo_CellClick_1);
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.btnGuardaSegmentos);
            this.groupBox13.Controls.Add(this.dgvSegmentos);
            this.groupBox13.Location = new System.Drawing.Point(296, 171);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(964, 215);
            this.groupBox13.TabIndex = 4;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Segmentos";
            // 
            // btnGuardaSegmentos
            // 
            this.btnGuardaSegmentos.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardaSegmentos.Location = new System.Drawing.Point(801, 20);
            this.btnGuardaSegmentos.Name = "btnGuardaSegmentos";
            this.btnGuardaSegmentos.Size = new System.Drawing.Size(156, 188);
            this.btnGuardaSegmentos.TabIndex = 1;
            this.btnGuardaSegmentos.Text = "Grabar";
            this.btnGuardaSegmentos.UseVisualStyleBackColor = true;
            this.btnGuardaSegmentos.Click += new System.EventHandler(this.btnGuardaSegmentos_Click);
            // 
            // dgvSegmentos
            // 
            this.dgvSegmentos.AllowUserToAddRows = false;
            this.dgvSegmentos.AllowUserToDeleteRows = false;
            this.dgvSegmentos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSegmentos.Location = new System.Drawing.Point(6, 20);
            this.dgvSegmentos.Name = "dgvSegmentos";
            this.dgvSegmentos.Size = new System.Drawing.Size(790, 188);
            this.dgvSegmentos.TabIndex = 0;
            this.dgvSegmentos.Leave += new System.EventHandler(this.dgvSegmentos_Leave);
            // 
            // tbpAyudaDx
            // 
            this.tbpAyudaDx.Controls.Add(this.groupBox16);
            this.tbpAyudaDx.Controls.Add(this.groupBox15);
            this.tbpAyudaDx.Location = new System.Drawing.Point(61, 4);
            this.tbpAyudaDx.Name = "tbpAyudaDx";
            this.tbpAyudaDx.Padding = new System.Windows.Forms.Padding(3);
            this.tbpAyudaDx.Size = new System.Drawing.Size(1284, 431);
            this.tbpAyudaDx.TabIndex = 2;
            this.tbpAyudaDx.Text = "Informes de Ayuda Diagnóstica";
            this.tbpAyudaDx.UseVisualStyleBackColor = true;
            this.tbpAyudaDx.Enter += new System.EventHandler(this.tbpAyudaDx_Enter);
            this.tbpAyudaDx.Leave += new System.EventHandler(this.tabPage3_Leave);
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.dgvAyudaDiagnostica);
            this.groupBox16.Location = new System.Drawing.Point(570, 7);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(709, 418);
            this.groupBox16.TabIndex = 1;
            this.groupBox16.TabStop = false;
            // 
            // dgvAyudaDiagnostica
            // 
            this.dgvAyudaDiagnostica.AllowUserToAddRows = false;
            this.dgvAyudaDiagnostica.AllowUserToDeleteRows = false;
            this.dgvAyudaDiagnostica.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAyudaDiagnostica.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.btnDelAyudaDx,
            this.btnUpdAyudaDx});
            this.dgvAyudaDiagnostica.Location = new System.Drawing.Point(7, 12);
            this.dgvAyudaDiagnostica.Name = "dgvAyudaDiagnostica";
            this.dgvAyudaDiagnostica.ReadOnly = true;
            this.dgvAyudaDiagnostica.Size = new System.Drawing.Size(696, 400);
            this.dgvAyudaDiagnostica.TabIndex = 0;
            this.dgvAyudaDiagnostica.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAyudaDiagnostica_CellClick);
            this.dgvAyudaDiagnostica.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgvAyudaDiagnostica_CellPainting);
            // 
            // btnDelAyudaDx
            // 
            this.btnDelAyudaDx.HeaderText = "";
            this.btnDelAyudaDx.Name = "btnDelAyudaDx";
            this.btnDelAyudaDx.ReadOnly = true;
            // 
            // btnUpdAyudaDx
            // 
            this.btnUpdAyudaDx.HeaderText = "";
            this.btnUpdAyudaDx.Name = "btnUpdAyudaDx";
            this.btnUpdAyudaDx.ReadOnly = true;
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.btnAyudaDiagNuevo);
            this.groupBox15.Controls.Add(this.txtAyudaDiagId);
            this.groupBox15.Controls.Add(this.label7);
            this.groupBox15.Controls.Add(this.btnGuardarAyudaDx);
            this.groupBox15.Controls.Add(this.txtInfExamen);
            this.groupBox15.Controls.Add(this.label45);
            this.groupBox15.Controls.Add(this.cmbTipoExamen);
            this.groupBox15.Controls.Add(this.label44);
            this.groupBox15.Controls.Add(this.dtpFechaExamen);
            this.groupBox15.Controls.Add(this.label43);
            this.groupBox15.Location = new System.Drawing.Point(7, 7);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(553, 418);
            this.groupBox15.TabIndex = 0;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Registro";
            // 
            // btnAyudaDiagNuevo
            // 
            this.btnAyudaDiagNuevo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAyudaDiagNuevo.Location = new System.Drawing.Point(421, 348);
            this.btnAyudaDiagNuevo.Name = "btnAyudaDiagNuevo";
            this.btnAyudaDiagNuevo.Size = new System.Drawing.Size(117, 29);
            this.btnAyudaDiagNuevo.TabIndex = 9;
            this.btnAyudaDiagNuevo.Text = "Nuevo";
            this.btnAyudaDiagNuevo.UseVisualStyleBackColor = true;
            this.btnAyudaDiagNuevo.Click += new System.EventHandler(this.btnAyudaDiagNuevo_Click);
            // 
            // txtAyudaDiagId
            // 
            this.txtAyudaDiagId.Location = new System.Drawing.Point(306, 19);
            this.txtAyudaDiagId.Name = "txtAyudaDiagId";
            this.txtAyudaDiagId.Size = new System.Drawing.Size(100, 20);
            this.txtAyudaDiagId.TabIndex = 8;
            this.txtAyudaDiagId.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(286, 23);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(16, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Id";
            this.label7.Visible = false;
            // 
            // btnGuardarAyudaDx
            // 
            this.btnGuardarAyudaDx.Enabled = false;
            this.btnGuardarAyudaDx.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardarAyudaDx.Location = new System.Drawing.Point(421, 383);
            this.btnGuardarAyudaDx.Name = "btnGuardarAyudaDx";
            this.btnGuardarAyudaDx.Size = new System.Drawing.Size(117, 29);
            this.btnGuardarAyudaDx.TabIndex = 6;
            this.btnGuardarAyudaDx.Text = "Agregar";
            this.btnGuardarAyudaDx.UseVisualStyleBackColor = true;
            this.btnGuardarAyudaDx.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // txtInfExamen
            // 
            this.txtInfExamen.Enabled = false;
            this.txtInfExamen.Location = new System.Drawing.Point(98, 71);
            this.txtInfExamen.Multiline = true;
            this.txtInfExamen.Name = "txtInfExamen";
            this.txtInfExamen.Size = new System.Drawing.Size(308, 341);
            this.txtInfExamen.TabIndex = 5;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(10, 75);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(42, 13);
            this.label45.TabIndex = 4;
            this.label45.Text = "Informe";
            // 
            // cmbTipoExamen
            // 
            this.cmbTipoExamen.Enabled = false;
            this.cmbTipoExamen.FormattingEnabled = true;
            this.cmbTipoExamen.Items.AddRange(new object[] {
            "Laboratorio",
            "Imágenes",
            "Patología",
            "Otros"});
            this.cmbTipoExamen.Location = new System.Drawing.Point(98, 45);
            this.cmbTipoExamen.Name = "cmbTipoExamen";
            this.cmbTipoExamen.Size = new System.Drawing.Size(308, 21);
            this.cmbTipoExamen.TabIndex = 3;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(10, 49);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(84, 13);
            this.label44.TabIndex = 2;
            this.label44.Text = "Tipo de Examen";
            // 
            // dtpFechaExamen
            // 
            this.dtpFechaExamen.Enabled = false;
            this.dtpFechaExamen.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFechaExamen.Location = new System.Drawing.Point(98, 20);
            this.dtpFechaExamen.Name = "dtpFechaExamen";
            this.dtpFechaExamen.Size = new System.Drawing.Size(100, 20);
            this.dtpFechaExamen.TabIndex = 1;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(10, 24);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(37, 13);
            this.label43.TabIndex = 0;
            this.label43.Text = "Fecha";
            // 
            // tbpPlanTrabajo
            // 
            this.tbpPlanTrabajo.Controls.Add(this.groupBox19);
            this.tbpPlanTrabajo.Controls.Add(this.groupBox18);
            this.tbpPlanTrabajo.Controls.Add(this.groupBox17);
            this.tbpPlanTrabajo.Location = new System.Drawing.Point(61, 4);
            this.tbpPlanTrabajo.Name = "tbpPlanTrabajo";
            this.tbpPlanTrabajo.Padding = new System.Windows.Forms.Padding(3);
            this.tbpPlanTrabajo.Size = new System.Drawing.Size(1284, 431);
            this.tbpPlanTrabajo.TabIndex = 3;
            this.tbpPlanTrabajo.Text = "Plan de Trabajo";
            this.tbpPlanTrabajo.UseVisualStyleBackColor = true;
            this.tbpPlanTrabajo.Enter += new System.EventHandler(this.tbpPlanTrabajo_Enter);
            this.tbpPlanTrabajo.Leave += new System.EventHandler(this.tabPage4_Leave);
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this.dgvAsistencia);
            this.groupBox19.Location = new System.Drawing.Point(724, 7);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(554, 418);
            this.groupBox19.TabIndex = 1;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "Control de Asistencia";
            // 
            // dgvAsistencia
            // 
            this.dgvAsistencia.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAsistencia.Location = new System.Drawing.Point(6, 19);
            this.dgvAsistencia.Name = "dgvAsistencia";
            this.dgvAsistencia.Size = new System.Drawing.Size(542, 393);
            this.dgvAsistencia.TabIndex = 0;
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.txtTratamObserv);
            this.groupBox18.Controls.Add(this.label19);
            this.groupBox18.Controls.Add(this.dgvTratamientos);
            this.groupBox18.Controls.Add(this.btnGrabarTx);
            this.groupBox18.Controls.Add(this.txtTratamNumSesiones);
            this.groupBox18.Controls.Add(this.label47);
            this.groupBox18.Controls.Add(this.cmbTratamientos);
            this.groupBox18.Controls.Add(this.label46);
            this.groupBox18.Location = new System.Drawing.Point(6, 77);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(712, 348);
            this.groupBox18.TabIndex = 0;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "Tratamientos";
            // 
            // txtTratamObserv
            // 
            this.txtTratamObserv.Location = new System.Drawing.Point(91, 77);
            this.txtTratamObserv.Name = "txtTratamObserv";
            this.txtTratamObserv.Size = new System.Drawing.Size(504, 20);
            this.txtTratamObserv.TabIndex = 7;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(9, 81);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(78, 13);
            this.label19.TabIndex = 6;
            this.label19.Text = "Observaciones";
            // 
            // dgvTratamientos
            // 
            this.dgvTratamientos.AllowUserToAddRows = false;
            this.dgvTratamientos.AllowUserToDeleteRows = false;
            this.dgvTratamientos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTratamientos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.btnTratamientosDel,
            this.btnTratamientosUpd});
            this.dgvTratamientos.Location = new System.Drawing.Point(6, 104);
            this.dgvTratamientos.Name = "dgvTratamientos";
            this.dgvTratamientos.ReadOnly = true;
            this.dgvTratamientos.Size = new System.Drawing.Size(700, 238);
            this.dgvTratamientos.TabIndex = 5;
            this.dgvTratamientos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTratamientos_CellClick);
            this.dgvTratamientos.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgvTratamientos_CellPainting);
            // 
            // btnTratamientosDel
            // 
            this.btnTratamientosDel.HeaderText = "";
            this.btnTratamientosDel.Name = "btnTratamientosDel";
            this.btnTratamientosDel.ReadOnly = true;
            // 
            // btnTratamientosUpd
            // 
            this.btnTratamientosUpd.HeaderText = "";
            this.btnTratamientosUpd.Name = "btnTratamientosUpd";
            this.btnTratamientosUpd.ReadOnly = true;
            // 
            // btnGrabarTx
            // 
            this.btnGrabarTx.Location = new System.Drawing.Point(601, 76);
            this.btnGrabarTx.Name = "btnGrabarTx";
            this.btnGrabarTx.Size = new System.Drawing.Size(105, 22);
            this.btnGrabarTx.TabIndex = 4;
            this.btnGrabarTx.Text = "Agregar";
            this.btnGrabarTx.UseVisualStyleBackColor = true;
            this.btnGrabarTx.Click += new System.EventHandler(this.btnGrabarTx_Click);
            // 
            // txtTratamNumSesiones
            // 
            this.txtTratamNumSesiones.Location = new System.Drawing.Point(91, 49);
            this.txtTratamNumSesiones.Name = "txtTratamNumSesiones";
            this.txtTratamNumSesiones.Size = new System.Drawing.Size(57, 20);
            this.txtTratamNumSesiones.TabIndex = 3;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(7, 53);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(80, 13);
            this.label47.TabIndex = 2;
            this.label47.Text = "N° de Sesiones";
            // 
            // cmbTratamientos
            // 
            this.cmbTratamientos.FormattingEnabled = true;
            this.cmbTratamientos.Location = new System.Drawing.Point(91, 20);
            this.cmbTratamientos.Name = "cmbTratamientos";
            this.cmbTratamientos.Size = new System.Drawing.Size(504, 21);
            this.cmbTratamientos.TabIndex = 1;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(24, 24);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(63, 13);
            this.label46.TabIndex = 0;
            this.label46.Text = "Tratamiento";
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.btnEco);
            this.groupBox17.Controls.Add(this.btnPat);
            this.groupBox17.Controls.Add(this.btnImg);
            this.groupBox17.Controls.Add(this.btnLab);
            this.groupBox17.Location = new System.Drawing.Point(7, 7);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(711, 64);
            this.groupBox17.TabIndex = 0;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "Ayuda Diagnóstica";
            // 
            // btnEco
            // 
            this.btnEco.Location = new System.Drawing.Point(385, 21);
            this.btnEco.Name = "btnEco";
            this.btnEco.Size = new System.Drawing.Size(117, 30);
            this.btnEco.TabIndex = 10;
            this.btnEco.Text = "Ecografía";
            this.btnEco.UseVisualStyleBackColor = true;
            this.btnEco.Click += new System.EventHandler(this.btnEco_Click);
            // 
            // btnPat
            // 
            this.btnPat.Location = new System.Drawing.Point(260, 21);
            this.btnPat.Name = "btnPat";
            this.btnPat.Size = new System.Drawing.Size(117, 30);
            this.btnPat.TabIndex = 0;
            this.btnPat.Text = "Patología";
            this.btnPat.UseVisualStyleBackColor = true;
            this.btnPat.Click += new System.EventHandler(this.btnPat_Click);
            // 
            // btnImg
            // 
            this.btnImg.Location = new System.Drawing.Point(135, 21);
            this.btnImg.Name = "btnImg";
            this.btnImg.Size = new System.Drawing.Size(117, 30);
            this.btnImg.TabIndex = 0;
            this.btnImg.Text = "Imágenes";
            this.btnImg.UseVisualStyleBackColor = true;
            this.btnImg.Click += new System.EventHandler(this.btnImg_Click);
            // 
            // btnLab
            // 
            this.btnLab.Location = new System.Drawing.Point(10, 21);
            this.btnLab.Name = "btnLab";
            this.btnLab.Size = new System.Drawing.Size(117, 30);
            this.btnLab.TabIndex = 0;
            this.btnLab.Text = "Laboratorio";
            this.btnLab.UseVisualStyleBackColor = true;
            this.btnLab.Click += new System.EventHandler(this.btnLab_Click);
            // 
            // tbpDiagnostico
            // 
            this.tbpDiagnostico.Controls.Add(this.groupBox20);
            this.tbpDiagnostico.Cursor = System.Windows.Forms.Cursors.Default;
            this.tbpDiagnostico.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tbpDiagnostico.Location = new System.Drawing.Point(61, 4);
            this.tbpDiagnostico.Name = "tbpDiagnostico";
            this.tbpDiagnostico.Padding = new System.Windows.Forms.Padding(3);
            this.tbpDiagnostico.Size = new System.Drawing.Size(1284, 431);
            this.tbpDiagnostico.TabIndex = 4;
            this.tbpDiagnostico.Text = "Diagnóstico (*)";
            this.tbpDiagnostico.UseVisualStyleBackColor = true;
            this.tbpDiagnostico.Enter += new System.EventHandler(this.tbpDiagnostico_Enter);
            this.tbpDiagnostico.Leave += new System.EventHandler(this.tabPage5_Leave);
            // 
            // groupBox20
            // 
            this.groupBox20.Controls.Add(this.txtDxNombre);
            this.groupBox20.Controls.Add(this.lstDiagnosticos);
            this.groupBox20.Controls.Add(this.chkDxPrincipal);
            this.groupBox20.Controls.Add(this.groupBox27);
            this.groupBox20.Controls.Add(this.dgvDiagnosticos);
            this.groupBox20.Controls.Add(this.btnGrabar);
            this.groupBox20.Controls.Add(this.txtDiagObserv);
            this.groupBox20.Controls.Add(this.txtCIECod);
            this.groupBox20.Controls.Add(this.label56);
            this.groupBox20.Controls.Add(this.label54);
            this.groupBox20.Location = new System.Drawing.Point(7, 7);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(1275, 418);
            this.groupBox20.TabIndex = 0;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "Diagnóstico Actual";
            // 
            // txtDxNombre
            // 
            this.txtDxNombre.Location = new System.Drawing.Point(170, 27);
            this.txtDxNombre.Name = "txtDxNombre";
            this.txtDxNombre.Size = new System.Drawing.Size(1099, 20);
            this.txtDxNombre.TabIndex = 9;
            this.txtDxNombre.TextChanged += new System.EventHandler(this.txtDxNombre_TextChanged);
            // 
            // lstDiagnosticos
            // 
            this.lstDiagnosticos.FormattingEnabled = true;
            this.lstDiagnosticos.Location = new System.Drawing.Point(170, 47);
            this.lstDiagnosticos.Name = "lstDiagnosticos";
            this.lstDiagnosticos.Size = new System.Drawing.Size(1099, 30);
            this.lstDiagnosticos.TabIndex = 8;
            this.lstDiagnosticos.Visible = false;
            this.lstDiagnosticos.SelectedIndexChanged += new System.EventHandler(this.lstDiagnosticos_SelectedIndexChanged);
            // 
            // chkDxPrincipal
            // 
            this.chkDxPrincipal.AutoSize = true;
            this.chkDxPrincipal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkDxPrincipal.Location = new System.Drawing.Point(426, 94);
            this.chkDxPrincipal.Name = "chkDxPrincipal";
            this.chkDxPrincipal.Size = new System.Drawing.Size(151, 17);
            this.chkDxPrincipal.TabIndex = 6;
            this.chkDxPrincipal.Text = "Es el Diagnóstico Principal";
            this.chkDxPrincipal.UseVisualStyleBackColor = true;
            // 
            // groupBox27
            // 
            this.groupBox27.Controls.Add(this.rbtPresuntivo);
            this.groupBox27.Controls.Add(this.rbtDefinitivo);
            this.groupBox27.Controls.Add(this.rbtCronico);
            this.groupBox27.Location = new System.Drawing.Point(9, 78);
            this.groupBox27.Name = "groupBox27";
            this.groupBox27.Size = new System.Drawing.Size(389, 42);
            this.groupBox27.TabIndex = 5;
            this.groupBox27.TabStop = false;
            this.groupBox27.Text = "Clasificación";
            // 
            // rbtPresuntivo
            // 
            this.rbtPresuntivo.AutoSize = true;
            this.rbtPresuntivo.Location = new System.Drawing.Point(13, 16);
            this.rbtPresuntivo.Name = "rbtPresuntivo";
            this.rbtPresuntivo.Size = new System.Drawing.Size(75, 17);
            this.rbtPresuntivo.TabIndex = 1;
            this.rbtPresuntivo.TabStop = true;
            this.rbtPresuntivo.Text = "Presuntivo";
            this.rbtPresuntivo.UseVisualStyleBackColor = true;
            // 
            // rbtDefinitivo
            // 
            this.rbtDefinitivo.AutoSize = true;
            this.rbtDefinitivo.Location = new System.Drawing.Point(140, 16);
            this.rbtDefinitivo.Name = "rbtDefinitivo";
            this.rbtDefinitivo.Size = new System.Drawing.Size(69, 17);
            this.rbtDefinitivo.TabIndex = 1;
            this.rbtDefinitivo.TabStop = true;
            this.rbtDefinitivo.Text = "Definitivo";
            this.rbtDefinitivo.UseVisualStyleBackColor = true;
            // 
            // rbtCronico
            // 
            this.rbtCronico.AutoSize = true;
            this.rbtCronico.Location = new System.Drawing.Point(258, 16);
            this.rbtCronico.Name = "rbtCronico";
            this.rbtCronico.Size = new System.Drawing.Size(121, 17);
            this.rbtCronico.TabIndex = 1;
            this.rbtCronico.TabStop = true;
            this.rbtCronico.Text = "Repetitivo o Crónico";
            this.rbtCronico.UseVisualStyleBackColor = true;
            // 
            // dgvDiagnosticos
            // 
            this.dgvDiagnosticos.AllowUserToAddRows = false;
            this.dgvDiagnosticos.AllowUserToDeleteRows = false;
            this.dgvDiagnosticos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDiagnosticos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.btnDelDiagnostico,
            this.btnUpdDiagnostico});
            this.dgvDiagnosticos.Location = new System.Drawing.Point(9, 170);
            this.dgvDiagnosticos.Name = "dgvDiagnosticos";
            this.dgvDiagnosticos.ReadOnly = true;
            this.dgvDiagnosticos.Size = new System.Drawing.Size(1260, 242);
            this.dgvDiagnosticos.TabIndex = 3;
            this.dgvDiagnosticos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDiagnosticos_CellClick);
            this.dgvDiagnosticos.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgvDiagnosticos_CellPainting);
            // 
            // btnDelDiagnostico
            // 
            this.btnDelDiagnostico.HeaderText = "";
            this.btnDelDiagnostico.Name = "btnDelDiagnostico";
            this.btnDelDiagnostico.ReadOnly = true;
            this.btnDelDiagnostico.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.btnDelDiagnostico.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // btnUpdDiagnostico
            // 
            this.btnUpdDiagnostico.HeaderText = "";
            this.btnUpdDiagnostico.Name = "btnUpdDiagnostico";
            this.btnUpdDiagnostico.ReadOnly = true;
            // 
            // btnGrabar
            // 
            this.btnGrabar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGrabar.Location = new System.Drawing.Point(9, 129);
            this.btnGrabar.Name = "btnGrabar";
            this.btnGrabar.Size = new System.Drawing.Size(129, 35);
            this.btnGrabar.TabIndex = 2;
            this.btnGrabar.Text = "Agregar";
            this.btnGrabar.UseVisualStyleBackColor = true;
            this.btnGrabar.Click += new System.EventHandler(this.btnGrabar_Click);
            // 
            // txtDiagObserv
            // 
            this.txtDiagObserv.Location = new System.Drawing.Point(76, 54);
            this.txtDiagObserv.Name = "txtDiagObserv";
            this.txtDiagObserv.Size = new System.Drawing.Size(1193, 20);
            this.txtDiagObserv.TabIndex = 1;
            // 
            // txtCIECod
            // 
            this.txtCIECod.Location = new System.Drawing.Point(76, 27);
            this.txtCIECod.Name = "txtCIECod";
            this.txtCIECod.Size = new System.Drawing.Size(87, 20);
            this.txtCIECod.TabIndex = 1;
            this.txtCIECod.TextChanged += new System.EventHandler(this.txtCIECod_TextChanged);
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(6, 58);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(67, 13);
            this.label56.TabIndex = 0;
            this.label56.Text = "Observación";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(6, 31);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(63, 13);
            this.label54.TabIndex = 0;
            this.label54.Text = "Diagnóstico";
            // 
            // tbpPrescripcion
            // 
            this.tbpPrescripcion.Controls.Add(this.groupBox24);
            this.tbpPrescripcion.Controls.Add(this.groupBox23);
            this.tbpPrescripcion.Location = new System.Drawing.Point(61, 4);
            this.tbpPrescripcion.Name = "tbpPrescripcion";
            this.tbpPrescripcion.Padding = new System.Windows.Forms.Padding(3);
            this.tbpPrescripcion.Size = new System.Drawing.Size(1284, 431);
            this.tbpPrescripcion.TabIndex = 5;
            this.tbpPrescripcion.Text = "Prescripción";
            this.tbpPrescripcion.UseVisualStyleBackColor = true;
            this.tbpPrescripcion.Enter += new System.EventHandler(this.tbpPrescripcion_Enter);
            this.tbpPrescripcion.Leave += new System.EventHandler(this.tabPage6_Leave);
            // 
            // groupBox24
            // 
            this.groupBox24.Controls.Add(this.btnAceptar);
            this.groupBox24.Controls.Add(this.gpbDatos);
            this.groupBox24.Controls.Add(this.rbtPreparados);
            this.groupBox24.Controls.Add(this.dgvPreparados);
            this.groupBox24.Controls.Add(this.rbtLaboratorio);
            this.groupBox24.Controls.Add(this.rbtGenerico);
            this.groupBox24.Controls.Add(this.lstMedicinasFiltro);
            this.groupBox24.Controls.Add(this.cmbCriterio);
            this.groupBox24.Controls.Add(this.lblCriterio);
            this.groupBox24.Location = new System.Drawing.Point(758, 7);
            this.groupBox24.Name = "groupBox24";
            this.groupBox24.Size = new System.Drawing.Size(520, 418);
            this.groupBox24.TabIndex = 8;
            this.groupBox24.TabStop = false;
            this.groupBox24.Text = "Criterios de Búsqueda";
            // 
            // btnAceptar
            // 
            this.btnAceptar.Location = new System.Drawing.Point(381, 383);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(133, 23);
            this.btnAceptar.TabIndex = 11;
            this.btnAceptar.Text = "Grabar Fórmula";
            this.btnAceptar.UseVisualStyleBackColor = true;
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // gpbDatos
            // 
            this.gpbDatos.Controls.Add(this.btnAgregarComponente);
            this.gpbDatos.Controls.Add(this.label3);
            this.gpbDatos.Controls.Add(this.rbtElementoBase);
            this.gpbDatos.Controls.Add(this.rbtPrincActivo);
            this.gpbDatos.Controls.Add(this.lblUndElemBase);
            this.gpbDatos.Controls.Add(this.txtCantElemBase);
            this.gpbDatos.Controls.Add(this.lblCantidad);
            this.gpbDatos.Controls.Add(this.cmbComponente);
            this.gpbDatos.Controls.Add(this.btnAddCompuesto);
            this.gpbDatos.Location = new System.Drawing.Point(6, 82);
            this.gpbDatos.Name = "gpbDatos";
            this.gpbDatos.Size = new System.Drawing.Size(508, 112);
            this.gpbDatos.TabIndex = 21;
            this.gpbDatos.TabStop = false;
            this.gpbDatos.Visible = false;
            // 
            // btnAgregarComponente
            // 
            this.btnAgregarComponente.Location = new System.Drawing.Point(7, 83);
            this.btnAgregarComponente.Name = "btnAgregarComponente";
            this.btnAgregarComponente.Size = new System.Drawing.Size(181, 23);
            this.btnAgregarComponente.TabIndex = 14;
            this.btnAgregarComponente.Text = "Agregar Componentes";
            this.btnAgregarComponente.UseVisualStyleBackColor = true;
            this.btnAgregarComponente.Click += new System.EventHandler(this.btnAgregarComponente_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Componente";
            // 
            // rbtElementoBase
            // 
            this.rbtElementoBase.AutoSize = true;
            this.rbtElementoBase.Location = new System.Drawing.Point(211, 14);
            this.rbtElementoBase.Name = "rbtElementoBase";
            this.rbtElementoBase.Size = new System.Drawing.Size(96, 17);
            this.rbtElementoBase.TabIndex = 12;
            this.rbtElementoBase.TabStop = true;
            this.rbtElementoBase.Text = "Elemento Base";
            this.rbtElementoBase.UseVisualStyleBackColor = true;
            this.rbtElementoBase.CheckedChanged += new System.EventHandler(this.rbtElementoBase_CheckedChanged);
            // 
            // rbtPrincActivo
            // 
            this.rbtPrincActivo.AutoSize = true;
            this.rbtPrincActivo.Location = new System.Drawing.Point(16, 14);
            this.rbtPrincActivo.Name = "rbtPrincActivo";
            this.rbtPrincActivo.Size = new System.Drawing.Size(98, 17);
            this.rbtPrincActivo.TabIndex = 11;
            this.rbtPrincActivo.TabStop = true;
            this.rbtPrincActivo.Text = "Principio Activo";
            this.rbtPrincActivo.UseVisualStyleBackColor = true;
            this.rbtPrincActivo.CheckedChanged += new System.EventHandler(this.rbtPrincActivo_CheckedChanged);
            // 
            // lblUndElemBase
            // 
            this.lblUndElemBase.AutoSize = true;
            this.lblUndElemBase.Location = new System.Drawing.Point(468, 49);
            this.lblUndElemBase.Name = "lblUndElemBase";
            this.lblUndElemBase.Size = new System.Drawing.Size(30, 13);
            this.lblUndElemBase.TabIndex = 10;
            this.lblUndElemBase.Text = "Und.";
            // 
            // txtCantElemBase
            // 
            this.txtCantElemBase.Enabled = false;
            this.txtCantElemBase.Location = new System.Drawing.Point(400, 45);
            this.txtCantElemBase.Name = "txtCantElemBase";
            this.txtCantElemBase.Size = new System.Drawing.Size(63, 20);
            this.txtCantElemBase.TabIndex = 9;
            this.txtCantElemBase.TextChanged += new System.EventHandler(this.txtCantElemBase_TextChanged);
            // 
            // lblCantidad
            // 
            this.lblCantidad.AutoSize = true;
            this.lblCantidad.Location = new System.Drawing.Point(364, 49);
            this.lblCantidad.Name = "lblCantidad";
            this.lblCantidad.Size = new System.Drawing.Size(32, 13);
            this.lblCantidad.TabIndex = 8;
            this.lblCantidad.Text = "Cant.";
            // 
            // cmbComponente
            // 
            this.cmbComponente.Enabled = false;
            this.cmbComponente.FormattingEnabled = true;
            this.cmbComponente.Location = new System.Drawing.Point(86, 45);
            this.cmbComponente.Name = "cmbComponente";
            this.cmbComponente.Size = new System.Drawing.Size(271, 21);
            this.cmbComponente.TabIndex = 4;
            this.cmbComponente.SelectedIndexChanged += new System.EventHandler(this.cmbComponente_SelectedIndexChanged);
            // 
            // btnAddCompuesto
            // 
            this.btnAddCompuesto.Location = new System.Drawing.Point(320, 83);
            this.btnAddCompuesto.Name = "btnAddCompuesto";
            this.btnAddCompuesto.Size = new System.Drawing.Size(181, 23);
            this.btnAddCompuesto.TabIndex = 0;
            this.btnAddCompuesto.Text = "Grabar";
            this.btnAddCompuesto.UseVisualStyleBackColor = true;
            this.btnAddCompuesto.Click += new System.EventHandler(this.btnAddCompuesto_Click);
            // 
            // rbtPreparados
            // 
            this.rbtPreparados.AutoSize = true;
            this.rbtPreparados.Location = new System.Drawing.Point(402, 22);
            this.rbtPreparados.Name = "rbtPreparados";
            this.rbtPreparados.Size = new System.Drawing.Size(107, 17);
            this.rbtPreparados.TabIndex = 20;
            this.rbtPreparados.TabStop = true;
            this.rbtPreparados.Text = "Fórmula Magistral";
            this.rbtPreparados.UseVisualStyleBackColor = true;
            this.rbtPreparados.CheckedChanged += new System.EventHandler(this.rbtPreparados_CheckedChanged);
            // 
            // dgvPreparados
            // 
            this.dgvPreparados.AllowUserToAddRows = false;
            this.dgvPreparados.AllowUserToDeleteRows = false;
            this.dgvPreparados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPreparados.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.btnDelCompuesto,
            this.btnUpdCompuesto});
            this.dgvPreparados.Location = new System.Drawing.Point(6, 200);
            this.dgvPreparados.Name = "dgvPreparados";
            this.dgvPreparados.ReadOnly = true;
            this.dgvPreparados.Size = new System.Drawing.Size(508, 176);
            this.dgvPreparados.TabIndex = 19;
            this.dgvPreparados.Visible = false;
            this.dgvPreparados.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPreparados_CellClick);
            this.dgvPreparados.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgvPreparados_CellPainting);
            // 
            // btnDelCompuesto
            // 
            this.btnDelCompuesto.HeaderText = "";
            this.btnDelCompuesto.Name = "btnDelCompuesto";
            this.btnDelCompuesto.ReadOnly = true;
            // 
            // btnUpdCompuesto
            // 
            this.btnUpdCompuesto.HeaderText = "";
            this.btnUpdCompuesto.Name = "btnUpdCompuesto";
            this.btnUpdCompuesto.ReadOnly = true;
            // 
            // rbtLaboratorio
            // 
            this.rbtLaboratorio.AutoSize = true;
            this.rbtLaboratorio.Location = new System.Drawing.Point(217, 22);
            this.rbtLaboratorio.Name = "rbtLaboratorio";
            this.rbtLaboratorio.Size = new System.Drawing.Size(78, 17);
            this.rbtLaboratorio.TabIndex = 18;
            this.rbtLaboratorio.TabStop = true;
            this.rbtLaboratorio.Text = "Laboratorio";
            this.rbtLaboratorio.UseVisualStyleBackColor = true;
            this.rbtLaboratorio.CheckedChanged += new System.EventHandler(this.rbtLaboratorio_CheckedChanged);
            // 
            // rbtGenerico
            // 
            this.rbtGenerico.AutoSize = true;
            this.rbtGenerico.Location = new System.Drawing.Point(22, 22);
            this.rbtGenerico.Name = "rbtGenerico";
            this.rbtGenerico.Size = new System.Drawing.Size(98, 17);
            this.rbtGenerico.TabIndex = 17;
            this.rbtGenerico.TabStop = true;
            this.rbtGenerico.Text = "Principio Activo";
            this.rbtGenerico.UseVisualStyleBackColor = true;
            this.rbtGenerico.CheckedChanged += new System.EventHandler(this.rbtGenerico_CheckedChanged);
            // 
            // lstMedicinasFiltro
            // 
            this.lstMedicinasFiltro.FormattingEnabled = true;
            this.lstMedicinasFiltro.Location = new System.Drawing.Point(8, 39);
            this.lstMedicinasFiltro.Name = "lstMedicinasFiltro";
            this.lstMedicinasFiltro.Size = new System.Drawing.Size(203, 17);
            this.lstMedicinasFiltro.TabIndex = 16;
            this.lstMedicinasFiltro.Visible = false;
            this.lstMedicinasFiltro.SelectedIndexChanged += new System.EventHandler(this.lstMedicinasFiltro_SelectedIndexChanged);
            // 
            // cmbCriterio
            // 
            this.cmbCriterio.FormattingEnabled = true;
            this.cmbCriterio.Location = new System.Drawing.Point(106, 55);
            this.cmbCriterio.Name = "cmbCriterio";
            this.cmbCriterio.Size = new System.Drawing.Size(408, 21);
            this.cmbCriterio.TabIndex = 9;
            this.cmbCriterio.SelectedValueChanged += new System.EventHandler(this.cmbCriterio_SelectedValueChanged);
            // 
            // lblCriterio
            // 
            this.lblCriterio.Location = new System.Drawing.Point(13, 59);
            this.lblCriterio.Name = "lblCriterio";
            this.lblCriterio.Size = new System.Drawing.Size(90, 13);
            this.lblCriterio.TabIndex = 8;
            this.lblCriterio.Text = "Criterio";
            this.lblCriterio.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // groupBox23
            // 
            this.groupBox23.Controls.Add(this.label22);
            this.groupBox23.Controls.Add(this.lblRpCaduca);
            this.groupBox23.Controls.Add(this.cmbVigencia);
            this.groupBox23.Controls.Add(this.label24);
            this.groupBox23.Controls.Add(this.lstMedicinas);
            this.groupBox23.Controls.Add(this.txtPrdId);
            this.groupBox23.Controls.Add(this.txtPrdDescripcion);
            this.groupBox23.Controls.Add(this.btnPresc);
            this.groupBox23.Controls.Add(this.btnRepiteRx);
            this.groupBox23.Controls.Add(this.dgvPrescripcion);
            this.groupBox23.Controls.Add(this.btnAddPresc);
            this.groupBox23.Controls.Add(this.txtIndicaciones);
            this.groupBox23.Controls.Add(this.label61);
            this.groupBox23.Controls.Add(this.label60);
            this.groupBox23.Location = new System.Drawing.Point(7, 7);
            this.groupBox23.Name = "groupBox23";
            this.groupBox23.Size = new System.Drawing.Size(747, 418);
            this.groupBox23.TabIndex = 0;
            this.groupBox23.TabStop = false;
            this.groupBox23.Text = "Prescripción";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Red;
            this.label22.Location = new System.Drawing.Point(6, 394);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(13, 16);
            this.label22.TabIndex = 29;
            this.label22.Text = "*";
            // 
            // lblRpCaduca
            // 
            this.lblRpCaduca.AutoSize = true;
            this.lblRpCaduca.ForeColor = System.Drawing.Color.Blue;
            this.lblRpCaduca.Location = new System.Drawing.Point(336, 393);
            this.lblRpCaduca.Name = "lblRpCaduca";
            this.lblRpCaduca.Size = new System.Drawing.Size(135, 13);
            this.lblRpCaduca.TabIndex = 28;
            this.lblRpCaduca.Text = "CADUCA EL: dd/mm/aaaa";
            // 
            // cmbVigencia
            // 
            this.cmbVigencia.FormattingEnabled = true;
            this.cmbVigencia.Location = new System.Drawing.Point(140, 389);
            this.cmbVigencia.Name = "cmbVigencia";
            this.cmbVigencia.Size = new System.Drawing.Size(167, 21);
            this.cmbVigencia.TabIndex = 26;
            this.cmbVigencia.SelectedIndexChanged += new System.EventHandler(this.cmbVigencia_SelectedIndexChanged);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.ForeColor = System.Drawing.Color.Red;
            this.label24.Location = new System.Drawing.Point(17, 393);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(112, 13);
            this.label24.TabIndex = 25;
            this.label24.Text = "Vigencia de la Receta";
            // 
            // lstMedicinas
            // 
            this.lstMedicinas.FormattingEnabled = true;
            this.lstMedicinas.Location = new System.Drawing.Point(649, 19);
            this.lstMedicinas.Name = "lstMedicinas";
            this.lstMedicinas.Size = new System.Drawing.Size(90, 17);
            this.lstMedicinas.TabIndex = 15;
            this.lstMedicinas.Visible = false;
            this.lstMedicinas.SelectedIndexChanged += new System.EventHandler(this.lstMedicinas_SelectedIndexChanged);
            // 
            // txtPrdId
            // 
            this.txtPrdId.Location = new System.Drawing.Point(578, 18);
            this.txtPrdId.Name = "txtPrdId";
            this.txtPrdId.Size = new System.Drawing.Size(65, 20);
            this.txtPrdId.TabIndex = 16;
            this.txtPrdId.Visible = false;
            // 
            // txtPrdDescripcion
            // 
            this.txtPrdDescripcion.Location = new System.Drawing.Point(105, 18);
            this.txtPrdDescripcion.Name = "txtPrdDescripcion";
            this.txtPrdDescripcion.Size = new System.Drawing.Size(467, 20);
            this.txtPrdDescripcion.TabIndex = 13;
            this.txtPrdDescripcion.TextChanged += new System.EventHandler(this.txtPrdDescripcion_TextChanged);
            // 
            // btnPresc
            // 
            this.btnPresc.BackColor = System.Drawing.Color.Crimson;
            this.btnPresc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPresc.ForeColor = System.Drawing.Color.White;
            this.btnPresc.Location = new System.Drawing.Point(578, 131);
            this.btnPresc.Name = "btnPresc";
            this.btnPresc.Size = new System.Drawing.Size(163, 35);
            this.btnPresc.TabIndex = 11;
            this.btnPresc.Text = "Prescripciones Anteriores";
            this.btnPresc.UseVisualStyleBackColor = false;
            this.btnPresc.Click += new System.EventHandler(this.btnPresc_Click);
            // 
            // btnRepiteRx
            // 
            this.btnRepiteRx.Location = new System.Drawing.Point(578, 89);
            this.btnRepiteRx.Name = "btnRepiteRx";
            this.btnRepiteRx.Size = new System.Drawing.Size(163, 30);
            this.btnRepiteRx.TabIndex = 10;
            this.btnRepiteRx.Text = "Repetir Receta";
            this.btnRepiteRx.UseVisualStyleBackColor = true;
            this.btnRepiteRx.Click += new System.EventHandler(this.btnRepiteRx_Click);
            // 
            // dgvPrescripcion
            // 
            this.dgvPrescripcion.AllowUserToAddRows = false;
            this.dgvPrescripcion.AllowUserToDeleteRows = false;
            this.dgvPrescripcion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPrescripcion.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.btnDelMedicinas,
            this.btnUpdMedicinas});
            this.dgvPrescripcion.Location = new System.Drawing.Point(6, 178);
            this.dgvPrescripcion.Name = "dgvPrescripcion";
            this.dgvPrescripcion.ReadOnly = true;
            this.dgvPrescripcion.Size = new System.Drawing.Size(735, 198);
            this.dgvPrescripcion.TabIndex = 6;
            this.dgvPrescripcion.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPrescripcion_CellClick);
            this.dgvPrescripcion.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgvPrescripcion_CellPainting);
            // 
            // btnDelMedicinas
            // 
            this.btnDelMedicinas.HeaderText = "";
            this.btnDelMedicinas.Name = "btnDelMedicinas";
            this.btnDelMedicinas.ReadOnly = true;
            // 
            // btnUpdMedicinas
            // 
            this.btnUpdMedicinas.HeaderText = "";
            this.btnUpdMedicinas.Name = "btnUpdMedicinas";
            this.btnUpdMedicinas.ReadOnly = true;
            // 
            // btnAddPresc
            // 
            this.btnAddPresc.Location = new System.Drawing.Point(578, 47);
            this.btnAddPresc.Name = "btnAddPresc";
            this.btnAddPresc.Size = new System.Drawing.Size(163, 30);
            this.btnAddPresc.TabIndex = 5;
            this.btnAddPresc.Text = "Agregar";
            this.btnAddPresc.UseVisualStyleBackColor = true;
            this.btnAddPresc.Click += new System.EventHandler(this.btnAddPresc_Click);
            // 
            // txtIndicaciones
            // 
            this.txtIndicaciones.Location = new System.Drawing.Point(105, 48);
            this.txtIndicaciones.Multiline = true;
            this.txtIndicaciones.Name = "txtIndicaciones";
            this.txtIndicaciones.Size = new System.Drawing.Size(467, 118);
            this.txtIndicaciones.TabIndex = 4;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(32, 48);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(67, 13);
            this.label61.TabIndex = 2;
            this.label61.Text = "Indicaciones";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(6, 22);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(93, 13);
            this.label60.TabIndex = 2;
            this.label60.Text = "Nombre Comercial";
            // 
            // tbpComplementarios
            // 
            this.tbpComplementarios.BackColor = System.Drawing.Color.Transparent;
            this.tbpComplementarios.Controls.Add(this.groupBox43);
            this.tbpComplementarios.Controls.Add(this.groupBox42);
            this.tbpComplementarios.Location = new System.Drawing.Point(61, 4);
            this.tbpComplementarios.Name = "tbpComplementarios";
            this.tbpComplementarios.Padding = new System.Windows.Forms.Padding(3);
            this.tbpComplementarios.Size = new System.Drawing.Size(1284, 431);
            this.tbpComplementarios.TabIndex = 6;
            this.tbpComplementarios.Text = "Datos Complementarios";
            this.tbpComplementarios.Leave += new System.EventHandler(this.tabPage7_Leave);
            // 
            // groupBox43
            // 
            this.groupBox43.Controls.Add(this.txtObservaciones);
            this.groupBox43.Location = new System.Drawing.Point(641, 8);
            this.groupBox43.Name = "groupBox43";
            this.groupBox43.Size = new System.Drawing.Size(643, 417);
            this.groupBox43.TabIndex = 1;
            this.groupBox43.TabStop = false;
            this.groupBox43.Text = "Observaciones Generales";
            // 
            // txtObservaciones
            // 
            this.txtObservaciones.Location = new System.Drawing.Point(7, 20);
            this.txtObservaciones.Multiline = true;
            this.txtObservaciones.Name = "txtObservaciones";
            this.txtObservaciones.Size = new System.Drawing.Size(630, 391);
            this.txtObservaciones.TabIndex = 1;
            // 
            // groupBox42
            // 
            this.groupBox42.Controls.Add(this.txtRecomendaciones);
            this.groupBox42.Controls.Add(this.groupBox41);
            this.groupBox42.Location = new System.Drawing.Point(6, 8);
            this.groupBox42.Name = "groupBox42";
            this.groupBox42.Size = new System.Drawing.Size(629, 417);
            this.groupBox42.TabIndex = 1;
            this.groupBox42.TabStop = false;
            this.groupBox42.Text = "Recomendaciones";
            // 
            // txtRecomendaciones
            // 
            this.txtRecomendaciones.Location = new System.Drawing.Point(7, 20);
            this.txtRecomendaciones.Multiline = true;
            this.txtRecomendaciones.Name = "txtRecomendaciones";
            this.txtRecomendaciones.Size = new System.Drawing.Size(612, 335);
            this.txtRecomendaciones.TabIndex = 0;
            // 
            // groupBox41
            // 
            this.groupBox41.Controls.Add(this.label17);
            this.groupBox41.Controls.Add(this.dtpProximaCita);
            this.groupBox41.Location = new System.Drawing.Point(7, 361);
            this.groupBox41.Name = "groupBox41";
            this.groupBox41.Size = new System.Drawing.Size(612, 50);
            this.groupBox41.TabIndex = 0;
            this.groupBox41.TabStop = false;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(10, 21);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(98, 13);
            this.label17.TabIndex = 1;
            this.label17.Text = "Fecha Próxima Cita";
            // 
            // dtpProximaCita
            // 
            this.dtpProximaCita.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpProximaCita.Location = new System.Drawing.Point(118, 17);
            this.dtpProximaCita.Name = "dtpProximaCita";
            this.dtpProximaCita.Size = new System.Drawing.Size(100, 20);
            this.dtpProximaCita.TabIndex = 0;
            // 
            // tbpConsultaAnterior
            // 
            this.tbpConsultaAnterior.BackColor = System.Drawing.Color.AliceBlue;
            this.tbpConsultaAnterior.Controls.Add(this.groupBox31);
            this.tbpConsultaAnterior.Controls.Add(this.groupBox30);
            this.tbpConsultaAnterior.Controls.Add(this.groupBox29);
            this.tbpConsultaAnterior.Location = new System.Drawing.Point(61, 4);
            this.tbpConsultaAnterior.Name = "tbpConsultaAnterior";
            this.tbpConsultaAnterior.Padding = new System.Windows.Forms.Padding(3);
            this.tbpConsultaAnterior.Size = new System.Drawing.Size(1284, 431);
            this.tbpConsultaAnterior.TabIndex = 7;
            this.tbpConsultaAnterior.Text = "Resumen de la Consulta Anterior";
            this.tbpConsultaAnterior.Enter += new System.EventHandler(this.tbpConsultaAnterior_Enter);
            // 
            // groupBox31
            // 
            this.groupBox31.Controls.Add(this.tabControl3);
            this.groupBox31.Location = new System.Drawing.Point(351, 5);
            this.groupBox31.Name = "groupBox31";
            this.groupBox31.Size = new System.Drawing.Size(933, 424);
            this.groupBox31.TabIndex = 6;
            this.groupBox31.TabStop = false;
            // 
            // tabControl3
            // 
            this.tabControl3.Controls.Add(this.tabPage3);
            this.tabControl3.Controls.Add(this.tabPage4);
            this.tabControl3.Controls.Add(this.tabPage5);
            this.tabControl3.Location = new System.Drawing.Point(6, 15);
            this.tabControl3.Name = "tabControl3";
            this.tabControl3.SelectedIndex = 0;
            this.tabControl3.Size = new System.Drawing.Size(921, 403);
            this.tabControl3.TabIndex = 1;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.AliceBlue;
            this.tabPage3.Controls.Add(this.groupBox7);
            this.tabPage3.Controls.Add(this.groupBox5);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(913, 377);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Diagnóstico / Prescripción";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.dgvPrescCA);
            this.groupBox7.Location = new System.Drawing.Point(6, 199);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(901, 172);
            this.groupBox7.TabIndex = 1;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Prescripción";
            // 
            // dgvPrescCA
            // 
            this.dgvPrescCA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPrescCA.Enabled = false;
            this.dgvPrescCA.Location = new System.Drawing.Point(6, 22);
            this.dgvPrescCA.Name = "dgvPrescCA";
            this.dgvPrescCA.Size = new System.Drawing.Size(888, 143);
            this.dgvPrescCA.TabIndex = 1;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.dgvDiagCA);
            this.groupBox5.Location = new System.Drawing.Point(6, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(901, 172);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Diagnóstico";
            // 
            // dgvDiagCA
            // 
            this.dgvDiagCA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDiagCA.Enabled = false;
            this.dgvDiagCA.Location = new System.Drawing.Point(6, 19);
            this.dgvDiagCA.Name = "dgvDiagCA";
            this.dgvDiagCA.Size = new System.Drawing.Size(888, 143);
            this.dgvDiagCA.TabIndex = 0;
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.Color.AliceBlue;
            this.tabPage4.Controls.Add(this.btnEcoCA);
            this.tabPage4.Controls.Add(this.btnProcCA);
            this.tabPage4.Controls.Add(this.dgvExamCA);
            this.tabPage4.Controls.Add(this.btnPatCA);
            this.tabPage4.Controls.Add(this.btnImgCA);
            this.tabPage4.Controls.Add(this.btnLabCA);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(913, 377);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Plan de Trabajo";
            // 
            // btnEcoCA
            // 
            this.btnEcoCA.Location = new System.Drawing.Point(26, 230);
            this.btnEcoCA.Name = "btnEcoCA";
            this.btnEcoCA.Size = new System.Drawing.Size(146, 63);
            this.btnEcoCA.TabIndex = 9;
            this.btnEcoCA.Text = "Ecografías";
            this.btnEcoCA.UseVisualStyleBackColor = true;
            this.btnEcoCA.Click += new System.EventHandler(this.btnEcoCA_Click);
            // 
            // btnProcCA
            // 
            this.btnProcCA.Location = new System.Drawing.Point(26, 303);
            this.btnProcCA.Name = "btnProcCA";
            this.btnProcCA.Size = new System.Drawing.Size(146, 63);
            this.btnProcCA.TabIndex = 8;
            this.btnProcCA.Text = "Tratamientos";
            this.btnProcCA.UseVisualStyleBackColor = true;
            this.btnProcCA.Click += new System.EventHandler(this.btnProcCA_Click);
            // 
            // dgvExamCA
            // 
            this.dgvExamCA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvExamCA.Location = new System.Drawing.Point(199, 6);
            this.dgvExamCA.Name = "dgvExamCA";
            this.dgvExamCA.Size = new System.Drawing.Size(708, 365);
            this.dgvExamCA.TabIndex = 7;
            // 
            // btnPatCA
            // 
            this.btnPatCA.Location = new System.Drawing.Point(26, 157);
            this.btnPatCA.Name = "btnPatCA";
            this.btnPatCA.Size = new System.Drawing.Size(146, 63);
            this.btnPatCA.TabIndex = 6;
            this.btnPatCA.Text = "Patología";
            this.btnPatCA.UseVisualStyleBackColor = true;
            this.btnPatCA.Click += new System.EventHandler(this.btnPatCA_Click);
            // 
            // btnImgCA
            // 
            this.btnImgCA.Location = new System.Drawing.Point(26, 84);
            this.btnImgCA.Name = "btnImgCA";
            this.btnImgCA.Size = new System.Drawing.Size(146, 63);
            this.btnImgCA.TabIndex = 5;
            this.btnImgCA.Text = "Imágenes";
            this.btnImgCA.UseVisualStyleBackColor = true;
            this.btnImgCA.Click += new System.EventHandler(this.btnImgCA_Click);
            // 
            // btnLabCA
            // 
            this.btnLabCA.Location = new System.Drawing.Point(26, 11);
            this.btnLabCA.Name = "btnLabCA";
            this.btnLabCA.Size = new System.Drawing.Size(146, 63);
            this.btnLabCA.TabIndex = 4;
            this.btnLabCA.Text = "Laboratorio";
            this.btnLabCA.UseVisualStyleBackColor = true;
            this.btnLabCA.Click += new System.EventHandler(this.btnLabCA_Click);
            // 
            // tabPage5
            // 
            this.tabPage5.BackColor = System.Drawing.Color.AliceBlue;
            this.tabPage5.Controls.Add(this.groupBox38);
            this.tabPage5.Controls.Add(this.groupBox37);
            this.tabPage5.Controls.Add(this.groupBox36);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(913, 377);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Antecedentes";
            // 
            // groupBox38
            // 
            this.groupBox38.Controls.Add(this.dgvFamCA);
            this.groupBox38.Location = new System.Drawing.Point(606, 5);
            this.groupBox38.Name = "groupBox38";
            this.groupBox38.Size = new System.Drawing.Size(304, 363);
            this.groupBox38.TabIndex = 1;
            this.groupBox38.TabStop = false;
            this.groupBox38.Text = "Familiares";
            // 
            // dgvFamCA
            // 
            this.dgvFamCA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFamCA.Location = new System.Drawing.Point(8, 19);
            this.dgvFamCA.Name = "dgvFamCA";
            this.dgvFamCA.Size = new System.Drawing.Size(288, 338);
            this.dgvFamCA.TabIndex = 0;
            // 
            // groupBox37
            // 
            this.groupBox37.Controls.Add(this.dgvQuirCA);
            this.groupBox37.Location = new System.Drawing.Point(306, 5);
            this.groupBox37.Name = "groupBox37";
            this.groupBox37.Size = new System.Drawing.Size(304, 363);
            this.groupBox37.TabIndex = 2;
            this.groupBox37.TabStop = false;
            this.groupBox37.Text = "Quirúrgicos";
            // 
            // dgvQuirCA
            // 
            this.dgvQuirCA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvQuirCA.Location = new System.Drawing.Point(8, 19);
            this.dgvQuirCA.Name = "dgvQuirCA";
            this.dgvQuirCA.Size = new System.Drawing.Size(288, 338);
            this.dgvQuirCA.TabIndex = 0;
            // 
            // groupBox36
            // 
            this.groupBox36.Controls.Add(this.dgvPatCA);
            this.groupBox36.Location = new System.Drawing.Point(6, 5);
            this.groupBox36.Name = "groupBox36";
            this.groupBox36.Size = new System.Drawing.Size(304, 363);
            this.groupBox36.TabIndex = 3;
            this.groupBox36.TabStop = false;
            this.groupBox36.Text = "Patológicos";
            // 
            // dgvPatCA
            // 
            this.dgvPatCA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPatCA.Location = new System.Drawing.Point(6, 19);
            this.dgvPatCA.Name = "dgvPatCA";
            this.dgvPatCA.Size = new System.Drawing.Size(288, 338);
            this.dgvPatCA.TabIndex = 0;
            // 
            // groupBox30
            // 
            this.groupBox30.Controls.Add(this.groupBox35);
            this.groupBox30.Controls.Add(this.txtMotivoEvolCA);
            this.groupBox30.Location = new System.Drawing.Point(4, 122);
            this.groupBox30.Name = "groupBox30";
            this.groupBox30.Size = new System.Drawing.Size(341, 303);
            this.groupBox30.TabIndex = 5;
            this.groupBox30.TabStop = false;
            this.groupBox30.Text = "Mot. de la Consulta / Evoluc. del Paciente";
            // 
            // groupBox35
            // 
            this.groupBox35.Controls.Add(this.groupBox39);
            this.groupBox35.Controls.Add(this.label66);
            this.groupBox35.Controls.Add(this.txtEspecHabCA);
            this.groupBox35.Enabled = false;
            this.groupBox35.Location = new System.Drawing.Point(7, 153);
            this.groupBox35.Name = "groupBox35";
            this.groupBox35.Size = new System.Drawing.Size(329, 144);
            this.groupBox35.TabIndex = 4;
            this.groupBox35.TabStop = false;
            this.groupBox35.Text = "Hábitos Nocivos";
            // 
            // groupBox39
            // 
            this.groupBox39.Controls.Add(this.chkOtrosHabCA);
            this.groupBox39.Controls.Add(this.chkDrogasCA);
            this.groupBox39.Controls.Add(this.chkTabacoCA);
            this.groupBox39.Controls.Add(this.chkAlcoholCA);
            this.groupBox39.Location = new System.Drawing.Point(9, 12);
            this.groupBox39.Name = "groupBox39";
            this.groupBox39.Size = new System.Drawing.Size(315, 27);
            this.groupBox39.TabIndex = 11;
            this.groupBox39.TabStop = false;
            // 
            // chkOtrosHabCA
            // 
            this.chkOtrosHabCA.AutoSize = true;
            this.chkOtrosHabCA.Location = new System.Drawing.Point(237, 8);
            this.chkOtrosHabCA.Name = "chkOtrosHabCA";
            this.chkOtrosHabCA.Size = new System.Drawing.Size(51, 17);
            this.chkOtrosHabCA.TabIndex = 0;
            this.chkOtrosHabCA.Text = "Otros";
            this.chkOtrosHabCA.UseVisualStyleBackColor = true;
            // 
            // chkDrogasCA
            // 
            this.chkDrogasCA.AutoSize = true;
            this.chkDrogasCA.Location = new System.Drawing.Point(161, 8);
            this.chkDrogasCA.Name = "chkDrogasCA";
            this.chkDrogasCA.Size = new System.Drawing.Size(60, 17);
            this.chkDrogasCA.TabIndex = 0;
            this.chkDrogasCA.Text = "Drogas";
            this.chkDrogasCA.UseVisualStyleBackColor = true;
            // 
            // chkTabacoCA
            // 
            this.chkTabacoCA.AutoSize = true;
            this.chkTabacoCA.Location = new System.Drawing.Point(82, 8);
            this.chkTabacoCA.Name = "chkTabacoCA";
            this.chkTabacoCA.Size = new System.Drawing.Size(63, 17);
            this.chkTabacoCA.TabIndex = 0;
            this.chkTabacoCA.Text = "Tabaco";
            this.chkTabacoCA.UseVisualStyleBackColor = true;
            // 
            // chkAlcoholCA
            // 
            this.chkAlcoholCA.AutoSize = true;
            this.chkAlcoholCA.Location = new System.Drawing.Point(5, 8);
            this.chkAlcoholCA.Name = "chkAlcoholCA";
            this.chkAlcoholCA.Size = new System.Drawing.Size(61, 17);
            this.chkAlcoholCA.TabIndex = 0;
            this.chkAlcoholCA.Text = "Alcohol";
            this.chkAlcoholCA.UseVisualStyleBackColor = true;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(6, 42);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(59, 13);
            this.label66.TabIndex = 9;
            this.label66.Text = "Especificar";
            // 
            // txtEspecHabCA
            // 
            this.txtEspecHabCA.Enabled = false;
            this.txtEspecHabCA.Location = new System.Drawing.Point(5, 61);
            this.txtEspecHabCA.Multiline = true;
            this.txtEspecHabCA.Name = "txtEspecHabCA";
            this.txtEspecHabCA.Size = new System.Drawing.Size(319, 77);
            this.txtEspecHabCA.TabIndex = 8;
            // 
            // txtMotivoEvolCA
            // 
            this.txtMotivoEvolCA.Enabled = false;
            this.txtMotivoEvolCA.Location = new System.Drawing.Point(7, 20);
            this.txtMotivoEvolCA.Multiline = true;
            this.txtMotivoEvolCA.Name = "txtMotivoEvolCA";
            this.txtMotivoEvolCA.Size = new System.Drawing.Size(324, 127);
            this.txtMotivoEvolCA.TabIndex = 0;
            // 
            // groupBox29
            // 
            this.groupBox29.Controls.Add(this.txtTipoAtencionCA);
            this.groupBox29.Controls.Add(this.txtHoraCA);
            this.groupBox29.Controls.Add(this.txtFechaCA);
            this.groupBox29.Controls.Add(this.label65);
            this.groupBox29.Controls.Add(this.label58);
            this.groupBox29.Controls.Add(this.label57);
            this.groupBox29.Location = new System.Drawing.Point(4, 5);
            this.groupBox29.Name = "groupBox29";
            this.groupBox29.Size = new System.Drawing.Size(341, 111);
            this.groupBox29.TabIndex = 4;
            this.groupBox29.TabStop = false;
            this.groupBox29.Text = "Consulta Anterior";
            // 
            // txtTipoAtencionCA
            // 
            this.txtTipoAtencionCA.Enabled = false;
            this.txtTipoAtencionCA.Location = new System.Drawing.Point(111, 29);
            this.txtTipoAtencionCA.Name = "txtTipoAtencionCA";
            this.txtTipoAtencionCA.Size = new System.Drawing.Size(220, 20);
            this.txtTipoAtencionCA.TabIndex = 1;
            // 
            // txtHoraCA
            // 
            this.txtHoraCA.Enabled = false;
            this.txtHoraCA.Location = new System.Drawing.Point(111, 81);
            this.txtHoraCA.Name = "txtHoraCA";
            this.txtHoraCA.Size = new System.Drawing.Size(110, 20);
            this.txtHoraCA.TabIndex = 1;
            // 
            // txtFechaCA
            // 
            this.txtFechaCA.Enabled = false;
            this.txtFechaCA.Location = new System.Drawing.Point(111, 55);
            this.txtFechaCA.Name = "txtFechaCA";
            this.txtFechaCA.Size = new System.Drawing.Size(110, 20);
            this.txtFechaCA.TabIndex = 1;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(6, 33);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(88, 13);
            this.label65.TabIndex = 0;
            this.label65.Text = "Tipo de Atención";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(64, 85);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(30, 13);
            this.label58.TabIndex = 0;
            this.label58.Text = "Hora";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(57, 59);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(37, 13);
            this.label57.TabIndex = 0;
            this.label57.Text = "Fecha";
            // 
            // btnCerrar
            // 
            this.btnCerrar.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.btnCerrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCerrar.ForeColor = System.Drawing.Color.White;
            this.btnCerrar.Location = new System.Drawing.Point(1016, 658);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(165, 32);
            this.btnCerrar.TabIndex = 4;
            this.btnCerrar.Text = "Cerrar Ficha";
            this.btnCerrar.UseVisualStyleBackColor = false;
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // pbGrabar
            // 
            this.pbGrabar.Location = new System.Drawing.Point(257, 662);
            this.pbGrabar.Maximum = 1000;
            this.pbGrabar.Name = "pbGrabar";
            this.pbGrabar.Size = new System.Drawing.Size(226, 13);
            this.pbGrabar.TabIndex = 5;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // lblGuardando
            // 
            this.lblGuardando.AutoSize = true;
            this.lblGuardando.Location = new System.Drawing.Point(42, 662);
            this.lblGuardando.Name = "lblGuardando";
            this.lblGuardando.Size = new System.Drawing.Size(209, 13);
            this.lblGuardando.TabIndex = 7;
            this.lblGuardando.Text = "Guardando información, por favor espere...";
            // 
            // btnHistorial
            // 
            this.btnHistorial.BackColor = System.Drawing.Color.Crimson;
            this.btnHistorial.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHistorial.ForeColor = System.Drawing.Color.White;
            this.btnHistorial.Location = new System.Drawing.Point(1187, 658);
            this.btnHistorial.Name = "btnHistorial";
            this.btnHistorial.Size = new System.Drawing.Size(165, 32);
            this.btnHistorial.TabIndex = 8;
            this.btnHistorial.Text = "Ver Historial";
            this.btnHistorial.UseVisualStyleBackColor = false;
            this.btnHistorial.Click += new System.EventHandler(this.btnHistorial_Click);
            // 
            // frmFichaMedica
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1354, 695);
            this.Controls.Add(this.btnHistorial);
            this.Controls.Add(this.btnCerrar);
            this.Controls.Add(this.lblGuardando);
            this.Controls.Add(this.pbGrabar);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Name = "frmFichaMedica";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Ficha Médica";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmFichaMedica_FormClosed);
            this.Load += new System.EventHandler(this.frmFichaMedica_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tbpMotivoConsulta.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tbpAnamnesis.ResumeLayout(false);
            this.groupBox40.ResumeLayout(false);
            this.groupBox40.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox21.ResumeLayout(false);
            this.groupBox21.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSintomas)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.btnAntecPatologicos.ResumeLayout(false);
            this.gpbAntecedentes.ResumeLayout(false);
            this.gpbAntecedentes.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAntecedentes)).EndInit();
            this.tbpDiagnosticosAnteriores.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAntecDiagnosticos)).EndInit();
            this.tpgExamFisico.ResumeLayout(false);
            this.tabControl4.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbxParteCuerpo)).EndInit();
            this.groupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPartesCuerpo)).EndInit();
            this.groupBox13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSegmentos)).EndInit();
            this.tbpAyudaDx.ResumeLayout(false);
            this.groupBox16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAyudaDiagnostica)).EndInit();
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.tbpPlanTrabajo.ResumeLayout(false);
            this.groupBox19.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAsistencia)).EndInit();
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTratamientos)).EndInit();
            this.groupBox17.ResumeLayout(false);
            this.tbpDiagnostico.ResumeLayout(false);
            this.groupBox20.ResumeLayout(false);
            this.groupBox20.PerformLayout();
            this.groupBox27.ResumeLayout(false);
            this.groupBox27.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDiagnosticos)).EndInit();
            this.tbpPrescripcion.ResumeLayout(false);
            this.groupBox24.ResumeLayout(false);
            this.groupBox24.PerformLayout();
            this.gpbDatos.ResumeLayout(false);
            this.gpbDatos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPreparados)).EndInit();
            this.groupBox23.ResumeLayout(false);
            this.groupBox23.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPrescripcion)).EndInit();
            this.tbpComplementarios.ResumeLayout(false);
            this.groupBox43.ResumeLayout(false);
            this.groupBox43.PerformLayout();
            this.groupBox42.ResumeLayout(false);
            this.groupBox42.PerformLayout();
            this.groupBox41.ResumeLayout(false);
            this.groupBox41.PerformLayout();
            this.tbpConsultaAnterior.ResumeLayout(false);
            this.groupBox31.ResumeLayout(false);
            this.tabControl3.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPrescCA)).EndInit();
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDiagCA)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvExamCA)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.groupBox38.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFamCA)).EndInit();
            this.groupBox37.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvQuirCA)).EndInit();
            this.groupBox36.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPatCA)).EndInit();
            this.groupBox30.ResumeLayout(false);
            this.groupBox30.PerformLayout();
            this.groupBox35.ResumeLayout(false);
            this.groupBox35.PerformLayout();
            this.groupBox39.ResumeLayout(false);
            this.groupBox39.PerformLayout();
            this.groupBox29.ResumeLayout(false);
            this.groupBox29.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblNumHistoria;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblHora;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblFecha;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblPaciente;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tbpMotivoConsulta;
        private System.Windows.Forms.TabPage tpgExamFisico;
        private System.Windows.Forms.TabPage tbpAyudaDx;
        private System.Windows.Forms.TabPage tbpPlanTrabajo;
        private System.Windows.Forms.TabPage tbpDiagnostico;
        private System.Windows.Forms.TabPage tbpPrescripcion;
        private System.Windows.Forms.TabPage tbpComplementarios;
        private System.Windows.Forms.Label lblProfesion;
        private System.Windows.Forms.Label lblProcedencia;
        private System.Windows.Forms.Label lblGradoInstruccion;
        private System.Windows.Forms.Label lblLugarNacimiento;
        private System.Windows.Forms.Label lblDocIdentidad;
        private System.Windows.Forms.Label lblEdad;
        private System.Windows.Forms.Label lblSexo;
        private System.Windows.Forms.Label lblGrupoSanguineo;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tbpAnamnesis;
        private System.Windows.Forms.TabPage btnAntecPatologicos;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtObservHabNoc;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DataGridView dgvSintomas;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtMotivo;
        private System.Windows.Forms.GroupBox gpbAntecedentes;
        private System.Windows.Forms.DataGridView dgvAntecedentes;
        private System.Windows.Forms.Button btnAddAntecedentes;
        private System.Windows.Forms.TextBox txtAntecedenteObserv;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox cboAntecedente;
        private System.Windows.Forms.Label lblAntecedente;
        private System.Windows.Forms.TabPage tbpDiagnosticosAnteriores;
        private System.Windows.Forms.DataGridView dgvAntecDiagnosticos;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.DataGridView dgvAyudaDiagnostica;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.TextBox txtInfExamen;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.ComboBox cmbTipoExamen;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.DateTimePicker dtpFechaExamen;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.Button btnPat;
        private System.Windows.Forms.Button btnImg;
        private System.Windows.Forms.Button btnLab;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.DataGridView dgvAsistencia;
        private System.Windows.Forms.DataGridView dgvTratamientos;
        private System.Windows.Forms.TextBox txtTratamNumSesiones;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.ComboBox cmbTratamientos;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.GroupBox groupBox20;
        private System.Windows.Forms.TextBox txtDiagObserv;
        private System.Windows.Forms.TextBox txtCIECod;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.DataGridView dgvDiagnosticos;
        private System.Windows.Forms.GroupBox groupBox23;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.DataGridView dgvPrescripcion;
        private System.Windows.Forms.Button btnAddPresc;
        private System.Windows.Forms.TextBox txtIndicaciones;
        private System.Windows.Forms.Button btnCerrar;
        private System.Windows.Forms.ProgressBar pbGrabar;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label lblGuardando;
        private System.Windows.Forms.Button btnGrabarTx;
        private System.Windows.Forms.Button btnGrabar;
        private System.Windows.Forms.TextBox txtAlergias;
        private System.Windows.Forms.Button btnGuardarAyudaDx;
        private System.Windows.Forms.Button btnHistorial;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Seleccion;
        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.CheckBox chkOtros;
        private System.Windows.Forms.CheckBox chkDrogas;
        private System.Windows.Forms.CheckBox chkTabaco;
        private System.Windows.Forms.CheckBox chkAlcohol;
        private System.Windows.Forms.GroupBox groupBox40;
        private System.Windows.Forms.TextBox txtFUltMenstruacion;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage tbpConsultaAnterior;
        private System.Windows.Forms.GroupBox groupBox31;
        private System.Windows.Forms.GroupBox groupBox30;
        private System.Windows.Forms.GroupBox groupBox35;
        private System.Windows.Forms.GroupBox groupBox39;
        private System.Windows.Forms.CheckBox chkOtrosHabCA;
        private System.Windows.Forms.CheckBox chkDrogasCA;
        private System.Windows.Forms.CheckBox chkTabacoCA;
        private System.Windows.Forms.CheckBox chkAlcoholCA;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.TextBox txtEspecHabCA;
        private System.Windows.Forms.TextBox txtMotivoEvolCA;
        private System.Windows.Forms.GroupBox groupBox29;
        private System.Windows.Forms.TextBox txtTipoAtencionCA;
        private System.Windows.Forms.TextBox txtHoraCA;
        private System.Windows.Forms.TextBox txtFechaCA;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.GroupBox groupBox43;
        private System.Windows.Forms.TextBox txtObservaciones;
        private System.Windows.Forms.GroupBox groupBox42;
        private System.Windows.Forms.TextBox txtRecomendaciones;
        private System.Windows.Forms.GroupBox groupBox41;
        private System.Windows.Forms.DateTimePicker dtpProximaCita;
        public System.Windows.Forms.Label lblServicio;
        private System.Windows.Forms.ComboBox cboTipoAtencion;
        private System.Windows.Forms.Button btnAntecFamiliares;
        private System.Windows.Forms.Button btnAntecAlergias;
        private System.Windows.Forms.Button btnAntecQuirurg;
        private System.Windows.Forms.Button btnAntecPatol;
        private System.Windows.Forms.DataGridViewButtonColumn btnDelAntecedentes;
        private System.Windows.Forms.DataGridViewButtonColumn btnUpdAntecedentes;
        private System.Windows.Forms.GroupBox groupBox27;
        private System.Windows.Forms.RadioButton rbtPresuntivo;
        private System.Windows.Forms.RadioButton rbtDefinitivo;
        private System.Windows.Forms.RadioButton rbtCronico;
        private System.Windows.Forms.DataGridViewButtonColumn btnDelAyudaDx;
        private System.Windows.Forms.DataGridViewButtonColumn btnUpdAyudaDx;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button btnPresc;
        private System.Windows.Forms.Button btnRepiteRx;
        private System.Windows.Forms.TabControl tabControl3;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.GroupBox groupBox38;
        private System.Windows.Forms.DataGridView dgvFamCA;
        private System.Windows.Forms.GroupBox groupBox37;
        private System.Windows.Forms.DataGridView dgvQuirCA;
        private System.Windows.Forms.GroupBox groupBox36;
        private System.Windows.Forms.DataGridView dgvPatCA;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.DataGridView dgvDiagCA;
        private System.Windows.Forms.DataGridView dgvExamCA;
        private System.Windows.Forms.Button btnPatCA;
        private System.Windows.Forms.Button btnImgCA;
        private System.Windows.Forms.Button btnLabCA;
        private System.Windows.Forms.Button btnProcCA;
        private System.Windows.Forms.ComboBox cmbFamiliar;
        private System.Windows.Forms.Label lblFamiliar;
        private System.Windows.Forms.TabControl tabControl4;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.ComboBox cboEstHidratacion;
        private System.Windows.Forms.ComboBox cboEstNutricional;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.ComboBox cboEstGeneral;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.CheckBox chkOPersona;
        private System.Windows.Forms.CheckBox chkOEspacio;
        private System.Windows.Forms.CheckBox chkOTiempo;
        private System.Windows.Forms.CheckBox chkLucido;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox txtTalla;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox txtPeso;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox txtTemperatura;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox txtPulso;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txtPAMinima;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txtPAMaxima;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.DataGridView dgvPartesCuerpo;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.DataGridView dgvSegmentos;
        private System.Windows.Forms.PictureBox pbxParteCuerpo;
        private System.Windows.Forms.Button btnGuardaSegmentos;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtAyudaDiagId;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnAyudaDiagNuevo;
        private System.Windows.Forms.Button btnEco;
        private System.Windows.Forms.Button btnEcoCA;
        private System.Windows.Forms.DataGridViewButtonColumn btnTratamientosDel;
        private System.Windows.Forms.DataGridViewButtonColumn btnTratamientosUpd;
        private System.Windows.Forms.TextBox txtTratamObserv;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.GroupBox groupBox24;
        private System.Windows.Forms.ComboBox cmbCriterio;
        private System.Windows.Forms.Label lblCriterio;
        private System.Windows.Forms.TextBox txtPrdDescripcion;
        private System.Windows.Forms.TextBox txtPrdId;
        private System.Windows.Forms.ListBox lstMedicinasFiltro;
        private System.Windows.Forms.DataGridViewButtonColumn btnDelMedicinas;
        private System.Windows.Forms.DataGridViewButtonColumn btnUpdMedicinas;
        private System.Windows.Forms.RadioButton rbtLaboratorio;
        private System.Windows.Forms.RadioButton rbtGenerico;
        private System.Windows.Forms.ListBox lstMedicinas;
        private System.Windows.Forms.DataGridView dgvPreparados;
        private System.Windows.Forms.RadioButton rbtPreparados;
        private System.Windows.Forms.GroupBox gpbDatos;
        private System.Windows.Forms.Label lblUndElemBase;
        private System.Windows.Forms.TextBox txtCantElemBase;
        private System.Windows.Forms.Label lblCantidad;
        private System.Windows.Forms.ComboBox cmbComponente;
        private System.Windows.Forms.Button btnAddCompuesto;
        private System.Windows.Forms.Button btnAceptar;
        private System.Windows.Forms.DataGridViewButtonColumn btnDelCompuesto;
        private System.Windows.Forms.DataGridViewButtonColumn btnUpdCompuesto;
        private System.Windows.Forms.CheckBox chkDxPrincipal;
        private System.Windows.Forms.ListBox lstDiagnosticos;
        private System.Windows.Forms.TextBox txtDxNombre;
        private System.Windows.Forms.DataGridViewButtonColumn btnDelDiagnostico;
        private System.Windows.Forms.DataGridViewButtonColumn btnUpdDiagnostico;
        private System.Windows.Forms.DataGridView dgvPrescCA;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton rbtElementoBase;
        private System.Windows.Forms.RadioButton rbtPrincActivo;
        private System.Windows.Forms.Button btnAgregarComponente;
        private System.Windows.Forms.ComboBox cmbVigencia;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label lblRpCaduca;
        private System.Windows.Forms.Label label22;
    }
}