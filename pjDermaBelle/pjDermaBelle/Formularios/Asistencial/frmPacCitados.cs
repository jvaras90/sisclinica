﻿using pjDermaBelle.Formularios.Asistencial;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pjDermaBelle
{
    public partial class frmPacCitados : Form
    {
        public frmPacCitados()
        {
            InitializeComponent();
        }

        ClaseDatos obj = new ClaseDatos();
        DermaBelleDataDataContext linq = new DermaBelleDataDataContext();
        public static string NumHistoria;
        public int _SerId;
        public int _MedCod;

        void CargaCitas()
        {
            int totcit = 0;
            int totate = 0;
            dgvPacCitados.DataSource = obj.GridPacientesCitados(_MedCod);
            for (int indice = 0; indice < dgvPacCitados.Rows.Count; indice++)
            {
                //dgvPacCitados.Columns[1].DefaultCellStyle.Format = "d";
                dgvPacCitados.Columns[1].Width = 50;      //Hora
                dgvPacCitados.Columns[2].Width = 100;     //HIstoria
                dgvPacCitados.Columns[3].Width = 200;     //Paciente
                dgvPacCitados.Columns[4].Width = 100;     //Estado
                dgvPacCitados.Columns[5].Width = 200;     //Tipo de Atención
                dgvPacCitados.Columns[6].Width = 300;     //Atención
                dgvPacCitados.Columns[7].Width = 300;     //Observaciones
                dgvPacCitados.Columns[8].Visible = false;     //CitaId
                totcit++;
                if (string.Equals(dgvPacCitados.Rows[indice].Cells[2].Value.ToString(), "Cerrada"))
                { totate++; }

                dgvPacCitados.Rows[indice].ReadOnly = true;
                //dgvPacCitados.Columns[0].Visible = false;
            }
            lblCitados.Text = totcit.ToString();
            lblAtendidos.Text = totate.ToString();
        }

        void Historial()
        {
            int ind = dgvPacCitados.CurrentCell.RowIndex;
            string Pac_HC = dgvPacCitados.Rows[ind].Cells[2].Value.ToString();
            //NumHistoria = Pac_HC;

            dgvPacFichas.DataSource = obj.GridPacientesFicha(Pac_HC);

            for (int indice = 0; indice < dgvPacFichas.Rows.Count; indice++)
            {
                dgvPacFichas.Columns[0].Width = 80;     //Fecha
                dgvPacFichas.Columns[1].Width = 100;    //Tipo de Atención
                dgvPacFichas.Columns[2].Width = 100;    //Servicio
                dgvPacFichas.Columns[3].Width = 50;     //CIE10
                dgvPacFichas.Columns[4].Width = 300;    //Diagnóstico

                dgvPacFichas.Rows[indice].ReadOnly = true;
            }
        }

        private void frmPacCitados_Load(object sender, EventArgs e)
        {
            frmMenuPrincipal principal = new frmMenuPrincipal();
            //lblMedico.Text = 
            DateTime hoy = new DateTime();
            hoy = DateTime.Today;
            CargaCitas();
        }

        private void dgvPacCitados_SelectionChanged(object sender, EventArgs e)
        {
            int indice = dgvPacCitados.CurrentCell.RowIndex;
            string Pac_HC = dgvPacCitados.Rows[indice].Cells[2].Value.ToString();
            NumHistoria = Pac_HC;
        }

        private void dgvPacCitados_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == -1)
            { return;}

            if (this.dgvPacCitados.Columns[e.ColumnIndex].Name == "btnAbrirFicha")
            {
                int ind = dgvPacCitados.CurrentCell.RowIndex;
                short CitaId = short.Parse(dgvPacCitados.Rows[ind].Cells[8].Value.ToString());
                if (dgvPacCitados.Rows[ind].Cells[5].Value.ToString() == "CONSULTA MEDICA")
                {
                    frmFichaMedica ficha = new frmFichaMedica();
                    ficha.pCitaId = CitaId;
                    ficha.lblServicio.Text = lblServicio.Text;
                    ficha.SerId = _SerId;
                    ficha.MedCod = _MedCod;
                    ficha.FormClosed += new FormClosedEventHandler(ficha_FormClosed);
                    ficha.ShowDialog();
                }
                else
                { 
                    frmProcedimientos ficha = new frmProcedimientos();
                    if (dgvPacCitados.Rows[ind].Cells[4].Value.ToString() == "Cerrada")
                        ficha.cerrada = true;
                    else
                        ficha.cerrada = false;

                    ficha.pCitaId = CitaId;
                    ficha.lblServicio.Text = dgvPacCitados.Rows[ind].Cells[5].Value.ToString() + ": " + dgvPacCitados.Rows[ind].Cells[6].Value.ToString();
                    ficha.SerId = _SerId;
                    ficha.MedId = _MedCod;
                    ficha.FormClosed += new FormClosedEventHandler(ficha_FormClosed);
                    ficha.ShowDialog();
                }
            }
            else
            {
                Historial();
            }
        }

        private void ficha_FormClosed(object sender, FormClosedEventArgs e)
        {
            CargaCitas();
        }

        private void dgvPacCitados_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.ColumnIndex >= 0 && this.dgvPacCitados.Columns[e.ColumnIndex].Name == "btnAbrirFicha" && e.RowIndex >= 0)
            {
                e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                DataGridViewButtonCell celBoton = this.dgvPacCitados.Rows[e.RowIndex].Cells["btnAbrirFicha"] as DataGridViewButtonCell;
                Icon icoEliminar = new Icon(@"C:\Windows\Dermabelle\Ico\Open_Folder.ico");
                e.Graphics.DrawIcon(icoEliminar, e.CellBounds.Left + 3, e.CellBounds.Top + 3);

                this.dgvPacCitados.Rows[e.RowIndex].Height = icoEliminar.Height + 10;
                this.dgvPacCitados.Columns[e.ColumnIndex].Width = icoEliminar.Width + 10;

                e.Handled = true;
            }
        }
    }
}
