﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pjDermaBelle.Formularios.Asistencial
{
    public partial class frmPrescAnteriores : Form
    {
        public frmPrescAnteriores()
        {
            InitializeComponent();
        }

        ClaseDatos obj = new ClaseDatos();
        DermaBelleDataDataContext linq = new DermaBelleDataDataContext();
        public string Pac_HC;

        private void frmPrescAnteriores_Load(object sender, EventArgs e)
        {
            //MessageBox.Show(Pac_HC);
            dgvMedicinas.DataSource = obj.dgvPrescripAnteriores(Pac_HC);
            for (int indice = 0; indice < dgvMedicinas.Rows.Count; indice++)
            {
                dgvMedicinas.Columns[0].Width = 300;      //Comercial
                dgvMedicinas.Columns[1].Width = 300;      //Generico
                dgvMedicinas.Columns[2].Width = 280;      //Indicaciones
                dgvMedicinas.Columns[3].Width = 100;      //Fecha

                dgvMedicinas.Rows[indice].ReadOnly = true;
            }
        }
    }
}
