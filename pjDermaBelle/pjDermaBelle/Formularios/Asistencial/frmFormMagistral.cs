﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pjDermaBelle.Formularios.Asistencial
{
    public partial class frmFormMagistral : Form
    {
        public frmFormMagistral()
        {
            InitializeComponent();
        }

        ClaseDatos obj = new ClaseDatos();
        DermaBelleDataDataContext linq = new DermaBelleDataDataContext();

        private void frmFormMagistral_Load(object sender, EventArgs e)
        {
            cmbCompuestos.DataSource = obj.cmbPrincipios("FM","%");
            cmbCompuestos.ValueMember = "PrA_Id";
            cmbCompuestos.DisplayMember = "PrA_Nombre";

            cmbElementoBase.DataSource = obj.cmbPrincipios("EB","%");
            cmbElementoBase.ValueMember = "PrA_Id";
            cmbElementoBase.DisplayMember = "PrA_Nombre";
            cmbElementoBase.Enabled = false;
            txtCantElemBase.Enabled = false;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            cmbCompuestos.Enabled = !checkBox1.Checked;
            txtCantComp.Enabled = !checkBox1.Checked;
            cmbElementoBase.Enabled = checkBox1.Checked;
            txtCantElemBase.Enabled = checkBox1.Checked;
        }
    }
}
