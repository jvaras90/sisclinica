﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pjDermaBelle.Formularios.Asistencial
{
    public partial class frmEcografia : Form
    {
        public frmEcografia()
        {
            InitializeComponent();
        }

        ClaseDatos obj = new ClaseDatos();
        DermaBelleDataDataContext linq = new DermaBelleDataDataContext();
        FM_ExamenesAuxiliares oFM_ExamenesAuxiliares = new FM_ExamenesAuxiliares();

        public int FichaNum;

        //**********************Area de Carga De los CheckListBox***************************************************
        void CargarAbdomen()
        {
            List<usp_dgvExamenesLisResult> Abdomen = new List<usp_dgvExamenesLisResult>();
            Abdomen = obj.dgvExamenesLis(FichaNum, 4, 24);
            if (Abdomen.Count() > 0)
            {
                foreach (usp_dgvExamenesLisResult reg in Abdomen)
                {
                    chklAbdomen.Items.Add(reg.Examen, reg.Seleccionado);
                }
            }
        }

        void CargarDoppler()
        {
            List<usp_dgvExamenesLisResult> Doppler = new List<usp_dgvExamenesLisResult>();
            Doppler = obj.dgvExamenesLis(FichaNum, 4, 25);
            if (Doppler.Count() > 0)
            {
                foreach (usp_dgvExamenesLisResult reg in Doppler)
                {
                    chklDoppler.Items.Add(reg.Examen, reg.Seleccionado);
                }
            }
        }

        void CargarGinecologia()
        {
            List<usp_dgvExamenesLisResult> Ginecologia = new List<usp_dgvExamenesLisResult>();
            Ginecologia = obj.dgvExamenesLis(FichaNum, 4, 26);
            if (Ginecologia.Count() > 0)
            {
                foreach (usp_dgvExamenesLisResult reg in Ginecologia)
                {
                    chklGinecologia.Items.Add(reg.Examen, reg.Seleccionado);
                }
            }
        }

        void CargarPorOrgano()
        {
            List<usp_dgvExamenesLisResult> PorOrgano = new List<usp_dgvExamenesLisResult>();
            PorOrgano = obj.dgvExamenesLis(FichaNum, 4, 27);
            if (PorOrgano.Count() > 0)
            {
                foreach (usp_dgvExamenesLisResult reg in PorOrgano)
                {
                    chklPorOrgano.Items.Add(reg.Examen, reg.Seleccionado);
                }
            }
        }

        void CargarEcog3D4D()
        {
            List<usp_dgvExamenesLisResult> Ecog3D4D = new List<usp_dgvExamenesLisResult>();
            Ecog3D4D = obj.dgvExamenesLis(FichaNum, 4, 29);
            if (Ecog3D4D.Count() > 0)
            {
                foreach (usp_dgvExamenesLisResult reg in Ecog3D4D)
                {
                    chklEcog3D4D.Items.Add(reg.Examen, reg.Seleccionado);
                }
            }
        }

        void CargarOtros()
        {
            List<usp_dgvExamenesLisResult> Otros = new List<usp_dgvExamenesLisResult>();
            Otros = obj.dgvExamenesLis(FichaNum, 4, 30);
            if (Otros.Count() > 0)
            {
                foreach (usp_dgvExamenesLisResult reg in Otros)
                {
                    chklOtros.Items.Add(reg.Examen, reg.Seleccionado);
                }
            }
        }

        void CargarIndicaciones()
        {
            List<usp_dgvExamenesLisResult> Liquidos = new List<usp_dgvExamenesLisResult>();
            Liquidos = obj.dgvExamenesLis(FichaNum, 4, 33);
            if (Liquidos.Count() > 0)
            {
                foreach (usp_dgvExamenesLisResult reg in Liquidos)
                {
                    txtIndicaciones.Text = reg.Observacion;
                }
            }
        }

        //**********************Area de Almacenamiento en Tablas***************************************************
        void GuardarAbdomen()
        {
            oFM_ExamenesAuxiliares.FichaMedicaId = Convert.ToSByte(FichaNum);
            oFM_ExamenesAuxiliares.ExamenTipo = 4;
            oFM_ExamenesAuxiliares.ExamenGrupo = 24;
            obj.FM_ExamAuxDel(oFM_ExamenesAuxiliares);
            foreach (object ind in chklAbdomen.CheckedItems)
            {
                string titulo = ind.ToString();
                List<usp_ExamenIdRecResult> listado = new List<usp_ExamenIdRecResult>();
                listado = obj.usp_ExamenIdRec(titulo);
                if (listado.Count() > 0)
                {
                    foreach (usp_ExamenIdRecResult reg in listado)
                    {
                        oFM_ExamenesAuxiliares.ExamenId = reg.ExamenId;
                        oFM_ExamenesAuxiliares.ExamenSelec = true;
                        obj.FM_ExamAuxAdd(oFM_ExamenesAuxiliares);
                    }
                }
            }
        }

        void GuardarDoppler()
        {
            oFM_ExamenesAuxiliares.FichaMedicaId = Convert.ToSByte(FichaNum);
            oFM_ExamenesAuxiliares.ExamenTipo = 4;
            oFM_ExamenesAuxiliares.ExamenGrupo = 25;
            obj.FM_ExamAuxDel(oFM_ExamenesAuxiliares);
            foreach (object ind in chklDoppler.CheckedItems)
            {
                string titulo = ind.ToString();
                List<usp_ExamenIdRecResult> listado = new List<usp_ExamenIdRecResult>();
                listado = obj.usp_ExamenIdRec(titulo);
                if (listado.Count() > 0)
                {
                    foreach (usp_ExamenIdRecResult reg in listado)
                    {
                        oFM_ExamenesAuxiliares.ExamenId = reg.ExamenId;
                        oFM_ExamenesAuxiliares.ExamenSelec = true;
                        obj.FM_ExamAuxAdd(oFM_ExamenesAuxiliares);
                    }
                }
            }
        }

        void GuardarGinecologia()
        {
            oFM_ExamenesAuxiliares.FichaMedicaId = Convert.ToSByte(FichaNum);
            oFM_ExamenesAuxiliares.ExamenTipo = 4;
            oFM_ExamenesAuxiliares.ExamenGrupo = 26;
            obj.FM_ExamAuxDel(oFM_ExamenesAuxiliares);
            foreach (object ind in chklDoppler.CheckedItems)
            {
                string titulo = ind.ToString();
                List<usp_ExamenIdRecResult> listado = new List<usp_ExamenIdRecResult>();
                listado = obj.usp_ExamenIdRec(titulo);
                if (listado.Count() > 0)
                {
                    foreach (usp_ExamenIdRecResult reg in listado)
                    {
                        oFM_ExamenesAuxiliares.ExamenId = reg.ExamenId;
                        oFM_ExamenesAuxiliares.ExamenSelec = true;
                        obj.FM_ExamAuxAdd(oFM_ExamenesAuxiliares);
                    }
                }
            }
        }

        void GuardarPorOrgano()
        {
            oFM_ExamenesAuxiliares.FichaMedicaId = Convert.ToSByte(FichaNum);
            oFM_ExamenesAuxiliares.ExamenTipo = 4;
            oFM_ExamenesAuxiliares.ExamenGrupo = 27;
            obj.FM_ExamAuxDel(oFM_ExamenesAuxiliares);
            foreach (object ind in chklDoppler.CheckedItems)
            {
                string titulo = ind.ToString();
                List<usp_ExamenIdRecResult> listado = new List<usp_ExamenIdRecResult>();
                listado = obj.usp_ExamenIdRec(titulo);
                if (listado.Count() > 0)
                {
                    foreach (usp_ExamenIdRecResult reg in listado)
                    {
                        oFM_ExamenesAuxiliares.ExamenId = reg.ExamenId;
                        oFM_ExamenesAuxiliares.ExamenSelec = true;
                        obj.FM_ExamAuxAdd(oFM_ExamenesAuxiliares);
                    }
                }
            }
        }


        void GuardarEcog3D4D()
        {
            oFM_ExamenesAuxiliares.FichaMedicaId = Convert.ToSByte(FichaNum);
            oFM_ExamenesAuxiliares.ExamenTipo = 4;
            oFM_ExamenesAuxiliares.ExamenGrupo = 29;
            obj.FM_ExamAuxDel(oFM_ExamenesAuxiliares);
            foreach (object ind in chklDoppler.CheckedItems)
            {
                string titulo = ind.ToString();
                List<usp_ExamenIdRecResult> listado = new List<usp_ExamenIdRecResult>();
                listado = obj.usp_ExamenIdRec(titulo);
                if (listado.Count() > 0)
                {
                    foreach (usp_ExamenIdRecResult reg in listado)
                    {
                        oFM_ExamenesAuxiliares.ExamenId = reg.ExamenId;
                        oFM_ExamenesAuxiliares.ExamenSelec = true;
                        obj.FM_ExamAuxAdd(oFM_ExamenesAuxiliares);
                    }
                }
            }
        }

        void GuardarOtros()
        {
            oFM_ExamenesAuxiliares.FichaMedicaId = Convert.ToSByte(FichaNum);
            oFM_ExamenesAuxiliares.ExamenTipo = 4;
            oFM_ExamenesAuxiliares.ExamenGrupo = 30;
            obj.FM_ExamAuxDel(oFM_ExamenesAuxiliares);
            foreach (object ind in chklDoppler.CheckedItems)
            {
                string titulo = ind.ToString();
                List<usp_ExamenIdRecResult> listado = new List<usp_ExamenIdRecResult>();
                listado = obj.usp_ExamenIdRec(titulo);
                if (listado.Count() > 0)
                {
                    foreach (usp_ExamenIdRecResult reg in listado)
                    {
                        oFM_ExamenesAuxiliares.ExamenId = reg.ExamenId;
                        oFM_ExamenesAuxiliares.ExamenSelec = true;
                        obj.FM_ExamAuxAdd(oFM_ExamenesAuxiliares);
                    }
                }
            }
        }

        void GuardarIndicaciones()
        {
            oFM_ExamenesAuxiliares.FichaMedicaId = Convert.ToSByte(FichaNum);
            oFM_ExamenesAuxiliares.ExamenTipo = 4;
            oFM_ExamenesAuxiliares.ExamenGrupo = 33;
            obj.FM_ExamAuxDel(oFM_ExamenesAuxiliares);
            string titulo = "ECOGRAFIA INDICACIONES";
            List<usp_ExamenIdRecResult> listado = new List<usp_ExamenIdRecResult>();
            listado = obj.usp_ExamenIdRec(titulo);
            if (listado.Count() > 0)
            {
                foreach (usp_ExamenIdRecResult reg in listado)
                {
                    oFM_ExamenesAuxiliares.ExamenId = reg.ExamenId;
                    oFM_ExamenesAuxiliares.ExamenObserv = txtIndicaciones.Text.ToUpper();
                    oFM_ExamenesAuxiliares.ExamenSelec = true;
                    obj.FM_ExamAuxAdd(oFM_ExamenesAuxiliares);
                }
            }
        }

        private void frmEcografia_Load(object sender, EventArgs e)
        {
            CargarAbdomen();
            CargarDoppler();
            CargarGinecologia();
            CargarPorOrgano();
            CargarEcog3D4D();
            CargarOtros();
            CargarIndicaciones();
        }

        private void frmEcografia_FormClosed(object sender, FormClosedEventArgs e)
        {
            GuardarAbdomen();
            GuardarDoppler();
            GuardarGinecologia();
            GuardarPorOrgano();
            GuardarEcog3D4D();
            GuardarOtros();
            GuardarIndicaciones();
        }
    }
}
