﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pjDermaBelle.Formularios.Asistencial
{
    public partial class frmPatologia : Form
    {
        public frmPatologia()
        {
            InitializeComponent();
        }

        ClaseDatos obj = new ClaseDatos();
        DermaBelleDataDataContext linq = new DermaBelleDataDataContext();
        FM_ExamenesAuxiliares oFM_ExamenesAuxiliares = new FM_ExamenesAuxiliares();

        public int FichaNum;

        private void frmPatologia_Load(object sender, EventArgs e)
        {
            //Cargar Patologías
            List<usp_dgvExamenesLisResult> Patologia = new List<usp_dgvExamenesLisResult>();
            Patologia = obj.dgvExamenesLis(FichaNum, 3, 23);
            if (Patologia.Count() > 0)
            {
                foreach (usp_dgvExamenesLisResult reg in Patologia)
                {
                    chklPatologia.Items.Add(reg.Examen, reg.Seleccionado);
                }
            }

            //Cargar Otras Patologías
            List<usp_dgvExamenesLisResult> Liquidos = new List<usp_dgvExamenesLisResult>();
            Liquidos = obj.dgvExamenesLis(FichaNum, 3, 32);
            if (Liquidos.Count() > 0)
            {
                foreach (usp_dgvExamenesLisResult reg in Liquidos)
                {
                    txtOtros.Text = reg.Observacion;
                }
            }
        }

        private void frmPatologia_FormClosed(object sender, FormClosedEventArgs e)
        {
            //Guardar Patologías
            oFM_ExamenesAuxiliares.FichaMedicaId = Convert.ToSByte(FichaNum);
            oFM_ExamenesAuxiliares.ExamenTipo = 3;
            oFM_ExamenesAuxiliares.ExamenGrupo = 23;
            obj.FM_ExamAuxDel(oFM_ExamenesAuxiliares);
            foreach (object ind in chklPatologia.CheckedItems)
            {
                string titulo = ind.ToString();
                List<usp_ExamenIdRecResult> listado = new List<usp_ExamenIdRecResult>();
                listado = obj.usp_ExamenIdRec(titulo);
                if (listado.Count() > 0)
                {
                    foreach (usp_ExamenIdRecResult reg in listado)
                    {
                        oFM_ExamenesAuxiliares.ExamenId = reg.ExamenId;
                        oFM_ExamenesAuxiliares.ExamenSelec = true;
                        obj.FM_ExamAuxAdd(oFM_ExamenesAuxiliares);
                    }
                }
            }

            //Guardar Otros
            oFM_ExamenesAuxiliares.FichaMedicaId = Convert.ToSByte(FichaNum);
            oFM_ExamenesAuxiliares.ExamenTipo = 3;
            oFM_ExamenesAuxiliares.ExamenGrupo = 32;
            obj.FM_ExamAuxDel(oFM_ExamenesAuxiliares);
            List<usp_ExamenIdRecResult> Otros = new List<usp_ExamenIdRecResult>();
            Otros = obj.usp_ExamenIdRec("OTRAS PATOLOGIAS");
            if (Otros.Count() > 0)
            {
                foreach (usp_ExamenIdRecResult reg in Otros)
                {
                    oFM_ExamenesAuxiliares.ExamenId = reg.ExamenId;
                    oFM_ExamenesAuxiliares.ExamenObserv = txtOtros.Text.ToUpper();
                    oFM_ExamenesAuxiliares.ExamenSelec = true;
                    obj.FM_ExamAuxAdd(oFM_ExamenesAuxiliares);
                }
            }

        }
    }
}
