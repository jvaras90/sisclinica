﻿namespace pjDermaBelle.Formularios.Asistencial
{
    partial class frmEcografia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.chklAbdomen = new System.Windows.Forms.CheckedListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.chklEcog3D4D = new System.Windows.Forms.CheckedListBox();
            this.label6 = new System.Windows.Forms.Label();
            this.chklDoppler = new System.Windows.Forms.CheckedListBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.chklGinecologia = new System.Windows.Forms.CheckedListBox();
            this.chklPorOrgano = new System.Windows.Forms.CheckedListBox();
            this.txtIndicaciones = new System.Windows.Forms.TextBox();
            this.chklOtros = new System.Windows.Forms.CheckedListBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Blue;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(4, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(453, 20);
            this.label1.TabIndex = 13;
            this.label1.Text = "ECOGRAFÍA DE ABDOMEN";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chklAbdomen
            // 
            this.chklAbdomen.FormattingEnabled = true;
            this.chklAbdomen.Location = new System.Drawing.Point(4, 28);
            this.chklAbdomen.Name = "chklAbdomen";
            this.chklAbdomen.Size = new System.Drawing.Size(453, 184);
            this.chklAbdomen.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Blue;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(464, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(453, 20);
            this.label2.TabIndex = 15;
            this.label2.Text = "UN SOLO ÓRGANO";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Blue;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(464, 306);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(453, 20);
            this.label5.TabIndex = 21;
            this.label5.Text = "ECOGRAFÍA 3D Y 4D";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chklEcog3D4D
            // 
            this.chklEcog3D4D.FormattingEnabled = true;
            this.chklEcog3D4D.Location = new System.Drawing.Point(463, 329);
            this.chklEcog3D4D.Name = "chklEcog3D4D";
            this.chklEcog3D4D.Size = new System.Drawing.Size(453, 349);
            this.chklEcog3D4D.TabIndex = 20;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Blue;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(4, 215);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(453, 20);
            this.label6.TabIndex = 19;
            this.label6.Text = "DOPPLER VASCULAR PERIFÉRICO";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chklDoppler
            // 
            this.chklDoppler.FormattingEnabled = true;
            this.chklDoppler.Location = new System.Drawing.Point(4, 238);
            this.chklDoppler.Name = "chklDoppler";
            this.chklDoppler.Size = new System.Drawing.Size(453, 214);
            this.chklDoppler.TabIndex = 18;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Blue;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(924, 215);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(418, 20);
            this.label7.TabIndex = 29;
            this.label7.Text = "INDICACIONES";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.Blue;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(924, 5);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(418, 20);
            this.label8.TabIndex = 27;
            this.label8.Text = "OTROS";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.Blue;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(4, 456);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(453, 20);
            this.label9.TabIndex = 25;
            this.label9.Text = "GINECOLOGÍA Y OBSTETRICIA";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chklGinecologia
            // 
            this.chklGinecologia.FormattingEnabled = true;
            this.chklGinecologia.Location = new System.Drawing.Point(4, 479);
            this.chklGinecologia.Name = "chklGinecologia";
            this.chklGinecologia.Size = new System.Drawing.Size(453, 199);
            this.chklGinecologia.TabIndex = 24;
            // 
            // chklPorOrgano
            // 
            this.chklPorOrgano.FormattingEnabled = true;
            this.chklPorOrgano.Location = new System.Drawing.Point(464, 28);
            this.chklPorOrgano.Name = "chklPorOrgano";
            this.chklPorOrgano.Size = new System.Drawing.Size(453, 274);
            this.chklPorOrgano.TabIndex = 30;
            // 
            // txtIndicaciones
            // 
            this.txtIndicaciones.Location = new System.Drawing.Point(923, 238);
            this.txtIndicaciones.Multiline = true;
            this.txtIndicaciones.Name = "txtIndicaciones";
            this.txtIndicaciones.Size = new System.Drawing.Size(419, 440);
            this.txtIndicaciones.TabIndex = 31;
            // 
            // chklOtros
            // 
            this.chklOtros.FormattingEnabled = true;
            this.chklOtros.Location = new System.Drawing.Point(924, 28);
            this.chklOtros.Name = "chklOtros";
            this.chklOtros.Size = new System.Drawing.Size(418, 184);
            this.chklOtros.TabIndex = 26;
            // 
            // frmEcografia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1348, 684);
            this.Controls.Add(this.txtIndicaciones);
            this.Controls.Add(this.chklPorOrgano);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.chklOtros);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.chklGinecologia);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.chklEcog3D4D);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.chklDoppler);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chklAbdomen);
            this.Name = "frmEcografia";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Solicitud de Examenes Auxiliares - Ecografía";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmEcografia_FormClosed);
            this.Load += new System.EventHandler(this.frmEcografia_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckedListBox chklAbdomen;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckedListBox chklEcog3D4D;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckedListBox chklDoppler;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckedListBox chklGinecologia;
        private System.Windows.Forms.CheckedListBox chklPorOrgano;
        private System.Windows.Forms.TextBox txtIndicaciones;
        private System.Windows.Forms.CheckedListBox chklOtros;
    }
}