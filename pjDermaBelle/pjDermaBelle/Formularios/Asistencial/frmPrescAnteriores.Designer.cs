﻿namespace pjDermaBelle.Formularios.Asistencial
{
    partial class frmPrescAnteriores
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrescAnteriores));
            this.dgvMedicinas = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMedicinas)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvMedicinas
            // 
            this.dgvMedicinas.AllowUserToAddRows = false;
            this.dgvMedicinas.AllowUserToDeleteRows = false;
            this.dgvMedicinas.AllowUserToOrderColumns = true;
            this.dgvMedicinas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMedicinas.Location = new System.Drawing.Point(3, 49);
            this.dgvMedicinas.Name = "dgvMedicinas";
            this.dgvMedicinas.ReadOnly = true;
            this.dgvMedicinas.Size = new System.Drawing.Size(998, 336);
            this.dgvMedicinas.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Crimson;
            this.label1.Location = new System.Drawing.Point(3, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(998, 37);
            this.label1.TabIndex = 2;
            this.label1.Text = resources.GetString("label1.Text");
            // 
            // frmPrescAnteriores
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1005, 388);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgvMedicinas);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmPrescAnteriores";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Prescripciones Anteriores";
            this.Load += new System.EventHandler(this.frmPrescAnteriores_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMedicinas)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvMedicinas;
        private System.Windows.Forms.Label label1;
    }
}