﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;

namespace pjDermaBelle.Formularios.Asistencial
{
    public partial class frmProcedimientos : Form
    {

        public frmProcedimientos()
        {
            InitializeComponent();
        }

        ClaseDatos obj = new ClaseDatos();

        public SqlConnection cn = new SqlConnection("data source = PC; integrated security = true; initial catalog = DermaBelle; user id = DBUser; password = dbuser");
        public int SerId;
        public int MedId;

        public string consulta;
        public string campos;
        public string tablas;
        public string condicion;
        public string valores;

        public short pCitaId;
        public short pGri_Id;
        public short pProfesionId;
        public int FichaNum;
        public bool cerrada;

        void Limpiar()
        {
            txtHoraCita.Text = "";
            txtHoraAtencion.Text = "";
            txtNumSesion.Text = "";
            txtInfSesion.Text = "";
            gpbInforme.Enabled = false;
        }

        void ActualizaCitas()
        {
            //Actualizar la tabla Citas
            try
            {
                SqlCommand comando = new SqlCommand();
                comando.Connection = cn;

                ObtFichaMedicaId(lblNumHistoria.Text);

                tablas = "Citas";
                campos = "FichaMedicaId = " + FichaNum + ", FM_Estado = 1";
                condicion = " CitaId = " + pCitaId;

                consulta = "Update " + tablas + " Set " + campos + " Where " + condicion;


                comando.CommandText = consulta;
                cn.Open();
                int nFilas = comando.ExecuteNonQuery();
                if (nFilas > 0)
                    MessageBox.Show("Registro AGREGADO correctamente");
            }
            catch (SqlException ex)
            {
                MessageBox.Show("ERROR: " + ex.Message);
            }
            cn.Close();
        }

        void CargarCabecera()
        {
            List<usp_FMedicaCabeceraResult> lista = new List<usp_FMedicaCabeceraResult>();
            lista = obj.FMedicaCabecera(lblNumHistoria.Text);
            if (lista.Count() > 0)
            {
                foreach (usp_FMedicaCabeceraResult reg in lista)
                {
                    lblPaciente.Text = reg.Pac_NomComp;
                    lblGrupoSanguineo.Text = reg.Pac_GpoSang;
                    lblDocIdentidad.Text = reg.DocIdent;
                    pGri_Id = short.Parse(reg.Pac_InstruccionId.ToString());
                    lblGradoInstruccion.Text = reg.GrI_Nombre;
                    pProfesionId = short.Parse(reg.Pac_ProfesionId.ToString());
                    lblProfesion.Text = reg.Prf_Nombre;
                    lblSexo.Text = reg.Pac_Sexo;
                    lblEdad.Text = reg.Pac_Edad.ToString();
                    lblLugarNacimiento.Text = reg.Pac_LugNacim;
                    lblProcedencia.Text = reg.Pac_Proced;
                }
            }
        }

        void ObtFichaMedicaId(string NumHistoria)
        {
            List<usp_FMedicaIdResult> FichaId = new List<usp_FMedicaIdResult>();
            FichaId = obj.NumeroFicha(frmPacCitados.NumHistoria);
            FichaNum = 0;
            if (FichaId.Count() > 0)
            {
                foreach (usp_FMedicaIdResult reg in FichaId)
                {
                    FichaNum = reg.FichaMedicaId;
                }
            }
        }

        void CargarProcedimientos()
        {
            campos = "Convert(char(10), a.CitaFecha, 103) As Fecha, Cast(a.CitaHora As char(5)) As Hora, a.NumSesion As Sesión, Observacion As Observación, (Select Convert(char(10), FM_Fecha, 103) From FichaMedica Where FichaMedicaId = a.FichaMedicaId) As F_Atención, (Select Cast(FM_HoraIni As char(5)) From FichaMedica Where FichaMedicaId = a.FichaMedicaId)  As H_Atención, (Select FM_MotivoConsulta From FichaMedica Where FichaMedicaId = a.FichaMedicaId) As Informe, IsNull(a.FichaMedicaId,0) As FichaMedicaId ";
            tablas = "Citas a ";
            condicion = "Pac_HC = '" + lblNumHistoria.Text + "' And LEFT(a.Tar_Cod,2) <> 'CO' And	a.CitaEstado = 'C' And a.Ser_Id = " + SerId + " And a.Med_Id = " + MedId + " Order By a.CitaFecha, a.CitaHora";

            consulta = "Select " + campos + " From " + tablas + " Where " + condicion;
            cn.Open();
            SqlDataAdapter daCitas = new SqlDataAdapter(consulta, cn);
            DataSet dsCitas = new DataSet();
            daCitas.Fill(dsCitas, "Citas");

            dgvProcedimientos.DataSource = dsCitas;
            dgvProcedimientos.DataMember = "Citas";

            for (int indice = 0; indice < dgvProcedimientos.Rows.Count; indice++)
            {
                dgvProcedimientos.Columns[0].Width = 100;       //Fecha
                dgvProcedimientos.Columns[1].Width = 100;       //Hora
                dgvProcedimientos.Columns[2].Width = 100;       //Sesion
                dgvProcedimientos.Columns[3].Width = 300;       //Observacion
                dgvProcedimientos.Columns[4].Width = 100;       //Fecha Atencion
                dgvProcedimientos.Columns[5].Width = 100;       //Hora Atencion
                dgvProcedimientos.Columns[6].Width = 500;       //Informe
                dgvProcedimientos.Columns[7].Visible = false;       //FichaMedicaId
            }

            cn.Close();

        }

        private void frmProcedimientos_Load(object sender, EventArgs e)
        {
            Limpiar();
            DateTime FechaHoy = DateTime.Today;
            DateTime HoraHoy = DateTime.Now;
            lblFecha.Text = string.Format(DateTime.Now.ToString("dd/MM/yyyy"));  //FechaHoy.ToString();

            lblHora.Text = string.Format(DateTime.Now.ToString("HH:mm")); //HoraHoy.ToString(); 
            lblNumHistoria.Text = frmPacCitados.NumHistoria;
            CargarCabecera();
            CargarProcedimientos();
        }

        private void dgvProcedimientos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int indice = dgvProcedimientos.CurrentCell.RowIndex;
            dtpFechaCita.Text = dgvProcedimientos.Rows[indice].Cells[0].Value.ToString();
            txtHoraCita.Text = dgvProcedimientos.Rows[indice].Cells[1].Value.ToString();
            dtpFechaAtencion.Text = dgvProcedimientos.Rows[indice].Cells[4].Value.ToString();
            txtHoraAtencion.Text = dgvProcedimientos.Rows[indice].Cells[5].Value.ToString();
            txtNumSesion.Text = dgvProcedimientos.Rows[indice].Cells[2].Value.ToString();
            txtInfSesion.Text = dgvProcedimientos.Rows[indice].Cells[6].Value.ToString();
            FichaNum = int.Parse(dgvProcedimientos.Rows[indice].Cells[7].Value.ToString());

            if ((!string.IsNullOrEmpty(dgvProcedimientos.Rows[indice].Cells[4].Value.ToString()) &&
                !string.Equals(lblFecha.Text, dgvProcedimientos.Rows[indice].Cells[4].Value.ToString())) ||
                cerrada)
            {
                gpbInforme.Enabled = false;
                MessageBox.Show("El INFORME seleccionado NO puede ser EDITADO por pertenecer a una atención ya cerrada", "AVISO");
            }
            else
            {
                txtInfSesion.Focus();
                gpbInforme.Enabled = true;
            }
        }

        private void btnDescartar_Click(object sender, EventArgs e)
        {
            Limpiar();
            gpbInforme.Enabled = false;
        }

        private void btnGrabar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtInfSesion.Text))
            {
                MessageBox.Show("Informe no puede quedar en blanco");
                txtInfSesion.Focus();
                return;
            }

            if (FichaNum == 0)
            {
                // Agregar
                try
                {
                    SqlCommand comando = new SqlCommand();
                    comando.Connection = cn;

                    tablas = "FichaMedica";
                    campos = "(Pac_HC, Pac_TipoAtencion, Pac_Edad, Gri_Id, ProfesionId, FM_MotivoConsulta, FM_Estado, FM_Fecha, FM_HoraIni, FM_HoraFIn, FM_Servicio, FM_MedCod)";
                    valores = "('" + lblNumHistoria.Text + "', 'C'," + lblEdad.Text + ", " + pGri_Id + ", " + pProfesionId + ", '" + txtInfSesion.Text.ToUpper() + "', 1, Cast(GetDate() As Date), Cast(GetDate() As Time), Cast(GetDate() As Time), " + SerId + ", " + MedId + ")";

                    consulta = "Insert Into " + tablas + " " + campos + " Values " + valores;

                    comando.CommandText = consulta;
                    cn.Open();
                    int nFilas = comando.ExecuteNonQuery();
                    if (nFilas > 0)
                    {
                        MessageBox.Show("Registro AGREGADO correctamente");
                        cn.Close();
                        ActualizaCitas();
                    }
                }
                catch (SqlException ex)
                {
                    MessageBox.Show("ERROR: " + ex.Message);
                }
            }
            else
            {
                //Modificar
                try
                {
                    SqlCommand comando = new SqlCommand();
                    comando.Connection = cn;

                    tablas = "FichaMedica";
                    campos = "FM_MotivoConsulta = '" + txtInfSesion.Text.ToUpper() + "'";
                    condicion = "FichaMedicaId = " + FichaNum;

                    consulta = "Update " + tablas + " Set " + campos + " Where " + condicion;

                    comando.CommandText = consulta;
                    cn.Open();
                    int nFilas = comando.ExecuteNonQuery();
                    if (nFilas > 0)
                        MessageBox.Show("Registro ACTUALIZADO correctamente");
                }
                catch (SqlException ex)
                {
                    MessageBox.Show("ERROR: " + ex.Message);
                }
                cn.Close();
            }
            gpbInforme.Enabled = false;
            Limpiar();
            CargarProcedimientos();
        }
    }
}
