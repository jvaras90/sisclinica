﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using pjDermaBelle.Formularios.Asistencial;
using pjDermaBelle.Formularios.Impresiones;

namespace pjDermaBelle
{
    public partial class frmFichaMedica : Form
    {
        public frmFichaMedica()
        {
            InitializeComponent();
        }

        ClaseDatos obj = new ClaseDatos();
        DermaBelleDataDataContext linq = new DermaBelleDataDataContext();
        FichaMedica oFMedica = new FichaMedica();
        Citas oCitas = new Citas();
        FM_Sintomas oSintomas = new FM_Sintomas();
        FM_AntecPatologicos oFM_AntecPatologicos = new FM_AntecPatologicos();
        FM_AntecCirugias oFM_AntecCirugias = new FM_AntecCirugias();
        FM_AntecAlergias oFM_AntecAlergias = new FM_AntecAlergias();
        FM_AntecFamiliares oFM_AntecFamiliares = new FM_AntecFamiliares();
        FM_Prescripcion oFM_Prescripcion = new FM_Prescripcion();
        FM_ExamPreferencial oFM_ExamPreferencial = new FM_ExamPreferencial();
        FM_ExamPreferencialDetalle oFM_ExamPreferencialDetalle = new FM_ExamPreferencialDetalle();
        FM_AyudaDiagnostica oFM_AyudaDiagnostica = new FM_AyudaDiagnostica();
        FM_Tratamientos oFM_Tratamientos = new FM_Tratamientos();
        FM_Preparados oFM_Preparados = new FM_Preparados();
        FM_Diagnosticos oFM_Diagnosticos = new FM_Diagnosticos();

        public short pCitaId;
        public int FichaNum;
        public char EstadoFicha;
        public int PrA_Id = 0;
        public int Prd_Id = 0;
        public short pGri_Id;
        public short pProfesionId;
        public bool nuevo;
        public bool enProceso = false;
        public string _TipoAtencion;
        public int _opc;
        public int SerId;
        public int MedCod;
        public int _PartesCuerpoId;
        public bool FlagSi;
        public int NumFichaCA;
        public string mensaje;

        void ObtFichaMedicaId(string NumHistoria)
        {
            List<usp_FMedicaIdResult> FichaId = new List<usp_FMedicaIdResult>();
            FichaId = obj.NumeroFicha(frmPacCitados.NumHistoria);
            FichaNum = 0;
            if (FichaId.Count() > 0)
            {
                foreach (usp_FMedicaIdResult reg in FichaId)
                {
                    FichaNum = reg.FichaMedicaId;
                }
            }
        }

        void CargarComponentes(string valor)
        {
            cmbComponente.DataSource = null;

            cmbComponente.DataSource = obj.cmbPrincipios(valor, "%");
            cmbComponente.ValueMember = "PrA_Id";
            cmbComponente.DisplayMember = "PrA_Nombre";
            cmbComponente.Text = "";
        }

        void verificaDx()
        {
            List<usp_FM_DiagnosticosLisResult> lista = new List<usp_FM_DiagnosticosLisResult>();
            lista = obj.dgvDiagnosticos(FichaNum);
            if (lista.Count() > 0)
            { 
                //btnCerrar.Enabled = true;
                btnCerrar.Visible = true;
            }
            else
            {
                //btnCerrar.Enabled = false;
                btnCerrar.Visible = false;
            }
        }

       void Limpiar()
        {
            //Cabecera
            lblPaciente.Text = "";
            lblGrupoSanguineo.Text = "";
            lblDocIdentidad.Text = "";
            lblGradoInstruccion.Text = "";
            lblProfesion.Text = "";
            lblSexo.Text = "";
            lblEdad.Text = "";
            lblLugarNacimiento.Text = "";
            lblProcedencia.Text = "";

            //Motivo de la Consulta
            txtMotivo.Text = "";
            chkAlcohol.Checked = false;
            chkTabaco.Checked = false;
            chkDrogas.Checked = false;
            chkOtros.Checked = false;
            txtObservHabNoc.Text = "";
            txtFUltMenstruacion.Text = "";

            //Examen Físico
            txtPAMaxima.Text = "0";
            txtPAMinima.Text = "0";
            txtPulso.Text = "0";
            txtTemperatura.Text = "0";
            txtPeso.Text = "0";
            txtTalla.Text = "0";
            chkLucido.Checked = true;
            chkOEspacio.Checked = true;
            chkOTiempo.Checked = true;
            chkOPersona.Checked = true;
            cboEstGeneral.Text = "Bueno";
            cboEstHidratacion.Text = "Bueno";
            cboEstNutricional.Text = "Bueno";

            //Datos Complementarios
            dtpProximaCita.Value = DateTime.Today;
            txtRecomendaciones.Text = "";
            txtObservaciones.Text = "";
        }

        void CargarCabecera()
        {
            Limpiar();

            List<usp_FMedicaCabeceraResult> lista = new List<usp_FMedicaCabeceraResult>();
            lista = obj.FMedicaCabecera(lblNumHistoria.Text);
            if (lista.Count() > 0)
            {
                foreach (usp_FMedicaCabeceraResult reg in lista)
                {
                    lblPaciente.Text = reg.Pac_NomComp;
                    lblGrupoSanguineo.Text = reg.Pac_GpoSang;
                    lblDocIdentidad.Text = reg.DocIdent;
                    pGri_Id = short.Parse(reg.Pac_InstruccionId.ToString());
                    lblGradoInstruccion.Text = reg.GrI_Nombre;
                    pProfesionId = short.Parse(reg.Pac_ProfesionId.ToString());
                    lblProfesion.Text = reg.Prf_Nombre;
                    lblSexo.Text = reg.Pac_Sexo;
                    lblEdad.Text = reg.Pac_Edad.ToString();
                    lblLugarNacimiento.Text = reg.Pac_LugNacim;
                    lblProcedencia.Text = reg.Pac_Proced;

                    if (_TipoAtencion == "N")
                    {
                        cboTipoAtencion.Text = "NUEVO";
                        cboTipoAtencion.Enabled = false;
                    }

                    if (_TipoAtencion == "C")
                    {
                        cboTipoAtencion.Text = "CONTINUADOR";
                        cboTipoAtencion.Enabled = true;
                    }

                    if (_TipoAtencion == "R")
                    {
                        cboTipoAtencion.Text = "CONTINUADOR";
                        cboTipoAtencion.Enabled = true;
                    }
                }
            }
        }

        void Validar_EnProceso()
        {
            enProceso = false;
            List<usp_FMedicaProcesoResult> FichaRec = new List<usp_FMedicaProcesoResult>();
            FichaRec = obj.FMedicaProceso(FichaNum);
            if (FichaRec.Count() > 0)
            {
                foreach (usp_FMedicaProcesoResult reg in FichaRec)
                    if (string.Equals(reg.FM_Fecha.ToString().Substring(0, 10), lblFecha.Text))
                    {
                        if (string.Equals(reg.FM_Estado.ToString(), "2"))
                        {
                            enProceso = true;
                            nuevo = false;
                            _TipoAtencion = reg.Pac_TipoAtencion.ToString();
                        }
                        else
                        {
                            _TipoAtencion = "C".ToString();
                            enProceso = false;
                            nuevo = true;
                        }
                    }
                    else
                    {
                        _TipoAtencion = "C".ToString();
                        enProceso = false;
                        nuevo = true;
                    }
            }
            else
            {
                _TipoAtencion = "N".ToString();
                cboTipoAtencion.Text = "NUEVO";
                enProceso = false;
                nuevo = true;
            }
        }

        void CargarFicha()
        {
            CargarCabecera();
            List<usp_FMedicaProcesoResult> FichaProceso = new List<usp_FMedicaProcesoResult>();
            FichaProceso = obj.FMedicaProceso(FichaNum);
            if (FichaProceso.Count() > 0)
            {
                foreach (usp_FMedicaProcesoResult reg in FichaProceso)
                {
                    //Motivo de la Consulta
                    txtMotivo.Text = reg.FM_MotivoConsulta;
                    chkAlcohol.Checked = Convert.ToBoolean(reg.FM_Alcohol);
                    chkTabaco.Checked = Convert.ToBoolean(reg.FM_Tabaco);
                    chkDrogas.Checked = Convert.ToBoolean(reg.FM_Drogas);
                    chkOtros.Checked = Convert.ToBoolean(reg.FM_OtrosHabNoc);
                    txtObservHabNoc.Text = reg.FM_ObservHabNoc;
                    txtFUltMenstruacion.Text = reg.FM_FechaUltMenst;

                    //Motivo de la Consulta
                    CargarSintomas();

                    //Examen Físico
                    txtPAMaxima.Text = reg.FM_PAMax.ToString();
                    if (string.IsNullOrEmpty(txtPAMaxima.Text))
                    {  txtPAMaxima.Text = "0";}
                    txtPAMinima.Text = reg.FM_PAMin.ToString();
                    if (string.IsNullOrEmpty(txtPAMinima.Text))
                    {  txtPAMinima.Text = "0";}
                    txtPulso.Text = reg.FM_Pulso.ToString();
                    if (string.IsNullOrEmpty(txtPulso.Text))
                    {  txtPulso.Text = "0";}
                    txtTemperatura.Text = reg.FM_Temp.ToString();
                    if (string.IsNullOrEmpty(txtTemperatura.Text))
                    {  txtTemperatura.Text = "0";}
                    txtPeso.Text = reg.FM_Peso.ToString();
                    if (string.IsNullOrEmpty(txtPeso.Text))
                    {  txtPeso.Text = "0";}
                    txtTalla.Text = reg.FM_Talla.ToString();
                    if (string.IsNullOrEmpty(txtTalla.Text))
                    {  txtTalla.Text = "0";}

                    chkLucido.Checked = Convert.ToBoolean(reg.FM_Lucido);
                    chkOEspacio.Checked = Convert.ToBoolean(reg.FM_OEspacio);
                    chkOTiempo.Checked = Convert.ToBoolean(reg.FM_OTiempo);
                    chkOPersona.Checked = Convert.ToBoolean(reg.FM_OPersona);
                    cboEstGeneral.Text = reg.FM_EstGeneral;
                    cboEstHidratacion.Text = reg.FM_EstHidratacion;
                    cboEstNutricional.Text = reg.FM_EstNutricion;

                    //Datos Complementarios
                    dtpProximaCita.Value = Convert.ToDateTime(reg.FM_ProxCita);
                    txtRecomendaciones.Text = reg.FM_Recomend;
                    txtObservaciones.Text = reg.FM_ObservGenerales;
                }
            }
        }

        void CargardgvPrescripcion()
        {
            dgvPrescripcion.DataSource = obj.DgvPrescripcion(FichaNum);

            for (int indice = 0; indice < dgvPrescripcion.Rows.Count; indice++)
            {
                dgvPrescripcion.Columns[2].Width = 30;       //Item
                dgvPrescripcion.Columns[3].Visible = false;  //Codigo
                dgvPrescripcion.Columns[4].Width = 200;      //Comercial
                dgvPrescripcion.Columns[5].Width = 200;      //Generico
                dgvPrescripcion.Columns[6].Width = 210;      //Indicaciones
                dgvPrescripcion.Columns[7].Visible = false;  //PrA_Cod
            }
        }

        void Tratamientos()
        {
            cmbTratamientos.DataSource = obj.cmbTratamientos();
            cmbTratamientos.ValueMember = "Tar_Id";
            cmbTratamientos.DisplayMember = "Tar_Nombre";
        }

        void CargardgvTratamientos()
        {
            dgvTratamientos.DataSource = obj.dgvTratamientos(FichaNum);

            for (int indice = 0; indice < dgvTratamientos.Rows.Count; indice++)
            {
                dgvTratamientos.Columns[2].Visible = false;  //FichaMedicaId
                dgvTratamientos.Columns[3].Visible = false;  //TratamientoId
                dgvTratamientos.Columns[4].Width = 550;      //Tratamiento
                dgvTratamientos.Columns[5].Width = 55;       //Numero de sesiones
                dgvTratamientos.Columns[6].Visible = false;  //Observaciones
            }
        }

        void Patologias()
        {
            cboAntecedente.DataSource = obj.CboPatologias();
            cboAntecedente.ValueMember = "PatologiaId";
            cboAntecedente.DisplayMember = "PatologiaNombre";
        }

        void AyudaDiagnostica()
        {
            cmbTipoExamen.DataSource = obj.cboAyudaDiagnostica();
            cmbTipoExamen.ValueMember = "TAyudaDiagId";
            cmbTipoExamen.DisplayMember = "TAyudaDiagNombre";
        }

        void Cirugias()
        {
            cboAntecedente.DataSource = obj.CboCirugias();
            cboAntecedente.ValueMember = "Qui_Id";
            cboAntecedente.DisplayMember = "Qui_Nombre";
        }

        void Alergias()
        {
            cboAntecedente.DataSource = obj.CboAlergias();
            cboAntecedente.ValueMember = "AlergiaId";
            cboAntecedente.DisplayMember = "AlergiaNombre";
        }

        void CargarAlergias()
        {
            txtAlergias.Text = "Alergias referidas por el paciente";
        }

        void CargardgvAyudaDiagnostica()
        {
            dgvAyudaDiagnostica.DataSource = obj.FM_AyudaDiagnosticaLis(lblNumHistoria.Text);

            for (int indice = 0; indice < dgvAyudaDiagnostica.Rows.Count; indice++)
            {
                dgvAyudaDiagnostica.Columns[2].Visible = false;  //FichaMedicaId
                dgvAyudaDiagnostica.Columns[3].Visible = false;  //Id
                dgvAyudaDiagnostica.Columns[4].Width = 80;       //Fecha
                dgvAyudaDiagnostica.Columns[5].Width = 150;      //Examen
                dgvAyudaDiagnostica.Columns[6].Width = 370;      //Informe
            }

        }

        void CargardgvdAntecedentes(int opc)
        {
            lblFamiliar.Visible = false;
            cmbFamiliar.Visible = false;

            dgvAntecedentes.DataSource = null;
            _opc = opc;
            switch (opc)
            {
                case 1:
                    // Cargar Antecedentes Patológicos
                    Patologias();
                    gpbAntecedentes.Text = "Antecedentes Patológicos";
                    lblAntecedente.Text = "Patologías";
                    dgvAntecedentes.DataSource = obj.DgvAntecPatologicos(lblNumHistoria.Text);
                    break;
                case 2:
                    // Cargar Antecedentes Quirúrgicos
                    Cirugias();
                    gpbAntecedentes.Text = "Antecedentes Quirúrgicos";
                    lblAntecedente.Text = "Cirugías";
                    dgvAntecedentes.DataSource = null;
                    dgvAntecedentes.DataSource = obj.DgvAntecQuirurgicos(lblNumHistoria.Text);
                    break;
                case 3:
                    // Cargar Antecedentes Alérgicos
                    Alergias();
                    gpbAntecedentes.Text = "Antecedentes Alérgicos";
                    lblAntecedente.Text = "Alergias";
                    dgvAntecedentes.DataSource = null;
                    dgvAntecedentes.DataSource = obj.DgvAntecAlergicos(lblNumHistoria.Text);
                    break;
                case 4:
                    // Cargar Antecedentes Familiares
                    //Familiares();
                    Patologias();
                    gpbAntecedentes.Text = "Antecedentes Familiares";
                    lblAntecedente.Text = "Patologías";

                    cmbFamiliar.DataSource = obj.CboFamiliares();
                    cmbFamiliar.ValueMember = "FamiliarId";
                    cmbFamiliar.DisplayMember = "FamiliarNombre";

                    lblFamiliar.Visible = true;
                    cmbFamiliar.Text = " ";
                    cmbFamiliar.Visible = true;

                    dgvAntecedentes.DataSource = null;
                    dgvAntecedentes.DataSource = obj.DgvAntecFamiliares(lblNumHistoria.Text);
                    break;
            }

            //Configurar Grid de Antecedentes Patológicos
            for (int indice = 0; indice < dgvAntecedentes.Rows.Count; indice++)
            {
                if (opc == 4)
                {
                    dgvAntecedentes.Columns[2].Visible = false;  //FichaMedicaId
                    dgvAntecedentes.Columns[3].Visible = false;  //FamiliarId
                    dgvAntecedentes.Columns[5].Visible = false;  //PatologiaId
                    dgvAntecedentes.Columns[4].Width = 100;       //Familiar
                    dgvAntecedentes.Columns[6].Width = 260;      //Patología
                    dgvAntecedentes.Columns[7].Width = 400;      //Observaciones
                }
                else
                {
                    dgvAntecedentes.Columns[2].Visible = false;  //FichaMedicaId
                    dgvAntecedentes.Columns[3].Visible = false;  //Id
                    dgvAntecedentes.Columns[4].Width = 260;      //Patología
                    dgvAntecedentes.Columns[5].Width = 500;      //Observaciones
                }
            }
        }

        void AddAntecedentes(int opc)
        {
            switch (opc)
            {
                case 1:
                    // Actualiza Antecedente Patológico
                    oFM_AntecPatologicos.FichaMedicaId = Convert.ToSByte(FichaNum);
                    oFM_AntecPatologicos.Pac_HC = lblNumHistoria.Text;
                    oFM_AntecPatologicos.PatologiaId = Convert.ToInt16(cboAntecedente.SelectedValue.ToString());
                    oFM_AntecPatologicos.PatologiaObserv = txtAntecedenteObserv.Text;
                    obj.FM_AntecPatologicosAdd(oFM_AntecPatologicos);
                    break;

                case 2:
                    // Actualiza Antecedente Quirúrgico
                    oFM_AntecCirugias.FichaMedicaId = Convert.ToSByte(FichaNum);
                    oFM_AntecCirugias.Pac_HC = lblNumHistoria.Text;
                    oFM_AntecCirugias.CirugiaId = Convert.ToInt16(cboAntecedente.SelectedValue.ToString());
                    oFM_AntecCirugias.CirugiaObserv = txtAntecedenteObserv.Text;
                    obj.FM_AntecQuirurgicosAdd(oFM_AntecCirugias);
                    break;
                case 3:
                    // Actualiza Antecedente Alérgico
                    oFM_AntecAlergias.FichaMedicaId = Convert.ToSByte(FichaNum);
                    oFM_AntecAlergias.Pac_HC = lblNumHistoria.Text;
                    oFM_AntecAlergias.AlergiaId = Convert.ToInt16(cboAntecedente.SelectedValue.ToString());
                    oFM_AntecAlergias.AlergiaObserv = txtAntecedenteObserv.Text;
                    obj.FM_AntecAlergicosAdd(oFM_AntecAlergias);
                    break;
                case 4:
                    // Actualiza Antecedente Familiar
                    oFM_AntecFamiliares.FichaMedicaId = Convert.ToSByte(FichaNum);
                    oFM_AntecFamiliares.Pac_HC = lblNumHistoria.Text;
                    oFM_AntecFamiliares.FamiliarId = Convert.ToInt16(cmbFamiliar.SelectedValue.ToString());
                    oFM_AntecFamiliares.PatologiaId = Convert.ToInt16(cboAntecedente.SelectedValue.ToString());
                    oFM_AntecFamiliares.AntecFamObserv = txtAntecedenteObserv.Text;
                    obj.FM_AntecFamiliaresAdd(oFM_AntecFamiliares);
                    break;
            }

            CargardgvdAntecedentes(opc);
            cboAntecedente.Text = "";
            cboAntecedente.Enabled = true;
            txtAntecedenteObserv.Text = "";
        }

        void UpdAntecedentes(int opc)
        {
            switch (opc)
            {
               case 1:
                    // Actualiza Antecedente Patológico
                    oFM_AntecPatologicos.FichaMedicaId = Convert.ToSByte(FichaNum);
                    oFM_AntecPatologicos.Pac_HC = lblNumHistoria.Text;
                    oFM_AntecPatologicos.PatologiaId = Convert.ToInt16(cboAntecedente.SelectedValue.ToString());
                    oFM_AntecPatologicos.PatologiaObserv = txtAntecedenteObserv.Text;
                    obj.FM_AntecPatologicosUpd(oFM_AntecPatologicos);
                    break;

               case 2:
                    // Actualiza Antecedente Quirúrgico
                    oFM_AntecCirugias.FichaMedicaId = Convert.ToSByte(FichaNum);
                    oFM_AntecCirugias.Pac_HC = lblNumHistoria.Text;
                    oFM_AntecCirugias.CirugiaId = Convert.ToInt16(cboAntecedente.SelectedValue.ToString());
                    oFM_AntecCirugias.CirugiaObserv = txtAntecedenteObserv.Text;
                    obj.FM_AntecQuirurgicosUpd(oFM_AntecCirugias);
                    break;
               case 3:
                    // Actualiza Antecedentes Alérgicos
                    oFM_AntecAlergias.FichaMedicaId = Convert.ToSByte(FichaNum);
                    oFM_AntecAlergias.Pac_HC = lblNumHistoria.Text;
                    oFM_AntecAlergias.AlergiaId = Convert.ToInt16(cboAntecedente.SelectedValue.ToString());
                    oFM_AntecAlergias.AlergiaObserv = txtAntecedenteObserv.Text;
                    obj.FM_AntecAlergicosUpd(oFM_AntecAlergias);
                    break;
               case 4:
                    // Actualiza Antecedentes Familiares
                    oFM_AntecFamiliares.FichaMedicaId = Convert.ToSByte(FichaNum);
                    oFM_AntecFamiliares.Pac_HC = lblNumHistoria.Text;
                    oFM_AntecFamiliares.FamiliarId = Convert.ToInt16(cmbFamiliar.SelectedValue.ToString());
                    oFM_AntecFamiliares.PatologiaId = Convert.ToInt16(cboAntecedente.SelectedValue.ToString());
                    oFM_AntecFamiliares.AntecFamObserv = txtAntecedenteObserv.Text;
                    obj.FM_AntecFamiliaresUpd(oFM_AntecFamiliares);
                    cmbFamiliar.Text = "";
                    cmbFamiliar.Enabled = true;
                    break;
            }

            CargardgvdAntecedentes(opc);
            cboAntecedente.Text = "";
            cboAntecedente.Enabled = true;
            txtAntecedenteObserv.Text = "";
            btnAddAntecedentes.Text = "Agregar";
        }

        void DelAntecedentes(int opc)
        {
            switch (opc)
            { 
                case 1:
                    // Elimina Antecedente Patológico
                     oFM_AntecPatologicos.FichaMedicaId = Convert.ToSByte(FichaNum);
                     oFM_AntecPatologicos.Pac_HC = lblNumHistoria.Text;
                     oFM_AntecPatologicos.PatologiaId = Convert.ToInt16(cboAntecedente.SelectedValue.ToString());
                     oFM_AntecPatologicos.PatologiaObserv = txtAntecedenteObserv.Text;
                     obj.FM_AntecPatologicosDel(oFM_AntecPatologicos);
                     break;
                case 2:
                    // Elimina Antecedente Quirúrgico
                     oFM_AntecCirugias.FichaMedicaId = Convert.ToSByte(FichaNum);
                     oFM_AntecCirugias.Pac_HC = lblNumHistoria.Text;
                     oFM_AntecCirugias.CirugiaId = Convert.ToInt16(cboAntecedente.SelectedValue.ToString());
                     oFM_AntecCirugias.CirugiaObserv = txtAntecedenteObserv.Text;
                     obj.FM_AntecQuirurgicosDel(oFM_AntecCirugias);
                     break;
                case 3:
                    // Elimina Antecedente Alérgico
                     oFM_AntecAlergias.FichaMedicaId = Convert.ToSByte(FichaNum);
                     oFM_AntecAlergias.Pac_HC = lblNumHistoria.Text;
                     oFM_AntecAlergias.AlergiaId = Convert.ToInt16(cboAntecedente.SelectedValue.ToString());
                     oFM_AntecAlergias.AlergiaObserv = txtAntecedenteObserv.Text;
                     obj.FM_AntecAlergicosDel(oFM_AntecAlergias);
                     break;
                case 4:
                    // Actualiza Antecedente Familiar
                     oFM_AntecFamiliares.FichaMedicaId = Convert.ToSByte(FichaNum);
                     oFM_AntecFamiliares.Pac_HC = lblNumHistoria.Text;
                     oFM_AntecFamiliares.FamiliarId = Convert.ToInt16(cmbFamiliar.SelectedValue.ToString());
                     oFM_AntecFamiliares.PatologiaId = Convert.ToInt16(cboAntecedente.SelectedValue.ToString());
                     oFM_AntecFamiliares.AntecFamObserv = txtAntecedenteObserv.Text;
                     obj.FM_AntecFamiliaresDel(oFM_AntecFamiliares);
                     break;
            }
            CargardgvdAntecedentes(opc);
            cboAntecedente.Text = " ";
            txtAntecedenteObserv.Text = " ";
        }

        void GuardarCabecera()
        {
            //Cabecera
            oFMedica.Pac_TipoAtencion = System.Convert.ToChar(cboTipoAtencion.Text.Substring(0, 1));
            oFMedica.Pac_HC = lblNumHistoria.Text;
            oFMedica.GrI_Id = pGri_Id;
            oFMedica.ProfesionId = pProfesionId;
            oFMedica.Pac_Edad = short.Parse(lblEdad.Text);
            oFMedica.FM_Servicio = short.Parse(SerId.ToString());
            oFMedica.FM_MedCod = int.Parse(MedCod.ToString());
            oFMedica.FM_ProxCita = dtpProximaCita.Value.Date;
            oFMedica.FM_Estado = EstadoFicha;
            if (nuevo)
            { obj.FMedicaAdicionar(oFMedica); }
            else
            { obj.FMedicaActualizar(oFMedica); }
                        
            lblGuardando.Visible = true;
            pbGrabar.Show();
            timer1.Start();
            nuevo = false;

            ObtFichaMedicaId(lblNumHistoria.Text);
        }

        void ActualizaCitas()
        {
            //Actualizar la tabla Citas
            oCitas.CitaId = short.Parse(pCitaId.ToString());
            oCitas.FM_Estado = EstadoFicha;
            oCitas.FichaMedicaId = Convert.ToSByte(FichaNum);
            obj.FMedicaActCitas(oCitas);
        }

        void GuardarMotivoConsulta()
        {
            //Motivo de la Consulta
            oFMedica.FichaMedicaId = Convert.ToSByte(FichaNum);
            oFMedica.FM_MotivoConsulta = txtMotivo.Text;
            oFMedica.FM_Alcohol = chkAlcohol.Checked;
            oFMedica.FM_Tabaco = chkTabaco.Checked;
            oFMedica.FM_Drogas = chkDrogas.Checked;
            oFMedica.FM_OtrosHabNoc = chkOtros.Checked;
            obj.FMedicaActualizar(oFMedica);

            //Sintomas
            // Primero borrar los registros previos
            oSintomas.FichaMedicaId = Convert.ToSByte(FichaNum);
            obj.FM_SintomasDel(oSintomas);

            // Luego recorrer el grid y grabar los registros seleccionados
            for (int indice = 0; indice < dgvSintomas.Rows.Count; indice++)
            {
                if (Convert.ToBoolean(dgvSintomas.Rows[indice].Cells[0].Value))
                {
                    oSintomas.FichaMedicaId = Convert.ToSByte(FichaNum);
                    oSintomas.SintomaId = short.Parse(dgvSintomas.Rows[indice].Cells[1].Value.ToString());
                    oSintomas.SintomaObserv = dgvSintomas.Rows[indice].Cells[3].Value.ToString();
                    oSintomas.SintomaSelec = Convert.ToBoolean(dgvSintomas.Rows[indice].Cells[0].Value);
                    obj.FM_SintomasAdd(oSintomas);
                }
            }

            lblGuardando.Visible = true;
            pbGrabar.Show();
            timer1.Start();
            nuevo = false;
        }

        void GuardarExamenFisico()
        {
            //Examen Físico
            oFMedica.FM_ObservHabNoc = txtObservHabNoc.Text;
            oFMedica.FM_PAMax = short.Parse(txtPAMaxima.Text);
            oFMedica.FM_PAMin = short.Parse(txtPAMinima.Text);
            oFMedica.FM_Pulso = short.Parse(txtPulso.Text);
            oFMedica.FM_Temp = Convert.ToDecimal(txtTemperatura.Text);
            oFMedica.FM_Peso = Convert.ToDecimal(txtPeso.Text);
            oFMedica.FM_Talla = Convert.ToDecimal(txtTalla.Text);
            oFMedica.FM_Lucido = chkLucido.Checked;
            oFMedica.FM_OEspacio = chkOEspacio.Checked;
            oFMedica.FM_OTiempo = chkOTiempo.Checked;
            oFMedica.FM_OPersona = chkOPersona.Checked;
            oFMedica.FM_EstGeneral = cboEstGeneral.Text;
            oFMedica.FM_EstHidratacion = cboEstHidratacion.Text;
            oFMedica.FM_EstNutricion = cboEstNutricional.Text;
            oFMedica.FM_Estado = EstadoFicha;
            oFMedica.FichaMedicaId = Convert.ToSByte(FichaNum);
            obj.FMedicaActualizar(oFMedica);

            oFM_ExamPreferencial.FichaMedicaId = Convert.ToSByte(FichaNum);
            obj.FM_PartesCuerpoDel(oFM_ExamPreferencial);
            //// Luego recorrer el grid y grabar los registros seleccionados
            for (int ind = 0; ind < dgvPartesCuerpo.Rows.Count; ind++)
            {
                if (!string.IsNullOrWhiteSpace(dgvPartesCuerpo.Rows[ind].Cells[2].Value.ToString()))
                {
                    oFM_ExamPreferencial.ParteCuerpoId = short.Parse(dgvPartesCuerpo.Rows[ind].Cells[0].Value.ToString());
                    oFM_ExamPreferencial.ParteCuerpoObserv = dgvPartesCuerpo.Rows[ind].Cells[2].Value.ToString().ToUpper();
                    oFM_ExamPreferencial.ParteCuerpoSelec = true;
                    obj.FM_PartesCuerpoAdd(oFM_ExamPreferencial);
                }
            }

            lblGuardando.Visible = true;
            pbGrabar.Show();
            timer1.Start();
            nuevo = false;
        }

        void GuardarAyudaDiagnostica()
        {
            //oFM_AyudaDiagnostica.FichaMedicaId = Convert.ToSByte(FichaNum);
            //oFM_AyudaDiagnostica.AyudaDiagFecha = dtpFechaExamen.Value.Date;
            //oFM_AyudaDiagnostica.AyudaDiagTipo = int.Parse(cmbTipoExamen.SelectedValue.ToString());
            //oFM_AyudaDiagnostica.AyudaDiagInforme = txtInfExamen.Text;
            //obj.FM_AyudaDiagnosticaAdd(oFM_AyudaDiagnostica);
            //CargardgvAyudaDiagnostica();
            //lblGuardando.Visible = true;
            //pbGrabar.Show();
            //timer1.Start();
            //nuevo = false;
        }

        void GuardarDatosComplementarios()
        {
            // Datos Complementarios
            oFMedica.FM_ProxCita = dtpProximaCita.Value.Date;
            oFMedica.FM_Recomend = txtRecomendaciones.Text;
            oFMedica.FM_ObservGenerales = txtObservaciones.Text;

            oFMedica.FichaMedicaId = Convert.ToSByte(FichaNum);
            obj.FMedicaActualizar(oFMedica);

            lblGuardando.Visible = true;
            pbGrabar.Show();
            timer1.Start();
            nuevo = false;
        }

        void GuardarFicha()
        {
            GuardarCabecera();
            GuardarMotivoConsulta();
            GuardarExamenFisico();
            //GuardarPlanTrabajo();
            //GuardarDiagnostico();
            //GuardarPrescripcion();
            GuardarDatosComplementarios();
            ActualizaCitas();
        }

        private byte[] byimagen;
        void CargarImagen()
        {
            SqlConnection con1 = new SqlConnection();
            con1.ConnectionString = "Data Source=PC;Initial Catalog=Dermabelle;uid=DBUser;pwd=dbuser;Integrated Security=False";

            SqlCommand cmd = new SqlCommand("Select PartesCuerpoImagen From PartesCuerpo Where PartesCuerpoId = @PartesCuerpoId", con1);
            cmd.Parameters.AddWithValue("@PartesCuerpoId", _PartesCuerpoId);
            con1.Open();

            SqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                byimagen = (byte[])dr.GetValue(0);
            }

            MemoryStream ms = new MemoryStream(byimagen);
            pbxParteCuerpo.Image = Image.FromStream(ms);
            con1.Close();
        }
        
        void CargarSegmentos()
        {
            int ind = dgvPartesCuerpo.CurrentCell.RowIndex;
            _PartesCuerpoId = int.Parse(dgvPartesCuerpo.Rows[ind].Cells[0].Value.ToString());

            CargarImagen();
            dgvSegmentos.DataSource = obj.DgvSegmentos(FichaNum, _PartesCuerpoId);

            for (int indice = 0; indice < dgvSegmentos.Rows.Count; indice++)
            {
                dgvSegmentos.Columns[0].Visible = false;    //PartesCuerpoId
                dgvSegmentos.Columns[1].Visible = false;    //SegmentosId
                dgvSegmentos.Columns[2].ReadOnly = true;    //Segmento
                dgvSegmentos.Columns[2].Width = 300;        //Segmento
                dgvSegmentos.Columns[3].Width = 445;        //Observación
            }
        }

        void CboMedicinas()
        {
            //cboMedicamentos.DataSource = obj.CboMedicamentos(cboMedicamentos.Text);
            //cboMedicamentos.ValueMember = "Prd_Id";
            //cboMedicamentos.DisplayMember = "Prd_Descripcion";
        }

        void CargarSintomas()
        {
            dgvSintomas.DataSource = obj.DgvSintomasVer(FichaNum);
            for (int indice = 0; indice < dgvSintomas.Rows.Count; indice++)
            {
                dgvSintomas.Columns[1].Visible = false;
                dgvSintomas.Columns[4].Visible = false;
                dgvSintomas.Columns[2].Width = 250;
                dgvSintomas.Columns[3].Width = 500;
                dgvSintomas.Rows[indice].Cells[2].ReadOnly = true;
                dgvSintomas.Rows[indice].Cells[3].ReadOnly = true;
                dgvSintomas.Rows[indice].Cells[0].Value = dgvSintomas.Rows[indice].Cells[4].Value;
            }
        }

        private void frmFichaMedica_Load(object sender, EventArgs e)
        {
            Limpiar();
            DateTime FechaHoy = DateTime.Today;
            DateTime HoraHoy = DateTime.Now;
            cboTipoAtencion.Text = "NUEVO";
            _TipoAtencion = "N";
            //FichaNum = 0;
            lblFecha.Text = string.Format(DateTime.Now.ToString("dd/MM/yyyy"));  //FechaHoy.ToString();

            lblHora.Text = string.Format(DateTime.Now.ToString("HH:mm")); //HoraHoy.ToString(); 
            lblNumHistoria.Text = frmPacCitados.NumHistoria;
            ObtFichaMedicaId(lblNumHistoria.Text);

            if (FichaNum > 0)
            {
                Validar_EnProceso();
                if (enProceso)
                {
                    CargarFicha();
                    //btnCerrar.Enabled = true;
                    btnCerrar.Visible = true;
                }
                else
                {
                    Limpiar();
                    CargarCabecera();
                    FichaNum = 0;
                    CargarSintomas();
                    //btnCerrar.Enabled = false;
                    btnCerrar.Visible = false;
                }
            }
            else 
            {
                FichaNum = 0; 
                CargarCabecera();
                CargarSintomas();
                txtPAMaxima.Text = "0";
                txtPAMinima.Text = "0";
                txtPulso.Text = "0";
                txtTemperatura.Text = "0";
                txtPeso.Text = "0";
                txtTalla.Text = "0";
                nuevo = true; 
            }

            if (lblSexo.Text == "Masculino")
                {
                    txtFUltMenstruacion.Text = "No Aplica";
                    txtFUltMenstruacion.ReadOnly = true;
                }

            EstadoFicha = Convert.ToChar("2");
            GuardarCabecera();
            lblGuardando.Visible = false;
            pbGrabar.Hide();
            //Diagnosticos();
            CargarAlergias();

            if (_TipoAtencion == "N")
            {
                tabControl1.SelectedTab = tbpMotivoConsulta;
                txtMotivo.Focus();
            }
            else
            {
                tabControl1.SelectedTab = tbpConsultaAnterior;
            }
            verificaDx();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            pbGrabar.Increment(500);
            if (pbGrabar.Value == pbGrabar.Maximum)
            {
                timer1.Stop();
                lblGuardando.Visible = false;
                pbGrabar.Hide();
                pbGrabar.Value = 0;
            }
        }

        private void tabPage1_Leave(object sender, EventArgs e)
        {
            //Motivo de la Consulta
            GuardarMotivoConsulta();
        }
        
        private void tabPage2_Leave(object sender, EventArgs e)
        {
            //Examen Fisico
            GuardarExamenFisico();
        }

         private void tabPage3_Leave(object sender, EventArgs e)
        {
            //Ayuda Diagnóstica
            //GuardarAyudaDiagnostica();
        }

         private void tabPage4_Leave(object sender, EventArgs e)
         {
             //Plan de Trabajo
             //GuardarPlanTrabajo();
         }

         private void tabPage5_Leave(object sender, EventArgs e)
         {
             //Diagnósticos
             //GuardarDiagnosticos();
         }

         private void tabPage6_Leave(object sender, EventArgs e)
         {
             //Prescripción
             //GuardarPresripcion();
         }

         private void tabPage7_Leave(object sender, EventArgs e)
         {
             //Datos Complementarios
             GuardarDatosComplementarios();
         }

         private void btnCerrar_Click(object sender, EventArgs e)
         {
             if ((MessageBox.Show("Después de CERRAR la ficha ya no podrá modificar nada de lo que ha registrado en la misma ¿Seguro que desea CERRAR ?", "Aviso",
                      MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                      MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes))
             {
                 EstadoFicha = Convert.ToChar("1");
                 GuardarFicha();
                 //PruebaPdf prbpdf = new PruebaPdf();
                 //prbpdf.FichaNum = FichaNum;
                 //prbpdf.ShowDialog();
                 this.Close();
             }
         }

         private void btnHistorial_Click(object sender, EventArgs e)
         {
             GuardarFicha();
             frmHistorial frm = new frmHistorial();
             frm.lblNumHistoria.Text = lblNumHistoria.Text;
             frm.lblPaciente.Text = lblPaciente.Text;
             frm.lblGrupoSanguineo.Text = lblGrupoSanguineo.Text;
             frm.lblDocIdentidad.Text = lblDocIdentidad.Text;
             frm.lblSexo.Text = lblSexo.Text;
             frm.lblProcedencia.Text = lblProcedencia.Text;
             frm.ShowDialog();
         }

         private void tabPage8_Leave(object sender, EventArgs e)
         {
             //Motivo de Consulta (Anamnesis)
             GuardarMotivoConsulta();
         }

         private void tabPage9_Leave(object sender, EventArgs e)
         {
             //Motivo de Consulta (Antecedentes)
             GuardarMotivoConsulta();
         }

         private void tabPage10_Leave(object sender, EventArgs e)
         {
             //Motivo de Consulta (Diagnósticos Anteriores)
             GuardarMotivoConsulta();
         }

         private void dgvSintomas_CellContentClick(object sender, DataGridViewCellEventArgs e)
         {
             if (e.RowIndex == -1)
                 return;

             if (dgvSintomas.Columns[e.ColumnIndex].Name == "Seleccion")
             {
                 DataGridViewRow row = dgvSintomas.Rows[e.RowIndex];
   
                 DataGridViewCheckBoxCell cellSelecion = row.Cells["Seleccion"] as DataGridViewCheckBoxCell;

                 int indice = dgvSintomas.CurrentCell.RowIndex;

                 if (Convert.ToBoolean(cellSelecion.Value))
                 {
                     dgvSintomas.Rows[indice].Cells[3].ReadOnly = false;
                 }
                 else
                 {
                     dgvSintomas.Rows[indice].Cells[3].Value = " ";
                     dgvSintomas.Rows[indice].Cells[3].ReadOnly = true;
                 }
             }
         }

         private void dgvSintomas_CurrentCellDirtyStateChanged(object sender, EventArgs e)
         {
             if (dgvSintomas.IsCurrentCellDirty)
                {
                    dgvSintomas.CommitEdit(DataGridViewDataErrorContexts.Commit);
                }
         }

         private void tbpAntecedentes_Enter(object sender, EventArgs e)
         {
             cboAntecedente.Text = "";
             btnAntecPatol_Click(null, null);
             txtAntecedenteObserv.Focus();
         }

         private void frmFichaMedica_FormClosed(object sender, FormClosedEventArgs e)
         {
             GuardarFicha();
         }

         private void tbpPrescripcion_Enter(object sender, EventArgs e)
         {
             //Prescripcion
             FlagSi = true;
             muestralistbox = true;
             CargardgvPrescripcion();
             cmbVigencia.DataSource = obj.CmbVigencias();
             cmbVigencia.ValueMember = "VigId";
             cmbVigencia.DisplayMember = "VigNombre";
             cmbVigencia.Text = "1 MES";
         }

         private void cboPrinActivoFA_SelectionChangeCommitted(object sender, EventArgs e)
         {
             PrA_Id = Convert.ToInt16(cmbCriterio.SelectedValue.ToString());
         }

         private void cboMedicamentos_SelectionChangeCommitted(object sender, EventArgs e)
         {
             PrA_Id = Convert.ToInt16(cmbCriterio.SelectedValue.ToString());
             //Prd_Id = Convert.ToInt16(cboMedicamentos.SelectedValue.ToString());
         }

         private void cboMedicamentos_Enter(object sender, EventArgs e)
         {
             PrA_Id = Convert.ToInt16(cmbCriterio.SelectedValue.ToString());
         }
        
         int NumItem(int FichaNum)
         { 
             int n = 1;
             List<usp_FMPrescripcionLisResult> lista = new List<usp_FMPrescripcionLisResult>();
             lista = obj.DgvPrescripcion(FichaNum);

             if (lista.Count() > 0)
             {
                 foreach (usp_FMPrescripcionLisResult reg in lista)
                 {
                     n = int.Parse(reg.Item.ToString())+1;
                 }
             }    
             return n;
         }

         private void btnAddPresc_Click(object sender, EventArgs e)
         {
             muestralistbox = false; 
             cmbVigencia.Enabled = false;
             if (string.IsNullOrEmpty(txtPrdDescripcion.Text) | string.IsNullOrEmpty(txtIndicaciones.Text))
             {
                 MessageBox.Show("No puede dejar en blanco el nombre del medicamento y/o las indicaciones");
                 return;
             }

             oFM_Prescripcion.FichaMedicaId = Convert.ToSByte(FichaNum);
             oFM_Prescripcion.Prd_Id = int.Parse(txtPrdId.Text);
             oFM_Prescripcion.RpNomComercial = txtPrdDescripcion.Text;
             oFM_Prescripcion.RpIndicaciones = txtIndicaciones.Text;
             oFM_Prescripcion.RpVigenciaId = int.Parse(cmbVigencia.SelectedValue.ToString());
             oFM_Prescripcion.RpCaduca = lblRpCaduca.Text.Substring(11,10);
             if (string.Equals(btnAddPresc.Text,"Agregar"))
             {
                 oFM_Prescripcion.RpItem = Convert.ToSByte(NumItem(FichaNum));
                 obj.FM_PrescripcionAdd(oFM_Prescripcion);
             }
             else
             {
                 obj.FM_PrescripcionUpd(oFM_Prescripcion);
             }


             FlagSi = true;
             CargardgvPrescripcion();

             txtPrdId.Text = "";
             txtPrdDescripcion.Text = "";
             txtPrdDescripcion.Enabled = true;
             txtIndicaciones.Text = "";
             lstMedicinas.Visible = false;
             rbtGenerico.Checked = false;
             rbtLaboratorio.Checked = false;

             btnAddPresc.Text = "Agregar";

             rbtGenerico.Checked = false;
             rbtLaboratorio.Checked = false;
             rbtPreparados.Checked = false;
             lblCriterio.Text = "Criterio";
             cmbCriterio.Text = "";
             cmbCriterio.Enabled = true;

             gpbDatos.Visible = false;
             dgvPreparados.Visible = false;

             muestralistbox = true;
         }

         private void dgvAntecedentes_CellClick(object sender, DataGridViewCellEventArgs e)
         {
             int indice = dgvAntecedentes.CurrentCell.RowIndex;
             if (!string.Equals(FichaNum.ToString(),dgvAntecedentes.Rows[indice].Cells[2].Value.ToString()))
             { 
                 MessageBox.Show("El item seleccionado NO puede ser ELIMINADO ni EDITADO por pertenecer a una consulta ya cerrada", "AVISO");
                 return;
             }
             if (_opc == 4)
             {
                 cboAntecedente.Text = dgvAntecedentes.Rows[indice].Cells[6].Value.ToString();
                 cboAntecedente.Enabled = false;
                 cmbFamiliar.Text = dgvAntecedentes.Rows[indice].Cells[4].Value.ToString();
                 cmbFamiliar.Enabled = false;
                 txtAntecedenteObserv.Text = dgvAntecedentes.Rows[indice].Cells[7].Value.ToString();
             }
             else
             {
                 cboAntecedente.Text = dgvAntecedentes.Rows[indice].Cells[4].Value.ToString();
                 cboAntecedente.Enabled = false;
                 txtAntecedenteObserv.Text = dgvAntecedentes.Rows[indice].Cells[5].Value.ToString();
             }

             if (this.dgvAntecedentes.Columns[e.ColumnIndex].Name == "btnDelAntecedentes")
             {
                 DelAntecedentes(_opc);
             }
             else
             {
                 if (this.dgvAntecedentes.Columns[e.ColumnIndex].Name == "btnUpdAntecedentes")
                 {
                     btnAddAntecedentes.Text = "Grabar";
                 }
                 else
                 { return; }
             }
         }

         private void dgvAntecedentes_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
         {
             if (e.ColumnIndex >= 0 && this.dgvAntecedentes.Columns[e.ColumnIndex].Name == "btnDelAntecedentes" && e.RowIndex >= 0)
             {
                 e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                 DataGridViewButtonCell celBoton = this.dgvAntecedentes.Rows[e.RowIndex].Cells["btnDelAntecedentes"] as DataGridViewButtonCell;
                 Icon icoEliminar = new Icon(@"C:\Windows\Dermabelle\Ico\Delete.ico");
                 e.Graphics.DrawIcon(icoEliminar, e.CellBounds.Left + 3, e.CellBounds.Top + 3);

                 this.dgvAntecedentes.Rows[e.RowIndex].Height = icoEliminar.Height + 10;
                 this.dgvAntecedentes.Columns[e.ColumnIndex].Width = icoEliminar.Width + 10;

                 e.Handled = true;
             }

             if (e.ColumnIndex >= 0 && this.dgvAntecedentes.Columns[e.ColumnIndex].Name == "btnUpdAntecedentes" && e.RowIndex >= 0)
             {
                 e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                 DataGridViewButtonCell celBoton = this.dgvAntecedentes.Rows[e.RowIndex].Cells["btnUpdAntecedentes"] as DataGridViewButtonCell;
                 Icon icoModificar = new Icon(@"C:\Windows\Dermabelle\Ico\Style_file.ico");
                 e.Graphics.DrawIcon(icoModificar, e.CellBounds.Left + 3, e.CellBounds.Top + 3);

                 this.dgvAntecedentes.Rows[e.RowIndex].Height = icoModificar.Height + 10;
                 this.dgvAntecedentes.Columns[e.ColumnIndex].Width = icoModificar.Width + 10;

                 e.Handled = true;
             }
         }

         private void btnAntecPatol_Click(object sender, EventArgs e)
         {
             CargardgvdAntecedentes(1);
         }

         private void btnAntecQuirurg_Click(object sender, EventArgs e)
         {
             CargardgvdAntecedentes(2);
         }

         private void btnAntecAlergias_Click(object sender, EventArgs e)
         {
             CargardgvdAntecedentes(3);
         }

         private void btnAntecFamiliares_Click(object sender, EventArgs e)
         {
             CargardgvdAntecedentes(4);
         }

         private void btnAddAntecedentes_Click(object sender, EventArgs e)
         {
             if (string.Equals(btnAddAntecedentes.Text, "Grabar"))
             { UpdAntecedentes(_opc); }
             else
             { AddAntecedentes(_opc); }
         }

         void CargardgvDiagnosticos()
         {
             dgvDiagnosticos.DataSource = obj.dgvDiagnosticos(FichaNum);
         }

         private void tbpDiagnostico_Enter(object sender, EventArgs e)
         {
             txtCIECod.Text = "";
             txtDxNombre.Text = "";
             rbtPresuntivo.Checked = true;
             CargardgvDiagnosticos();
         }

         private void tbpMotivoConsulta_Enter(object sender, EventArgs e)
         {
             dgvAntecDiagnosticos.DataSource = obj.AntecDiagAnteriores(lblNumHistoria.Text);
             for (int indice = 0; indice < dgvAntecDiagnosticos.Rows.Count; indice++)
             {
                 dgvAntecDiagnosticos.Columns[0].Width = 80;
                 dgvAntecDiagnosticos.Columns[1].Width = 60;
                 dgvAntecDiagnosticos.Columns[2].Width = 500;
                 dgvAntecDiagnosticos.Columns[3].Width = 80;
                 dgvAntecDiagnosticos.Columns[4].Width = 50;
                 dgvAntecDiagnosticos.Columns[5].Width = 600;
             }
         }

         private void tpgExamFisico_Enter(object sender, EventArgs e)
         {
             dgvPartesCuerpo.DataSource = obj.DgvPartesCuerpo(FichaNum, SerId);
             for (int indice = 0; indice < dgvPartesCuerpo.Rows.Count; indice++)
             {
                 dgvPartesCuerpo.Columns[0].Visible = false;
                 dgvPartesCuerpo.Columns[1].ReadOnly = true;
                 dgvPartesCuerpo.Columns[1].Width = 400;
                 dgvPartesCuerpo.Columns[1].ReadOnly = true;
                 dgvPartesCuerpo.Columns[2].Width = 488;
             }
         }

         private void dgvPartesCuerpo_CellClick_1(object sender, DataGridViewCellEventArgs e)
         {
             if (e.ColumnIndex == -1)
             { return; }

             CargarSegmentos();
         }

         private void dgvSegmentos_Leave(object sender, EventArgs e)
         {
             oFM_ExamPreferencialDetalle.FichaMedicaId = Convert.ToSByte(FichaNum);
             oFM_ExamPreferencialDetalle.ParteCuerpoId = _PartesCuerpoId; //int.Parse(dgvSegmentos.Rows[0].Cells[0].Value.ToString());
             obj.FM_SegmentosDel(oFM_ExamPreferencialDetalle);
             // Luego recorrer el grid y grabar los registros seleccionados
             for (int ind = 0; ind < dgvSegmentos.Rows.Count; ind++)
             {
                 if (!string.IsNullOrWhiteSpace(dgvSegmentos.Rows[ind].Cells[3].Value.ToString()))
                 {
                     oFM_ExamPreferencialDetalle.SegmentoId = int.Parse(dgvSegmentos.Rows[ind].Cells[1].Value.ToString());
                     oFM_ExamPreferencialDetalle.SegmentoObserv = dgvSegmentos.Rows[ind].Cells[3].Value.ToString().ToUpper();
                     oFM_ExamPreferencialDetalle.SegmentoSelec = true;
                     obj.FM_SegmentosAdd(oFM_ExamPreferencialDetalle);
                 }
             }
         }

         private void tabPage2_Leave_1(object sender, EventArgs e)
         {
             //Examen Fisico (Examen Dirigido)
             GuardarExamenFisico();
         }

         private void tabPage1_Leave_1(object sender, EventArgs e)
         {
             //Examen Fisico (Funciones Vitales)
             GuardarExamenFisico();
         }

         private void btnGuardaSegmentos_Click(object sender, EventArgs e)
         {
             oFM_ExamPreferencialDetalle.FichaMedicaId = Convert.ToSByte(FichaNum);
             oFM_ExamPreferencialDetalle.ParteCuerpoId = _PartesCuerpoId; //int.Parse(dgvSegmentos.Rows[0].Cells[0].Value.ToString());
             obj.FM_SegmentosDel(oFM_ExamPreferencialDetalle);
             // Luego recorrer el grid y grabar los registros seleccionados
             for (int ind = 0; ind < dgvSegmentos.Rows.Count; ind++)
             {
                 if (!string.IsNullOrWhiteSpace(dgvSegmentos.Rows[ind].Cells[3].Value.ToString()))
                 {
                     oFM_ExamPreferencialDetalle.SegmentoId = int.Parse(dgvSegmentos.Rows[ind].Cells[1].Value.ToString());
                     oFM_ExamPreferencialDetalle.SegmentoObserv = dgvSegmentos.Rows[ind].Cells[3].Value.ToString().ToUpper();
                     oFM_ExamPreferencialDetalle.SegmentoSelec = true;
                     obj.FM_SegmentosAdd(oFM_ExamPreferencialDetalle);
                 }
             }
             CargarSegmentos();
         }

         private void btnAgregar_Click(object sender, EventArgs e)
         {
             //Guardar Ayuda Diagnostica
             oFM_AyudaDiagnostica.FichaMedicaId = Convert.ToSByte(FichaNum);
             oFM_AyudaDiagnostica.AyudaDiagId = int.Parse(txtAyudaDiagId.Text);
             oFM_AyudaDiagnostica.AyudaDiagFecha = dtpFechaExamen.Value.Date;
             oFM_AyudaDiagnostica.AyudaDiagTipo = int.Parse(cmbTipoExamen.SelectedValue.ToString());
             oFM_AyudaDiagnostica.AyudaDiagInforme = txtInfExamen.Text;

             if (string.Equals(btnGuardarAyudaDx.Text, "Agregar"))
             { obj.FM_AyudaDiagnosticaAdd(oFM_AyudaDiagnostica); }
             else
             { obj.FM_AyudaDiagnosticaUpd(oFM_AyudaDiagnostica); }

             btnGuardarAyudaDx.Text = "Agregar";
             CargardgvAyudaDiagnostica();
             txtAyudaDiagId.Text = "";
             dtpFechaExamen.Value = DateTime.Today;
             cmbTipoExamen.Text = "";
             txtInfExamen.Text = "";

             dtpFechaExamen.Enabled = false;
             cmbTipoExamen.Enabled = false;
             txtInfExamen.Enabled = false;
             btnGuardarAyudaDx.Enabled = false;
             nuevo = false;
         }

         private void tbpAyudaDx_Enter(object sender, EventArgs e)
         {
             AyudaDiagnostica();
             CargardgvAyudaDiagnostica();
             dtpFechaExamen.Value = DateTime.Today;
             txtInfExamen.Text = "";
         }

         private void dgvAyudaDiagnostica_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
         {
             if (e.ColumnIndex >= 0 && this.dgvAyudaDiagnostica.Columns[e.ColumnIndex].Name == "btnDelAyudaDx" && e.RowIndex >= 0)
             {
                 e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                 DataGridViewButtonCell celBoton = this.dgvAyudaDiagnostica.Rows[e.RowIndex].Cells["btnDelAyudaDx"] as DataGridViewButtonCell;
                 Icon icoEliminar = new Icon(@"C:\Windows\Dermabelle\Ico\Delete.ico");
                 e.Graphics.DrawIcon(icoEliminar, e.CellBounds.Left + 3, e.CellBounds.Top + 3);

                 this.dgvAyudaDiagnostica.Rows[e.RowIndex].Height = icoEliminar.Height + 10;
                 this.dgvAyudaDiagnostica.Columns[e.ColumnIndex].Width = icoEliminar.Width + 10;

                 e.Handled = true;
             }

             if (e.ColumnIndex >= 0 && this.dgvAyudaDiagnostica.Columns[e.ColumnIndex].Name == "btnUpdAyudaDx" && e.RowIndex >= 0)
             {
                 e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                 DataGridViewButtonCell celBoton = this.dgvAyudaDiagnostica.Rows[e.RowIndex].Cells["btnUpdAyudaDx"] as DataGridViewButtonCell;
                 Icon icoModificar = new Icon(@"C:\Windows\Dermabelle\Ico\Style_file.ico");
                 e.Graphics.DrawIcon(icoModificar, e.CellBounds.Left + 3, e.CellBounds.Top + 3);

                 this.dgvAyudaDiagnostica.Rows[e.RowIndex].Height = icoModificar.Height + 10;
                 this.dgvAyudaDiagnostica.Columns[e.ColumnIndex].Width = icoModificar.Width + 10;

                 e.Handled = true;
             }
         }

         private void dgvAyudaDiagnostica_CellClick(object sender, DataGridViewCellEventArgs e)
         {
             int indice = dgvAyudaDiagnostica.CurrentCell.RowIndex;
             if (!string.Equals(FichaNum.ToString(), dgvAyudaDiagnostica.Rows[indice].Cells[3].Value.ToString()))
             {
                 MessageBox.Show("El item seleccionado NO puede ser ELIMINADO ni EDITADO por pertenecer a una consulta ya cerrada", "AVISO");
                 return;
             }

             if (this.dgvAyudaDiagnostica.Columns[e.ColumnIndex].Name == "btnDelAyudaDx")
             {
                 oFM_AyudaDiagnostica.FichaMedicaId = Convert.ToSByte(FichaNum);
                 oFM_AyudaDiagnostica.AyudaDiagId = int.Parse(dgvAyudaDiagnostica.Rows[indice].Cells[2].Value.ToString());
                 obj.FM_AyudaDiagDel(oFM_AyudaDiagnostica);
                 CargardgvAyudaDiagnostica();
             }

             if (this.dgvAyudaDiagnostica.Columns[e.ColumnIndex].Name == "btnUpdAyudaDx")
             {
                 txtAyudaDiagId.Text = dgvAyudaDiagnostica.Rows[indice].Cells[2].Value.ToString();
                 dtpFechaExamen.Value = Convert.ToDateTime(dgvAyudaDiagnostica.Rows[indice].Cells[4].Value.ToString()); 
                 cmbTipoExamen.Text = dgvAyudaDiagnostica.Rows[indice].Cells[5].Value.ToString();
                 txtInfExamen.Text = dgvAyudaDiagnostica.Rows[indice].Cells[6].Value.ToString();
                 btnGuardarAyudaDx.Text = "Actualizar";
             }
             else
            { return; }
         }

         private void btnAyudaDiagNuevo_Click(object sender, EventArgs e)
         {
             txtAyudaDiagId.Text = "";
             dtpFechaExamen.Value = DateTime.Today;
             cmbTipoExamen.Text = "";
             txtInfExamen.Text = "";

             dtpFechaExamen.Enabled = true;
             cmbTipoExamen.Enabled = true;
             txtInfExamen.Enabled = true;
             btnGuardarAyudaDx.Enabled = true;
         }

         private void btnLab_Click(object sender, EventArgs e)
         {
             frmLaboratorio frm = new frmLaboratorio();
             frm.FichaNum = FichaNum;
             frm.ShowDialog();
         }

         private void btnImg_Click(object sender, EventArgs e)
         {
             frmRayosX frm = new frmRayosX();
             frm.FichaNum = FichaNum;
             frm.ShowDialog();
         }

         private void btnPat_Click(object sender, EventArgs e)
         {
             frmPatologia frm = new frmPatologia();
             frm.FichaNum = FichaNum;
             frm.ShowDialog();
         }

         private void btnEco_Click(object sender, EventArgs e)
         {
             frmEcografia frm = new frmEcografia();
             frm.FichaNum = FichaNum;
             frm.ShowDialog();
         }

         private void tbpPlanTrabajo_Enter(object sender, EventArgs e)
         {
             Tratamientos();
             CargardgvTratamientos();

         }

         private void dgvTratamientos_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
         {
             if (e.ColumnIndex >= 0 && this.dgvTratamientos.Columns[e.ColumnIndex].Name == "btnTratamientosDel" && e.RowIndex >= 0)
             {
                 e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                 DataGridViewButtonCell celBoton = this.dgvTratamientos.Rows[e.RowIndex].Cells["btnTratamientosDel"] as DataGridViewButtonCell;
                 Icon icoEliminar = new Icon(@"C:\Windows\Dermabelle\Ico\Delete.ico");
                 e.Graphics.DrawIcon(icoEliminar, e.CellBounds.Left + 3, e.CellBounds.Top + 3);

                 this.dgvTratamientos.Rows[e.RowIndex].Height = icoEliminar.Height + 10;
                 this.dgvTratamientos.Columns[e.ColumnIndex].Width = icoEliminar.Width + 10;

                 e.Handled = true;
             }

             if (e.ColumnIndex >= 0 && this.dgvTratamientos.Columns[e.ColumnIndex].Name == "btnTratamientosUpd" && e.RowIndex >= 0)
             {
                 e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                 DataGridViewButtonCell celBoton = this.dgvTratamientos.Rows[e.RowIndex].Cells["btnTratamientosUpd"] as DataGridViewButtonCell;
                 Icon icoModificar = new Icon(@"C:\Windows\Dermabelle\Ico\Style_file.ico");
                 e.Graphics.DrawIcon(icoModificar, e.CellBounds.Left + 3, e.CellBounds.Top + 3);

                 this.dgvTratamientos.Rows[e.RowIndex].Height = icoModificar.Height + 10;
                 this.dgvTratamientos.Columns[e.ColumnIndex].Width = icoModificar.Width + 10;

                 e.Handled = true;
             }
         }

         private void dgvTratamientos_CellClick(object sender, DataGridViewCellEventArgs e)
         {
             int indice = dgvTratamientos.CurrentCell.RowIndex;
             oFM_Tratamientos.FichaMedicaId = int.Parse(dgvTratamientos.Rows[indice].Cells[2].Value.ToString());
             oFM_Tratamientos.TratamId = int.Parse(dgvTratamientos.Rows[indice].Cells[3].Value.ToString());
             if (this.dgvTratamientos.Columns[e.ColumnIndex].Name == "btnTratamientosDel")
             {
                 obj.FM_TratamientosDel(oFM_Tratamientos);
                 CargardgvTratamientos();
             }
             else
             {
                 if (this.dgvTratamientos.Columns[e.ColumnIndex].Name == "btnTratamientosUpd")
                 {
                     cmbTratamientos.Text = dgvTratamientos.Rows[indice].Cells[4].Value.ToString();
                     txtTratamNumSesiones.Text = dgvTratamientos.Rows[indice].Cells[5].Value.ToString();
                     txtTratamObserv.Text = dgvTratamientos.Rows[indice].Cells[6].Value.ToString();
                     btnGrabarTx.Text = "Grabar";
                 }
                 else
                 { return; }
             }
         }

         private void btnGrabarTx_Click(object sender, EventArgs e)
         {
             oFM_Tratamientos.FichaMedicaId = Convert.ToSByte(FichaNum);
             oFM_Tratamientos.TratamId = int.Parse(cmbTratamientos.SelectedValue.ToString());
             oFM_Tratamientos.TratamNumSesiones = int.Parse(txtTratamNumSesiones.Text);
             oFM_Tratamientos.TratamObserv = txtTratamObserv.Text;
             
             if (string.Equals(btnGrabarTx.Text, "Agregar"))
             { obj.FM_TratamientosAdd(oFM_Tratamientos); }
             else
             { obj.FM_TratamientosUpd(oFM_Tratamientos); }
             btnGrabarTx.Text = "Agregar";
             CargardgvTratamientos(); 
         }

         private void cboPrinActivoFA_SelectedValueChanged(object sender, EventArgs e)
         {
         }

         private void cboMedicamentos_SelectedValueChanged(object sender, EventArgs e)
         {
             CboMedicinas();
         }

         private void lstMedicinas_SelectedIndexChanged(object sender, EventArgs e)
         {
             txtPrdDescripcion.Text = lstMedicinas.SelectedItem.ToString();
             lstMedicinas.Visible = false;
         }

         private void txtPrdDescripcion_TextChanged(object sender, EventArgs e)
         {
             if (!muestralistbox)
                 return;
             lstMedicinas.Location = new Point(105, 39);
             lstMedicinas.Size = new Size(429, 212);
             lstMedicinas.Visible = true;
             lstMedicinas.Items.Clear();
             txtPrdId.Text = "";
             List<usp_CboMedicamentosResult> lista = new List<usp_CboMedicamentosResult>();
             lista = obj.CboMedicamentos(txtPrdDescripcion.Text);
             if (lista.Count() > 0)
             {
                 foreach (usp_CboMedicamentosResult reg in lista)
                 {
                     lstMedicinas.Items.Add(reg.Prd_Descripcion);
                     txtPrdId.Text = reg.Prd_Id.ToString();
                 }
             }
         }

         bool muestralistbox;
         void CargarcmbCriterio(int opc)
         {
             switch (opc)
             { 
                 case 1:
                     cmbCriterio.DataSource = null;
                     cmbCriterio.DataSource = obj.CboPrincActivoFA();
                     cmbCriterio.ValueMember = "PrA_Id";
                     cmbCriterio.DisplayMember = "PrA_Nombre";
                     muestralistbox = true;
                     break;
                 case 2:
                     cmbCriterio.DataSource = null;
                     cmbCriterio.DataSource = obj.CboLaboratoriosFA();
                     cmbCriterio.ValueMember = "Lab_Id";
                     cmbCriterio.DisplayMember = "Lab_Nombre";
                     muestralistbox = true;
                     break;
                 case 3:
                     cmbCriterio.DataSource = null;
                     cmbCriterio.DataSource = obj.cmbPreparados();
                     cmbCriterio.ValueMember = "Prd_Id";
                     cmbCriterio.DisplayMember = "Prd_Descripcion";

                     cmbComponente.Enabled = false;

                     muestralistbox = false;
                     break;
             }
             if (muestralistbox)
             {
                 gpbDatos.Visible = false;
                 dgvPreparados.Visible = false;
                 lstMedicinasFiltro.Location = new Point(6, 83);
                 lstMedicinasFiltro.Size = new Size(508, 329);
                 lstMedicinasFiltro.Visible = true;
                 int valor = int.Parse(cmbCriterio.SelectedValue.ToString());
                 CargarlstMedFiltro(opc, valor);
             }
             else
             {
                 lstMedicinasFiltro.Visible = false;
                 gpbDatos.Visible = true;
                 dgvPreparados.Location = new Point(6, 200);
                 dgvPreparados.Size = new Size(508, 176);
                 dgvPreparados.Visible = true;
             }
         }

         void CargarlstMedFiltro(int opc, int valor)
         {
             switch (opc)
             {
                 case 1:
                     lstMedicinasFiltro.Items.Clear();
                     txtPrdId.Text = "";
                     List<usp_MedicinasGenResult> listaGen = new List<usp_MedicinasGenResult>();
                     listaGen = obj.lstMedicamentosGen(valor);
                     if (listaGen.Count() > 0)
                     {
                         foreach (usp_MedicinasGenResult reg in listaGen)
                         {
                             lstMedicinasFiltro.Items.Add(reg.Prd_Descripcion);
                             txtPrdId.Text = reg.Prd_Id.ToString();
                         }
                     }
                     break;
                 case 2:
                     lstMedicinasFiltro.Items.Clear();
                     txtPrdId.Text = "";
                     List<usp_MedicinasLabResult> listaLab = new List<usp_MedicinasLabResult>();
                     listaLab = obj.lstMedicamentosLab(valor);
                     if (listaLab.Count() > 0)
                     {
                         foreach (usp_MedicinasLabResult reg in listaLab)
                         {
                             lstMedicinasFiltro.Items.Add(reg.Prd_Descripcion);
                             txtPrdId.Text = reg.Prd_Id.ToString();
                         }
                     }
                     break;
             }
         }

         void CargarlstDiagnosticos(int opc, string buscar)
         {
             if (!string.IsNullOrEmpty(txtCIECod.Text) &
                 !string.IsNullOrEmpty(txtDxNombre.Text))
             {
                 lstDiagnosticos.Visible = false;
                 return;
             }
             switch (opc)
             {
                 case 1: //Por Codigo
                     lstDiagnosticos.Items.Clear();
                     txtDxNombre.Text = "";
                     List<DiagxCodResult> listaCod = new List<DiagxCodResult>();
                     listaCod = obj.DiagxCod(buscar);
                     if (listaCod.Count() > 0)
                     {
                         foreach (DiagxCodResult reg in listaCod)
                         {
                             lstDiagnosticos.Items.Add(reg.Diagnostico);
                         }
                     }
                     
                     break;
                 case 2: //Por Nombre
                     lstDiagnosticos.Items.Clear();
                     txtCIECod.Text = "";
                     List<DiagxNomResult> listaNom = new List<DiagxNomResult>();
                     listaNom = obj.DiagxNom(buscar);
                     if (listaNom.Count() > 0)
                     {
                         foreach (DiagxNomResult reg in listaNom)
                         {
                             lstDiagnosticos.Items.Add(reg.Diagnostico);
                         }
                     }
                     break;
             }
             lstDiagnosticos.Location = new Point(170, 47);
             lstDiagnosticos.Size = new Size(1099, 225);
             lstDiagnosticos.Visible = true;
         }

         void CargardgvFormula(int opc, int valor)
         {
             switch (opc)
             {
                 case 1:
                     dgvPreparados.DataSource = obj.dgvPreparados(valor);
                     dgvPreparados.Enabled = false;
                     FlagSi = false;
                     break;
                 case 2:
                     dgvPreparados.DataSource = obj.PreparadosLis(FichaNum, valor);
                     dgvPreparados.Enabled = true;
                     FlagSi = true;
                     break;
             }

             for (int indice = 0; indice < dgvPreparados.Rows.Count; indice++)
             {
                 dgvPreparados.Columns[0].Visible = FlagSi; //btnDelComponente
                 dgvPreparados.Columns[1].Visible = FlagSi; //btnUpdComponente
                 dgvPreparados.Columns[2].Visible = false;   //Prd_Id
                 dgvPreparados.Columns[3].Visible = false;   //PrA_Id
                 dgvPreparados.Columns[4].Visible = false;   //Tipo
                 dgvPreparados.Columns[5].Width = 310;       //Compuesto
                 dgvPreparados.Columns[6].Width = 50;        //Cant
                 dgvPreparados.Columns[7].Width = 50;        //Und
             }
         }

         private void rbtGenerico_CheckedChanged(object sender, EventArgs e)
         {
             if (rbtGenerico.Checked)
             {
                 lblCriterio.Text = rbtGenerico.Text;
                 CargarcmbCriterio(1);
                 btnAceptar.Visible = false;
             }
         }

         private void rbtLaboratorio_CheckedChanged(object sender, EventArgs e)
         {
             if (rbtLaboratorio.Checked)
             {
                 lblCriterio.Text = rbtLaboratorio.Text;
                 CargarcmbCriterio(2);
                 btnAceptar.Visible = false;
             }
         }

         private void cmbCriterio_SelectedValueChanged(object sender, EventArgs e)
         {
             try
             {
                 if (rbtGenerico.Checked)
                 { CargarlstMedFiltro(1, int.Parse(cmbCriterio.SelectedValue.ToString())); }
                 if (rbtLaboratorio.Checked)
                 { CargarlstMedFiltro(2, int.Parse(cmbCriterio.SelectedValue.ToString())); }
                 if (rbtPreparados.Checked)
                 { CargardgvFormula(1,int.Parse(cmbCriterio.SelectedValue.ToString())); }
             }
             catch { }
         }

         private void lstMedicinasFiltro_SelectedIndexChanged(object sender, EventArgs e)
         {
             txtPrdDescripcion.Text = lstMedicinasFiltro.SelectedItem.ToString();
             lstMedicinas.Visible = false;
             txtIndicaciones.Focus();
         }

         private void dgvPrescripcion_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
         {
             if (e.ColumnIndex >= 0 && this.dgvPrescripcion.Columns[e.ColumnIndex].Name == "btnDelMedicinas" && e.RowIndex >= 0)
             {
                 e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                 DataGridViewButtonCell celBoton = this.dgvPrescripcion.Rows[e.RowIndex].Cells["btnDelMedicinas"] as DataGridViewButtonCell;
                 Icon icoEliminar = new Icon(@"C:\Windows\Dermabelle\Ico\Delete.ico");                 
                 e.Graphics.DrawIcon(icoEliminar, e.CellBounds.Left + 3, e.CellBounds.Top + 3);

                 this.dgvPrescripcion.Rows[e.RowIndex].Height = icoEliminar.Height + 10;
                 this.dgvPrescripcion.Columns[e.ColumnIndex].Width = icoEliminar.Width + 10;

                 e.Handled = true;
             }

             if (e.ColumnIndex >= 0 && this.dgvPrescripcion.Columns[e.ColumnIndex].Name == "btnUpdMedicinas" && e.RowIndex >= 0)
             {
                 e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                 DataGridViewButtonCell celBoton = this.dgvPrescripcion.Rows[e.RowIndex].Cells["btnUpdMedicinas"] as DataGridViewButtonCell;
                 Icon icoModificar = new Icon(@"C:\Windows\Dermabelle\Ico\Style_file.ico");
                 e.Graphics.DrawIcon(icoModificar, e.CellBounds.Left + 3, e.CellBounds.Top + 3);

                 this.dgvPrescripcion.Rows[e.RowIndex].Height = icoModificar.Height + 10;
                 this.dgvPrescripcion.Columns[e.ColumnIndex].Width = icoModificar.Width + 10;

                 e.Handled = true;
             }
         }

         private void dgvPrescripcion_CellClick(object sender, DataGridViewCellEventArgs e)
         {
             int indice = dgvPrescripcion.CurrentCell.RowIndex;
             oFM_Prescripcion.FichaMedicaId = Convert.ToSByte(FichaNum);
             oFM_Prescripcion.RpItem = short.Parse(dgvPrescripcion.Rows[indice].Cells[2].Value.ToString());
             oFM_Prescripcion.Prd_Id = short.Parse(dgvPrescripcion.Rows[indice].Cells[3].Value.ToString());
             oFM_Prescripcion.RpNomComercial = dgvPrescripcion.Rows[indice].Cells[4].Value.ToString();
             oFM_Prescripcion.RpIndicaciones = dgvPrescripcion.Rows[indice].Cells[6].Value.ToString();

             if (this.dgvPrescripcion.Columns[e.ColumnIndex].Name == "btnDelMedicinas")
             {
                 if ((MessageBox.Show("¿Seguro que desea eliminar el producto: " +
                     oFM_Prescripcion.RpNomComercial.ToString() + "?", "CONFIRMAR",
                          MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                          MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes))
                 {
                     oFM_Preparados.FichaMedicaId = oFM_Prescripcion.FichaMedicaId;
                     oFM_Preparados.Prd_Id = oFM_Prescripcion.Prd_Id;
                     obj.FM_PreparadosLim(oFM_Preparados);
                     obj.FM_PrescripcionDel(oFM_Prescripcion);
                     CargardgvPrescripcion();
                 }
             }

             if (this.dgvPrescripcion.Columns[e.ColumnIndex].Name == "btnUpdMedicinas")
             {
                 cmbVigencia.Enabled = true;
                 muestralistbox = false;
                 txtPrdId.Text = dgvPrescripcion.Rows[indice].Cells[3].Value.ToString();
                 txtPrdDescripcion.Text = dgvPrescripcion.Rows[indice].Cells[4].Value.ToString();
                 txtIndicaciones.Text = dgvPrescripcion.Rows[indice].Cells[6].Value.ToString();
                 if(string.Equals(dgvPrescripcion.Rows[indice].Cells[7].Value.ToString(),"FM")) //Es Formula Magistral
                 {
                     //btnAceptar.Visible = false;
                     FlagSi = false;
                     rbtPreparados.Checked = true;
                     cmbCriterio.Text = txtPrdDescripcion.Text;
                     cmbCriterio.Enabled = false;
                     CargardgvFormula(2, int.Parse(txtPrdId.Text));
                     btnAgregarComponente.Enabled = true;
                 }
                 txtPrdDescripcion.Enabled = false;
                 btnAddPresc.Text = "Grabar";
             }
         }

         private void btnPresc_Click(object sender, EventArgs e)
         {
             frmPrescAnteriores frm = new frmPrescAnteriores();
             frm.Pac_HC = lblNumHistoria.Text;
             frm.ShowDialog();
         }

         private void btnRepiteRx_Click(object sender, EventArgs e)
         {
             List<usp_UltimaPrescRecupResult> lista = new List<usp_UltimaPrescRecupResult>();
             lista = obj.UltimaPrescripcion(lblNumHistoria.Text, SerId, MedCod);
             if (lista.Count() > 0)
             {
                 foreach (usp_UltimaPrescRecupResult reg in lista)
                 {
                     oFM_Prescripcion.FichaMedicaId = Convert.ToSByte(FichaNum);;
                     oFM_Prescripcion.RpItem = Convert.ToSByte(NumItem(FichaNum));
                     oFM_Prescripcion.Prd_Id = reg.Prd_Id;
                     oFM_Prescripcion.RpNomComercial = reg.RpNomComercial;
                     oFM_Prescripcion.RpIndicaciones = reg.RpIndicaciones;
                     obj.FM_PrescripcionAdd(oFM_Prescripcion);
                 }
             }
             CargardgvPrescripcion();
         }

         private void btnFormMagistral_Click(object sender, EventArgs e)
         {
             frmFormMagistral frm = new frmFormMagistral();
             frm.ShowDialog();
         }

         private void rbtPreparados_CheckedChanged(object sender, EventArgs e)
         {
             if (rbtPreparados.Checked)
             {
                 rbtPrincActivo.Enabled = false;
                 rbtElementoBase.Enabled = false;
                 btnAgregarComponente.Enabled = false;
                 btnAddCompuesto.Enabled = false;
                 lblCriterio.Text = rbtPreparados.Text;
                 CargarcmbCriterio(3);
                 btnAceptar.Visible = true;
             }
         }

         private void dgvPreparados_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
         {
             if (e.ColumnIndex >= 0 && this.dgvPreparados.Columns[e.ColumnIndex].Name == "btnDelCompuesto" && e.RowIndex >= 0)
             {
                 e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                 DataGridViewButtonCell celBoton = this.dgvPreparados.Rows[e.RowIndex].Cells["btnDelCompuesto"] as DataGridViewButtonCell;
                 Icon icoEliminar = new Icon(@"C:\Windows\Dermabelle\Ico\Delete.ico");
                 e.Graphics.DrawIcon(icoEliminar, e.CellBounds.Left + 3, e.CellBounds.Top + 3);

                 this.dgvPreparados.Rows[e.RowIndex].Height = icoEliminar.Height + 10;
                 this.dgvPreparados.Columns[e.ColumnIndex].Width = icoEliminar.Width + 10;

                 e.Handled = true;
             }

             if (e.ColumnIndex >= 0 && this.dgvPreparados.Columns[e.ColumnIndex].Name == "btnUpdCompuesto" && e.RowIndex >= 0)
             {
                 e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                 DataGridViewButtonCell celBoton = this.dgvPreparados.Rows[e.RowIndex].Cells["btnUpdCompuesto"] as DataGridViewButtonCell;
                 Icon icoModificar = new Icon(@"C:\Windows\Dermabelle\Ico\Style_file.ico");
                 e.Graphics.DrawIcon(icoModificar, e.CellBounds.Left + 3, e.CellBounds.Top + 3);

                 this.dgvPreparados.Rows[e.RowIndex].Height = icoModificar.Height + 10;
                 this.dgvPreparados.Columns[e.ColumnIndex].Width = icoModificar.Width + 10;

                 e.Handled = true;
             }
         }

         private void btnAceptar_Click(object sender, EventArgs e)
         {
             FlagSi = true;
             txtPrdDescripcion.Text = cmbCriterio.Text;
             txtPrdId.Text = cmbCriterio.SelectedValue.ToString();
             lstMedicinas.Visible = false;

             // Primero borrar los registros previos
             oFM_Preparados.FichaMedicaId = Convert.ToSByte(FichaNum);
             oFM_Preparados.Prd_Id = int.Parse(cmbCriterio.SelectedValue.ToString());
             obj.FM_PreparadosLim(oFM_Preparados);

             // Luego recorrer el grid y grabar los registros seleccionados
             for (int indice = 0; indice < dgvPreparados.Rows.Count; indice++)
             {
                oFM_Preparados.FichaMedicaId = Convert.ToSByte(FichaNum);
                oFM_Preparados.Prd_Id = short.Parse(dgvPreparados.Rows[indice].Cells[2].Value.ToString());
                oFM_Preparados.PrA_Tipo = dgvPreparados.Rows[indice].Cells[4].Value.ToString();
                oFM_Preparados.PrA_Id = int.Parse(dgvPreparados.Rows[indice].Cells[3].Value.ToString());
                oFM_Preparados.Cantidad = dgvPreparados.Rows[indice].Cells[6].Value.ToString();
                oFM_Preparados.Unidad = dgvPreparados.Rows[indice].Cells[7].Value.ToString();
                obj.FM_PreparadosAdd(oFM_Preparados);
             }

             btnAddPresc.Enabled = true;
             btnRepiteRx.Enabled = true;
             btnPresc.Enabled = true;
             txtIndicaciones.Focus();
         }

         private void btnAddCompuesto_Click(object sender, EventArgs e)
         {
             if (rbtElementoBase.Checked &&
                 string.IsNullOrEmpty(txtCantElemBase.Text))
             {
                 MessageBox.Show("Debe especificar la CANTIDAD");
                 return;
             }

             oFM_Preparados.FichaMedicaId = Convert.ToSByte(FichaNum);
             oFM_Preparados.Prd_Id = int.Parse(cmbCriterio.SelectedValue.ToString());
             oFM_Preparados.PrA_Id = int.Parse(cmbComponente.SelectedValue.ToString());
             oFM_Preparados.Cantidad = txtCantElemBase.Text;
             if (rbtPrincActivo.Checked)
                 oFM_Preparados.PrA_Tipo = "FM";
             if(rbtElementoBase.Checked)
                 oFM_Preparados.PrA_Tipo = "EB";
             oFM_Preparados.Unidad = lblUndElemBase.Text;

             if (string.Equals(btnAddCompuesto.Text, "Agregar a la Fórmula"))
             {
                 obj.FM_PreparadosAdd(oFM_Preparados);
                 MessageBox.Show("Registro AGREGADO satisfactoriamente");
                 if (oFM_Preparados.PrA_Tipo == "EB")
                     MessageBox.Show("Usted ha agregado un NUEVO ELEMENTO BASE. Por favor asegurece de BORRAR EL ANTERIOR", "AVISO");
             }
             else
             { 
                 obj.FM_PreparadosUpd(oFM_Preparados);
                 MessageBox.Show("Registro MODIFICADO satisfactoriamente");
             }

             //rbtPrincActivo.Enabled = true;
             //rbtElementoBase.Enabled = true;
             //lblCantidad.Visible = false;
             //txtCantElemBase.Text = "";
             //txtCantElemBase.Visible = false;
             //lblUndElemBase.Text = "";
             //lblUndElemBase.Visible = false;

             cmbComponente.Text = "-- Seleccione Componente --";

             btnAddCompuesto.Text = "Agregar a la Fórmula";
             FlagSi = true;
             CargardgvFormula(2, int.Parse(cmbCriterio.SelectedValue.ToString()));
             btnAddPresc.Enabled = true;
             btnRepiteRx.Enabled = true;
             btnPresc.Enabled = true;
             //txtIndicaciones.Focus();
         }

         private void dgvPreparados_CellClick(object sender, DataGridViewCellEventArgs e)
         {
             btnAddPresc.Enabled = false;
             btnRepiteRx.Enabled = false;
             btnPresc.Enabled = false;
             int indice = dgvPreparados.CurrentCell.RowIndex;
             oFM_Preparados.FichaMedicaId = Convert.ToSByte(FichaNum);
             oFM_Preparados.Prd_Id = int.Parse(txtPrdId.Text);
             oFM_Preparados.PrA_Tipo = dgvPreparados.Rows[indice].Cells[4].Value.ToString();
             oFM_Preparados.PrA_Id = int.Parse(dgvPreparados.Rows[indice].Cells[3].Value.ToString());
             oFM_Preparados.Cantidad = dgvPreparados.Rows[indice].Cells[6].Value.ToString();
             oFM_Preparados.Unidad = dgvPreparados.Rows[indice].Cells[7].Value.ToString();
             
             if (this.dgvPreparados.Columns[e.ColumnIndex].Name == "btnDelCompuesto")
             {
                 if (oFM_Preparados.PrA_Tipo.ToString() == "FM")
                     mensaje = "¿Seguro que desea eliminar el componente: " +
                     dgvPreparados.Rows[indice].Cells[5].Value.ToString() + "?";

                 if (oFM_Preparados.PrA_Tipo.ToString() == "FM")
                     mensaje = "¿Seguro que desea eliminar el elemento base: " +
                     dgvPreparados.Rows[indice].Cells[5].Value.ToString() + "?";

                 if ((MessageBox.Show(mensaje, "CONFIRMAR",
                          MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                          MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes))
                 {
                     obj.FM_PreparadosDel(oFM_Preparados);
                     CargardgvFormula(2, oFM_Preparados.Prd_Id);
                 }
             }

             if (this.dgvPreparados.Columns[e.ColumnIndex].Name == "btnUpdCompuesto")
             {
                 if (string.Equals(oFM_Preparados.PrA_Tipo,"FM"))
                 {
                     MessageBox.Show("Este componente no puede ser EDITADO. Si desea cambiarlo por favor ELIMÍNELO y luego ingrese un NUEVO COMPONENTE");
                 }
                 else
                 {
                     rbtPrincActivo.Checked = false;
                     rbtPrincActivo.Enabled = false;
                     rbtElementoBase.Checked = true;
                     rbtElementoBase.Enabled = false;
                     cmbComponente.Text = dgvPreparados.Rows[indice].Cells[5].Value.ToString();
                     txtCantElemBase.Text = oFM_Preparados.Cantidad;
                     txtCantElemBase.Visible = true;
                     txtCantElemBase.Enabled = true;
                     lblUndElemBase.Text = oFM_Preparados.Unidad;
                     lblUndElemBase.Visible = true;
                 }

                 btnAddCompuesto.Text = "Grabar Cambios";
             }
         }

         private void dgvDiagnosticos_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
         {
             if (e.ColumnIndex >= 0 && this.dgvDiagnosticos.Columns[e.ColumnIndex].Name == "btnDelDiagnostico" && e.RowIndex >= 0)
             {
                 e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                 DataGridViewButtonCell celBoton = this.dgvDiagnosticos.Rows[e.RowIndex].Cells["btnDelDiagnostico"] as DataGridViewButtonCell;
                 Icon icoEliminar = new Icon(@"C:\Windows\Dermabelle\Ico\Delete.ico");
                 e.Graphics.DrawIcon(icoEliminar, e.CellBounds.Left + 3, e.CellBounds.Top + 3);

                 this.dgvDiagnosticos.Rows[e.RowIndex].Height = icoEliminar.Height + 10;
                 this.dgvDiagnosticos.Columns[e.ColumnIndex].Width = icoEliminar.Width + 10;

                 e.Handled = true;
             }

             if (e.ColumnIndex >= 0 && this.dgvDiagnosticos.Columns[e.ColumnIndex].Name == "btnUpdDiagnostico" && e.RowIndex >= 0)
             {
                 e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                 DataGridViewButtonCell celBoton = this.dgvDiagnosticos.Rows[e.RowIndex].Cells["btnUpdDiagnostico"] as DataGridViewButtonCell;
                 Icon icoModificar = new Icon(@"C:\Windows\Dermabelle\Ico\Style_file.ico");
                 e.Graphics.DrawIcon(icoModificar, e.CellBounds.Left + 3, e.CellBounds.Top + 3);

                 this.dgvDiagnosticos.Rows[e.RowIndex].Height = icoModificar.Height + 10;
                 this.dgvDiagnosticos.Columns[e.ColumnIndex].Width = icoModificar.Width + 10;

                 e.Handled = true;
             }
         }

         private void btnGrabar_Click(object sender, EventArgs e)
         {
             if (string.IsNullOrEmpty(txtCIECod.Text) |
                 string.IsNullOrEmpty(txtDxNombre.Text))
             {
                 MessageBox.Show("No ha seleccionado Diagnóstico de la lista");
                 return;
             }
           
             oFM_Diagnosticos.FichaMedicaId = Convert.ToSByte(FichaNum);
             oFM_Diagnosticos.DiagCod = txtCIECod.Text;
             oFM_Diagnosticos.DiagObserv = txtDiagObserv.Text;
             if (rbtPresuntivo.Checked)
             { oFM_Diagnosticos.DiagClasificacion = 'P'; }
             if (rbtDefinitivo.Checked)
             { oFM_Diagnosticos.DiagClasificacion = 'D'; }
             if (rbtCronico.Checked)
             { oFM_Diagnosticos.DiagClasificacion = 'C'; }
             oFM_Diagnosticos.DiagPrincipal = chkDxPrincipal.Checked;
             if (string.Equals(btnGrabar.Text, "Agregar"))
             { obj.FM_DiagnosticosAdd(oFM_Diagnosticos); }
             else
             { obj.FM_DiagnosticosUpd(oFM_Diagnosticos); }
             CargardgvDiagnosticos();

             //btnCerrar.Enabled = true;
             btnCerrar.Visible = true;
         }

         private void dgvDiagnosticos_CellClick(object sender, DataGridViewCellEventArgs e)
         {
             int indice = dgvDiagnosticos.CurrentCell.RowIndex;
             oFM_Diagnosticos.FichaMedicaId = Convert.ToSByte(FichaNum);
             oFM_Diagnosticos.DiagCod = dgvDiagnosticos.Rows[indice].Cells[2].Value.ToString();
             oFM_Diagnosticos.DiagObserv = dgvDiagnosticos.Rows[indice].Cells[6].Value.ToString();
             oFM_Diagnosticos.DiagClasificacion = Convert.ToChar(dgvDiagnosticos.Rows[indice].Cells[4].Value);
             oFM_Diagnosticos.DiagPrincipal = Convert.ToBoolean(dgvDiagnosticos.Rows[indice].Cells[5].Value.ToString());

             if (this.dgvDiagnosticos.Columns[e.ColumnIndex].Name == "btnDelDiagnostico")
             {
                 obj.FM_DiagnosticosDel(oFM_Diagnosticos);
                 CargardgvDiagnosticos();
             }
             else
             {
                 if (this.dgvDiagnosticos.Columns[e.ColumnIndex].Name == "btnUpdDiagnostico")
                 {
                     txtCIECod.Text = oFM_Diagnosticos.DiagCod;
                     txtDxNombre.Text = dgvDiagnosticos.Rows[indice].Cells[3].Value.ToString();
                     txtDiagObserv.Text = oFM_Diagnosticos.DiagObserv;
                     switch  (oFM_Diagnosticos.DiagClasificacion)
                     {
                         case 'P':
                             rbtPresuntivo.Checked = true;
                             break;
                         case 'D':
                             rbtDefinitivo.Checked = true;
                             break;
                         case 'C':
                             rbtCronico.Checked = true;
                             break;
                     }
                     chkDxPrincipal.Checked = oFM_Diagnosticos.DiagPrincipal;
                     btnGrabar.Text = "Grabar";
                 }
                 else
                 { return; }
             }
         }

         private void txtCIECod_TextChanged(object sender, EventArgs e)
         {
             CargarlstDiagnosticos(1,txtCIECod.Text);
         }

         private void txtDxNombre_TextChanged(object sender, EventArgs e)
         {
             CargarlstDiagnosticos(2,txtDxNombre.Text);
         }

         private void lstDiagnosticos_SelectedIndexChanged(object sender, EventArgs e)
         {
             lstDiagnosticos.Visible = false;
             txtCIECod.Text = lstDiagnosticos.SelectedItem.ToString().Substring(0, 10);

             List<RecNomDiagResult> nombre = new List<RecNomDiagResult>();
             nombre = obj.RecupNomDiag(txtCIECod.Text);
             if (nombre.Count() > 0)
             {
                 foreach (RecNomDiagResult reg in nombre)
                 {
                     txtDxNombre.Text = reg.DiagDescripcion;
                 }
             }
         }

         private void tbpConsultaAnterior_Enter(object sender, EventArgs e)
         {
             List<usp_CA_FichaMedicaIdResult> CA_FichaId = new List<usp_CA_FichaMedicaIdResult>();
             CA_FichaId = obj.CA_FichaMEedicaId(frmPacCitados.NumHistoria);
             if (CA_FichaId.Count() > 0)
             {
                 foreach (usp_CA_FichaMedicaIdResult reg in CA_FichaId)
                 {
                     NumFichaCA = reg.FichaMedicaId;
                     txtTipoAtencionCA.Text = reg.TipoAtencion;
                     txtFechaCA.Text = reg.Fecha.ToString();
                     txtHoraCA.Text = reg.Hora.ToString();
                     txtMotivoEvolCA.Text = reg.FM_MotivoConsulta;
                     chkAlcoholCA.Checked = Convert.ToBoolean(reg.Alcohol);
                     chkTabacoCA.Checked = Convert.ToBoolean(reg.Tabaco);
                     chkDrogasCA.Checked = Convert.ToBoolean(reg.Drogas);
                     chkOtrosHabCA.Checked = Convert.ToBoolean(reg.Otros);
                     txtEspecHabCA.Text = reg.FM_ObservHabNoc;
                 }
             }

             //Diagnósticos
             dgvDiagCA.DataSource = obj.CA_Diagnosticos(NumFichaCA);

             for (int indice = 0; indice < dgvDiagCA.Rows.Count; indice++)
             {
                 dgvDiagCA.Columns[0].Width = 80;       //CIE10
                 dgvDiagCA.Columns[1].Width = 400;      //Diagnóstico
                 dgvDiagCA.Columns[2].Width = 100;      //Clasificación
                 dgvDiagCA.Columns[3].Width = 50;       //Principal
                 dgvDiagCA.Columns[4].Width = 220;      //Observaciones
             }

             //Prescripciones
             dgvPrescCA.DataSource = obj.CA_Prescrip(NumFichaCA);

             for (int indice = 0; indice < dgvPrescCA.Rows.Count; indice++)
             {
                 dgvPrescCA.Columns[0].Width = 50;       //Item
                 dgvPrescCA.Columns[1].Width = 500;      //Medicamento
                 dgvPrescCA.Columns[2].Width = 300;      //Indicaciones
             }

             //Antecedentes Patologicos
             dgvPatCA.DataSource = obj.CA_AntePatol(NumFichaCA);

             for (int indice = 0; indice < dgvPatCA.Rows.Count; indice++)
             {
                 dgvPatCA.Columns[0].Width = 150;      //Antecedente
                 dgvPatCA.Columns[1].Width = 100;      //Observación
             }

             //Antecedentes Quirurgicos
             dgvQuirCA.DataSource = obj.CA_AnteQuirurg(NumFichaCA);

             for (int indice = 0; indice < dgvQuirCA.Rows.Count; indice++)
             {
                 dgvQuirCA.Columns[0].Width = 150;      //Antecedente
                 dgvQuirCA.Columns[1].Width = 100;      //Observación
             }

             //Antecedentes Familiares
             dgvFamCA.DataSource = obj.CA_AnteFamiliar(NumFichaCA);

             for (int indice = 0; indice < dgvFamCA.Rows.Count; indice++)
             {
                 dgvFamCA.Columns[0].Width = 100;      //Familiar
                 dgvFamCA.Columns[1].Width = 150;      //Antecedente
                 dgvFamCA.Columns[2].Width = 100;      //Observación
             }
         }

         private void btnLabCA_Click(object sender, EventArgs e)
         {
             CargarExamenesCA(1);
         }

         private void btnImgCA_Click(object sender, EventArgs e)
         {
             CargarExamenesCA(2);
         }

         private void btnPatCA_Click(object sender, EventArgs e)
         {
             CargarExamenesCA(3);
         }

         private void btnEcoCA_Click(object sender, EventArgs e)
         {
             CargarExamenesCA(4);
         }

         void CargarExamenesCA(int ExamenTipo)
         {
             dgvExamCA.DataSource = null;
             dgvExamCA.DataSource = obj.CA_Plantrab(NumFichaCA, ExamenTipo);

             for (int indice = 0; indice < dgvExamCA.Rows.Count; indice++)
             {
                 dgvExamCA.Columns[0].Width = 500;       //Examemn
                 dgvExamCA.Columns[1].Width = 500;      //Observación
             }
         }

         private void btnProcCA_Click(object sender, EventArgs e)
         {
             dgvExamCA.DataSource = null;
             dgvExamCA.DataSource = obj.CA_Procedim(NumFichaCA);

             for (int indice = 0; indice < dgvExamCA.Rows.Count; indice++)
             {
                 dgvExamCA.Columns[0].Width = 500;      //Procedimiento
                 dgvExamCA.Columns[1].Width = 50;       //Sesiones
                 dgvExamCA.Columns[2].Width = 300;      //Observación
             }
         }

         private void txtCantElemBase_TextChanged(object sender, EventArgs e)
         {
             btnAddCompuesto.Enabled = true;
         }

         private void btnAgregarComponente_Click(object sender, EventArgs e)
         {
             rbtPrincActivo.Checked = true;
             rbtPrincActivo.Enabled = true;
             rbtElementoBase.Enabled = true;
             cmbComponente.Enabled = true;
             btnAgregarComponente.Enabled = false;
             btnAddCompuesto.Text = "Agregar a la Fórmula";
             btnAddCompuesto.Enabled = true;
         }

         private void rbtPrincActivo_CheckedChanged(object sender, EventArgs e)
         {
             if (rbtPrincActivo.Checked)
             {
                 rbtElementoBase.Checked = false;
                 CargarComponentes("FM");
                 lblCantidad.Visible = false;
                 txtCantElemBase.Visible = false;
                 lblUndElemBase.Visible = false;
                 cmbComponente.Text = "-- Seleccione Componente --";
             }
         }

         private void rbtElementoBase_CheckedChanged(object sender, EventArgs e)
         {
             if (rbtElementoBase.Checked)
             {
                 rbtPrincActivo.Checked = false;
                 CargarComponentes("EB");
                 lblCantidad.Visible = true;
                 txtCantElemBase.Visible = true;
                 txtCantElemBase.Enabled = true;
                 lblUndElemBase.Visible = true;
                 cmbComponente.Text = "-- Seleccione Elemento Base --";
             }
         }

         private void cmbComponente_SelectedIndexChanged(object sender, EventArgs e)
         {
             try
             {
                 List<usp_RecuperaUndPAResult> lista = new List<usp_RecuperaUndPAResult>();
                 lista = obj.RecuperaUndPA(int.Parse(cmbComponente.SelectedValue.ToString()));
                 if (lista.Count() > 0)
                 {
                     foreach (usp_RecuperaUndPAResult reg in lista)
                     {
                         lblUndElemBase.Text = reg.PrA_Presentacion;
                     }
                 }
             }
             catch { }
         }

         private void cmbVigencia_SelectedIndexChanged(object sender, EventArgs e)
         {
             try
             {
                 int diasvig = 0;
                 List<uspDiasVigenciaResult> lista = new List<uspDiasVigenciaResult>();
                 lista = obj.DiasVigencia(int.Parse(cmbVigencia.SelectedValue.ToString()));
                 if (lista.Count() > 0)
                 {
                     foreach (uspDiasVigenciaResult reg in lista)
                     {
                         diasvig = reg.VigDias;
                         lblRpCaduca.Text = "CADUCA EL: " + DateTime.Today.AddDays(diasvig).ToString().Substring(0,10);
                     }
                 }
             }
             catch { }
         }
    }
}
