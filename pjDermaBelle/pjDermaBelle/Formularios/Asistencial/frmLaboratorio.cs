﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pjDermaBelle.Formularios.Asistencial
{
    public partial class frmLaboratorio : Form
    {
        public frmLaboratorio()
        {
            InitializeComponent();
        }

        ClaseDatos obj = new ClaseDatos();
        DermaBelleDataDataContext linq = new DermaBelleDataDataContext();
        FM_ExamenesAuxiliares oFM_ExamenesAuxiliares = new FM_ExamenesAuxiliares();

        public int FichaNum;

        //**********************Area de Carga De los CheckListBox***************************************************
        void CargarPerfiles()
        {
            List<usp_dgvExamenesLisResult> Perfiles = new List<usp_dgvExamenesLisResult>();
            Perfiles = obj.dgvExamenesLis(FichaNum, 1, 1);
            if (Perfiles.Count() > 0)
            {
                foreach (usp_dgvExamenesLisResult reg in Perfiles)
                {
                    chklPerfiles.Items.Add(reg.Examen, reg.Seleccionado);
                }
            }
        }

        void CargarHematologia()
        {
            List<usp_dgvExamenesLisResult> Hematologia = new List<usp_dgvExamenesLisResult>();
            Hematologia = obj.dgvExamenesLis(FichaNum, 1, 2);
            if (Hematologia.Count() > 0)
            {
                foreach (usp_dgvExamenesLisResult reg in Hematologia)
                {
                    chklHematologia.Items.Add(reg.Examen, reg.Seleccionado);
                }
            }
        }

        void CargarHemostasis()
        {
            List<usp_dgvExamenesLisResult> Hemostasis = new List<usp_dgvExamenesLisResult>();
            Hemostasis = obj.dgvExamenesLis(FichaNum, 1, 3);
            if (Hemostasis.Count() > 0)
            {
                foreach (usp_dgvExamenesLisResult reg in Hemostasis)
                {
                    chklHemostasis.Items.Add(reg.Examen, reg.Seleccionado);
                }
            }
        }

        void CargarBioquimica()
        {
            List<usp_dgvExamenesLisResult> Bioquimica = new List<usp_dgvExamenesLisResult>();
            Bioquimica = obj.dgvExamenesLis(FichaNum, 1, 4);
            if (Bioquimica.Count() > 0)
            {
                foreach (usp_dgvExamenesLisResult reg in Bioquimica)
                {
                    chklBioquimica.Items.Add(reg.Examen, reg.Seleccionado);
                }
            }
        }

        void CargarOrina()
        {
            List<usp_dgvExamenesLisResult> Orina = new List<usp_dgvExamenesLisResult>();
            Orina = obj.dgvExamenesLis(FichaNum, 1, 5);
            if (Orina.Count() > 0)
            {
                foreach (usp_dgvExamenesLisResult reg in Orina)
                {
                    chklOrina.Items.Add(reg.Examen, reg.Seleccionado);
                }
            }
        }

        void CargarDrogas()
        {
            List<usp_dgvExamenesLisResult> Drogas = new List<usp_dgvExamenesLisResult>();
            Drogas = obj.dgvExamenesLis(FichaNum, 1, 6);
            if (Drogas.Count() > 0)
            {
                foreach (usp_dgvExamenesLisResult reg in Drogas)
                {
                    chklDrogas.Items.Add(reg.Examen, reg.Seleccionado);
                }
            }
        }

        void CargarOncologicos()
        {
            List<usp_dgvExamenesLisResult> Oncologicos = new List<usp_dgvExamenesLisResult>();
            Oncologicos = obj.dgvExamenesLis(FichaNum, 1, 7);
            if (Oncologicos.Count() > 0)
            {
                foreach (usp_dgvExamenesLisResult reg in Oncologicos)
                {
                    chklOncologicos.Items.Add(reg.Examen, reg.Seleccionado);
                }
            }

        }

        void CargarHormonas()
        {
            List<usp_dgvExamenesLisResult> Hormonas = new List<usp_dgvExamenesLisResult>();
            Hormonas = obj.dgvExamenesLis(FichaNum, 1, 8);
            if (Hormonas.Count() > 0)
            {
                foreach (usp_dgvExamenesLisResult reg in Hormonas)
                {
                    chklHormonas.Items.Add(reg.Examen, reg.Seleccionado);
                }
            }
        }

        void CargarAutoinmunidad()
        {
            List<usp_dgvExamenesLisResult> Autoinmunidad = new List<usp_dgvExamenesLisResult>();
            Autoinmunidad = obj.dgvExamenesLis(FichaNum, 1, 9);
            if (Autoinmunidad.Count() > 0)
            {
                foreach (usp_dgvExamenesLisResult reg in Autoinmunidad)
                {
                    chklAutoinmunidad.Items.Add(reg.Examen, reg.Seleccionado);
                }
            }
        }

        void CargarInfecciosas()
        {
            List<usp_dgvExamenesLisResult> Infecciosas = new List<usp_dgvExamenesLisResult>();
            Infecciosas = obj.dgvExamenesLis(FichaNum, 1, 10);
            if (Infecciosas.Count() > 0)
            {
                foreach (usp_dgvExamenesLisResult reg in Infecciosas)
                {
                    chklInfecciosas.Items.Add(reg.Examen, reg.Seleccionado);
                }
            }
        }
        
        void CargarVirales()
        {
            List<usp_dgvExamenesLisResult> Virales = new List<usp_dgvExamenesLisResult>();
            Virales = obj.dgvExamenesLis(FichaNum, 1, 11);
            if (Virales.Count() > 0)
            {
                foreach (usp_dgvExamenesLisResult reg in Virales)
                {
                    chklVirales.Items.Add(reg.Examen, reg.Seleccionado);
                }
            }
        }
        
        void CargarMicrobiologia()
        {
            List<usp_dgvExamenesLisResult> Microbiologia = new List<usp_dgvExamenesLisResult>();
            Microbiologia = obj.dgvExamenesLis(FichaNum, 1, 12);
            if (Microbiologia.Count() > 0)
            {
                foreach (usp_dgvExamenesLisResult reg in Microbiologia)
                {
                    chklMicrobiologia.Items.Add(reg.Examen, reg.Seleccionado);
                }
            }
        }

        void CargarLiquidos()
        {
            List<usp_dgvExamenesLisResult> Liquidos = new List<usp_dgvExamenesLisResult>();
            Liquidos = obj.dgvExamenesLis(FichaNum, 1, 13);
            if (Liquidos.Count() > 0)
            {
                foreach (usp_dgvExamenesLisResult reg in Liquidos)
                {
                    chklLiquidos.Items.Add(reg.Examen, reg.Seleccionado);
                }
            }
        }

        void CargarOtros()
        {
            List<usp_dgvExamenesLisResult> Liquidos = new List<usp_dgvExamenesLisResult>();
            Liquidos = obj.dgvExamenesLis(FichaNum, 1, 14);
            if (Liquidos.Count() > 0)
            {
                foreach (usp_dgvExamenesLisResult reg in Liquidos)
                {
                    txtOtros.Text = reg.Observacion;
                }
            }
        }

        //**********************Area de Almacenamiento en Tablas***************************************************
        void GuardarPerfiles()
        {
            oFM_ExamenesAuxiliares.FichaMedicaId = Convert.ToSByte(FichaNum);
            oFM_ExamenesAuxiliares.ExamenTipo = 1;
            oFM_ExamenesAuxiliares.ExamenGrupo = 1;
            obj.FM_ExamAuxDel(oFM_ExamenesAuxiliares);
            foreach (object ind in chklPerfiles.CheckedItems)
            {
                string titulo = ind.ToString();
                List<usp_ExamenIdRecResult> listado = new List<usp_ExamenIdRecResult>();
                listado = obj.usp_ExamenIdRec(titulo);
                if (listado.Count() > 0)
                {
                    foreach (usp_ExamenIdRecResult reg in listado)
                    {
                        oFM_ExamenesAuxiliares.ExamenId = reg.ExamenId;
                        oFM_ExamenesAuxiliares.ExamenSelec = true;
                        obj.FM_ExamAuxAdd(oFM_ExamenesAuxiliares);
                    }
                }
            }
        }
        
        void GuardarHematologia()
        {
            oFM_ExamenesAuxiliares.FichaMedicaId = Convert.ToSByte(FichaNum);
            oFM_ExamenesAuxiliares.ExamenTipo = 1;
            oFM_ExamenesAuxiliares.ExamenGrupo = 2;
            obj.FM_ExamAuxDel(oFM_ExamenesAuxiliares);
            foreach (object ind in chklHematologia.CheckedItems)
            {
                string titulo = ind.ToString();
                List<usp_ExamenIdRecResult> listado = new List<usp_ExamenIdRecResult>();
                listado = obj.usp_ExamenIdRec(titulo);
                if (listado.Count() > 0)
                {
                    foreach (usp_ExamenIdRecResult reg in listado)
                    {
                        oFM_ExamenesAuxiliares.ExamenId = reg.ExamenId;
                        oFM_ExamenesAuxiliares.ExamenSelec = true;
                        obj.FM_ExamAuxAdd(oFM_ExamenesAuxiliares);
                    }
                }
            }
        }

        void GuardarHemostasis()
        {
            oFM_ExamenesAuxiliares.FichaMedicaId = Convert.ToSByte(FichaNum);
            oFM_ExamenesAuxiliares.ExamenTipo = 1;
            oFM_ExamenesAuxiliares.ExamenGrupo = 3;
            obj.FM_ExamAuxDel(oFM_ExamenesAuxiliares);
            foreach (object ind in chklHemostasis.CheckedItems)
            {
                string titulo = ind.ToString();
                List<usp_ExamenIdRecResult> listado = new List<usp_ExamenIdRecResult>();
                listado = obj.usp_ExamenIdRec(titulo);
                if (listado.Count() > 0)
                {
                    foreach (usp_ExamenIdRecResult reg in listado)
                    {
                        oFM_ExamenesAuxiliares.ExamenId = reg.ExamenId;
                        oFM_ExamenesAuxiliares.ExamenSelec = true;
                        obj.FM_ExamAuxAdd(oFM_ExamenesAuxiliares);
                    }
                }
            }
        }

        void GuardarBioquimica()
        {
            oFM_ExamenesAuxiliares.FichaMedicaId = Convert.ToSByte(FichaNum);
            oFM_ExamenesAuxiliares.ExamenTipo = 1;
            oFM_ExamenesAuxiliares.ExamenGrupo = 4;
            obj.FM_ExamAuxDel(oFM_ExamenesAuxiliares);
            foreach (object ind in chklBioquimica.CheckedItems)
            {
                string titulo = ind.ToString();
                List<usp_ExamenIdRecResult> listado = new List<usp_ExamenIdRecResult>();
                listado = obj.usp_ExamenIdRec(titulo);
                if (listado.Count() > 0)
                {
                    foreach (usp_ExamenIdRecResult reg in listado)
                    {
                        oFM_ExamenesAuxiliares.ExamenId = reg.ExamenId;
                        oFM_ExamenesAuxiliares.ExamenSelec = true;
                        obj.FM_ExamAuxAdd(oFM_ExamenesAuxiliares);
                    }
                }
            }
        }

        void GuardarOrina()
        {
            oFM_ExamenesAuxiliares.FichaMedicaId = Convert.ToSByte(FichaNum);
            oFM_ExamenesAuxiliares.ExamenTipo = 1;
            oFM_ExamenesAuxiliares.ExamenGrupo = 5;
            obj.FM_ExamAuxDel(oFM_ExamenesAuxiliares);
            foreach (object ind in chklOrina.CheckedItems)
            {
                string titulo = ind.ToString();
                List<usp_ExamenIdRecResult> listado = new List<usp_ExamenIdRecResult>();
                listado = obj.usp_ExamenIdRec(titulo);
                if (listado.Count() > 0)
                {
                    foreach (usp_ExamenIdRecResult reg in listado)
                    {
                        oFM_ExamenesAuxiliares.ExamenId = reg.ExamenId;
                        oFM_ExamenesAuxiliares.ExamenSelec = true;
                        obj.FM_ExamAuxAdd(oFM_ExamenesAuxiliares);
                    }
                }
            }
        }

        void GuardarDrogas()
        {
            oFM_ExamenesAuxiliares.FichaMedicaId = Convert.ToSByte(FichaNum);
            oFM_ExamenesAuxiliares.ExamenTipo = 1;
            oFM_ExamenesAuxiliares.ExamenGrupo = 6;
            obj.FM_ExamAuxDel(oFM_ExamenesAuxiliares);
            foreach (object ind in chklDrogas.CheckedItems)
            {
                string titulo = ind.ToString();
                List<usp_ExamenIdRecResult> listado = new List<usp_ExamenIdRecResult>();
                listado = obj.usp_ExamenIdRec(titulo);
                if (listado.Count() > 0)
                {
                    foreach (usp_ExamenIdRecResult reg in listado)
                    {
                        oFM_ExamenesAuxiliares.ExamenId = reg.ExamenId;
                        oFM_ExamenesAuxiliares.ExamenSelec = true;
                        obj.FM_ExamAuxAdd(oFM_ExamenesAuxiliares);
                    }
                }
            }
        }

        void GuardarOncologicos()
        {
            oFM_ExamenesAuxiliares.FichaMedicaId = Convert.ToSByte(FichaNum);
            oFM_ExamenesAuxiliares.ExamenTipo = 1;
            oFM_ExamenesAuxiliares.ExamenGrupo = 7;
            obj.FM_ExamAuxDel(oFM_ExamenesAuxiliares);
            foreach (object ind in chklOncologicos.CheckedItems)
            {
                string titulo = ind.ToString();
                List<usp_ExamenIdRecResult> listado = new List<usp_ExamenIdRecResult>();
                listado = obj.usp_ExamenIdRec(titulo);
                if (listado.Count() > 0)
                {
                    foreach (usp_ExamenIdRecResult reg in listado)
                    {
                        oFM_ExamenesAuxiliares.ExamenId = reg.ExamenId;
                        oFM_ExamenesAuxiliares.ExamenSelec = true;
                        obj.FM_ExamAuxAdd(oFM_ExamenesAuxiliares);
                    }
                }
            }
        }

        void GuardarHormonas()
        {
            oFM_ExamenesAuxiliares.FichaMedicaId = Convert.ToSByte(FichaNum);
            oFM_ExamenesAuxiliares.ExamenTipo = 1;
            oFM_ExamenesAuxiliares.ExamenGrupo = 8;
            obj.FM_ExamAuxDel(oFM_ExamenesAuxiliares);
            foreach (object ind in chklHormonas.CheckedItems)
            {
                string titulo = ind.ToString();
                List<usp_ExamenIdRecResult> listado = new List<usp_ExamenIdRecResult>();
                listado = obj.usp_ExamenIdRec(titulo);
                if (listado.Count() > 0)
                {
                    foreach (usp_ExamenIdRecResult reg in listado)
                    {
                        oFM_ExamenesAuxiliares.ExamenId = reg.ExamenId;
                        oFM_ExamenesAuxiliares.ExamenSelec = true;
                        obj.FM_ExamAuxAdd(oFM_ExamenesAuxiliares);
                    }
                }
            }
        }

        void GuardarAutoinmunidad()
        {
            oFM_ExamenesAuxiliares.FichaMedicaId = Convert.ToSByte(FichaNum);
            oFM_ExamenesAuxiliares.ExamenTipo = 1;
            oFM_ExamenesAuxiliares.ExamenGrupo = 9;
            obj.FM_ExamAuxDel(oFM_ExamenesAuxiliares);
            foreach (object ind in chklAutoinmunidad.CheckedItems)
            {
                string titulo = ind.ToString();
                List<usp_ExamenIdRecResult> listado = new List<usp_ExamenIdRecResult>();
                listado = obj.usp_ExamenIdRec(titulo);
                if (listado.Count() > 0)
                {
                    foreach (usp_ExamenIdRecResult reg in listado)
                    {
                        oFM_ExamenesAuxiliares.ExamenId = reg.ExamenId;
                        oFM_ExamenesAuxiliares.ExamenSelec = true;
                        obj.FM_ExamAuxAdd(oFM_ExamenesAuxiliares);
                    }
                }
            }
        }

        void GuardarInfecciosas()
        {
            oFM_ExamenesAuxiliares.FichaMedicaId = Convert.ToSByte(FichaNum);
            oFM_ExamenesAuxiliares.ExamenTipo = 1;
            oFM_ExamenesAuxiliares.ExamenGrupo = 10;
            obj.FM_ExamAuxDel(oFM_ExamenesAuxiliares);
            foreach (object ind in chklInfecciosas.CheckedItems)
            {
                string titulo = ind.ToString();
                List<usp_ExamenIdRecResult> listado = new List<usp_ExamenIdRecResult>();
                listado = obj.usp_ExamenIdRec(titulo);
                if (listado.Count() > 0)
                {
                    foreach (usp_ExamenIdRecResult reg in listado)
                    {
                        oFM_ExamenesAuxiliares.ExamenId = reg.ExamenId;
                        oFM_ExamenesAuxiliares.ExamenSelec = true;
                        obj.FM_ExamAuxAdd(oFM_ExamenesAuxiliares);
                    }
                }
            }
        }

        void GuardarVirales()
        {
            oFM_ExamenesAuxiliares.FichaMedicaId = Convert.ToSByte(FichaNum);
            oFM_ExamenesAuxiliares.ExamenTipo = 11;
            oFM_ExamenesAuxiliares.ExamenGrupo = 2;
            obj.FM_ExamAuxDel(oFM_ExamenesAuxiliares);
            foreach (object ind in chklVirales.CheckedItems)
            {
                string titulo = ind.ToString();
                List<usp_ExamenIdRecResult> listado = new List<usp_ExamenIdRecResult>();
                listado = obj.usp_ExamenIdRec(titulo);
                if (listado.Count() > 0)
                {
                    foreach (usp_ExamenIdRecResult reg in listado)
                    {
                        oFM_ExamenesAuxiliares.ExamenId = reg.ExamenId;
                        oFM_ExamenesAuxiliares.ExamenSelec = true;
                        obj.FM_ExamAuxAdd(oFM_ExamenesAuxiliares);
                    }
                }
            }
        }

        void GuardarMicrobiologia()
        {
            oFM_ExamenesAuxiliares.FichaMedicaId = Convert.ToSByte(FichaNum);
            oFM_ExamenesAuxiliares.ExamenTipo = 1;
            oFM_ExamenesAuxiliares.ExamenGrupo = 12;
            obj.FM_ExamAuxDel(oFM_ExamenesAuxiliares);
            foreach (object ind in chklMicrobiologia.CheckedItems)
            {
                string titulo = ind.ToString();
                List<usp_ExamenIdRecResult> listado = new List<usp_ExamenIdRecResult>();
                listado = obj.usp_ExamenIdRec(titulo);
                if (listado.Count() > 0)
                {
                    foreach (usp_ExamenIdRecResult reg in listado)
                    {
                        oFM_ExamenesAuxiliares.ExamenId = reg.ExamenId;
                        oFM_ExamenesAuxiliares.ExamenSelec = true;
                        obj.FM_ExamAuxAdd(oFM_ExamenesAuxiliares);
                    }
                }
            }
        }

        void GuardarLiquidos()
        {
            oFM_ExamenesAuxiliares.FichaMedicaId = Convert.ToSByte(FichaNum);
            oFM_ExamenesAuxiliares.ExamenTipo = 1;
            oFM_ExamenesAuxiliares.ExamenGrupo = 13;
            obj.FM_ExamAuxDel(oFM_ExamenesAuxiliares);
            foreach (object ind in chklLiquidos.CheckedItems)
            {
                string titulo = ind.ToString();
                List<usp_ExamenIdRecResult> listado = new List<usp_ExamenIdRecResult>();
                listado = obj.usp_ExamenIdRec(titulo);
                if (listado.Count() > 0)
                {
                    foreach (usp_ExamenIdRecResult reg in listado)
                    {
                        oFM_ExamenesAuxiliares.ExamenId = reg.ExamenId;
                        oFM_ExamenesAuxiliares.ExamenSelec = true;
                        obj.FM_ExamAuxAdd(oFM_ExamenesAuxiliares);
                    }
                }
            }
        }

        void GuardarOtros()
        {
            oFM_ExamenesAuxiliares.FichaMedicaId = Convert.ToSByte(FichaNum);
            oFM_ExamenesAuxiliares.ExamenTipo = 1;
            oFM_ExamenesAuxiliares.ExamenGrupo = 14;
            obj.FM_ExamAuxDel(oFM_ExamenesAuxiliares);
            string titulo = "OTROS LABORATORIOS";
            List<usp_ExamenIdRecResult> listado = new List<usp_ExamenIdRecResult>();
            listado = obj.usp_ExamenIdRec(titulo);
            if (listado.Count() > 0)
            {
                foreach (usp_ExamenIdRecResult reg in listado)
                {
                    oFM_ExamenesAuxiliares.ExamenId = reg.ExamenId;
                    oFM_ExamenesAuxiliares.ExamenObserv = txtOtros.Text.ToUpper();
                    oFM_ExamenesAuxiliares.ExamenSelec = true;
                    obj.FM_ExamAuxAdd(oFM_ExamenesAuxiliares);
                }
            }
        }

        private void frmLaboratorio_Load(object sender, EventArgs e)
        {
            CargarPerfiles();
            CargarHematologia();
            CargarHemostasis();
            CargarBioquimica();
            CargarOrina();
            CargarDrogas();
            CargarOncologicos();
            CargarHormonas();
            CargarAutoinmunidad();
            CargarInfecciosas();
            CargarVirales();
            CargarMicrobiologia();
            CargarLiquidos();
            CargarOtros();
        }

        private void frmLaboratorio_FormClosed(object sender, FormClosedEventArgs e)
        {
            GuardarPerfiles();
            GuardarHematologia();
            GuardarHemostasis();
            GuardarBioquimica();
            GuardarOrina();
            GuardarDrogas();
            GuardarOncologicos();
            GuardarHormonas();
            GuardarAutoinmunidad();
            GuardarInfecciosas();
            GuardarVirales();
            GuardarMicrobiologia();
            GuardarLiquidos();
            GuardarOtros();
        }
    }
}
