﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pjDermaBelle
{
    public partial class frmMantEspecialidades : Form
    {
        public frmMantEspecialidades()
        {
            InitializeComponent();
        }

        ClaseDatos obj = new ClaseDatos();
        DermaBelleDataDataContext linq = new DermaBelleDataDataContext();
        Especialidades oEspecialidades = new Especialidades();

        bool nuevo = true;

        private void Limpiar()
        {
            txtId.Clear();
            txtNombre.Clear();
            chkEstado.Checked = false;
            txtId.Enabled = false;
            txtNombre.Enabled = false;
            btnNuevo.Enabled = true;
            btnGrabar.Enabled = false;
            btnActualizar.Enabled = false;
            btnAnular.Enabled = false;
            btnCerrar.Enabled = true;

            lblCreado.Visible = false;
            lblFecCrea.Visible = false;
            lblModificado.Visible = false;
            lblFecMod.Visible = false;

            btnNuevo.Focus();
        }

        private void frmMantEspecialidades_Load(object sender, EventArgs e)
        {
            dgvLista.DataSource = obj.EspecialidadesListar();
            Limpiar();
        }

        private void txtNombre_Leave(object sender, EventArgs e)
        {
            if (nuevo)
            {
                btnGrabar.Focus();
            }
            else
            { btnActualizar.Focus(); }
        }

        private void txtNombre_TextChanged(object sender, EventArgs e)
        {
            btnNuevo.Enabled = false;
            if (nuevo)
            {
                btnGrabar.Enabled = txtNombre.Text != string.Empty;
                btnAnular.Enabled = false;
            }
            else
            {
                btnGrabar.Enabled = false;
                btnActualizar.Enabled = true;
                btnAnular.Enabled = true;
            }
            btnCerrar.Enabled = true;
        }

        private void dgvLista_SelectionChanged(object sender, EventArgs e)
        {
            int indice = dgvLista.CurrentCell.RowIndex;
            int Esp_Id = int.Parse(dgvLista.Rows[indice].Cells[0].Value.ToString());

            List<usp_EspecialidadesRecResult> lista = new List<usp_EspecialidadesRecResult>();
            lista = obj.EspecialidadesDatos(Esp_Id);
            if (lista.Count() > 0)
            {
                foreach (usp_EspecialidadesRecResult reg in lista)
                {
                    txtId.Text = reg.Esp_Id.ToString();
                    txtNombre.Text = reg.Esp_Nombre;
                    lblFecCrea.Text = reg.FecCrea.ToString();
                    lblFecMod.Text = reg.FecModi.ToString();
                    chkEstado.Checked = reg.Estado;

                    if (chkEstado.Checked)
                    { btnAnular.Text = "A&nular"; }
                    else
                    { btnAnular.Text = "A&ctivar"; }

                    txtNombre.Enabled = true;
                    btnNuevo.Enabled = false;
                    btnGrabar.Enabled = false;
                    btnActualizar.Enabled = true;
                    btnAnular.Enabled = true;
                    btnCerrar.Enabled = true;

                    lblCreado.Visible = true;
                    lblFecCrea.Visible = true;
                    lblModificado.Visible = true;
                    lblFecMod.Visible = true;

                    nuevo = false;
                    txtNombre.Focus();
                }
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Limpiar();

            txtId.Enabled = false;
            txtNombre.Enabled = true;
            chkEstado.Checked = true;
            btnNuevo.Enabled = false;
            btnGrabar.Enabled = false;
            btnActualizar.Enabled = false;
            btnAnular.Enabled = false;
            btnCerrar.Enabled = true;

            txtNombre.Focus();
            nuevo = true;
        }

        private void btnGrabar_Click(object sender, EventArgs e)
        {
            oEspecialidades.Esp_Nombre = txtNombre.Text;
            oEspecialidades.Estado = chkEstado.Checked;
            oEspecialidades.UsuCrea = 1;
            oEspecialidades.UsuModi = 1;
            obj.EspecialidadesAdicionar(oEspecialidades);
            dgvLista.DataSource = obj.EspecialidadesListar();
            Limpiar();
            MessageBox.Show("Registro GRABADO satisfactoriamente", "AVISO");
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            oEspecialidades.Esp_Id = Convert.ToInt16(txtId.Text);
            oEspecialidades.Esp_Nombre = txtNombre.Text;
            oEspecialidades.UsuModi = 1;
            obj.EspecialidadesActualizar(oEspecialidades);
            dgvLista.DataSource = obj.EspecialidadesListar();
            Limpiar();
            MessageBox.Show("Registro ACTUALIZADO satisfactoriamente", "AVISO");
        }

        private void btnAnular_Click(object sender, EventArgs e)
        {
            oEspecialidades.Esp_Id = Convert.ToInt16(txtId.Text);
            oEspecialidades.Esp_Nombre = txtNombre.Text;
            oEspecialidades.Estado = !chkEstado.Checked;
            oEspecialidades.UsuModi = 1;
            obj.EspecialidadesAnular(oEspecialidades);
            dgvLista.DataSource = obj.EspecialidadesListar();
            Limpiar();
            if (oEspecialidades.Estado)
            { MessageBox.Show("Registro ACTIVADO satisfactoriamente", "AVISO"); }
            else
            { MessageBox.Show("Registro ANULADO satisfactoriamente", "AVISO"); }
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            if ((MessageBox.Show("¿Seguro que desea salir ?", "Aviso",
                     MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                     MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes))
            {
                this.Close();
            }
        }
    }
}
