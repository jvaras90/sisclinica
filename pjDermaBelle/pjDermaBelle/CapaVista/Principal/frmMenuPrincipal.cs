﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pjDermaBelle
{
    public partial class frmMenuPrincipal : Form
    {
        public frmMenuPrincipal()
        {
            InitializeComponent();
        }

        private void Cerrar()
        {
            if ((MessageBox.Show("¿Seguro que desea salir ?", "Aviso",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                    MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes))
            {
                this.Close();
            }
        }

        private void profesionesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMantProfesiones frm = new frmMantProfesiones();
            frm.Show();
        }

        private void gradosDeInstrucciónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMantGradosInstruccion frm = new frmMantGradosInstruccion();
            frm.Show();
        }

        private void ubigeoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMantUbigeo frm = new frmMantUbigeo();
            frm.Show();
        }

        private void documentosDeIdentidadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMantDocIdentidad frm = new frmMantDocIdentidad();
            frm.Show();
        }

        private void filiaciónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMantPacientes frm = new frmMantPacientes();
            frm.Show();
        }

        private void controlToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAdmCtrlAsistPacientes frm = new frmAdmCtrlAsistPacientes();
            frm.Show();
        }

        private void especialidadesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMantEspecialidades frm = new frmMantEspecialidades();
            frm.Show();
        }

        private void registroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMantProfSalud frm = new frmMantProfSalud();
            frm.Show();
        }

        private void serviciosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMantTarifario frm = new frmMantTarifario();
            frm.Show();
        }

        private void tipoDeTarifaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMantTiposTarifa frm = new frmMantTiposTarifa();
            frm.Show();
        }

        private void principiosActivosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMantPrincipiosActivos frm = new frmMantPrincipiosActivos();
            frm.Show();
        }

        private void laboratoriosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMantLaboratorios frm = new frmMantLaboratorios();
            frm.Show();
        }

        private void actoMédicoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmPacCitados frm = new frmPacCitados();
            frm.Show();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cerrar();
        }
    }
}
