﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pjDermaBelle
{
    public partial class frmMantProfesiones : Form
    {
        public frmMantProfesiones()
        {
            InitializeComponent();
        }

        ClaseDatos obj = new ClaseDatos();
        DermaBelleDataDataContext linq = new DermaBelleDataDataContext(); 
        Profesiones oProfesiones = new Profesiones();
        
        bool nuevo  = true;

        private void Limpiar()
        {
            txtId.Clear();
            txtNombre.Clear();
            chkEstado.Checked = false;
            txtId.Enabled = false;
            txtNombre.Enabled = false;
            btnNuevo.Enabled = true;
            btnGrabar.Enabled = false;
            btnActualizar.Enabled = false;
            btnAnular.Enabled = false;
            btnCerrar.Enabled = true;

            lblCreado.Visible = false;
            lblFecCrea.Visible = false;
            lblModificado.Visible = false;
            lblFecMod.Visible = false;

            btnNuevo.Focus();
        }

        private void frmMantProfesiones_Load(object sender, EventArgs e)
        {
            lblFecha.Text = obj.Fecha;
            //lblHora.Text = obj.Hora; 
            dgvLista.DataSource = obj.ProfesionListar();
            Limpiar();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //lblHora.Text = obj.Hora;
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Limpiar();
            lblFecCrea.Text = lblFecha.Text;
            
            txtId.Enabled = true;
            txtNombre.Enabled = true;
            chkEstado.Checked = true;
            btnNuevo.Enabled = false;
            btnGrabar.Enabled = false;
            btnActualizar.Enabled = false;
            btnAnular.Enabled = false;
            btnCerrar.Enabled = true;

            txtId.Focus();
            nuevo = true;
        }

        private void btnGrabar_Click_1(object sender, EventArgs e)
        {
            oProfesiones.Prf_Id = Convert.ToInt16(txtId.Text);
            oProfesiones.Prf_Nombre = txtNombre.Text;
            oProfesiones.Estado = chkEstado.Checked;
            oProfesiones.UsuCrea = 1;
            oProfesiones.UsuMod = 1;
            obj.ProfesionesAdicionar(oProfesiones);
            dgvLista.DataSource = obj.ProfesionListar();
            Limpiar();
            MessageBox.Show("Registro GRABADO satisfactoriamente", "AVISO");
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            oProfesiones.Prf_Id = Convert.ToInt16(txtId.Text);
            oProfesiones.Prf_Nombre = txtNombre.Text;
            oProfesiones.UsuMod = 1;
            obj.ProfesionesActualizar(oProfesiones);
            dgvLista.DataSource = obj.ProfesionListar();
            Limpiar();
            MessageBox.Show("Registro ACTUALIZADO satisfactoriamente", "AVISO");
        }

        private void dgvLista_SelectionChanged(object sender, EventArgs e)
        {
            int indice = dgvLista.CurrentCell.RowIndex;
            int prfId = int.Parse(dgvLista.Rows[indice].Cells[0].Value.ToString());

            List<usp_ProfesionesRecResult> lista = new List<usp_ProfesionesRecResult>();
            lista = obj.ProfesionDatos(prfId);
            if (lista.Count() > 0)
            {
                foreach (usp_ProfesionesRecResult reg in lista)
                {
                    txtId.Text = reg.Prf_Id.ToString();
                    txtNombre.Text = reg.Prf_Nombre;
                    lblFecCrea.Text  = reg.FecCrea.ToString();
                    lblFecMod.Text = reg.FecMod.ToString();
                    chkEstado.Checked = reg.Estado;

                    if (chkEstado.Checked)
                    { btnAnular.Text = "A&nular"; }
                    else
                    { btnAnular.Text = "A&ctivar"; }

                    txtNombre.Enabled = true;
                    btnNuevo.Enabled = false;
                    btnGrabar.Enabled = false;
                    btnActualizar.Enabled = true;
                    btnAnular.Enabled = true;
                    btnCerrar.Enabled = true;

                    lblCreado.Visible = true;
                    lblFecCrea.Visible = true;
                    lblModificado.Visible = true;
                    lblFecMod.Visible = true;

                    nuevo = false;
                    txtNombre.Focus();
                }
            }
        }

        private void btnAnular_Click(object sender, EventArgs e)
        {
            oProfesiones.Prf_Id = Convert.ToInt16(txtId.Text);
            oProfesiones.Estado = !chkEstado.Checked;
            oProfesiones.UsuMod = 1;
            obj.ProfesionesAnular(oProfesiones);
            dgvLista.DataSource = obj.ProfesionListar();
            Limpiar();
            if (oProfesiones.Estado)
            { MessageBox.Show("Registro ACTIVADO satisfactoriamente", "AVISO"); }
            else
            { MessageBox.Show("Registro ANULADO satisfactoriamente", "AVISO"); }
        }

        private void txtNombre_TextChanged_1(object sender, EventArgs e)
        {
            btnNuevo.Enabled = false;
            if (nuevo)
            {
                btnGrabar.Enabled = txtId.Text != string.Empty &&
                     txtNombre.Text != string.Empty;
                btnAnular.Enabled = false;
            }
            else
            {
                btnGrabar.Enabled = false;
                btnActualizar.Enabled = true;
                btnAnular.Enabled = true;
            }
            btnCerrar.Enabled = true;

        }

        private void txtNombre_Leave(object sender, EventArgs e)
        {
            if (nuevo)
            {
                btnGrabar.Focus();
            }
            else
            { btnActualizar.Focus(); }
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            if ((MessageBox.Show("¿Seguro que desea salir ?", "Aviso",
                     MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                     MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes))
            {
                this.Close();
            }
        }

    }
}
