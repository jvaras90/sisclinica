﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pjDermaBelle
{
    public partial class frmMantPacientes : Form
    {
        public frmMantPacientes()
        {
            InitializeComponent();
        }

        ClaseDatos obj = new ClaseDatos();
        DermaBelleDataDataContext linq = new DermaBelleDataDataContext();
        Pacientes oPacientes = new Pacientes();

        bool nuevo = true;

        void DocIdentidad()
        {
            cboDocIdentidad.DataSource = obj.CboDocIdentidad();
            cboDocIdentidad.ValueMember = "Did_Id";
            cboDocIdentidad.DisplayMember = "Did_Nombre";
        }

        void Instruccion()
        {
            cboInstruccion.DataSource = obj.CboInstruccion();
            cboInstruccion.ValueMember = "GrI_Id";
            cboInstruccion.DisplayMember = "GrI_Nombre";
        }

        void Profesiones()
        {
            cboProfesion.DataSource = obj.CboProfesiones();
            cboProfesion.ValueMember = "Prf_Id";
            cboProfesion.DisplayMember = "Prf_Nombre";
        }

        void LugarNacimiento()
        {
            cboLugNacimiento.DataSource = obj.CboLugNacimiento();
            cboLugNacimiento.ValueMember = "Ubi_Cod";
            cboLugNacimiento.DisplayMember = "Ubi_Nombre";
        }

        void DistPaciente()
        {
            cboDistPac.DataSource = obj.CboDistritos();
            cboDistPac.ValueMember = "Ubi_Cod";
            cboDistPac.DisplayMember = "Ubi_Nombre";
        }

        void DistResponsable()
        {
            cboDistResp.DataSource = obj.CboDistritos();
            cboDistResp.ValueMember = "Ubi_Cod";
            cboDistResp.DisplayMember = "Ubi_Nombre";
        }

        void DistFacturacion()
        {
            cboDistFacturacion.DataSource = obj.CboDistritos();
            cboDistFacturacion.ValueMember = "Ubi_Cod";
            cboDistFacturacion.DisplayMember = "Ubi_Nombre";
        }

        void Limpiar()
        {
            //Limpiar
            txtHistoria.Clear();
            txtPac_ApMat.Clear();
            txtPac_ApPat.Clear();
            txtPac_Direccion.Clear();
            txtPac_DirecFact.Clear();
            txtPac_DirecResp.Clear();
            txtPac_DocIdentNum.Clear();
            txtPac_Email.Clear();
            txtPac_EmailResp.Clear();
            txtPac_HC.Clear();
            txtPac_NombreFact.Clear();
            txtPac_Nombres.Clear();
            txtPac_PersResp.Clear();
            txtPac_RUCFact.Clear();
            txtPac_Telefonos.Clear();
            txtPac_TelefResp.Clear();
            txtPaciente.Clear();

            lblPac_NomComp.Text = " ";

            rbtFemenino.Checked = false;
            rbtMasculino.Checked = false;             

            //Desactivar Controles de datos
            txtPac_ApMat.Enabled = false;
            txtPac_ApPat.Enabled = false;
            txtPac_Direccion.Enabled = false;
            txtPac_DirecFact.Enabled = false;
            txtPac_DirecResp.Enabled = false;
            txtPac_DocIdentNum.Enabled = false;
            txtPac_Email.Enabled = false;
            txtPac_EmailResp.Enabled = false;
            txtPac_HC.Enabled = false;
            txtPac_NombreFact.Enabled = false;
            txtPac_Nombres.Enabled = false;
            txtPac_PersResp.Enabled = false;
            txtPac_RUCFact.Enabled = false; 
            txtPac_Telefonos.Enabled = false;
            txtPac_TelefResp.Enabled = false;

            dtpPac_FecNac.Enabled = false;

            rbtFemenino.Enabled = false;
            rbtMasculino.Enabled = false;

            cboDistFacturacion.Enabled = false;
            cboDistPac.Enabled = false;
            cboDistResp.Enabled = false;
            cboDocIdentidad.Enabled = false;
            cboInstruccion.Enabled = false;
            cboLugNacimiento.Enabled = false;
            cboProfesion.Enabled = false;
            cboGpoSang.Enabled = false;

            //Activar y Desactivar Botones
            btnNuevo.Enabled = true;
            btnGrabar.Enabled = false;
            btnActualizar.Enabled = false;
            btnAnular.Enabled = false;
            btnCerrar.Enabled = true;

            lblCreado.Visible = false;
            lblFecCrea.Visible = false;
            lblModificado.Visible = false;
            lblFecMod.Visible = false;

            //Dirigir Enfoque
            btnNuevo.Focus();
        }

        void NuevoReg()
        {
            Limpiar();
            nuevo = true;

            //Activar Controles de datos
            txtPac_ApMat.Enabled = true;
            txtPac_ApPat.Enabled = true;
            txtPac_Direccion.Enabled = true;
            txtPac_DirecFact.Enabled = true;
            txtPac_DirecResp.Enabled = true;
            txtPac_DocIdentNum.Enabled = true;
            txtPac_Email.Enabled = true;
            txtPac_EmailResp.Enabled = true;
            txtPac_HC.Enabled = true;
            txtPac_NombreFact.Enabled = true;
            txtPac_Nombres.Enabled = true;
            txtPac_PersResp.Enabled = true;
            txtPac_RUCFact.Enabled = true;
            txtPac_Telefonos.Enabled = true;
            txtPac_TelefResp.Enabled = true;

            dtpPac_FecNac.Enabled = true;

            rbtFemenino.Enabled = true;
            rbtMasculino.Enabled = true;

            cboDistFacturacion.Enabled = true;
            cboDistPac.Enabled = true;
            cboDistResp.Enabled = true;
            cboDocIdentidad.Enabled = true;
            cboInstruccion.Enabled = true;
            cboLugNacimiento.Enabled = true;
            cboProfesion.Enabled = true;
            cboGpoSang.Enabled = true;

            //Activar y Desactivar Botones
            btnNuevo.Enabled = true;
            btnGrabar.Enabled = false;
            btnActualizar.Enabled = false;
            btnAnular.Enabled = false;
            btnCerrar.Enabled = true;

            lblCreado.Visible = false;
            lblFecCrea.Visible = false;
            lblModificado.Visible = false;
            lblFecMod.Visible = false;

            //Dirigir Enfoque
            txtPac_HC.Focus();
        }

        private void frmMantPacientes_Load(object sender, EventArgs e)
        {
            DocIdentidad();
            Instruccion();
            Profesiones();
            LugarNacimiento();
            DistPaciente();
            DistResponsable();
            DistFacturacion();
            Limpiar();
            dgvPacientes.DataSource = obj.PacientesListar();
            tabControl1.SelectTab("tbcListado");
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            NuevoReg();
        }

        private void txtPac_Nombres_Leave(object sender, EventArgs e)
        {
            lblPac_NomComp.Text = txtPac_ApPat.Text.ToUpper() + " " +
                                  txtPac_ApMat.Text.ToUpper() + ", " +
                                  txtPac_Nombres.Text.ToUpper();
        }

        private void txtPac_Telefonos_Leave(object sender, EventArgs e)
        {
            if (nuevo)
            {
                btnGrabar.Focus();
            }
            else
            { btnActualizar.Focus(); }
        }

        private void txtPac_Telefonos_TextChanged(object sender, EventArgs e)
        {
            btnNuevo.Enabled = false;
            if (nuevo)
            {
                btnGrabar.Enabled = txtPac_Telefonos.Text != string.Empty;
                btnAnular.Enabled = false;
            }
            else
            {
                btnGrabar.Enabled = false;
                btnActualizar.Enabled = true;
                btnAnular.Enabled = true;
            }
            btnCerrar.Enabled = true;
        }

        private void btnGrabar_Click(object sender, EventArgs e)
        {
            oPacientes.Pac_HC = txtPac_HC.Text;
            oPacientes.Pac_ApPat = txtPac_ApPat.Text;
            oPacientes.Pac_ApMat = txtPac_ApMat.Text;
            oPacientes.Pac_Direccion = txtPac_Direccion.Text;
            oPacientes.Pac_DirecFact = txtPac_DirecFact.Text;
            oPacientes.Pac_DirecResp = txtPac_DirecResp.Text;
            oPacientes.Pac_DocIdentNum = txtPac_DocIdentNum.Text;
            oPacientes.Pac_Email = txtPac_Email.Text;
            oPacientes.Pac_EmailResp = txtPac_EmailResp.Text;
            oPacientes.Pac_NombreFact = txtPac_NombreFact.Text;
            oPacientes.Pac_Nombres = txtPac_Nombres.Text;
            oPacientes.Pac_PersResp = txtPac_PersResp.Text;
            oPacientes.Pac_RUCFact = txtPac_RUCFact.Text;
            oPacientes.Pac_Telefonos = txtPac_Telefonos.Text;
            oPacientes.Pac_TelefResp = txtPac_TelefResp.Text;

            oPacientes.Pac_NomComp = lblPac_NomComp.Text;

            oPacientes.Pac_FecNac = dtpPac_FecNac.Value.Date;

            if (rbtFemenino.Checked == true)
            { oPacientes.Pac_Sexo = 'F'; }

            if (rbtMasculino.Checked == true)
            { oPacientes.Pac_Sexo = 'M'; }

            oPacientes.Pac_DistFactId = cboDistFacturacion.SelectedValue.ToString();
            oPacientes.Pac_DistritoId = cboDistPac.SelectedValue.ToString();
            oPacientes.Pac_DistRespId = cboDistResp.SelectedValue.ToString();
            oPacientes.Pac_DocIdentId = short.Parse(cboDocIdentidad.SelectedValue.ToString());
            oPacientes.Pac_LugNacId = cboLugNacimiento.SelectedValue.ToString();
            oPacientes.Pac_InstruccionId = short.Parse(cboInstruccion.SelectedValue.ToString());
            oPacientes.Pac_ProfesionId = short.Parse(cboProfesion.SelectedValue.ToString());
            oPacientes.Pac_GpoSang = cboGpoSang.Text;

            oPacientes.Estado = true;
            oPacientes.UsuCrea = 1;
            oPacientes.UsuModi = 1;
            obj.PacientesAdicionar(oPacientes);
            dgvPacientes.DataSource = obj.PacientesListar();
            Limpiar();
            MessageBox.Show("Registro GRABADO satisfactoriamente", "AVISO");
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            oPacientes.Pac_HC = txtPac_HC.Text;
            oPacientes.Pac_ApPat = txtPac_ApPat.Text;
            oPacientes.Pac_ApMat = txtPac_ApMat.Text;
            oPacientes.Pac_Direccion = txtPac_Direccion.Text;
            oPacientes.Pac_DirecFact = txtPac_DirecFact.Text;
            oPacientes.Pac_DirecResp = txtPac_DirecResp.Text;
            oPacientes.Pac_DocIdentNum = txtPac_DocIdentNum.Text;
            oPacientes.Pac_Email = txtPac_Email.Text;
            oPacientes.Pac_EmailResp = txtPac_EmailResp.Text;
            oPacientes.Pac_NombreFact = txtPac_NombreFact.Text;
            oPacientes.Pac_Nombres = txtPac_Nombres.Text;
            oPacientes.Pac_PersResp = txtPac_PersResp.Text;
            oPacientes.Pac_RUCFact = txtPac_RUCFact.Text;
            oPacientes.Pac_Telefonos = txtPac_Telefonos.Text;
            oPacientes.Pac_TelefResp = txtPac_TelefResp.Text;

            oPacientes.Pac_NomComp = lblPac_NomComp.Text;

            oPacientes.Pac_FecNac = dtpPac_FecNac.Value.Date;

            if (rbtFemenino.Checked == true)
            { oPacientes.Pac_Sexo = 'F'; }

            if (rbtMasculino.Checked == true)
            { oPacientes.Pac_Sexo = 'M'; }

            oPacientes.Pac_DistFactId = cboDistFacturacion.SelectedValue.ToString();
            oPacientes.Pac_DistritoId = cboDistPac.SelectedValue.ToString();
            oPacientes.Pac_DistRespId = cboDistResp.SelectedValue.ToString();
            oPacientes.Pac_LugNacId = cboLugNacimiento.SelectedValue.ToString();
            oPacientes.Pac_DocIdentId = short.Parse(cboDocIdentidad.SelectedValue.ToString());
            oPacientes.Pac_InstruccionId = short.Parse(cboInstruccion.SelectedValue.ToString());
            oPacientes.Pac_ProfesionId = short.Parse(cboProfesion.SelectedValue.ToString());
            oPacientes.Pac_GpoSang = cboGpoSang.Text;

            oPacientes.Estado = true;
            oPacientes.UsuCrea = 1;
            oPacientes.UsuModi = 1;
            obj.PacientesActualizar(oPacientes);
            dgvPacientes.DataSource = obj.PacientesListar();
            Limpiar();
            MessageBox.Show("Registro MODIFICADO satisfactoriamente", "AVISO");

        }

        private void dgvPacientes_SelectionChanged(object sender, EventArgs e)
        {
            tabControl1.SelectTab("tbcRegistro");
            int indice = dgvPacientes.CurrentCell.RowIndex;
            string Pac_HC = dgvPacientes.Rows[indice].Cells[0].Value.ToString();

            List<usp_PacientesRecResult> lista = new List<usp_PacientesRecResult>();
            lista = obj.PacientesDatos(Pac_HC);
            if (lista.Count() > 0)
            {
                foreach (usp_PacientesRecResult reg in lista)
                {
                    txtPac_HC.Text = reg.Pac_HC;
                    txtPac_ApPat.Text = reg.Pac_ApPat;
                    txtPac_ApMat.Text = reg.Pac_ApMat;
                    txtPac_Direccion.Text = reg.Pac_Direccion;
                    txtPac_DirecFact.Text = reg.Pac_DirecFact;
                    txtPac_DirecResp.Text = reg.Pac_DirecResp;
                    txtPac_DocIdentNum.Text = reg.Pac_DocIdentNum;
                    txtPac_Email.Text = reg.Pac_Email;
                    txtPac_EmailResp.Text = reg.Pac_EmailResp;
                    txtPac_NombreFact.Text = reg.Pac_NombreFact;
                    txtPac_Nombres.Text = reg.Pac_Nombres;
                    txtPac_PersResp.Text = reg.Pac_PersResp;
                    txtPac_RUCFact.Text = reg.Pac_RUCFact;
                    txtPac_Telefonos.Text = reg.Pac_Telefonos;
                    txtPac_TelefResp.Text = reg.Pac_TelefResp;

                    lblPac_NomComp.Text = reg.Pac_NomComp;

                    dtpPac_FecNac.Value = Convert.ToDateTime(reg.Pac_FecNac);
                    
                    if (reg.Pac_Sexo == 'F')
                    {
                        rbtFemenino.Checked = true;
                    }

                    if (reg.Pac_Sexo == 'M')
                    {
                        rbtMasculino.Checked = true;
                    }

                    cboDistFacturacion.SelectedValue = reg.Pac_DistFactId;
                    cboDistPac.SelectedValue = reg.Pac_DistritoId;
                    cboDistResp.SelectedValue = reg.Pac_DistRespId;
                    cboLugNacimiento.SelectedValue = reg.Pac_LugNacId;
                    cboDocIdentidad.SelectedValue = reg.Pac_DocIdentId;
                    cboInstruccion.SelectedValue = reg.Pac_InstruccionId;
                    cboProfesion.SelectedValue = reg.Pac_ProfesionId;
                    cboGpoSang.Text = reg.Pac_GpoSang;

                    txtPac_HC.Enabled = false;

                    //Activar Controles de Datos
                    txtPac_ApMat.Enabled = true;
                    txtPac_ApPat.Enabled = true;
                    txtPac_Direccion.Enabled = true;
                    txtPac_DirecFact.Enabled = true;
                    txtPac_DirecResp.Enabled = true;
                    txtPac_DocIdentNum.Enabled = true;
                    txtPac_Email.Enabled = true;
                    txtPac_EmailResp.Enabled = true;
                    txtPac_NombreFact.Enabled = true;
                    txtPac_Nombres.Enabled = true;
                    txtPac_PersResp.Enabled = true;
                    txtPac_RUCFact.Enabled = true;
                    txtPac_Telefonos.Enabled = true;
                    txtPac_TelefResp.Enabled = true;

                    dtpPac_FecNac.Enabled = true;

                    rbtFemenino.Enabled = true;
                    rbtMasculino.Enabled = true;

                    cboDistFacturacion.Enabled = true;
                    cboDistPac.Enabled = true;
                    cboDistResp.Enabled = true;
                    cboDocIdentidad.Enabled = true;
                    cboInstruccion.Enabled = true;
                    cboLugNacimiento.Enabled = true;
                    cboProfesion.Enabled = true;
                    cboGpoSang.Enabled = true;


                    //Activar Controles de datos
                    btnNuevo.Enabled = false;
                    btnGrabar.Enabled = false;
                    btnActualizar.Enabled = true;
                    btnAnular.Enabled = true;
                    btnCerrar.Enabled = true;

                    lblCreado.Visible = true;
                    lblFecCrea.Visible = true;
                    lblModificado.Visible = true;
                    lblFecMod.Visible = true;

                    nuevo = false;
                    txtPac_ApPat.Focus();
                }
            }
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            if ((MessageBox.Show("¿Seguro que desea salir ?", "Aviso",
                     MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                     MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes))
            {
                this.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab("tbcRegistro");
            NuevoReg();
        }
    }
}
