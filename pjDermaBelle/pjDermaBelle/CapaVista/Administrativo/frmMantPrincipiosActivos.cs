﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pjDermaBelle
{
    public partial class frmMantPrincipiosActivos : Form
    {
        public frmMantPrincipiosActivos()
        {
            InitializeComponent();
        }

        ClaseDatos obj = new ClaseDatos();
        DermaBelleDataDataContext linq = new DermaBelleDataDataContext();
        PrincipiosActivos oPrincipiosActivos = new PrincipiosActivos();

        bool nuevo = true;

        private void Limpiar()
        {
            txtId.Clear();
            txtNombre.Clear();
            txtPresentacion.Clear();
            txtDosis.Clear();
            txtInteracciones.Clear();
            txtContraindicaciones.Clear();

            rbFarmacia.Checked = false;
            rbFormMagistral.Checked = false;
            rbElemBase.Checked = false;
            chkEstado.Checked = false;
            txtId.Enabled = false;
            txtNombre.Enabled = false;
            txtPresentacion.Enabled = false;
            txtDosis.Enabled = false;
            txtInteracciones.Enabled = false;
            txtContraindicaciones.Enabled = false;
            
            btnNuevo.Enabled = true;
            btnGrabar.Enabled = false;
            btnActualizar.Enabled = false;
            btnAnular.Enabled = false;
            btnCerrar.Enabled = true;

            lblCreado.Visible = false;
            lblFecCrea.Visible = false;
            lblModificado.Visible = false;
            lblFecMod.Visible = false;

            btnNuevo.Focus();
        }

        private void frmMantPrincipiosActivos_Load(object sender, EventArgs e)
        {
            dgvLista.DataSource = obj.PrincipiosActivosListar();
            Limpiar();
        }

        private void txtNombre_Leave(object sender, EventArgs e)
        {
            if (nuevo)
            {
                btnGrabar.Focus();
            }
            else
            { btnActualizar.Focus(); }
        }

        private void txtNombre_TextChanged(object sender, EventArgs e)
        {
            btnNuevo.Enabled = false;
            if (nuevo)
            {
                btnGrabar.Enabled = txtNombre.Text != string.Empty;
                btnAnular.Enabled = false;
            }
            else
            {
                btnGrabar.Enabled = false;
                btnActualizar.Enabled = true;
                btnAnular.Enabled = true;
            }
            btnCerrar.Enabled = true;
        }

        private void dgvLista_SelectionChanged(object sender, EventArgs e)
        {
            int indice = dgvLista.CurrentCell.RowIndex;
            int PrA_Id = int.Parse(dgvLista.Rows[indice].Cells[0].Value.ToString());

            List<usp_PrincipiosActivosRecResult> lista = new List<usp_PrincipiosActivosRecResult>();
            lista = obj.PrincipiosActivosDatos(PrA_Id);
            if (lista.Count() > 0)
            {
                foreach (usp_PrincipiosActivosRecResult reg in lista)
                {
                    txtId.Text = reg.PrA_Id.ToString();
                    txtNombre.Text = reg.PrA_Nombre;
                    if (reg.PrA_Tipo == "FA")
                    { rbFarmacia.Checked = true; }

                    if (reg.PrA_Tipo == "FM")
                    { rbFormMagistral.Checked = true; }

                    if (reg.PrA_Tipo == "EB")
                    { rbElemBase.Checked = true; }

                    lblFecCrea.Text = reg.FecCrea.ToString();
                    lblFecMod.Text = reg.FecModi.ToString();
                    chkEstado.Checked = reg.Estado;

                    if (chkEstado.Checked)
                    { btnAnular.Text = "A&nular"; }
                    else
                    { btnAnular.Text = "A&ctivar"; }

                    txtNombre.Enabled = true;
                    rbFarmacia.Enabled = true;
                    rbFormMagistral.Enabled = true;
                    rbElemBase.Enabled = true;
                    txtPresentacion.Enabled = true;
                    txtDosis.Enabled = true;
                    txtInteracciones.Enabled = true;
                    txtContraindicaciones.Enabled = true;

                    btnNuevo.Enabled = false;
                    btnGrabar.Enabled = false;
                    btnActualizar.Enabled = true;
                    btnAnular.Enabled = true;
                    btnCerrar.Enabled = true;

                    lblCreado.Visible = true;
                    lblFecCrea.Visible = true;
                    lblModificado.Visible = true;
                    lblFecMod.Visible = true;

                    nuevo = false;
                    txtNombre.Focus();
                }
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Limpiar();

            txtId.Enabled = false;
            txtNombre.Enabled = true;
            txtPresentacion.Enabled = true;
            txtDosis.Enabled = true;
            txtInteracciones.Enabled = true;
            txtContraindicaciones.Enabled = true;
            chkEstado.Checked = true;

            btnNuevo.Enabled = false;
            btnGrabar.Enabled = false;
            btnActualizar.Enabled = false;
            btnAnular.Enabled = false;
            btnCerrar.Enabled = true;

            txtNombre.Focus();
            nuevo = true;
        }

        private void btnGrabar_Click(object sender, EventArgs e)
        {
            oPrincipiosActivos.PrA_Nombre = txtNombre.Text;
            if (rbFarmacia.Checked)
            { oPrincipiosActivos.PrA_Tipo = "FA"; }
            if (rbFormMagistral.Checked)
            { oPrincipiosActivos.PrA_Tipo = "FM"; }
            if (rbElemBase.Checked)
            { oPrincipiosActivos.PrA_Tipo = "EB"; }
            oPrincipiosActivos.PrA_Presentacion = txtPresentacion.Text;
            oPrincipiosActivos.PrA_Dosis = txtDosis.Text;
            oPrincipiosActivos.PrA_Interacciones = txtInteracciones.Text;
            oPrincipiosActivos.PrA_Contraindicaciones = txtContraindicaciones.Text;
            oPrincipiosActivos.Estado = chkEstado.Checked;
            oPrincipiosActivos.UsuCrea = 1;
            oPrincipiosActivos.UsuModi = 1;
            obj.PrincipiosActivosAdicionar(oPrincipiosActivos);
            dgvLista.DataSource = obj.PrincipiosActivosListar();
            Limpiar();
            MessageBox.Show("Registro GRABADO satisfactoriamente", "AVISO");
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            oPrincipiosActivos.PrA_Id = Convert.ToInt16(txtId.Text);
            oPrincipiosActivos.PrA_Nombre = txtNombre.Text;
            if (rbFarmacia.Checked)
            { oPrincipiosActivos.PrA_Tipo = "FA"; }
            if (rbFormMagistral.Checked)
            { oPrincipiosActivos.PrA_Tipo = "FM"; }
            if (rbElemBase.Checked)
            { oPrincipiosActivos.PrA_Tipo = "EB"; }
            oPrincipiosActivos.PrA_Presentacion = txtPresentacion.Text;
            oPrincipiosActivos.PrA_Dosis = txtDosis.Text;
            oPrincipiosActivos.PrA_Interacciones = txtInteracciones.Text;
            oPrincipiosActivos.PrA_Contraindicaciones = txtContraindicaciones.Text;
            oPrincipiosActivos.UsuModi = 1;
            obj.PrincipiosActivosActualizar(oPrincipiosActivos);
            dgvLista.DataSource = obj.PrincipiosActivosListar();
            Limpiar();
            MessageBox.Show("Registro ACTUALIZADO satisfactoriamente", "AVISO");
        }

        private void btnAnular_Click(object sender, EventArgs e)
        {
            oPrincipiosActivos.PrA_Id = Convert.ToInt16(txtId.Text);
            //oPrincipiosActivos.PrA_Nombre = txtNombre.Text;
            oPrincipiosActivos.Estado = !chkEstado.Checked;
            oPrincipiosActivos.UsuModi = 1;
            obj.PrincipiosActivosAnular(oPrincipiosActivos);
            dgvLista.DataSource = obj.PrincipiosActivosListar();
            Limpiar();
            if (oPrincipiosActivos.Estado)
            { MessageBox.Show("Registro ACTIVADO satisfactoriamente", "AVISO"); }
            else
            { MessageBox.Show("Registro ANULADO satisfactoriamente", "AVISO"); }
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            if ((MessageBox.Show("¿Seguro que desea salir ?", "Aviso",
                     MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                     MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes))
            {
                this.Close();
            }
        }

        private void txtContraindicaciones_Leave(object sender, EventArgs e)
        {
        }

        private void txtContraindicaciones_TextChanged(object sender, EventArgs e)
        {
        }
    }
}
