﻿namespace pjDermaBelle
{
    partial class frmFichaMedica
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblTipoAtencion = new System.Windows.Forms.Label();
            this.lblProfesion = new System.Windows.Forms.Label();
            this.lblProcedencia = new System.Windows.Forms.Label();
            this.lblGradoInstruccion = new System.Windows.Forms.Label();
            this.lblLugarNacimiento = new System.Windows.Forms.Label();
            this.lblDocIdentidad = new System.Windows.Forms.Label();
            this.lblEdad = new System.Windows.Forms.Label();
            this.lblSexo = new System.Windows.Forms.Label();
            this.lblGrupoSanguineo = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lblHora = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblFecha = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblPaciente = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblNumHistoria = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtAlergias = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tbpMotivoConsulta = new System.Windows.Forms.TabPage();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tbpAnamnesis = new System.Windows.Forms.TabPage();
            this.groupBox40 = new System.Windows.Forms.GroupBox();
            this.txtFUltMenstruacion = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this.chkOtros = new System.Windows.Forms.CheckBox();
            this.chkDrogas = new System.Windows.Forms.CheckBox();
            this.chkTabaco = new System.Windows.Forms.CheckBox();
            this.chkAlcohol = new System.Windows.Forms.CheckBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtObservHabNoc = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.dgvSintomas = new System.Windows.Forms.DataGridView();
            this.Seleccion = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtMotivo = new System.Windows.Forms.TextBox();
            this.tbpAntecedentes = new System.Windows.Forms.TabPage();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.cboAlergias = new System.Windows.Forms.ComboBox();
            this.button25 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.label28 = new System.Windows.Forms.Label();
            this.dataGridView5 = new System.Windows.Forms.DataGridView();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.cboPatolFamiliar = new System.Windows.Forms.ComboBox();
            this.button23 = new System.Windows.Forms.Button();
            this.label27 = new System.Windows.Forms.Label();
            this.button22 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.cboFamiliar = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.dataGridView4 = new System.Windows.Forms.DataGridView();
            this.button21 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.cboCirugias = new System.Windows.Forms.ComboBox();
            this.label24 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.dgvAntecPatologia = new System.Windows.Forms.DataGridView();
            this.btnDelAntecPatologia = new System.Windows.Forms.Button();
            this.btnUpdAntecPatologia = new System.Windows.Forms.Button();
            this.btnAddAntecPatologia = new System.Windows.Forms.Button();
            this.txtPatologiaObserv = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.cboPatologias = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.tbpDiagnosticosAnteriores = new System.Windows.Forms.TabPage();
            this.dataGridView6 = new System.Windows.Forms.DataGridView();
            this.tpgExamFisico = new System.Windows.Forms.TabPage();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.cboEstHidratacion = new System.Windows.Forms.ComboBox();
            this.cboEstNutricional = new System.Windows.Forms.ComboBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.cboEstGeneral = new System.Windows.Forms.ComboBox();
            this.label36 = new System.Windows.Forms.Label();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.dataGridView16 = new System.Windows.Forms.DataGridView();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.chkOPersona = new System.Windows.Forms.CheckBox();
            this.chkOEspacio = new System.Windows.Forms.CheckBox();
            this.chkOTiempo = new System.Windows.Forms.CheckBox();
            this.chkLucido = new System.Windows.Forms.CheckBox();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.label38 = new System.Windows.Forms.Label();
            this.txtTalla = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.txtPeso = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.txtTemperatura = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.txtPulso = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.txtPAMinima = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txtPAMaxima = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.tbpAyudaDx = new System.Windows.Forms.TabPage();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.dataGridView7 = new System.Windows.Forms.DataGridView();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.button32 = new System.Windows.Forms.Button();
            this.button31 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.comboBox10 = new System.Windows.Forms.ComboBox();
            this.label44 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label43 = new System.Windows.Forms.Label();
            this.tbpPlanTrabajo = new System.Windows.Forms.TabPage();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.dataGridView9 = new System.Windows.Forms.DataGridView();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.dataGridView8 = new System.Windows.Forms.DataGridView();
            this.button27 = new System.Windows.Forms.Button();
            this.button26 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.comboBox11 = new System.Windows.Forms.ComboBox();
            this.label46 = new System.Windows.Forms.Label();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.tbpDiagnostico = new System.Windows.Forms.TabPage();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this.cboCIEDescripcion = new System.Windows.Forms.ComboBox();
            this.dataGridView10 = new System.Windows.Forms.DataGridView();
            this.groupBox22 = new System.Windows.Forms.GroupBox();
            this.groupBox28 = new System.Windows.Forms.GroupBox();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.groupBox27 = new System.Windows.Forms.GroupBox();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.button29 = new System.Windows.Forms.Button();
            this.button28 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.txtCIECod = new System.Windows.Forms.TextBox();
            this.label56 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.tbpPrescripcion = new System.Windows.Forms.TabPage();
            this.groupBox23 = new System.Windows.Forms.GroupBox();
            this.cboMedicamentos = new System.Windows.Forms.ComboBox();
            this.groupBox25 = new System.Windows.Forms.GroupBox();
            this.textBox30 = new System.Windows.Forms.TextBox();
            this.dataGridView13 = new System.Windows.Forms.DataGridView();
            this.label72 = new System.Windows.Forms.Label();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.btnDelFMag = new System.Windows.Forms.Button();
            this.btnUpdFMag = new System.Windows.Forms.Button();
            this.btnAddFMag = new System.Windows.Forms.Button();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.lblUnidad = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.lblSimbolo = new System.Windows.Forms.Label();
            this.comboBox14 = new System.Windows.Forms.ComboBox();
            this.label63 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.groupBox24 = new System.Windows.Forms.GroupBox();
            this.txtLaboratorio = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblClasifTerap = new System.Windows.Forms.Label();
            this.txtPrA_Contraindicaciones = new System.Windows.Forms.TextBox();
            this.txtPrA_Interacciones = new System.Windows.Forms.TextBox();
            this.txtPrA_Dosis = new System.Windows.Forms.TextBox();
            this.label71 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.lblPrA_Nombre = new System.Windows.Forms.Label();
            this.dgvPrescripcion = new System.Windows.Forms.DataGridView();
            this.btnDelPresc = new System.Windows.Forms.Button();
            this.btnUpdPresc = new System.Windows.Forms.Button();
            this.btnAddPresc = new System.Windows.Forms.Button();
            this.txtIndicaciones = new System.Windows.Forms.TextBox();
            this.label61 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.cboPrinActivoFA = new System.Windows.Forms.ComboBox();
            this.label59 = new System.Windows.Forms.Label();
            this.tbpConsultaAnterior = new System.Windows.Forms.TabPage();
            this.groupBox43 = new System.Windows.Forms.GroupBox();
            this.txtObservaciones = new System.Windows.Forms.TextBox();
            this.groupBox42 = new System.Windows.Forms.GroupBox();
            this.txtRecomendaciones = new System.Windows.Forms.TextBox();
            this.groupBox41 = new System.Windows.Forms.GroupBox();
            this.dtpProximaCita = new System.Windows.Forms.DateTimePicker();
            this.tbpComplementarios = new System.Windows.Forms.TabPage();
            this.groupBox34 = new System.Windows.Forms.GroupBox();
            this.dataGridView21 = new System.Windows.Forms.DataGridView();
            this.groupBox33 = new System.Windows.Forms.GroupBox();
            this.dataGridView20 = new System.Windows.Forms.DataGridView();
            this.groupBox32 = new System.Windows.Forms.GroupBox();
            this.dataGridView22 = new System.Windows.Forms.DataGridView();
            this.groupBox31 = new System.Windows.Forms.GroupBox();
            this.groupBox38 = new System.Windows.Forms.GroupBox();
            this.dataGridView19 = new System.Windows.Forms.DataGridView();
            this.groupBox37 = new System.Windows.Forms.GroupBox();
            this.dataGridView18 = new System.Windows.Forms.DataGridView();
            this.groupBox36 = new System.Windows.Forms.GroupBox();
            this.dataGridView17 = new System.Windows.Forms.DataGridView();
            this.groupBox30 = new System.Windows.Forms.GroupBox();
            this.groupBox35 = new System.Windows.Forms.GroupBox();
            this.groupBox39 = new System.Windows.Forms.GroupBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.label66 = new System.Windows.Forms.Label();
            this.textBox29 = new System.Windows.Forms.TextBox();
            this.textBox28 = new System.Windows.Forms.TextBox();
            this.groupBox29 = new System.Windows.Forms.GroupBox();
            this.textBox27 = new System.Windows.Forms.TextBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.btnCerrar = new System.Windows.Forms.Button();
            this.pbGrabar = new System.Windows.Forms.ProgressBar();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lblGuardando = new System.Windows.Forms.Label();
            this.btnHistorial = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tbpMotivoConsulta.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tbpAnamnesis.SuspendLayout();
            this.groupBox40.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox21.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSintomas)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.tbpAntecedentes.SuspendLayout();
            this.groupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).BeginInit();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAntecPatologia)).BeginInit();
            this.tbpDiagnosticosAnteriores.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView6)).BeginInit();
            this.tpgExamFisico.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView16)).BeginInit();
            this.groupBox12.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.tbpAyudaDx.SuspendLayout();
            this.groupBox16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView7)).BeginInit();
            this.groupBox15.SuspendLayout();
            this.tbpPlanTrabajo.SuspendLayout();
            this.groupBox19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView9)).BeginInit();
            this.groupBox18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView8)).BeginInit();
            this.groupBox17.SuspendLayout();
            this.tbpDiagnostico.SuspendLayout();
            this.groupBox20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView10)).BeginInit();
            this.groupBox22.SuspendLayout();
            this.groupBox28.SuspendLayout();
            this.groupBox27.SuspendLayout();
            this.tbpPrescripcion.SuspendLayout();
            this.groupBox23.SuspendLayout();
            this.groupBox25.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView13)).BeginInit();
            this.groupBox24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPrescripcion)).BeginInit();
            this.tbpConsultaAnterior.SuspendLayout();
            this.groupBox43.SuspendLayout();
            this.groupBox42.SuspendLayout();
            this.groupBox41.SuspendLayout();
            this.tbpComplementarios.SuspendLayout();
            this.groupBox34.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView21)).BeginInit();
            this.groupBox33.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView20)).BeginInit();
            this.groupBox32.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView22)).BeginInit();
            this.groupBox31.SuspendLayout();
            this.groupBox38.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView19)).BeginInit();
            this.groupBox37.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView18)).BeginInit();
            this.groupBox36.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView17)).BeginInit();
            this.groupBox30.SuspendLayout();
            this.groupBox35.SuspendLayout();
            this.groupBox39.SuspendLayout();
            this.groupBox29.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblTipoAtencion);
            this.groupBox1.Controls.Add(this.lblProfesion);
            this.groupBox1.Controls.Add(this.lblProcedencia);
            this.groupBox1.Controls.Add(this.lblGradoInstruccion);
            this.groupBox1.Controls.Add(this.lblLugarNacimiento);
            this.groupBox1.Controls.Add(this.lblDocIdentidad);
            this.groupBox1.Controls.Add(this.lblEdad);
            this.groupBox1.Controls.Add(this.lblSexo);
            this.groupBox1.Controls.Add(this.lblGrupoSanguineo);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.lblHora);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.lblFecha);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.lblPaciente);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.lblNumHistoria);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.shapeContainer1);
            this.groupBox1.Location = new System.Drawing.Point(8, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1356, 152);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos del Paciente";
            // 
            // lblTipoAtencion
            // 
            this.lblTipoAtencion.AutoSize = true;
            this.lblTipoAtencion.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoAtencion.ForeColor = System.Drawing.Color.MediumSeaGreen;
            this.lblTipoAtencion.Location = new System.Drawing.Point(1062, 16);
            this.lblTipoAtencion.Name = "lblTipoAtencion";
            this.lblTipoAtencion.Size = new System.Drawing.Size(134, 18);
            this.lblTipoAtencion.TabIndex = 41;
            this.lblTipoAtencion.Text = "Tipo de Atención";
            // 
            // lblProfesion
            // 
            this.lblProfesion.AutoSize = true;
            this.lblProfesion.ForeColor = System.Drawing.Color.Blue;
            this.lblProfesion.Location = new System.Drawing.Point(238, 129);
            this.lblProfesion.Name = "lblProfesion";
            this.lblProfesion.Size = new System.Drawing.Size(115, 13);
            this.lblProfesion.TabIndex = 40;
            this.lblProfesion.Text = "Profesión u Ocupación";
            // 
            // lblProcedencia
            // 
            this.lblProcedencia.AutoSize = true;
            this.lblProcedencia.ForeColor = System.Drawing.Color.Blue;
            this.lblProcedencia.Location = new System.Drawing.Point(1100, 129);
            this.lblProcedencia.Name = "lblProcedencia";
            this.lblProcedencia.Size = new System.Drawing.Size(67, 13);
            this.lblProcedencia.TabIndex = 40;
            this.lblProcedencia.Text = "Procedencia";
            // 
            // lblGradoInstruccion
            // 
            this.lblGradoInstruccion.AutoSize = true;
            this.lblGradoInstruccion.ForeColor = System.Drawing.Color.Blue;
            this.lblGradoInstruccion.Location = new System.Drawing.Point(238, 111);
            this.lblGradoInstruccion.Name = "lblGradoInstruccion";
            this.lblGradoInstruccion.Size = new System.Drawing.Size(106, 13);
            this.lblGradoInstruccion.TabIndex = 40;
            this.lblGradoInstruccion.Text = "Grado de Instrucción";
            // 
            // lblLugarNacimiento
            // 
            this.lblLugarNacimiento.AutoSize = true;
            this.lblLugarNacimiento.ForeColor = System.Drawing.Color.Blue;
            this.lblLugarNacimiento.Location = new System.Drawing.Point(1100, 111);
            this.lblLugarNacimiento.Name = "lblLugarNacimiento";
            this.lblLugarNacimiento.Size = new System.Drawing.Size(105, 13);
            this.lblLugarNacimiento.TabIndex = 40;
            this.lblLugarNacimiento.Text = "Lugar de Nacimiento";
            // 
            // lblDocIdentidad
            // 
            this.lblDocIdentidad.AutoSize = true;
            this.lblDocIdentidad.ForeColor = System.Drawing.Color.Blue;
            this.lblDocIdentidad.Location = new System.Drawing.Point(238, 92);
            this.lblDocIdentidad.Name = "lblDocIdentidad";
            this.lblDocIdentidad.Size = new System.Drawing.Size(124, 13);
            this.lblDocIdentidad.TabIndex = 40;
            this.lblDocIdentidad.Text = "Documento de Identidad";
            // 
            // lblEdad
            // 
            this.lblEdad.AutoSize = true;
            this.lblEdad.ForeColor = System.Drawing.Color.Blue;
            this.lblEdad.Location = new System.Drawing.Point(1100, 92);
            this.lblEdad.Name = "lblEdad";
            this.lblEdad.Size = new System.Drawing.Size(32, 13);
            this.lblEdad.TabIndex = 40;
            this.lblEdad.Text = "Edad";
            // 
            // lblSexo
            // 
            this.lblSexo.AutoSize = true;
            this.lblSexo.ForeColor = System.Drawing.Color.Blue;
            this.lblSexo.Location = new System.Drawing.Point(1100, 74);
            this.lblSexo.Name = "lblSexo";
            this.lblSexo.Size = new System.Drawing.Size(31, 13);
            this.lblSexo.TabIndex = 40;
            this.lblSexo.Text = "Sexo";
            // 
            // lblGrupoSanguineo
            // 
            this.lblGrupoSanguineo.AutoSize = true;
            this.lblGrupoSanguineo.ForeColor = System.Drawing.Color.Red;
            this.lblGrupoSanguineo.Location = new System.Drawing.Point(238, 74);
            this.lblGrupoSanguineo.Name = "lblGrupoSanguineo";
            this.lblGrupoSanguineo.Size = new System.Drawing.Size(92, 13);
            this.lblGrupoSanguineo.TabIndex = 40;
            this.lblGrupoSanguineo.Text = "Grupo Sanguíneo";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(1062, 93);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(32, 13);
            this.label9.TabIndex = 33;
            this.label9.Text = "Edad";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(108, 93);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(124, 13);
            this.label12.TabIndex = 26;
            this.label12.Text = "Documento de Identidad";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(140, 75);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(92, 13);
            this.label16.TabIndex = 39;
            this.label16.Text = "Grupo Sanguíneo";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(1027, 129);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(67, 13);
            this.label15.TabIndex = 37;
            this.label15.Text = "Procedencia";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(126, 111);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(106, 13);
            this.label10.TabIndex = 28;
            this.label10.Text = "Grado de Instrucción";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(117, 129);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(115, 13);
            this.label11.TabIndex = 30;
            this.label11.Text = "Profesión u Ocupación";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(989, 111);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(105, 13);
            this.label14.TabIndex = 33;
            this.label14.Text = "Lugar de Nacimiento";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(1063, 75);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(31, 13);
            this.label13.TabIndex = 24;
            this.label13.Text = "Sexo";
            // 
            // lblHora
            // 
            this.lblHora.AutoSize = true;
            this.lblHora.Location = new System.Drawing.Point(1272, 32);
            this.lblHora.Name = "lblHora";
            this.lblHora.Size = new System.Drawing.Size(51, 13);
            this.lblHora.TabIndex = 8;
            this.lblHora.Text = "hh:mm:ss";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(1238, 32);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(30, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Hora";
            // 
            // lblFecha
            // 
            this.lblFecha.AutoSize = true;
            this.lblFecha.Location = new System.Drawing.Point(1272, 16);
            this.lblFecha.Name = "lblFecha";
            this.lblFecha.Size = new System.Drawing.Size(69, 13);
            this.lblFecha.TabIndex = 6;
            this.lblFecha.Text = "dd/mm/aaaa";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(1231, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Fecha";
            // 
            // lblPaciente
            // 
            this.lblPaciente.AutoSize = true;
            this.lblPaciente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPaciente.ForeColor = System.Drawing.Color.Blue;
            this.lblPaciente.Location = new System.Drawing.Point(239, 45);
            this.lblPaciente.Name = "lblPaciente";
            this.lblPaciente.Size = new System.Drawing.Size(448, 16);
            this.lblPaciente.TabIndex = 4;
            this.lblPaciente.Text = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(171, 45);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 16);
            this.label4.TabIndex = 3;
            this.label4.Text = "Paciente";
            // 
            // lblNumHistoria
            // 
            this.lblNumHistoria.AutoSize = true;
            this.lblNumHistoria.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumHistoria.ForeColor = System.Drawing.Color.Blue;
            this.lblNumHistoria.Location = new System.Drawing.Point(239, 29);
            this.lblNumHistoria.Name = "lblNumHistoria";
            this.lblNumHistoria.Size = new System.Drawing.Size(78, 16);
            this.lblNumHistoria.TabIndex = 2;
            this.lblNumHistoria.Text = "9999999999";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(151, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "H. Clinica N°";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(10, 20);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(89, 122);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(3, 16);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(1350, 133);
            this.shapeContainer1.TabIndex = 42;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape1
            // 
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 106;
            this.lineShape1.X2 = 1340;
            this.lineShape1.Y1 = 50;
            this.lineShape1.Y2 = 50;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox3.Controls.Add(this.txtAlergias);
            this.groupBox3.Location = new System.Drawing.Point(8, 156);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1356, 40);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = " Alergias";
            // 
            // txtAlergias
            // 
            this.txtAlergias.BackColor = System.Drawing.Color.DarkRed;
            this.txtAlergias.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAlergias.ForeColor = System.Drawing.Color.White;
            this.txtAlergias.Location = new System.Drawing.Point(7, 14);
            this.txtAlergias.Name = "txtAlergias";
            this.txtAlergias.ReadOnly = true;
            this.txtAlergias.Size = new System.Drawing.Size(1343, 20);
            this.txtAlergias.TabIndex = 0;
            this.txtAlergias.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tabControl1
            // 
            this.tabControl1.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.tabControl1.Controls.Add(this.tbpMotivoConsulta);
            this.tabControl1.Controls.Add(this.tpgExamFisico);
            this.tabControl1.Controls.Add(this.tbpAyudaDx);
            this.tabControl1.Controls.Add(this.tbpPlanTrabajo);
            this.tabControl1.Controls.Add(this.tbpDiagnostico);
            this.tabControl1.Controls.Add(this.tbpPrescripcion);
            this.tabControl1.Controls.Add(this.tbpConsultaAnterior);
            this.tabControl1.Controls.Add(this.tbpComplementarios);
            this.tabControl1.HotTrack = true;
            this.tabControl1.Location = new System.Drawing.Point(9, 202);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1355, 464);
            this.tabControl1.TabIndex = 3;
            // 
            // tbpMotivoConsulta
            // 
            this.tbpMotivoConsulta.Controls.Add(this.tabControl2);
            this.tbpMotivoConsulta.Location = new System.Drawing.Point(61, 4);
            this.tbpMotivoConsulta.Name = "tbpMotivoConsulta";
            this.tbpMotivoConsulta.Padding = new System.Windows.Forms.Padding(3);
            this.tbpMotivoConsulta.Size = new System.Drawing.Size(1290, 456);
            this.tbpMotivoConsulta.TabIndex = 0;
            this.tbpMotivoConsulta.Text = "Motivo de la Consulta";
            this.tbpMotivoConsulta.UseVisualStyleBackColor = true;
            this.tbpMotivoConsulta.Leave += new System.EventHandler(this.tabPage1_Leave);
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tbpAnamnesis);
            this.tabControl2.Controls.Add(this.tbpAntecedentes);
            this.tabControl2.Controls.Add(this.tbpDiagnosticosAnteriores);
            this.tabControl2.Location = new System.Drawing.Point(7, 7);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(934, 442);
            this.tabControl2.TabIndex = 0;
            // 
            // tbpAnamnesis
            // 
            this.tbpAnamnesis.Controls.Add(this.groupBox40);
            this.tbpAnamnesis.Controls.Add(this.groupBox6);
            this.tbpAnamnesis.Controls.Add(this.groupBox4);
            this.tbpAnamnesis.Controls.Add(this.groupBox2);
            this.tbpAnamnesis.Location = new System.Drawing.Point(4, 22);
            this.tbpAnamnesis.Name = "tbpAnamnesis";
            this.tbpAnamnesis.Padding = new System.Windows.Forms.Padding(3);
            this.tbpAnamnesis.Size = new System.Drawing.Size(926, 416);
            this.tbpAnamnesis.TabIndex = 0;
            this.tbpAnamnesis.Text = "Anamnesis";
            this.tbpAnamnesis.UseVisualStyleBackColor = true;
            this.tbpAnamnesis.Leave += new System.EventHandler(this.tabPage8_Leave);
            // 
            // groupBox40
            // 
            this.groupBox40.Controls.Add(this.txtFUltMenstruacion);
            this.groupBox40.Controls.Add(this.label2);
            this.groupBox40.Location = new System.Drawing.Point(7, 291);
            this.groupBox40.Name = "groupBox40";
            this.groupBox40.Size = new System.Drawing.Size(433, 119);
            this.groupBox40.TabIndex = 5;
            this.groupBox40.TabStop = false;
            this.groupBox40.Text = "Otros datos de Interés";
            // 
            // txtFUltMenstruacion
            // 
            this.txtFUltMenstruacion.Location = new System.Drawing.Point(163, 21);
            this.txtFUltMenstruacion.Name = "txtFUltMenstruacion";
            this.txtFUltMenstruacion.Size = new System.Drawing.Size(100, 20);
            this.txtFUltMenstruacion.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(151, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Fecha de Última Menstruación";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.groupBox21);
            this.groupBox6.Controls.Add(this.label18);
            this.groupBox6.Controls.Add(this.txtObservHabNoc);
            this.groupBox6.Location = new System.Drawing.Point(7, 153);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(433, 132);
            this.groupBox6.TabIndex = 3;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Hábitos Nocivos";
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this.chkOtros);
            this.groupBox21.Controls.Add(this.chkDrogas);
            this.groupBox21.Controls.Add(this.chkTabaco);
            this.groupBox21.Controls.Add(this.chkAlcohol);
            this.groupBox21.Location = new System.Drawing.Point(6, 13);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(421, 27);
            this.groupBox21.TabIndex = 10;
            this.groupBox21.TabStop = false;
            // 
            // chkOtros
            // 
            this.chkOtros.AutoSize = true;
            this.chkOtros.Location = new System.Drawing.Point(360, 8);
            this.chkOtros.Name = "chkOtros";
            this.chkOtros.Size = new System.Drawing.Size(51, 17);
            this.chkOtros.TabIndex = 0;
            this.chkOtros.Text = "Otros";
            this.chkOtros.UseVisualStyleBackColor = true;
            // 
            // chkDrogas
            // 
            this.chkDrogas.AutoSize = true;
            this.chkDrogas.Location = new System.Drawing.Point(243, 8);
            this.chkDrogas.Name = "chkDrogas";
            this.chkDrogas.Size = new System.Drawing.Size(60, 17);
            this.chkDrogas.TabIndex = 0;
            this.chkDrogas.Text = "Drogas";
            this.chkDrogas.UseVisualStyleBackColor = true;
            // 
            // chkTabaco
            // 
            this.chkTabaco.AutoSize = true;
            this.chkTabaco.Location = new System.Drawing.Point(123, 8);
            this.chkTabaco.Name = "chkTabaco";
            this.chkTabaco.Size = new System.Drawing.Size(63, 17);
            this.chkTabaco.TabIndex = 0;
            this.chkTabaco.Text = "Tabaco";
            this.chkTabaco.UseVisualStyleBackColor = true;
            // 
            // chkAlcohol
            // 
            this.chkAlcohol.AutoSize = true;
            this.chkAlcohol.Location = new System.Drawing.Point(5, 8);
            this.chkAlcohol.Name = "chkAlcohol";
            this.chkAlcohol.Size = new System.Drawing.Size(61, 17);
            this.chkAlcohol.TabIndex = 0;
            this.chkAlcohol.Text = "Alcohol";
            this.chkAlcohol.UseVisualStyleBackColor = true;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 42);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(59, 13);
            this.label18.TabIndex = 9;
            this.label18.Text = "Especificar";
            // 
            // txtObservHabNoc
            // 
            this.txtObservHabNoc.Location = new System.Drawing.Point(5, 58);
            this.txtObservHabNoc.Multiline = true;
            this.txtObservHabNoc.Name = "txtObservHabNoc";
            this.txtObservHabNoc.Size = new System.Drawing.Size(421, 66);
            this.txtObservHabNoc.TabIndex = 8;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.dgvSintomas);
            this.groupBox4.Location = new System.Drawing.Point(448, 7);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(472, 403);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Síntomas y Signos Referidos";
            // 
            // dgvSintomas
            // 
            this.dgvSintomas.AllowUserToAddRows = false;
            this.dgvSintomas.AllowUserToDeleteRows = false;
            this.dgvSintomas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSintomas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Seleccion});
            this.dgvSintomas.Location = new System.Drawing.Point(6, 20);
            this.dgvSintomas.Name = "dgvSintomas";
            this.dgvSintomas.Size = new System.Drawing.Size(460, 377);
            this.dgvSintomas.TabIndex = 0;
            this.dgvSintomas.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSintomas_CellContentClick);
            this.dgvSintomas.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvSintomas_CurrentCellDirtyStateChanged);
            // 
            // Seleccion
            // 
            this.Seleccion.HeaderText = "";
            this.Seleccion.Name = "Seleccion";
            this.Seleccion.Width = 30;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtMotivo);
            this.groupBox2.Location = new System.Drawing.Point(7, 7);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(433, 140);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Motivo de la Consulta o Evolución del Paciente";
            // 
            // txtMotivo
            // 
            this.txtMotivo.Location = new System.Drawing.Point(6, 19);
            this.txtMotivo.Multiline = true;
            this.txtMotivo.Name = "txtMotivo";
            this.txtMotivo.Size = new System.Drawing.Size(421, 111);
            this.txtMotivo.TabIndex = 5;
            // 
            // tbpAntecedentes
            // 
            this.tbpAntecedentes.Controls.Add(this.groupBox10);
            this.tbpAntecedentes.Controls.Add(this.groupBox8);
            this.tbpAntecedentes.Controls.Add(this.groupBox9);
            this.tbpAntecedentes.Controls.Add(this.groupBox7);
            this.tbpAntecedentes.Location = new System.Drawing.Point(4, 22);
            this.tbpAntecedentes.Name = "tbpAntecedentes";
            this.tbpAntecedentes.Padding = new System.Windows.Forms.Padding(3);
            this.tbpAntecedentes.Size = new System.Drawing.Size(926, 416);
            this.tbpAntecedentes.TabIndex = 1;
            this.tbpAntecedentes.Text = "Antecedentes";
            this.tbpAntecedentes.UseVisualStyleBackColor = true;
            this.tbpAntecedentes.Enter += new System.EventHandler(this.tbpAntecedentes_Enter);
            this.tbpAntecedentes.Leave += new System.EventHandler(this.tabPage9_Leave);
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.cboAlergias);
            this.groupBox10.Controls.Add(this.button25);
            this.groupBox10.Controls.Add(this.button24);
            this.groupBox10.Controls.Add(this.button4);
            this.groupBox10.Controls.Add(this.label28);
            this.groupBox10.Controls.Add(this.dataGridView5);
            this.groupBox10.Controls.Add(this.textBox7);
            this.groupBox10.Controls.Add(this.label25);
            this.groupBox10.Location = new System.Drawing.Point(467, 200);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(452, 210);
            this.groupBox10.TabIndex = 0;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Alergias";
            // 
            // cboAlergias
            // 
            this.cboAlergias.FormattingEnabled = true;
            this.cboAlergias.Location = new System.Drawing.Point(91, 17);
            this.cboAlergias.Name = "cboAlergias";
            this.cboAlergias.Size = new System.Drawing.Size(354, 21);
            this.cboAlergias.TabIndex = 11;
            // 
            // button25
            // 
            this.button25.Location = new System.Drawing.Point(370, 67);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(75, 23);
            this.button25.TabIndex = 4;
            this.button25.Text = "Eliminar";
            this.button25.UseVisualStyleBackColor = true;
            // 
            // button24
            // 
            this.button24.Location = new System.Drawing.Point(295, 67);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(75, 23);
            this.button24.TabIndex = 4;
            this.button24.Text = "Modificar";
            this.button24.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(220, 67);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 4;
            this.button4.Text = "Agregar";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(10, 21);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(39, 13);
            this.label28.TabIndex = 8;
            this.label28.Text = "Alergia";
            // 
            // dataGridView5
            // 
            this.dataGridView5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView5.Location = new System.Drawing.Point(6, 96);
            this.dataGridView5.Name = "dataGridView5";
            this.dataGridView5.Size = new System.Drawing.Size(440, 107);
            this.dataGridView5.TabIndex = 5;
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(90, 42);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(355, 20);
            this.textBox7.TabIndex = 3;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(10, 46);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(78, 13);
            this.label25.TabIndex = 2;
            this.label25.Text = "Observaciones";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.cboPatolFamiliar);
            this.groupBox8.Controls.Add(this.button23);
            this.groupBox8.Controls.Add(this.label27);
            this.groupBox8.Controls.Add(this.button22);
            this.groupBox8.Controls.Add(this.button2);
            this.groupBox8.Controls.Add(this.dataGridView3);
            this.groupBox8.Controls.Add(this.textBox5);
            this.groupBox8.Controls.Add(this.label21);
            this.groupBox8.Controls.Add(this.cboFamiliar);
            this.groupBox8.Controls.Add(this.label22);
            this.groupBox8.Location = new System.Drawing.Point(8, 200);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(452, 210);
            this.groupBox8.TabIndex = 0;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Familiares";
            // 
            // cboPatolFamiliar
            // 
            this.cboPatolFamiliar.FormattingEnabled = true;
            this.cboPatolFamiliar.Location = new System.Drawing.Point(272, 15);
            this.cboPatolFamiliar.Name = "cboPatolFamiliar";
            this.cboPatolFamiliar.Size = new System.Drawing.Size(174, 21);
            this.cboPatolFamiliar.TabIndex = 7;
            // 
            // button23
            // 
            this.button23.Location = new System.Drawing.Point(371, 67);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(75, 23);
            this.button23.TabIndex = 4;
            this.button23.Text = "Eliminar";
            this.button23.UseVisualStyleBackColor = true;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(216, 21);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(53, 13);
            this.label27.TabIndex = 6;
            this.label27.Text = "Patología";
            // 
            // button22
            // 
            this.button22.Location = new System.Drawing.Point(296, 67);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(75, 23);
            this.button22.TabIndex = 4;
            this.button22.Text = "Modificar";
            this.button22.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(221, 67);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "Agregar";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // dataGridView3
            // 
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Location = new System.Drawing.Point(6, 96);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.Size = new System.Drawing.Size(440, 107);
            this.dataGridView3.TabIndex = 5;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(90, 42);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(356, 20);
            this.textBox5.TabIndex = 3;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(10, 46);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(78, 13);
            this.label21.TabIndex = 2;
            this.label21.Text = "Observaciones";
            // 
            // cboFamiliar
            // 
            this.cboFamiliar.FormattingEnabled = true;
            this.cboFamiliar.Location = new System.Drawing.Point(90, 17);
            this.cboFamiliar.Name = "cboFamiliar";
            this.cboFamiliar.Size = new System.Drawing.Size(106, 21);
            this.cboFamiliar.TabIndex = 1;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(10, 21);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(42, 13);
            this.label22.TabIndex = 0;
            this.label22.Text = "Familiar";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.dataGridView4);
            this.groupBox9.Controls.Add(this.button21);
            this.groupBox9.Controls.Add(this.button18);
            this.groupBox9.Controls.Add(this.textBox6);
            this.groupBox9.Controls.Add(this.button3);
            this.groupBox9.Controls.Add(this.label23);
            this.groupBox9.Controls.Add(this.cboCirugias);
            this.groupBox9.Controls.Add(this.label24);
            this.groupBox9.Location = new System.Drawing.Point(467, 8);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(452, 185);
            this.groupBox9.TabIndex = 0;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Quirúrgicos";
            // 
            // dataGridView4
            // 
            this.dataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView4.Location = new System.Drawing.Point(6, 97);
            this.dataGridView4.Name = "dataGridView4";
            this.dataGridView4.Size = new System.Drawing.Size(440, 81);
            this.dataGridView4.TabIndex = 5;
            // 
            // button21
            // 
            this.button21.Location = new System.Drawing.Point(370, 68);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(75, 23);
            this.button21.TabIndex = 4;
            this.button21.Text = "Eliminar";
            this.button21.UseVisualStyleBackColor = true;
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(295, 68);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(75, 23);
            this.button18.TabIndex = 4;
            this.button18.Text = "Modificar";
            this.button18.UseVisualStyleBackColor = true;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(90, 42);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(355, 20);
            this.textBox6.TabIndex = 3;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(220, 68);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 4;
            this.button3.Text = "Agregar";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(10, 46);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(78, 13);
            this.label23.TabIndex = 2;
            this.label23.Text = "Observaciones";
            // 
            // cboCirugias
            // 
            this.cboCirugias.FormattingEnabled = true;
            this.cboCirugias.Location = new System.Drawing.Point(90, 17);
            this.cboCirugias.Name = "cboCirugias";
            this.cboCirugias.Size = new System.Drawing.Size(355, 21);
            this.cboCirugias.TabIndex = 1;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(7, 20);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(46, 13);
            this.label24.TabIndex = 0;
            this.label24.Text = "Cirugías";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.dgvAntecPatologia);
            this.groupBox7.Controls.Add(this.btnDelAntecPatologia);
            this.groupBox7.Controls.Add(this.btnUpdAntecPatologia);
            this.groupBox7.Controls.Add(this.btnAddAntecPatologia);
            this.groupBox7.Controls.Add(this.txtPatologiaObserv);
            this.groupBox7.Controls.Add(this.label20);
            this.groupBox7.Controls.Add(this.cboPatologias);
            this.groupBox7.Controls.Add(this.label19);
            this.groupBox7.Location = new System.Drawing.Point(8, 8);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(452, 185);
            this.groupBox7.TabIndex = 0;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Patológicos";
            // 
            // dgvAntecPatologia
            // 
            this.dgvAntecPatologia.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAntecPatologia.Location = new System.Drawing.Point(6, 97);
            this.dgvAntecPatologia.Name = "dgvAntecPatologia";
            this.dgvAntecPatologia.ReadOnly = true;
            this.dgvAntecPatologia.Size = new System.Drawing.Size(440, 81);
            this.dgvAntecPatologia.TabIndex = 5;
            this.dgvAntecPatologia.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAntecPatologia_CellEnter);
            this.dgvAntecPatologia.SelectionChanged += new System.EventHandler(this.dgvAntecPatologia_SelectionChanged);
            // 
            // btnDelAntecPatologia
            // 
            this.btnDelAntecPatologia.Location = new System.Drawing.Point(371, 68);
            this.btnDelAntecPatologia.Name = "btnDelAntecPatologia";
            this.btnDelAntecPatologia.Size = new System.Drawing.Size(75, 23);
            this.btnDelAntecPatologia.TabIndex = 4;
            this.btnDelAntecPatologia.Text = "Eliminar";
            this.btnDelAntecPatologia.UseVisualStyleBackColor = true;
            this.btnDelAntecPatologia.Click += new System.EventHandler(this.btnDelAntecPatologia_Click);
            // 
            // btnUpdAntecPatologia
            // 
            this.btnUpdAntecPatologia.Location = new System.Drawing.Point(296, 68);
            this.btnUpdAntecPatologia.Name = "btnUpdAntecPatologia";
            this.btnUpdAntecPatologia.Size = new System.Drawing.Size(75, 23);
            this.btnUpdAntecPatologia.TabIndex = 4;
            this.btnUpdAntecPatologia.Text = "Modificar";
            this.btnUpdAntecPatologia.UseVisualStyleBackColor = true;
            this.btnUpdAntecPatologia.Click += new System.EventHandler(this.btnUpdAntecPatologia_Click);
            // 
            // btnAddAntecPatologia
            // 
            this.btnAddAntecPatologia.Location = new System.Drawing.Point(221, 68);
            this.btnAddAntecPatologia.Name = "btnAddAntecPatologia";
            this.btnAddAntecPatologia.Size = new System.Drawing.Size(75, 23);
            this.btnAddAntecPatologia.TabIndex = 4;
            this.btnAddAntecPatologia.Text = "Agregar";
            this.btnAddAntecPatologia.UseVisualStyleBackColor = true;
            this.btnAddAntecPatologia.Click += new System.EventHandler(this.btnAddAntecPatologia_Click);
            // 
            // txtPatologiaObserv
            // 
            this.txtPatologiaObserv.Location = new System.Drawing.Point(90, 42);
            this.txtPatologiaObserv.Name = "txtPatologiaObserv";
            this.txtPatologiaObserv.Size = new System.Drawing.Size(356, 20);
            this.txtPatologiaObserv.TabIndex = 3;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(10, 46);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(78, 13);
            this.label20.TabIndex = 2;
            this.label20.Text = "Observaciones";
            // 
            // cboPatologias
            // 
            this.cboPatologias.FormattingEnabled = true;
            this.cboPatologias.Location = new System.Drawing.Point(90, 17);
            this.cboPatologias.Name = "cboPatologias";
            this.cboPatologias.Size = new System.Drawing.Size(356, 21);
            this.cboPatologias.TabIndex = 1;
            this.cboPatologias.Enter += new System.EventHandler(this.cboPatologias_Enter);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(7, 20);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(53, 13);
            this.label19.TabIndex = 0;
            this.label19.Text = "Patología";
            // 
            // tbpDiagnosticosAnteriores
            // 
            this.tbpDiagnosticosAnteriores.Controls.Add(this.dataGridView6);
            this.tbpDiagnosticosAnteriores.Location = new System.Drawing.Point(4, 22);
            this.tbpDiagnosticosAnteriores.Name = "tbpDiagnosticosAnteriores";
            this.tbpDiagnosticosAnteriores.Padding = new System.Windows.Forms.Padding(3);
            this.tbpDiagnosticosAnteriores.Size = new System.Drawing.Size(926, 416);
            this.tbpDiagnosticosAnteriores.TabIndex = 2;
            this.tbpDiagnosticosAnteriores.Text = "Diagnósticos Anteriores";
            this.tbpDiagnosticosAnteriores.UseVisualStyleBackColor = true;
            this.tbpDiagnosticosAnteriores.Leave += new System.EventHandler(this.tabPage10_Leave);
            // 
            // dataGridView6
            // 
            this.dataGridView6.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView6.Location = new System.Drawing.Point(7, 7);
            this.dataGridView6.Name = "dataGridView6";
            this.dataGridView6.Size = new System.Drawing.Size(912, 403);
            this.dataGridView6.TabIndex = 0;
            // 
            // tpgExamFisico
            // 
            this.tpgExamFisico.Controls.Add(this.groupBox14);
            this.tpgExamFisico.Controls.Add(this.groupBox13);
            this.tpgExamFisico.Controls.Add(this.groupBox12);
            this.tpgExamFisico.Controls.Add(this.groupBox11);
            this.tpgExamFisico.Location = new System.Drawing.Point(61, 4);
            this.tpgExamFisico.Name = "tpgExamFisico";
            this.tpgExamFisico.Padding = new System.Windows.Forms.Padding(3);
            this.tpgExamFisico.Size = new System.Drawing.Size(1290, 456);
            this.tpgExamFisico.TabIndex = 1;
            this.tpgExamFisico.Text = "Examen Físico";
            this.tpgExamFisico.UseVisualStyleBackColor = true;
            this.tpgExamFisico.Leave += new System.EventHandler(this.tabPage2_Leave);
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.cboEstHidratacion);
            this.groupBox14.Controls.Add(this.cboEstNutricional);
            this.groupBox14.Controls.Add(this.label40);
            this.groupBox14.Controls.Add(this.label37);
            this.groupBox14.Controls.Add(this.cboEstGeneral);
            this.groupBox14.Controls.Add(this.label36);
            this.groupBox14.Location = new System.Drawing.Point(6, 90);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(935, 59);
            this.groupBox14.TabIndex = 2;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Estado del Paciente";
            // 
            // cboEstHidratacion
            // 
            this.cboEstHidratacion.FormattingEnabled = true;
            this.cboEstHidratacion.Items.AddRange(new object[] {
            "Bueno",
            "Malo",
            "Regular"});
            this.cboEstHidratacion.Location = new System.Drawing.Point(809, 22);
            this.cboEstHidratacion.Name = "cboEstHidratacion";
            this.cboEstHidratacion.Size = new System.Drawing.Size(121, 21);
            this.cboEstHidratacion.TabIndex = 1;
            // 
            // cboEstNutricional
            // 
            this.cboEstNutricional.FormattingEnabled = true;
            this.cboEstNutricional.Items.AddRange(new object[] {
            "Bueno",
            "Malo",
            "Regular"});
            this.cboEstNutricional.Location = new System.Drawing.Point(466, 22);
            this.cboEstNutricional.Name = "cboEstNutricional";
            this.cboEstNutricional.Size = new System.Drawing.Size(121, 21);
            this.cboEstNutricional.TabIndex = 1;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(693, 26);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(112, 13);
            this.label40.TabIndex = 0;
            this.label40.Text = "Estado de Hidratación";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(369, 26);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(93, 13);
            this.label37.TabIndex = 0;
            this.label37.Text = "Estado Nutricional";
            // 
            // cboEstGeneral
            // 
            this.cboEstGeneral.FormattingEnabled = true;
            this.cboEstGeneral.Items.AddRange(new object[] {
            "Bueno",
            "Malo",
            "Regular"});
            this.cboEstGeneral.Location = new System.Drawing.Point(92, 22);
            this.cboEstGeneral.Name = "cboEstGeneral";
            this.cboEstGeneral.Size = new System.Drawing.Size(121, 21);
            this.cboEstGeneral.TabIndex = 1;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(7, 26);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(80, 13);
            this.label36.TabIndex = 0;
            this.label36.Text = "Estado General";
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.dataGridView16);
            this.groupBox13.Location = new System.Drawing.Point(6, 157);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(936, 293);
            this.groupBox13.TabIndex = 2;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Examen Preferencial";
            // 
            // dataGridView16
            // 
            this.dataGridView16.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView16.Location = new System.Drawing.Point(6, 19);
            this.dataGridView16.Name = "dataGridView16";
            this.dataGridView16.Size = new System.Drawing.Size(924, 268);
            this.dataGridView16.TabIndex = 0;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.chkOPersona);
            this.groupBox12.Controls.Add(this.chkOEspacio);
            this.groupBox12.Controls.Add(this.chkOTiempo);
            this.groupBox12.Controls.Add(this.chkLucido);
            this.groupBox12.Location = new System.Drawing.Point(472, 7);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(469, 75);
            this.groupBox12.TabIndex = 1;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "LOTEP";
            // 
            // chkOPersona
            // 
            this.chkOPersona.AutoSize = true;
            this.chkOPersona.Location = new System.Drawing.Point(343, 30);
            this.chkOPersona.Name = "chkOPersona";
            this.chkOPersona.Size = new System.Drawing.Size(114, 17);
            this.chkOPersona.TabIndex = 1;
            this.chkOPersona.Text = "Orientado Persona";
            this.chkOPersona.UseVisualStyleBackColor = true;
            // 
            // chkOEspacio
            // 
            this.chkOEspacio.AutoSize = true;
            this.chkOEspacio.Location = new System.Drawing.Point(213, 30);
            this.chkOEspacio.Name = "chkOEspacio";
            this.chkOEspacio.Size = new System.Drawing.Size(113, 17);
            this.chkOEspacio.TabIndex = 1;
            this.chkOEspacio.Text = "Orientado Espacio";
            this.chkOEspacio.UseVisualStyleBackColor = true;
            // 
            // chkOTiempo
            // 
            this.chkOTiempo.AutoSize = true;
            this.chkOTiempo.Location = new System.Drawing.Point(86, 30);
            this.chkOTiempo.Name = "chkOTiempo";
            this.chkOTiempo.Size = new System.Drawing.Size(110, 17);
            this.chkOTiempo.TabIndex = 1;
            this.chkOTiempo.Text = "Orientado Tiempo";
            this.chkOTiempo.UseVisualStyleBackColor = true;
            // 
            // chkLucido
            // 
            this.chkLucido.AutoSize = true;
            this.chkLucido.Location = new System.Drawing.Point(11, 30);
            this.chkLucido.Name = "chkLucido";
            this.chkLucido.Size = new System.Drawing.Size(58, 17);
            this.chkLucido.TabIndex = 1;
            this.chkLucido.Text = "Lucido";
            this.chkLucido.UseVisualStyleBackColor = true;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.label38);
            this.groupBox11.Controls.Add(this.txtTalla);
            this.groupBox11.Controls.Add(this.label39);
            this.groupBox11.Controls.Add(this.label41);
            this.groupBox11.Controls.Add(this.txtPeso);
            this.groupBox11.Controls.Add(this.label42);
            this.groupBox11.Controls.Add(this.label33);
            this.groupBox11.Controls.Add(this.txtTemperatura);
            this.groupBox11.Controls.Add(this.label35);
            this.groupBox11.Controls.Add(this.label32);
            this.groupBox11.Controls.Add(this.txtPulso);
            this.groupBox11.Controls.Add(this.label34);
            this.groupBox11.Controls.Add(this.label31);
            this.groupBox11.Controls.Add(this.txtPAMinima);
            this.groupBox11.Controls.Add(this.label30);
            this.groupBox11.Controls.Add(this.txtPAMaxima);
            this.groupBox11.Controls.Add(this.label29);
            this.groupBox11.Location = new System.Drawing.Point(7, 7);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(460, 75);
            this.groupBox11.TabIndex = 0;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Funciones Vitales";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(284, 50);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(29, 13);
            this.label38.TabIndex = 20;
            this.label38.Text = "cmts";
            // 
            // txtTalla
            // 
            this.txtTalla.Location = new System.Drawing.Point(239, 46);
            this.txtTalla.Name = "txtTalla";
            this.txtTalla.Size = new System.Drawing.Size(39, 20);
            this.txtTalla.TabIndex = 19;
            this.txtTalla.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(195, 50);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(35, 13);
            this.label39.TabIndex = 18;
            this.label39.Text = "Talla";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(91, 50);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(20, 13);
            this.label41.TabIndex = 15;
            this.label41.Text = "Kg";
            // 
            // txtPeso
            // 
            this.txtPeso.Location = new System.Drawing.Point(52, 46);
            this.txtPeso.Name = "txtPeso";
            this.txtPeso.Size = new System.Drawing.Size(39, 20);
            this.txtPeso.TabIndex = 14;
            this.txtPeso.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(8, 50);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(35, 13);
            this.label42.TabIndex = 13;
            this.label42.Text = "Peso";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(438, 19);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(18, 13);
            this.label33.TabIndex = 12;
            this.label33.Text = "C°";
            // 
            // txtTemperatura
            // 
            this.txtTemperatura.Location = new System.Drawing.Point(394, 15);
            this.txtTemperatura.Name = "txtTemperatura";
            this.txtTemperatura.Size = new System.Drawing.Size(39, 20);
            this.txtTemperatura.TabIndex = 11;
            this.txtTemperatura.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(315, 19);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(78, 13);
            this.label35.TabIndex = 10;
            this.label35.Text = "Temperatura";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(283, 19);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(14, 13);
            this.label32.TabIndex = 9;
            this.label32.Text = "x\'";
            // 
            // txtPulso
            // 
            this.txtPulso.Location = new System.Drawing.Point(238, 15);
            this.txtPulso.Name = "txtPulso";
            this.txtPulso.Size = new System.Drawing.Size(39, 20);
            this.txtPulso.TabIndex = 6;
            this.txtPulso.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(194, 19);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(38, 13);
            this.label34.TabIndex = 5;
            this.label34.Text = "Pulso";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(147, 19);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(37, 13);
            this.label31.TabIndex = 4;
            this.label31.Text = "mmHg";
            // 
            // txtPAMinima
            // 
            this.txtPAMinima.Location = new System.Drawing.Point(102, 15);
            this.txtPAMinima.Name = "txtPAMinima";
            this.txtPAMinima.Size = new System.Drawing.Size(39, 20);
            this.txtPAMinima.TabIndex = 3;
            this.txtPAMinima.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(90, 19);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(12, 13);
            this.label30.TabIndex = 2;
            this.label30.Text = "/";
            // 
            // txtPAMaxima
            // 
            this.txtPAMaxima.Location = new System.Drawing.Point(51, 15);
            this.txtPAMaxima.Name = "txtPAMaxima";
            this.txtPAMaxima.Size = new System.Drawing.Size(39, 20);
            this.txtPAMaxima.TabIndex = 1;
            this.txtPAMaxima.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(7, 19);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(31, 13);
            this.label29.TabIndex = 0;
            this.label29.Text = "P.A.";
            // 
            // tbpAyudaDx
            // 
            this.tbpAyudaDx.Controls.Add(this.groupBox16);
            this.tbpAyudaDx.Controls.Add(this.groupBox15);
            this.tbpAyudaDx.Location = new System.Drawing.Point(61, 4);
            this.tbpAyudaDx.Name = "tbpAyudaDx";
            this.tbpAyudaDx.Padding = new System.Windows.Forms.Padding(3);
            this.tbpAyudaDx.Size = new System.Drawing.Size(1290, 456);
            this.tbpAyudaDx.TabIndex = 2;
            this.tbpAyudaDx.Text = "Informes de Ayuda Diagnóstica";
            this.tbpAyudaDx.UseVisualStyleBackColor = true;
            this.tbpAyudaDx.Leave += new System.EventHandler(this.tabPage3_Leave);
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.dataGridView7);
            this.groupBox16.Location = new System.Drawing.Point(7, 161);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(666, 289);
            this.groupBox16.TabIndex = 1;
            this.groupBox16.TabStop = false;
            // 
            // dataGridView7
            // 
            this.dataGridView7.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView7.Location = new System.Drawing.Point(7, 12);
            this.dataGridView7.Name = "dataGridView7";
            this.dataGridView7.Size = new System.Drawing.Size(653, 271);
            this.dataGridView7.TabIndex = 0;
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.button32);
            this.groupBox15.Controls.Add(this.button31);
            this.groupBox15.Controls.Add(this.button5);
            this.groupBox15.Controls.Add(this.textBox14);
            this.groupBox15.Controls.Add(this.label45);
            this.groupBox15.Controls.Add(this.comboBox10);
            this.groupBox15.Controls.Add(this.label44);
            this.groupBox15.Controls.Add(this.dateTimePicker1);
            this.groupBox15.Controls.Add(this.label43);
            this.groupBox15.Location = new System.Drawing.Point(7, 7);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(666, 154);
            this.groupBox15.TabIndex = 0;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Registro";
            // 
            // button32
            // 
            this.button32.Location = new System.Drawing.Point(585, 71);
            this.button32.Name = "button32";
            this.button32.Size = new System.Drawing.Size(75, 23);
            this.button32.TabIndex = 6;
            this.button32.Text = "Agregar";
            this.button32.UseVisualStyleBackColor = true;
            // 
            // button31
            // 
            this.button31.Location = new System.Drawing.Point(585, 94);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(75, 23);
            this.button31.TabIndex = 6;
            this.button31.Text = "Modificar";
            this.button31.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(585, 117);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 6;
            this.button5.Text = "Eliminar";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // textBox14
            // 
            this.textBox14.Location = new System.Drawing.Point(98, 71);
            this.textBox14.Multiline = true;
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(482, 70);
            this.textBox14.TabIndex = 5;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(10, 75);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(42, 13);
            this.label45.TabIndex = 4;
            this.label45.Text = "Informe";
            // 
            // comboBox10
            // 
            this.comboBox10.FormattingEnabled = true;
            this.comboBox10.Items.AddRange(new object[] {
            "Laboratorio",
            "Imágenes",
            "Patología",
            "Otros"});
            this.comboBox10.Location = new System.Drawing.Point(98, 45);
            this.comboBox10.Name = "comboBox10";
            this.comboBox10.Size = new System.Drawing.Size(216, 21);
            this.comboBox10.TabIndex = 3;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(10, 49);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(84, 13);
            this.label44.TabIndex = 2;
            this.label44.Text = "Tipo de Examen";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(98, 20);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(100, 20);
            this.dateTimePicker1.TabIndex = 1;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(10, 24);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(37, 13);
            this.label43.TabIndex = 0;
            this.label43.Text = "Fecha";
            // 
            // tbpPlanTrabajo
            // 
            this.tbpPlanTrabajo.Controls.Add(this.groupBox19);
            this.tbpPlanTrabajo.Controls.Add(this.groupBox18);
            this.tbpPlanTrabajo.Controls.Add(this.groupBox17);
            this.tbpPlanTrabajo.Location = new System.Drawing.Point(61, 4);
            this.tbpPlanTrabajo.Name = "tbpPlanTrabajo";
            this.tbpPlanTrabajo.Padding = new System.Windows.Forms.Padding(3);
            this.tbpPlanTrabajo.Size = new System.Drawing.Size(1290, 456);
            this.tbpPlanTrabajo.TabIndex = 3;
            this.tbpPlanTrabajo.Text = "Plan de Trabajo";
            this.tbpPlanTrabajo.UseVisualStyleBackColor = true;
            this.tbpPlanTrabajo.Leave += new System.EventHandler(this.tabPage4_Leave);
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this.dataGridView9);
            this.groupBox19.Location = new System.Drawing.Point(460, 7);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(479, 443);
            this.groupBox19.TabIndex = 1;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "Control de Asistencia";
            // 
            // dataGridView9
            // 
            this.dataGridView9.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView9.Location = new System.Drawing.Point(6, 19);
            this.dataGridView9.Name = "dataGridView9";
            this.dataGridView9.Size = new System.Drawing.Size(465, 416);
            this.dataGridView9.TabIndex = 0;
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.dataGridView8);
            this.groupBox18.Controls.Add(this.button27);
            this.groupBox18.Controls.Add(this.button26);
            this.groupBox18.Controls.Add(this.button9);
            this.groupBox18.Controls.Add(this.textBox16);
            this.groupBox18.Controls.Add(this.label47);
            this.groupBox18.Controls.Add(this.comboBox11);
            this.groupBox18.Controls.Add(this.label46);
            this.groupBox18.Location = new System.Drawing.Point(6, 77);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(447, 373);
            this.groupBox18.TabIndex = 0;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "Procedimientos y/o Tratamientos";
            // 
            // dataGridView8
            // 
            this.dataGridView8.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView8.Location = new System.Drawing.Point(11, 77);
            this.dataGridView8.Name = "dataGridView8";
            this.dataGridView8.Size = new System.Drawing.Size(427, 288);
            this.dataGridView8.TabIndex = 5;
            // 
            // button27
            // 
            this.button27.Location = new System.Drawing.Point(213, 43);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(75, 23);
            this.button27.TabIndex = 4;
            this.button27.Text = "Agregar";
            this.button27.UseVisualStyleBackColor = true;
            // 
            // button26
            // 
            this.button26.Location = new System.Drawing.Point(288, 44);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(75, 23);
            this.button26.TabIndex = 4;
            this.button26.Text = "Modificar";
            this.button26.UseVisualStyleBackColor = true;
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(363, 44);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 23);
            this.button9.TabIndex = 4;
            this.button9.Text = "Eliminar";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // textBox16
            // 
            this.textBox16.Location = new System.Drawing.Point(110, 45);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(57, 20);
            this.textBox16.TabIndex = 3;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(7, 49);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(80, 13);
            this.label47.TabIndex = 2;
            this.label47.Text = "N° de Sesiones";
            // 
            // comboBox11
            // 
            this.comboBox11.FormattingEnabled = true;
            this.comboBox11.Location = new System.Drawing.Point(110, 20);
            this.comboBox11.Name = "comboBox11";
            this.comboBox11.Size = new System.Drawing.Size(328, 21);
            this.comboBox11.TabIndex = 1;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(7, 24);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(74, 13);
            this.label46.TabIndex = 0;
            this.label46.Text = "Procedimiento";
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.label52);
            this.groupBox17.Controls.Add(this.label53);
            this.groupBox17.Controls.Add(this.label50);
            this.groupBox17.Controls.Add(this.label51);
            this.groupBox17.Controls.Add(this.label49);
            this.groupBox17.Controls.Add(this.label48);
            this.groupBox17.Controls.Add(this.button8);
            this.groupBox17.Controls.Add(this.button7);
            this.groupBox17.Controls.Add(this.button6);
            this.groupBox17.Location = new System.Drawing.Point(7, 7);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(446, 64);
            this.groupBox17.TabIndex = 0;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "Ayuda Diagnóstica";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.ForeColor = System.Drawing.Color.Blue;
            this.label52.Location = new System.Drawing.Point(418, 45);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(19, 13);
            this.label52.TabIndex = 9;
            this.label52.Text = "99";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(362, 45);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(58, 13);
            this.label53.TabIndex = 8;
            this.label53.Text = "Tot. Solic.:";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.ForeColor = System.Drawing.Color.Blue;
            this.label50.Location = new System.Drawing.Point(241, 45);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(19, 13);
            this.label50.TabIndex = 7;
            this.label50.Text = "99";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(185, 45);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(58, 13);
            this.label51.TabIndex = 6;
            this.label51.Text = "Tot. Solic.:";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.ForeColor = System.Drawing.Color.Blue;
            this.label49.Location = new System.Drawing.Point(65, 45);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(19, 13);
            this.label49.TabIndex = 5;
            this.label49.Text = "99";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(9, 45);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(58, 13);
            this.label48.TabIndex = 4;
            this.label48.Text = "Tot. Solic.:";
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(363, 19);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(75, 23);
            this.button8.TabIndex = 0;
            this.button8.Text = "Patología";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(186, 19);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 23);
            this.button7.TabIndex = 0;
            this.button7.Text = "Imágenes";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(9, 19);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 0;
            this.button6.Text = "Laboratorio";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // tbpDiagnostico
            // 
            this.tbpDiagnostico.Controls.Add(this.groupBox20);
            this.tbpDiagnostico.Cursor = System.Windows.Forms.Cursors.Default;
            this.tbpDiagnostico.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tbpDiagnostico.Location = new System.Drawing.Point(61, 4);
            this.tbpDiagnostico.Name = "tbpDiagnostico";
            this.tbpDiagnostico.Padding = new System.Windows.Forms.Padding(3);
            this.tbpDiagnostico.Size = new System.Drawing.Size(1290, 456);
            this.tbpDiagnostico.TabIndex = 4;
            this.tbpDiagnostico.Text = "Diagnóstico (*)";
            this.tbpDiagnostico.UseVisualStyleBackColor = true;
            this.tbpDiagnostico.Leave += new System.EventHandler(this.tabPage5_Leave);
            // 
            // groupBox20
            // 
            this.groupBox20.Controls.Add(this.cboCIEDescripcion);
            this.groupBox20.Controls.Add(this.dataGridView10);
            this.groupBox20.Controls.Add(this.groupBox22);
            this.groupBox20.Controls.Add(this.textBox19);
            this.groupBox20.Controls.Add(this.txtCIECod);
            this.groupBox20.Controls.Add(this.label56);
            this.groupBox20.Controls.Add(this.label55);
            this.groupBox20.Controls.Add(this.label54);
            this.groupBox20.Location = new System.Drawing.Point(7, 7);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(932, 442);
            this.groupBox20.TabIndex = 0;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "Diagnóstico Actual";
            // 
            // cboCIEDescripcion
            // 
            this.cboCIEDescripcion.FormattingEnabled = true;
            this.cboCIEDescripcion.Location = new System.Drawing.Point(76, 41);
            this.cboCIEDescripcion.Name = "cboCIEDescripcion";
            this.cboCIEDescripcion.Size = new System.Drawing.Size(849, 21);
            this.cboCIEDescripcion.TabIndex = 4;
            this.cboCIEDescripcion.SelectedIndexChanged += new System.EventHandler(this.cboCIEDescripcion_SelectedIndexChanged);
            // 
            // dataGridView10
            // 
            this.dataGridView10.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView10.Location = new System.Drawing.Point(9, 197);
            this.dataGridView10.Name = "dataGridView10";
            this.dataGridView10.Size = new System.Drawing.Size(916, 239);
            this.dataGridView10.TabIndex = 3;
            // 
            // groupBox22
            // 
            this.groupBox22.Controls.Add(this.groupBox28);
            this.groupBox22.Controls.Add(this.groupBox27);
            this.groupBox22.Controls.Add(this.button29);
            this.groupBox22.Controls.Add(this.button28);
            this.groupBox22.Controls.Add(this.button10);
            this.groupBox22.Location = new System.Drawing.Point(9, 91);
            this.groupBox22.Name = "groupBox22";
            this.groupBox22.Size = new System.Drawing.Size(412, 100);
            this.groupBox22.TabIndex = 2;
            this.groupBox22.TabStop = false;
            // 
            // groupBox28
            // 
            this.groupBox28.Controls.Add(this.radioButton5);
            this.groupBox28.Controls.Add(this.radioButton4);
            this.groupBox28.Location = new System.Drawing.Point(197, 15);
            this.groupBox28.Name = "groupBox28";
            this.groupBox28.Size = new System.Drawing.Size(128, 77);
            this.groupBox28.TabIndex = 4;
            this.groupBox28.TabStop = false;
            this.groupBox28.Text = "Tipo de Diagnóstico";
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.Location = new System.Drawing.Point(18, 42);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(79, 17);
            this.radioButton5.TabIndex = 0;
            this.radioButton5.TabStop = true;
            this.radioButton5.Text = "Secundario";
            this.radioButton5.UseVisualStyleBackColor = true;
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(18, 19);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(65, 17);
            this.radioButton4.TabIndex = 0;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "Principal";
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // groupBox27
            // 
            this.groupBox27.Controls.Add(this.radioButton1);
            this.groupBox27.Controls.Add(this.radioButton2);
            this.groupBox27.Controls.Add(this.radioButton3);
            this.groupBox27.Location = new System.Drawing.Point(6, 15);
            this.groupBox27.Name = "groupBox27";
            this.groupBox27.Size = new System.Drawing.Size(168, 77);
            this.groupBox27.TabIndex = 3;
            this.groupBox27.TabStop = false;
            this.groupBox27.Text = "Clasificación";
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(13, 19);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(75, 17);
            this.radioButton1.TabIndex = 1;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Presuntivo";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(13, 36);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(69, 17);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Definitivo";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(13, 53);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(121, 17);
            this.radioButton3.TabIndex = 1;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "Repetitivo o Crónico";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // button29
            // 
            this.button29.Location = new System.Drawing.Point(331, 40);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(75, 23);
            this.button29.TabIndex = 2;
            this.button29.Text = "Modificar";
            this.button29.UseVisualStyleBackColor = true;
            // 
            // button28
            // 
            this.button28.Location = new System.Drawing.Point(331, 9);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(75, 23);
            this.button28.TabIndex = 2;
            this.button28.Text = "Agregar";
            this.button28.UseVisualStyleBackColor = true;
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(331, 71);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(75, 23);
            this.button10.TabIndex = 2;
            this.button10.Text = "Eliminar";
            this.button10.UseVisualStyleBackColor = true;
            // 
            // textBox19
            // 
            this.textBox19.Location = new System.Drawing.Point(76, 66);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(849, 20);
            this.textBox19.TabIndex = 1;
            // 
            // txtCIECod
            // 
            this.txtCIECod.Location = new System.Drawing.Point(76, 16);
            this.txtCIECod.Name = "txtCIECod";
            this.txtCIECod.Size = new System.Drawing.Size(53, 20);
            this.txtCIECod.TabIndex = 1;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(6, 70);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(67, 13);
            this.label56.TabIndex = 0;
            this.label56.Text = "Observación";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(6, 45);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(63, 13);
            this.label55.TabIndex = 0;
            this.label55.Text = "Descripción";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(6, 20);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(40, 13);
            this.label54.TabIndex = 0;
            this.label54.Text = "Código";
            // 
            // tbpPrescripcion
            // 
            this.tbpPrescripcion.Controls.Add(this.groupBox23);
            this.tbpPrescripcion.Location = new System.Drawing.Point(61, 4);
            this.tbpPrescripcion.Name = "tbpPrescripcion";
            this.tbpPrescripcion.Padding = new System.Windows.Forms.Padding(3);
            this.tbpPrescripcion.Size = new System.Drawing.Size(1290, 456);
            this.tbpPrescripcion.TabIndex = 5;
            this.tbpPrescripcion.Text = "Prescripción";
            this.tbpPrescripcion.UseVisualStyleBackColor = true;
            this.tbpPrescripcion.Enter += new System.EventHandler(this.tbpPrescripcion_Enter);
            this.tbpPrescripcion.Leave += new System.EventHandler(this.tabPage6_Leave);
            // 
            // groupBox23
            // 
            this.groupBox23.Controls.Add(this.cboMedicamentos);
            this.groupBox23.Controls.Add(this.groupBox25);
            this.groupBox23.Controls.Add(this.groupBox24);
            this.groupBox23.Controls.Add(this.dgvPrescripcion);
            this.groupBox23.Controls.Add(this.btnDelPresc);
            this.groupBox23.Controls.Add(this.btnUpdPresc);
            this.groupBox23.Controls.Add(this.btnAddPresc);
            this.groupBox23.Controls.Add(this.txtIndicaciones);
            this.groupBox23.Controls.Add(this.label61);
            this.groupBox23.Controls.Add(this.label60);
            this.groupBox23.Controls.Add(this.cboPrinActivoFA);
            this.groupBox23.Controls.Add(this.label59);
            this.groupBox23.Location = new System.Drawing.Point(7, 7);
            this.groupBox23.Name = "groupBox23";
            this.groupBox23.Size = new System.Drawing.Size(1277, 443);
            this.groupBox23.TabIndex = 0;
            this.groupBox23.TabStop = false;
            this.groupBox23.Text = "Prescripción";
            // 
            // cboMedicamentos
            // 
            this.cboMedicamentos.FormattingEnabled = true;
            this.cboMedicamentos.Location = new System.Drawing.Point(105, 39);
            this.cboMedicamentos.Name = "cboMedicamentos";
            this.cboMedicamentos.Size = new System.Drawing.Size(312, 21);
            this.cboMedicamentos.TabIndex = 2;
            this.cboMedicamentos.SelectionChangeCommitted += new System.EventHandler(this.cboMedicamentos_SelectionChangeCommitted);
            this.cboMedicamentos.Enter += new System.EventHandler(this.cboMedicamentos_Enter);
            this.cboMedicamentos.Leave += new System.EventHandler(this.cboMedicamentos_Leave);
            // 
            // groupBox25
            // 
            this.groupBox25.Controls.Add(this.textBox30);
            this.groupBox25.Controls.Add(this.dataGridView13);
            this.groupBox25.Controls.Add(this.label72);
            this.groupBox25.Controls.Add(this.textBox22);
            this.groupBox25.Controls.Add(this.btnDelFMag);
            this.groupBox25.Controls.Add(this.btnUpdFMag);
            this.groupBox25.Controls.Add(this.btnAddFMag);
            this.groupBox25.Controls.Add(this.textBox21);
            this.groupBox25.Controls.Add(this.lblUnidad);
            this.groupBox25.Controls.Add(this.label64);
            this.groupBox25.Controls.Add(this.lblSimbolo);
            this.groupBox25.Controls.Add(this.comboBox14);
            this.groupBox25.Controls.Add(this.label63);
            this.groupBox25.Controls.Add(this.label62);
            this.groupBox25.Location = new System.Drawing.Point(6, 232);
            this.groupBox25.Name = "groupBox25";
            this.groupBox25.Size = new System.Drawing.Size(497, 205);
            this.groupBox25.TabIndex = 9;
            this.groupBox25.TabStop = false;
            this.groupBox25.Text = "Fórmula Magistral";
            // 
            // textBox30
            // 
            this.textBox30.Location = new System.Drawing.Point(105, 70);
            this.textBox30.Multiline = true;
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new System.Drawing.Size(291, 50);
            this.textBox30.TabIndex = 9;
            // 
            // dataGridView13
            // 
            this.dataGridView13.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView13.Location = new System.Drawing.Point(9, 126);
            this.dataGridView13.Name = "dataGridView13";
            this.dataGridView13.Size = new System.Drawing.Size(479, 73);
            this.dataGridView13.TabIndex = 3;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(32, 74);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(67, 13);
            this.label72.TabIndex = 8;
            this.label72.Text = "Indicaciones";
            // 
            // textBox22
            // 
            this.textBox22.Location = new System.Drawing.Point(105, 45);
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new System.Drawing.Size(33, 20);
            this.textBox22.TabIndex = 2;
            // 
            // btnDelFMag
            // 
            this.btnDelFMag.Location = new System.Drawing.Point(413, 97);
            this.btnDelFMag.Name = "btnDelFMag";
            this.btnDelFMag.Size = new System.Drawing.Size(75, 23);
            this.btnDelFMag.TabIndex = 5;
            this.btnDelFMag.Text = "Eliminar";
            this.btnDelFMag.UseVisualStyleBackColor = true;
            // 
            // btnUpdFMag
            // 
            this.btnUpdFMag.Location = new System.Drawing.Point(413, 74);
            this.btnUpdFMag.Name = "btnUpdFMag";
            this.btnUpdFMag.Size = new System.Drawing.Size(75, 23);
            this.btnUpdFMag.TabIndex = 5;
            this.btnUpdFMag.Text = "Modificar";
            this.btnUpdFMag.UseVisualStyleBackColor = true;
            // 
            // btnAddFMag
            // 
            this.btnAddFMag.Location = new System.Drawing.Point(413, 51);
            this.btnAddFMag.Name = "btnAddFMag";
            this.btnAddFMag.Size = new System.Drawing.Size(75, 23);
            this.btnAddFMag.TabIndex = 5;
            this.btnAddFMag.Text = "Agregar";
            this.btnAddFMag.UseVisualStyleBackColor = true;
            // 
            // textBox21
            // 
            this.textBox21.Location = new System.Drawing.Point(417, 19);
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new System.Drawing.Size(33, 20);
            this.textBox21.TabIndex = 2;
            // 
            // lblUnidad
            // 
            this.lblUnidad.AutoSize = true;
            this.lblUnidad.Location = new System.Drawing.Point(141, 49);
            this.lblUnidad.Name = "lblUnidad";
            this.lblUnidad.Size = new System.Drawing.Size(27, 13);
            this.lblUnidad.TabIndex = 0;
            this.lblUnidad.Text = "Und";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(50, 49);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(49, 13);
            this.label64.TabIndex = 0;
            this.label64.Text = "Cantidad";
            // 
            // lblSimbolo
            // 
            this.lblSimbolo.AutoSize = true;
            this.lblSimbolo.Location = new System.Drawing.Point(453, 23);
            this.lblSimbolo.Name = "lblSimbolo";
            this.lblSimbolo.Size = new System.Drawing.Size(30, 13);
            this.lblSimbolo.TabIndex = 0;
            this.lblSimbolo.Text = "Simb";
            // 
            // comboBox14
            // 
            this.comboBox14.FormattingEnabled = true;
            this.comboBox14.Location = new System.Drawing.Point(105, 19);
            this.comboBox14.Name = "comboBox14";
            this.comboBox14.Size = new System.Drawing.Size(224, 21);
            this.comboBox14.TabIndex = 1;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(335, 23);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(76, 13);
            this.label63.TabIndex = 0;
            this.label63.Text = "Concentración";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(19, 23);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(80, 13);
            this.label62.TabIndex = 0;
            this.label62.Text = "Principio Activo";
            // 
            // groupBox24
            // 
            this.groupBox24.Controls.Add(this.txtLaboratorio);
            this.groupBox24.Controls.Add(this.label3);
            this.groupBox24.Controls.Add(this.lblClasifTerap);
            this.groupBox24.Controls.Add(this.txtPrA_Contraindicaciones);
            this.groupBox24.Controls.Add(this.txtPrA_Interacciones);
            this.groupBox24.Controls.Add(this.txtPrA_Dosis);
            this.groupBox24.Controls.Add(this.label71);
            this.groupBox24.Controls.Add(this.label70);
            this.groupBox24.Controls.Add(this.label69);
            this.groupBox24.Controls.Add(this.lblPrA_Nombre);
            this.groupBox24.Location = new System.Drawing.Point(509, 16);
            this.groupBox24.Name = "groupBox24";
            this.groupBox24.Size = new System.Drawing.Size(762, 421);
            this.groupBox24.TabIndex = 7;
            this.groupBox24.TabStop = false;
            this.groupBox24.Text = "Farmacopea";
            // 
            // txtLaboratorio
            // 
            this.txtLaboratorio.ForeColor = System.Drawing.Color.Blue;
            this.txtLaboratorio.Location = new System.Drawing.Point(108, 73);
            this.txtLaboratorio.Name = "txtLaboratorio";
            this.txtLaboratorio.ReadOnly = true;
            this.txtLaboratorio.Size = new System.Drawing.Size(648, 20);
            this.txtLaboratorio.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(43, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Laboratorio";
            // 
            // lblClasifTerap
            // 
            this.lblClasifTerap.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClasifTerap.ForeColor = System.Drawing.Color.Red;
            this.lblClasifTerap.Location = new System.Drawing.Point(7, 41);
            this.lblClasifTerap.Name = "lblClasifTerap";
            this.lblClasifTerap.Size = new System.Drawing.Size(413, 17);
            this.lblClasifTerap.TabIndex = 3;
            this.lblClasifTerap.Text = "Clasificación Terapeútica";
            // 
            // txtPrA_Contraindicaciones
            // 
            this.txtPrA_Contraindicaciones.ForeColor = System.Drawing.Color.Blue;
            this.txtPrA_Contraindicaciones.Location = new System.Drawing.Point(108, 175);
            this.txtPrA_Contraindicaciones.Multiline = true;
            this.txtPrA_Contraindicaciones.Name = "txtPrA_Contraindicaciones";
            this.txtPrA_Contraindicaciones.ReadOnly = true;
            this.txtPrA_Contraindicaciones.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtPrA_Contraindicaciones.Size = new System.Drawing.Size(648, 47);
            this.txtPrA_Contraindicaciones.TabIndex = 2;
            // 
            // txtPrA_Interacciones
            // 
            this.txtPrA_Interacciones.ForeColor = System.Drawing.Color.Blue;
            this.txtPrA_Interacciones.Location = new System.Drawing.Point(108, 123);
            this.txtPrA_Interacciones.Multiline = true;
            this.txtPrA_Interacciones.Name = "txtPrA_Interacciones";
            this.txtPrA_Interacciones.ReadOnly = true;
            this.txtPrA_Interacciones.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtPrA_Interacciones.Size = new System.Drawing.Size(648, 47);
            this.txtPrA_Interacciones.TabIndex = 2;
            // 
            // txtPrA_Dosis
            // 
            this.txtPrA_Dosis.ForeColor = System.Drawing.Color.Blue;
            this.txtPrA_Dosis.Location = new System.Drawing.Point(108, 98);
            this.txtPrA_Dosis.Name = "txtPrA_Dosis";
            this.txtPrA_Dosis.ReadOnly = true;
            this.txtPrA_Dosis.Size = new System.Drawing.Size(648, 20);
            this.txtPrA_Dosis.TabIndex = 2;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(6, 175);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(97, 13);
            this.label71.TabIndex = 1;
            this.label71.Text = "Contraindicaciones";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(32, 123);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(71, 13);
            this.label70.TabIndex = 1;
            this.label70.Text = "Interacciones";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(70, 102);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(33, 13);
            this.label69.TabIndex = 1;
            this.label69.Text = "Dosis";
            // 
            // lblPrA_Nombre
            // 
            this.lblPrA_Nombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrA_Nombre.ForeColor = System.Drawing.Color.Blue;
            this.lblPrA_Nombre.Location = new System.Drawing.Point(7, 20);
            this.lblPrA_Nombre.Name = "lblPrA_Nombre";
            this.lblPrA_Nombre.Size = new System.Drawing.Size(413, 17);
            this.lblPrA_Nombre.TabIndex = 0;
            this.lblPrA_Nombre.Text = "Principio Activo";
            // 
            // dgvPrescripcion
            // 
            this.dgvPrescripcion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPrescripcion.Location = new System.Drawing.Point(9, 119);
            this.dgvPrescripcion.Name = "dgvPrescripcion";
            this.dgvPrescripcion.Size = new System.Drawing.Size(488, 104);
            this.dgvPrescripcion.TabIndex = 6;
            // 
            // btnDelPresc
            // 
            this.btnDelPresc.Location = new System.Drawing.Point(422, 89);
            this.btnDelPresc.Name = "btnDelPresc";
            this.btnDelPresc.Size = new System.Drawing.Size(75, 23);
            this.btnDelPresc.TabIndex = 5;
            this.btnDelPresc.Text = "Eliminar";
            this.btnDelPresc.UseVisualStyleBackColor = true;
            this.btnDelPresc.Click += new System.EventHandler(this.btnDelPresc_Click);
            // 
            // btnUpdPresc
            // 
            this.btnUpdPresc.Location = new System.Drawing.Point(422, 64);
            this.btnUpdPresc.Name = "btnUpdPresc";
            this.btnUpdPresc.Size = new System.Drawing.Size(75, 23);
            this.btnUpdPresc.TabIndex = 5;
            this.btnUpdPresc.Text = "Modificar";
            this.btnUpdPresc.UseVisualStyleBackColor = true;
            // 
            // btnAddPresc
            // 
            this.btnAddPresc.Location = new System.Drawing.Point(423, 39);
            this.btnAddPresc.Name = "btnAddPresc";
            this.btnAddPresc.Size = new System.Drawing.Size(75, 23);
            this.btnAddPresc.TabIndex = 5;
            this.btnAddPresc.Text = "Agregar";
            this.btnAddPresc.UseVisualStyleBackColor = true;
            this.btnAddPresc.Click += new System.EventHandler(this.btnAddPresc_Click);
            // 
            // txtIndicaciones
            // 
            this.txtIndicaciones.Location = new System.Drawing.Point(105, 62);
            this.txtIndicaciones.Multiline = true;
            this.txtIndicaciones.Name = "txtIndicaciones";
            this.txtIndicaciones.Size = new System.Drawing.Size(312, 50);
            this.txtIndicaciones.TabIndex = 4;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(32, 66);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(67, 13);
            this.label61.TabIndex = 2;
            this.label61.Text = "Indicaciones";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(6, 43);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(93, 13);
            this.label60.TabIndex = 2;
            this.label60.Text = "Nombre Comercial";
            // 
            // cboPrinActivoFA
            // 
            this.cboPrinActivoFA.FormattingEnabled = true;
            this.cboPrinActivoFA.Location = new System.Drawing.Point(105, 16);
            this.cboPrinActivoFA.Name = "cboPrinActivoFA";
            this.cboPrinActivoFA.Size = new System.Drawing.Size(392, 21);
            this.cboPrinActivoFA.TabIndex = 1;
            this.cboPrinActivoFA.SelectionChangeCommitted += new System.EventHandler(this.cboPrinActivoFA_SelectionChangeCommitted);
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(19, 20);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(80, 13);
            this.label59.TabIndex = 0;
            this.label59.Text = "Principio Activo";
            // 
            // tbpConsultaAnterior
            // 
            this.tbpConsultaAnterior.BackColor = System.Drawing.Color.Transparent;
            this.tbpConsultaAnterior.Controls.Add(this.groupBox43);
            this.tbpConsultaAnterior.Controls.Add(this.groupBox42);
            this.tbpConsultaAnterior.Controls.Add(this.groupBox41);
            this.tbpConsultaAnterior.Location = new System.Drawing.Point(61, 4);
            this.tbpConsultaAnterior.Name = "tbpConsultaAnterior";
            this.tbpConsultaAnterior.Padding = new System.Windows.Forms.Padding(3);
            this.tbpConsultaAnterior.Size = new System.Drawing.Size(1290, 456);
            this.tbpConsultaAnterior.TabIndex = 6;
            this.tbpConsultaAnterior.Text = "Datos Complementarios";
            this.tbpConsultaAnterior.Leave += new System.EventHandler(this.tabPage7_Leave);
            // 
            // groupBox43
            // 
            this.groupBox43.Controls.Add(this.txtObservaciones);
            this.groupBox43.Location = new System.Drawing.Point(478, 70);
            this.groupBox43.Name = "groupBox43";
            this.groupBox43.Size = new System.Drawing.Size(464, 380);
            this.groupBox43.TabIndex = 1;
            this.groupBox43.TabStop = false;
            this.groupBox43.Text = "Observaciones Generales";
            // 
            // txtObservaciones
            // 
            this.txtObservaciones.Location = new System.Drawing.Point(7, 20);
            this.txtObservaciones.Multiline = true;
            this.txtObservaciones.Name = "txtObservaciones";
            this.txtObservaciones.Size = new System.Drawing.Size(450, 354);
            this.txtObservaciones.TabIndex = 1;
            // 
            // groupBox42
            // 
            this.groupBox42.Controls.Add(this.txtRecomendaciones);
            this.groupBox42.Location = new System.Drawing.Point(6, 70);
            this.groupBox42.Name = "groupBox42";
            this.groupBox42.Size = new System.Drawing.Size(464, 380);
            this.groupBox42.TabIndex = 1;
            this.groupBox42.TabStop = false;
            this.groupBox42.Text = "Recomendaciones";
            // 
            // txtRecomendaciones
            // 
            this.txtRecomendaciones.Location = new System.Drawing.Point(7, 20);
            this.txtRecomendaciones.Multiline = true;
            this.txtRecomendaciones.Name = "txtRecomendaciones";
            this.txtRecomendaciones.Size = new System.Drawing.Size(450, 354);
            this.txtRecomendaciones.TabIndex = 0;
            // 
            // groupBox41
            // 
            this.groupBox41.Controls.Add(this.dtpProximaCita);
            this.groupBox41.Location = new System.Drawing.Point(7, 7);
            this.groupBox41.Name = "groupBox41";
            this.groupBox41.Size = new System.Drawing.Size(119, 56);
            this.groupBox41.TabIndex = 0;
            this.groupBox41.TabStop = false;
            this.groupBox41.Text = "Fecha Próxima Cita";
            // 
            // dtpProximaCita
            // 
            this.dtpProximaCita.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpProximaCita.Location = new System.Drawing.Point(7, 20);
            this.dtpProximaCita.Name = "dtpProximaCita";
            this.dtpProximaCita.Size = new System.Drawing.Size(100, 20);
            this.dtpProximaCita.TabIndex = 0;
            // 
            // tbpComplementarios
            // 
            this.tbpComplementarios.BackColor = System.Drawing.Color.AliceBlue;
            this.tbpComplementarios.Controls.Add(this.groupBox34);
            this.tbpComplementarios.Controls.Add(this.groupBox33);
            this.tbpComplementarios.Controls.Add(this.groupBox32);
            this.tbpComplementarios.Controls.Add(this.groupBox31);
            this.tbpComplementarios.Controls.Add(this.groupBox30);
            this.tbpComplementarios.Controls.Add(this.groupBox29);
            this.tbpComplementarios.Location = new System.Drawing.Point(61, 4);
            this.tbpComplementarios.Name = "tbpComplementarios";
            this.tbpComplementarios.Padding = new System.Windows.Forms.Padding(3);
            this.tbpComplementarios.Size = new System.Drawing.Size(1290, 456);
            this.tbpComplementarios.TabIndex = 7;
            this.tbpComplementarios.Text = "Resumen de la Consulta Anterior";
            // 
            // groupBox34
            // 
            this.groupBox34.Controls.Add(this.dataGridView21);
            this.groupBox34.Location = new System.Drawing.Point(454, 227);
            this.groupBox34.Name = "groupBox34";
            this.groupBox34.Size = new System.Drawing.Size(212, 223);
            this.groupBox34.TabIndex = 7;
            this.groupBox34.TabStop = false;
            this.groupBox34.Text = "Prescripción";
            // 
            // dataGridView21
            // 
            this.dataGridView21.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView21.Location = new System.Drawing.Point(6, 21);
            this.dataGridView21.Name = "dataGridView21";
            this.dataGridView21.Size = new System.Drawing.Size(200, 197);
            this.dataGridView21.TabIndex = 1;
            // 
            // groupBox33
            // 
            this.groupBox33.Controls.Add(this.dataGridView20);
            this.groupBox33.Location = new System.Drawing.Point(230, 227);
            this.groupBox33.Name = "groupBox33";
            this.groupBox33.Size = new System.Drawing.Size(212, 223);
            this.groupBox33.TabIndex = 8;
            this.groupBox33.TabStop = false;
            this.groupBox33.Text = "Diagnóstico";
            // 
            // dataGridView20
            // 
            this.dataGridView20.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView20.Location = new System.Drawing.Point(7, 20);
            this.dataGridView20.Name = "dataGridView20";
            this.dataGridView20.Size = new System.Drawing.Size(200, 197);
            this.dataGridView20.TabIndex = 0;
            // 
            // groupBox32
            // 
            this.groupBox32.Controls.Add(this.dataGridView22);
            this.groupBox32.Location = new System.Drawing.Point(6, 227);
            this.groupBox32.Name = "groupBox32";
            this.groupBox32.Size = new System.Drawing.Size(212, 223);
            this.groupBox32.TabIndex = 9;
            this.groupBox32.TabStop = false;
            this.groupBox32.Text = "Plan de Trabajo";
            // 
            // dataGridView22
            // 
            this.dataGridView22.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView22.Location = new System.Drawing.Point(6, 21);
            this.dataGridView22.Name = "dataGridView22";
            this.dataGridView22.Size = new System.Drawing.Size(200, 197);
            this.dataGridView22.TabIndex = 2;
            // 
            // groupBox31
            // 
            this.groupBox31.Controls.Add(this.groupBox38);
            this.groupBox31.Controls.Add(this.groupBox37);
            this.groupBox31.Controls.Add(this.groupBox36);
            this.groupBox31.Location = new System.Drawing.Point(672, 5);
            this.groupBox31.Name = "groupBox31";
            this.groupBox31.Size = new System.Drawing.Size(279, 446);
            this.groupBox31.TabIndex = 6;
            this.groupBox31.TabStop = false;
            this.groupBox31.Text = "Antecedentes";
            // 
            // groupBox38
            // 
            this.groupBox38.Controls.Add(this.dataGridView19);
            this.groupBox38.Location = new System.Drawing.Point(6, 300);
            this.groupBox38.Name = "groupBox38";
            this.groupBox38.Size = new System.Drawing.Size(267, 140);
            this.groupBox38.TabIndex = 0;
            this.groupBox38.TabStop = false;
            this.groupBox38.Text = "Familiares";
            // 
            // dataGridView19
            // 
            this.dataGridView19.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView19.Location = new System.Drawing.Point(8, 19);
            this.dataGridView19.Name = "dataGridView19";
            this.dataGridView19.Size = new System.Drawing.Size(255, 115);
            this.dataGridView19.TabIndex = 0;
            // 
            // groupBox37
            // 
            this.groupBox37.Controls.Add(this.dataGridView18);
            this.groupBox37.Location = new System.Drawing.Point(6, 160);
            this.groupBox37.Name = "groupBox37";
            this.groupBox37.Size = new System.Drawing.Size(267, 140);
            this.groupBox37.TabIndex = 0;
            this.groupBox37.TabStop = false;
            this.groupBox37.Text = "Quirúrgicos";
            // 
            // dataGridView18
            // 
            this.dataGridView18.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView18.Location = new System.Drawing.Point(8, 19);
            this.dataGridView18.Name = "dataGridView18";
            this.dataGridView18.Size = new System.Drawing.Size(255, 115);
            this.dataGridView18.TabIndex = 0;
            // 
            // groupBox36
            // 
            this.groupBox36.Controls.Add(this.dataGridView17);
            this.groupBox36.Location = new System.Drawing.Point(6, 20);
            this.groupBox36.Name = "groupBox36";
            this.groupBox36.Size = new System.Drawing.Size(267, 140);
            this.groupBox36.TabIndex = 0;
            this.groupBox36.TabStop = false;
            this.groupBox36.Text = "Patológicos";
            // 
            // dataGridView17
            // 
            this.dataGridView17.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView17.Location = new System.Drawing.Point(6, 19);
            this.dataGridView17.Name = "dataGridView17";
            this.dataGridView17.Size = new System.Drawing.Size(255, 115);
            this.dataGridView17.TabIndex = 0;
            // 
            // groupBox30
            // 
            this.groupBox30.Controls.Add(this.groupBox35);
            this.groupBox30.Controls.Add(this.textBox28);
            this.groupBox30.Location = new System.Drawing.Point(4, 68);
            this.groupBox30.Name = "groupBox30";
            this.groupBox30.Size = new System.Drawing.Size(662, 153);
            this.groupBox30.TabIndex = 5;
            this.groupBox30.TabStop = false;
            this.groupBox30.Text = "Mot. de la Consulta / Evoluc. del Paciente";
            // 
            // groupBox35
            // 
            this.groupBox35.Controls.Add(this.groupBox39);
            this.groupBox35.Controls.Add(this.label66);
            this.groupBox35.Controls.Add(this.textBox29);
            this.groupBox35.Location = new System.Drawing.Point(219, 12);
            this.groupBox35.Name = "groupBox35";
            this.groupBox35.Size = new System.Drawing.Size(436, 135);
            this.groupBox35.TabIndex = 4;
            this.groupBox35.TabStop = false;
            this.groupBox35.Text = "Hábitos Nocivos";
            // 
            // groupBox39
            // 
            this.groupBox39.Controls.Add(this.checkBox1);
            this.groupBox39.Controls.Add(this.checkBox2);
            this.groupBox39.Controls.Add(this.checkBox3);
            this.groupBox39.Controls.Add(this.checkBox4);
            this.groupBox39.Location = new System.Drawing.Point(9, 12);
            this.groupBox39.Name = "groupBox39";
            this.groupBox39.Size = new System.Drawing.Size(416, 27);
            this.groupBox39.TabIndex = 11;
            this.groupBox39.TabStop = false;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(360, 8);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(51, 17);
            this.checkBox1.TabIndex = 0;
            this.checkBox1.Text = "Otros";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(243, 8);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(60, 17);
            this.checkBox2.TabIndex = 0;
            this.checkBox2.Text = "Drogas";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(123, 8);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(63, 17);
            this.checkBox3.TabIndex = 0;
            this.checkBox3.Text = "Tabaco";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(5, 8);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(61, 17);
            this.checkBox4.TabIndex = 0;
            this.checkBox4.Text = "Alcohol";
            this.checkBox4.UseVisualStyleBackColor = true;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(6, 42);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(59, 13);
            this.label66.TabIndex = 9;
            this.label66.Text = "Especificar";
            // 
            // textBox29
            // 
            this.textBox29.Enabled = false;
            this.textBox29.Location = new System.Drawing.Point(5, 61);
            this.textBox29.Multiline = true;
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new System.Drawing.Size(425, 68);
            this.textBox29.TabIndex = 8;
            // 
            // textBox28
            // 
            this.textBox28.Enabled = false;
            this.textBox28.Location = new System.Drawing.Point(7, 20);
            this.textBox28.Multiline = true;
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new System.Drawing.Size(208, 127);
            this.textBox28.TabIndex = 0;
            // 
            // groupBox29
            // 
            this.groupBox29.Controls.Add(this.textBox27);
            this.groupBox29.Controls.Add(this.textBox12);
            this.groupBox29.Controls.Add(this.textBox2);
            this.groupBox29.Controls.Add(this.label65);
            this.groupBox29.Controls.Add(this.label58);
            this.groupBox29.Controls.Add(this.label57);
            this.groupBox29.Location = new System.Drawing.Point(4, 5);
            this.groupBox29.Name = "groupBox29";
            this.groupBox29.Size = new System.Drawing.Size(662, 56);
            this.groupBox29.TabIndex = 4;
            this.groupBox29.TabStop = false;
            this.groupBox29.Text = "Consulta Anterior";
            // 
            // textBox27
            // 
            this.textBox27.Enabled = false;
            this.textBox27.Location = new System.Drawing.Point(402, 20);
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new System.Drawing.Size(124, 20);
            this.textBox27.TabIndex = 1;
            // 
            // textBox12
            // 
            this.textBox12.Enabled = false;
            this.textBox12.Location = new System.Drawing.Point(174, 21);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(110, 20);
            this.textBox12.TabIndex = 1;
            // 
            // textBox2
            // 
            this.textBox2.Enabled = false;
            this.textBox2.Location = new System.Drawing.Point(57, 21);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(67, 20);
            this.textBox2.TabIndex = 1;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(309, 24);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(88, 13);
            this.label65.TabIndex = 0;
            this.label65.Text = "Tipo de Atención";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(138, 25);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(30, 13);
            this.label58.TabIndex = 0;
            this.label58.Text = "Hora";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(14, 25);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(37, 13);
            this.label57.TabIndex = 0;
            this.label57.Text = "Fecha";
            // 
            // btnCerrar
            // 
            this.btnCerrar.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.btnCerrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCerrar.ForeColor = System.Drawing.Color.White;
            this.btnCerrar.Location = new System.Drawing.Point(1199, 668);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(165, 32);
            this.btnCerrar.TabIndex = 4;
            this.btnCerrar.Text = "Cerrar Ficha";
            this.btnCerrar.UseVisualStyleBackColor = false;
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // pbGrabar
            // 
            this.pbGrabar.Location = new System.Drawing.Point(262, 674);
            this.pbGrabar.Maximum = 1000;
            this.pbGrabar.Name = "pbGrabar";
            this.pbGrabar.Size = new System.Drawing.Size(226, 13);
            this.pbGrabar.TabIndex = 5;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // lblGuardando
            // 
            this.lblGuardando.AutoSize = true;
            this.lblGuardando.Location = new System.Drawing.Point(47, 674);
            this.lblGuardando.Name = "lblGuardando";
            this.lblGuardando.Size = new System.Drawing.Size(209, 13);
            this.lblGuardando.TabIndex = 7;
            this.lblGuardando.Text = "Guardando información, por favor espere...";
            // 
            // btnHistorial
            // 
            this.btnHistorial.BackColor = System.Drawing.Color.Crimson;
            this.btnHistorial.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHistorial.ForeColor = System.Drawing.Color.White;
            this.btnHistorial.Location = new System.Drawing.Point(1028, 668);
            this.btnHistorial.Name = "btnHistorial";
            this.btnHistorial.Size = new System.Drawing.Size(165, 32);
            this.btnHistorial.TabIndex = 8;
            this.btnHistorial.Text = "Ver Historial";
            this.btnHistorial.UseVisualStyleBackColor = false;
            this.btnHistorial.Click += new System.EventHandler(this.btnHistorial_Click);
            // 
            // frmFichaMedica
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1362, 706);
            this.Controls.Add(this.btnHistorial);
            this.Controls.Add(this.btnCerrar);
            this.Controls.Add(this.lblGuardando);
            this.Controls.Add(this.pbGrabar);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Name = "frmFichaMedica";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Ficha Médica";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmFichaMedica_FormClosed);
            this.Load += new System.EventHandler(this.frmFichaMedica_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tbpMotivoConsulta.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tbpAnamnesis.ResumeLayout(false);
            this.groupBox40.ResumeLayout(false);
            this.groupBox40.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox21.ResumeLayout(false);
            this.groupBox21.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSintomas)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tbpAntecedentes.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).EndInit();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAntecPatologia)).EndInit();
            this.tbpDiagnosticosAnteriores.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView6)).EndInit();
            this.tpgExamFisico.ResumeLayout(false);
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView16)).EndInit();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.tbpAyudaDx.ResumeLayout(false);
            this.groupBox16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView7)).EndInit();
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.tbpPlanTrabajo.ResumeLayout(false);
            this.groupBox19.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView9)).EndInit();
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView8)).EndInit();
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            this.tbpDiagnostico.ResumeLayout(false);
            this.groupBox20.ResumeLayout(false);
            this.groupBox20.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView10)).EndInit();
            this.groupBox22.ResumeLayout(false);
            this.groupBox28.ResumeLayout(false);
            this.groupBox28.PerformLayout();
            this.groupBox27.ResumeLayout(false);
            this.groupBox27.PerformLayout();
            this.tbpPrescripcion.ResumeLayout(false);
            this.groupBox23.ResumeLayout(false);
            this.groupBox23.PerformLayout();
            this.groupBox25.ResumeLayout(false);
            this.groupBox25.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView13)).EndInit();
            this.groupBox24.ResumeLayout(false);
            this.groupBox24.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPrescripcion)).EndInit();
            this.tbpConsultaAnterior.ResumeLayout(false);
            this.groupBox43.ResumeLayout(false);
            this.groupBox43.PerformLayout();
            this.groupBox42.ResumeLayout(false);
            this.groupBox42.PerformLayout();
            this.groupBox41.ResumeLayout(false);
            this.tbpComplementarios.ResumeLayout(false);
            this.groupBox34.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView21)).EndInit();
            this.groupBox33.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView20)).EndInit();
            this.groupBox32.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView22)).EndInit();
            this.groupBox31.ResumeLayout(false);
            this.groupBox38.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView19)).EndInit();
            this.groupBox37.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView18)).EndInit();
            this.groupBox36.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView17)).EndInit();
            this.groupBox30.ResumeLayout(false);
            this.groupBox30.PerformLayout();
            this.groupBox35.ResumeLayout(false);
            this.groupBox35.PerformLayout();
            this.groupBox39.ResumeLayout(false);
            this.groupBox39.PerformLayout();
            this.groupBox29.ResumeLayout(false);
            this.groupBox29.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblNumHistoria;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblHora;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblFecha;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblPaciente;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tbpMotivoConsulta;
        private System.Windows.Forms.TabPage tpgExamFisico;
        private System.Windows.Forms.TabPage tbpAyudaDx;
        private System.Windows.Forms.TabPage tbpPlanTrabajo;
        private System.Windows.Forms.TabPage tbpDiagnostico;
        private System.Windows.Forms.TabPage tbpPrescripcion;
        private System.Windows.Forms.TabPage tbpConsultaAnterior;
        private System.Windows.Forms.Label lblProfesion;
        private System.Windows.Forms.Label lblProcedencia;
        private System.Windows.Forms.Label lblGradoInstruccion;
        private System.Windows.Forms.Label lblLugarNacimiento;
        private System.Windows.Forms.Label lblDocIdentidad;
        private System.Windows.Forms.Label lblEdad;
        private System.Windows.Forms.Label lblSexo;
        private System.Windows.Forms.Label lblGrupoSanguineo;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tbpAnamnesis;
        private System.Windows.Forms.TabPage tbpAntecedentes;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtObservHabNoc;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DataGridView dgvSintomas;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtMotivo;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.ComboBox cboAlergias;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.DataGridView dataGridView5;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.ComboBox cboPatolFamiliar;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox cboFamiliar;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.DataGridView dataGridView4;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox cboCirugias;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.DataGridView dgvAntecPatologia;
        private System.Windows.Forms.Button btnAddAntecPatologia;
        private System.Windows.Forms.TextBox txtPatologiaObserv;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox cboPatologias;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TabPage tbpDiagnosticosAnteriores;
        private System.Windows.Forms.DataGridView dataGridView6;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox txtTalla;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox txtPeso;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox txtTemperatura;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox txtPulso;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txtPAMinima;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txtPAMaxima;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.ComboBox cboEstHidratacion;
        private System.Windows.Forms.ComboBox cboEstNutricional;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.ComboBox cboEstGeneral;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.DataGridView dataGridView7;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.ComboBox comboBox10;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.DataGridView dataGridView9;
        private System.Windows.Forms.DataGridView dataGridView8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.ComboBox comboBox11;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.GroupBox groupBox20;
        private System.Windows.Forms.GroupBox groupBox22;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.TextBox txtCIECod;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.DataGridView dataGridView10;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.GroupBox groupBox23;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.ComboBox cboPrinActivoFA;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.GroupBox groupBox24;
        private System.Windows.Forms.DataGridView dgvPrescripcion;
        private System.Windows.Forms.Button btnAddPresc;
        private System.Windows.Forms.TextBox txtIndicaciones;
        private System.Windows.Forms.TextBox txtPrA_Contraindicaciones;
        private System.Windows.Forms.TextBox txtPrA_Interacciones;
        private System.Windows.Forms.TextBox txtPrA_Dosis;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label lblPrA_Nombre;
        private System.Windows.Forms.Button btnDelPresc;
        private System.Windows.Forms.Button btnUpdPresc;
        private System.Windows.Forms.Button btnCerrar;
        private System.Windows.Forms.ProgressBar pbGrabar;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label lblGuardando;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btnDelAntecPatologia;
        private System.Windows.Forms.Button btnUpdAntecPatologia;
        private System.Windows.Forms.DataGridView dataGridView16;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.GroupBox groupBox28;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.GroupBox groupBox27;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.Label lblTipoAtencion;
        private System.Windows.Forms.TextBox txtAlergias;
        private System.Windows.Forms.Button button32;
        private System.Windows.Forms.Button button31;
        private System.Windows.Forms.Button btnHistorial;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Seleccion;
        private System.Windows.Forms.CheckBox chkOPersona;
        private System.Windows.Forms.CheckBox chkOEspacio;
        private System.Windows.Forms.CheckBox chkOTiempo;
        private System.Windows.Forms.CheckBox chkLucido;
        private System.Windows.Forms.ComboBox cboCIEDescripcion;
        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.CheckBox chkOtros;
        private System.Windows.Forms.CheckBox chkDrogas;
        private System.Windows.Forms.CheckBox chkTabaco;
        private System.Windows.Forms.CheckBox chkAlcohol;
        private System.Windows.Forms.GroupBox groupBox40;
        private System.Windows.Forms.TextBox txtFUltMenstruacion;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage tbpComplementarios;
        private System.Windows.Forms.GroupBox groupBox34;
        private System.Windows.Forms.DataGridView dataGridView21;
        private System.Windows.Forms.GroupBox groupBox33;
        private System.Windows.Forms.DataGridView dataGridView20;
        private System.Windows.Forms.GroupBox groupBox32;
        private System.Windows.Forms.DataGridView dataGridView22;
        private System.Windows.Forms.GroupBox groupBox31;
        private System.Windows.Forms.GroupBox groupBox38;
        private System.Windows.Forms.DataGridView dataGridView19;
        private System.Windows.Forms.GroupBox groupBox37;
        private System.Windows.Forms.DataGridView dataGridView18;
        private System.Windows.Forms.GroupBox groupBox36;
        private System.Windows.Forms.DataGridView dataGridView17;
        private System.Windows.Forms.GroupBox groupBox30;
        private System.Windows.Forms.GroupBox groupBox35;
        private System.Windows.Forms.GroupBox groupBox39;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.TextBox textBox29;
        private System.Windows.Forms.TextBox textBox28;
        private System.Windows.Forms.GroupBox groupBox29;
        private System.Windows.Forms.TextBox textBox27;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.GroupBox groupBox43;
        private System.Windows.Forms.TextBox txtObservaciones;
        private System.Windows.Forms.GroupBox groupBox42;
        private System.Windows.Forms.TextBox txtRecomendaciones;
        private System.Windows.Forms.GroupBox groupBox41;
        private System.Windows.Forms.DateTimePicker dtpProximaCita;
        private System.Windows.Forms.GroupBox groupBox25;
        private System.Windows.Forms.TextBox textBox30;
        private System.Windows.Forms.DataGridView dataGridView13;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.Button btnDelFMag;
        private System.Windows.Forms.Button btnUpdFMag;
        private System.Windows.Forms.Button btnAddFMag;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.Label lblUnidad;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label lblSimbolo;
        private System.Windows.Forms.ComboBox comboBox14;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.ComboBox cboMedicamentos;
        private System.Windows.Forms.Label lblClasifTerap;
        private System.Windows.Forms.TextBox txtLaboratorio;
        private System.Windows.Forms.Label label3;
    }
}