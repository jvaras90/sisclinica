﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using pjDermaBelle.CapaDatos;

namespace pjDermaBelle.CapaNegocio
{
    public class AccesoLogica
    {
        public static DataTable ListarProfesiones()
        {
            return AccesoDatos.ListarProfesiones();
        }

        public int InsertaProfesiones(string Prf_Nombre, string Estado, int UsuCrea, int UsuModi)
        {
            AccesoDatos acceso = new AccesoDatos();
            return acceso.InsertaProfesiones(Prf_Nombre, Estado, UsuCrea, UsuModi);
        }
    }
}
