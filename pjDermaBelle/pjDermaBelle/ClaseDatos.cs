﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pjDermaBelle
{
    public class ClaseDatos
    {
        DermaBelleDataDataContext obj = new DermaBelleDataDataContext();

// Otros
        public string Fecha { get { return DateTime.Today.ToShortDateString(); } }
        //public string Hora { get { return DateTime.Now.ToLongTimeString(); } }

        //Llenado del grid de Pacientes Citados
        public List<usp_PacientesCitadosResult> GridPacientesCitados(int MedId)
        {
            return obj.usp_PacientesCitados(MedId).ToList();
        }

        public List<usp_CitasResult> GridCitas(int Ser_Id, int Con_Id, DateTime CitaFecha)
        {
            return obj.usp_Citas(Ser_Id, Con_Id, CitaFecha).ToList();
        }

        public List<usp_PacienteFichasResult> GridPacientesFicha(string Pac_HC)
        {
            return obj.usp_PacienteFichas(Pac_HC).ToList();
        }

        public List<usp_CabCitadosResult> CabCitados(int UsuId)
        {
            return obj.usp_CabCitados(UsuId).ToList();
        }

        // Metodos para llenado de Combos
        public List<usp_cmbFamiliaresResult> CboFamiliares()
        {
            return obj.usp_cmbFamiliares().ToList();
        }

        public List<usp_CboDocIdentidadResult> CboDocIdentidad()
        {
            return obj.usp_CboDocIdentidad().ToList();
        }

        public List<usp_CboGradosInstruccionResult> CboInstruccion()
        {
            return obj.usp_CboGradosInstruccion().ToList();
        }

        public List<usp_CboProfesionesResult> CboProfesiones()
        {
            return obj.usp_CboProfesiones().ToList();
        }

        public List<usp_CboLugNacimientoResult> CboLugNacimiento()
        {
            return obj.usp_CboLugNacimiento().ToList();
        }

        public List<usp_CboDistritoResult> CboDistritos()
        {
            return obj.usp_CboDistrito().ToList();
        }

        public List<usp_CboPatologiasResult> CboPatologias()
        {
            return obj.usp_CboPatologias().ToList();
        }

        public List<usp_CboCirugiasResult> CboCirugias()
        {
            return obj.usp_CboCirugias().ToList();
        }

        public List<usp_CboAlergiasResult> CboAlergias()
        {
            return obj.usp_CboAlergias().ToList();
        }

        public List<usp_CboDiagnosticosResult> CboDiagnosticos()
        {
            return obj.usp_CboDiagnosticos().ToList();
        }

        public List<usp_CboServicioResult> CboServicios()
        {
            return obj.usp_CboServicio().ToList();
        }

        public List<usp_CboConsultorioResult> CboConsultorios()
        {
            return obj.usp_CboConsultorio().ToList();
        }

        public List<usp_CboMedicosResult> CboMedicos(int Ser_Id)
        {
            return obj.usp_CboMedicos(Ser_Id).ToList();
        }

        public List<usp_cmbTipoTarifaResult> CboTipoTarifa()
        {
            return obj.usp_cmbTipoTarifa().ToList();
        }

        public List<usp_cmbTarifarioResult> CboTarifario(string TtrCod)
        {
            return obj.usp_cmbTarifario(TtrCod).ToList();
        }

        public List<usp_CboTarifasResult> CboTarifas(string Filtro)
        {
            return obj.usp_CboTarifas(Filtro).ToList();
        }

        public List<usp_TDocumentoCboResult> CboDocFact()
        {
            return obj.usp_TDocumentoCbo().ToList();
        }

        public List<usp_CboEspecialidadesResult> CboEspecialidades()
        {
            return obj.usp_CboEspecialidades().ToList();
        }

        public List<uspCmbVigenciasResult> CmbVigencias()
        {
            return obj.uspCmbVigencias().ToList();
        }

        // Fin de Metodos para llenado de Combos

        // Métodos para Recuperación de Datos
        public List<usp_BuscarHClinicaResult> BuscarHClinica(string Pac_HC)
        {
            return obj.usp_BuscarHClinica(Pac_HC).ToList();
        }

        public List<usp_BuscarPacienteResult> BuscarPacienteResult(string Pac_NomComp)
        {
            return obj.usp_BuscarPaciente(Pac_NomComp).ToList();
        }

        public List<usp_BuscarTarifaResult> BuscarTarifaResult(string Tar_Cod)
        {
            return obj.usp_BuscarTarifa(Tar_Cod).ToList();
        }

        // Fin de métodos para Recuperación de Datos

        public List<usp_DgvSintomasListResult> DgvSintomasVer(int FichaNum)
        {
            return obj.usp_DgvSintomasList(FichaNum).ToList();
        }

        public List<usp_DgvPartesCuerpoResult> DgvPartesCuerpo(int FichaNum, int SerId)
        {
            return obj.usp_DgvPartesCuerpo(FichaNum, SerId).ToList();
        }

        public List<usp_DgvSegmentosResult> DgvSegmentos(int FichaNum, int PartesCuerpoId)
        {
            return obj.usp_DgvSegmentos(FichaNum, PartesCuerpoId).ToList();
        }
        
        public List<usp_DgvMedicamentosResult> DgvComerciales(int PrA_Id)
        {
            return obj.usp_DgvMedicamentos(PrA_Id).ToList();
        }

        public List<usp_TCambioResult> TipoCambio()
        {
            return obj.usp_TCambio().ToList();
        }

        public List<usp_TDocumentoRecResult> TipoDocumento(string TipoDocumentoId)
        {
            return obj.usp_TDocumentoRec(TipoDocumentoId).ToList();
        }

        // Metodos para Profesiones
        public List<usp_ProfesionesRecResult> ProfesionDatos(int PrfId)
        {
            return obj.usp_ProfesionesRec(PrfId).ToList();
        }

        public List<usp_ProfesionesLisResult> ProfesionListar()
        {
            return obj.usp_ProfesionesLis().ToList();
        }

        public void ProfesionesAdicionar(Profesiones reg)
        {
            try 
            {
                obj.usp_ProfesionesAdd(reg.Prf_Nombre, reg.Estado, reg.UsuCrea, reg.UsuModi); 
            }
            catch (Exception ex) { throw ex; }
        }

        public void ProfesionesActualizar(Profesiones reg)
        {
            try
            {
                obj.usp_ProfesionesUpd(reg.Prf_Id,reg.Prf_Nombre,reg.UsuModi);
            }
            catch (Exception ex) { throw ex; }
        }

        public int ProfesionesAnular(Profesiones reg)
        {
            try
            {
                int x = obj.usp_ProfesionesA_D(reg.Prf_Id, reg.Estado, reg.UsuModi);
                if (x > 0)
                { return x; }
                else
                { return -1; }
            }
            catch (Exception ex) { throw ex; }
        }
        // Fin de Metodos para Profesiones


        // Metodos para Grados de Instrucción
        public List<usp_GradosInstruccionRecResult> GradosDatos(int GrI_Id)
        {
            return obj.usp_GradosInstruccionRec(GrI_Id).ToList();
        }

        public List<usp_GradosInstruccionLisResult> GradosListar()
        {
            return obj.usp_GradosInstruccionLis().ToList();
        }

        public void GradosAdicionar(GradosInstruccion reg)
        {
            try
            {
                obj.usp_GradosInsruccionAdd(reg.GrI_Nombre, reg.Estado, reg.UsuCrea, reg.UsuModi);
            }
            catch (Exception ex) { throw ex; }
        }

        public void GradosActualizar(GradosInstruccion reg)
        {
            try
            {
                obj.usp_GradosInstruccionUpd(reg.GrI_Id, reg.GrI_Nombre, reg.UsuModi);
            }
            catch (Exception ex) { throw ex; }
        }

        public int GradosAnular(GradosInstruccion reg)
        {
            try
            {
                int x = obj.usp_GradosInstruccionA_D(reg.GrI_Id, reg.Estado, reg.UsuModi);
                if (x > 0)
                { return x; }
                else
                { return -1; }
            }
            catch (Exception ex) { throw ex; }
        }
        // Fin de Metodos para Grados de Instrucción

        // Metodos para Ubigeo
        public List<usp_UbigeoRecResult> UbigeoDatos(string Ubi_Cod)
        {
            return obj.usp_UbigeoRec(Ubi_Cod).ToList();
        }

        public List<usp_UbigeoLisResult> UbigeoListar()
        {
            return obj.usp_UbigeoLis().ToList();
        }

        public void UbigeoAdicionar(Ubigeo reg)
        {
            try
            {
                obj.usp_UbigeoAdd(reg.Ubi_Cod, reg.Ubi_Nombre, reg.Estado, reg.UsuCrea, reg.UsuModi);
            }
            catch (Exception ex) { throw ex; }
        }

        public void UbigeoActualizar(Ubigeo reg)
        {
            try
            {
                obj.usp_UbigeoUpd(reg.Ubi_Cod, reg.Ubi_Nombre, reg.UsuModi);
            }
            catch (Exception ex) { throw ex; }
        }

        public int UbigeoAnular(Ubigeo reg)
        {
            try
            {
                int x = obj.usp_UbigeoA_D(reg.Ubi_Cod, reg.Estado, reg.UsuModi);
                if (x > 0)
                { return x; }
                else
                { return -1; }
            }
            catch (Exception ex) { throw ex; }
        }
        // Fin de Metodos para Ubigeo

        // Metodos para Tipos de Documento
        public List<usp_DocIdentidadRecResult> DocIdentidadDatos(int Did_Id)
        {
            return obj.usp_DocIdentidadRec(Did_Id).ToList();
        }

        public List<usp_DocIdentidadLisResult> DocIdentidadListar()
        {
            return obj.usp_DocIdentidadLis().ToList();
        }

        public void DocIdentidadAdicionar(DocIdentidad reg)
        {
            try
            {
                obj.usp_DocIdentidadAdd(reg.Did_Nombre, reg.Estado, reg.UsuCrea, reg.UsuModi);
            }
            catch (Exception ex) { throw ex; }
        }

        public void DocIdentidadActualizar(DocIdentidad reg)
        {
            try
            {
                obj.usp_DocIdentidadUpd(reg.Did_Id, reg.Did_Nombre, reg.UsuModi);
            }
            catch (Exception ex) { throw ex; }
        }

        public int DocIdentidadAnular(DocIdentidad reg)
        {
            try
            {
                int x = obj.usp_DocIdentidadA_D(reg.Did_Id, reg.Estado, reg.UsuModi);
                if (x > 0)
                { return x; }
                else
                { return -1; }
            }
            catch (Exception ex) { throw ex; }
        }
        // Fin de Metodos para Tipos de Documento

        // Metodos para Especialidades Médicas
        public List<usp_EspecialidadesRecResult> EspecialidadesDatos(int Esp_Id)
        {
            return obj.usp_EspecialidadesRec(Esp_Id).ToList();
        }

        public List<usp_EspecialidadesLisResult> EspecialidadesListar()
        {
            return obj.usp_EspecialidadesLis().ToList();
        }

        public void EspecialidadesAdicionar(Especialidades reg)
        {
            try
            {
                obj.usp_EspecialidadesAdd(reg.Esp_Nombre, reg.Estado, reg.UsuCrea, reg.UsuModi);
            }
            catch (Exception ex) { throw ex; }
        }

        public void EspecialidadesActualizar(Especialidades reg)
        {
            try
            {
                obj.usp_EspecialidadesUpd(reg.Esp_Id, reg.Esp_Nombre, reg.UsuModi);
            }
            catch (Exception ex) { throw ex; }
        }

        public int EspecialidadesAnular(Especialidades reg)
        {
            try
            {
                int x = obj.usp_EspecialidadesA_D(reg.Esp_Id, reg.Estado, reg.UsuModi);
                if (x > 0)
                { return x; }
                else
                { return -1; }
            }
            catch (Exception ex) { throw ex; }
        }
        // Fin de Metodos para Especialidades Médicas

        // Metodos para Tipos de Tarifa
        public List<usp_TiposTarifaRecResult> TiposTarifaDatos(string TTr_Codigo)
        {
            return obj.usp_TiposTarifaRec(TTr_Codigo).ToList();
        }

        public List<usp_TiposTarifaLisResult> TiposTarifaListar()
        {
            return obj.usp_TiposTarifaLis().ToList();
        }

        public void TiposTarifaAdicionar(TiposTarifa reg)
        {
            try
            {
                obj.usp_TiposTarifaAdd(reg.TTr_Codigo, reg.TTr_Nombre, reg.Estado, reg.UsuCrea, reg.UsuModi);
            }
            catch (Exception ex) { throw ex; }
        }

        public void TiposTarifaActualizar(TiposTarifa reg)
        {
            try
            {
                obj.usp_TiposTarifaUpd(reg.TTr_Codigo, reg.TTr_Nombre, reg.UsuModi);
            }
            catch (Exception ex) { throw ex; }
        }

        public int TiposTarifaAnular(TiposTarifa reg)
        {
            try
            {
                int x = obj.usp_TiposTarifaA_D(reg.TTr_Codigo, reg.Estado, reg.UsuModi);
                if (x > 0)
                { return x; }
                else
                { return -1; }
            }
            catch (Exception ex) { throw ex; }
        }
        // Fin de Metodos para Tipos de Tarifa

        // Metodos para Principios Activos
        public List<usp_PrincipiosActivosRecResult> PrincipiosActivosDatos(int PrA_Id)
        {
            return obj.usp_PrincipiosActivosRec(PrA_Id).ToList();
        }

        public List<usp_PrincipiosActivosLisResult> PrincipiosActivosListar()
        {
            return obj.usp_PrincipiosActivosLis().ToList();
        }

        public void PrincipiosActivosAdicionar(PrincipiosActivos reg)
        {
            try
            {
                obj.usp_PrincipiosActivosAdd(reg.PrA_Nombre,reg.PrA_Tipo, reg.PrA_Presentacion,
                    reg.PrA_Dosis,reg.PrA_Interacciones,reg.PrA_Contraindicaciones,reg.Estado, reg.UsuCrea, reg.UsuModi);
            }
            catch (Exception ex) { throw ex; }
        }

        public void PrincipiosActivosActualizar(PrincipiosActivos reg)
        {
            try
            {
                obj.usp_PrincipiosActivosUpd(reg.PrA_Id, reg.PrA_Nombre, reg.PrA_Tipo, reg.PrA_Presentacion,
                    reg.PrA_Dosis,reg.PrA_Interacciones,reg.PrA_Contraindicaciones,reg.UsuModi);
            }
            catch (Exception ex) { throw ex; }
        }

        public int PrincipiosActivosAnular(PrincipiosActivos reg)
        {
            try
            {
                int x = obj.usp_PrincipiosActivosA_D(reg.PrA_Id, reg.Estado, reg.UsuModi);
                if (x > 0)
                { return x; }
                else
                { return -1; }
            }
            catch (Exception ex) { throw ex; }
        }
        // Fin de Metodos para Principios Activos

        // Metodos para Laboratorios
        public List<usp_LaboratoriosCmbResult> LaboratoriosCmb()
        {
            return obj.usp_LaboratoriosCmb().ToList();
        }

        public List<usp_LaboratoriosRecResult> LaboratoriosDatos(int Lab_Id)
        {
            return obj.usp_LaboratoriosRec(Lab_Id).ToList();
        }

        public List<usp_LaboratoriosLisResult> LaboratoriosListar()
        {
            return obj.usp_LaboratoriosLis().ToList();
        }

        public void LaboratoriosAdicionar(Laboratorios reg)
        {
            try
            {
                obj.usp_LaboratoriosAdd(reg.Lab_Nombre, reg.Estado, reg.UsuCrea, reg.UsuModi);
            }
            catch (Exception ex) { throw ex; }
        }

        public void LaboratoriosActualizar(Laboratorios reg)
        {
            try
            {
                obj.usp_LaboratoriosUpd(reg.Lab_Id, reg.Lab_Nombre, reg.UsuModi);
            }
            catch (Exception ex) { throw ex; }
        }

        public int LaboratoriosAnular(Laboratorios reg)
        {
            try
            {
                int x = obj.usp_LaboratoriosA_D(reg.Lab_Id, reg.Estado, reg.UsuModi);
                if (x > 0)
                { return x; }
                else
                { return -1; }
            }
            catch (Exception ex) { throw ex; }
        }
        // Fin de Metodos para Laboratorios

        // Metodos para Clasificación Terapeútica
        public List<usp_ClasifTerapCmbResult> ClasifTerapCmb()
        {
            return obj.usp_ClasifTerapCmb().ToList();
        }

        public List<usp_ClasifTerapRecResult> ClasifTerapRec(int ClasTerapId)
        {
            return obj.usp_ClasifTerapRec(ClasTerapId).ToList();
        }

        public List<usp_ClasifTerapLisResult> ClasifterapLis()
        {
            return obj.usp_ClasifTerapLis().ToList();
        }

        public void ClasifTerapAdd(ClasificacionTerapeutica reg)
        {
            try
            {
                obj.usp_ClasifTerapIns(reg.ClasTerapId, reg.ClasTerapNombre, reg.Estado, reg.UsuCrea, reg.UsuModi);
            }
            catch (Exception ex) { throw ex; }
        }

        public void usp_ClasifTerapUpd(ClasificacionTerapeutica reg)
        {
            try
            {
                obj.usp_ClasifTerapUpd(reg.ClasTerapId, reg.ClasTerapNombre, reg.Estado, reg.UsuCrea, reg.UsuModi);
            }
            catch (Exception ex) { throw ex; }
        }
        // Fin de Metodos para Clasificación Terapeútica

        // Metodos para Formas Farmaceuticas
        public List<usp_PresentacionCmbResult> PresentacionCmb()
        {
            return obj.usp_PresentacionCmb().ToList();
        }

        public List<usp_PresentacionRecResult> PresentacionRec(int Prs_Id)
        {
            return obj.usp_PresentacionRec(Prs_Id).ToList();
        }

        public List<usp_PresentacionLisResult> PresentacionLis()
        {
            return obj.usp_PresentacionLis().ToList();
        }

        public void PresentacionAdd(Presentaciones reg)
        {
            try
            {
                obj.usp_PresentacionIns(reg.Prs_Id, reg.Prs_Nombre, reg.Estado, reg.UsuCrea, reg.UsuModi);
            }
            catch (Exception ex) { throw ex; }
        }

        public void usp_PresentacionUpd(Presentaciones reg)
        {
            try
            {
                obj.usp_PresentacionUpd(reg.Prs_Id, reg.Prs_Nombre, reg.Estado, reg.UsuCrea, reg.UsuModi);
            }
            catch (Exception ex) { throw ex; }
        }
        // Fin de Metodos para Formas Farmacológicas

        
        // Metodos para Pacientes
        public List<usp_PacientesRecResult> PacientesDatos(string Pac_HC)
        {
            return obj.usp_PacientesRec(Pac_HC).ToList();
        }

        public List<usp_PacientesLisResult> PacientesListar()
        {
            return obj.usp_PacientesLis().ToList();
        }

        public void PacientesAdicionar(Pacientes reg)
        {
            try
            {
                obj.usp_PacientesAdd(reg.Pac_HC, reg.Pac_ApPat, reg.Pac_ApMat, reg.Pac_Nombres, reg.Pac_NomComp,
                    reg.Pac_Sexo, reg.Pac_LugNacId, reg.Pac_FecNac, reg.Pac_DocIdentId, reg.Pac_DocIdentNum,
                    reg.Pac_GpoSang,reg.Pac_Direccion, reg.Pac_DistritoId, reg.Pac_Telefonos, reg.Pac_Email, 
                    reg.Pac_ProfesionId, reg.Pac_InstruccionId, reg.Pac_PersResp, reg.Pac_DirecResp, reg.Pac_DistRespId,
                    reg.Pac_TelefResp,reg.Pac_EmailResp,reg.Pac_NombreFact, reg.Pac_DirecFact, reg.Pac_DistFactId,
                    reg.Pac_RUCFact, reg.Pac_InfAdicional, reg.Estado, reg.UsuCrea, reg.UsuModi);
            }
            catch (Exception ex) { throw ex; }
        }

        public void PacientesActualizar(Pacientes reg)
        {
            try
            {
                obj.usp_PacientesUpd(reg.Pac_HC, reg.Pac_ApPat, reg.Pac_ApMat, reg.Pac_Nombres, reg.Pac_NomComp,
                    reg.Pac_Sexo,reg.Pac_LugNacId, reg.Pac_FecNac, reg.Pac_DocIdentId, reg.Pac_DocIdentNum, reg.Pac_GpoSang,
                    reg.Pac_Direccion, reg.Pac_DistritoId, reg.Pac_Telefonos, reg.Pac_Email, reg.Pac_ProfesionId,
                    reg.Pac_InstruccionId, reg.Pac_PersResp, reg.Pac_DirecResp, reg.Pac_DistRespId, reg.Pac_TelefResp,
                    reg.Pac_EmailResp, reg.Pac_NombreFact, reg.Pac_DirecFact, reg.Pac_DistFactId, reg.Pac_RUCFact, reg.Pac_InfAdicional, reg.UsuModi);
            }
            catch (Exception ex) { throw ex; }
        }

        public int Pacientes_A_D(Pacientes reg)
        {
            try
            {
                int x = obj.usp_PacientesA_D(reg.Pac_HC, reg.Estado, reg.UsuModi);
                if (x > 0)
                { return x; }
                else
                { return -1; }
            }
            catch (Exception ex) { throw ex; }
        }
        // Fin de Metodos para Pacientes
        
        // Metodos para Ficha Medica
        public List<usp_FMedicaCabeceraResult> FMedicaCabecera(string Pac_HC)
        {
            return obj.usp_FMedicaCabecera(Pac_HC).ToList();
        }

        public List<usp_FMedicaProcesoResult> FMedicaProceso(int FichaMedicaId)
        {
            return obj.usp_FMedicaProceso(FichaMedicaId).ToList();
        }

        public void FMedicaAdicionar(FichaMedica reg) 
        {
            try
            {
                obj.usp_FichaMedicaAdd(reg.Pac_TipoAtencion,reg.Pac_HC,reg.Pac_Edad,reg.GrI_Id,reg.ProfesionId,reg.FM_MotivoConsulta,
                reg.FM_FechaUltMenst,reg.FM_Alcohol,reg.FM_Tabaco,reg.FM_Drogas,reg.FM_OtrosHabNoc,reg.FM_ObservHabNoc,reg.FM_PAMax,reg.FM_PAMin,
                reg.FM_Pulso,reg.FM_Temp,reg.FM_Peso,reg.FM_Talla,reg.FM_Lucido,reg.FM_OTiempo,reg.FM_OEspacio,
                reg.FM_OPersona,reg.FM_EstGeneral,reg.FM_EstNutricion,reg.FM_EstHidratacion,reg.FM_ProxCita,reg.FM_Recomend,
                reg.FM_ObservGenerales,reg.FM_Estado,reg.FM_Servicio,reg.FM_MedCod);
            }
            catch{}
        }

        public void FMedicaActualizar(FichaMedica reg)
        {
            try
            {
                obj.usp_FichaMedicaUpd(reg.FichaMedicaId,reg.Pac_TipoAtencion, reg.Pac_HC, reg.Pac_Edad, reg.GrI_Id, reg.ProfesionId, reg.FM_MotivoConsulta,
                reg.FM_FechaUltMenst, reg.FM_Alcohol, reg.FM_Tabaco, reg.FM_Drogas, reg.FM_OtrosHabNoc, reg.FM_ObservHabNoc, reg.FM_PAMax, reg.FM_PAMin,
                reg.FM_Pulso, reg.FM_Temp, reg.FM_Peso, reg.FM_Talla, reg.FM_Lucido, reg.FM_OTiempo, reg.FM_OEspacio,
                reg.FM_OPersona, reg.FM_EstGeneral, reg.FM_EstNutricion, reg.FM_EstHidratacion, reg.FM_ProxCita, reg.FM_Recomend,
                reg.FM_ObservGenerales, reg.FM_Estado);
            }
            catch { }
        }

        public void FMedicaActCitas(Citas reg)
        {
            try
            {
                obj.usp_ActCitas(reg.CitaId, reg.FichaMedicaId, reg.FM_Estado);
            }
            catch { }
        }

        public List<usp_CboPrincActivosFAResult> CboPrincActivoFA()
        {
            return obj.usp_CboPrincActivosFA().ToList();
        }

        public List<usp_CboLaboratoriosFAResult> CboLaboratoriosFA()
        {
            return obj.usp_CboLaboratoriosFA().ToList();
        }

        public List<usp_InfPrincActivosFAResult> InfPrincActivoFA(int PrA_Id)
        {
            return obj.usp_InfPrincActivosFA(PrA_Id).ToList();
        }

        public List<usp_InfProductosResult> InfProductos(int Prd_Id)
        {
            return obj.usp_InfProductos(Prd_Id).ToList();
        }

        public List<usp_CboMedicamentosResult> CboMedicamentos(string PrdDescripcion)
        {
            return obj.usp_CboMedicamentos(PrdDescripcion).ToList();
        }

        //Metodos para FM_Prescripcion
        public List<usp_FMPrescripcionLisResult> DgvPrescripcion(int FichaMedicaId)
        {
            return obj.usp_FMPrescripcionLis(FichaMedicaId).ToList();
        }

        public void FM_PrescripcionAdd(FM_Prescripcion reg)
        {
            try
            {
                obj.usp_FMPrescripcionAdd(reg.FichaMedicaId, reg.RpItem, reg.Prd_Id, reg.RpNomComercial, reg.RpIndicaciones, reg.RpVigenciaId, reg.RpCaduca);
            }
            catch { }
        }

        public void FM_PrescripcionUpd(FM_Prescripcion reg)
        {
            try
            {
                obj.usp_FMPrescripcionUpd(reg.FichaMedicaId, reg.RpItem, reg.Prd_Id, reg.RpIndicaciones, reg.RpVigenciaId, reg.RpCaduca);
            }
            catch { }
        }

        public int FM_PrescripcionDel(FM_Prescripcion reg)
        {
            try
            {
                int x = obj.usp_FMPrescripcionDel(reg.FichaMedicaId, reg.RpItem, reg.Prd_Id);
                if (x > 0)
                { return x; }
                else
                { return -1; }
            }
            catch (Exception ex) { throw ex; }
        }

        //********************************************
        public List<usp_AntecPatologicosRecResult> AntecPatologicosDatos(int FichaMedica, int PatologiaId)
        {
            return obj.usp_AntecPatologicosRec(FichaMedica, PatologiaId).ToList();
        }

        public List<usp_AntecPatologicosLisResult> DgvAntecPatologicos(string Pac_HC)
        {
            return obj.usp_AntecPatologicosLis(Pac_HC).ToList();
        }

        public void FM_AntecPatologicosAdd(FM_AntecPatologicos reg)
        {
            try
            {
                obj.usp_AntecPatologicosAdd(reg.FichaMedicaId, reg.Pac_HC, reg.PatologiaId, reg.PatologiaObserv);
            }
            catch { }
        }

        public void FM_AntecPatologicosUpd(FM_AntecPatologicos reg)
        {
            try
            {
                obj.usp_AntecPatologicosUpd(reg.FichaMedicaId, reg.Pac_HC, reg.PatologiaId, reg.PatologiaObserv);
            }
            catch { }
        }

        public int FM_AntecPatologicosDel(FM_AntecPatologicos reg)
        {
            try
            {
                int x = obj.usp_AntecPatologicosDel(reg.FichaMedicaId, reg.PatologiaId);
                if (x > 0)
                { return x; }
                else
                { return -1; }
            }
            catch (Exception ex) { throw ex; }
        }

        //Ficha Medica Cirugia
        public List<usp_AntecQuiurgLisResult> DgvAntecQuirurgicos(string Pac_HC)
        {
            return obj.usp_AntecQuiurgLis(Pac_HC).ToList();
        }

        public void FM_AntecQuirurgicosAdd(FM_AntecCirugias reg)
        {
            try
            {
                obj.usp_AntecQuiurgAdd(reg.FichaMedicaId, reg.Pac_HC, reg.CirugiaId,reg.CirugiaObserv);
            }
            catch { }
        }

        public void FM_AntecQuirurgicosUpd(FM_AntecCirugias reg)
        {
            try
            {
                obj.usp_AntecQuiurgUpd(reg.FichaMedicaId, reg.Pac_HC, reg.CirugiaId, reg.CirugiaObserv);
            }
            catch { }
        }

        public int FM_AntecQuirurgicosDel(FM_AntecCirugias reg)
        {
            try
            {
                int x = obj.usp_AntecQuiurgDel(reg.FichaMedicaId, reg.CirugiaId);
                if (x > 0)
                { return x; }
                else
                { return -1; }
            }
            catch (Exception ex) { throw ex; }
        }
 
        //

        //Ficha Medica Antecedentes Alergicos
        public List<usp_AntecAlergicosLisResult> DgvAntecAlergicos(string Pac_HC)
        {
            return obj.usp_AntecAlergicosLis(Pac_HC).ToList();
        }

        public void FM_AntecAlergicosAdd(FM_AntecAlergias reg)
        {
            try
            {
                obj.usp_AntecAlergicosAdd(reg.FichaMedicaId, reg.Pac_HC, reg.AlergiaId,reg.AlergiaObserv);
            }
            catch { }
        }

        public void FM_AntecAlergicosUpd(FM_AntecAlergias reg)
        {
            try
            {
                obj.usp_AntecAlergicosUpd(reg.FichaMedicaId, reg.Pac_HC, reg.AlergiaId,reg.AlergiaObserv);
            }
            catch { }
        }

        public int FM_AntecAlergicosDel(FM_AntecAlergias reg)
        {
            try
            {
                int x = obj.usp_AntecAlergicosDel(reg.FichaMedicaId, reg.AlergiaId);
                if (x > 0)
                { return x; }
                else
                { return -1; }
            }
            catch (Exception ex) { throw ex; }
        }

        //

        //Ficha Medica Antecedentes Familiares
        public List<usp_AntecFamiliaresLisResult> DgvAntecFamiliares(string Pac_HC)
        {
            return obj.usp_AntecFamiliaresLis(Pac_HC).ToList();
        }

        public void FM_AntecFamiliaresAdd(FM_AntecFamiliares reg)
        {
            try
            {
                obj.usp_AntecFamiliaresAdd(reg.FichaMedicaId, reg.Pac_HC, reg.FamiliarId, reg.PatologiaId, reg.AntecFamObserv);
            }
            catch { }
        }

        public void FM_AntecFamiliaresUpd(FM_AntecFamiliares reg)
        {
            try
            {
                obj.usp_AntecFamiliaresUpd(reg.FichaMedicaId, reg.Pac_HC, reg.FamiliarId, reg.PatologiaId, reg.AntecFamObserv);
            }
            catch { }
        }

        public int FM_AntecFamiliaresDel(FM_AntecFamiliares reg)
        {
            try
            {
                int x = obj.usp_AntecFamiliaresDel(reg.FichaMedicaId,reg.FamiliarId,reg.PatologiaId);
                if (x > 0)
                { return x; }
                else
                { return -1; }
            }
            catch (Exception ex) { throw ex; }
        }

        //

        // Métodos para Reserva de Citas
        public void CitasReservar(Citas reg)
        {
            try
            {
                obj.usp_CitasReservar(reg.CitaFecha, reg.CitaHora, reg.CitaDuracion, reg.Pac_HC, reg.Pac_NomComp, reg.Pac_Telefonos, reg.Ser_Id, reg.Con_Id, reg.Med_Id,
                    reg.Tar_Cod, reg.Tar_Moneda, reg.TipoCambio, reg.Tar_Valor, reg.CitaEstado.ToString(), reg.Observacion, reg.UsuCrea, reg.UsuModi);
            }
            catch { }
        }

        public void CitasModificar(Citas reg)
        {
            try
            {
                obj.usp_CitasModificar(reg.CitaId, reg.CitaFecha, reg.CitaHora, reg.CitaDuracion, reg.Pac_HC, reg.Pac_NomComp, reg.Pac_Telefonos, reg.Ser_Id, reg.Con_Id, reg.Med_Id,
                    reg.Tar_Cod, reg.Tar_Moneda, reg.TipoCambio, reg.Tar_Valor, reg.CitaEstado.ToString(), reg.Observacion, reg.UsuCrea, reg.UsuModi);
            }
            catch { }
        }

        public int CitasEstado(Citas reg)
        {
            try
            {
                int x = obj.usp_CitasEstado(reg.CitaId, reg.CitaEstado, reg.UsuModi);
                if (x > 0)
                { return x;  }
                else
                { return -1; }
            }
            catch (Exception ex) { throw ex; }
        }

        public List<usp_CitasRecuperaResult> CitaDatos(short CitaId)
        {
            return obj.usp_CitasRecupera(CitaId).ToList();
        }
        // Fin de métodos para Reserva de Citas

        public List<usp_UsuarioDatResult> BuscaUsuario(string nu, string pw)
        {
            return obj.usp_UsuarioDat(nu, pw).ToList();
        }

        public List<usp_HabilitarModuloResult> Habilitar(int PerfilId, string ModuloIdent)
        {
            return obj.usp_HabilitarModulo(PerfilId, ModuloIdent).ToList();
        }

        public List<usp_IdentificaMedicoResult> IdentificaMedico(int Med_Id)
        {
            return obj.usp_IdentificaMedico(Med_Id).ToList();
        }

        public List<usp_NumHistoriaResult> NumeroHistoria()
        {
            return obj.usp_NumHistoria().ToList();
        }

        public List<usp_FMedicaIdResult> NumeroFicha(string Pac_HC)
        {
            return obj.usp_FMedicaId(Pac_HC).ToList();
        }

        public int FM_SintomasDel(FM_Sintomas reg)
        {
            try
            {
                int x = obj.usp_FM_SintomasDel(reg.FichaMedicaId);
                if (x > 0)
                { return x; }
                else
                { return -1; }
            }
            catch (Exception ex) { throw ex; }
        }

        public void FM_SintomasAdd(FM_Sintomas reg)
        {
            try
            {
                obj.usp_FM_SintomasAdd(reg.FichaMedicaId,reg.SintomaId,reg.SintomaObserv,reg.SintomaSelec);
            }
            catch (Exception ex) { throw ex; }
        }

        public List<usp_AntecDiagAnterioresResult> AntecDiagAnteriores(string Pac_HC)
        {
            return obj.usp_AntecDiagAnteriores(Pac_HC).ToList();
        }

        public List<usp_PartesCuerpoLisResult> PartesCuerpoList(int SerId)
        {
            return obj.usp_PartesCuerpoLis(SerId).ToList();
        }
        //  AQUI ESTA MI PROCEDIMIENTO
        //public List<usp_UsuariosLisResult> ListaUsuariosList(String filtro)
        //{
         //   return obj.usp_UsuariosLis(filtro).ToList();
        //}
        // AQUI TERMINA
        public List<usp_PartesCuerpoRecResult> PartesCuerpoRec(int PartesCuerpoId)
        {
            return obj.usp_PartesCuerpoRec(PartesCuerpoId).ToList();
        }
        
        public void PartesCuerpoAdd(PartesCuerpo reg)
        {
            try
            {
                obj.usp_PartesCuerpoAdd(reg.PartesCuerpoDescripcion, reg.PartesCuerpoSerId, reg.PartesCuerpoOrden,
                    reg.PartesCuerpoImagen, reg.Estado, reg.UsuCrea, reg.UsuModi);
            }
            catch (Exception ex) { throw ex; }
        }

        public void PartesCuerpoUpd(PartesCuerpo reg)
        {
            try
            {
                obj.usp_PartesCuerpoUpd(reg.PartesCuerpoId, reg.PartesCuerpoDescripcion, reg.PartesCuerpoSerId,
                    reg.PartesCuerpoOrden, reg.PartesCuerpoImagen, reg.Estado, reg.UsuModi);
            }
            catch (Exception ex) { throw ex; }
        }

        public List<usp_PartesCuerpoSegmentosLisResult> PartesCuerpoDetalleLis(int PartesCuerpoId)
        {
            return obj.usp_PartesCuerpoSegmentosLis(PartesCuerpoId).ToList();
        }

        public int SegmentosDel(PartesCuerpoSegmentos reg)
        {
            try
            {
                int x = obj.usp_SegmentosDel(reg.PartesCuerpoId);
                if (x > 0)
                { return x; }
                else
                { return -1; }
            }
            catch (Exception ex) { throw ex; }
        }

        public void SegmentosAdd(PartesCuerpoSegmentos reg)
        {
            try
            {
                obj.usp_SegmentosAdd(reg.SegmentoDescripcion,reg.PartesCuerpoId,reg.Estado,reg.UsuCrea,reg.UsuModi);
            }
            catch (Exception ex) { throw ex; }
        }

        public void SegmentosUpd(PartesCuerpoSegmentos reg)
        {
            try
            {
                obj.usp_SegmentosUpd(reg.SegmentoId,reg.SegmentoDescripcion, reg.PartesCuerpoId, reg.Estado, reg.UsuCrea, reg.UsuModi);
            }
            catch (Exception ex) { throw ex; }
        }

        public int FM_PartesCuerpoDel(FM_ExamPreferencial reg)
        {
            try
            {
                int x = obj.usp_FM_PartesCuerpoDel(reg.FichaMedicaId);
                if (x > 0)
                { return x; }
                else
                { return -1; }
            }
            catch (Exception ex) { throw ex; }
        }
      
        public void FM_PartesCuerpoAdd(FM_ExamPreferencial reg)
        {
            try
            {
                obj.usp_FM_PartesCuerpoAdd(reg.FichaMedicaId, reg.ParteCuerpoId, reg.ParteCuerpoObserv, reg.ParteCuerpoSelec);
            }
            catch { }
        }

        public void FM_SegmentosAdd(FM_ExamPreferencialDetalle reg)
        {
            try
            {
                obj.usp_FM_SegmentosAdd(reg.FichaMedicaId, reg.ParteCuerpoId, reg.SegmentoId, reg.SegmentoObserv, reg.SegmentoSelec);
            }
            catch { }
        }

        public int FM_SegmentosDel(FM_ExamPreferencialDetalle reg)
        {
            try
            {
                int x = obj.usp_FM_SegmentosDel(reg.FichaMedicaId, reg.ParteCuerpoId);
                if (x > 0)
                { return x; }
                else
                { return -1; }
            }
            catch (Exception ex) { throw ex; }
        }

        public List<usp_cboAyudaDiagnosticaResult> cboAyudaDiagnostica()
        {
            return obj.usp_cboAyudaDiagnostica().ToList();
        }

        public List<usp_AyudaDiagLisResult> FM_AyudaDiagnosticaLis(string PacHc)
        {
            return obj.usp_AyudaDiagLis(PacHc).ToList();
        }

        public void FM_AyudaDiagnosticaAdd(FM_AyudaDiagnostica reg)
        {
            try
            {
                obj.usp_AyudaDiagAdd(reg.FichaMedicaId, reg.AyudaDiagFecha, reg.AyudaDiagTipo, reg.AyudaDiagInforme);
            }
            catch
            { }
        }

        public void FM_AyudaDiagnosticaUpd(FM_AyudaDiagnostica reg)
        {
            try
            {
                obj.usp_AyudaDiagUpd(reg.AyudaDiagId, reg.AyudaDiagFecha, reg.FichaMedicaId, reg.AyudaDiagTipo, reg.AyudaDiagInforme);
            }
            catch
            { }
        }

        public int FM_AyudaDiagDel(FM_AyudaDiagnostica reg)
        {
            try
            {
                int x = obj.usp_AyudaDiagDel(reg.FichaMedicaId, reg.AyudaDiagId);
                if (x > 0)
                { return x; }
                else
                { return -1; }
            }
            catch (Exception ex) { throw ex; }
        }

        public List<usp_dgvExamenesLisResult> dgvExamenesLis(int FichaNum, int ExamenTipo, int ExamenGrupo)
        { return obj.usp_dgvExamenesLis(FichaNum, ExamenTipo, ExamenGrupo).ToList(); }

        public List<usp_ExamenIdRecResult> usp_ExamenIdRec(string ExamenNombre)
        { return obj.usp_ExamenIdRec(ExamenNombre).ToList(); }

        public int FM_ExamAuxDel(FM_ExamenesAuxiliares reg)
        {
            try
            {
                int x = obj.usp_FM_ExamAuxDel(reg.FichaMedicaId, reg.ExamenTipo, reg.ExamenGrupo);
                if (x > 0)
                { return x; }
                else
                { return -1; }
            }
            catch (Exception ex) { throw ex; }
        }

        public void FM_ExamAuxAdd(FM_ExamenesAuxiliares reg)
        {
            try
            {
                obj.usp_FM_ExamAuxAdd(reg.FichaMedicaId, reg.ExamenId, reg.ExamenTipo, reg.ExamenGrupo, reg.ExamenObserv, reg.ExamenSelec);
            }
            catch
            { }
        }

        public List<usp_TratamientosLisResult> cmbTratamientos()
        { return obj.usp_TratamientosLis().ToList(); }

        public List<usp_dgvTratamientosResult> dgvTratamientos(int FichaNum)
        { return obj.usp_dgvTratamientos(FichaNum).ToList(); }

//***********************
        public int FM_TratamientosDel(FM_Tratamientos reg)
        {
            try
            {
                int x = obj.usp_FM_TratamientosDel(reg.FichaMedicaId, reg.TratamId);
                if (x > 0)
                { return x; }
                else
                { return -1; }
            }
            catch (Exception ex) { throw ex; }
        }

        public void FM_TratamientosAdd(FM_Tratamientos reg)
        {
            try
            {
                obj.usp_FM_TratamientosAdd(reg.FichaMedicaId, reg.TratamId, reg.TratamNumSesiones, reg.TratamObserv);
            }
            catch
            { }
        }

        public void FM_TratamientosUpd(FM_Tratamientos reg)
        {
            try
            {
                obj.usp_FM_TratamientosUpd(reg.FichaMedicaId, reg.TratamId, reg.TratamNumSesiones, reg.TratamObserv);
            }
            catch
            { }
        }
        //*******************************************************************************
        public List<usp_MedicinasGenResult> lstMedicamentosGen(int PrA_Id)
        { return obj.usp_MedicinasGen(PrA_Id).ToList(); }

        public List<usp_MedicinasLabResult> lstMedicamentosLab(int Lab_Cod)
        { return obj.usp_MedicinasLab(Lab_Cod).ToList(); }

        public List<usp_PrescAnterioresResult> dgvPrescripAnteriores(string Pac_HC)
        { return obj.usp_PrescAnteriores(Pac_HC).ToList(); }

        public List<usp_UltimaPrescRecupResult> UltimaPrescripcion(string Pac_HC, int SerId, int MedCod)
        { return obj.usp_UltimaPrescRecup(Pac_HC, SerId, MedCod).ToList(); }

        public List<usp_CboPrincActivosResult> cmbPrincipios(string PrA_Tipo, string PrA_Nombre)
        { return obj.usp_CboPrincActivos(PrA_Tipo, PrA_Nombre).ToList(); }


        //***************************** PREPARADOS ¨******************************************
        public List<usp_PreparadosResult> cmbPreparados()
        { return obj.usp_Preparados().ToList(); }

        public List<usp_dgvPreparadosResult> dgvPreparados(int Prd_Id)
        { return obj.usp_dgvPreparados(Prd_Id).ToList(); }

        public List<usp_FM_PreparadosLisResult> PreparadosLis(int FichaMedicaId, int Prd_Id)
        { return obj.usp_FM_PreparadosLis(FichaMedicaId, Prd_Id).ToList(); }

        public List<usp_RecuperaUndPAResult> RecuperaUndPA(int PrA_Id)
        { return obj.usp_RecuperaUndPA(PrA_Id).ToList(); }

        public int FM_PreparadosDel(FM_Preparados reg)
        {
            try
            {
                int x = obj.usp_FM_PreparadosDel(reg.FichaMedicaId, reg.Prd_Id, reg.PrA_Id);
                if (x > 0)
                { return x; }
                else
                { return -1; }
            }
            catch (Exception ex) { throw ex; }
        }

        public int FM_PreparadosLim(FM_Preparados reg)
        {
            try
            {
                int x = obj.usp_FM_PreparadosLim(reg.FichaMedicaId, reg.Prd_Id);
                if (x > 0)
                { return x; }
                else
                { return -1; }
            }
            catch (Exception ex) { throw ex; }
        }

        public void FM_PreparadosAdd(FM_Preparados reg)
        {
            try
            {
                obj.usp_FM_PreparadosAdd(reg.FichaMedicaId, reg.Prd_Id, reg.PrA_Tipo, reg.PrA_Id, reg.Cantidad, reg.Unidad);
            }
            catch
            { }
        }

        public void FM_PreparadosUpd(FM_Preparados reg)
        {
            try
            {
                obj.usp_FM_PreparadosUpd(reg.FichaMedicaId, reg.Prd_Id, reg.PrA_Tipo, reg.PrA_Id, reg.Cantidad, reg.Unidad);
            }
            catch
            { }
        }

        //********************************* FM_DIAGNOSTICOS ******************************************
        public List<DiagxCodResult> DiagxCod(string Codigo)
        { return obj.DiagxCod(Codigo).ToList(); }

        public List<DiagxNomResult> DiagxNom(string Nombre)
        { return obj.DiagxNom(Nombre).ToList(); }

        public List<RecNomDiagResult> RecupNomDiag(string Codigo)
        { return obj.RecNomDiag(Codigo).ToList(); }

        public List<usp_FM_DiagnosticosLisResult> dgvDiagnosticos(int FichaMedicaId)
        { return obj.usp_FM_DiagnosticosLis(FichaMedicaId).ToList(); }

        public int FM_DiagnosticosDel(FM_Diagnosticos reg)
        {
            try
            {
                int x = obj.usp_FM_DiagnosticosDel(reg.FichaMedicaId, reg.DiagCod);
                if (x > 0)
                { return x; }
                else
                { return -1; }
            }
            catch (Exception ex) { throw ex; }
        }

        public void FM_DiagnosticosAdd(FM_Diagnosticos reg)
        {
            try
            {
                obj.usp_FM_DiagnosticosAdd(reg.FichaMedicaId, reg.DiagCod, reg.DiagObserv, reg.DiagClasificacion, reg.DiagPrincipal);
            }
            catch
            { }
        }

        public void FM_DiagnosticosUpd(FM_Diagnosticos reg)
        {
            try
            {
                obj.usp_FM_DiagnosticosUpd(reg.FichaMedicaId, reg.DiagCod, reg.DiagObserv, reg.DiagClasificacion, reg.DiagPrincipal);
            }
            catch
            { }
        }

        //********************************* Historial de Fichas Méddicas ******************************************
        public List<Hist_ListadoResult> Hist_Listado(string Pac_HC)
        { return obj.Hist_Listado(Pac_HC).ToList(); }

        public List<Hist_FM_MotivConsultaResult> Hist_FMMotivoConsulta(int FichaMedicaId)
        { return obj.Hist_FM_MotivConsulta(FichaMedicaId).ToList(); }

        public List<Hist_FMSintomasResult> Hist_FMSintomas(int FichaMedicaId)
        { return obj.Hist_FMSintomas(FichaMedicaId).ToList(); }

        public List<Hist_FMAntecPatResult> Hist_FMAntecPat(int FichaMedicaId)
        { return obj.Hist_FMAntecPat(FichaMedicaId).ToList(); }

        public List<Hist_FMAntecQuiResult> Hist_FMAntecQui(int FichaMedicaId)
        { return obj.Hist_FMAntecQui(FichaMedicaId).ToList(); }

        public List<Hist_FMAntecAleResult> Hist_FMAntecAle(int FichaMedicaId)
        { return obj.Hist_FMAntecAle(FichaMedicaId).ToList(); }

        public List<Hist_FMAntecFamResult> Hist_FMAntecFam(int FichaMedicaId)
        { return obj.Hist_FMAntecFam(FichaMedicaId).ToList(); }

        public List<Hist_FuncVitalesResult> Hist_FuncVitales(int FichaMedicaId)
        { return obj.Hist_FuncVitales(FichaMedicaId).ToList(); }

        public List<Hist_PartesCuerpoResult> Hist_PartesCuerpo(int FichaMedicaId)
        { return obj.Hist_PartesCuerpo(FichaMedicaId).ToList(); }

        public List<Hist_SegmentosResult> Hist_Segmentos(int FichaMedicaId, int PartesCuerpoId)
        { return obj.Hist_Segmentos(FichaMedicaId, PartesCuerpoId).ToList(); }

        public List<Hist_InfAydDiagnostResult> Hist_InfAydDiagnost(int FichaMedicaId)
        { return obj.Hist_InfAydDiagnost(FichaMedicaId).ToList(); }

        public List<Hist_ExamAuxResult> Hist_ExamAux(int FichaMedicaId, int ExamemTipo)
        { return obj.Hist_ExamAux(FichaMedicaId, ExamemTipo).ToList(); }

        //--------------------------------------------------------//
        public List<usp_CajaSecuenciaResult> CajaSecuencia()
        { return obj.usp_CajaSecuencia().ToList(); }

        public void CajaCabeceraAdd(Caja_Cabecera reg)
        {
            try
            {
                obj.usp_CajaCabeceraIns(reg.CajaOrigen, reg.CajaNumReferencia, reg.TDocFact, reg.CajaSerie,
                                        reg.CajaCorrelativo, reg.CajaFecha, reg.Pac_HC, reg.CajaClienteNombre,
                                        reg.CajaClienteRUC, reg.CajaClienteDireccion, reg.CajaClienteDNI,
                                        reg.CajaTotal, reg.CajaIGV, reg.CajaEstado, reg.UsuCrea, reg.UsuModi);
            }
            catch
            { }
        }

        public void CajaDetalleAdd(Caja_Detalle reg)
        {
            try
            {
                obj.usp_CajaDetalleIns(reg.CajaId, reg.CajaItem, reg.CajaTipoItem, reg.CajaCodigo, reg.CajaTipoTarifa,
                                       reg.CajaItemPrecioSol, reg.CajaItemPrecioDol, reg.CajaItemIgv, reg.CajaItemCantidad,
                                       reg.CajaItemPrecioFinal, reg.CajaItemTotal, reg.UsuCrea, reg.UsuModi);
            }
            catch
            { }
        }

        //******************************************
        public void ProductosAdd(Productos reg)
        {
            try
            {
                obj.usp_ProductosIns(reg.Prd_Id, reg.Prd_Descripcion, reg.TPr_Cod, reg.PrA_Cod, reg.Lab_Cod, reg.ClasTerapId,
                                     reg.Concentracion, reg.Prs_Id, reg.prd_Venta, reg.Prd_IGV, reg.Prd_VVF, reg.Prd_PVF, reg.Prd_PVP,
                                     reg.Prd_CCompra, reg.Prd_CPromedio, reg.Prd_DctoC1, reg.Prd_DctoC2, reg.Prd_DctoC3,
                                     reg.Prd_DctoC4, reg.Prd_Utilidad, reg.Estado, reg.UsuCrea, reg.UsuModi);
            }
            catch
            { }
        }

        public void ProductosUpd(Productos reg)
        {
            try
            {
                obj.usp_ProductosUpd(reg.Prd_Id, reg.Prd_Descripcion, reg.TPr_Cod, reg.PrA_Cod, reg.Lab_Cod, reg.ClasTerapId,
                                     reg.Concentracion, reg.Prs_Id, reg.prd_Venta, reg.Prd_IGV, reg.Prd_VVF, reg.Prd_PVF, reg.Prd_PVP,
                                     reg.Prd_CCompra, reg.Prd_CPromedio, reg.Prd_DctoC1, reg.Prd_DctoC2, reg.Prd_DctoC3,
                                     reg.Prd_DctoC4, reg.Prd_Utilidad, reg.Estado, reg.UsuCrea, reg.UsuModi);
            }
            catch
            { }
        }

        public List<usp_ProductosLisResult> ProductosLis(string Prd_Descripcion)
        { return obj.usp_ProductosLis(Prd_Descripcion).ToList(); }

        public List<usp_ProductosVenResult> ProductosVen(string Prd_Descripcion)
        { return obj.usp_ProductosVen(Prd_Descripcion).ToList(); }

        public List<usp_ProductosRecResult> ProductosRec(int Prd_Id)
        { return obj.usp_ProductosRec(Prd_Id).ToList(); }

        public List<usp_ProductosIdResult> ProductosId()
        { return obj.usp_ProductosId().ToList(); }

        //Metodos para Registro de Fórmulas Magistrales
        public List<usp_FormulaLisResult> FormulaLis(int Prd_Id)
        {
            return obj.usp_FormulaLis(Prd_Id).ToList();
        }

        public void FormulaAdd(Preparados reg)
        {
            try
            {
                obj.usp_FormulaIns(reg.Prd_Id, reg.PrA_Tipo, reg.PrA_Id, reg.Cantidad, reg.Unidad);
            }
            catch { }
        }

        public void FormulaUpd(Preparados reg)
        {
            try
            {
                obj.usp_FormulaUpd(reg.Prd_Id, reg.PrA_Tipo, reg.PrA_Id, reg.Cantidad, reg.Unidad);
            }
            catch { }
        }

        public int FormulaDel(Preparados reg)
        {
            try
            {
                int x = obj.usp_FormulaDel(reg.Prd_Id, reg.PrA_Id);
                if (x > 0)
                { return x; }
                else
                { return -1; }
            }
            catch (Exception ex) { throw ex; }
        }

        //Metodos para Productos_x_Almacen
        public List<usp_ProdAlmacenLisResult> ProdAlmacenLis(int Prd_Id)
        { return obj.usp_ProdAlmacenLis(Prd_Id).ToList(); }

        public void ProdAlmacenIns(Productos_Almacen reg)
        {
            try
            {
                obj.usp_ProdAlmacenIns(reg.Prd_Id, reg.Alm_Id);
            }
            catch { }
        }

        public int ProdAlmacenDel(Productos_Almacen reg)
        {
            try
            {
                int x = obj.usp_ProdAlmacenDel(reg.Prd_Id, reg.Alm_Id);
                if (x > 0)
                { return x; }
                else
                { return -1; }
            }
            catch (Exception ex) { throw ex; }
        }

        //Metodos para Almacenes
        public List<usp_AlmacenesCmbResult> AlmacenesCmb()
        {
            return obj.usp_AlmacenesCmb().ToList();
        }

        //Varios
        public List<usp_RecupPra_IdResult> RecupPra_Id(string PrA_Nombre)
        { return obj.usp_RecupPra_Id(PrA_Nombre).ToList(); }

        public List<usp_RecupPedIdResult> RecupPedId()
        { return obj.usp_RecupPedId().ToList(); }

        public List<usp_PacientesBusResult> BuscarPaciente(string Pac_NomComp)
        { return obj.usp_PacientesBus(Pac_NomComp).ToList(); }

        public List<uspDiasVigenciaResult> DiasVigencia(int VigId)
        { return obj.uspDiasVigencia(VigId).ToList(); }

        //Ventas Farmacia
        //Cabecera
        public void FmciaCabeceraAdd(FarmaciaCabecera reg)
        {
            try
            {
                obj.usp_FciaCabIns(reg.TipoCliente, reg.Pac_HC, reg.ClienteNombre, reg.Total);
            }
            catch { }
        }

        public void FmciaCabeceraUpd(FarmaciaCabecera reg)
        {
            try
            {
                obj.usp_FciaCabUpd(reg.VentaId, reg.TipoCliente, reg.Pac_HC, reg.ClienteNombre, reg.Total, reg.Estado);
            }
            catch { }
        }

        //Detalle
        public void FmciaDetalleAdd(FarmaciaDetalle reg)
        {
            try
            {
                obj.usp_FciaDetIns(reg.VentaId, reg.Item, reg.Prd_Id, reg.Prd_PVP, reg.Prd_PVPFinal, reg.Cantidad, reg.TotalItem);
            }
            catch { }
        }

        public void FmciaDetalleUpd(FarmaciaDetalle reg)
        {
            try
            {
                obj.usp_FciaDetUpd(reg.VentaId, reg.Item, reg.Prd_Id, reg.Prd_PVP, reg.Prd_PVPFinal, reg.Cantidad, reg.TotalItem);
            }
            catch { }
        }

        public int FmciaDetalleDel(FarmaciaDetalle reg)
        {
            try
            {
                int x = obj.usp_FciaDetDel(reg.VentaId, reg.Item, reg.Prd_Id);
                if (x > 0)
                { return x; }
                else
                { return -1; }
            }
            catch (Exception ex) { throw ex; }
        }

        public List<usp_FciaDetLisResult> FmciaDetLis()
        { return obj.usp_FciaDetLis().ToList(); }

        public List<usp_FciaDetRecResult> FmciaDetRec(int VentaId, int Item, int Prd_Id)
        { return obj.usp_FciaDetRec(VentaId, Item, Prd_Id).ToList(); }

        // Mantenimiento de Médicos
        public List<usp_MedicosRecResult> MedicosDatos(int Med_Id)
        {
            return obj.usp_MedicosRec(Med_Id).ToList();
        }

        public List<usp_MedicosLisResult> MedicosListar()
        {
            return obj.usp_MedicosLis().ToList();
        }

        public void MedicosAdicionar(Medicos reg)
        {
            try 
            {
                obj.usp_MedicosAdd(reg.Med_ApPat, reg.Med_ApMat, reg.Med_Nombre, reg.Med_NomComp,
                                   reg.Med_NumCol, reg.Med_EspecId, reg.Med_RNE, reg.Med_Direccion,
                                   reg.Med_Distrito, reg.Med_Telefonos, reg.Med_Email, reg.Med_FIngreso,
                                   reg.Estado, reg.UsuCrea, reg.UsuModi); 
            }
            catch (Exception ex) { throw ex; }
        }

        public void MedicosActualizar(Medicos reg)
        {
            try
            {
                obj.usp_MedicosUpd(reg.Med_Id, reg.Med_ApPat, reg.Med_ApMat, reg.Med_Nombre,
                                   reg.Med_NomComp, reg.Med_NumCol, reg.Med_EspecId, reg.Med_RNE,
                                   reg.Med_Direccion, reg.Med_Distrito, reg.Med_Telefonos,
                                   reg.Med_Email, reg.Med_FIngreso, reg.Estado, reg.UsuModi);
            }
            catch (Exception ex) { throw ex; }
        }

        public void MedicosEstado(Medicos reg)
        {
            try
            {
                obj.usp_MedicosAD(reg.Med_Id, reg.Estado, reg.UsuModi);
            }
            catch (Exception ex) { throw ex; }
        }

        // Recupera Datos Consulta Anterior
        public List<usp_CA_FichaMedicaIdResult> CA_FichaMEedicaId(string Pac_HC)
        { return obj.usp_CA_FichaMedicaId(Pac_HC).ToList(); }

        public List<usp_CA_DiagnosticosResult> CA_Diagnosticos(int FichaMedicaId)
        { return obj.usp_CA_Diagnosticos(FichaMedicaId).ToList(); }

        public List<usp_CA_PrescripResult> CA_Prescrip(int FichaMedicaId)
        { return obj.usp_CA_Prescrip(FichaMedicaId).ToList(); }

        public List<usp_CA_PlanTrabResult> CA_Plantrab(int FichaMedicaId, int ExamenTipo)
        { return obj.usp_CA_PlanTrab(FichaMedicaId, ExamenTipo).ToList(); }

        public List<usp_CA_ProcedimResult> CA_Procedim(int FichaMedicaId)
        { return obj.usp_CA_Procedim(FichaMedicaId).ToList(); }

        public List<usp_CA_AntePatolResult> CA_AntePatol(int FichaMedicaId)
        { return obj.usp_CA_AntePatol(FichaMedicaId).ToList(); }

        public List<usp_CA_AnteQuirurgResult> CA_AnteQuirurg(int FichaMedicaId)
        { return obj.usp_CA_AnteQuirurg(FichaMedicaId).ToList(); }

        public List<usp_CA_AnteFamiliarResult> CA_AnteFamiliar(int FichaMedicaId)
        { return obj.usp_CA_AnteFamiliar(FichaMedicaId).ToList(); }
    }
}
