//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace pjDermaBelle
{
    using System;
    using System.Collections.Generic;
    
    public partial class Producto
    {
        public short Prd_Id { get; set; }
        public string Prd_Descripcion { get; set; }
        public string TPr_Cod { get; set; }
        public short PrA_Cod { get; set; }
        public short Lab_Cod { get; set; }
        public short ClasTerapId { get; set; }
        public string Concentracion { get; set; }
        public Nullable<short> Prs_Id { get; set; }
        public string Prd_IGV { get; set; }
        public Nullable<decimal> Prd_VVF { get; set; }
        public Nullable<decimal> Prd_PVF { get; set; }
        public Nullable<decimal> Prd_PVP { get; set; }
        public Nullable<decimal> Prd_CCompra { get; set; }
        public Nullable<decimal> Prd_CPromedio { get; set; }
        public Nullable<decimal> Prd_DctoC1 { get; set; }
        public Nullable<decimal> Prd_DctoC2 { get; set; }
        public Nullable<decimal> Prd_DctoC3 { get; set; }
        public Nullable<decimal> Prd_DEcto4 { get; set; }
        public Nullable<decimal> Prd_Utilidad { get; set; }
        public bool Estado { get; set; }
        public System.DateTime FecCrea { get; set; }
        public short UsuCrea { get; set; }
        public System.DateTime FecModi { get; set; }
        public short UsuModi { get; set; }
    }
}
