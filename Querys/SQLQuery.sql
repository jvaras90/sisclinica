/*******************************************************
             Procedures de Mantenimiento
Desarrollado por:	Daniel Risco Casta�eda
Fecha			:	31/12/2014
Observaci�n		:	Creaci�n, Modificaci�n, Anulaci�n
					y Listado de registros
*******************************************************/
/******************************************************
                      Miscelaneos
*******************************************************/
--Procedure para obtener el correlativo
Create Proc usp_Secuencias
@SecId char(3)
As
Begin
	Select Sec_Ultimo From Secuencias
	Where Sec_Id = @SecId
End
Go

--Procedure para alimentar el Combo de Profesiones
Alter Proc usp_CboProfesiones
As
Begin
	Select	Prf_Id, Prf_Nombre
	From	Profesiones
	Where	Estado = '1'
	Order By Prf_Nombre
End
Go

-- Procedure para alimentar el Combo de Grados de Instrucci�n
Alter Proc usp_CboGradosInstruccion
As
Begin
	Select	GrI_Id,GrI_Nombre
	From	GradosInstruccion
	Where	Estado = '1'
	Order By GrI_Nombre
End
Go

-- Procedure para alimentar el Combo de Ubigeo
Alter Proc usp_CboUbigeo
As
Begin
	Select	Ubi_Cod,Ubi_Nombre
	From	Ubigeo
	Where	Estado = '1'
	Order By Ubi_Nombre
End
Go

-- Procedure para alimentar el Combo de Lugar de Nacimiento
Alter Proc usp_CboLugNacimiento
As
Begin
	Select	Ubi_Cod,Ubi_Nombre
	From	Ubigeo
	Where	Estado = '1' And
			RIGHT(Ubi_Cod,5) = '00.00' Or
			Ubi_Cod = '99.99.99'
	Order By Ubi_Nombre
End
Go

-- Procedure para alimentar el Combo de Distritos
Alter Proc usp_CboDistrito
As
Begin
	Select	Ubi_Cod,Ubi_Nombre
	From	Ubigeo
	Where	Estado = '1' And
			Left(Ubi_Cod,5) In ('15.01', '07.01') And
			Right(Ubi_Cod,2) <> '00'
	Union 

	Select	Ubi_Cod,Ubi_Nombre
	From	Ubigeo
	Where	Ubi_Cod = '00.00.00'

	Order By Ubi_Nombre
End
Go

-- Procedure para alimentar el Combo de Antcedentes Patol�gicos
Create Proc usp_CboPatologias
As
Begin
	Select	PatologiaId,PatologiaNombre
	From	Patologias
	Where	Estado = '1' 
	Order By PatologiaNombre
End
Go

-- Procedure para alimentar el Combo de Antecedentes Quir�rgicos
Create Proc usp_CboCirugias
As
Begin
	Select	Qui_Id,Qui_Nombre
	From	Cirugias
	Where	Estado = 1
	Order By Qui_Nombre
End
Go

-- Procedure para alimentar el Combo de Antecedentes Al�rgicos
Create Proc usp_CboAlergias
As
Begin
	Select	AlergiaId,AlergiaNombre
	From	Alergias
	Where	Estado = 1
	And		TipoAlergiaId = 3
	Order By AlergiaNombre
End
Go

-- Procedure para alimentar el Combo de Diagn�sticos
Create Proc usp_CboDiagnosticos
As
Begin
	Select	DiagCod,DiagDescripcion
	From	Diagnosticos
	Where	Estado = 1
	Order By DiagDescripcion
End
Go

-- Procedure para alimentar el Combo de Documentos de Identidad
Alter Proc usp_CboDocIdentidad
As
Begin
	Select	Did_Id, Did_Nombre
	From	DocIdentidad
	Where	Estado = '1'
End
Go

-- Procedure para alimentar el grid de Pacientes Citados
Alter Proc usp_PacientesCitados
As
Begin
	Select	c.CitaHora [Hora], c.Pac_HC [Historia], p.Pac_NomComp [Paciente], 
			Case c.AntConsultaTipoAtencion
				When '1' Then 'Nuevo'
				When '2' Then 'Continuador'
				When '3' Then 'Reingreso'
			End As AtendidoComo,
			c.AntConsultaFecha [Fecha], s.Ser_Nombre [Servicio], 
			m.Med_NomComp [Atendido Por],
			c.AntConsultaDiagCod [CIE], d.DiagDescripcion [Diagn�stico],
			Case c.FM_Estado
				When '1' Then 'Cerrada'
				When '2' Then 'En Proceso'
				Else 'Por Atender'
			End As EstadoFicha
	From	Citas c, Pacientes p, Servicios s, Diagnosticos d, Medicos m
	Where	Convert(char(10),c.CitaFecha,103) = Convert(char(10),Getdate(),103)
	And		c.Pac_HC = p.Pac_HC
	And		c.Ser_Id = s.Ser_Id
	And		c.AntConsultaDiagCod = d.DiagCod
End
Go

-- Procedure para recuperar la cabecera del paciente a ser atendido
Alter Proc usp_FMedicaCabecera
@Pac_HC varchar(10)
As
Begin
	Select	p.Pac_HC, p.Pac_NomComp, d.Did_Nombre + ' ' + p.Pac_DocIdentNum As DocIdent,
			i.GrI_Nombre, o.Prf_Nombre,p.Pac_GpoSang,
			Case p.Pac_Sexo
				When 'F' Then 'Femenino'
				Else 'Masculino'
			End As Pac_Sexo,
			YEAR(getdate()) - YEAR(p.Pac_FecNac) As Pac_Edad,
			(Select Ubi_Nombre From Ubigeo 
			 Where Ubi_Cod = p.Pac_LugNacId) As Pac_LugNacim,
			(Select Ubi_Nombre From Ubigeo 
			 Where Ubi_Cod = p.Pac_DistritoId) As Pac_Proced
	From	Pacientes p, DocIdentidad d, GradosInstruccion i, Profesiones o
	Where	p.Pac_HC = @Pac_HC
	And		p.Pac_DocIdentId = d.Did_Id
	And		p.Pac_InstruccionId = i.GrI_Id
	And		p.Pac_ProfesionId = o.Prf_Id
End
Go

-- Procedure para alimentar el grid de S�ntomas
Alter Proc usp_DgvSintomasAdd
@FichaNum int
AS
Begin
	Insert Into FM_Sintomas 
	Select @FichaNum, SintomaId, Null From Sintomas Where Estado = 1
End
Go

Alter Proc usp_DgvSintomasList
@FichaNum int
AS
Begin
	Select s.SintomaNombre [S�ntoma],f.SintomaObserv [Observaci�n]
	From Sintomas s Left Join FM_Sintomas f
    On  s.SintomaId = f.SintomaId And F.FichaMedicaId = @FichaNum
	Order By s.SintomaNombre
End
Go

Create Proc usp_DgvSintomasLoad
AS
Begin
	Select SintomaNombre [S�ntoma], '' [Observaci�n] From Sintomas 
	Where  Estado = 1
	And    SintomaId <> 1
End
Go

/*************************************************************************/

/******************************************************
               Mantenimiento de Profesiones
*******************************************************/
--Creaci�n
Alter Proc usp_ProfesionesAdd
@Prf_Id int, @Prf_Nombre varchar(50),
@Estado bit, @UsuCrea int, @UsuMod int
As
Begin
	Insert Profesiones
	(Prf_Id, Prf_Nombre,Estado,UsuCrea,FecCrea,UsuMod,FecMod)
	Values
	(@Prf_Id, @Prf_Nombre,@Estado,@UsuCrea,GETDATE(),@UsuMod,GETDATE())
End
Go

-- Modificaci�n
Alter Proc usp_ProfesionesUpd
@Prf_Id int, @Prf_Nombre varchar(50),
@UsuMod int, @FecMod Datetime
As
Begin
	Update Profesiones
	Set	Prf_Nombre = @Prf_Nombre,
		FecMod = @FecMod,
		Usumod = @UsuMod
	Where Prf_Id = @Prf_Id
End
Go

-- Anulaci�n
Create Proc usp_ProfesionesA_D
@Prf_Id int, @Estado bit, @UsuMmod int
As
Begin
	Update Profesiones
	Set	Estado = @Estado,
		FecMod = GETDATE(),
		Usumod = @UsuMmod
	Where Prf_Id = @Prf_Id
End
Go

-- Listar
Alter Proc usp_ProfesionesLis
As
Begin
	Select	Prf_Id As [Id], Prf_Nombre As [Profesi�n],
			Case Estado When '1' Then 'Si' Else 'No' End As Activo
	From	Profesiones
End
Go

-- Recupera datos
Create Proc usp_ProfesionesRec
@PrfId int
As
Begin
	Select * From Profesiones Where Prf_Id = @PrfId
End
Go

/******************************************************
         Mantenimiento de Grados de Instrucci�n
*******************************************************/
--Creaci�n
Create Proc usp_GradosInsruccionAdd
@GrI_Id int, @GrI_Nombre varchar(50),
@Estado bit, @UsuCrea int, @UsuModi int
As
Begin
	Insert GradosInstruccion
	(GrI_Id, GrI_Nombre,Estado,UsuCrea,FecCrea,UsuModi,FecModi)
	Values
	(@GrI_Id, @GrI_Nombre,@Estado,@UsuCrea,GETDATE(),@UsuModi,GETDATE())
End
Go

-- Modificaci�n
Create Proc usp_GradosInstruccionUpd
@GrI_Id int, @GrI_Nombre varchar(50),
@UsuModi int
As
Begin
	Update GradosInstruccion
	Set	GrI_Nombre = @GrI_Nombre,
		FecModi = GETDATE(),
		UsuModi = @UsuModi
	Where GrI_Id = @GrI_Id
End
Go

-- Anulaci�n
Create Proc usp_GradosInstruccionA_D
@GrI_Id int, @Estado bit, @UsuModi int
As
Begin
	Update GradosInstruccion
	Set	Estado = @Estado,
		FecModi = GETDATE(),
		Usumodi = @UsuModi
	Where GrI_Id = @GrI_Id
End
Go

-- Listar
Alter Proc usp_GradosInstruccionLis
As
Begin
	Select	GrI_Id As [Id], GrI_Nombre As [Grados Instrucci�n],
			Case Estado When '1' Then 'Si' Else 'No' End As Activo
	From	GradosInstruccion
End
Go

-- Recupera datos
Create Proc usp_GradosInstruccionRec
@GrI_Id int
As
Begin
	Select * From GradosInstruccion Where GrI_Id = @GrI_Id
End
Go

/******************************************************
                Mantenimiento de Ubigeos
*******************************************************/
--Creaci�n
Alter Proc usp_UbigeoAdd
@Ubi_Cod varchar(10), @Ubi_Nombre varchar(50),
@Estado bit, @UsuCrea int, @UsuModi int
As
Begin
	Insert Ubigeo
	(Ubi_Cod, Ubi_Nombre,Estado,UsuCrea,FecCrea,UsuModi,FecModi)
	Values
	(@Ubi_Cod, @Ubi_Nombre,@Estado,@UsuCrea,GETDATE(),@UsuModi,GETDATE())
End
Go

-- Modificaci�n
Alter Proc usp_UbigeoUpd
@Ubi_Cod varchar(10), @Ubi_Nombre varchar(50),
@UsuModi int
As
Begin
	Update Ubigeo
	Set	Ubi_Nombre = @Ubi_Nombre,
		FecModi = GETDATE(),
		UsuModi = @UsuModi
	Where Ubi_Cod = @Ubi_Cod
End
Go

-- Anulaci�n
Alter Proc usp_UbigeoA_D
@Ubi_Cod varchar(10), @Estado bit, @UsuModi int
As
Begin
	Update Ubigeo
	Set	Estado = @Estado,
		FecModi = GETDATE(),
		Usumodi = @UsuModi
	Where Ubi_Cod = @Ubi_Cod
End
Go

-- Listar
Alter Proc usp_UbigeoLis
As
Begin
	Select	Ubi_Cod As [C�digo], Ubi_Nombre As [Ubigeo],
			Case Estado When '1' Then 'Si' Else 'No' End As Activo
	From	Ubigeo
End
Go

-- Recupera datos
Alter Proc usp_UbigeoRec
@Ubi_Cod varchar(10)
As
Begin
	Select * From Ubigeo Where Ubi_Cod = @Ubi_Cod
End
Go

/******************************************************
    Mantenimiento de Tipos de Documento de Identidad
*******************************************************/
--Creaci�n
Alter Proc usp_DocIdentidadAdd
@Did_Nombre varchar(50),
@Estado bit, @UsuCrea int, @UsuModi int
As
Begin
	Insert DocIdentidad
	(Did_Nombre,Estado,UsuCrea,FecCrea,UsuModi,FecModi)
	Values
	(@Did_Nombre,@Estado,@UsuCrea,GETDATE(),@UsuModi,GETDATE())
End
Go

-- Modificaci�n
Create Proc usp_DocIdentidadUpd
@Did_Id int, @Did_Nombre varchar(50),
@UsuModi int
As
Begin
	Update DocIdentidad
	Set	Did_Nombre = @Did_Nombre,
		FecModi = GETDATE(),
		UsuModi = @UsuModi
	Where Did_Id = @Did_Id
End
Go

-- Anulaci�n
Create Proc usp_DocIdentidadA_D
@Did_Id int,@Estado bit, @UsuModi int
As
Begin
	Update DocIdentidad
	Set	Estado = @Estado,
		FecModi = GETDATE(),
		Usumodi = @UsuModi
	Where Did_Id = @Did_Id
End
Go

-- Listar
Create Proc usp_DocIdentidadLis
As
Begin
	Select	Did_Id As [C�digo], Did_Nombre As [Documento Identidad],
			Case Estado When '1' Then 'Si' Else 'No' End As Activo
	From	DocIdentidad
End
Go

-- Recupera datos
Create Proc usp_DocIdentidadRec
@Did_Id int
As
Begin
	Select * From DocIdentidad Where Did_Id = @Did_Id
End
Go

/******************************************************
    Mantenimiento de Especialidades M�dicas
*******************************************************/
--Creaci�n
Create Proc usp_EspecialidadesAdd
@Esp_Nombre varchar(50),
@Estado bit, @UsuCrea int, @UsuModi int
As
Begin
	Insert Especialidades
	(Esp_Nombre,Estado,UsuCrea,FecCrea,UsuModi,FecModi)
	Values
	(@Esp_Nombre,@Estado,@UsuCrea,GETDATE(),@UsuModi,GETDATE())
End
Go

-- Modificaci�n
Create Proc usp_EspecialidadesUpd
@Esp_Id int, @Esp_Nombre varchar(50),
@UsuModi int
As
Begin
	Update Especialidades
	Set	Esp_Nombre = @Esp_Nombre,
		FecModi = GETDATE(),
		UsuModi = @UsuModi
	Where Esp_Id = @Esp_Id
End
Go

-- Anulaci�n
Create Proc usp_EspecialidadesA_D
@Esp_Id int,@Estado bit, @UsuModi int
As
Begin
	Update Especialidades
	Set	Estado = @Estado,
		FecModi = GETDATE(),
		Usumodi = @UsuModi
	Where Esp_Id = @Esp_Id
End
Go

-- Listar
Create Proc usp_EspecialidadesLis
As
Begin
	Select	Esp_Id As [Id], Esp_Nombre As [Especialidad],
			Case Estado When '1' Then 'Si' Else 'No' End As Activo
	From	Especialidades
End
Go

-- Recupera datos
Create Proc usp_EspecialidadesRec
@Esp_Id int
As
Begin
	Select * From Especialidades Where Esp_Id = @Esp_Id
End
Go

/******************************************************
    Mantenimiento de Tipos de Tarifa
*******************************************************/
--Creaci�n
Create Proc usp_TiposTarifaAdd
@TTr_Codigo char(2), @TTr_Nombre varchar(50),
@Estado bit, @UsuCrea int, @UsuModi int
As
Begin
	Insert TiposTarifa
	(TTr_Codigo, Ttr_Nombre,Estado,UsuCrea,FecCrea,UsuModi,FecModi)
	Values
	(@TTr_Codigo, @TTr_Nombre,@Estado,@UsuCrea,GETDATE(),@UsuModi,GETDATE())
End
Go

-- Modificaci�n
Alter Proc usp_TiposTarifaUpd
@TTr_Codigo char(2), @TTr_Nombre varchar(50),
@UsuModi int
As
Begin
	Update TiposTarifa
	Set	@TTr_Nombre = @TTr_Nombre,
		FecModi = GETDATE(),
		UsuModi = @UsuModi
	Where TTr_Codigo = @TTr_Codigo
End
Go

-- Anulaci�n
Alter Proc usp_TiposTarifaA_D
@TTr_Codigo char(2), @Estado bit, @UsuModi int
As
Begin
	Update TiposTarifa
	Set	Estado = @Estado,
		FecModi = GETDATE(),
		Usumodi = @UsuModi
	Where TTr_Codigo = @TTr_Codigo
End
Go

-- Listar
Alter Proc usp_TiposTarifaLis
As
Begin
	Select	TTr_Codigo As [C�digo], TTr_Nombre As [Tipo Tarifa],
			Case Estado When '1' Then 'Si' Else 'No' End As Activo
	From	TiposTarifa
End
Go

-- Recupera datos
Alter Proc usp_TiposTarifaRec
@TTr_Codigo char(2)
As
Begin
	Select * From TiposTarifa Where TTr_Codigo = @TTr_Codigo
End
Go

/******************************************************
         Mantenimiento de Principios Activos
*******************************************************/
--Creaci�n
Alter Proc usp_PrincipiosActivosAdd
@PrA_Nombre varchar(100), @PrA_Tipo char(2),
@PrA_Presentacion varchar(50),@PrA_Dosis varchar(50),
@PrA_Interacciones varchar(500),@PrA_Contraindicaciones varchar(500),
@Estado bit, @UsuCrea int, @UsuModi int
As
Begin
	Insert PrincipiosActivos
	(PrA_Nombre,PrA_Tipo,PrA_Presentacion,PrA_Dosis,
	 PrA_Interacciones, PrA_Contraindicaciones,
	 Estado,UsuCrea,FecCrea,UsuModi,FecModi)
	Values
	(@PrA_Nombre,@PrA_Tipo,@PrA_Presentacion,@PrA_Dosis,
	 @PrA_Interacciones, @PrA_Contraindicaciones,
	 @Estado,@UsuCrea,GETDATE(),@UsuModi,GETDATE())
End
Go

-- Modificaci�n
Alter Proc usp_PrincipiosActivosUpd
@PrA_Id int, @PrA_Nombre varchar(100),@PrA_Tipo char(2),
@PrA_Presentacion varchar(50),@PrA_Dosis varchar(50),
@PrA_Interacciones varchar(500),@PrA_Contraindicaciones varchar(500),
@UsuModi int
As
Begin
	Update PrincipiosActivos
	Set	PrA_Nombre = @PrA_Nombre,
		PrA_Tipo = @PrA_Tipo,
		PrA_Presentacion = @PrA_Presentacion,
		PrA_Dosis = @Pra_Dosis,
		PrA_Interacciones = @PrA_Interacciones,
		PrA_Contraindicaciones = @PrA_Contraindicaciones,
		FecModi = GETDATE(),
		UsuModi = @UsuModi
	Where PrA_Id = @PrA_Id
End
Go

-- Anulaci�n
Alter Proc usp_PrincipiosActivosA_D
@PrA_Id int, @Estado bit, @UsuModi int
As
Begin
	Update PrincipiosActivos
	Set	Estado = @Estado,
		FecModi = GETDATE(),
		Usumodi = @UsuModi
	Where PrA_Id = @PrA_Id
End
Go

-- Listar
Alter Proc usp_PrincipiosActivosLis
As
Begin
	Select	PrA_Id As [Id], PrA_Nombre As [Princ_Activo],
			Pra_Presentacion [Presentaci�n],PrA_Tipo As [Tipo],
			Case Estado When '1' Then 'Si' Else 'No' End As Activo
	From	PrincipiosActivos
End
Go

-- Recupera datos
Create Proc usp_PrincipiosActivosRec
@PrA_Id int
As
Begin
	Select * From PrincipiosActivos Where PrA_Id = @PrA_Id
End
Go

/******************************************************
         Mantenimiento de Laboratorios
*******************************************************/
--Creaci�n
Create Proc usp_LaboratoriosAdd
@Lab_Nombre varchar(50),
@Estado bit, @UsuCrea int, @UsuModi int
As
Begin
	Insert Laboratorios
	(Lab_Nombre,Estado,UsuCrea,FecCrea,UsuModi,FecModi)
	Values
	(@Lab_Nombre,@Estado,@UsuCrea,GETDATE(),@UsuModi,GETDATE())
End
Go

-- Modificaci�n
Create Proc usp_LaboratoriosUpd
@Lab_Id int, @Lab_Nombre varchar(50),
@UsuModi int
As
Begin
	Update Laboratorios
	Set	Lab_Nombre = @Lab_Nombre,
		FecModi = GETDATE(),
		UsuModi = @UsuModi
	Where Lab_Id = @Lab_Id
End
Go

-- Anulaci�n
Create Proc usp_LaboratoriosA_D
@Lab_Id int, @Estado bit, @UsuModi int
As
Begin
	Update Laboratorios
	Set	Estado = @Estado,
		FecModi = GETDATE(),
		Usumodi = @UsuModi
	Where Lab_Id = @Lab_Id
End
Go

-- Listar
Create Proc usp_LaboratoriosLis
As
Begin
	Select	Lab_Id As [Id], Lab_Nombre As [Laboratorio],
			Case Estado When '1' Then 'Si' Else 'No' End As Activo
	From	Laboratorios
End
Go

-- Recupera datos
Create Proc usp_LaboratoriosRec
@Lab_Id int
As
Begin
	Select * From Laboratorios Where Lab_Id = @Lab_Id
End
Go

/******************************************************
         Mantenimiento de Pacientes
*******************************************************/
--Creaci�n
Alter Proc usp_PacientesAdd
@Pac_HC varchar(20),
@Pac_ApPat varchar(50),
@Pac_ApMat varchar(50),
@Pac_Nombres varchar(50),
@Pac_NomComp varchar(150),
@Pac_Sexo char(1),
@Pac_LugNacId varchar(10),
@Pac_FecNac datetime,
@Pac_DocIdentId int,
@Pac_DocIdentNum varchar(20),
@Pac_GpoSang varchar(20),
@Pac_Direccion varchar(150),
@Pac_DistritoId varchar(10),
@Pac_Telefonos varchar(30),
@Pac_Email varchar(30),
@Pac_ProfesionId int,
@Pac_InstruccionId int,
@Pac_PersResp varchar(150),
@Pac_DirecResp varchar(150),
@Pac_DistRespId varchar(10),
@Pac_TelefResp varchar(30),
@Pac_EmailResp varchar(30),
@Pac_NombreFact varchar(150),
@Pac_DirecFact varchar(150),
@Pac_DistFactId varchar(10),
@Pac_RUCFact varchar(15),
@Estado bit,
@UsuCrea int,
@UsuModi int
As
Begin
	Insert Pacientes
	(Pac_HC,Pac_ApPat,Pac_ApMat,Pac_Nombres,Pac_NomComp,
	Pac_Sexo,Pac_LugNacId,Pac_FecNac,Pac_GpoSang,
	Pac_DocIdentId,Pac_DocIdentNum,Pac_ProfesionId,Pac_InstruccionId,
	Pac_Direccion,Pac_DistritoId,Pac_Telefonos,Pac_Email,
	Pac_PersResp,Pac_DirecResp,Pac_DistRespId,Pac_TelefResp,Pac_EmailResp,
	Pac_NombreFact,Pac_DirecFact,Pac_DistFactId,Pac_RUCFact,
	Estado,UsuCrea,FecCrea,UsuModi,FecModi)
	Values
	(@Pac_HC,@Pac_ApPat,@Pac_ApMat,@Pac_Nombres,@Pac_NomComp,
	@Pac_Sexo,@Pac_LugNacId,@Pac_FecNac,@Pac_GpoSang,
	@Pac_DocIdentId,@Pac_DocIdentNum,@Pac_ProfesionId,@Pac_InstruccionId,
	@Pac_Direccion,@Pac_DistritoId,@Pac_Telefonos,@Pac_Email,
	@Pac_PersResp,@Pac_DirecResp,@Pac_DistRespId,@Pac_TelefResp,@Pac_EmailResp,
	@Pac_NombreFact,@Pac_DirecFact,@Pac_DistFactId,@Pac_RUCFact,
	@Estado,@UsuCrea,GETDATE(),@UsuModi,GETDATE())
End
Go

-- Modificaci�n
Alter Proc usp_PacientesUpd
@Pac_HC varchar(20),
@Pac_ApPat varchar(50),
@Pac_ApMat varchar(50),
@Pac_Nombres varchar(50),
@Pac_NomComp varchar(150),
@Pac_Sexo char(1),
@Pac_LugNacId varchar(10),
@Pac_FecNac datetime,
@Pac_DocIdentId int,
@Pac_DocIdentNum varchar(20),
@Pac_GpoSang varchar(20),
@Pac_Direccion varchar(150),
@Pac_DistritoId varchar(10),
@Pac_Telefonos varchar(30),
@Pac_Email varchar(30),
@Pac_ProfesionId int,
@Pac_InstruccionId int,
@Pac_PersResp varchar(150),
@Pac_DirecResp varchar(150),
@Pac_DistRespId varchar(10),
@Pac_TelefResp varchar(30),
@Pac_EmailResp varchar(30),
@Pac_NombreFact varchar(150),
@Pac_DirecFact varchar(150),
@Pac_DistFactId varchar(10),
@Pac_RUCFact varchar(15),
@UsuModi int
As
Begin
	Update Pacientes
	Set	Pac_ApPat = @Pac_ApPat,
		Pac_ApMat = @Pac_ApMat,
		Pac_Nombres = @Pac_Nombres,
		Pac_NomComp = @Pac_NomComp,
		Pac_Sexo = @Pac_Sexo,
		Pac_LugNacId = @Pac_LugNacId,
		Pac_FecNac = @Pac_FecNac,
		Pac_DocIdentId = @Pac_DocIdentId,
		Pac_DocIdentNum = @Pac_DocIdentNum,
		Pac_GpoSang = @Pac_GpoSang,
		Pac_Direccion = @Pac_Direccion,
		Pac_DistritoId = @Pac_DistritoId,
		Pac_Telefonos = @Pac_Telefonos,
		Pac_Email = @Pac_Email,
		Pac_ProfesionId = @Pac_ProfesionId,
		Pac_InstruccionId = @Pac_InstruccionId,
		Pac_PersResp = @Pac_PersResp,
		Pac_DirecResp = @Pac_DirecResp,
		Pac_DistRespId = @Pac_DistRespId,
		Pac_TelefResp = @Pac_TelefResp,
		Pac_EmailResp = @Pac_EmailResp,
		Pac_NombreFact = @Pac_NombreFact,
		Pac_DirecFact = @Pac_DirecFact,
		Pac_DistFactId = @Pac_DistFactId,
		Pac_RUCFact = @Pac_RUCFact,
		FecModi = GETDATE(),
		UsuModi = @UsuModi
	Where Pac_HC = @Pac_HC
End
Go

-- Anulaci�n
Create Proc usp_PacientesA_D
@Pac_HC varchar(20), @Estado bit, @UsuModi int
As
Begin
	Update Pacientes
	Set	Estado = @Estado,
		FecModi = GETDATE(),
		Usumodi = @UsuModi
	Where Pac_HC = @Pac_HC
End
Go

-- Listar
Create Proc usp_PacientesLis
As
Begin
	Select	p.Pac_HC As [Historia], p.Pac_NomComp As [Paciente],
			g.GrI_Nombre[Instrucci�n],o.Prf_Nombre [Profesi�n],
			Case p.Estado When '1' Then 'Si' Else 'No' End As Activo
	From	Pacientes p, GradosInstruccion g, Profesiones o
	Where	p.Pac_InstruccionId = g.GrI_Id And
			p.Pac_ProfesionId = o.Prf_Id
End
Go

-- Recupera datos
Alter Proc usp_PacientesRec
@Pac_HC varchar(20)
As
Begin
	Select * From Pacientes Where Pac_HC = @Pac_HC
End
Go

/******************************************************/
/******************************************************
                 Manejo de la Ficha Medica
*******************************************************/
--Procedure para Guardar datos por primera vez
Alter Proc usp_FichaMedicaAdd
@TipoAtencion char(1),
@HC varchar(20),
@Edad int,
@InstId int,
@ProfesionId int,
@MotivoConsulta varchar(500),
@FM_FechaUltMenst varchar(20),
@Alcohol bit,
@Tabaco bit,
@Drogas bit,
@OtrosHN bit,
@ObservHN varchar(250),
@PAMaxima int,
@PAMinima int,
@Pulso int,
@Temperatura decimal(18,1),
@Peso decimal(18,3),
@Talla decimal(18,3),
@Lucido bit,
@OTiempo bit,
@OEspacio bit,
@OPersona bit,
@EstGeneral varchar(20),
@EstNutricion varchar(20),
@EstHidratacion varchar(20),
@FM_ProxCita datetime,
@FM_Recomend varchar(250),
@FM_ObservGenerales varchar(250),
@Estado char(1)
As
Begin
	Insert FichaMedica
	(Pac_TipoAtencion,Pac_HC,Pac_Edad,GrI_Id,ProfesionId,FM_MotivoConsulta,FM_FechaUltMenst,
	FM_Alcohol,FM_Tabaco,FM_Drogas,FM_OtrosHabNoc,FM_ObservHabNoc,FM_PAMax,FM_PAMin,
	FM_Pulso,FM_Temp,FM_Peso,FM_Talla,FM_Lucido,FM_OTiempo,FM_OEspacio,
	FM_OPersona,FM_EstGeneral,FM_EstNutricion,FM_EstHidratacion,FM_ProxCita,
	FM_Recomend,FM_ObservGenerales,FM_Estado)
	Values
	(@TipoAtencion,@HC,@Edad,@InstId,@ProfesionId,@MotivoConsulta,@FM_FechaUltMenst,
	@Alcohol,@Tabaco,@Drogas,@OtrosHN,@ObservHN,@PAMaxima,@PAMinima,
	@Pulso,@Temperatura,@Peso,@Talla,@Lucido,@OTiempo,@OEspacio,
	@OPersona,@EstGeneral,@EstNutricion,@EstHidratacion,@FM_ProxCita,
	@FM_Recomend,@FM_ObservGenerales,@Estado)
End
Go

--Procedure para Guardar datos cuando esta en "Proceso" y al dar click
--en el bot�n Cerrar Ficha
Alter Proc usp_FichaMedicaUpd
@FichaMedicaId int,
@TipoAtencion char(1),
@HC varchar(20),
@Edad int,
@InstId int,
@ProfesionId int,
@MotivoConsulta varchar(500),
@FM_FechaUltMenst varchar(20),
@Alcohol bit,
@Tabaco bit,
@Drogas bit,
@OtrosHN bit,
@ObservHN varchar(250),
@PAMaxima int,
@PAMinima int,
@Pulso int,
@Temperatura decimal(18,1),
@Peso decimal(18,3),
@Talla decimal(18,3),
@Lucido bit,
@OTiempo bit,
@OEspacio bit,
@OPersona bit,
@EstGeneral varchar(20),
@EstNutricion varchar(20),
@EstHidratacion varchar(20),
@FM_ProxCita datetime,
@FM_Recomend varchar(250),
@FM_ObservGenerales varchar(250),
@Estado char(1)
As
Begin
	Update FichaMedica
	Set	Pac_TipoAtencion = @TipoAtencion,
		Pac_HC = @HC,
		Pac_Edad = @Edad,
		GrI_Id = @InstId,
		ProfesionId = @ProfesionId,
		FM_MotivoConsulta = @MotivoConsulta,
		FM_FechaUltMenst = @FM_FechaUltMenst,
		FM_Alcohol = @Alcohol,
		FM_Tabaco = @Tabaco,
		FM_Drogas = @Drogas,
		FM_OtrosHabNoc = @OtrosHN,
		FM_ObservHabNoc = @ObservHN,
		FM_PAMax = @PAMaxima,
		FM_PAMin = @PAMinima,
		FM_Pulso = @Pulso,
		FM_Temp = @Temperatura,
		FM_Peso = @Peso,
		FM_Talla = @Talla,
		FM_Lucido = @Lucido,
		FM_OTiempo = @OTiempo,
		FM_OEspacio = @OEspacio,
		FM_OPersona = @OPersona,
		FM_EstGeneral = @EstGeneral,
		FM_EstNutricion = @EstNutricion,
		FM_EstHidratacion = @EstHidratacion,
		FM_ProxCita = @FM_ProxCita,
		FM_Recomend = @FM_Recomend,
		FM_ObservGenerales = @FM_ObservGenerales,
		FM_Estado = @Estado
	Where FichaMedicaId = @FichaMedicaId
End
Go

-- Recuperar el n�mero de la �ltima Ficha con estado en "Proceso"
Create Proc usp_FMedicaId
@Pac_HC varchar(20)
As
Begin
	Select Top(1) FichaMedica 
	From FichaMedica
	Where Pac_HC = @Pac_HC
	Order By FichaMedica Desc 
End
Go

-- Recuperar los datos de la �ltima Ficha con estado en "Proceso"
Alter Proc usp_FMedicaProceso
@Pac_HC varchar(20)
As
Begin
	Select Top(1) * 
	From  	FichaMedica
	Where	Pac_HC = @Pac_HC
	Order By FichaMedicaId Desc
End
Go

-- Llenar combo de Principios Activos - Medicamentos
Create Proc usp_CboPrincActivosFA
As
Begin
	Select	PrA_Id, PrA_Nombre
	From	PrincipiosActivos
	Where	PrA_Tipo = 'FA'
	And		Estado = 1
End
Go

-- Recupera informaci�n del Principio Activo para Mostrarla
Create Proc usp_InfPrincActivosFA
@PrA_Id int
As
Begin
	Select	PrA_Nombre, PrA_Presentacion,PrA_Dosis, PrA_Interacciones,
			PrA_Contraindicaciones
	From	PrincipiosActivos
	Where	PrA_Id = @PrA_Id
End
Go

-- Llena el Combo de Medicamentos segun el Principio Activo
ALTER Proc usp_CboMedicamentos
@PrA_Id int
As
Begin
	Select	pd.Prd_Id,(pd.Prd_Descripcion + ' ' +
			pd.concentracion + ' ' + pr.Prs_Nombre) As Prd_Descripcion
	From	Productos pd, Laboratorios la, Presentaciones pr
	Where 	pd.Estado = 1
	And		pd.Lab_Cod = la.Lab_Id
	And		pd.prs_id = pr.Prs_Id
	And		pd.PrA_Cod = @PrA_Id
End
Go

-- Llena el Registro de Medicamentos segun el c�digo de producto
Alter Proc usp_InfProductos
@Prd_Id int
As
Begin
	Select	ct.ClasTerapNombre, pd.Prd_Descripcion, la.Lab_Nombre, pd.concentracion, pr.Prs_Nombre
	From	Productos pd, Laboratorios la, Presentaciones pr, ClasificacionTerapeutica ct
	Where	pd.Lab_Cod = la.Lab_Id
	And		pd.prs_id = pr.Prs_Id
	And		pd.ClasTerapId = ct.ClasTerapId
	And		pd.Prd_Id = @Prd_Id
End
Go

-- Llenar la tabla Ficha FM_Prescripcion
Create Proc usp_FMPrescripcionAdd
@FichaMedicaId int,
@PrA_Id int,
@RpNomComercial varchar(150),
@RpIndicaciones varchar(250)
As
Begin
	Insert FM_Prescripcion
	(FichaMedicaId,PrA_Id,RpNomComercial,RpIndicaciones)
	Values
	(@FichaMedicaId,@PrA_Id,@RpNomComercial,@RpIndicaciones)
End
Go

-- Actualizar Medicina en la tabla Ficha FM_Prescripcion
Alter Proc usp_FMPrescripcionUpd
@FichaMedicaId int,
@RpIndicaciones varchar(250)
As
Begin
	Update	FM_Prescripcion
	Set		RpIndicaciones = @RpIndicaciones
	Where	FichaMedicaId = @FichaMedicaId
End
Go

-- Eliminar Medicina de la tabla Ficha FM_Prescripcion
Create Proc usp_FMPrescripcionDel
@FichaMedicaId int
As
Begin
	Delete	FM_Prescripcion
	Where	FichaMedicaId = @FichaMedicaId
End
Go

-- LLenar el Grid de Medicamentos
Alter Proc usp_FMPrescripcionLis
@FichaMedicaId int
As
Begin
--	Select	g.PrA_Nombre [Principio Activo],r.RpNomComercial [Comercial], r.RpIndicaciones [Indicaciones]
	Select	r.PrA_Id [Principio Activo],r.RpNomComercial [Comercial], r.RpIndicaciones [Indicaciones]
	From	FM_Prescripcion r, PrincipiosActivos g
	Where	r.FichaMedicaId = @FichaMedicaId
	And		r.PrA_Id = g.PrA_Id  
End
Go

--Mantenimiento de Antecedentes Patol�gicos
--Adicionar Antecedentes Patol�gicos
Create Proc usp_AntecPatologicosAdd
@FichaMedicaId int,
@Pac_HC varchar(20),
@PatologiaId int,
@PatologiaObserv varchar(250)
As
Begin
	Insert FM_AntecPatologicos
	(FichaMedicaId,Pac_HC,PatologiaId,PatologiaObserv)
	Values
	(@FichaMedicaId,@Pac_HC,@PatologiaId,@PatologiaObserv)
End 
Go

--Actualizar Antecedentes Patol�gicos
Create Proc usp_AntecPatologicosUpd
@FichaMedicaId int,
@Pac_HC varchar(20),
@PatologiaId int,
@PatologiaObserv varchar(250)
As
Begin
	Update	FM_AntecPatologicos
	Set		PatologiaObserv = @PatologiaObserv
	Where	FichaMedicaId = @FichaMedicaId
	And		PatologiaId = @PatologiaId
End 
Go

--Eliminar Antecedentes Patol�gicos
Alter Proc usp_AntecPatologicosDel
@FichaMedicaId int,
@PatologiaId int
As
Begin
	Delete	FM_AntecPatologicos
	Where	FichaMedicaId = @FichaMedicaId
	And		PatologiaId = @PatologiaId
End 
Go

--Listar Antecedentes Patol�gicos
Alter Proc usp_AntecPatologicosLis
@Pac_HC varchar(20)
As
Begin
	Select	b.PatologiaId, b.PatologiaNombre As Patolog�a, a.PatologiaObserv As Observaciones
	From	FM_AntecPatologicos a, Patologias b
	Where	a.Pac_HC = @Pac_HC
	And		a.PatologiaId = b.PatologiaId
End 
Go

--Listar Antecedentes Patol�gicos
Alter Proc usp_AntecPatologicosRec
@FichaMedicaId int,
@PatologiaId int
As
Begin
	Select	b.PatologiaId, b.PatologiaNombre As Patolog�a, a.PatologiaObserv As Observaciones
	From	FM_AntecPatologicos a, Patologias b
	Where	a.FichaMedicaId = @FichaMedicaId
	And		a.PatologiaId = @PatologiaId
	And		a.PatologiaId = b.PatologiaId
End 
Go
