//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace pjDermaBelle
{
    using System;
    
    public partial class usp_PacientesCitados_Result
    {
        public System.TimeSpan Hora { get; set; }
        public string Historia { get; set; }
        public string Paciente { get; set; }
        public string AtendidoComo { get; set; }
        public Nullable<System.DateTime> Fecha { get; set; }
        public string Servicio { get; set; }
        public string Atendido_Por { get; set; }
        public string CIE { get; set; }
        public string Diagnóstico { get; set; }
        public string EstadoFicha { get; set; }
    }
}
