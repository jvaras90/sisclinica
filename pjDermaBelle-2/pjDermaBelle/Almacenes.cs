//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace pjDermaBelle
{
    using System;
    using System.Collections.Generic;
    
    public partial class Almacenes
    {
        public short Alm_Id { get; set; }
        public string Alm_Cod { get; set; }
        public string Alm_Nombre { get; set; }
        public string Estado { get; set; }
        public System.DateTime FecCrea { get; set; }
        public short UsuCrea { get; set; }
        public System.DateTime FecMod { get; set; }
        public short UsuMod { get; set; }
    }
}
