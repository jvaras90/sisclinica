﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pjDermaBelle
{
    public class ClaseDatos
    {
        DermaBelleDataDataContext obj = new DermaBelleDataDataContext();

// Otros
        public string Fecha { get { return DateTime.Today.ToShortDateString(); } }
        //public string Hora { get { return DateTime.Now.ToLongTimeString(); } }

        //Llenado del grid de Pacientes Citados
        public List<usp_PacientesCitadosResult> GridPacientesCitados()
        {
            return obj.usp_PacientesCitados().ToList();
        }

        // Metodos para llenado de Combos
        public List<usp_CboDocIdentidadResult> CboDocIdentidad()
        {
            return obj.usp_CboDocIdentidad().ToList();
        }

        public List<usp_CboGradosInstruccionResult> CboInstruccion()
        {
            return obj.usp_CboGradosInstruccion().ToList();
        }

        public List<usp_CboProfesionesResult> CboProfesiones()
        {
            return obj.usp_CboProfesiones().ToList();
        }

        public List<usp_CboLugNacimientoResult> CboLugNacimiento()
        {
            return obj.usp_CboLugNacimiento().ToList();
        }

        public List<usp_CboDistritoResult> CboDistritos()
        {
            return obj.usp_CboDistrito().ToList();
        }

        public List<usp_CboPatologiasResult> CboPatologias()
        {
            return obj.usp_CboPatologias().ToList();
        }

        public List<usp_CboCirugiasResult> CboCirugias()
        {
            return obj.usp_CboCirugias().ToList();
        }

        public List<usp_CboAlergiasResult> CboAlergias()
        {
            return obj.usp_CboAlergias().ToList();
        }

        public List<usp_CboDiagnosticosResult> CboDiagnosticos()
        {
            return obj.usp_CboDiagnosticos().ToList();
        }

        // Fin de Metodos para llenado de Combos

        public List<usp_DgvSintomasListResult> DgvSintomasVer(int FichaNum)
        {
            return obj.usp_DgvSintomasList(FichaNum).ToList();
        }

        public List<usp_DgvMedicamentosResult>  DgvComerciales(int PrA_Id)
        {
            return obj.usp_DgvMedicamentos(PrA_Id).ToList();
        }

        // Metodos para Profesiones
        public List<usp_ProfesionesRecResult> ProfesionDatos(int PrfId)
        {
            return obj.usp_ProfesionesRec(PrfId).ToList();
        }

        public List<usp_ProfesionesLisResult> ProfesionListar()
        {
            return obj.usp_ProfesionesLis().ToList();
        }

        public void ProfesionesAdicionar(Profesiones reg)
        {
            try 
            {
                obj.usp_ProfesionesAdd(reg.Prf_Id, reg.Prf_Nombre, reg.Estado, reg.UsuCrea, reg.UsuMod); 
            }
            catch (Exception ex) { throw ex; }
        }

        public void ProfesionesActualizar(Profesiones reg)
        {
            try
            {
                obj.usp_ProfesionesUpd(reg.Prf_Id,reg.Prf_Nombre,reg.UsuMod,reg.FecMod);
            }
            catch (Exception ex) { throw ex; }
        }

        public int ProfesionesAnular(Profesiones reg)
        {
            try
            {
                int x = obj.usp_ProfesionesA_D(reg.Prf_Id, reg.Estado, reg.UsuMod);
                if (x > 0)
                { return x; }
                else
                { return -1; }
            }
            catch (Exception ex) { throw ex; }
        }
        // Fin de Metodos para Profesiones


        // Metodos para Grados de Instrucción
        public List<usp_GradosInstruccionRecResult> GradosDatos(int GrI_Id)
        {
            return obj.usp_GradosInstruccionRec(GrI_Id).ToList();
        }

        public List<usp_GradosInstruccionLisResult> GradosListar()
        {
            return obj.usp_GradosInstruccionLis().ToList();
        }

        public void GradosAdicionar(GradosInstruccion reg)
        {
            try
            {
                obj.usp_GradosInsruccionAdd(reg.GrI_Id, reg.GrI_Nombre, reg.Estado, reg.UsuCrea, reg.UsuModi);
            }
            catch (Exception ex) { throw ex; }
        }

        public void GradosActualizar(GradosInstruccion reg)
        {
            try
            {
                obj.usp_GradosInstruccionUpd(reg.GrI_Id, reg.GrI_Nombre, reg.UsuModi);
            }
            catch (Exception ex) { throw ex; }
        }

        public int GradosAnular(GradosInstruccion reg)
        {
            try
            {
                int x = obj.usp_GradosInstruccionA_D(reg.GrI_Id, reg.Estado, reg.UsuModi);
                if (x > 0)
                { return x; }
                else
                { return -1; }
            }
            catch (Exception ex) { throw ex; }
        }
        // Fin de Metodos para Grados de Instrucción

        // Metodos para Ubigeo
        public List<usp_UbigeoRecResult> UbigeoDatos(string Ubi_Cod)
        {
            return obj.usp_UbigeoRec(Ubi_Cod).ToList();
        }

        public List<usp_UbigeoLisResult> UbigeoListar()
        {
            return obj.usp_UbigeoLis().ToList();
        }

        public void UbigeoAdicionar(Ubigeo reg)
        {
            try
            {
                obj.usp_UbigeoAdd(reg.Ubi_Cod, reg.Ubi_Nombre, reg.Estado, reg.UsuCrea, reg.UsuModi);
            }
            catch (Exception ex) { throw ex; }
        }

        public void UbigeoActualizar(Ubigeo reg)
        {
            try
            {
                obj.usp_UbigeoUpd(reg.Ubi_Cod, reg.Ubi_Nombre, reg.UsuModi);
            }
            catch (Exception ex) { throw ex; }
        }

        public int UbigeoAnular(Ubigeo reg)
        {
            try
            {
                int x = obj.usp_UbigeoA_D(reg.Ubi_Cod, reg.Estado, reg.UsuModi);
                if (x > 0)
                { return x; }
                else
                { return -1; }
            }
            catch (Exception ex) { throw ex; }
        }
        // Fin de Metodos para Ubigeo

        // Metodos para Tipos de Documento
        public List<usp_DocIdentidadRecResult> DocIdentidadDatos(int Did_Id)
        {
            return obj.usp_DocIdentidadRec(Did_Id).ToList();
        }

        public List<usp_DocIdentidadLisResult> DocIdentidadListar()
        {
            return obj.usp_DocIdentidadLis().ToList();
        }

        public void DocIdentidadAdicionar(DocIdentidad reg)
        {
            try
            {
                obj.usp_DocIdentidadAdd(reg.Did_Nombre, reg.Estado, reg.UsuCrea, reg.UsuModi);
            }
            catch (Exception ex) { throw ex; }
        }

        public void DocIdentidadActualizar(DocIdentidad reg)
        {
            try
            {
                obj.usp_DocIdentidadUpd(reg.Did_Id, reg.Did_Nombre, reg.UsuModi);
            }
            catch (Exception ex) { throw ex; }
        }

        public int DocIdentidadAnular(DocIdentidad reg)
        {
            try
            {
                int x = obj.usp_DocIdentidadA_D(reg.Did_Id, reg.Estado, reg.UsuModi);
                if (x > 0)
                { return x; }
                else
                { return -1; }
            }
            catch (Exception ex) { throw ex; }
        }
        // Fin de Metodos para Tipos de Documento

        // Metodos para Especialidades Médicas
        public List<usp_EspecialidadesRecResult> EspecialidadesDatos(int Esp_Id)
        {
            return obj.usp_EspecialidadesRec(Esp_Id).ToList();
        }

        public List<usp_EspecialidadesLisResult> EspecialidadesListar()
        {
            return obj.usp_EspecialidadesLis().ToList();
        }

        public void EspecialidadesAdicionar(Especialidades reg)
        {
            try
            {
                obj.usp_EspecialidadesAdd(reg.Esp_Nombre, reg.Estado, reg.UsuCrea, reg.UsuModi);
            }
            catch (Exception ex) { throw ex; }
        }

        public void EspecialidadesActualizar(Especialidades reg)
        {
            try
            {
                obj.usp_EspecialidadesUpd(reg.Esp_Id, reg.Esp_Nombre, reg.UsuModi);
            }
            catch (Exception ex) { throw ex; }
        }

        public int EspecialidadesAnular(Especialidades reg)
        {
            try
            {
                int x = obj.usp_EspecialidadesA_D(reg.Esp_Id, reg.Estado, reg.UsuModi);
                if (x > 0)
                { return x; }
                else
                { return -1; }
            }
            catch (Exception ex) { throw ex; }
        }
        // Fin de Metodos para Especialidades Médicas

        // Metodos para Tipos de Tarifa
        public List<usp_TiposTarifaRecResult> TiposTarifaDatos(string TTr_Codigo)
        {
            return obj.usp_TiposTarifaRec(TTr_Codigo).ToList();
        }

        public List<usp_TiposTarifaLisResult> TiposTarifaListar()
        {
            return obj.usp_TiposTarifaLis().ToList();
        }

        public void TiposTarifaAdicionar(TiposTarifa reg)
        {
            try
            {
                obj.usp_TiposTarifaAdd(reg.TTr_Codigo, reg.TTr_Nombre, reg.Estado, reg.UsuCrea, reg.UsuModi);
            }
            catch (Exception ex) { throw ex; }
        }

        public void TiposTarifaActualizar(TiposTarifa reg)
        {
            try
            {
                obj.usp_TiposTarifaUpd(reg.TTr_Codigo, reg.TTr_Nombre, reg.UsuModi);
            }
            catch (Exception ex) { throw ex; }
        }

        public int TiposTarifaAnular(TiposTarifa reg)
        {
            try
            {
                int x = obj.usp_TiposTarifaA_D(reg.TTr_Codigo, reg.Estado, reg.UsuModi);
                if (x > 0)
                { return x; }
                else
                { return -1; }
            }
            catch (Exception ex) { throw ex; }
        }
        // Fin de Metodos para Tipos de Tarifa

        // Metodos para Principios Activos
        public List<usp_PrincipiosActivosRecResult> PrincipiosActivosDatos(int PrA_Id)
        {
            return obj.usp_PrincipiosActivosRec(PrA_Id).ToList();
        }

        public List<usp_PrincipiosActivosLisResult> PrincipiosActivosListar()
        {
            return obj.usp_PrincipiosActivosLis().ToList();
        }

        public void PrincipiosActivosAdicionar(PrincipiosActivos reg)
        {
            try
            {
                obj.usp_PrincipiosActivosAdd(reg.PrA_Nombre,reg.PrA_Tipo, reg.PrA_Presentacion,
                    reg.PrA_Dosis,reg.PrA_Interacciones,reg.PrA_Contraindicaciones,reg.Estado, reg.UsuCrea, reg.UsuModi);
            }
            catch (Exception ex) { throw ex; }
        }

        public void PrincipiosActivosActualizar(PrincipiosActivos reg)
        {
            try
            {
                obj.usp_PrincipiosActivosUpd(reg.PrA_Id, reg.PrA_Nombre, reg.PrA_Tipo, reg.PrA_Presentacion,
                    reg.PrA_Dosis,reg.PrA_Interacciones,reg.PrA_Contraindicaciones,reg.UsuModi);
            }
            catch (Exception ex) { throw ex; }
        }

        public int PrincipiosActivosAnular(PrincipiosActivos reg)
        {
            try
            {
                int x = obj.usp_PrincipiosActivosA_D(reg.PrA_Id, reg.Estado, reg.UsuModi);
                if (x > 0)
                { return x; }
                else
                { return -1; }
            }
            catch (Exception ex) { throw ex; }
        }
        // Fin de Metodos para Principios Activos

        // Metodos para Laboratorios
        public List<usp_LaboratoriosRecResult> LaboratoriosDatos(int Lab_Id)
        {
            return obj.usp_LaboratoriosRec(Lab_Id).ToList();
        }

        public List<usp_LaboratoriosLisResult> LaboratoriosListar()
        {
            return obj.usp_LaboratoriosLis().ToList();
        }

        public void LaboratoriosAdicionar(Laboratorios reg)
        {
            try
            {
                obj.usp_LaboratoriosAdd(reg.Lab_Nombre, reg.Estado, reg.UsuCrea, reg.UsuModi);
            }
            catch (Exception ex) { throw ex; }
        }

        public void LaboratoriosActualizar(Laboratorios reg)
        {
            try
            {
                obj.usp_LaboratoriosUpd(reg.Lab_Id, reg.Lab_Nombre, reg.UsuModi);
            }
            catch (Exception ex) { throw ex; }
        }

        public int LaboratoriosAnular(Laboratorios reg)
        {
            try
            {
                int x = obj.usp_LaboratoriosA_D(reg.Lab_Id, reg.Estado, reg.UsuModi);
                if (x > 0)
                { return x; }
                else
                { return -1; }
            }
            catch (Exception ex) { throw ex; }
        }
        // Fin de Metodos para Laboratorios

        // Metodos para Pacientes
        public List<usp_PacientesRecResult> PacientesDatos(string Pac_HC)
        {
            return obj.usp_PacientesRec(Pac_HC).ToList();
        }

        public List<usp_PacientesLisResult> PacientesListar()
        {
            return obj.usp_PacientesLis().ToList();
        }

        public void PacientesAdicionar(Pacientes reg)
        {
            try
            {
                obj.usp_PacientesAdd(reg.Pac_HC, reg.Pac_ApPat, reg.Pac_ApMat, reg.Pac_Nombres, reg.Pac_NomComp,
                    reg.Pac_Sexo, reg.Pac_LugNacId, reg.Pac_FecNac, reg.Pac_DocIdentId, reg.Pac_DocIdentNum,
                    reg.Pac_GpoSang,reg.Pac_Direccion, reg.Pac_DistritoId, reg.Pac_Telefonos, reg.Pac_Email, 
                    reg.Pac_ProfesionId, reg.Pac_InstruccionId, reg.Pac_PersResp, reg.Pac_DirecResp, reg.Pac_DistRespId,
                    reg.Pac_TelefResp,reg.Pac_EmailResp,reg.Pac_NombreFact, reg.Pac_DirecFact, reg.Pac_DistFactId,
                    reg.Pac_RUCFact, reg.Estado, reg.UsuCrea, reg.UsuModi);
            }
            catch (Exception ex) { throw ex; }
        }

        public void PacientesActualizar(Pacientes reg)
        {
            try
            {
                obj.usp_PacientesUpd(reg.Pac_HC, reg.Pac_ApPat, reg.Pac_ApMat, reg.Pac_Nombres, reg.Pac_NomComp,
                    reg.Pac_Sexo,reg.Pac_LugNacId, reg.Pac_FecNac, reg.Pac_DocIdentId, reg.Pac_DocIdentNum, reg.Pac_GpoSang,
                    reg.Pac_Direccion, reg.Pac_DistritoId, reg.Pac_Telefonos, reg.Pac_Email, reg.Pac_ProfesionId,
                    reg.Pac_InstruccionId, reg.Pac_PersResp, reg.Pac_DirecResp, reg.Pac_DistRespId, reg.Pac_TelefResp,
                    reg.Pac_EmailResp, reg.Pac_NombreFact, reg.Pac_DirecFact, reg.Pac_DistFactId, reg.Pac_RUCFact, reg.UsuModi);
            }
            catch (Exception ex) { throw ex; }
        }

        public int Pacientes(Pacientes reg)
        {
            try
            {
                int x = obj.usp_PacientesA_D(reg.Pac_HC, reg.Estado, reg.UsuModi);
                if (x > 0)
                { return x; }
                else
                { return -1; }
            }
            catch (Exception ex) { throw ex; }
        }
        // Fin de Metodos para Pacientes
        
        // Metodos para Ficha Medica
        public List<usp_FMedicaCabeceraResult> FMedicaCabecera(string Pac_HC)
        {
            return obj.usp_FMedicaCabecera(Pac_HC).ToList();
        }

        public List<usp_FMedicaProcesoResult> FMedicaProceso(string Pac_HC)
        {
            return obj.usp_FMedicaProceso(Pac_HC).ToList();
        }

        public void FMedicaAdicionar(FichaMedica reg) 
        {
            try
            {
                obj.usp_FichaMedicaAdd(reg.Pac_TipoAtencion,reg.Pac_HC,reg.Pac_Edad,reg.GrI_Id,reg.ProfesionId,reg.FM_MotivoConsulta,
                reg.FM_FechaUltMenst,reg.FM_Alcohol,reg.FM_Tabaco,reg.FM_Drogas,reg.FM_OtrosHabNoc,reg.FM_ObservHabNoc,reg.FM_PAMax,reg.FM_PAMin,
                reg.FM_Pulso,reg.FM_Temp,reg.FM_Peso,reg.FM_Talla,reg.FM_Lucido,reg.FM_OTiempo,reg.FM_OEspacio,
                reg.FM_OPersona,reg.FM_EstGeneral,reg.FM_EstNutricion,reg.FM_EstHidratacion,reg.FM_ProxCita,reg.FM_Recomend,
                reg.FM_ObservGenerales,reg.FM_Estado);
            }
            catch{}
        }

        public void FMedicaActualizar(FichaMedica reg)
        {
            try
            {
                obj.usp_FichaMedicaUpd(reg.FichaMedicaId,reg.Pac_TipoAtencion, reg.Pac_HC, reg.Pac_Edad, reg.GrI_Id, reg.ProfesionId, reg.FM_MotivoConsulta,
                reg.FM_FechaUltMenst, reg.FM_Alcohol, reg.FM_Tabaco, reg.FM_Drogas, reg.FM_OtrosHabNoc, reg.FM_ObservHabNoc, reg.FM_PAMax, reg.FM_PAMin,
                reg.FM_Pulso, reg.FM_Temp, reg.FM_Peso, reg.FM_Talla, reg.FM_Lucido, reg.FM_OTiempo, reg.FM_OEspacio,
                reg.FM_OPersona, reg.FM_EstGeneral, reg.FM_EstNutricion, reg.FM_EstHidratacion, reg.FM_ProxCita, reg.FM_Recomend,
                reg.FM_ObservGenerales, reg.FM_Estado);
            }
            catch { }
        }

        public List<usp_CboPrincActivosFAResult> CboPrincActivoFA()
        {
            return obj.usp_CboPrincActivosFA().ToList();
        }

        public List<usp_InfPrincActivosFAResult> InfPrincActivoFA(int PrA_Id)
        {
            return obj.usp_InfPrincActivosFA(PrA_Id).ToList();
        }

        public List<usp_InfProductosResult> InfProductos(int Prd_Id)
        {
            return obj.usp_InfProductos(Prd_Id).ToList();
        }

        public List<usp_CboMedicamentosResult> CboMedicamentos(int PrA_Id)
        {
            return obj.usp_CboMedicamentos(PrA_Id).ToList();
        }

        //Metodos para FM_Prescripcion
        public List<usp_FMPrescripcionLisResult> DgvPrescripcion(int FichaMedicaId)
        {
            return obj.usp_FMPrescripcionLis(FichaMedicaId).ToList();
        }

        public void FM_PrescripcionAdd(FM_Prescripcion reg)
        {
            try
            {
                obj.usp_FMPrescripcionAdd(reg.FichaMedicaId, reg.PrA_Id, reg.RpNomComercial, reg.RpIndicaciones);
            }
            catch { }
        }

        public void FM_PrescripcionUpd(FM_Prescripcion reg)
        {
            try
            {
                obj.usp_FMPrescripcionUpd(reg.FichaMedicaId, reg.RpIndicaciones);
            }
            catch { }
        }

        public int FM_PrescripcionDel(FM_Prescripcion reg)
        {
            try
            {
                int x = obj.usp_FMPrescripcionDel(reg.FichaMedicaId);
                if (x > 0)
                { return x; }
                else
                { return -1; }
            }
            catch (Exception ex) { throw ex; }
        }

        //********************************************
        public List<usp_AntecPatologicosRecResult> AntecPatologicosDatos(int FichaMedica, int PatologiaId)
        {
            return obj.usp_AntecPatologicosRec(FichaMedica, PatologiaId).ToList();
        }

        public List<usp_AntecPatologicosLisResult> DgvAntecPatologicos(string Pac_HC)
        {
            return obj.usp_AntecPatologicosLis(Pac_HC).ToList();
        }

        public void FM_AntecPatologicosAdd(FM_AntecPatologicos reg)
        {
            try
            {
                obj.usp_AntecPatologicosAdd(reg.FichaMedicaId, reg.Pac_HC, reg.PatologiaId, reg.PatologiaObserv);
            }
            catch { }
        }

        public void FM_AntecPatologicosUpd(FM_AntecPatologicos reg)
        {
            try
            {
                obj.usp_AntecPatologicosUpd(reg.FichaMedicaId, reg.Pac_HC, reg.PatologiaId, reg.PatologiaObserv);
            }
            catch { }
        }

        public int FM_AntecPatologicosDel(FM_AntecPatologicos reg)
        {
            try
            {
                int x = obj.usp_AntecPatologicosDel(reg.FichaMedicaId, reg.PatologiaId);
                if (x > 0)
                { return x; }
                else
                { return -1; }
            }
            catch (Exception ex) { throw ex; }
        }
    }
}
