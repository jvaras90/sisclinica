//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace pjDermaBelle
{
    using System;
    using System.Collections.Generic;
    
    public partial class MovAlmCabecera
    {
        public short Kdx_id { get; set; }
        public string Alm_Cod { get; set; }
        public string Mov_Cod { get; set; }
        public string Prv_Cod { get; set; }
        public string Mov_TDoc { get; set; }
        public string Mov_NDoc { get; set; }
        public string Pag_Cod { get; set; }
        public System.DateTime Mov_FEmision { get; set; }
        public System.DateTime Mov_FPago { get; set; }
        public decimal Mov_VVenta { get; set; }
        public decimal Mov_IGV { get; set; }
        public decimal Mov_Total { get; set; }
        public string Mov_Observ { get; set; }
        public System.DateTime Mov_FRegistro { get; set; }
        public string Estado { get; set; }
        public System.DateTime FecCrea { get; set; }
        public short UsuCrea { get; set; }
        public System.DateTime FecMod { get; set; }
        public short UsuMod { get; set; }
    }
}
