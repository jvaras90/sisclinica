﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pjDermaBelle
{
    public partial class frmPacCitados : Form
    {
        public frmPacCitados()
        {
            InitializeComponent();
        }

        ClaseDatos obj = new ClaseDatos();
        DermaBelleDataDataContext linq = new DermaBelleDataDataContext();
        public static string NumHistoria;

        private void frmPacCitados_Load(object sender, EventArgs e)
        {
            lblServicio.Text = "Dermatología";
            lblConsultorio.Text = "Consultorio 01";
            lblMedico.Text = "Rebolledo Martinez, Rosa";
            lblFecha.Text = "25/01/2015";
            dgvPacCitados.DataSource = obj.GridPacientesCitados();

            for (int indice = 0; indice < dgvPacCitados.Rows.Count; indice++)
            {
                dgvPacCitados.Columns[0].Width = 50;    //Hora
                dgvPacCitados.Columns[1].Width = 70;   //Historia
                dgvPacCitados.Columns[2].Width = 200;   //Paciente
                dgvPacCitados.Columns[3].Width = 50;    //Atendido como
                dgvPacCitados.Columns[4].Width = 70;    //Fecha
                dgvPacCitados.Columns[5].Width = 95;   //Servicio
                dgvPacCitados.Columns[6].Width = 150;   //Atendido por
                dgvPacCitados.Columns[7].Width = 50;    //CIE
                dgvPacCitados.Columns[8].Width = 500;   //Diagnóstico
                dgvPacCitados.Columns[9].Width = 70;    //Estado ficha

                dgvPacCitados.Rows[indice].ReadOnly = true;
                //dgvPacCitados.Rows[indice].Cells[1].ReadOnly = true;
                //dgvPacCitados.Rows[indice].Cells[2].ReadOnly = true;
            }

        }

        private void dgvPacCitados_SelectionChanged(object sender, EventArgs e)
        {
            int indice = dgvPacCitados.CurrentCell.RowIndex;
            string Pac_HC = dgvPacCitados.Rows[indice].Cells[1].Value.ToString();
            NumHistoria = Pac_HC;

        }

        private void dgvPacCitados_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            frmFichaMedica frm = new frmFichaMedica();
            frm.Show();

        }
    }
}
