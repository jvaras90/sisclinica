﻿namespace pjDermaBelle
{
    partial class frmPacCitados
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblConsultorio = new System.Windows.Forms.Label();
            this.lblMedico = new System.Windows.Forms.Label();
            this.lblFecha = new System.Windows.Forms.Label();
            this.dermaBelleDataSet = new pjDermaBelle.DermaBelleDataSet();
            this.citasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.citasTableAdapter = new pjDermaBelle.DermaBelleDataSetTableAdapters.CitasTableAdapter();
            this.dgvPacCitados = new System.Windows.Forms.DataGridView();
            this.label5 = new System.Windows.Forms.Label();
            this.lblServicio = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblCitados = new System.Windows.Forms.Label();
            this.lblAtendidos = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblNuevos = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblContinuadores = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lblReingresos = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            ((System.ComponentModel.ISupportInitialize)(this.dermaBelleDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.citasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPacCitados)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(618, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(134, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Pacientes Citados";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Consultorio";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(26, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Médico";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(31, 110);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Fecha";
            // 
            // lblConsultorio
            // 
            this.lblConsultorio.AutoSize = true;
            this.lblConsultorio.ForeColor = System.Drawing.Color.Blue;
            this.lblConsultorio.Location = new System.Drawing.Point(72, 70);
            this.lblConsultorio.Name = "lblConsultorio";
            this.lblConsultorio.Size = new System.Drawing.Size(59, 13);
            this.lblConsultorio.TabIndex = 2;
            this.lblConsultorio.Text = "Consultorio";
            // 
            // lblMedico
            // 
            this.lblMedico.AutoSize = true;
            this.lblMedico.ForeColor = System.Drawing.Color.Blue;
            this.lblMedico.Location = new System.Drawing.Point(72, 90);
            this.lblMedico.Name = "lblMedico";
            this.lblMedico.Size = new System.Drawing.Size(42, 13);
            this.lblMedico.TabIndex = 2;
            this.lblMedico.Text = "Médico";
            // 
            // lblFecha
            // 
            this.lblFecha.AutoSize = true;
            this.lblFecha.ForeColor = System.Drawing.Color.Blue;
            this.lblFecha.Location = new System.Drawing.Point(72, 110);
            this.lblFecha.Name = "lblFecha";
            this.lblFecha.Size = new System.Drawing.Size(37, 13);
            this.lblFecha.TabIndex = 2;
            this.lblFecha.Text = "Fecha";
            // 
            // dermaBelleDataSet
            // 
            this.dermaBelleDataSet.DataSetName = "DermaBelleDataSet";
            this.dermaBelleDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // citasBindingSource
            // 
            this.citasBindingSource.DataMember = "Citas";
            this.citasBindingSource.DataSource = this.dermaBelleDataSet;
            // 
            // citasTableAdapter
            // 
            this.citasTableAdapter.ClearBeforeFill = true;
            // 
            // dgvPacCitados
            // 
            this.dgvPacCitados.AllowUserToAddRows = false;
            this.dgvPacCitados.AllowUserToDeleteRows = false;
            this.dgvPacCitados.AllowUserToOrderColumns = true;
            this.dgvPacCitados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPacCitados.Location = new System.Drawing.Point(13, 136);
            this.dgvPacCitados.Name = "dgvPacCitados";
            this.dgvPacCitados.ReadOnly = true;
            this.dgvPacCitados.Size = new System.Drawing.Size(1345, 493);
            this.dgvPacCitados.TabIndex = 3;
            this.dgvPacCitados.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPacCitados_CellClick);
            this.dgvPacCitados.SelectionChanged += new System.EventHandler(this.dgvPacCitados_SelectionChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(23, 50);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Servicio";
            // 
            // lblServicio
            // 
            this.lblServicio.AutoSize = true;
            this.lblServicio.ForeColor = System.Drawing.Color.Blue;
            this.lblServicio.Location = new System.Drawing.Point(72, 50);
            this.lblServicio.Name = "lblServicio";
            this.lblServicio.Size = new System.Drawing.Size(45, 13);
            this.lblServicio.TabIndex = 2;
            this.lblServicio.Text = "Servicio";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(12, 650);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(160, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Total de Pacientes Citados";
            // 
            // lblCitados
            // 
            this.lblCitados.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCitados.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCitados.ForeColor = System.Drawing.Color.Blue;
            this.lblCitados.Location = new System.Drawing.Point(175, 645);
            this.lblCitados.Name = "lblCitados";
            this.lblCitados.Size = new System.Drawing.Size(52, 23);
            this.lblCitados.TabIndex = 5;
            this.lblCitados.Text = "label7";
            this.lblCitados.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblAtendidos
            // 
            this.lblAtendidos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAtendidos.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAtendidos.ForeColor = System.Drawing.Color.Blue;
            this.lblAtendidos.Location = new System.Drawing.Point(437, 645);
            this.lblAtendidos.Name = "lblAtendidos";
            this.lblAtendidos.Size = new System.Drawing.Size(52, 23);
            this.lblAtendidos.TabIndex = 7;
            this.lblAtendidos.Text = "label8";
            this.lblAtendidos.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(262, 650);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(174, 13);
            this.label9.TabIndex = 6;
            this.label9.Text = "Total de Pacientes Atendidos";
            // 
            // lblNuevos
            // 
            this.lblNuevos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNuevos.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNuevos.ForeColor = System.Drawing.Color.Blue;
            this.lblNuevos.Location = new System.Drawing.Point(794, 645);
            this.lblNuevos.Name = "lblNuevos";
            this.lblNuevos.Size = new System.Drawing.Size(52, 23);
            this.lblNuevos.TabIndex = 9;
            this.lblNuevos.Text = "label10";
            this.lblNuevos.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(738, 650);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(50, 13);
            this.label11.TabIndex = 8;
            this.label11.Text = "Nuevos";
            // 
            // lblContinuadores
            // 
            this.lblContinuadores.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblContinuadores.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblContinuadores.ForeColor = System.Drawing.Color.Blue;
            this.lblContinuadores.Location = new System.Drawing.Point(1069, 645);
            this.lblContinuadores.Name = "lblContinuadores";
            this.lblContinuadores.Size = new System.Drawing.Size(52, 23);
            this.lblContinuadores.TabIndex = 11;
            this.lblContinuadores.Text = "label12";
            this.lblContinuadores.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(975, 650);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(88, 13);
            this.label13.TabIndex = 10;
            this.label13.Text = "Continuadores";
            // 
            // lblReingresos
            // 
            this.lblReingresos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblReingresos.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReingresos.ForeColor = System.Drawing.Color.Blue;
            this.lblReingresos.Location = new System.Drawing.Point(1306, 645);
            this.lblReingresos.Name = "lblReingresos";
            this.lblReingresos.Size = new System.Drawing.Size(52, 23);
            this.lblReingresos.TabIndex = 13;
            this.lblReingresos.Text = "label14";
            this.lblReingresos.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(1230, 650);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(70, 13);
            this.label15.TabIndex = 12;
            this.label15.Text = "Reingresos";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 685);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1370, 22);
            this.statusStrip1.TabIndex = 14;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // frmPacCitados
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1370, 707);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.lblReingresos);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.lblContinuadores);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.lblNuevos);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.lblAtendidos);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.lblCitados);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.dgvPacCitados);
            this.Controls.Add(this.lblFecha);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblMedico);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblServicio);
            this.Controls.Add(this.lblConsultorio);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "frmPacCitados";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pacientes Citados";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmPacCitados_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dermaBelleDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.citasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPacCitados)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblConsultorio;
        private System.Windows.Forms.Label lblMedico;
        private System.Windows.Forms.Label lblFecha;
        private DermaBelleDataSet dermaBelleDataSet;
        private System.Windows.Forms.BindingSource citasBindingSource;
        private DermaBelleDataSetTableAdapters.CitasTableAdapter citasTableAdapter;
        private System.Windows.Forms.DataGridView dgvPacCitados;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblServicio;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblCitados;
        private System.Windows.Forms.Label lblAtendidos;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblNuevos;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lblContinuadores;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lblReingresos;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.StatusStrip statusStrip1;
    }
}