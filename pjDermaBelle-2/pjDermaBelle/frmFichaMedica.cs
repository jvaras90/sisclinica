﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pjDermaBelle
{
    public partial class frmFichaMedica : Form
    {
        public frmFichaMedica()
        {
            InitializeComponent();
        }

        ClaseDatos obj = new ClaseDatos();
        DermaBelleDataDataContext linq = new DermaBelleDataDataContext();
        FichaMedica oFMedica = new FichaMedica();
        FM_Prescripcion oFM_Prescripcion = new FM_Prescripcion();
        FM_AntecPatologicos oFM_AntecPatologicos = new FM_AntecPatologicos();
        public int FichaNum;
        public char EstadoFicha;
        public int PrA_Id = 0;
        public int Prd_Id = 0;

        void FichaMedicaId(string NumHistoria)
        {
            List<usp_FMedicaProcesoResult> FichaId = new List<usp_FMedicaProcesoResult>();
            FichaId = obj.FMedicaProceso(frmPacCitados.NumHistoria);
            if (FichaId.Count() > 0)
            {
                foreach (usp_FMedicaProcesoResult reg in FichaId)
                { FichaNum = reg.FichaMedicaId; }
            }
        }

        void CargarCabecera()
        {
            List<usp_FMedicaCabeceraResult> lista = new List<usp_FMedicaCabeceraResult>();
            lista = obj.FMedicaCabecera(lblNumHistoria.Text);
            if (lista.Count() > 0)
            {
                foreach (usp_FMedicaCabeceraResult reg in lista)
                {
                    lblTipoAtencion.Text = "NUEVO";
                    lblFecha.Text = "29/01/2015";
                    lblHora.Text = "00:36:00";

                    lblPaciente.Text = reg.Pac_NomComp;
                    lblGrupoSanguineo.Text = reg.Pac_GpoSang;
                    lblDocIdentidad.Text = reg.DocIdent;
                    lblGradoInstruccion.Text = reg.GrI_Nombre;
                    lblProfesion.Text = reg.Prf_Nombre;
                    lblSexo.Text = reg.Pac_Sexo;
                    lblEdad.Text = reg.Pac_Edad.ToString();
                    lblLugarNacimiento.Text = reg.Pac_LugNacim;
                    lblProcedencia.Text = reg.Pac_Proced;
                }
            }
        }

        void CargarFicha()
        {
            List<usp_FMedicaProcesoResult> FichaProceso = new List<usp_FMedicaProcesoResult>();
            FichaProceso = obj.FMedicaProceso(frmPacCitados.NumHistoria);
            if (FichaProceso.Count() > 0)
            {
                foreach (usp_FMedicaProcesoResult reg in FichaProceso)
                {
                    FichaNum = reg.FichaMedicaId;
                    //Motivo de la Consulta
                    txtMotivo.Text = reg.FM_MotivoConsulta;
                    chkAlcohol.Checked = Convert.ToBoolean(reg.FM_Alcohol);
                    chkTabaco.Checked = Convert.ToBoolean(reg.FM_Tabaco);
                    chkDrogas.Checked = Convert.ToBoolean(reg.FM_Drogas);
                    chkOtros.Checked = Convert.ToBoolean(reg.FM_OtrosHabNoc);
                    txtObservHabNoc.Text = reg.FM_ObservHabNoc;
                    txtFUltMenstruacion.Text = reg.FM_FechaUltMenst;

                    //Examen Físico
                    txtPAMaxima.Text = reg.FM_PAMax.ToString();
                    txtPAMinima.Text = reg.FM_PAMin.ToString();
                    txtPulso.Text = reg.FM_Pulso.ToString();
                    txtPeso.Text = reg.FM_Peso.ToString();
                    txtTalla.Text = reg.FM_Talla.ToString();
                    chkLucido.Checked = Convert.ToBoolean(reg.FM_Lucido);
                    chkOEspacio.Checked = Convert.ToBoolean(reg.FM_OEspacio);
                    chkOTiempo.Checked = Convert.ToBoolean(reg.FM_OTiempo);
                    chkOPersona.Checked = Convert.ToBoolean(reg.FM_OPersona);
                    cboEstGeneral.Text = reg.FM_EstGeneral;
                    cboEstHidratacion.Text = reg.FM_EstHidratacion;
                    cboEstNutricional.Text = reg.FM_EstNutricion;

                    // Datos Complementarios
                    //dtpProximaCita.Value = Convert.ToDateTime(reg.FM_ProxCita);
                    txtRecomendaciones.Text = reg.FM_Recomend;
                    txtObservaciones.Text = reg.FM_ObservGenerales;
                }
            }
        }

        void Patologias()
        {
            cboPatologias.DataSource = obj.CboPatologias();
            cboPatologias.ValueMember = "PatologiaId";
            cboPatologias.DisplayMember = "PatologiaNombre";

            cboPatolFamiliar.DataSource = obj.CboPatologias();
            cboPatolFamiliar.ValueMember = "PatologiaId";
            cboPatolFamiliar.DisplayMember = "PatologiaNombre";
        }

        void Cirugias()
        {
            cboCirugias.DataSource = obj.CboCirugias();
            cboCirugias.ValueMember = "Qui_Id";
            cboCirugias.DisplayMember = "Qui_Nombre";
        }

        void Alergias()
        {
            cboAlergias.DataSource = obj.CboAlergias();
            cboAlergias.ValueMember = "AlergiaId";
            cboAlergias.DisplayMember = "AlergiaNombre";
        }

        void Diagnosticos()
        {
            cboCIEDescripcion.DataSource = obj.CboDiagnosticos();
            cboCIEDescripcion.ValueMember = "DiagCod";
            cboCIEDescripcion.DisplayMember = "DiagDescripcion";
        }
        
        void CargarAlergias()
        {
            //for (int n = 1; n <= 10; n++)
            //{
            //    txtAlergias.Text = txtAlergias.Text + "Hola Mundo..." + n.ToString() + "      ";
            //}
            txtAlergias.Text = "Alergias referidas por el paciente";
        }

        void Guardar()
        {
            //Cabecera
            oFMedica.Pac_TipoAtencion = '1'; //Luego cambiar por la variable segun corresponda
            oFMedica.Pac_HC = lblNumHistoria.Text;
            //oFMedica.GrI_Id = 
            //oFMedica.ProfesionId = 
            //oFMedica.Pac_Edad = 

            //Motivo de la Consulta
            oFMedica.FM_MotivoConsulta = txtMotivo.Text;
            oFMedica.FM_Alcohol = chkAlcohol.Checked;
            oFMedica.FM_Tabaco = chkTabaco.Checked;
            oFMedica.FM_Drogas = chkDrogas.Checked;
            oFMedica.FM_OtrosHabNoc = chkOtros.Checked;

            //Examen Físico
            oFMedica.FM_ObservHabNoc = txtObservHabNoc.Text;
            //oFMedica.FM_PAMax = short.Parse(txtPAMaxima.Text);
            //oFMedica.FM_PAMin = sbyte.Parse(txtPAMinima.Text);
            //oFMedica.FM_Pulso = sbyte.Parse(txtPulso.Text);
            //oFMedica.FM_Peso = Convert.ToDecimal(txtPeso.Text);
            //oFMedica.FM_Talla = sbyte.Parse(txtTalla.Text);
            oFMedica.FM_Lucido = chkLucido.Checked;
            oFMedica.FM_OEspacio = chkOEspacio.Checked;
            oFMedica.FM_OTiempo = chkOTiempo.Checked;
            oFMedica.FM_OPersona = chkOPersona.Checked;
            oFMedica.FM_EstGeneral = cboEstGeneral.Text;
            oFMedica.FM_EstHidratacion = cboEstHidratacion.Text;
            oFMedica.FM_EstNutricion = cboEstNutricional.Text;
            oFMedica.FM_Estado = EstadoFicha;

            // Datos Complementarios
            oFMedica.FM_ProxCita = dtpProximaCita.Value.Date;
            oFMedica.FM_Recomend = txtRecomendaciones.Text;
            oFMedica.FM_ObservGenerales = txtObservaciones.Text;

            if (FichaNum > 0)
                {
                    oFMedica.FichaMedicaId = Convert.ToSByte(FichaNum);
                    obj.FMedicaActualizar(oFMedica);
                }
            else
                { obj.FMedicaAdicionar(oFMedica); }

            FichaMedicaId(frmPacCitados.NumHistoria);
            lblGuardando.Visible = true;
            pbGrabar.Show();
            timer1.Start();
        }

        void CboPrincActivoFA()
        {
            cboPrinActivoFA.DataSource = obj.CboPrincActivoFA();
            cboPrinActivoFA.ValueMember = "PrA_Id";
            cboPrinActivoFA.DisplayMember = "PrA_Nombre";
        }

        void InfPrincActivoFA(int PrA_Id)
        {
            List<usp_InfPrincActivosFAResult> InfPrinActivo = new List<usp_InfPrincActivosFAResult>();
            InfPrinActivo = obj.InfPrincActivoFA(PrA_Id);
            if (InfPrinActivo.Count() > 0)
            {
                foreach (usp_InfPrincActivosFAResult reg in InfPrinActivo)
                {
                    lblPrA_Nombre.Text = reg.PrA_Nombre;
                    //txtPrA_Presentacion.Text = reg.PrA_Presentacion;
                    txtPrA_Dosis.Text = reg.PrA_Dosis;
                    txtPrA_Contraindicaciones.Text = reg.PrA_Contraindicaciones;
                    txtPrA_Interacciones.Text = reg.PrA_Interacciones;
                }
            }
        }

        void InfProductos(int Prd_Id)
        {
            List<usp_InfProductosResult> InfProductos = new List<usp_InfProductosResult>();
            InfProductos = obj.InfProductos(Prd_Id);
            if (InfProductos.Count() > 0)
            {
                foreach (usp_InfProductosResult reg in InfProductos)
                {
                    lblClasifTerap.Text = reg.ClasTerapNombre;
                    txtLaboratorio.Text = reg.Lab_Nombre;
                }
            }
            else
            {
                lblClasifTerap.Text = " ";
                txtLaboratorio.Text = " ";
            }
        }

        void CboMedicinas(int Pra_Id)
        {
            cboMedicamentos.Text = " ";
            cboMedicamentos.DataSource = obj.CboMedicamentos(PrA_Id);
            cboMedicamentos.ValueMember = "Prd_Id";
            cboMedicamentos.DisplayMember = "Prd_Descripcion";
        }

         private void frmFichaMedica_Load(object sender, EventArgs e)
        {
            lblNumHistoria.Text = frmPacCitados.NumHistoria;
            FichaMedicaId(frmPacCitados.NumHistoria);
            CargarCabecera();

            if (FichaNum > 0)
                { 
                    CargarFicha();
                    btnCerrar.Enabled = true;
                }
            else
                {
                    btnCerrar.Enabled = false;
                }

            if (lblSexo.Text == "Masculino")
                {
                    txtFUltMenstruacion.Text = "No Aplica";
                    txtFUltMenstruacion.ReadOnly = true;
                }

            dgvSintomas.DataSource = obj.DgvSintomasVer(FichaNum);
            for (int indice = 0; indice < dgvSintomas.Rows.Count; indice++)
            {
                dgvSintomas.Columns[1].Width = 150;
                dgvSintomas.Columns[2].Width = 240;
                dgvSintomas.Rows[indice].Cells[1].ReadOnly = true;
                dgvSintomas.Rows[indice].Cells[2].ReadOnly = true;
            }

            dgvPrescripcion.DataSource = obj.DgvPrescripcion(FichaNum);
            dgvAntecPatologia.DataSource = obj.DgvAntecPatologicos(lblNumHistoria.Text);
            lblGuardando.Visible = false;
            pbGrabar.Hide();
            Diagnosticos();
            CargarAlergias();
            txtMotivo.Focus();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            pbGrabar.Increment(500);
            if (pbGrabar.Value == pbGrabar.Maximum)
            {
                timer1.Stop();
                lblGuardando.Visible = false;
                pbGrabar.Hide();
                pbGrabar.Value = 0;
            }
        }

        private void tabPage1_Leave(object sender, EventArgs e)
        {
            EstadoFicha = Convert.ToChar("0");
            Guardar();
        }
        
        private void tabPage2_Leave(object sender, EventArgs e)
        {
            EstadoFicha = Convert.ToChar("0");
            Guardar();
        }

         private void tabPage3_Leave(object sender, EventArgs e)
        {
            EstadoFicha = Convert.ToChar("0");
            Guardar();
        }

         private void tabPage4_Leave(object sender, EventArgs e)
         {
             EstadoFicha = Convert.ToChar("0");
             Guardar();
         }

         private void tabPage5_Leave(object sender, EventArgs e)
         {
             EstadoFicha = Convert.ToChar("0");
             Guardar();
         }

         private void tabPage6_Leave(object sender, EventArgs e)
         {
             EstadoFicha = Convert.ToChar("0");
             Guardar();
         }

         private void tabPage7_Leave(object sender, EventArgs e)
         {
             EstadoFicha = Convert.ToChar("0");
             Guardar();
         }

         private void btnCerrar_Click(object sender, EventArgs e)
         {
             if ((MessageBox.Show("Después de CERRAR la ficha ya no podrá modificar nada de lo que ha registrado en la misma ¿Seguro que desea CERRAR ?", "Aviso",
                      MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                      MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes))
             {
                 EstadoFicha = Convert.ToChar("1");
                 Guardar();
                 this.Close();
             }
         }

         private void btnHistorial_Click(object sender, EventArgs e)
         {
             Guardar();
             MessageBox.Show("Mostrar relación cronológica de Atenciones");
         }

         private void tabPage8_Leave(object sender, EventArgs e)
         {
             EstadoFicha = Convert.ToChar("0");
             Guardar();
         }

         private void tabPage9_Leave(object sender, EventArgs e)
         {
             EstadoFicha = Convert.ToChar("0");
             Guardar();
         }

         private void tabPage10_Leave(object sender, EventArgs e)
         {
             EstadoFicha = Convert.ToChar("0");
             Guardar();
         }

         private void dgvSintomas_CellContentClick(object sender, DataGridViewCellEventArgs e)
         {
             if (e.RowIndex == -1)
                 return;

             if (dgvSintomas.Columns[e.ColumnIndex].Name == "Seleccion")
             {
                 DataGridViewRow row = dgvSintomas.Rows[e.RowIndex];
   
                 DataGridViewCheckBoxCell cellSelecion = row.Cells["Seleccion"] as DataGridViewCheckBoxCell;

                 int indice = dgvSintomas.CurrentCell.RowIndex;

                 if (Convert.ToBoolean(cellSelecion.Value))
                 {
                     dgvSintomas.Rows[indice].Cells[2].ReadOnly = false;
                 }
                 else
                 {
                     dgvSintomas.Rows[indice].Cells[2].Value = " ";
                     dgvSintomas.Rows[indice].Cells[2].ReadOnly = true;
                 }
             }
         }

         private void dgvSintomas_CurrentCellDirtyStateChanged(object sender, EventArgs e)
         {
             if (dgvSintomas.IsCurrentCellDirty)
                {
                    dgvSintomas.CommitEdit(DataGridViewDataErrorContexts.Commit);
                }
         }

         private void tbpAntecedentes_Enter(object sender, EventArgs e)
         {
             Patologias();
             Cirugias();
             Alergias();

             //Configurar Grid de Antecedentes Patológicos

             for (int indice = 0; indice < dgvAntecPatologia.Rows.Count; indice++)
             {
                 dgvAntecPatologia.Columns[0].Visible = false;  //Id
                 dgvAntecPatologia.Columns[1].Width = 195;      //Patología
                 dgvAntecPatologia.Columns[2].Width = 200;      //Observaciones

                 //dgvAntecPatologia.Rows[indice].ReadOnly = true;
                 //dgvAntecPatologia.Rows[indice].Cells[1].ReadOnly = true;
                 //dgvAntecPatologia.Rows[indice].Cells[2].ReadOnly = true;
             }

             btnAddAntecPatologia.Enabled = false;
             btnUpdAntecPatologia.Enabled = false;
             btnDelAntecPatologia.Enabled = false;

             txtPatologiaObserv.Focus();

         }

         private void cboCIEDescripcion_SelectedIndexChanged(object sender, EventArgs e)
         {
             txtCIECod.Text = cboCIEDescripcion.SelectedValue.ToString();
         }

         private void frmFichaMedica_FormClosed(object sender, FormClosedEventArgs e)
         {
             Guardar();
         }

         private void tbpPrescripcion_Enter(object sender, EventArgs e)
         {
             CboPrincActivoFA();
             cboPrinActivoFA.Focus();
             InfPrincActivoFA(0);
             //DgvComerciales(1);
         }

         private void cboPrinActivoFA_SelectionChangeCommitted(object sender, EventArgs e)
         {
             PrA_Id = Convert.ToInt16(cboPrinActivoFA.SelectedValue.ToString());
 //            InfPrincActivoFA(PrA_Id);
             CboMedicinas(PrA_Id);
             
            // DgvComerciales(PrA_Id);
         }

         private void cboMedicamentos_SelectionChangeCommitted(object sender, EventArgs e)
         {
             PrA_Id = Convert.ToInt16(cboPrinActivoFA.SelectedValue.ToString());
             InfPrincActivoFA(PrA_Id);
             Prd_Id = Convert.ToInt16(cboMedicamentos.SelectedValue.ToString());
             InfProductos(Prd_Id);
         }

         private void cboMedicamentos_Enter(object sender, EventArgs e)
         {
             PrA_Id = Convert.ToInt16(cboPrinActivoFA.SelectedValue.ToString());
             InfPrincActivoFA(PrA_Id);
             Prd_Id = Convert.ToInt16(cboMedicamentos.SelectedValue.ToString());
             InfProductos(Prd_Id);

         }

         private void cboMedicamentos_Leave(object sender, EventArgs e)
         {
             lblPrA_Nombre.Text = "Principio Activo";
             lblClasifTerap.Text = "Clasificación Terapeútica";
             txtLaboratorio.Text = " ";
             txtPrA_Dosis.Text = " ";
             txtPrA_Interacciones.Text = " ";
             txtPrA_Contraindicaciones.Text = " ";
         }

         private void btnAddPresc_Click(object sender, EventArgs e)
         {         
             oFM_Prescripcion.FichaMedicaId = Convert.ToSByte(FichaNum);
             oFM_Prescripcion.PrA_Id = Convert.ToSByte(PrA_Id);
             oFM_Prescripcion.RpNomComercial = cboMedicamentos.Text;
             oFM_Prescripcion.RpIndicaciones = txtIndicaciones.Text;
             obj.FM_PrescripcionAdd(oFM_Prescripcion);
             dgvPrescripcion.DataSource = obj.DgvPrescripcion(FichaNum);

             cboPrinActivoFA.Text = " ";
             cboMedicamentos.Text = " ";
             lblPrA_Nombre.Text = "Principio Activo";
             lblClasifTerap.Text = "Clasificación Terapeútica";
             txtLaboratorio.Text = " ";
             txtPrA_Dosis.Text = " ";
             txtPrA_Interacciones.Text = " ";
             txtPrA_Contraindicaciones.Text = " ";
         }

         private void btnAddAntecPatologia_Click(object sender, EventArgs e)
         {
             oFM_AntecPatologicos.FichaMedicaId = Convert.ToSByte(FichaNum);
             oFM_AntecPatologicos.Pac_HC = lblNumHistoria.Text;
             oFM_AntecPatologicos.PatologiaId = Convert.ToInt16(cboPatologias.SelectedValue.ToString());
             oFM_AntecPatologicos.PatologiaObserv = txtPatologiaObserv.Text;
             obj.FM_AntecPatologicosAdd(oFM_AntecPatologicos);
             dgvAntecPatologia.DataSource = obj.DgvAntecPatologicos(lblNumHistoria.Text);

             cboPatologias.Text = " ";
             txtPatologiaObserv.Text = " ";
         }

         private void btnUpdAntecPatologia_Click(object sender, EventArgs e)
         {
             oFM_AntecPatologicos.FichaMedicaId = Convert.ToSByte(FichaNum);
             oFM_AntecPatologicos.Pac_HC = lblNumHistoria.Text;
             oFM_AntecPatologicos.PatologiaId = Convert.ToInt16(cboPatologias.SelectedValue.ToString());
             oFM_AntecPatologicos.PatologiaObserv = txtPatologiaObserv.Text;
             obj.FM_AntecPatologicosUpd(oFM_AntecPatologicos);
             dgvAntecPatologia.DataSource = obj.DgvAntecPatologicos(lblNumHistoria.Text);

             cboPatologias.Text = " ";
             txtPatologiaObserv.Text = " ";

         }

         private void btnDelPresc_Click(object sender, EventArgs e)
         {

         }

         private void btnDelAntecPatologia_Click(object sender, EventArgs e)
         {
             oFM_AntecPatologicos.FichaMedicaId = Convert.ToSByte(FichaNum);
             oFM_AntecPatologicos.Pac_HC = lblNumHistoria.Text;
             oFM_AntecPatologicos.PatologiaId = Convert.ToInt16(cboPatologias.SelectedValue.ToString());
             oFM_AntecPatologicos.PatologiaObserv = txtPatologiaObserv.Text;
             obj.FM_AntecPatologicosDel(oFM_AntecPatologicos);
             dgvAntecPatologia.DataSource = obj.DgvAntecPatologicos(lblNumHistoria.Text);

             cboPatologias.Text = " ";
             txtPatologiaObserv.Text = " ";
         }

         private void dgvAntecPatologia_SelectionChanged(object sender, EventArgs e)
         {
             //int indice = dgvAntecPatologia.CurrentCell.RowIndex;
             //int PatologiaId = int.Parse(dgvAntecPatologia.Rows[indice].Cells[0].Value.ToString());

             //List<usp_AntecPatologicosRecResult> lista = new List<usp_AntecPatologicosRecResult>();
             //lista = obj.AntecPatologicosDatos(FichaNum, PatologiaId);
             //if (lista.Count() > 0)
             //{
             //    foreach (usp_AntecPatologicosRecResult reg in lista)
             //    {
             //        cboPatologias.Text = reg.Patología;
             //        txtPatologiaObserv.Text = reg.Observaciones;

             //        cboPatologias.Enabled = false;
             //        txtPatologiaObserv.Enabled = true;
             //        //btnNuevo.Enabled = false;
             //        //btnGrabar.Enabled = false;
             //        //btnActualizar.Enabled = true;
             //        //btnAnular.Enabled = true;
             //        //btnCerrar.Enabled = true;

             //        //lblCreado.Visible = true;
             //        //lblFecCrea.Visible = true;
             //        //lblModificado.Visible = true;
             //        //lblFecMod.Visible = true;

             //        //nuevo = false;
             //        //txtNombre.Focus();
             //    }
             //}
         }

         private void dgvAntecPatologia_CellEnter(object sender, DataGridViewCellEventArgs e)
         {
             int indice = dgvAntecPatologia.CurrentCell.RowIndex;
             int PatologiaId = int.Parse(dgvAntecPatologia.Rows[indice].Cells[0].Value.ToString());

             List<usp_AntecPatologicosRecResult> lista = new List<usp_AntecPatologicosRecResult>();
             lista = obj.AntecPatologicosDatos(FichaNum, PatologiaId);
             if (lista.Count() > 0)
             {
                 foreach (usp_AntecPatologicosRecResult reg in lista)
                 {
                     cboPatologias.Text = reg.Patología;
                     txtPatologiaObserv.Text = reg.Observaciones;

                     //cboPatologias.Enabled = false;
                     txtPatologiaObserv.Enabled = true;
                     //btnNuevo.Enabled = false;
                     //btnGrabar.Enabled = false;
                     //btnActualizar.Enabled = true;
                     //btnAnular.Enabled = true;
                     //btnCerrar.Enabled = true;

                     //lblCreado.Visible = true;
                     //lblFecCrea.Visible = true;
                     //lblModificado.Visible = true;
                     //lblFecMod.Visible = true;

                     //nuevo = false;
                     //txtNombre.Focus();
                 }
             }
         }

         private void cboPatologias_Enter(object sender, EventArgs e)
         {
             cboPatologias.Text = "-- No Refiere --";
             txtPatologiaObserv.Text = " ";
         }
    }
}
